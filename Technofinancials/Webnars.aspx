﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Webnars.aspx.cs" Inherits="Technofinancials.Webnars" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer2.ascx" TagName="Footer" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />

</head>
<body class="slider-body">
    <form id="form1" runat="server">
        <section>
            <div class="container-fluid" style="padding-left:0px;padding-right:0px;">
                <div class="row" style="margin-left:0px;margin-right:0px;">
                    <div class="col-sm-12" style="padding-left:0px;padding-right:0px;">
                        <div class="header-bg-div">
                            <div class="header-name-div">
                                <h2 class="header-name-slide">News & Media</h2>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row"   style="margin-left:0px;margin-right:0px;">
                    <div class="col-sm-12" >
                        <div class="terms-of-use-main-div">
                            <h1 class="terms-of-use-heading">Webnars</h1>
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                        </div>
                    </div>
                </div>

    <uc:Footer ID="Footer1" runat="server"></uc:Footer>


            </div>
        </section>

        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
</body>
</html>
