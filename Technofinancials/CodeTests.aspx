﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CodeTests.aspx.cs" Inherits="Technofinancials.CodeTests" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="/assets/css/jquery.treegrid.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:GridView ID="gvDraft" runat="server" CssClass="gv table table-bordered tree" ClientIDMode="Static" AutoGenerateColumns="false" OnPreRender="gvDraft_PreRender">
            <Columns>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Parent ID">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("ParentID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
        <script src="libs/bower/jquery/dist/jquery.js"></script>
        <script src="/assets/js/jquery.treegrid.js"></script>
        <script>
            $('.tree').treegrid();
        </script>
    </form>
</body>
</html>
