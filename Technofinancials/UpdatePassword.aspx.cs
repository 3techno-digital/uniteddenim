﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials
{
    public partial class UpdatePassword : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        public static string userID = "";
        public static string userEmail = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                
                divAlertMsg.Visible = false;
                string path = HttpContext.Current.Request.Url.AbsolutePath;
                if (path == "/update-password")
                {
                    CheckSessions();
                    userEmail = Session["UserEmail"].ToString();
                }
                else
                {

                    divForm.Visible = false;
                    string passwordKey = "";

                    if (Request.QueryString["user-email"] != null)
                    {
                        userEmail = Request.QueryString["user-email"].ToString();
                    }

                    if (Request.QueryString["access-token"] != null)
                    {
                        passwordKey = Request.QueryString["access-token"].ToString();
                    }

                    checkPasswordKey(userEmail, passwordKey);
                }

            }
        }
        
        private void checkPasswordKey(string userEmail, string passwordKey)
        {
            string returnMsg = "";
            objDB.Email = userEmail;
            objDB.PasswordKey = Common.Decrypt(passwordKey);
            DataTable dt = objDB.GetPassowrdKeyByEmail(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    returnMsg = "Valid Access Token";
                    userID = dt.Rows[0]["UserID"].ToString();
                    userEmail = dt.Rows[0]["UserEmail"].ToString();
                }
                else
                {
                    returnMsg = "<p id = 'link-expired'>Link Expired !!</p> <p id = 'link-expired2'>Please <a href='/forgot-password'>Click Here</a> to generate new Link</p>";
                }
            }
            else
            {
                returnMsg = "<p id = 'link-expired'>Link Expired !!</p> <p id = 'link-expired2'>Please <a href='/forgot-password'>Click Here</a> to generate new Link</p>";
            }

            if (returnMsg != "Valid Access Token")
            {
                divAlertMsg.Visible = true;
                divAlertMsg.InnerHtml = returnMsg;
            }
            else
            {
                divForm.Visible = true;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnUpdatePassword_ServerClick(object sender, EventArgs e)
        {
            string returnMsg = "";
            if (!string.IsNullOrEmpty(userID))
            {
                objDB.UserID = Convert.ToInt32(userID);
            }
            else
            {
                objDB.UserID = Convert.ToInt32(Session["UserID"].ToString());
            }
            if(txtConfirmPassword.Value == txtPassword.Value)
            {
                objDB.Password = txtPassword.Value;
                returnMsg = objDB.UpdatePassword();
            }
            else
            {
                returnMsg = "Password did not match";
            }
            
            if (returnMsg == "Password Updated")
            {
                authenticateUser();
            }
            else
            {
                divAlertMsg.Visible = true;
                pAlertMsg.InnerHtml = returnMsg;
            }
        }

        private string checkAccessLevel()
        {
            string url = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/";

            if (Session["UserAccess"].ToString() == "Directors")
            {
                url += "directors/view/company-profile";
            }
            else if (Session["UserAccess"].ToString() == "Procrument")
            {
                url += "procrument-management/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Human Resource")
            {
                url += "people-management/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "CRM")
            {
                url += "crm/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Normal User")
            {
                url += "employee-self-service/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Fixed Asset")
            {
                url += "fixed-asset/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Finance")
            {
                url += "finance/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "OM")
            {
                url = "http://operations.technofinancials.com/admin/index";
            }
            else if (Session["UserAccess"].ToString() == "CRM General")
            {
                url += "crm-general/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Trading Management System")
            {
                url += "trading-management-system/view/dashboard";
            }
            else
            {
                url += "/login";
            }
            return url;
        }

        private void authenticateUser()
        {
            objDB.Email = userEmail;
            objDB.Password = txtPassword.Value;
            DataTable dt = objDB.AuthenticateUser(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "Invalid Email Address" && dt.Rows[0][0].ToString() != "Account Not Approved" && dt.Rows[0][0].ToString() != "Wrong Password!!")
                    {
                       

                        Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                        Session["EmployeeID"] = dt.Rows[0]["EmployeeID"].ToString();
                        Session["UserEmail"] = dt.Rows[0]["Email"].ToString();
                        Session["UserPassword"] = dt.Rows[0]["Password"].ToString();
                        Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                        Session["UserPhoto"] = dt.Rows[0]["Photo"].ToString();
                        Session["CompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                        Session["CompanyName"] = dt.Rows[0]["CompanyName"].ToString();
                        Session["DeptID"] = dt.Rows[0]["DeptID"].ToString();
                        Session["DeptName"] = dt.Rows[0]["DeptName"].ToString();
                        Session["DesgID"] = dt.Rows[0]["DesgID"].ToString();
                        Session["CompanyLogo"] = dt.Rows[0]["CompanyLogo"].ToString();
                        Session["UserAccess"] = dt.Rows[0]["UserAccess"].ToString();
                        Session["OldUserAccess"] = dt.Rows[0]["UserAccess"].ToString();
                        Session["CompanyShortName"] = dt.Rows[0]["CompanyShortName"].ToString();
                        Session["ShiftID"] = dt.Rows[0]["ShiftID"].ToString();
                        Session["LoginCountryName"] = hdnCountryName.Value;
                        Session["LoginCityName"] = hdnCityName.Value;
                        Session["LoginIP"] = hdnIP.Value;
                        Session["LoginState"] = hdnState.Value;
                        Session["Loginlatitude"] = hdnlatitude.Value;
                        Session["Loginlongitude"] = hdnlongitude.Value;
                        Session["Records"] = dt.Rows[0]["Records"].ToString();

                        if (dt.Rows[0]["ParentCompanyID"] == null)
                        {
                            Session["ParentCompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                        }
                        else if (dt.Rows[0]["ParentCompanyID"].ToString() == "")
                        {
                            Session["ParentCompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                        }
                        else
                        {
                            Session["ParentCompanyID"] = dt.Rows[0]["ParentCompanyID"].ToString();
                        }

                        if (dt.Rows[0]["UserAccess"].ToString() == "Custom" || dt.Rows[0]["Records"].ToString() != "1")
                        {
                            Session["isCustom"] = "True";

                            Response.Redirect("/switch-panel");

                        }
                        else
                        {
                            Session["isCustom"] = "False";
                            Response.Redirect(checkAccessLevel());
                        }

                    }
                    else
                    {
                        divAlertMsg.Visible = true;
                        pAlertMsg.InnerHtml = dt.Rows[0][0].ToString();
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    pAlertMsg.InnerHtml = "Could not connect with database";
                }
            }
            else
            {
                divAlertMsg.Visible = true;
                pAlertMsg.InnerHtml = "Could not connect with database";
            }
        }
    }
}