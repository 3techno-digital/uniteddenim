﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using ComponentSpace.SAML2;

namespace Technofinancials
{
    public partial class Default2 : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        private class AttributeDataSource
        {
            private string attributeName;
            private string attributeValue;

            public static IList<AttributeDataSource> Get(IDictionary<string, string> attributes)
            {
                IList<AttributeDataSource> attributeDataSources = new List<AttributeDataSource>();

                foreach (string attributeName in attributes.Keys)
                {
                    attributeDataSources.Add(new AttributeDataSource(attributeName, HttpUtility.HtmlEncode(attributes[attributeName])));
                }

                return attributeDataSources;
            }

            private AttributeDataSource(string attributeName, string attributeValue)
            {
                this.attributeName = attributeName;
                this.attributeValue = attributeValue;
            }

            public string AttributeName
            {
                get
                {
                    return attributeName;
                }
            }

            public string AttributeValue
            {
                get
                {
                    return attributeValue;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
                // Display the attributes returned by the identity provider.
                IDictionary<string, string> attributes = (IDictionary<string, string>)Session[SAML.AssertionConsumerService.AttributesSessionKey];
                attributes = null;

                if (attributes != null && attributes.Count > 0)
                {
                    attributesDiv.Visible = true;
                    attributesRepeater.DataSource = AttributeDataSource.Get(attributes);
                    attributesRepeater.DataBind();
                }
              
                objDB.Email = Context.User.Identity.Name.ToString();
                string email = Context.User.Identity.Name.ToString();
                // objDB.Password = txtUserPassword.Text;
                DataTable dt = objDB.AuthenticateUserOKTA(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0][0].ToString() != "Invalid Email Address" && dt.Rows[0][0].ToString() != "Account Not Approved" && dt.Rows[0][0].ToString() != "Wrong Password!!")
                        {
                            Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                            Session["EmployeeID"] = dt.Rows[0]["EmployeeID"].ToString();
                            Session["UserEmail"] = dt.Rows[0]["Email"].ToString();
                            Session["UserPassword"] = dt.Rows[0]["Password"].ToString();
                            Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                            Session["UserPhoto"] = dt.Rows[0]["Photo"].ToString();
                            Session["CompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            Session["CompanyName"] = dt.Rows[0]["CompanyName"].ToString();
                            Session["DeptID"] = dt.Rows[0]["DeptID"].ToString();
                            Session["DeptName"] = dt.Rows[0]["DeptName"].ToString();
                            Session["DesgID"] = dt.Rows[0]["DesgID"].ToString();
                            Session["CompanyLogo"] = dt.Rows[0]["CompanyLogo"].ToString();
                            Session["UserAccess"] = dt.Rows[0]["UserAccess"].ToString();
                            Session["OldUserAccess"] = dt.Rows[0]["UserAccess"].ToString();
                            Session["CompanyShortName"] = dt.Rows[0]["CompanyShortName"].ToString();
                            Session["ShiftID"] = dt.Rows[0]["ShiftID"].ToString();
                            Session["LoginCountryName"] = hdnCountryName.Value;
                            Session["LoginCityName"] = hdnCityName.Value;
                            Session["LoginIP"] = hdnIP.Value;
                            Session["LoginState"] = hdnState.Value;
                            Session["Loginlatitude"] = hdnlatitude.Value;
                            Session["Loginlongitude"] = hdnlongitude.Value;
                            Session["Records"] = dt.Rows[0]["Records"].ToString();
                            Session["TeamCount"] = dt.Rows[0]["TeamCount"].ToString();

                            if (dt.Rows[0]["ParentCompanyID"] == null)
                            {
                                Session["ParentCompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            }
                            else if (dt.Rows[0]["ParentCompanyID"].ToString() == "" || dt.Rows[0]["ParentCompanyID"].ToString() == "0")
                            {
                                Session["ParentCompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            }
                            else
                            {
                                Session["ParentCompanyID"] = dt.Rows[0]["ParentCompanyID"].ToString();
                            }
                            if (dt.Rows[0]["isNew"].ToString() == "True")
                            {
                                Session["isNewUser"] = "1";
                                Response.Redirect("/update-security-questions");
                            }

                            if (dt.Rows[0]["UserAccess"].ToString() == "Custom" || dt.Rows[0]["Records"].ToString() != "1")
                            {
                                Session["isCustom"] = "True";

                                Response.Redirect("/switch-panel");

                            }
                            else
                            {
                                Session["isCustom"] = "False";
                                Response.Redirect(checkAccessLevel());
                            }
                        }
                        else
                        {
                            //divAlertMsg.Visible = true;
                            //pAlertMsg.InnerHtml = dt.Rows[0][0].ToString();
                        }
                    }
                    else
                    {
                        //divAlertMsg.Visible = true;
                        //pAlertMsg.InnerHtml = "Could not connect with database";
                    }
                }

            }
            catch(Exception ex)
			{
				divAlertMsg.Visible = true;
				pAlertMsg.InnerHtml = ex.Message;
			}

        }

        protected void logoutButton_Click(object sender, EventArgs e)
        {
            // Logout locally.
            FormsAuthentication.SignOut();

            if (SAMLServiceProvider.CanSLO(WebConfigurationManager.AppSettings[AppSettings.PartnerIdP]))
            {
                // Request logout at the identity provider.
                string partnerIdP = WebConfigurationManager.AppSettings[AppSettings.PartnerIdP];
                SAMLServiceProvider.InitiateSLO(Response, null, null, partnerIdP);
            }
            else
            {
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        private string checkAccessLevel()
        {

            string url = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/";

            if (Session["UserAccess"].ToString() == "Directors")
            {
                Common.addlog("Login", "Admin", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "directors/view/company-profile";
            }
            if (Session["UserAccess"].ToString() == "Procrument")
            {
                Common.addlog("Login", "Procrument", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "procrument-management/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Human Resource")
            {
                Common.addlog("Login", "HR", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "people-management/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Normal User")
            {
                Common.addlog("Login", "ESS", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "employee-self-service/view/dashboard";
            }
            // for US employee
            else if (Session["UserAccess"].ToString() == "Limited")
            {
                Common.addlog("Login", "ESS", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "employee-self-service/view/dashboardUS";
            }
            else if (Session["UserAccess"].ToString() == "Fixed Asset")
            {
                Common.addlog("Login", "FAM", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "fixed-asset/view/dashboard";
            }

            else if (Session["UserAccess"].ToString() == "CRM")
            {
                Common.addlog("Login", "CRM", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "crm/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Finance")
            {
                Common.addlog("Login", "Finance", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "finance/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "CRM General")
            {
                Common.addlog("Login", "CRM General", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "crm-general/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "OM")
            {
                Common.addlog("Login", "Operation Management", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url = "http://operations.technofinancials.com/admin/index";

            }
            else if (Session["UserAccess"].ToString() == "Super Admin")
            {
                //Common.addlog("Login", "SuperUser", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url = "/technofinancials/super-admin/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Trading Management System")
            {
                Common.addlog("Login", "Trading Management System", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));
                url = "technofinancials/trading-management-system/view/dashboard";
            }
            return url;
        }
    }
}