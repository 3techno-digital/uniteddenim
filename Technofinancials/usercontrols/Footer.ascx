﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="Technofinancials.usercontrols.Footer" %>
<!-- APP FOOTER -->
<div class="wrap p-t-0  tf-navbar tf-footer-wrapper">
    <footer class="app-footer tf-footer">
        <div class="clearfix">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="tf-color-white tf-font-times-new-roman tf-mt-5">&copy; <span id="spnCurrentYear" runat="server"></span> TechnoFinancials. All rights reserved.</div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <ul class="footer-menu centre" style="float: none;">
                    <li><a href="/privacy-policy" class="tf-color-white tf-footer-border-right tf-font-times-new-roman">Privacy</a></li>
                    <li><a href="/security" class="tf-color-white tf-footer-border-right tf-font-times-new-roman">Security</a></li>
                    <li><a href="/terms-of-use" class="tf-color-white tf-font-times-new-roman">Terms of Service</a></li>
                </ul>
            </div>
<%--            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="copyright pull-right">
                    <a href="http://3techno.com" target="_blank" class="tf-footer-name tf-font-times-new-roman"> A Product of
                    <img src="/assets/images/footer_logo.png" class="tf-footer-logo" />
                    3techno</a></div>
            </div>--%>
        </div>
    </footer>
</div>
<!-- /#app-footer -->
