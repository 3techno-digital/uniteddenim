﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.usercontrols
{
    public partial class Footer2 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
            }
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            divAlertMsg.Visible = true;
            string res = sendEmails();

            if (res == "Message Recived successfully")
            {
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }


        private string sendEmails()
        {
            string returnMsg = "Message Recived successfully";
            try
            {
                
                        string html = "<p><b>A new user subscribed to your newsletter</b></p><br />";
                        html += txtUserEmail.Value;
                        html += "<br /><br /><p style='color:red;'>Please DO NOT reply to this email.</p>";
                        MailMessage mail = new MailMessage();
                        mail.Subject = "Newsletter";
                        mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(),"Technofinancials");
                        mail.To.Add("info@technofinancials.com");
                        mail.Body = html;
                        mail.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["NoReplySMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NoReplyPort"].ToString()));
                        smtp.EnableSsl = true;
                        NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["NoReplyPassword"].ToString());
                        smtp.Credentials = netCre;
                        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                        smtp.Send(mail);
                  
              
            }
            catch (Exception ex)
            {
                returnMsg = ex.Message;
            }
            return returnMsg;

        }


    }
}