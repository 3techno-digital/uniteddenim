﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer2.ascx.cs" Inherits="Technofinancials.usercontrols.Footer2" %>

        <asp:ScriptManager ID="ScrptMangr" runat="server"></asp:ScriptManager>
                <div class="footer-bg-div">
                    <div class="footer-bg-inner-div">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="footer-links-div">
                                    <ul class="ull">
                                        <li  class="ull-li"><a href="/features">Features</a></li>
                                        <hr class="footer-bg-div-hr" />
                                        <li class="ull-li"><a href="/news">News</a></li>
                                        <hr  class="footer-bg-div-hr" />
                                        <li class="ull-li"><a href="/blogs">Blogs</a></li>
                                        <hr class="footer-bg-div-hr" />
                                        <li class="ull-li" ><a href="/press-realease">Press Release</a></li>
                                        <hr class="footer-bg-div-hr" />
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="footer-links-div-2">
                                    <ul class="ull">
                                        <%--<li class="ull-li" ><a href="/awards">Awards</a></li>
                                        <hr class="footer-bg-div-hr" />--%>
                                        <li class="ull-li" ><a href="/events">Events</a></li>
                                        <hr class="footer-bg-div-hr" />
                                       <%-- <li class="ull-li" ><a href="/webnars">Webnars</a></li>
                                        <hr class="footer-bg-div-hr" />--%>

                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-5">
                            </div>
                            
                     <asp:UpdatePanel ID="UpdPnl" runat="server">
                                <ContentTemplate>
                            <div class="col-sm-3">
                                <img src="/assets/images/tf-logo-white.png" class="footer-tf-logo" />

                                <label  class="textbox-label">Subscribe to our monthly newsletter</label>
                                <div class="form-group">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtUserEmail" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnNewsLtr" ForeColor="Red" ></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" runat="server" ControlToValidate="txtUserEmail" ForeColor="Red" ErrorMessage=" Email Address Not Valid" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="btnNewsLtr"></asp:RegularExpressionValidator>
                                    <input type="email" class="form-control" runat="server" id="txtUserEmail" placeholder="Enter Your Email Address" style="height: 50px !important;" />
                                    <asp:Button Text="text" OnClick="Unnamed_Click" ID="SubButton" style="display:none;" runat="server" ValidationGroup="btnNewsLtr" />
                                </div>
                               <br />
                             <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                        </div>
                            
                                    </ContentTemplate>
                         </asp:UpdatePanel>
                        </div>
                     <hr style="border-top:2px solid white;" />

                        <div class="row">
                         
                            <div class="col-sm-12">
                                <div style="float: right;">
                                    <i class="fa fa-facebook" aria-hidden="true" style="font-size: 24px;color: #003780;margin-right: 10px;border-radius: 0px !important;background: white;padding: 3px 10px 1px 8px;"></i>
                                    <i class="fa fa-twitter" aria-hidden="true" style="    font-size: 24px;color: #1da7df;margin-right: 10px;border-radius: 0px !important;background: white;padding:3px 5px 1px 4px;"></i>
                                    <i class="fa fa-linkedin" aria-hidden="true" style="    font-size: 24px;color: #003780;margin-right: 10px;border-radius: 0px !important;background: white;padding: 3px 4px 1px 6px;"></i>
                                    <i class="fa fa-google-plus" aria-hidden="true" style="    font-size: 23px;color: #1da8e1; margin-right: 10px;border-radius: 0px !important;background: white;padding:4px 2px 1px 2px;"></i>

                                    <i class="fa fa-play" aria-hidden="true" style="    font-size: 18px;color: #003780;border-radius: 0px !important;background: white;padding: 9px 7px 1px 8px;"></i>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                              <div class="col-sm-4">
                                <img src="/assets/images/3t-logo.png" style="width: 35px;" />
                                <p  class="trusted-digital-partner"><span style="font-style: italic;">Trusted Digital Partner</span></p>
                            </div>


                            <div class="col-sm-8">
                                <p  class="copyright-footer">Copyright © 2017-<% Response.Write(DateTime.Now.Year); %> . All rights reserved by 3techno Digital Pvt Ltd.</p>
                            </div>


                        </div>
                    </div>
                </div>

