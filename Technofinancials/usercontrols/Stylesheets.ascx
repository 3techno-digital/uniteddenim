﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Stylesheets.ascx.cs" Inherits="Technofinancials.usercontrols.Stylesheets" %>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="Admin, Dashboard, Bootstrap" />
<link rel="shortcut icon" sizes="196x196" href="/assets/images/Fav_icon_tf.png">
<title>Techno Financials | A Product of 3Techno Digital </title>
<link rel="stylesheet" href="/libs/bower/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.css">
<%--<link rel="stylesheet" href="/libs/bower/animate.css/animate.min.css">--%>
<link rel="stylesheet" href="/libs/bower/fullcalendar/dist/fullcalendar.min.css">
<link rel="stylesheet" href="/libs/bower/perfect-scrollbar/css/perfect-scrollbar.css">
<link rel="stylesheet" href="/assets/css/bootstrap.css">
<link rel="stylesheet" href="/assets/css/core.css">
<link rel="stylesheet" href="/assets/css/app.css">
<link rel="stylesheet" href="/assets/css/styles.css">
<link href="https://fonts.googleapis.com/css?family=Muli:400,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css" />
<link rel="stylesheet" href="/assets/css/PA/csshake.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link href="/assets/css/PA/fontawesome-stars.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">
<link href="/libs/bower/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"  rel="stylesheet" type="text/css" />  
   