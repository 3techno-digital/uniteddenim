﻿using System;
using System.Web.UI;

namespace Technofinancials.usercontrols
{
	public partial class Header : UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");

            GlobalLogo.Src= Session["CompanyLogo"].ToString(); ;
            if (!Page.IsPostBack)
            {
                
                imgCompanyLogo.Src = Session["CompanyLogo"].ToString();
                lblUserName.InnerHtml = "Welcome, " + Session["UserName"].ToString();
                lblLoginUserName.Text= Session["UserName"].ToString();
                string profileLable = "Profile";
                string url = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/";
                string url3 = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/";
                if (Session["isCustom"].ToString() == "True")
                {
                    lnkSwitch.Visible = true;
                }
                else
                {
                    lnkSwitch.Visible = false;
                }

                if (Session["UserAccess"].ToString() == "Normal User")
                {
                    if (Session["OldUserAccess"].ToString() == "Directors")
                    {
                        profileLable = "Directors Module";
                        url3 += "directors/view/company-profile";
                    }
                    if (Session["OldUserAccess"].ToString() == "Procrument")
                    {
                        profileLable = "Procrument Module";
                        url3 += "procrument-management/view/dashboard";
                    }
                    else if (Session["OldUserAccess"].ToString() == "Human Resource")
                    {
                        profileLable = "HCM Module";
                        url3 += "people-management/view/dashboard";
                    }
                    else if (Session["OldUserAccess"].ToString() == "IT")
                    {
                        profileLable = "Roster Management";
                        url3 += "IT/view/dashboard";
                    }
                    else if (Session["OldUserAccess"].ToString() == "CRM")
                    {
                        profileLable = "CRM Module";
                        url3 += "crm/view/dashboard";
                    }
                    else if (Session["OldUserAccess"].ToString() == "Fixed Asset")
                    {
                        profileLable = "FAM Module";
                        url3 += "fixed-asset/view/dashboard";
                    }
                    else if (Session["OldUserAccess"].ToString() == "Finance")
                    {
                        profileLable = "Finance Module";
                        url3 += "finance/view/dashboard";
                    }
                    else if (Session["OldUserAccess"].ToString() == "Normal User")
                    {
                        profileLable = "Dashboard";
                        url3 += "employee-self-service/view/dashboard";
                       // hrefProfile.Visible = false;
                    }

                }
                else
                {
                    profileLable = "Profile";
                    url3 += "employee-self-service/view/dashboard";
                    //hrefProfile.Visible = false;
                }

                hrefProfile.HRef = url3;
                hrefProfile.InnerHtml = profileLable;
                hrefProfile.Visible = false;
                if (Session["UserAccess"].ToString() == "Directors")
                {
                    url += "directors/view/company-profile";
                }
                if (Session["UserAccess"].ToString() == "Procrument")
                {
                    url += "procrument-management/view/dashboard";
                }
                else if (Session["UserAccess"].ToString() == "Human Resource")
                {
                    url += "people-management/view/dashboard";
                }
                else if (Session["UserAccess"].ToString() == "IT")
                {
                    url += "IT/view/dashboard";
                }
                else if (Session["UserAccess"].ToString() == "Normal User")
                {
                    url += "employee-self-service/view/dashboard";
                }
                else if (Session["UserAccess"].ToString() == "CRM")
                {
                    url += "crm/view/dashboard";
                }
                else if (Session["UserAccess"].ToString() == "Fixed Asset")
                {
                    url += "fixed-asset/view/dashboard";
                }
                else if (Session["UserAccess"].ToString() == "Finance")
                {
                    url += "finance/view/dashboard";
                }

                btnDashboard.HRef = url;

                string url2 = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/";

                if (Session["UserAccess"].ToString() == "Directors")
                {
                    url2 = "/change-password";
                }
                if (Session["UserAccess"].ToString() == "Procrument")
                {
                    url2 += "procrument-management/manage/password/change-password";
                }
                else if (Session["UserAccess"].ToString() == "Human Resource")
                {
                    url2 += "people-management/manage/password/change-password";
                }
                else if (Session["UserAccess"].ToString() == "CRM")
                {
                    url2 += "crm/manage/password/change-password";
                }
                else if (Session["UserAccess"].ToString() == "Normal User")
                {
                    url2 += "employee-self-service/manage/password/change-password";
                }
                else if (Session["UserAccess"].ToString() == "Fixed Asset")
                {
                    url2 += "fixed-asset/manage/password/change-password";
                }
                else if (Session["UserAccess"].ToString() == "Finance")
                {
                    url2 += "finance/manage/password/change-password";
                }
                else if (Session["UserAccess"].ToString() == "IT")
                {
                    url2 += "IT/manage/password/change-password";
                }

                    btnChangePassword.HRef = url2;

                
                
            }

            Page.ClientScript.GetPostBackEventReference(this, string.Empty);
            string ctrlName = Request.Params.Get("__EVENTTARGET");
            string ctrlArgs = Request.Params.Get("__EVENTARGUMENT");
            if (!String.IsNullOrEmpty(ctrlName) && ctrlName == "LinkButtonSingOut")
            {
                Common.addlog("Logout", "All", "User \"" + Session["UserName"].ToString() + "\" Logout", "BalanceTransfer", Convert.ToInt32(Session["UserID"].ToString()));

                Session.Clear();
                Session.Abandon();
                Session["UserID"] = null;
                Session["EmployeeID"] = null;
                Session["UserEmail"] = null;
                Session["UserName"] = null;
                Session["UserPhoto"] = null;
                Session["CompanyID"] = null;
                Session["CompanyName"] = null;
                Session["DeptID"] = null;
                Session["DesgID"] = null;

                Response.Redirect("/login");
            }
        }

        protected void lnkSwitch_Click(object sender, EventArgs e)
        {

        }
    }
}