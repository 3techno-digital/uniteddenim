﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="Technofinancials.usercontrols.Header" %>
<!-- APP NAVBAR ==========-->

<nav id="app-navbar" class="navbar navbar-inverse navbar-fixed-top tf-navbar">

    <!-- navbar header -->
    <div class="tf-header-padding">
        <div class="navbar-header tf-navbar tf-border-right">

            <button type="button" id="menubar-toggle-btn" class="navbar-toggle visible-xs-inline-block navbar-toggle-left hamburger hamburger--collapse js-hamburger">
                <span class="sr-only">Toggle navigation</span>
                <span class="hamburger-box"><span class="hamburger-inner"></span></span>
            </button>

            <button type="button" class="navbar-toggle navbar-toggle-right collapsed " data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="zmdi zmdi-hc-lg zmdi-more"></span>
            </button>

            <a id="btnDashboard" runat="server" class="navbar-brand">

                <span class="brand-icon"><i></i></span>
                <span class="brand-name">
                    <img src="/assets/images/tf.png" class="img-responsive "  style="max-width: 90%;" />
                </span>
            </a>

        </div>
        <!-- .navbar-header -->
    </div>
    <div class="navbar-container container-fluid tf-navbar">
        <div class="collapse navbar-collapse " id="app-navbar-collapse">

            <ul class="nav navbar-toolbar navbar-toolbar-left navbar-left">

                <li class="hidden-menubar-top">
                      <img style="display:none;" id="imgCompanyLogo" runat="server" class="img-responsive tf-client-logo" alt="Beta 2.0"  clientidmode="static" />
                     <img id="GlobalLogo" runat="server" src="/assets/images/admin-payroll_48120.png" class="img-responsive tf-client-logo" alt="Beta 2.0" />
<%--                    <img src="/assets/images/sandbox.png" class="img-responsive tf-client-logo" alt="Alternate Text" alt="Beta 2.0" />   --%>                 
                </li>

                 <li class="hidden-float hidden-menubar-top D_NONE">
                    <%if (Session["UserID"] == null)
                            Response.Redirect("/login");
                    %>
                                       

                </li>

            </ul>
            <ul class="nav navbar-toolbar navbar-toolbar-right navbar-right">

            
                <li class="nav-item dropdown new_drop">
                    <asp:Label CssClass="login-user-name" runat="server" ID="lblLoginUserName"></asp:Label>
                </li>

               
                <li class="dropdown notification_data">
                    <span class="notiNum" id="NotificatioCount">0</span>
                    <a class="bellNew" style="height: 30px !important;" data-toggle="dropdown" href="#">
                       <i class="fa fa-bell" aria-hidden="true" style="font-size: 15px;padding-left: 1px;padding-right: 1px;"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <ul class="noti_ul TFNotificationList2" id="style-12">
                            <li>
                                <h5 class="">Notifications <span class="float-right"><a class="clearAllA" onclick="ViewAll();" id="btnViewAll">Clear All</a></span></h5>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item dropdown new_drop">
                    <a class="nav-link user" data-toggle="dropdown" href="#">
                        <i class="fa fa-user" aria-hidden="true" style="font-size: 15px;padding-top: 3px;"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right dash_User">
                        <span class="dropdown-header">
                            <img src="/assets/images/user.png" class="img-responsive" >
                        </span>
                        <ul>
                            <li> <a href="javascript:void(0)" class="user_name" id="lblUserName" runat="server">Fahad</a></li>
                           <li><a id="hrefProfile" runat="server" class="dropdown-item" href="#">Profile</a></li> 
                           <li runat="server" id="lnkSwitch"><a href="<% Response.Write("/switch-panel"); %>" class="dropdown-item ">Switch Panel</a></li> 
                           <li><a id="btnChangePassword" style="display:none;" runat="server" class="dropdown-item ">Change Password</a></li> 
                           <li><a onclick="LogOut();" class="dropdown-item dropdown-footer" style="cursor: pointer;">Logout</a></li> 
                        </ul>
                    </div>
                </li>


            </ul>
        </div>
    </div>
    <!-- navbar-container -->
</nav>
        
<div id="audiofile" style="display: none">

    <audio id="NotificationAudio">
        <source src="/assets/audio/ios.mp3" type="audio/mpeg">
    </audio>

</div>
<script>
    function LogOut() {
        __doPostBack('LinkButtonSingOut', '');
    }
</script>

<style>
            .crossss {
            position: relative;
            top: -40px;
            float: right;
            height: 0;
        }
        .notification_data .notiNum {
            position: absolute;
            background: #D11010;
            font-size: 11px;
            border-radius: 20px;
            width: 25px;
            height: 25px;
            text-align: center;
            padding: 4px 0;
            font-weight: bold;
            overflow: hidden;
            margin-left: 24px;
            margin-top: 3px;
            z-index: 1;
        }

        .nav-item .bell {
            color: #737373 !IMPORTANT;
            border: 2px solid #737373;
            padding: 4px 7px !IMPORTANT;
            border-radius: 38px;
            margin: 6px 5px 0;
            height: 32px !important;
        }

    .notification_data .noti_ul {
        list-style-type: none;
        padding: 0;
        height: 246px;
        overflow-y: overlay;
        color: #212529;
    }
        .notification_data .noti_ul li {
            padding: .5rem 1rem;
            border-bottom: 1px solid #e9ecef;
            cursor: pointer;
        }
            .notification_data .noti_ul li:hover {
                background: #003780 ;
                color: #fff;
            }
            .notification_data .noti_ul li:hover a {
                background: #003780 ;
                color: #fff;
            }
            .clearAllA{
                color: #003780 !important;
                  background: #fff !important;
            }
            .clearAllA:hover{
                color: #003780 !important;
                  background: #fff !important;
            }
            .notification_data .noti_ul li:nth-child(1) {
                padding: 1rem 1rem;
            }
                .notification_data .noti_ul li:nth-child(1):hover {
                    background: none !important;
                    color: inherit !important;
                }
        .notification_data .noti_ul li:nth-last-child(1) {
            border-bottom: none !important;
        }
            .notification_data .noti_ul li:nth-last-child(1):hover {
            
                border-radius:0 0 10px 10px;
            }
    .notification_data p {
        font-size: 10px;
        margin: 4px 0 0;
    }
    .notification_data p span {
        text-align: right;
        float: right;
    }
    .notification_data h5 {
        font-size: 12px;
        margin: 0;
          font-family: Noto-SemiBold;
    }
    .notification_data h6 {
        font-size: 12px;
        margin: 0;
    }

        .float-right {
            text-align: right;
            float: right;
        }

        .tf-bell-btn {
            background-color: #575757 !important;
            padding: 6px !important;
            border-radius: 50px !important;
            border: none !important;
            color: #fff !important;
            box-shadow: 0 8px 6px -5px #cacaca !important;
            height: 30px !important;
            width: 30px;
            margin-top: 6px;
            margin-bottom: 14px;
        }

            .tf-bell-btn i {
                color: #fff !important;
                font-family: fontawesome;
                font-style: normal;
                font-size: 18px;
            }

        .nav-item .plus:hover,
        .nav-item .notify:hover,
        .nav-item .question:hover,
        .nav-item .user:hover {
            color: #fff !IMPORTANT;
            background-color: #737373 !important;
        }

        .nav-item .plus:focus,
        .nav-item .notify:focus,
        .nav-item .question:focus,
        .nav-item .user:focus {
            color: #fff !IMPORTANT;
            background-color: #737373 !important;
        }
        /*.nav .open.new_drop > a,
        .nav .open.new_drop > a:hover,
        .nav .open.new_drop > a:focus {
            color: #737373 !IMPORTANT;
            border: 2px solid #737373;
        }*/
        .nav .open > a,
        .nav .open > a:hover,
        .nav .open > a:focus {
            border-color: #737373 !important;
        }

        .nav-item .user {
            color: #737373 !IMPORTANT;
            border: 2px solid #737373;
            padding: 3px 9px !IMPORTANT;
            border-radius: 38px;
            margin: 6px 5px 0;
            height: 32px !important;
        }

        .nav-item .plus {
            color: #737373 !IMPORTANT;
            border: 2px solid #737373;
            padding: 4px 9px !IMPORTANT;
            border-radius: 38px;
            margin: 6px 5px 0;
            height: 32px !important;
        }

        .dropdown-menu.show {
            display: block;
            top: 38px;
        }

        .dash_User {
            width: 190px !important;
            border-radius: 10px;
            padding: 0 !important;
            border: 1px solid rgba(0,0,0,.15);
        }

        .dropdown-header {
            display: block;
            padding: .5rem 1.5rem;
            margin-bottom: 0;
            font-size: .875rem;
            color: #6c757d;
            white-space: nowrap;
        }
        .dash_User img {
        margin: 0 auto;
        display: block;
        border: 3px solid #8389ab;
        border-radius: 50px;
        /* padding: 3px 2px 0 0; */
        height: 60px;
        width: 60px;
    }
        .dash_User .user_name {
            border-bottom: 1px solid;
            font-size: 14px;
            color: #369de5;
            text-align: center;
    /*        font-family: Noto-Bold !important;*/
            width: 166px;
            margin: 5px auto;
            padding-bottom: 3px;
            display: block;
            cursor: context-menu;
        }

        .dropdown-item {
            display: block;
            width: 100%;
            padding: .25rem 1.5rem;
            clear: both;
            font-weight: 400;
            color: #212529;
            text-align: inherit;
            white-space: nowrap;
            background-color: transparent;
            border: 0;
        }

        .dash_User .dropdown-item {
            color: #464646;
            padding: 8px 13px;
    /*        font-family: Noto-Bold !important;
    */        font-size: 13px;
            cursor:pointer;
        }

            .dash_User .dropdown-item:hover {
                color: #45a4e7;
                background-color: #f8f9fa;
            }

            .dash_User .dropdown-item.dropdown-footer:hover {
                border-radius: 0 0 10px 10px;
            }

            .dash_User .dropdown-item.dropdown-first:hover {
                border-radius: 10px 10px 0 0;
            }

        .notification_data .dropdown-menu {
            padding: 0;
            margin: 0;
            max-width: 300px;
            min-width: 280px;
            border-radius: 10px;
            border: 1px solid rgba(0,0,0,.15);
        }
        ul.dropdown-menu.animated.flipInY li a{
            font-size:12px;
        }
        /*.notification_data .noti_ul li:hover .crossss{
            color:#D11010 !important;
        }*/
        /* width */
    #style-12::-webkit-scrollbar {
        width: 7px;
    
    }
    /* Track */
    #style-12::-webkit-scrollbar-track {
        background: #f1f1f1;
        border-radius: 0 10px 10px 0;
    }

    /* Handle */
    #style-12::-webkit-scrollbar-thumb {
        background: #003780;
        border-radius: 5px;
    }

    .login-user-name{
        color: black;
        padding-top: 7%;
        padding-right: 5%;
        font-weight: 600;
        display: inline-block;
        width: 180px;
        white-space: nowrap;
        overflow: hidden !important;
        text-overflow: ellipsis;
        text-align: right;
    }
</style>
<!--========== END app navbar -->