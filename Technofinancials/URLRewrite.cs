﻿using System;
using System.Web;
using System.Web.Compilation;
using System.Web.Routing;
using System.Web.UI;
public class URLRewrite : IRouteHandler
{
    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        try
        {
            string general = requestContext.RouteData.Values["general"] as string;
            string companyName = requestContext.RouteData.Values["companyName"] as string;
            string deptName = requestContext.RouteData.Values["deptName"] as string;
            string view_type = requestContext.RouteData.Values["view_type"] as string;
            string access_type = requestContext.RouteData.Values["access_type"] as string;
            string manage_type = requestContext.RouteData.Values["manage_type"] as string;
            string manage_id = requestContext.RouteData.Values["manage_id"] as string;

            if (!string.IsNullOrEmpty(general))
            {
                switch (general)
                {
                    case "login":
                        return BuildManager.CreateInstanceFromVirtualPath("~/Default.aspx", typeof(Page)) as Page;
                    case "update-security-questions":
                        return BuildManager.CreateInstanceFromVirtualPath("~/SecurityQuestions.aspx", typeof(Page)) as Page;
                    case "update-password":
                        return BuildManager.CreateInstanceFromVirtualPath("~/UpdatePassword.aspx", typeof(Page)) as Page;
                    case "forgot-password":
                        return BuildManager.CreateInstanceFromVirtualPath("~/SecurityEmail.aspx", typeof(Page)) as Page;
                    case "reset-options":
                        return BuildManager.CreateInstanceFromVirtualPath("~/ResetPassword.aspx", typeof(Page)) as Page;
                    case "reset-password":
                        return BuildManager.CreateInstanceFromVirtualPath("~/UpdatePassword.aspx", typeof(Page)) as Page;
                    case "reset-password-via-security-questions":
                        return BuildManager.CreateInstanceFromVirtualPath("~/SecurityQuestions.aspx", typeof(Page)) as Page;
                    case "change-password":
                        return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/UpdatePassword.aspx", typeof(Page)) as Page;
                    case "something-went-wrong":
                        return BuildManager.CreateInstanceFromVirtualPath("~/ErrorPage.aspx", typeof(Page)) as Page;
                    case "terms-of-use":
                        return BuildManager.CreateInstanceFromVirtualPath("~/terms-of-use.aspx", typeof(Page)) as Page;
                    case "privacy-policy":
                        return BuildManager.CreateInstanceFromVirtualPath("~/PrivacyPolicy.aspx", typeof(Page)) as Page;
                    case "security":
                        return BuildManager.CreateInstanceFromVirtualPath("~/Security.aspx", typeof(Page)) as Page;
                    case "features":
                        return BuildManager.CreateInstanceFromVirtualPath("~/Features.aspx", typeof(Page)) as Page;
                    case "news":
                        return BuildManager.CreateInstanceFromVirtualPath("~/News.aspx", typeof(Page)) as Page;
                    case "events":
                        return BuildManager.CreateInstanceFromVirtualPath("~/Events.aspx", typeof(Page)) as Page;
                    case "blogs":
                        return BuildManager.CreateInstanceFromVirtualPath("~/Blogs.aspx", typeof(Page)) as Page;
                    case "webnars":
                        return BuildManager.CreateInstanceFromVirtualPath("~/Webnars.aspx", typeof(Page)) as Page;
                    case "awards":
                        return BuildManager.CreateInstanceFromVirtualPath("~/Awards.aspx", typeof(Page)) as Page;
                    case "legal":
                        return BuildManager.CreateInstanceFromVirtualPath("~/Legal.aspx", typeof(Page)) as Page;
                    case "press-realease":
                        return BuildManager.CreateInstanceFromVirtualPath("~/PressRealease.aspx", typeof(Page)) as Page;
                    //temporaery urls
                    case "temp":
                        return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PerformanceAppraisal.aspx", typeof(Page)) as Page;
                    case "switch-panel":
                        return BuildManager.CreateInstanceFromVirtualPath("~/SwitchPanel.aspx", typeof(Page)) as Page;
                }
            }

            if (!string.IsNullOrEmpty(deptName))
            {
                if (deptName == "operation-management")
                {
                    if (access_type == "view")
                    {
                        if (view_type == "dashboard")
                        {
                            return BuildManager.CreateInstanceFromVirtualPath("~/OperationManagement/TR.aspx", typeof(Page)) as Page;
                        }
                    }
                }

                if (deptName == "super-admin")
                {
                    if (access_type == "view")
                    {
                        if (view_type == "dashboard")
                        {
                            return BuildManager.CreateInstanceFromVirtualPath("~/SuperAdmin/Dashboard.aspx", typeof(Page)) as Page;
                        }
                        if (view_type == "companies")
                        {
                            return BuildManager.CreateInstanceFromVirtualPath("~/SuperAdmin/View/Companies.aspx", typeof(Page)) as Page;
                        }
                    }
                    if (access_type == "manage")
                    {
                        if (manage_type == "users")
                        {
                            if (manage_id == "add-new-user")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/SuperAdmin/Manage/Users.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-user-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["CompanyID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/SuperAdmin/Manage/Users.aspx", typeof(Page)) as Page;
                            }
                        }
                    }
                }

                if (deptName == "directors")
                {
                    if (access_type == "view")
                    {
                        switch (view_type)
                        {
                            case "company-profile":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/CompanyProfile.aspx", typeof(Page)) as Page;

                            case "child-companies":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/ChildCompanies.aspx", typeof(Page)) as Page;

                            case "departments":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/Department.aspx", typeof(Page)) as Page;

                            case "cost-center":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/CostCenter.aspx", typeof(Page)) as Page;

                            case "designations":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/Designation.aspx", typeof(Page)) as Page;

                            case "grades":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/Grades.aspx", typeof(Page)) as Page;

                            case "users":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/Users.aspx", typeof(Page)) as Page;

                            case "job-descriptions":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/JobDescriptions.aspx", typeof(Page)) as Page;

                            case "divisions":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/Divisions.aspx", typeof(Page)) as Page;

                            case "teams":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/Teams.aspx", typeof(Page)) as Page;

                            case "organization-chart":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/OrganizationChart.aspx", typeof(Page)) as Page;

                            case "black-listing-employees":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/BlackListingEmployees.aspx", typeof(Page)) as Page;

                            case "black-listing-vendors":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/BlackListingSuppliers.aspx", typeof(Page)) as Page;

                            case "log":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/Logs.aspx", typeof(Page)) as Page;

                            case "locations":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/Locations.aspx", typeof(Page)) as Page;

                            case "workday-sheet":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/View/WorkDaySheet.aspx", typeof(Page)) as Page;
                        }
                    }
                    if (access_type == "manage")
                    {
                        if (manage_type == "companies")
                        {
                            if (manage_id == "add-new-company")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Companies.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-company-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["CompanyID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Companies.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "locations")
                        {
                            if (manage_id == "add-new-location")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Locations.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-location-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LocationID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Locations.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "workday-sheet")
                        {

                            if (manage_id.Contains("leaves-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["MasterID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/LeavesWorkDayIntegrationDetailsSheet.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id.Contains("details-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["MasterID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/WorkDaySheet.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "departments")
                        {
                            if (manage_id == "add-new-department")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Department.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-department-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["DepartmentID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Department.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "cost-center")
                        {
                            if (manage_id == "add-new-cost-center")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/CostCenter.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-cost-center-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["CostCenterID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/CostCenter.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "designations")
                        {
                            if (manage_id == "add-new-designation")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Designation.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-designation-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["DesgID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Designation.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "grades")
                        {
                            if (manage_id == "add-new-grade")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Grads.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-grade-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["GradeID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Grads.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "users")
                        {
                            if (manage_id == "add-new-user")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Users.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-user-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["UserID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/Users.aspx", typeof(Page)) as Page;
                            }

                        }

                        if (manage_type == "job-descriptions")
                        {
                            if (manage_id == "add-new-job-description")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/JobDescriptions.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-job-description-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["NodeID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Administrator/Manage/JobDescriptions.aspx", typeof(Page)) as Page;
                            }
                        }
                    }
                }

                if (deptName == "finance")
                {
                    if (access_type == "view")
                    {
                        switch (view_type)
                        {
                            case "AddOnsExpense":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/AddOnsExpense.aspx", typeof(Page)) as Page;

                            case "currency":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/Currency.aspx", typeof(Page)) as Page;

                            case "SCB":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Reports/SCB_Report.aspx", typeof(Page)) as Page;

                            case "IBFT":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Reports/IBFT_Report.aspx", typeof(Page)) as Page;

                            case "payroll-Monthly-Tax-Report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Reports/TaxDeductionReport.aspx", typeof(Page)) as Page;

                            case "payroll-Provident-Fund-Detail":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Reports/PFDeductionReport.aspx", typeof(Page)) as Page;

                            case "employee-eobi":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Reports/EmployeeEOBIReport.aspx", typeof(Page)) as Page;

                            case "employee-WHT-report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Reports/EmployeeWHTReport.aspx", typeof(Page)) as Page;
                            
                            case "Employee-SESSI-IESSI-PESSI-report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Reports/EmployeeSESSIIESSIPESSIreport.aspx", typeof(Page)) as Page;

							case "employee-fnf-settlement":

								return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/EmployeeFullAndFinalSettlement.aspx", typeof(Page)) as Page;
                            case "mini-payroll-detail-Report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/MiniPayrollDetailReport.aspx", typeof(Page)) as Page;
                            case "payroll-detail-Report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/PayrollDetailReport.aspx", typeof(Page)) as Page;
                            case "attendance-adjustment":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/AttendanceAdjustment.aspx", typeof(Page)) as Page;

                            case "deduction":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/Deduction.aspx", typeof(Page)) as Page;

                            case "AddOnsExpenseNew":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/AddOnsExpenseNew.aspx", typeof(Page)) as Page;
                            case "UploadLoanSchedule":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/LoanSchedulePaymentUpload.aspx", typeof(Page)) as Page;

                            case "IT3Approval":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/IT3Approval.aspx", typeof(Page)) as Page;
                                
                            case "fnf-pendings":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/FnFPendings.aspx", typeof(Page)) as Page;

                            case "attendence-detail":

                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/Absents.aspx", typeof(Page)) as Page;
                            case "attendance-sheet":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/AttendanceSheet.aspx", typeof(Page)) as Page;
                            case "clockwise-sheet":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/ClockwiseAttendance.aspx", typeof(Page)) as Page;
                            case "Flag-wise-Employees":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/AttendanceDetailedReport.aspx", typeof(Page)) as Page;

                            case "fnf-detail-Report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/FnFView.aspx", typeof(Page)) as Page;
                            case "attendance-uploader":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/AttendanceUploader.aspx", typeof(Page)) as Page;
                            case "standard-payroll":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/PayrollUsingUploadedAttendance.aspx", typeof(Page)) as Page;

                            case "employee-clearance":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/EmployeeClearance.aspx", typeof(Page)) as Page;
                            case "holidays":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/Holidays.aspx", typeof(Page)) as Page;

                            case "Overtime-Approval":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/OvertimeRequests.aspx", typeof(Page)) as Page;

                            case "JEReport":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Finance/reports/JE_Report.aspx", typeof(Page)) as Page;

                            case "dashboard":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Dashboard.aspx", typeof(Page)) as Page;

                            case "teams":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Finance/view/Teams.aspx", typeof(Page)) as Page;
                            case "audit":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/AuthorizationLogs.aspx", typeof(Page)) as Page;

                            case "tracking":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/Tracking.aspx", typeof(Page)) as Page;

                            case "journal-voucher":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/JurnalVouchers.aspx", typeof(Page)) as Page;

                            case "payrolls2":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/Payroll3.aspx", typeof(Page)) as Page;

                            case "payroll":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/Payroll2.aspx", typeof(Page)) as Page;

                            case "general-ledger":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/JournalLedger.aspx", typeof(Page)) as Page;

                            case "purchase-order-jv":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/POJV.aspx", typeof(Page)) as Page;

                            case "invoice-jv":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/InvoiceJV.aspx", typeof(Page)) as Page;
                            case "mini-payroll":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/MiniPayroll.aspx", typeof(Page)) as Page;
                            case "AddOns":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/AddOns.aspx", typeof(Page)) as Page;
                            case "deduction-head":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/DeductionHead.aspx", typeof(Page)) as Page;
                            case "overtime-group":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/OvertimeGroup.aspx", typeof(Page)) as Page;
                            case "clearance-items":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Finance/View/ClearanceItems.aspx", typeof(Page)) as Page;

                            case "minipayrolllist":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/MiniPayrollList.aspx", typeof(Page)) as Page;

                            case "mini-payroll-New":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/MiniPayrollNew.aspx", typeof(Page)) as Page;

                            case "expense-jv":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/ExpenseJV.aspx", typeof(Page)) as Page;

                            case "bank-vouchers":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/BankVouchers.aspx", typeof(Page)) as Page;

                            case "cash-vouchers":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/CashVouchers.aspx", typeof(Page)) as Page;

                            case "revenue-dashboard":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/RevenueDashboard.aspx", typeof(Page)) as Page;

                            case "invoice":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/Invoice.aspx", typeof(Page)) as Page;

                            case "qoutation":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/Quotation.aspx", typeof(Page)) as Page;

                            case "credit-note":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/CreditNote.aspx", typeof(Page)) as Page;

                            case "statement-sharing":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/StatementSharing.aspx", typeof(Page)) as Page;

                            case "product-and-services":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/ProducAndServices.aspx", typeof(Page)) as Page;

                            case "cdms":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/CDMS.aspx", typeof(Page)) as Page;

                            case "COAQuickSetup":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/COAQuickSetup.aspx", typeof(Page)) as Page;

                            case "procurement-dashboard":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/ProcurementDashboard.aspx", typeof(Page)) as Page;

                            case "purchase-order":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/PurchaseOrder.aspx", typeof(Page)) as Page;

                            case "goods-receive-notes":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/GRN.aspx", typeof(Page)) as Page;

                            case "debit-note":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/DebitNote.aspx", typeof(Page)) as Page;

                            case "expenses-dashboard":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/ExpensesDashboard.aspx", typeof(Page)) as Page;

                            case "expenses":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/Expenses.aspx", typeof(Page)) as Page;

                            case "cashflow-dashboard":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/CashflowDashboard.aspx", typeof(Page)) as Page;

                            case "accounts":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/Accounts.aspx", typeof(Page)) as Page;

                            case "direct-payment":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/DirectPayment.aspx", typeof(Page)) as Page;

                            case "coa-transfer-money":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/COATransferMoney.aspx", typeof(Page)) as Page;

                            case "transfer-money":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/TransferMoney.aspx", typeof(Page)) as Page;

                            case "bank-reconciliation":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/BankReconciliation.aspx", typeof(Page)) as Page;

                            case "pettry-cash-maanagement":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/PettryCashMaanagement.aspx", typeof(Page)) as Page;

                            case "opening-balance":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/OpeningBalance.aspx", typeof(Page)) as Page;

                            case "multi-currency":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/MultCurrency.aspx", typeof(Page)) as Page;

                            case "tax":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/Tax.aspx", typeof(Page)) as Page;
                            case "loansNadvance":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Finance/View/LoanAndAdvance.aspx", typeof(Page)) as Page;

                            case "coa":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/COAview.aspx", typeof(Page)) as Page;

                            case "employees":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/EmployeesList.aspx", typeof(Page)) as Page;

                            case "shift-lists":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/ShiftLists.aspx", typeof(Page)) as Page;

                            case "payrolls":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/PayRolls.aspx", typeof(Page)) as Page;

                            case "chart-of-account-step-01":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/coa/Company.aspx", typeof(Page)) as Page;

                            case "chart-of-account-step-02":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/coa/Location.aspx", typeof(Page)) as Page;

                            case "chart-of-account-step-03":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/coa/Category.aspx", typeof(Page)) as Page;

                            case "chart-of-account-step-04":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/coa/SubCategory.aspx", typeof(Page)) as Page;

                            case "chart-of-account-step-05":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/coa/AccountType.aspx", typeof(Page)) as Page;

                            case "chart-of-account-step-06":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/coa/Account.aspx", typeof(Page)) as Page;

                            case "generate-pay-slip":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Finance/Reports/GeneratePayroll.aspx", typeof(Page)) as Page;

                            case "generate-minipay-slip":
                                return BuildManager.CreateInstanceFromVirtualPath("~/Finance/Reports/MiniPayrollList.aspx", typeof(Page)) as Page;

                            case "Other-expense":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/OtherExpenses.aspx", typeof(Page)) as Page;

                            case "yearly-Tax-Report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Reports/YearlyTaxReport.aspx", typeof(Page)) as Page;
                            case "assets-deductions":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Reports/AssetsAndDeductions.aspx", typeof(Page)) as Page;
                            case "fnf-detail":
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/FnFDetails.aspx", typeof(Page)) as Page;
                        }

                        if (view_type.Contains("auto-attendance"))
                        {

                            return BuildManager.CreateInstanceFromVirtualPath("~/finance/view/AutoAttendance.aspx", typeof(Page)) as Page;

                        }
                        if (view_type.Contains("payment-schedule-"))
                        {
                            string val = view_type;
                            string[] sVal = val.Split('-');// UNDERSCORE
                            HttpContext.Current.Items["LoanID"] = sVal[sVal.Length - 1];
                            return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/PaymentSchedule.aspx", typeof(Page)) as Page;
                        }
                        if (view_type.Contains("fnf-view"))
                        {
                            string val = view_type;
                            string[] sVal = val.Split('_');// UNDERSCORE
                            HttpContext.Current.Items["PayrollMonth"] = sVal[sVal.Length - 1];
                            return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/FnFRelease.aspx", typeof(Page)) as Page;
                        }

                        if (view_type.Contains("fnf-Report"))
                        {
                            string val = view_type;
                            string[] sVal = val.Split('-');
                            HttpContext.Current.Items["EmpID"] = sVal[sVal.Length - 1];
                            return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/FnFReport.aspx", typeof(Page)) as Page;
                        }
                        
                         if (view_type.Contains("fnf-month-"))
                        {
                            string val = view_type;
                            string[] sVal = val.Split('-');
                            HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                            return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/FnFMonthWise.aspx", typeof(Page)) as Page;
                        }

                        if (view_type.Contains("employee_fnf_settlement_"))
                                {
                            string val = view_type;
                            string[] sVal = val.Split('_');
                            HttpContext.Current.Items["PayrollMonth"] = sVal[sVal.Length - 1];
                            return BuildManager.CreateInstanceFromVirtualPath("~/finance/View/EmployeeFullAndFinalSettlement.aspx", typeof(Page)) as Page;
                        }
                    }

                    if (access_type == "manage")
                    {

                        if (manage_type == "holidays")
                        {
                            if (manage_id == "add-new-holiday")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/Holidays.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-holiday-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["HolidayID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/Holidays.aspx", typeof(Page)) as Page;
                            }
                        }


                        if (manage_type == "overtime-group")
                        {
                            if (manage_id == "add-new-overtime-group")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/OvertimeGroup.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-overtime-group-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["GroupID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/OvertimeGroup.aspx", typeof(Page)) as Page;

                            }

                        }


                        if (manage_type == "standard-payroll")
                        {
                            if (manage_id == "add-new-standard-payroll")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayrollUsingUploadedAttendance.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-standard-payroll-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayrollUsingUploadedAttendance.aspx", typeof(Page)) as Page;

                            }

                        }

                        if (manage_type == "attendance-uploader")
                        {
                            if (manage_id == "add-new-attendance-file")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/UploadAttendance.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-attendance-file-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["SheetID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/UploadAttendance.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "loansNadvance")
                        {
                           
                            if (manage_id.Contains("edit-loansNadvance-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LoanID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Finance/Manage/LoanAndAdvance.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "IT3Approval")
                        {

                           


                            if (manage_id.Contains("edit-IT3Approval-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["IT3_ID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/Finance/Manage/IT3Approval.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "UploadLoanSchedule")
                        {
                            if (manage_id == "add-new-UploadLoanSchedule")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/UploadLoanSchedule.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-UploadLoanSchedule-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["SheetID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/UploadLoanSchedule.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "clearance-items")
                        {
                            if (manage_id == "add-new-item")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/ClearanceItems.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-item-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["ClearanceItemID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/ClearanceItems.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "deduction-head")
                        {
                            if (manage_id == "add-new-deduction-head")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/DeductionHead.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-deduction-head-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AddOnsID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/DeductionHead.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "AddOns")
                        {
                            if (manage_id == "add-new-AddOns")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/AddOns.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-AddOns-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AddOnsID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/AddOns.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "AddOnsExpense")
                        {
                            if (manage_id == "add-new-AddOnsExpense")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/AddOnsExpenseNew.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-AddOnsExpense-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AddOnsExpenseID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/AddOnsExpenseNew.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "currency")
                        {
                            if (manage_id == "add-new-currency")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/currency.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-currency-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["CurrencyID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/currency.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "PayrollDate")
                        {
                            if (manage_id == "add-new-PayrollDate")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayrollDate.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-PayrollDate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollDateID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayrollDate.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "provinent-funds")
                        {

                            if (manage_id == "manage-provinent-funds")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/ProvinentFund.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "Other-expense")
                        {
                            if (manage_id == "add-new-Other-expense")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/OtherExpenses.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-Other-expense-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["OtherExpenseID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/OtherExpenses.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "deduction")
                        {
                            if (manage_id == "add-new-deduction")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/Deduction.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-deduction-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AddOnsExpenseID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/Deduction.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "Payroll")
                        {
                            if (manage_id == "add-new-payroll")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayRollAEPNew.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id.Contains("edit-payroll-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayRollAEPNew.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id == "add-new-spotlit-payroll")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayRoll2.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id.Contains("edit-spotlit-payroll-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayRoll2.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("edit-mini-payroll-New-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/MiniPayrollNew.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "add-new-mini-payroll")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/MiniPayrollNew.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("edit-payrollz-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/ParollAEAP2.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "Overtime-Approval")
                        {
                            if (manage_id.Contains("edit-OverTime-Details-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AttendanceID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/ApproveOvertimeRequests.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "attendance-adjustment")
                        {
                            if (manage_id == "add-new-attendance-adjustment")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/AttendanceAdjustment.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-attendance-adjustment-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AttendanceAdjustmentID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/AttendanceAdjustment.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "employee-clearance")
                        {
                            if (manage_id.Contains("edit-seperation-item-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["SeperationId"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/EmployeeClearance.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "reports")
                        {
                           
                            if (manage_id.Contains("generate-purchase-orders-report-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["POID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/printables/POsReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-grn-report-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["GRNID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/printables/GRNReport.aspx", typeof(Page)) as Page;
                            }


                            if (manage_id.Contains("generate-debit-notes-report-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["DBNID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/printables/DBNsReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "COAQuickSetup")
                        {
                            if (manage_id == "add-COAQuickSetup")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/COAQuickSetup.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-COAQuickSetup-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["COAQuickSetupID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/COAQuickSetup.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "payroll")
                        {
                            if (manage_id == "add-payroll")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/Payroll.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-payroll-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/PayrollAEPNew.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "accounts")
                        {
                            if (manage_id == "add-account")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/Account.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-account-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AccountID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/Account.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "direct-payment")
                        {
                            if (manage_id == "add-direct-payment")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/DirectPayment.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-direct-payment-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["DirectPaymentID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/DirectPayment.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "transfer-money")
                        {
                            if (manage_id == "add-transfer-money")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/TransferMoney.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-transfer-money-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["BalanceTranferID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/TransferMoney.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "coa-transfer-money")
                        {
                            if (manage_id == "add-coa-transfer-money")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/COATransferMoney.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-coa-transfer-money-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["COABalanceTransferID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/COATransferMoney.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "bank-reconciliation")
                        {
                            if (manage_id == "add-bank-reconciliation")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/BankReconciliation.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-bank-reconciliation-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["BRID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/BankReconciliation.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "pettry-cash")
                        {
                            if (manage_id == "add-pettry-cash")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/PettryCash.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-pettry-cash-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PCID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/PettryCash.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "coa")
                        {
                            if (manage_id == "add-coa")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/COAKT.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-coa-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["COAID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/COAKT.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "opening-balance")
                        {
                            if (manage_id == "add-opening-balance")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/OpeningBalance.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-opening-balance-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["OBID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/OpeningBalance.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "multi-currency")
                        {
                            if (manage_id == "add-multi-currency")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/MultiCurrency.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-multi-currency-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["MCID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/MultiCurrency.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "tax")
                        {
                            if (manage_id == "add-tax")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/Tax.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-tax-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["TaxRateID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/manage/Tax.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "employees")
                        {
                            if (manage_id == "add-new-employee")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/Employee.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-employee-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["EmployeeID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/Employee.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "shifts")
                        {
                            if (manage_id == "add-new-shift")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/Shifts.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-shift-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["ShiftID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/Shifts.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "payrolls")
                        {
                            if (manage_id == "add-new-payroll")
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayRollAEPL.aspx", typeof(Page)) as Page;

                            if (manage_id.Contains("edit-payroll-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayRollAEPL.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id == "add-new-spotlit-payroll")
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayRolls.aspx", typeof(Page)) as Page;

                            if (manage_id.Contains("edit-spotlit-payroll-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/PayRolls.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "coa-setup")
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/Manage/COASetup.aspx", typeof(Page)) as Page;
                        }
                    }

                    if (access_type == "reports")
                    {
                        if (manage_type == "balance-sheet")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "Balance Sheet";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "Balance Sheet";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/BalanceSheet.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == ("generate"))
                            {
                                HttpContext.Current.Items["PageType"] = "Balance Sheet";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "coa-detail")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "COA Detail";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "COA Detail";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/COA Detail.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == ("generate"))
                            {
                                HttpContext.Current.Items["PageType"] = "COA Detail";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "finance-report")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "finance Report";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "generate")
                            {
                                HttpContext.Current.Items["PageType"] = "finance Report";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "expense-voucher")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "Expense Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PrimaryValue"] = sVal[sVal.Length - 1];
                                HttpContext.Current.Items["PageType"] = "Expense Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "journal-voucher")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/COAReports.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PrimaryValue"] = sVal[sVal.Length - 1];
                                HttpContext.Current.Items["PageType"] = "Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "General-Ledger")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "General Ledger";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "grn-journal-voucher")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "GRN Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "GRN Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/COAReports.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PrimaryValue"] = sVal[sVal.Length - 1];
                                HttpContext.Current.Items["PageType"] = "GRN Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "expense-journal-voucher")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "Expense Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "Expense Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/COAReports.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PrimaryValue"] = sVal[sVal.Length - 1];
                                HttpContext.Current.Items["PageType"] = "Expense Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "depreciation-journal-voucher")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "Depreciation Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "Depreciation Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/COAReports.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PrimaryValue"] = sVal[sVal.Length - 1];
                                HttpContext.Current.Items["PageType"] = "Depreciation Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "invoice-journal-voucher")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "Invoive Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "Invoive Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/COAReports.aspx", typeof(Page)) as Page;
                            }
                            
                            if (manage_id.Contains("generate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PrimaryValue"] = sVal[sVal.Length - 1];
                                HttpContext.Current.Items["PageType"] = "Invoive Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "payroll-journal-voucher")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "Payroll Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "Payroll Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/COAReports.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PrimaryValue"] = sVal[sVal.Length - 1];
                                HttpContext.Current.Items["PageType"] = "Payroll Journal Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "bank-voucher")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "Bank Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "Bank Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/COAReports.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PrimaryValue"] = sVal[sVal.Length - 1];
                                HttpContext.Current.Items["PageType"] = "Bank Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "cash-voucher")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "Cash Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PrimaryValue"] = sVal[sVal.Length - 1];
                                HttpContext.Current.Items["PageType"] = "Cash Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "payment-voucher")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "Payment Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PrimaryValue"] = sVal[sVal.Length - 1];
                                HttpContext.Current.Items["PageType"] = "Payment Voucher";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "coa-debit-credit")
                        {
                            if (manage_id == "design")
                            {
                                HttpContext.Current.Items["PageType"] = "COA Debit/Credit";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/DesignReport.aspx", typeof(Page)) as Page;

                            }
                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "COA Debit/Credit";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/COAReports.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id == ("generate"))
                            {
                                HttpContext.Current.Items["PageType"] = "Trial Balance";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/GenerateReport.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "coa-debit-credit-aepl")
                        {
                            if (manage_id == "view")
                            {
                                HttpContext.Current.Items["PageType"] = "COA Debit/Credit";
                                return BuildManager.CreateInstanceFromVirtualPath("~/finance/reports/COAReportForAEPL.aspx", typeof(Page)) as Page;
                            }
                        }
                    }
                }

                if (deptName == "IT")
                {
                    if (access_type == "view")
                    {
                        switch (view_type)
                        {
                            case "dashboard":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Dashboard.aspx", typeof(Page)) as Page;

                            case "employee-clearance":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/View/EmployeeClearance.aspx", typeof(Page)) as Page;

                            case "ChangeWorkingShift":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/View/ChangeWorkingShift.aspx", typeof(Page)) as Page;

                            case "shifts":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/View/ShiftLists.aspx", typeof(Page)) as Page;

                            case "attendance-sheet":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Reports/AttendanceSheet.aspx", typeof(Page)) as Page;

                            case "Employee-Roaster":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Reports/EmployeeRoaster.aspx", typeof(Page)) as Page;

                            case "Employee-Absent":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Reports/EmployeeAbsentReport.aspx", typeof(Page)) as Page;

                            case "Employee-Leave":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Reports/EmployeeLeaveReport.aspx", typeof(Page)) as Page;

                            case "New-Joiners":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Reports/NewJoiners.aspx", typeof(Page)) as Page;

                            case "Left-Employee":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Reports/LeftEmployee.aspx", typeof(Page)) as Page;

                            case "attendance-summmary":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Reports/AttendanceSummary.aspx", typeof(Page)) as Page;

                            case "attendance-sheet-flagwise":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Reports/AttendanceDetailedReport.aspx", typeof(Page)) as Page;

                            case "Employee-Adjustment-Report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Reports/EmployeeAdjustmentReport.aspx", typeof(Page)) as Page;
                            case "auto-attendance":
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/View/AutoAttendance.aspx", typeof(Page)) as Page;



                        }

                        if (view_type.Contains("attendance-sheet-flagwise-"))
                        {
                            string val = manage_id;
                            string[] sVal = view_type.Split('-');
                            HttpContext.Current.Items["AttendanceFilter"] = sVal[sVal.Length - 1];
                            return BuildManager.CreateInstanceFromVirtualPath("~/IT/Reports/AttendanceDetailedReport.aspx", typeof(Page)) as Page;
                        }
                    }
                    
                    if (access_type == "manage")
                    {
                        if (manage_type == "ChangeWorkingShift")
                        {
                            if (manage_id == "add-new-ChangeWorkingShift")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Manage/ChangeWorkingShift.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-ChangeWorkingShift-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["ChangeWorkingShiftID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Manage/ChangeWorkingShift.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("change-shift-by-employee-id-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["EmployeeID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Manage/ChangeWorkingShift.aspx", typeof(Page)) as Page;
                            }

                        }
                        
                        if (manage_type == "employee-clearance")
                        {
                            if (manage_id.Contains("edit-seperation-item-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["SeperationId"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Manage/EmployeeClearance.aspx", typeof(Page)) as Page;
                            }
                        }
                        
                        if (manage_type == "shifts")
                        {
                            if (manage_id == "add-new-shift")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Manage/Shifts.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-shift-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["ShiftID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/IT/Manage/Shifts.aspx", typeof(Page)) as Page;
                            }
                        }
                    }
                }

                if (deptName == "employee-self-service")
                {
                    if (access_type == "view")
                    {
                        switch (view_type)
                        {
                            case "payroll-detailed-report-ess":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/reports/PayrollDetailedReportESS.aspx", typeof(Page)) as Page;

                            case "Approve-attendance-adjustment":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/AttendanceAdjustment.aspx", typeof(Page)) as Page;

                            case "OverTime-Details":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/EmployeeOverTimeRequests.aspx", typeof(Page)) as Page;

                            case "attendance-adjustment":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/AttendanceAdjustment.aspx", typeof(Page)) as Page;

                            case "break-adjustment":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/BreakAdjustment.aspx", typeof(Page)) as Page;

                            case "AddOnsExpense":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/AddOnsExpense.aspx", typeof(Page)) as Page;

                            case "ChangeWorkingShift":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/ChangeWorkingShift.aspx", typeof(Page)) as Page;

                            case "team-members":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/EmployeeList.aspx", typeof(Page)) as Page;

                            case "balance-leaves":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/BalanceLeaves.aspx", typeof(Page)) as Page;
                           case "audit":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/AuthorizationLogs.aspx", typeof(Page)) as Page;

                            case "tracking":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/Tracking.aspx", typeof(Page)) as Page;

                            case "employee-tax-calculator":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/TaxCalculator.aspx", typeof(Page)) as Page;

                            case "attendance-sheet":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Reports/AttendanceSheet.aspx", typeof(Page)) as Page;

                            case "attendance-sheet-flagwise":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Reports/AttendanceDetailedReport.aspx", typeof(Page)) as Page;

                            case "other-expense":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/TravelExpense.aspx", typeof(Page)) as Page;
                            case "AddonsUploader":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/AddOnsBulkUpload.aspx", typeof(Page)) as Page;
                            case "new-vacancies":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/NewVacancies.aspx", typeof(Page)) as Page;

                            case "purchase-requisitions":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/PR.aspx", typeof(Page)) as Page;

                            case "leads":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Leads.aspx", typeof(Page)) as Page;

                            case "employee-leaves":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Leaves.aspx", typeof(Page)) as Page;

                            case "dashboard":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Dashboard3.aspx", typeof(Page)) as Page;
                            case "dashboardUS":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/DashboardUS.aspx", typeof(Page)) as Page;

                            case "dashboard3":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Dashboard.aspx", typeof(Page)) as Page;

                            case "basic-information":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/BasicInfo.aspx", typeof(Page)) as Page;

                            case "qualification-experience":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/QualificationAndExperience.aspx", typeof(Page)) as Page;

                            case "bank-details":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/BankDetail.aspx", typeof(Page)) as Page;

                            case "attendence-detail":
                             
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Absents.aspx", typeof(Page)) as Page;
                          
                            case "assets":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Asset.aspx", typeof(Page)) as Page;

                            case "payroll":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Payroll.aspx", typeof(Page)) as Page;

                            case "attendence":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Attendence.aspx", typeof(Page)) as Page;

                            case "seperation":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Seperation.aspx", typeof(Page)) as Page;

                            case "details":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Details.aspx", typeof(Page)) as Page;

                            case "tests":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/TestCalendar.aspx", typeof(Page)) as Page;

                            case "tests-list":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/TestList.aspx", typeof(Page)) as Page;

                            case "surveys":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/SurveyCalendar.aspx", typeof(Page)) as Page;

                            case "holidays":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Holidays.aspx", typeof(Page)) as Page;

                            case "surveys-list":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/SurveyList.aspx", typeof(Page)) as Page;

                            case "trainings":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/TrainingCalendar.aspx", typeof(Page)) as Page;

                            case "trainings-list":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/TrainingList.aspx", typeof(Page)) as Page;

                            case "provinent-fund":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/ProvinentFund.aspx", typeof(Page)) as Page;

                            case "attachments":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Attachments.aspx", typeof(Page)) as Page;

                            case "IT3":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/IT3.aspx", typeof(Page)) as Page;

                            case "loans":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Loans.aspx", typeof(Page)) as Page;
                            case "loansNadvance":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/LoanAndAdvance.aspx", typeof(Page)) as Page;
                            case "loansNadvanceapproval":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/LoanAndAdvanceTrusteeView.aspx", typeof(Page)) as Page;
                                
                            case "PFWithdrawalFormat":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/PFWithdrawalFormat.aspx", typeof(Page)) as Page;
                            case "shifts":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Shits.aspx", typeof(Page)) as Page;

                            case "performance-appraisal":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/PerformanceAppraisal.aspx", typeof(Page)) as Page;

                            case "announcements":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Announcement.aspx", typeof(Page)) as Page;

                            case "generate-pay-slip":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Reports/GeneratePaySlip.aspx", typeof(Page)) as Page;

                            case "generate-minipay-slip":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Reports/EmployeeMiniPayslip.aspx", typeof(Page)) as Page;

                            case "adjustment":
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Adjustments.aspx", typeof(Page)) as Page;
                        }
                        if (view_type.Contains("leaves-details-"))
                        {
                            string val = view_type;
                            string[] sVal = val.Split('-');
                            HttpContext.Current.Items["SelectedEmp"] = sVal[sVal.Length - 1];
                            HttpContext.Current.Items["SelectedEmpName"] = sVal[sVal.Length - 2];

                            return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/LeavesDetails.aspx", typeof(Page)) as Page;

                         
                        }
                        //if (view_type.Contains("attendence-detail-"))
                        //{
                        //    string val = view_type;
                        //    string[] sVal = val.Split('-');
                        //    HttpContext.Current.Items["SelectedEmp"] = sVal[sVal.Length - 1];
                        //    return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Absents.aspx", typeof(Page)) as Page;
                        //}

                        if (view_type.Contains("attendence-detail-"))
                        {
                            string val = view_type;
                            string[] sVal = val.Split('-');
                            HttpContext.Current.Items["AttendanceFilter"] = sVal[sVal.Length - 1];
                            return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Absents.aspx", typeof(Page)) as Page;
                        }
                        if (view_type.Contains("payment-schedule-"))
                        {
                            string val = view_type;
                            string[] sVal = val.Split('-');// UNDERSCORE
                            HttpContext.Current.Items["LoanID"] = sVal[sVal.Length - 1];
                            return BuildManager.CreateInstanceFromVirtualPath("~/ESS/View/PaymentSchedule.aspx", typeof(Page)) as Page;
                        }
                    }
                    if (access_type == "manage")
                    {


                        if (manage_type == "AddonsUploader")
                        {
                            if (manage_id == "add-new-AddonsUploader")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/UploadFileAddon.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-AddonsUploader-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["SheetID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/UploadFileAddon.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "AddOnsExpense")
                        {
                            if (manage_id == "add-new-AddOnsExpense")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/AddOnsExpense.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-AddOnsExpense-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AddOnsExpenseID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/AddOnsExpense.aspx", typeof(Page)) as Page;
                            }
                        }
                        
                        if (manage_type == "ChangeWorkingShift")
                        {
                            if (manage_id == "add-new-ChangeWorkingShift")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/ChangeWorkingShift.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("change-shift-by-employee-id-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["EmployeeID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/ChangeWorkingShift.aspx", typeof(Page)) as Page;
                            }


                            if (manage_id.Contains("edit-ChangeWorkingShift-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["ChangeWorkingShiftID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/ChangeWorkingShift.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "ChangeWorkingShiftNew")
                        {
                            if (manage_id == "add-new-ChangeWorkingShiftNew")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/ChangeWorkingShift.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-ChangeWorkingShiftNew-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["ChangeWorkingShiftID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/ChangeWorkingShiftNew.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "Other-expense")
                        {
                            if (manage_id == "add-new-Other-expense")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/TravelExpense2.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-Other-expense-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["OtherExpenseID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/TravelExpense2.aspx", typeof(Page)) as Page;
                            }
                        }
                        
                        if (manage_type == "attendance-adjustment")
                        {
                            if (manage_id == "add-new-attendance-adjustment")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/AttendanceAdjustment.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-attendance-adjustment-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AttendanceAdjustmentID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/AttendanceAdjustment.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("edit-attendance-adjust-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AttendanceID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/AttendanceAdjustment.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "break-adjustment")
                        {
                            if (manage_id == "add-new-break-adjustment")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/BreakAdjustment.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-break-adjustment-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["BreakAdjustmentID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/BreakAdjustment.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "OverTime-Details")
                        {
                            if (manage_id.Contains("edit-OverTime-Details-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AttendanceID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/EmployeeOverTimeRequests.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "leads")
                        {
                            if (manage_id.Contains("edit-lead-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LeadID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/Leads.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "loans")
                        {
                            if (manage_id == "add-new-loan")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/Loans.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-loan-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LoanID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/Loans.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "loansNadvanceapproval")
                        {
                           
                            if (manage_id.Contains("edit-loansNadvance-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LoanID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/LoanAndAdvanceTrustees.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "loansNadvance")
                        {
                            if (manage_id == "add-new-loan")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/LoanAndAdvanceApplication.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-loansNadvance-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LoanID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/LoanAndAdvance.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "leaves")
                        {
                            if (manage_id == "add-new-leave")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/Leaves.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-leave-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LeaveID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/ESS/Manage/Leaves.aspx", typeof(Page)) as Page;
                            }
                        }
                    }
                    if (access_type == "reports")
                    {
                        if (view_type == "generate-pay-slip")
                        {
                            HttpContext.Current.Items["PageType"] = "Pay Slip";
                            return BuildManager.CreateInstanceFromVirtualPath("~/ESS/reports/GenerateReport.aspx", typeof(Page)) as Page;
                        }
                    }
                }

                if (deptName == "people-management")
                {
                    if (access_type == "view")
                    {
                        switch (view_type)
                        {
                            case "dashboard":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Dashboard.aspx", typeof(Page)) as Page;

                            case "dashboard2":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Dashboard2.aspx", typeof(Page)) as Page;

                            case "generate-pay-slip":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/GeneratePayroll.aspx", typeof(Page)) as Page;

                            case "generate-minipay-slip":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/EmployeeMiniPayslip.aspx", typeof(Page)) as Page;

                            case "IT3Approval":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/IT3Approval.aspx", typeof(Page)) as Page;

                            case "SCB":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/SCB_Report.aspx", typeof(Page)) as Page;

                            case "IBFT":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/IBFT_Report.aspx", typeof(Page)) as Page;

                            case "SeparationApprovals":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/SeparationApp.aspx", typeof(Page)) as Page;

                            case "loan-report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/LoanReport.aspx", typeof(Page)) as Page;
                            case "loansNadvance":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/LoanAndAdvance.aspx", typeof(Page)) as Page;

                            case "leave-consumed-report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/LeaveConsumption.aspx", typeof(Page)) as Page;

                            case "pf-report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/PFReport.aspx", typeof(Page)) as Page;

                            case "new-vacancies":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/view/NewVacancy.aspx", typeof(Page)) as Page;

                            case "employees-current-location":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/view/EmployeesCurrentLocation.aspx", typeof(Page)) as Page;

                            case "employees":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/EmployeesList.aspx", typeof(Page)) as Page;

                            case "teams":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/Teams.aspx", typeof(Page)) as Page;
                            case "audit":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/view/AuthorizationLogs.aspx", typeof(Page)) as Page;
                            case "tracking":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/Tracking.aspx", typeof(Page)) as Page;

                            case "holidays":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/Holidays.aspx", typeof(Page)) as Page;

                            case "Announcement":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/Announcement.aspx", typeof(Page)) as Page;

                            case "currency":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/Currency.aspx", typeof(Page)) as Page;

                            case "ChangeWorkingShift":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/ChangeWorkingShift.aspx", typeof(Page)) as Page;

                            case "AddOnsExpense":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/AddOnsExpense.aspx", typeof(Page)) as Page;

                            case "deduction":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/Deduction.aspx", typeof(Page)) as Page;

                            case "deduction-head":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/DeductionHead.aspx", typeof(Page)) as Page;

                            case "AddOns":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/AddOns.aspx", typeof(Page)) as Page;

                            case "clearance-items":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/ClearanceItems.aspx", typeof(Page)) as Page;

                            case "employee-clearance":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/EmployeeClearance.aspx", typeof(Page)) as Page;

                            case "OvertimeRatio":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/OvertimeRatio.aspx", typeof(Page)) as Page;

                            case "PayrollDate":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/PayrollDate.aspx", typeof(Page)) as Page;

                            case "PolicyProcedures":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/PolicyProcedure.aspx", typeof(Page)) as Page;

                            case "payrolls":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/Payroll2.aspx", typeof(Page)) as Page;

                            case "payroll-Monthly-Provident-Fund-Report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/payroll_Monthly_PF_Report.aspx", typeof(Page)) as Page;

                            case "payroll-Provident-Fund-Detail":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/payroll_PF_Detail.aspx", typeof(Page)) as Page;

                            case "payrolls2":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/Payroll3.aspx", typeof(Page)) as Page;

                            case "payroll-detail-Report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/PayrollDetailReport.aspx", typeof(Page)) as Page;

                            case "pending-request":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/PendingRequestsList.aspx", typeof(Page)) as Page;

                            case "mini-payroll":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/MiniPayroll.aspx", typeof(Page)) as Page;

                            case "loans":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/Loans.aspx", typeof(Page)) as Page;

                            case "employee-bonus":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/EmployeeBonus.aspx", typeof(Page)) as Page;

                            case "attendance-adjustment":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/AttendanceAdjustment.aspx", typeof(Page)) as Page;

                            case "break-adjustment":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/BreakAdjustment.aspx", typeof(Page)) as Page;

                            case "shifts":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/ShiftLists.aspx", typeof(Page)) as Page;

                            case "provident-fund-summary":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/view/ProvinentFund.aspx", typeof(Page)) as Page;

                            case "Other-expense":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/view/OtherExpenses.aspx", typeof(Page)) as Page;

                            case "attendance-sheet":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/AttendanceSheet.aspx", typeof(Page)) as Page;

                            case "New-Joiners":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/NewJoiners.aspx", typeof(Page)) as Page;

                            case "MaleFemale-Ratio-Department-Wise":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/DepartmentWiseMaleFemaleRatio.aspx", typeof(Page)) as Page;

                            case "Turnover-Report-Department-Wise":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/TurnOverReport.aspx", typeof(Page)) as Page;

                            case "Left-Employees":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/LeftEmployee.aspx", typeof(Page)) as Page;

                            case "Employee-Leaves":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/EmployeeLeaveReport.aspx", typeof(Page)) as Page;

                            case "Absent-Employees":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/EmployeeAbsentReport.aspx", typeof(Page)) as Page;

                            case "attendance-summmary":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/AttendanceSummary.aspx", typeof(Page)) as Page;

                            case "Employee-Roaster":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/EmployeeRoaster.aspx", typeof(Page)) as Page;

                            case "Flag-wise-Employees":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/AttendanceDetailedReport.aspx", typeof(Page)) as Page;

                            case "employee-history":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/EmployeeHistory.aspx", typeof(Page)) as Page;

                            case "individual-attendance":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/IndividualAttendance.aspx", typeof(Page)) as Page;

                            case "employee-leaves":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/Leaves.aspx", typeof(Page)) as Page;

                            case "leave-encashment":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/LeaveEncashment.aspx", typeof(Page)) as Page;

                            case "payroll-report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/PayrollReport.aspx", typeof(Page)) as Page;

                            case "attendance-report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/AttendanceReport.aspx", typeof(Page)) as Page;

                            case "daily-attendance-sheet":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/DailyAttendance.aspx", typeof(Page)) as Page;

                            case "late-comers-sheet":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/LateComers.aspx", typeof(Page)) as Page;

                            case "org-chart":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/OrgChart.aspx", typeof(Page)) as Page;

                            case "pf-deductiondetails":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/PFDeductionDetailsReport.aspx", typeof(Page)) as Page;

                            case "employee-deductiondetails":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/EmployeeDeductionReport.aspx", typeof(Page)) as Page;

                            case "employee-eobi":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/EmployeeEOBIReport.aspx", typeof(Page)) as Page;

                            case "employee-WHT-report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/EmployeeWHTReport.aspx", typeof(Page)) as Page;

                            case "Employee-SESSI-IESSI-PESSI-report":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/EmployeeSESSIIESSIPESSIreport.aspx", typeof(Page)) as Page;

                            case "Employee-attendance-lock":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/AttendanceLock.aspx", typeof(Page)) as Page;
                            case "balance-leaves":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/BalanceLeaves.aspx", typeof(Page)) as Page;
                            case "auto-attendance":
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/AutoAttendance.aspx", typeof(Page)) as Page;



                            default:
                                {
                                   
                                    if (view_type.Contains("leaves-details-"))
                                    {
                                        string val = view_type;
                                        string[] sVal = val.Split('-');
                                        HttpContext.Current.Items["SelectedEmp"] = sVal[sVal.Length - 1];
                                        HttpContext.Current.Items["SelectedEmpName"] = sVal[sVal.Length - 2];

                                        return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/LeavesDetails.aspx", typeof(Page)) as Page;


                                    }
                                    if (view_type.Contains("pending_request_"))
                                    {
                                        string val = view_type;
                                        string[] sVal = val.Split('_');
                                        HttpContext.Current.Items["PendingDate"] = sVal[sVal.Length - 1];
                                        return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/PendingRequestsList.aspx", typeof(Page)) as Page;
                                    }
                                    if (view_type.Contains("New-Joiners-"))
                                    {
                                        string val = manage_id;
                                        string[] sVal = view_type.Split('-');
                                        HttpContext.Current.Items["CurrentMonth"] = sVal[sVal.Length - 1];
                                        return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/NewJoiners.aspx", typeof(Page)) as Page;
                                    }
                                    if (view_type.Contains("Turnover-Report-Department-Wise-"))
                                    {
                                        string val = manage_id;
                                        string[] sVal = view_type.Split('_');
                                        HttpContext.Current.Items["CurrentMonth"] = sVal[sVal.Length - 1];
                                        return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/TurnOverReport.aspx", typeof(Page)) as Page;
                                    }
                                    if (view_type.Contains("Left-Employees-"))
                                    {
                                        string val = manage_id;
                                        string[] sVal = view_type.Split('-');
                                        HttpContext.Current.Items["CurrentMonth"] = sVal[sVal.Length - 1];
                                        return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/LeftEmployee.aspx", typeof(Page)) as Page;
                                    }
                                    if (view_type.Contains("attendance-sheet-"))
                                    {
                                        string val = manage_id;
                                        string[] sVal = view_type.Split('-');
                                        HttpContext.Current.Items["CurrentMonth"] = sVal[sVal.Length - 1];
                                        return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/AttendanceSheet.aspx", typeof(Page)) as Page;
                                    }

                                    if (view_type.Contains("Flag-wise-Employees-"))
                                    {
                                        string val = manage_id;
                                        string[] sVal = view_type.Split('-');
                                        HttpContext.Current.Items["AttendanceFilter"] = sVal[sVal.Length - 1];
                                        return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Reports/AttendanceDetailedReport.aspx", typeof(Page)) as Page;
                                    }

                                    return BuildManager.CreateInstanceFromVirtualPath("~/ErrorPages/CustomErrorPage.aspx", typeof(Page)) as Page;
                                }
                        }
                    }
                    if (access_type == "setup")
                    {
                        if (manage_type == "performance-appraisal")
                        {
                            if (manage_id == "step-01")
                            {
                                HttpContext.Current.Items["PageType"] = "Instructions";
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PASetup/Instructions.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id == "step-02")
                            {
                                HttpContext.Current.Items["PageType"] = "Categories";
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PASetup/Category.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id == "step-03")
                            {
                                HttpContext.Current.Items["PageType"] = "Questions";
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PASetup/Questions.aspx", typeof(Page)) as Page;
                            }
                        }
                    }

                    if (access_type == "manage")
                    {
                        if (manage_type == "loansNadvance")
                        {

                            if (manage_id.Contains("edit-loansNadvance-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LoanID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/LoanAndAdvance.aspx", typeof(Page)) as Page;
                            }
                        }
                        if (manage_type == "Other-expense")
                        {
                            if (manage_id == "add-new-Other-expense")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/OtherExpenses.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-Other-expense-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["OtherExpenseID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/OtherExpenses.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "pending-request")
                        {

                            if (manage_id.Contains("view-pending-request-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["ManagerId"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PendingRequestsDetails.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "employee-seperation")
                        {
                            if (manage_id == "add-new-employee-seperation")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Seperation.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-employee-seperation-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["EmployeeFullAndFinalID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Seperation.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "emp-summary")
                        {
                            if (manage_id == "design-emp-summary")
                            {
                                HttpContext.Current.Items["PageType"] = "Employee Summary";
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/OfferLetterDesign.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id.Contains("generate-emp-summary"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["EmployeeID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/EmployeeSummary.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "org-chart")
                        {
                            if (manage_id == "with-photos")
                            {
                                HttpContext.Current.Items["OrgChartID"] = "with-photo";
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/OrgChart.aspx", typeof(Page)) as Page;

                            }
                            if (manage_id.Contains("without-photos"))
                            {
                                HttpContext.Current.Items["OrgChartID"] = "without-photo";
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/View/OrgChart.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "attendance")
                        {

                            if (manage_id == "edit-attendance")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Attendance.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "leaves")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Leaves.aspx", typeof(Page)) as Page;
                            }

                            if (manage_id == "upload-attendance-sheet")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/ZKTAttenanceSheet.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "holidays")
                        {
                            if (manage_id == "add-new-holiday")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Holidays.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-holiday-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["HolidayID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Holidays.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "Announcements")
                        {
                            if (manage_id == "add-new-Announcement")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Announcement.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-Announcement-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AnnouncementID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Announcement.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "PayrollDate")
                        {
                            if (manage_id == "add-new-PayrollDate")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PayrollDate.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-PayrollDate-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollDateID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PayrollDate.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "AddOnsExpense")
                        {
                            if (manage_id == "add-new-AddOnsExpense")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/AddOnsExpense.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-AddOnsExpense-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AddOnsExpenseID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/AddOnsExpense.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "deduction")
                        {
                            if (manage_id == "add-new-deduction")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Deduction.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-deduction-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AddOnsExpenseID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Deduction.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "clearance-items")
                        {
                            if (manage_id == "add-new-item")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/ClearanceItems.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-item-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["ClearanceItemID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/ClearanceItems.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "employee-clearance")
                        {
                            if (manage_id.Contains("edit-seperation-item-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["SeperationId"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/EmployeeClearance.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "deduction-head")
                        {
                            if (manage_id == "add-new-deduction-head")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/DeductionHead.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-deduction-head-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AddOnsID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/DeductionHead.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "currency")
                        {
                            if (manage_id == "add-new-currency")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/currency.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-currency-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["CurrencyID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/currency.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "AddOns")
                        {
                            if (manage_id == "add-new-AddOns")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/AddOns.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-AddOns-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AddOnsID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/AddOns.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "ChangeWorkingShift")
                        {
                            if (manage_id == "add-new-ChangeWorkingShift")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/ChangeWorkingShift.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-ChangeWorkingShift-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["ChangeWorkingShiftID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/ChangeWorkingShift.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("change-shift-by-employee-id-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["EmployeeID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/ChangeWorkingShift.aspx", typeof(Page)) as Page;
                            }

                        }

                        if (manage_type == "OvertimeRatio")
                        {
                            if (manage_id == "add-new-OvertimeRatio")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/OvertimeRatio.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-OvertimeRatio-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["OvertimeRatioID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/OvertimeRatio.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "PolicyProcedures")
                        {
                            if (manage_id == "add-new-PolicyProcedure")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PolicyProcedure.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-PolicyProcedure-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PolicyProcedureID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PolicyProcedure.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "leaves")
                        {
                            if (manage_id == "add-new-leave")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Leaves.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-leave-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LeaveID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Leaves.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "leave-encashment")
                        {
                            if (manage_id == "add-new-leave-encashment")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/LeaveEncashment.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-leave-encashment-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LeaveEncashmentID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/LeaveEncashment.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "loans")
                        {
                            if (manage_id == "add-new-loan")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Loans.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-loan-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["LoanID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Loans.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "attendance-adjustment")
                        {
                            if (manage_id == "add-new-attendance-adjustment")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/AttendanceAdjustment.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-attendance-adjustment-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["AttendanceAdjustmentID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/AttendanceAdjustment.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "break-adjustment")
                        {
                            if (manage_id == "add-new-break-adjustment")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/BreakAdjustment.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-break-adjustment-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["BreakAdjustmentID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/BreakAdjustment.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "shifts")
                        {
                            if (manage_id == "add-new-shift")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Shifts.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-shift-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["ShiftID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Shifts.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "employees")
                        {
                            if (manage_id == "add-new-employee")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Employee.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-employee-"))
                            {

                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["EmployeeID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/Employee.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "mailing-list")
                        {
                            if (manage_id == "add-new-mailing-list")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/MailingList.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("edit-mailing-list-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["MailingListID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/MailingList.aspx", typeof(Page)) as Page;
                            }
                            if (manage_id.Contains("boradcast-emails-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["MailingListID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/MessageBroadcast.aspx", typeof(Page)) as Page;
                            }

                        }

                        if (manage_type == "payrolls")
                        {
                            if (manage_id == "design-payroll")
                            {
                                HttpContext.Current.Items["PageType"] = "Payroll";
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/OfferLetterDesign.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id == "add-new-payroll")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PayRollAEP.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id.Contains("edit-payroll-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PayRollAEP.aspx", typeof(Page)) as Page;
                            }

                            else if (manage_id.Contains("edit-payrollz-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/ParollAEAP2.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id == "add-new-mini-payroll")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/MiniPayroll.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id.Contains("edit-mini-payroll-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/MiniPayroll.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id == "add-new-spotlit-payroll")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PayRoll2.aspx", typeof(Page)) as Page;
                            }
                            else if (manage_id.Contains("edit-spotlit-payroll-"))
                            {
                                string val = manage_id;
                                string[] sVal = val.Split('-');
                                HttpContext.Current.Items["PayrollID"] = sVal[sVal.Length - 1];
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/PayRoll2.aspx", typeof(Page)) as Page;
                            }
                        }

                        if (manage_type == "provinent-funds")
                        {

                            if (manage_id == "manage-provinent-funds")
                            {
                                return BuildManager.CreateInstanceFromVirtualPath("~/PeopleManagement/Manage/ProvinentFund.aspx", typeof(Page)) as Page;
                            }
                        }
                    }
                }
            }

            return BuildManager.CreateInstanceFromVirtualPath("~/ErrorPages/CustomErrorPage.aspx", typeof(Page)) as Page;
        }
        catch (Exception ex)
        {
            //throw;
			return BuildManager.CreateInstanceFromVirtualPath("~/ErrorPages/CustomErrorPage.aspx", typeof(Page)) as Page;
		}
    }
}