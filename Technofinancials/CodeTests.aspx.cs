﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace Technofinancials
{
    public partial class CodeTests : System.Web.UI.Page
    {
        private DataTable dt
        {
            get
            {
                if (ViewState["dt"] != null)
                {
                    return (DataTable)ViewState["dt"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dt"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            dt = new DataTable();
            dt.TableName = "Table";
            dt.Columns.Add("ID");
            dt.Columns.Add("ParentID");
            dt.Columns.Add("Name");
            dt.AcceptChanges();

            dt.Rows.Add(new object[] {
                "1",
                "",
                "Parent"
            });

            dt.AcceptChanges();

            dt.Rows.Add(new object[] {
                "2",
                "1",
                "Child"
            });

            dt.Rows.Add(new object[] {
                "3",
                "",
                "Parent 2"
            });

            dt.AcceptChanges();

            dt.Rows.Add(new object[] {
                "4",
                "3",
                "Child 3"
            });

            dt.AcceptChanges();

            gvDraft.DataSource = dt;
            gvDraft.DataBind();

            gvDraft.UseAccessibleHeader = true;
            gvDraft.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void gvDraft_PreRender(object sender, EventArgs e)
        {
            int i = 1; 
            foreach (GridViewRow row in gvDraft.Rows)
            {
                row.Attributes["class"] = "treegrid-"+ i + " treegrid-parent-"+dt.Rows[row.RowIndex]["ParentID"];
                i++;
            }
        }
    }

}