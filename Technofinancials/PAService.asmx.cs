﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Technofinancials
{
    /// <summary>
    /// Summary description for PAService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class PAService : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
       public string addFeedbackForm(string appraisalDate, string typeOfReview, List<Object> ratings, List<Object> emojis, List<Object> questions, List<Object> Mngratings, List<Object> Mngemojis, List<Object> Mngquestions)
        {
            try
            {
                if (Session["UserID"] == null && Session["EmployeeID"] == null)
                {
                    return "notLogin";
                }
                else
                {
                    int PerformanceAppraisalID;
                    string res = "";

                    DBQueries objDB = new DBQueries();
                    objDB.EmployeeID = int.Parse(Session["APEmployeeID"].ToString());
                    objDB.AppraisersID = int.Parse(Session["APEmployeeID2"].ToString());
                    objDB.AppraisalDate = appraisalDate;
                    objDB.TypeOfReview = typeOfReview;

                    bool isNew = false;

                    if (Session["PerformanceAppraisalID"] != null)
                    {
                        PerformanceAppraisalID = Convert.ToInt32(Session["PerformanceAppraisalID"]);
                        objDB.ModifiedBy = Session["UserName"].ToString();
                        objDB.UpdatePerformanceAppraisal();
                        res = "Performance Appraisal Data Updated";
                    }
                    else
                    {
                        objDB.CreatedBy = Session["UserName"].ToString();
                        PerformanceAppraisalID = int.Parse(objDB.AddPerformanceAppraisal());
                        res = "New Performance Appraisal Added";
                        Session["PerformanceAppraisalID"] = PerformanceAppraisalID;
                        isNew = true;
                    }

                    objDB.PerformanceAppraisalID = PerformanceAppraisalID;
                    objDB.DeleteAppraisalFunctionaltTraits();


                    objDB.CompanyID = int.Parse(Session["CompanyID"].ToString());
                    objDB.PerformanceAppraisalID = PerformanceAppraisalID;
                    objDB.AppraisalScheduleID = 1;
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.EmpRating = 0;
                    objDB.MngRating = 0;

                    foreach (Dictionary<string, object> item in ratings)
                    {
                        string id = item["ID"].ToString();
                        string value = item["Value"].ToString();

                        objDB.Type = "Star";
                        objDB.QID = int.Parse(id);
                        objDB.EmpAnswer = value;
                        objDB.AddAppraisalQAByEmployee();

                    };

                    
                    foreach (Dictionary<string, object> item in emojis)
                    {
                        string id = item["ID"].ToString();
                        string value = item["Value"].ToString();

                        objDB.Type = "Emoji";
                        objDB.QID = int.Parse(id);
                        objDB.EmpAnswer = value;
                        objDB.AddAppraisalQAByEmployee();
                    };

                    foreach (Dictionary<string, object> item in questions)
                    {
                        string id = item["ID"].ToString();
                        string value = item["Value"].ToString();

                        objDB.Type = "Question";
                        objDB.QID = int.Parse(id);
                        objDB.EmpAnswer = value;
                        objDB.AddAppraisalQAByEmployee();
                    };


                    foreach (Dictionary<string, object> item in Mngratings)
                    {
                        string id = item["ID"].ToString();
                        string value = item["Value"].ToString();

                        objDB.Type = "Star";
                        objDB.QID = int.Parse(id);
                        objDB.ManAnswer = value;
                        objDB.AddAppraisalQAByManager();

                    };


                    foreach (Dictionary<string, object> item in Mngemojis)
                    {
                        string id = item["ID"].ToString();
                        string value = item["Value"].ToString();

                        objDB.Type = "Emoji";
                        objDB.QID = int.Parse(id);
                        objDB.ManAnswer = value;
                        objDB.AddAppraisalQAByManager();
                    };

                    foreach (Dictionary<string, object> item in Mngquestions)
                    {
                        string id = item["ID"].ToString();
                        string value = item["Value"].ToString();

                        objDB.Type = "Question";
                        objDB.QID = int.Parse(id);
                        objDB.ManAnswer = value;
                        objDB.AddAppraisalQAByManager();
                    };



                    //objDB.AppFunTitle = "Punctuality";
                    //objDB.EmpRating = int.Parse(txtEmpPunRat.Value);
                    //objDB.MngRating = 0;
                    //objDB.MngComments = "";
                    //objDB.EmpEvidance = "";
                    //objDB.AddAppraisalFunctionaltTraits();


                    //objDB.EventID = 0;
                    //objDB.OrgID = 41;
                    //if(Session["SF_OrganizationID"] != null)
                    //    objDB.OrgID = Convert.ToInt32(Session["SF_OrganizationID"].ToString());
                    //objDB.CusName = Session["CusName"].ToString();
                    //objDB.CusEmail = Session["CusEmail"].ToString();
                    //objDB.CusPhone = Session["CusPhone"].ToString();
                    //objDB.Waiter = "";
                    //if (Session["Waiter"] != null)
                    //    objDB.Waiter = Session["Waiter"].ToString();
                    //objDB.Type = brunchType;
                    //objDB.CreatedBy = "";
                    //if (Session["UserName"] != null)
                    //    objDB.CreatedBy = Session["UserName"].ToString();



                    ////////////////////////////////////////////


                    //int feedbackID = Convert.ToInt32(objDB.AddSmartFeedBack());

                    //foreach (Dictionary<string, object> item in ratings)
                    //{
                    //    string id = item["ID"].ToString();
                    //    string value = item["Value"].ToString();

                    //    objDB.FeedbackID = feedbackID;
                    //    objDB.Type = "Star";
                    //    objDB.TypeID = Convert.ToInt32(id);
                    //    objDB.Answer = value;
                    //    if (Session["Waiter"] != null)
                    //        objDB.CreatedBy = Session["Waiter"].ToString();
                    //    objDB.AddSmartFeedBackDetails();

                    //};

                    //foreach (Dictionary<string, object> item in emojis)
                    //{
                    //    string id = item["ID"].ToString();
                    //    string value = item["Value"].ToString();

                    //    objDB.FeedbackID = feedbackID;
                    //    objDB.Type = "Emoji";
                    //    objDB.TypeID = Convert.ToInt32(id);
                    //    objDB.Answer = value;
                    //    if (Session["Waiter"] != null)
                    //        objDB.CreatedBy = Session["Waiter"].ToString();
                    //    objDB.AddSmartFeedBackDetails();
                    //};

                    //foreach (Dictionary<string, object> item in questions)
                    //{
                    //    string id = item["ID"].ToString();
                    //    string value = item["Value"].ToString();

                    //    objDB.FeedbackID = feedbackID;
                    //    objDB.Type = "Question";
                    //    objDB.TypeID = Convert.ToInt32(id);
                    //    objDB.Answer = value;
                    //    if (Session["Waiter"] != null)
                    //        objDB.CreatedBy = Session["Waiter"].ToString();
                    //    objDB.AddSmartFeedBackDetails();
                    //};

                    return "Successfull!!";
                }

                    
                
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

    }
}
