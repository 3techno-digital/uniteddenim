﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Blogs.aspx.cs" Inherits="Technofinancials.Blogs" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%--<%@ Register Src="~/usercontrols/Footer2.ascx" TagName="Footer" TagPrefix="uc" %>--%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />

</head>
<body class="slider-body">
    <form id="form1" runat="server">
        <section>
            <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="header-bg-div">
                        <div class="header-name-div">
                            <h2 class="header-name-slide">Blogs</h2>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="terms-of-use-main-div">
                        <br>
                        <h3 class="spanHeading">This section in currently under maintenance</h3>

                    </div>
                </div>
            </div>

        </div>

        </section>
                <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-5">
                    <p>© 2018-2020 TechnoFinancials. All rights reserved.</p>
                </div>
                <div class="col-sm-4 d-flex justify-content-center p-0">
                    <ul class="<%--list-group list-group-horizontal fotor_ul--%> fotor_ul list-inline">
                        <li><a href="/privacy-policy">Policy</a></li>
                        <li><a href="/security">Security</a></li>
                        <li><a href="/terms-of-use">Terms of Service</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
<%--                    <div class="copyright pull-right">
                        <a href="http://3techno.com" target="_blank" class="tf-footer-name tf-font-times-new-roman">A Product of
                    <img src="/assets/images/footer_logo.png" class="tf-footer-logo">
                            3techno</a>
                    </div>--%>
                </div>
            </div>
        </div>
    </footer>
        
            <!--<div class="container-fluid" style="padding-left:0px;padding-right:0px;">
                <div class="row" style="margin-left:0px;margin-right:0px;">
                    <div class="col-sm-12" style="padding-left:0px;padding-right:0px;">
                        <div class="header-bg-div">
                            <div class="header-name-div">
                                <h2 class="header-name-slide">News & Media</h2>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row"   style="margin-left:0px;margin-right:0px;">
                    <div class="col-sm-12" >
                        <div class="terms-of-use-main-div">
                            <h1 class="terms-of-use-heading">Blogs</h1>
                          <br />
                          <br />
                            <h4>This section in currently under maintenance</h4>
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                          <br />
                        </div>
                    </div>
                </div>


            </div> -->

<%--        <uc:Footer ID="Footer1" runat="server"></uc:Footer>--%>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    <style>

        ul{
            list-style-type: disc !important;
            padding: 0px 35px !important;
            margin-top:0px !important;
            margin-bottom:10px !important;
        }
          .terms-of-use-main-div p {
    font-size: 14px;
    text-align: justify;
}
        .spanHeading {
            /*font-weight: bold;
            color: #6a6c6f;
            font-size: 21px;*/
            font-weight: bold;
    color: rgb(0, 55, 128);
    font-size: 18px;
    padding: 5px 0px;
        }
        .terms-of-use-main-div{
    margin:auto 60px !important;
    padding-right:20px;
}
        .container-fluid{
            padding-left:0px !important; 
        }
 .main-footer {
    color: #000 !important;
    font-size: 14px;
    padding: 10px 30px;
    box-shadow: inset 0px 0 9px 0px #c3c1c1;
    border-top: 1px solid;
    position: fixed;
    width: 100%;
    bottom: 0;
    background: #fff;
    right: 0;
    transition: all 0.3s;
    z-index: 1;
}
        .main-footer ul {
    list-style-type: none;
    margin-bottom:0px !important;
}
        .main-footer ul li {
    padding: 0 10px;
}
        .main-footer ul li a {
    font-family: Noto-Regular;
    color: inherit;
    text-decoration: none;
    transition: all 0.3s;
    outline: none !important;
}
    </style>
</body>
</html>
