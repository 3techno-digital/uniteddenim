﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdatePassword.aspx.cs" Inherits="Technofinancials.UpdatePassword" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />
        <style>
        span.select2-container.select2-container--default.select2-container--open ul li:before {
            content: "";
        }

        #login-box {
            box-sizing: border-box;
            width: 350px;
            box-shadow: 1px 1px 3px #DDD;
            margin: auto;
            line-height: 20px;
            display: block;
            border: 1px solid #EAEAEA;
            padding: 35px 20px;
            font-family: 'Open Sans', 'Helvetica', sans-serif;
            font-size: 14px;
            border-radius: 5px;
        }

        #form1 {
            width: 100%;
            height: 100vh;
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            -ms-flex-pack: center;
            -ms-flex-align: center;
            padding: 15px;
            position: relative;
            z-index: 1;
            background-color: #fff;
        }

        .img-pad {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }

        .mb-20 {
            margin-bottom: 20px;
        }

        .mb-15 {
            margin-bottom: 15px;
        }

        .mb-10 {
            margin-bottom: 10px;
        }

        input#userName, input#password {
            width: 100%;
        }

        .privacy_statement_inline_block {
            width: 100% !important;
            font-family: 'Open Sans','Helvetica',sans-serif;
            font-size: 12px;
            color: #999999;
        }

        w100, .row {
            width: auto;
        }

        .descP {
            /* margin-top: 20px; */
            text-align: center;
            font-size: 12px;
            max-width: 285px;
            display: block;
            margin: 10px auto 0;
        }

        .box-header {
            border: none;
            padding: 0px;
        }

        .login_img {
            margin: 20px 0;
            text-align: center;
            display: block;
            margin: 20px auto;
            max-width: 90%;
        }
    </style>
</head>
<body class="slider-body">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScrptMangr" runat="server"></asp:ScriptManager>
        <section>
            <div class="container-fluid" style="padding-left: 12px; padding-right: 12px;">
                <div class="row ">
                    <div class="box" id="login-box">
                        <div id="login-box-header" class="box-header">
                            <div class="row wrap-image text-center mb-20">
                                <span class="img-pad"></span>
                                <img class=" img-responsive login_img logo-img" src="/assets/images/tf_logo.png" alt="Alternate Text" align="center" id="login-logo">
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdPnl" runat="server">
                            <ContentTemplate>
                                <div class="col-sm-12">
                                                                      <div class="clearfix">&nbsp;</div>
                                    <div class="signup-inner" style="margin-top:0px !important;">

                                        <div id="divForm" runat="server">
                                            <div class="signup-form">
                                                <div class="input-group">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="abc-test" runat="server" ControlToValidate="txtPassword" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorStreet" runat="server" ErrorMessage="" ValidationGroup="btnValidate" Display="Dynamic" ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%^&*+]).{8,}$" ControlToValidate="txtPassword"></asp:RegularExpressionValidator>
                                                    <input class="form-control" placeholder="New Password" type="password" id="txtPassword" runat="server" />
                                                </div>
                                                <div class="input-group">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator><asp:CompareValidator runat="server" ErrorMessage="Password Not Match" ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword" ValidationGroup="btnValidate" Display="Dynamic" ForeColor="Red" />
                                                    <input class="form-control" placeholder="Confirm Password" type="password" id="txtConfirmPassword" runat="server" />
                                                </div>
                                                <asp:Button ID="btnUpdatePassword" Text="Update" runat="server" OnClick="btnUpdatePassword_ServerClick" CssClass="btn-sucess btn btn-default btn-login"></asp:Button>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

<%--                                    <div class="col-md-12">
                                        <!-- Begin DigiCert site seal HTML and JavaScript -->
                                        <div id="DigiCertClickID_BkDeAnmk" data-language="en">
	
</div>
                                        <script type="text/javascript">
                                            var __dcid = __dcid || []; __dcid.push(["DigiCertClickID_BkDeAnmk", "15", "s", "black", "BkDeAnmk"]); (function () { var cid = document.createElement("script"); cid.async = true; cid.src = "//seal.digicert.com/seals/cascade/seal.min.js"; var s = document.getElementsByTagName("script"); var ls = s[(s.length - 1)]; ls.parentNode.insertBefore(cid, ls.nextSibling); }());
                                        </script>
                                        <!-- End DigiCert site seal HTML and JavaScript -->
                                    </div>--%>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                                  <div class="clearfix">&nbsp;</div>
                        <div id="login-box-footer" class="box-footer">
                            <div class="row mb-15 privacy_statement_static privacy_statement_inline_block descP">
                                By clicking on the Sign In button, you understand and agree to our
                                        <a href="/news" class="link">News</a> ,
                                        <a href="/blogs" class="link">Blogs</a> , <a href="/events" class="link">Events</a> and <a href="/features" class="link">Features</a>
                            </div>
                    </div>
                </div>
            </div>
        </section>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/UpdatePassword.js"></script>

        <script>
            var slideIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                slideIndex++;
                if (slideIndex > x.length) { slideIndex = 1 }
                x[slideIndex - 1].style.display = "block";
                setTimeout(carousel, 5000); // Change image every 5 seconds
            }
        </script>
        
        <asp:HiddenField ID="hdnCountryName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCityName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnIP" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnState" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnlatitude" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnlongitude" runat="server" ClientIDMode="Static" />

        <script>
            $(document).ready(function () {
                $.ajax({
                    url: 'https://geoip-db.com/jsonp',
                    jsonpCallback: 'callback',
                    dataType: 'jsonp',
                    success: function (location) {
                        $("#hdnCountryName").val(location.country_name);
                        $("#hdnCityName").val(location.city);
                        $("#hdnIP").val(location.IPv4);
                        $("#hdnState").val(location.state);
                        $("#hdnlatitude").val(location.latitude);
                        $("#hdnlongitude").val(location.longitude);
         
                    }
                });
            });

           
        </script>

    </form>
</body>
</html>
