﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.printables
{
    public partial class GRNReport : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        public string DocName
        {
            get
            {
                if (ViewState["DocName"] != null)
                {
                    return (string)ViewState["DocName"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocName"] = value;
            }
        }
        public int PrimaryValue
        {
            get
            {
                if (ViewState["PrimaryValue"] != null)
                {
                    return (int)ViewState["PrimaryValue"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrimaryValue"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/goods-receive-notes";
                if (HttpContext.Current.Items["GRNID"] != null)
                {
                    PrimaryValue = Convert.ToInt32(HttpContext.Current.Items["GRNID"].ToString());
                    getDocumentDesign();
                }
            }
        }

        private void getDocumentDesign()
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DocType = "Goods Recieve Notes";
            dt = objDB.GetDocumentDesign(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                    string content = dt.Rows[0]["DocContent"].ToString();
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                    txtContent.Content = addDetails(content);
                }
            }



            if (dt == null)
            {
                objDB.CompanyID = 2;
                objDB.DocType = "Goods Recieve Notes";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                        string content = dt.Rows[0]["DocContent"].ToString();
                        txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                        txtContent.Content = addDetails(content);
                    }
                }
            }
        }

        private string addDetails(string content)
        {

            DataTable dt = new DataTable();
            objDB.GRNID = PrimaryValue;
            dt = objDB.GetGRNByIDFotReport(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    content = content.Replace("##AGAINSTPO##", dt.Rows[0]["POCode"].ToString());
                    content = content.Replace("##SUPPLIER##", dt.Rows[0]["SupplierName"].ToString());
                    content = content.Replace("##GRNCODE##", dt.Rows[0]["GRNCode"].ToString());
                    content = content.Replace("##GENERATIONDATE##", dt.Rows[0]["GRNDate"].ToString());
                    content = content.Replace("##NOTES##", dt.Rows[0]["Notes"].ToString());
                    content = content.Replace("##SITE_NAME##", dt.Rows[0]["SiteName"].ToString());
                    //content = content.Replace("##TITLE##", dt.Rows[0]["GRNCode"].ToString());
                    //content = content.Replace("##SUPPLIER_NAME##", dt.Rows[0]["SupplierName"].ToString());
                    //content = content.Replace("##SUPPLIER_ADDRESSLINE1##", dt.Rows[0]["SupplierAddress1"].ToString());
                    //content = content.Replace("##SUPPLIER_ADDRESSLINE2##", dt.Rows[0]["SupplierAddress2"].ToString());
                    //content = content.Replace("##SUPPLIER_CITY##", dt.Rows[0]["SupplierCity"].ToString());
                    //content = content.Replace("##SUPPLIER_COUNTRY##", dt.Rows[0]["SupplierCountry"].ToString());
                    //// content = content.Replace("##SITE_NAME##", dt.Rows[0]["SiteName"].ToString());
                    //content = content.Replace("##POCODE##", dt.Rows[0]["GRNCode"].ToString());
                    //content = content.Replace("##REQUESTDATE##", dt.Rows[0]["GRNDate"].ToString());
                    //content = content.Replace("##AGAINSTPO##", dt.Rows[0]["POCode"].ToString());
                    //// content = content.Replace("##DESCRIPTION##", dt.Rows[0]["Description"].ToString());
                    //content = content.Replace("##NTN_N0##", dt.Rows[0]["NTN"].ToString());
                    //content = content.Replace("##NTN_NO##", dt.Rows[0]["SupplierNTN"].ToString());

                    DocName = dt.Rows[0]["GRNCode"].ToString().Replace(" ", "-");
                   
                }
            }

            content = content.Replace("##TABLE##", GetSubTables());
            return content;
        }
        private string GetSubTables()
        {
            string str = @"<table class='line-three'>
    <tbody>
      <tr>
        <th style='width:75px'>Sr No.</th>
        <th>Material Description</th>
        <th style='width:50px'>Unit</th>
        <th style='width:50px'>Demand Qty</th>
        <th style='width:50px'>Available CBS</th>
        <th style='width:150px'>Remarks</th>
      </tr>";
            DataTable dt = new DataTable();
            objDB.GRNID = PrimaryValue;
            dt = objDB.GetGRNItemsByGRNIDFinance(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    str += "<tbody>";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        str += "<tr><td>" + dt.Rows[i]["SrNo"] + "</td><td>" + dt.Rows[i]["ItemCode"] + "</td><td>" + dt.Rows[i]["ItemUnit"] + "</td><td>" + string.Format("{0:n2}", double.Parse(dt.Rows[0]["Quantity"].ToString() == "" ? "0" : dt.Rows[0]["Quantity"].ToString())) + "</td><td>" + dt.Rows[i]["Remarks"] + "</td></tr>";
                    }
                }
            }
            str += "</tbody>";
            str += "</table>";

            return str;
        }

        private string GetGRNTaxesByGRNID(int GRNID)
        {

            DataTable dt = new DataTable();
            objDB.GRNID = GRNID;
            dt = objDB.GetGRNTaxesByGRNID(ref errorMsg);
            string taxes = "";
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        taxes += (dt.Rows[i]["TaxRateName"].ToString() + ", ");


                    }
                }
                taxes = taxes.Substring(0, taxes.Length - 2);

            }
            return taxes;
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["POID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Common.generatePDF(txtHeader.Content, txtFooter.Content, txtContent.Content, DocName, "A4", "Portrait");
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

    }
}