﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GRNReport.aspx.cs" Inherits="Technofinancials.Finance.printables.GRNReport" %>
     
<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
    
        <main id="app-main" class="app-main">
            <div class="wrap">
      
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <img src="/assets/images/warning.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">GRN Report</h3>
                        </div>
                        <div class="col-sm-4" style="margin-top: 10px;">
                            <div class="pull-right">
                                <button class="tf-pdf-btn" type="button" "PDF" id="btnPDF" runat="server" onserverclick="btnPDF_ServerClick"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                                <%--<button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>--%>
                                <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Header</h4>
                            <textboxio:Textboxio runat="server" ID="txtHeader" ScriptSrc="/assets/textboxio/textboxio.js"  Width="900px" Height="300px" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Content</h4>
                            <textboxio:Textboxio runat="server" ID="txtContent" ScriptSrc="/assets/textboxio/textboxio.js"  Width="900px" Height="500px" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Footer</h4>
                            <textboxio:Textboxio runat="server" ID="txtFooter" ScriptSrc="/assets/textboxio/textboxio.js" Width="900px" Height="200px" />
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                    <div class="col-md-4 col-md-offset-4" style="margin-bottom: 10px;">
                        <div class="form-group" id="divAlertMsg" runat="server">
                            <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                <span>
                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                </span>
                                <p id="pAlertMsg" runat="server">
                                </p>
                            </div>
                        </div>
                    </div>
                        </div>
                </section>
                <!-- #dash-content -->
  
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
     
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .month-table-show {
                display: none;
                margin-top: 40px;
            }

            .modal-dialog {
                width: 450px;
                margin: 30px auto;
            }

            section.app-content {
                padding-bottom: 53px;
            }

            .modal-footer {
                border-top: 0px solid #e5e5e5;
            }

            .hiring-create-new-btn {
                margin-top: 17px;
                border: none;
                background: none;
                color: #01769a;
            }

            .user-img-div img {
                width: 169px !important;
                display: block;
                margin: 0 auto;
            }

            .user-img-div {
                width: auto;
            }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
        </style>
    </form>
</body>
</html>