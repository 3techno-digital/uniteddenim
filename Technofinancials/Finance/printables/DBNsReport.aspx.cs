﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.printables
{
    public partial class DBNsReport : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        public string DocName
        {
            get
            {
                if (ViewState["DocName"] != null)
                {
                    return (string)ViewState["DocName"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocName"] = value;
            }
        }
        public int PrimaryValue
        {
            get
            {
                if (ViewState["PrimaryValue"] != null)
                {
                    return (int)ViewState["PrimaryValue"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrimaryValue"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            { 
                divAlertMsg.Visible = false;
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/debit-note";
                if (HttpContext.Current.Items["DBNID"] != null)
                {
                    PrimaryValue = Convert.ToInt32(HttpContext.Current.Items["DBNID"].ToString());
                    getDocumentDesign();
                }
            }
        }

        private void getDocumentDesign()
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DocType = "Debit Note";
            dt = objDB.GetDocumentDesign(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                    string content = dt.Rows[0]["DocContent"].ToString();
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                    txtContent.Content = addDetails(content);
                }
            }
        }

        private string addDetails(string content)
        {

            DataTable dt = new DataTable();
            objDB.DBNID = PrimaryValue;
            dt = objDB.GetActiveDBNs(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    content = content.Replace("##MRN##", dt.Rows[0]["MRNCode"].ToString());
                    content = content.Replace("##PO##", dt.Rows[0]["POCode"].ToString());
                    content = content.Replace("##SUPPLIER##", dt.Rows[0]["SupplierName"].ToString());
                    content = content.Replace("##DEBITNOTECODE##", dt.Rows[0]["DBNCode"].ToString());
                    content = content.Replace("##GENERATEDATE##", dt.Rows[0]["DBNDate"].ToString());
                    content = content.Replace("##SALESINVOICENO##", dt.Rows[0]["SalesInvoiceNo"].ToString());
                    content = content.Replace("##TOTALAMOUNT##", dt.Rows[0]["AmountOfInvoice"].ToString());
                    content = content.Replace("##NOTES##", dt.Rows[0]["Notes"].ToString());
                    DocName = dt.Rows[0]["DBNCode"].ToString().Replace(" ", "-");

                    //         objDB.DocID = PrimaryValue;
                    //         objDB.DocType = "PurchasePlan";
                    //         content = content.Replace("##NOTES##", objDB.GetDocNotes());
                }
            }

            content = content.Replace("##TABLE##", GetSubTables());
            return content;
        }
        private string GetSubTables()
        {
            string str = "<table>";
            str += "<thead><tr><td>Sr No</td><td>Item Code</td><td>Item Unit</td><td>Quantity</td><td>Unit Price (PKR)</td><td>Net Price (PKR)</td><td>Remarks</td></tr></thead>";
            DataTable dt = new DataTable();
            objDB.DBNID = PrimaryValue;
            dt = objDB.GetDBNItemsByDBNID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    str += "<tbody>";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        str += "<tr><td>" + dt.Rows[i]["SrNo"] + "</td><td>" + dt.Rows[i]["ItemCode"] + "</td><td>" + dt.Rows[i]["ItemUnit"] + "</td><td>" + dt.Rows[i]["Quantity"] + "</td><td>" + dt.Rows[i]["UnitPrice"] + "</td><td>" + dt.Rows[i]["NetPrice"] + "</td><td>" + dt.Rows[i]["Remarks"] + "</td></tr>";
                    }
                    str += "</tbody>";
                }
            }
            str += "</table>";

            return str;
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["DBNID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Common.addlog("Report", "Procrument", "DBN Document Design Updated", "DocumentDesigns", PrimaryValue);

                Common.generatePDF(txtHeader.Content, txtFooter.Content, txtContent.Content, DocName, "A4", "Portrait");
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

    }
}