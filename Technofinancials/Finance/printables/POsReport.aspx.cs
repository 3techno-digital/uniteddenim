﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.printables
{
    public partial class POsReport : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        public string DocName
        {
            get
            {
                if (ViewState["DocName"] != null)
                {
                    return (string)ViewState["DocName"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocName"] = value;
            }
        }
        public int PrimaryValue
        {
            get
            {
                if (ViewState["PrimaryValue"] != null)
                {
                    return (int)ViewState["PrimaryValue"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrimaryValue"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {  
                divAlertMsg.Visible = false;
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/purchase-order";
                if (HttpContext.Current.Items["POID"] != null)
                {
                    PrimaryValue = Convert.ToInt32(HttpContext.Current.Items["POID"].ToString());
                    getDocumentDesign();
                }
            }
        }

        private void getDocumentDesign()
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DocType = "Purchase Orders";
            dt = objDB.GetDocumentDesign(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                    string content = dt.Rows[0]["DocContent"].ToString();
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                    txtContent.Content = addDetails(content);
                }
            }

            if (dt == null)
            {
                objDB.CompanyID = 2;
                objDB.DocType = "Purchase Orders";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                        string content = dt.Rows[0]["DocContent"].ToString();
                        txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                        txtContent.Content = addDetails(content);
                    }
                }
            }
        }

        private string addDetails(string content)
        {

            DataTable dt = new DataTable();
            objDB.POID = PrimaryValue;
            dt = objDB.GetPOsByIDFotReport(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    content = content.Replace("##TITLE##", "Purchase Order");
                    content = content.Replace("##AGAINSTRFQ##", dt.Rows[0]["AgainstCode"].ToString());
                    content = content.Replace("##SUPPLIER_NAME##", dt.Rows[0]["SupplierName"].ToString());
                    content = content.Replace("##SUPPLIER_ADDRESSLINE1##", dt.Rows[0]["SupplierAddress1"].ToString());
                    content = content.Replace("##SUPPLIER_ADDRESSLINE2##", dt.Rows[0]["SupplierAddress2"].ToString());
                    content = content.Replace("##SUPPLIER_COUNTRY##", dt.Rows[0]["SupplierCountry"].ToString());
                    content = content.Replace("##SUPPLIER_CITY##", dt.Rows[0]["SupplierCity"].ToString());
                    content = content.Replace("##NTN_N0##", dt.Rows[0]["NTN"].ToString());
                    content = content.Replace("##NTN_NO##", dt.Rows[0]["SupplierNTN"].ToString());
                    content = content.Replace("##POCODE##", dt.Rows[0]["POCode"].ToString());
                    content = content.Replace("##REQUESTDATE##", dt.Rows[0]["PODate"].ToString());
                    
                    content = content.Replace("##NOTES##", dt.Rows[0]["Notes"].ToString());

                    content = content.Replace("##SITE_NAME##", dt.Rows[0]["LocationName"].ToString());
                    content = content.Replace("##BUSINESS_NAME##", dt.Rows[0]["Business"].ToString());
                    content = content.Replace("##CREATED_BY##", dt.Rows[0]["CreatedBy"].ToString());
                    content = content.Replace("##SUPPLIER_PHONE##", dt.Rows[0]["SupplierPhone"].ToString());
                    content = content.Replace("##SUPPLIER_BANK##", dt.Rows[0]["BankName"].ToString());
                    content = content.Replace("##SUPPLIER_ACCOUNT##", dt.Rows[0]["AccountNo"].ToString());
                    content = content.Replace("##AMOUNT_IN_WORDS##", Common.NumberToWords(dt.Rows[0]["POAmount"].ToString()));


                    DocName = dt.Rows[0]["POCode"].ToString().Replace(" ", "-");

                    //         objDB.DocID = PrimaryValue;
                    //         objDB.DocType = "PurchasePlan";
                    //         content = content.Replace("##NOTES##", objDB.GetDocNotes());
                }
            }

            content = content.Replace("##TABLE##", GetSubTables());
            content = content.Replace("##TOTALAMOUNT##", dt.Rows[0]["POAmount"].ToString());
            content = content.Replace("##TAXESAPPLIED##", dt.Rows[0]["TotalTax"].ToString());
            content = content.Replace("##DISCOUNT##", dt.Rows[0]["Discount"].ToString());
            content = content.Replace("##GROSS_AMOUNT##", dt.Rows[0]["GrossAmount"].ToString());
            return content;
        }
        private string GetSubTables()
        {
            //string str = "<table>";
            string str = @"<table class='line-three' style='height: 63px;'>
    <tbody>
      <tr style='height: 21px;'>
        <th style='width: 50px; height: 21px;'>S NO</th>
        <th style='height: 21px;'>Description</th>
        <th style='width: 60px; height: 21px;'>Unit</th>
        <th style='width: 60px; height: 21px;'>Qty</th>
        <th style='width: 138px; height: 21px;'>Unit Price</th>
        <th style='width: 138px; height: 21px;'>Total Price</th>
      </tr>";
            //str += "<thead><tr><td>Sr No</td><td>Item Code</td><td>Item Unit</td><td>Quantity</td><td>Unit Price (PKR)</td><td>Net Price (PKR)</td><td>Remarks</td></tr></thead>";
            DataTable dt = new DataTable();
            objDB.POID = PrimaryValue;
            dt = objDB.GetPOItemsByPOID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    //str += "<tbody>";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        str += "<tr><td>" + dt.Rows[i]["SrNo"] + "</td><td class='bold-text'>" + dt.Rows[i]["ItemCode"] + "</td><td>" + dt.Rows[i]["ItemUnit"] + "</td><td>" + dt.Rows[i]["Quantity"] + "</td><td>" + dt.Rows[i]["UnitPrice"] + "</td><td>" + dt.Rows[i]["NetPrice"] + "</td></tr>";
                    }
                }
            }
            str += @"<tr style='height: 21px;'>
        <td rowspan='4' colspan='4' style='height: 42px;'></td>
        <td class='bold-text' style='height: 21px;'>Sub Total</td>
        <td style='height: 21px;'>##GROSS_AMOUNT##</td>
      </tr>

      <tr style='height: 21px;'>

        <td class='bold-text' style='height: 21px;'>Tax</td>
        <td style='height: 21px;'>##TAXESAPPLIED##</td>
      </tr>

      <tr style='height: 21px;'>

        <td class='bold-text' style='height: 21px;'>Discount</td>
        <td style='height: 21px;'>##DISCOUNT##</td>
      </tr>
      <tr style='height: 21px;'>

        <td class='bold-text' style='height: 21px;'>Gross Value PRs</td>
        <td style='height: 21px;'>##TOTALAMOUNT##</td>
      </tr>

    </tbody>
  </table>";

            return str;
        }
        private string GetPOTaxesByPOID(int POID)
        {

            DataTable dt = new DataTable();
            objDB.POID = POID;
            dt = objDB.GetPOTaxesByPOID(ref errorMsg);
            string taxes = "";
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        taxes += (dt.Rows[i]["TaxRateName"].ToString() + ", ");


                    }
                }
                taxes = taxes.Substring(0, taxes.Length - 2);

            }
            return taxes;
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["POID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Common.addlog("Report", "Finance", "PO Document Design Updated", "DocumentDesigns", PrimaryValue);

                Common.generatePDF(txtHeader.Content, txtFooter.Content, txtContent.Content, DocName, "A4", "Portrait");
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

    }
}