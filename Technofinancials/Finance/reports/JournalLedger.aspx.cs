﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.reports
{
    public partial class JournalLedger : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        private DataTable dtVoucher
        {
            get
            {
                if (ViewState["dtVoucher"] != null)
                {
                    return (DataTable)ViewState["dtVoucher"];
                }
                else
                {
                    return new DataTable();
                }
            }
            set
            {
                ViewState["dtVoucher"] = value;
            }
        }

        private string Locations
        {
            get
            {
                if (ViewState["Locations"] != null)
                {
                    return (string)ViewState["Locations"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["Locations"] = value;
            }
        }

        private float TotalCredit
        {
            get
            {
                if (ViewState["TotalCredit"] != null)
                {
                    return (float)ViewState["TotalCredit"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["TotalCredit"] = value;
            }
        }

        private float TotalDebit
        {
            get
            {
                if (ViewState["TotalDebit"] != null)
                {
                    return (float)ViewState["TotalDebit"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["TotalDebit"] = value;
            }
        }

        private float TotalBalance
        {
            get
            {
                if (ViewState["TotalBalance"] != null)
                {
                    return (float)ViewState["TotalBalance"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["TotalBalance"] = value;
            }
        }

        private string VoucherRows
        {
            get
            {
                if (ViewState["VoucherRows"] != null)
                {
                    return (string)ViewState["VoucherRows"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["VoucherRows"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                BindDropdown();
                //gvLiteral.Text = "<h1>abc</h1><h2>cdf</h2>";
            }
        }

        //protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    lblCOABal.Text = "Balance: 0";
        //    try
        //    {
        //        objDB.COA_ID = Convert.ToInt32(ddlCOA.SelectedValue);
        //        DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
        //        if (dtCOABalance != null)
        //        {
        //            if (dtCOABalance.Rows.Count > 0)
        //            {
        //                lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }
        //}
        private void BindDropdown()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            //ddlCOA.Items.Clear();
            //ddlCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            //ddlCOA.DataTextField = "CodeTitle";
            //ddlCOA.DataValueField = "COA_ID";
            //ddlCOA.DataBind();
            //ddlCOA.Items.Insert(0, new ListItem("--- Select COA---", "0"));


            lbCOAs.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            //lbTaxes.DataSource = dtTaxTable;
            lbCOAs.DataTextField = "CodeTitle";
            lbCOAs.DataValueField = "COA_ID";
            lbCOAs.DataBind();
            lbCOAs.Items.Insert(0, new ListItem("All", "All"));

            lbLocations.DataSource = objDB.GetAllApprovedLocations(ref errorMsg);
            //lbTaxes.DataSource = dtTaxTable;
            lbLocations.DataTextField = "SiteName";
            lbLocations.DataValueField = "SiteID";
            lbLocations.DataBind();
            lbLocations.Items.Insert(0, new ListItem("All", "All"));
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string whereClause = "where TransactionHistory.DocStatus = 'Approved' AND TransactionHistory.CompanyID = " + Session["CompanyID"].ToString();
                if (txtFromDate.Value != "")
                {
                    objDB.FromDate = " AND FORMAT(Cast(TransactionHistory.Date as Date),'yyyMMdd') >= FORMAT(Cast('" + txtFromDate.Value + "' as Date),'yyyMMdd') ";
                }
                else
                {
                    objDB.FromDate = "";
                }
                if (txtToDate.Value != "")
                {
                    objDB.ToDate = " AND FORMAT(Cast(TransactionHistory.Date as Date),'yyyMMdd') <= FORMAT(Cast('" + txtToDate.Value + "' as Date),'yyyMMdd') ";
                }
                else
                {
                    objDB.ToDate = "";
                }

                whereClause += objDB.FromDate + objDB.ToDate;
                int index = 0;
                string temp = "";
                foreach (ListItem li in lbCOAs.Items)
                {
                    if (li.Selected == true)
                    {
                        if (li.Value == "All")
                        {
                            temp = "";
                            index = 0;
                            break;
                        }
                        if (index == 0)
                        {
                            temp += " AND (TransactionHistory.COA_ID = " + li.Value;
                            index++;
                        }
                        else
                        {
                            temp += " or TransactionHistory.COA_ID = " + li.Value;
                        }
                    }
                }
                if (temp != "")
                {
                    whereClause += (temp + ")");
                }

                index = 0;
                temp = "";
                foreach (ListItem li in lbLocations.Items)
                {
                    if (li.Selected == true)
                    {
                        if (li.Value == "All")
                        {
                            temp = "";
                            index = 0;
                            Locations = li.Text;
                            break;
                        }
                        if (index == 0)
                        {
                            temp += " AND (TransactionHistory.LocationID = " + li.Value;
                            index++;
                            Locations += ", " + li.Text;
                        }
                        else
                        {
                            temp += " or TransactionHistory.LocationID = " + li.Value;
                            Locations += ", " + li.Text;
                        }
                    }
                }

                if (temp != "")
                {
                    whereClause += (temp + ")");
                    Locations = Locations.Substring(2);
                }
                objDB.WhereClause = whereClause;
                DataTable dt = objDB.GetJournalLedgerNew(ref errorMsg);
                DataTable dtTemp = new DataTable();
                dtTemp.Columns.Add("Date");
                dtTemp.Columns.Add("VT");
                dtTemp.Columns.Add("V.No.");
                dtTemp.Columns.Add("Project");
                dtTemp.Columns.Add("Instrument");
                dtTemp.Columns.Add("TransactionDetail");
                //dtTemp.Columns.Add("OpeningBalance");
                dtTemp.Columns.Add("Debit");
                dtTemp.Columns.Add("Credit");
                dtTemp.Columns.Add("Balance");
                dtTemp.AcceptChanges();
                string tempCOA = "";
                VoucherRows = "";
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataRow dr = dtTemp.NewRow();
                            if (dt.Rows[i]["COA"].ToString() != tempCOA)
                            {
                                if (i > 0)
                                {
                                    TotalBalance = TotalDebit - TotalCredit;
                                    VoucherRows += "<tr><td colspan = '6' style = 'text-align:right;'>" + "TOTAL: " + "</td><td>" + string.Format("{0:n2}", TotalDebit) + "</td><td>" + string.Format("{0:n2}", TotalCredit) + "</td><td>" + string.Format("{0:n2}", TotalBalance) + "</td></tr>";
                                    TotalDebit = 0;
                                    TotalCredit = 0;
                                    TotalBalance = 0;
                                }
                                dr[0] = dt.Rows[i]["COA"].ToString();
                                dr[1] = "";
                                dr[2] = "";
                                dr[3] = "";
                                dr[4] = "";
                                dr[5] = "";
                                dr[6] = "";
                                dr[7] = "";
                                dr[8] = "";
                                tempCOA = dt.Rows[i]["COA"].ToString();
                                dtTemp.Rows.Add(dr);
                                dtTemp.AcceptChanges();
                                VoucherRows += "<tr><td colspan='9'>" + dt.Rows[i]["COA"].ToString() + "</td></tr>";
                            }
                            dr = dtTemp.NewRow();
                            dr[0] = dt.Rows[i]["Date"].ToString();
                            dr[1] = dt.Rows[i]["VT"].ToString();
                            dr[2] = dt.Rows[i]["V.No."].ToString();
                            dr[3] = dt.Rows[i]["Project"].ToString();
                            dr[4] = dt.Rows[i]["Inst/Cheq#"].ToString();
                            dr[5] = dt.Rows[i]["TransactionDetail"].ToString();
                            //dr[5] = dt.Rows[i]["OpeningBalance"].ToString();
                            dr[6] = dt.Rows[i]["Debit"].ToString();
                            dr[7] = dt.Rows[i]["Credit"].ToString();
                            dr[8] = dt.Rows[i]["Balance"].ToString();
                            dtTemp.Rows.Add(dr);
                            dtTemp.AcceptChanges();
                            TotalDebit += float.Parse(dt.Rows[i]["Debit"].ToString());
                            TotalCredit += float.Parse(dt.Rows[i]["Credit"].ToString());
                            VoucherRows += "<tr><td>" + dt.Rows[i]["Date"].ToString() + "</td><td>" + dt.Rows[i]["VT"].ToString() + "</td><td>" + dt.Rows[i]["V.No."].ToString() + "</td><td>" + dt.Rows[i]["Project"].ToString() + "</td><td>" + dt.Rows[i]["Inst/Cheq#"].ToString() + "</td><td>" + dt.Rows[i]["TransactionDetail"].ToString() + "</td><td>" + dt.Rows[i]["Debit"].ToString() + "</td><td>" + dt.Rows[i]["Credit"].ToString() + "</td><td>" + dt.Rows[i]["Balance"].ToString() + "</td></tr>";
                            if (i == dt.Rows.Count - 1)
                            {
                                TotalBalance = TotalDebit - TotalCredit;
                                VoucherRows += "<tr><td colspan = '6' style = 'text-align:right;'>" + "TOTAL: " + "</td><td>" + string.Format("{0:n2}", TotalDebit) + "</td><td>" + string.Format("{0:n2}", TotalCredit) + "</td><td>" + string.Format("{0:n2}", TotalBalance) + "</td></tr>";
                                TotalDebit = 0;
                                TotalCredit = 0;
                                TotalBalance = 0;
                            }
                        }
                    }
                }

                dtVoucher = dtTemp;
                gv.DataSource = dtTemp;
                gv.DataBind();
                if (dtTemp != null)
                {
                    if (dtTemp.Rows.Count > 0)
                    {
                        divAlertMsg.Visible = false;
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gv.DataSource = null;
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                }
                else
                {
                    gv.DataSource = null;
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
            Common.addlog("Report", "Finance", "General Voucher Report", "");
        }
        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
            {
                Response.Redirect("/login");
                return false;
            }
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
            {
                Response.Redirect("/login");
                return false;

            }
            return true;
        }


        protected void GenVoucherPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                //string DocName = "Voucher";
                DataTable dt = new DataTable();
                string txtHeader = "";
                string txtContent = "";
                string txtFooter = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.DocType = "General Ledger";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt == null)
                {
                    objDB.CompanyID = 2;
                    objDB.DocType = "";
                    dt = objDB.GetDocumentDesign(ref errorMsg);
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtHeader = dt.Rows[0]["DocHeader"].ToString();
                        if (Session["CompanyName"].ToString() != null)
                        {
                            txtHeader = txtHeader.Replace("##COMPANY_NAME##", Session["CompanyName"].ToString());
                        }
                        txtContent = dt.Rows[0]["DocContent"].ToString();
                        txtFooter = dt.Rows[0]["DocFooter"].ToString();
                    }
                }
                if (dtVoucher != null && dtVoucher.Rows.Count > 0)
                {
                    //DocName = Common.getCompleteVoucherName(dtVoucher.Rows[0]["VoucherType"].ToString());
                    txtContent = txtContent.Replace("##LOCATION_NAME##", Locations == "" ? "All" : Locations);
                    txtContent = txtContent.Replace("##FROM_DATE##", txtFromDate.Value == "" ? "-" : txtFromDate.Value);
                    txtContent = txtContent.Replace("##TO_DATE##", txtToDate.Value == "" ? "-" : txtToDate.Value);
                    //DocName = DocName + "-" + dtVoucher.Rows[0]["SerialNo"].ToString() + "-" + dtVoucher.Rows[0]["VoucherNo"].ToString();

                    string table = @"<table class='line-three'>
    <tbody>
      <tr>
        <th style='width:100px'>Date</th>
        <th>VT</th>
        <th>V.NO</th>
        <th>Project</th>
        <th>Instrument#</th>
        <th>Transaction Details</th>
        <th>Debit</th>
        <th>Credit</th>
        <th>Balance</th>
      </tr>";

                    //for (int i = 0; i < dtVoucher.Rows.Count; i++)
                    //{
                    table += VoucherRows;
                    //totAmount += double.Parse(dtVoucher.Rows[i]["Debit"].ToString());
                    //}

                    table += "";
                    table += "</tbody></table>";
                    //table += "<p>" + Common.NumberToWords(totAmount) + "</p>";
                    txtContent = txtContent.Replace("##DETAIL_TABLE##", table);

                    Common.generatePDF(txtHeader, txtFooter, txtContent, "Accounts Ledger", "A4", "Portrait");
                }
            }
            catch (Exception ex)
            {
            }
        }

    }
}