﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DesignReport.aspx.cs" Inherits="Technofinancials.Finance.reports.DesignReport" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <div class="wrap">
                <asp:UpdatePanel ID="UpdPnl" runat="server">
                    <ContentTemplate>
                        <section class="app-content">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                <img src="/assets/images/Offer Letter Design.png" class="img-responsive tf-page-heading-img" id="imgHeading" runat="server" />
                                <h3 class="tf-page-heading-text" id="pageHeading" runat="server">Reports Letter Design</h3>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                <div class="alert alert-info">
                                    Please use the keywords to bind the database values. For keywords list please 
                                 
                                 <a href="#" data-toggle="modal" data-target="#modalReportTemplate" "View" id="btnFinanceReport" runat="server">Click Here</a>
                                 <a href="#" data-toggle="modal" data-target="#modalExpenseVoucher" "View" id="btnExpenseVoucher" runat="server">Click Here</a>
                                 <a href="#" data-toggle="modal" data-target="#modalAssestsList" "View" id="btnAssetsListReport" runat="server">Click Here</a>
                                 <a href="#" data-toggle="modal" data-target="#modalInvoice" "View" id="btnInvoice" runat="server">Click Here</a>
                                 
                                
                                    </div>
                            </div>
                            <div class="col-sm-4" >
                                <div class="pull-right flex">


                                    <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                    <%--<a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>--%>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr />
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="letters-width">
                                    <h4>Header</h4>
                                    <textboxio:Textboxio runat="server" ID="txtHeader" ScriptSrc="/assets/textboxio/textboxio.js" Width="100%" Height="300px" />
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="letters-width">
                                    <h4>Content</h4>
                                    <textboxio:Textboxio runat="server" ID="txtContent" ScriptSrc="/assets/textboxio/textboxio.js" Width="100%" Height="500px" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="letters-width">
                                    <h4>Footer</h4>
                                    <textboxio:Textboxio runat="server" ID="txtFooter" ScriptSrc="/assets/textboxio/textboxio.js" Width="100%" Height="200px" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4" style="margin-bottom: 10px;">
                                <div class="form-group" id="divAlertMsg" runat="server">
                                    <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                        <span>
                                            <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                        </span>
                                        <p id="pAlertMsg" runat="server">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </section>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        

        <div class="modal fade" id="modalReportTemplate" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Keywords</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Notations</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>Title</td>
                                        <td>##TITLE##</td>
                                    </tr>

                                    <tr>
                                        <td>Table</td>
                                        <td>##TABLE##</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        <div class="modal fade" id="modalAssestsList" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Keywords</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Notations</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>Assets Name</td>
                                        <td>##NAME##</td>
                                    </tr>

                                    <tr>
                                        <td>Assets Number</td>
                                        <td>##NUMBER##</td>
                                    </tr>
                                      <tr>
                                        <td>Assets Type</td>
                                        <td>##TYPE##</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>  
        
        
        <div class="modal fade" id="modalExpenseVoucher" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Keywords</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Notations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <tr>
                                        <td>Date</td>
                                        <td>##DATE##</td>
                                    </tr>


                                    <tr>
                                        <td>Title</td>
                                        <td>##TITLE##</td>
                                    </tr>
                                     <tr>
                                        <td>Amount</td>
                                        <td>##AMOUNT##</td>
                                    </tr>

                                    <tr>
                                        <td>Voucher No</td>
                                        <td>##VOUCHER_NO##</td>
                                    </tr>

                                    <tr>
                                        <td>Voucher Type</td>
                                        <td>##VOUCHER_TYPE##</td>
                                    </tr>

                                     <tr>
                                        <td>Item Table</td>
                                        <td>##ITEMS##</td>
                                    </tr>

                                    

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        
        <div class="modal fade" id="modalInvoice" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Keywords</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Notations</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>Title</td>
                                        <td>##TITLE##</td>
                                    </tr>

                                    <tr>
                                        <td>Table</td>
                                        <td>##TABLE##</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        <!--========== END app main -->


        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .month-table-show {
                display: none;
                margin-top: 40px;
            }

            .modal-dialog {
                width: 450px;
                margin: 30px auto;
            }

            section.app-content {
                padding-bottom: 53px;
            }

            .modal-footer {
                border-top: 0px solid #e5e5e5;
            }

            .hiring-create-new-btn {
                margin-top: 17px;
                border: none;
                background: none;
                color: #01769a;
            }

            .user-img-div img {
                width: 169px !important;
                display: block;
                margin: 0 auto;
            }

            .user-img-div {
                width: auto;
            }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
        </style>
    </form>
</body>
</html>

