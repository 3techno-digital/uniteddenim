﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.IO;
using System.IO.Compression;

namespace Technofinancials.Finance.reports
{
	public partial class ReportForm : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();

                if (!Page.IsPostBack)
                {
                    ShowReport();
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }
        private void sendOutZIP(byte[] zippedFiles, string filename)
        {
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/x-compressed";
            Response.Charset = string.Empty;
            Response.Cache.SetCacheability(System.Web.HttpCacheability.Public);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(zippedFiles);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
            Response.End();
        }
        public void ShowReport()
        {
            try
            {
                List<string> emps = (List<string>)Session["EmployeeID"];
                List<byte[]> BytesList = new List<byte[]>();
                List<string> EmployeeName = new List<string>();
                objDB.CompanyID = Convert.ToInt16(Session["CompanyID"]);
                objDB.FinancialYear = Session["FYID"].ToString();
                objDB.DeptID = Convert.ToInt32(Session["DeptID"]);
                objDB.DesgID = Convert.ToInt32(Session["DesgID"]);
                objDB.WorkDayID = Session["WorkDayID"].ToString();
                objDB.Location = Session["Location"].ToString();
                //objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
				foreach (var emp in emps)
				{
					objDB.EmployeeID = Convert.ToInt32(emp);
                    DataTable dt = objDB.GetEmployeeYearlyTax(ref errorMsg);
                    EmployeeName.Add(dt.Rows[0]["EmployeeName"].ToString());
                    float TotalTaxes = 0;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TotalTaxes = +TotalTaxes + float.Parse(dt.Rows[i]["TotalTaxes"].ToString());
                    }
                    DataTable dt2 = objDB.GetEmployeeYearlyTaxSlabs(TotalTaxes.ToString(), dt.Rows[0]["TaxDate"].ToString(), ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            ReportViewer1.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~/bin/Finance/reports/Report1.rdlc");
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet2", dt2));
                            Warning[] warnings;
                            string[] streamIds;
                            string deviceInfo = string.Empty; // "<DeviceInfo><OutputFormat>PDF</OutputFormat><PageWidth>8.27in</PageWidth><PageHeight>11.69in</PageHeight><MarginTop>0.25in</MarginTop><MarginLeft>0.4in</MarginLeft><MarginRight>0in</MarginRight><MarginBottom>0.25in</MarginBottom></DeviceInfo>";
                            string mimeType = string.Empty;
                            string encoding = string.Empty;
                            string extension = string.Empty;
                            deviceInfo = @"<DeviceInfo><EmbedFonts>None</EmbedFonts></DeviceInfo>";
                            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", deviceInfo, out mimeType, out encoding, out extension, out streamIds, out warnings);
                            BytesList.Add(bytes);
							// Now that you have all the bytes representing the PDF report, buffer it and send it to the client.    
						}
                    }
                }
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    // Creates the .zip
                    using (ZipArchive archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                    {
                        int empIndex = 0;

                        foreach (byte[] f in BytesList)
                        {
                            System.IO.Compression.ZipArchiveEntry zipItem = archive.CreateEntry($"{EmployeeName[empIndex].Replace(' ', '_') }_YearlyTaxReport.pdf");
                            // add the item bytes to the zip entry by opening the original file and copying the bytes
                            using (System.IO.MemoryStream originalFileMemoryStream = new System.IO.MemoryStream(f))
                            {
                                using (System.IO.Stream entryStream = zipItem.Open())
                                {
                                    originalFileMemoryStream.CopyTo(entryStream);
                                }
                            }
                            empIndex++;
                        }
                    }
                    sendOutZIP(ms.ToArray(), "Yealy-Tax-Report-"+DateTime.Now.ToString("ddMMyyyyHH:mm:ss")+".zip");
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}