﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.reports
{
    public partial class AssetsAndDeductions : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
          
            if (!Page.IsPostBack)
            {
                CheckSessions();

              
                divAlertMsg.Visible = false;
            }
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            GetAssetsAndDeductions();
        }
 
     
        private void GetAssetsAndDeductions()
        {
            divAlertMsg.Visible = false;
            DataTable dt = new DataTable();
         
            objDB.PayrollDate = txtPayrollDate.Text;
            objDB.PayrollDate = "01-" + txtPayrollDate.Text;
            dt = objDB.GetAssetsAndDeductions( ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.DataSource = dt;
                    gv.DataBind();
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;

                   
                }
            }

            Common.addlog("ViewAll", "Finance", "AssetsAndDeductions", "AssetsAndDeductions(");
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }
    }
}