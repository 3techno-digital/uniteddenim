﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceReport.aspx.cs" Inherits="Technofinancials.Finance.reports.InvoiceReport" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/StatementSharing.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Invoice Report</h3>
                        </div>

                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                        <div class="col-sm-4">
                            <div class="pull-right">

                                <button class="tf-pdf-btn" "Generate Report" id="btnReport" runat="server" onserverclick="btnReport_ServerClick" type="button"><i class="fa fa-file-pdf-o"></i></button>

                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
      <div class="col-sm-12">
                                 <div class="row">
                                         <div class="col-sm-3">
                                        <div class="form-group">
                                            <h4>From Date<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtFromDate" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtFromDate" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <h4>To Date<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtToDate" runat="server" />
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Client<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlClient" InitialValue="0" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                        <div class="form-group">
                                            <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control js-example-basic-single" ID="ddlClient" >
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                        <ContentTemplate>
                                            <div class="col-md-3">
                                                <h4>&nbsp;</h4>

                                                <button class="tf-view-btn" "View" id="btnView" runat="server" ValidationGroup="btnValidate" onserverclick="btnView_ServerClick" type="button"><i class="far fa-eye"></i></button>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                 </div>
                                </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row ">
                     <div class="col-sm-12">
                             <div class="tab-content ">
                            <div class="tab-pane active row">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                <div class="col-sm-12 gv-overflow-scrool">
                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("Code") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Client Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol12" Text='<%# Eval("ClientName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Site Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol13" Text='<%# Eval("SiteName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Amount">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol11" Text='<%# (Eval("Amount").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("Amount").ToString())):Eval("Amount").ToString() %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Paid Amount">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol112" Text='<%# (float.Parse(Eval("PaidAmount").ToString())>float.Parse(Eval("Amount").ToString()))?(Eval("Amount").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("Amount").ToString())):Eval("Amount").ToString():(Eval("PaidAmount").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("PaidAmount").ToString())):Eval("PaidAmount").ToString() %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remaining Balance">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol113" Text='<%# (float.Parse(Eval("RemainingBalance").ToString())<0)?"0.00":(Eval("RemainingBalance").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("RemainingBalance").ToString())):Eval("RemainingBalance").ToString() %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payment Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol114" Text='<%# (float.Parse(Eval("RemainingBalance").ToString())>0)?"Un Paid": "Paid" %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--                                     <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <a href="<%# "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/statement-sharing/edit-statement-sharing-" + Eval("SSID") %>" class="edit-class" >View</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                            </div>
                        </div>
                     </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
             
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }
        </style>
    </form>
</body>
</html>
