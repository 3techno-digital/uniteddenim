﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.reports
{
    public partial class BankVouchers : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                GetAllUsers();
            }
        }




        private void GetAllUsers()
        {
            CheckSessions();

            DataTable dt = new DataTable();

            objDB.Against = "Invoice";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetAllBankVouchers(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (gv != null)
            {
                if (gv.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "Finance", "All  Bank Vouchers  Viewed", "BankCashVouchers");
        }


        protected bool CheckSessions()
        {
            if (Session["UserName"] == null)
            {
                Response.Redirect("/login?url=" + HttpContext.Current.Request.Url.AbsolutePath.ToString());
                return false;
            }
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
            return true;
        }

        protected void lnkCommand_Click(object sender, EventArgs e)
        {
            if (CheckSessions())
            {
                LinkButton btn = (LinkButton)sender as LinkButton;
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/" + btn.CommandArgument.ToString());
            }
        }
    }
}