﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeWHTReport.aspx.cs" Inherits="Technofinancials.Finance.reports.EmployeeWHTReport" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
            .AD_btn_inn{padding: 3px 24px;}
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }
                    div div#gv_wrapper {
    width: 2000px;
    padding-bottom:5rem;
}
                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
                    padding: 5px!important;
    height: 38px!important;
    width: 37px!important;
    display: inline-block!important;
    text-align: center!important;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

                .table > thead:first-child > tr:first-child > th{
                    font-size:11px;
                }
                table#gv {   
    width: 3000px;
}
                    div div#gv_wrapper::-webkit-scrollbar-thumb {
                        background-color: #003780;
                        border: 2px solid #003780;
                        border-radius: 10px;
                    }
                    div div#gv_wrapper::-webkit-scrollbar {
    height: 10px;
    background-color: #ffffff;
}
                    div div#gv_wrapper::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #FFF;
}
            @media only screen and (max-width: 992px) and (min-width: 768px) {
                div div#gv_wrapper {
                    overflow-x: scroll;
                    overflow-y: hidden;
                    padding-bottom: 5rem;
                }
            }
            @media only screen and (max-width: 1024px) and (min-width: 993px) {
                div div#gv_wrapper {
                    overflow-x: scroll;
                    overflow-y: hidden;
                    padding-bottom: 5rem;
                }
            }
            @media only screen and (max-width: 1280px) and (min-width: 800px) {
                div div#gv_wrapper {
                    overflow-x: scroll;
                    overflow-y: hidden;
                    padding-bottom: 5rem;
                }
            }
        </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">

                  <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Employee With-Holding Tax Report</h1>
                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->




                    </div>
                    <!-- /.container-fluid -->
                </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                               
                                
                                  <div class="col-sm-6">
                                <%--    <div class="form-group">
                                        <h4>Location</h4>
                                        <asp:DropDownList ID="ddlLocation" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>--%>
                                    <div class="form-group">
                                    <h4>Location</h4>
                                    <asp:DropDownList ID="ddlLocation" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                    </asp:DropDownList>
                                </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Month
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtEOBIDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtEOBIDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
                                    </div>
                                </div>
                            <div class="col-sm-6">
                                     <div class="form-group">
                                        <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="GetDatabyLocationandMonth" type="button">View</button>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12"></div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                                                                        <div class="clear-fix">&nbsp;</div>
                    <div class="row ">
                      <div class="col-sm-12">
                            <div class="tab-content ">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                     
                                  <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Payment Section">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("PaymentSection") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                 
                                            
                                        
                                        
                                          
                                            <asp:TemplateField HeaderText="Tax Payer NTN">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("TaxPayerNTN") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tax Payer CNIC">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("CNIC") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Tax Payer Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Tax Payer City">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("City") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tax Payer Address">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("AddressLine1") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Tax Payer Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("TaxPayerStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tax Payer Bussiness Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("TaxPayerBussinessName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Taxable Amount">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("TaxableIncome") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                              <asp:TemplateField HeaderText="Tax Amount">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Totaltax") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              
                                             
                                             
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                                        <div class="clear-fix">&nbsp;</div>
                    <div class="clear-fix">&nbsp;</div>
                    <div class="clear-fix">&nbsp;</div>
                </section>
              
            </div>
            
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

        
    </form>
</body>
</html>

