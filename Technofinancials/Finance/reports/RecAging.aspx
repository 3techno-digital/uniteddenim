﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecAging.aspx.cs" Inherits="Technofinancials.Finance.reports.RecAging" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/Admin_Purchase_Orders.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Receivable Aging</h3>
                        </div>

                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                        <div class="col-sm-4">
                            <div class="pull-right">

                                <button class="tf-pdf-btn" "Generate Report" id="btnReport" runat="server" onserverclick="btnReport_ServerClick" type="button"><i class="fa fa-file-pdf-o"></i></button>

                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="row">

                                    <div class="col-md-4">
                                        <h4>Clients</h4>
                                        <asp:DropDownList runat="server" data-plugin="select2" CssClass="select2 form-control" ID="ddlClient">
                                        </asp:DropDownList>
                                    </div>

                                    <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                        <ContentTemplate>
                                            <div class="col-md-8">
                                                <h4>&nbsp;</h4>

                                                <button class="tf-view-btn" "View" id="btnView" runat="server" onserverclick="btnView_ServerClick" type="button"><i class="far fa-eye"></i></button>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>


                            </div>
                            <div class="cleafix">&nbsp;</div>
                            <div class="cleafix">&nbsp;</div>
                            <div>&nbsp;</div>
                            <div class="col-sm-12 table-09-main-div">
                                <div class="add-table-div gv-overflow-scrool">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr No.">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex + 1%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Customer Name">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblPODate" Text='<%# Eval("ClientName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Total Invoice Amount">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblRFQCode" Text='<%# (Eval("TotAmount").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("TotAmount").ToString())):Eval("TotAmount").ToString() %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="35 Days">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSupplierName" Text='<%# (Eval("35").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("35").ToString())):Eval("35").ToString() %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="45 Days">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSupplierName" Text='<%# (Eval("45").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("45").ToString())):Eval("45").ToString() %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Above 45 Days">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSupplierName" Text='<%# (Eval("65").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("65").ToString())):Eval("65").ToString() %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Total Received Amount">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSupplierName" Text='<%# (Eval("TotPaidAmount").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("TotPaidAmount").ToString())):Eval("TotPaidAmount").ToString() %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Total Out Standing Amount">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblTotRemainingAmount" Text='<%# (Eval("TotRemainingAmount").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("TotRemainingAmount").ToString())):Eval("TotRemainingAmount").ToString() %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                            </div>

                        </div>
                    </div>
                
                </section>
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
<style>
    .add-table-div {
        margin-bottom: 60px;
    }
</style>

</body>

</html>
