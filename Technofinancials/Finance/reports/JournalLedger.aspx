﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JournalLedger.aspx.cs" Inherits="Technofinancials.Finance.reports.JournalLedger" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        .select2-selection {
            height: auto !important;
        }

        .background-color {
            color: red;
        }
    </style>

</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/Admin_Purchase_Orders.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">General Ledger</h3>
                        </div>

                        <div class="col-md-4">

                            <div class="form-group" id="divAlertMsg" runat="server">
                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                    <span>
                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                    </span>
                                    <p id="pAlertMsg" runat="server">
                                    </p>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-4">
                            <div class="pull-right">
                                <button class="tf-pdf-btn" "Generate Report" id="btnReport" runat="server" onserverclick="GenVoucherPDF_ServerClick" type="button"><i class="fa fa-file-pdf-o"></i></button>
                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                COA<span style="color: red !important;">*</span>
                                            </label>
                                            <div class="form-group">
                                                <asp:ListBox ID="lbCOAs" runat="server" CssClass="select2" ClientIDMode="Static" SelectionMode="Multiple" data-plugin="select2"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Locations <span style="color: red !important;">*</span>
                                            </label>
                                            <div class="form-group">
                                                <asp:ListBox ID="lbLocations" runat="server" CssClass="select2" ClientIDMode="Static" SelectionMode="Multiple" data-plugin="select2"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <h4>From Date</h4>
                                            <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtFromDate" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <h4>To Date</h4>
                                            <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtToDate" runat="server" />
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <h4>&nbsp;</h4>
                                        <button class="tf-view-btn" "View" id="btnView" runat="server" validationgroup="btnValidate" onserverclick="btnView_ServerClick" type="button"><i class="far fa-eye"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="cleafix">&nbsp;</div>
                            <div class="cleafix">&nbsp;</div>
                            <div>&nbsp;</div>
                            <div class="col-sm-12 ">
                                <div class="add-table-div gv-overflow-scrool">
                                    <%--<div id="headingDiv">
                                                 <h4>Testing</h4>
                                    </div>--%>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="gv" runat="server" CssClass="table table-bordered" ClientIDMode="Static" ShowHeaderWhenEmpty="true">
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                            </div>

                        </div>
                    </div>

                </section>
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>
            $(function () {
                $(".table").DataTable({
                    destroy: 'true',
                    dom: 'Bfrtip',
                    bSort: false,
                    searching: true,
                    lengthChange: true,
                    paging: false,
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: function () { return 'Accounts Leadger' }
                        }
                    ]
                });
                $('.buttons-excel span').html('<span "Excel Export"><i class="fa fa-file-excel-o icn2" aria-hidden="true"></i></span>');
                $('.buttons-pdf span').html('<span "PDF Export"><i class="fa fa-file-pdf-o icn2" aria-hidden="true"></i></span>');
                $('.buttons-print span').html('<span "Print"><i class="fa fa-print icn2" aria-hidden="true"></i></span>');
            });


        </script>
    </form>
    <style>
        .add-table-div {
            margin-bottom: 60px;
        }
    </style>

</body>

</html>
