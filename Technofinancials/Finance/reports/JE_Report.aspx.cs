﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.reports
{
    public partial class JE_Report : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void ddlbtnFnF_SelectedIndexChangedNew(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.PayrollDate = txtPayrollDate.Text;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                dt = objDB.GetFnFJEReport(ref errorMsg);
                gv.DataSource = dt;
                gv.DataBind();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;

            }
        }
        protected void ddlbtnMini_SelectedIndexChangedNew(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.PayrollDate = txtPayrollDate.Text;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                dt = objDB.GetMiniJEReport(ref errorMsg);
                gv.DataSource = dt;
                gv.DataBind();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;

            }
        }
        protected void ddlYear_SelectedIndexChangedNew(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.PayrollDate = txtPayrollDate.Text;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                dt = objDB.GetJEReport(ref errorMsg);
                gv.DataSource = dt;
                gv.DataBind();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;

            }
        }

    }
}