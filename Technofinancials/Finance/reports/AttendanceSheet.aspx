﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttendanceSheet.aspx.cs" Inherits="Technofinancials.Finance.reports.AttendanceSheet" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        td:nth-child(2) {
            text-align: left !important;
        }

        .content-header.second_heading .container-fluid {
            padding-left: 0px;
        }

        .content-header.second_heading h1 {
            margin-left: 0px !important;
        }

        table tbody td {
            text-align: left !important;
        }


        .tf-note-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-note-btn i {
                color: #fff !important;
            }

        .tf-disapproved-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-disapproved-btn i {
                color: #fff !important;
            }

        .tf-add-btn {
            background-color: #575757;
            padding: 12px 10px 8px 10px !important;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-add-btn i {
                color: #fff !important;
            }



        th.sorting {
            font-size: 12px !important;
            padding-left: 5px !important;
            padding-right: 5px !important;
        }

        td {
            text-align: center;
        }

        .offf {
            background: #ccc;
            color: #fff;
        }

        .color_infoP
        h3 {
            font-size: 16px;
            margin: 0 !important;
        }

            .color_infoP
            h3 span {
                margin-left: 15px;
            }

        .form-group.color_infoP {
            width: fit-content;
            display: inline-block;
            font-size: 16px;
        }

        button#btnView {
            padding: 4px 24px;
            margin-top: 3px;
        }
    </style>
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="UpdatePanel10"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Attendance Sheet</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right; display: none;">
                                    <button class="AD_btn" id="btnPDF" runat="server" onserverclick="btnPDF_ServerClick">PDF</button>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <section class="app-content">
                    <div class="row">

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group D_NONE">
                                        <h4>From </h4>
                                        <asp:DropDownList ID="ddlYear" runat="server" class="form-control select2 D_NONE" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtDate" runat="server" />
                                    </div>
                                    <div class="form-group">
                                        <h4>From Date<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator26" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtFromDate" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group D_NONE">
                                        <h4>To</h4>
                                        <asp:DropDownList ID="ddlMonth" runat="server" class="form-control select2 D_NONE" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--- Select Month ---</asp:ListItem>
                                            <asp:ListItem Value="1">January</asp:ListItem>
                                            <asp:ListItem Value="2">February</asp:ListItem>
                                            <asp:ListItem Value="3">March</asp:ListItem>
                                            <asp:ListItem Value="4">April</asp:ListItem>
                                            <asp:ListItem Value="5">May</asp:ListItem>
                                            <asp:ListItem Value="6">June</asp:ListItem>
                                            <asp:ListItem Value="7">July</asp:ListItem>
                                            <asp:ListItem Value="8">August</asp:ListItem>
                                            <asp:ListItem Value="9">September</asp:ListItem>
                                            <asp:ListItem Value="10">October</asp:ListItem>
                                            <asp:ListItem Value="11">November</asp:ListItem>
                                            <asp:ListItem Value="12">December</asp:ListItem>

                                        </asp:DropDownList>
                                        <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="Text1" runat="server" />
                                    </div>
                                    <div class="form-group ">
                                        <h4>To Date<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtToDate" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <h4>Grade </h4>
                                        <asp:DropDownList ID="ddlGrades" runat="server" CssClass="form-control select2">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-" style="display: none;">
                                    <div class="form-group">
                                        <h4>Shift</h4>
                                        <asp:DropDownList ID="ddlShifts" runat="server" class="form-control select2 ">
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee Name</h4>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2 ">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnValidate" onserverclick="btnView_ServerClick">View</button>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Department Name</h4>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" onchange="GetDesignationsByDepartmentIdAndCompanyId()" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Designation Name</h4>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" onchange="GetEmployeesByDesignationIdAndDepartmentId()" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Location</h4>
                                        <asp:DropDownList ID="ddlLocation" runat="server" class="form-control select2 ">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee Code</h4>
                                        <asp:TextBox ID="txtKTId" runat="server" class="form-control">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                    <ContentTemplate>
                                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group" id="divAlertMsg" runat="server">
                                                    <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                        <span>
                                                            <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                        </span>
                                                        <p id="pAlertMsg" runat="server">
                                                        </p>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnView" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-2">
                                    <h1 class="m-0 text-dark">Attendance</h1>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-10">
                                    <div class="attendance-wrapper">
                                        <div class="form-group color_infoP">
                                            <h3><span class="pres">P</span> - Present </h3>
                                        </div>
                                        <div class="form-group color_infoP">
                                            <h3><span class="abs">A</span> - Absent</h3>
                                        </div>
                                        <div class="form-group color_infoP">
                                            <h3><span class="sick">L</span> - Leave</h3>
                                        </div>
                                        <%--<div class="form-group color_infoP">
                                            <h3><span class="casual">C</span> - Casual Leave</h3>
                                        </div>--%>
                                        <div class="form-group color_infoP">
                                            <h3><span class="year">H</span> - Holiday</h3>
                                        </div>
                                        <div class="form-group color_infoP">
                                            <h3><span><b>O</b></span> - OFF</h3>
                                        </div>
                                        <div class="form-group color_infoP">
                                            <h3><span class="year"><b>OT</b></span> - Over Time</h3>
                                        </div>

                                        <%--  <div class="form-group color_infoP">
                                            <h3><span class="mater">M</span> - Maternal  Leave</h3>
                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <div class="row">
                        <%--<asp:UpdatePanel runat="server">
                            <ContentTemplate>--%>
                        <div class="col-sm-12 gv-overflow-scrool">
                            <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" ShowHeaderWhenEmpty="true" OnRowDataBound="gv_RowDataBound">
                            </asp:GridView>
                        </div>
                        <%--</ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>

                    <div class="row" style="display: none;">
                        <div class="col-md-12">
                            <div class="card-body table-responsive tbl-cont ">
                                <table class="table table-hover tbl-pagenation table-sm gv">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Employee Name</th>
                                            <th>Present </th>
                                            <th>Leave</th>
                                            <th>Absent </th>
                                            <th>1</th>
                                            <th>2</th>
                                            <th>3</th>
                                            <th>4</th>
                                            <th>5</th>
                                            <th class="offf">6</th>
                                            <th class="offf">7</th>
                                            <th>8</th>
                                            <th>9</th>
                                            <th>10</th>
                                            <th>11</th>
                                            <th>12</th>
                                            <th class="offf">13</th>
                                            <th class="offf">14</th>
                                            <th>15</th>
                                            <th>16</th>
                                            <th>17</th>
                                            <th>18</th>
                                            <th>19</th>
                                            <th class="offf">20</th>
                                            <th class="offf">21</th>
                                            <th>22</th>
                                            <th>23</th>
                                            <th>24</th>
                                            <th>25</th>
                                            <th>26</th>
                                            <th class="offf">27</th>
                                            <th class="offf">28</th>

                                            <th>29</th>
                                            <th>30</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td>1</td>
                                            <td>Muhammad Ahmed Shaikh</td>
                                            <td>22</td>
                                            <td>4</td>
                                            <td>0</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>

                                            <td class="casual">C</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>

                                            <td class="casual">C</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="sick">S</td>
                                            <td class="sick">S</td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P</td>

                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>

                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Emma Smith</td>
                                            <td>20</td>
                                            <td>2</td>
                                            <td>0 </td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>

                                            <td class="sick">S</td>
                                            <td class="sick">S</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>

                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                        </tr>
                                        <tr>
                                            <td>3
                                            </td>
                                            <td>Sophia WILLIAMS
                                            </td>
                                            <td>19
                                            </td>
                                            <td>3</td>
                                            <td>0
                                            </td>
                                            <td class="pres">P</td>
                                            <td class="casual">C</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P</td>
                                            <td class="pres">P
                                            </td>
                                            <td class="sick">S
                                            </td>
                                            <td class="sick">S
                                            </td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P</td>

                                        </tr>
                                        <tr>
                                            <td>4
                                            </td>
                                            <td>James Smith
                                            </td>
                                            <td>20
                                            </td>
                                            <td>1</td>
                                            <td>1
                                            </td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                            <td class="abs">A
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="casual">C
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>5
                                            </td>
                                            <td>Thomas Lee
                                            </td>
                                            <td>22
                                            </td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>

                                            <td class="pres">P</td>
                                            <td class="pres">P</td>


                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="pres">P
                                            </td>
                                            <td class="offf"></td>
                                            <td class="offf"></td>
                                            <td class="pres">P</td>
                                            <td class="pres">P</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>
        <script src="/business/scripts/ViewDesignations.js"></script>
        <script>
            var table = document.getElementById('gv');
            var tbody = table.getElementsByTagName('tbody')[0];
            var cells = tbody.getElementsByTagName('td');

            for (var i = 0, len = cells.length; i < len; i++) {
                if (cells[i].innerHTML == 'P') {
                    cells[i].className = 'pres';
                }
                else if (cells[i].innerHTML == 'A') {
                    cells[i].className = 'abs';
                }
                else if (cells[i].innerHTML == 'O' || cells[i].innerHTML == 'H') {
                    cells[i].className = 'offf';
                }
                else if (cells[i].innerHTML == 'L') {
                    cells[i].className = 'sick';
                }
                else if (cells[i].innerHTML == 'A') {
                    cells[i].className = 'sick';
                }
                else {
                    cells[i].className = 'not-marekd';
                }

            }

        </script>


    </form>
</body>
</html>
