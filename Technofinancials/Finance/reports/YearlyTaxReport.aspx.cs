using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.reports
{
    public partial class YearlyTaxReport : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false;
                    CheckSessions();
                    BindDropdown();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void BindDropdown()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDepartment.DataTextField = "DeptName";
            ddlDepartment.DataValueField = "DeptID";
            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

            ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
            ddlDesignation.DataTextField = "DesgTitle";
            ddlDesignation.DataValueField = "DesgID";
            ddlDesignation.DataBind();
            ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

            lstEmployees.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg);
            lstEmployees.DataTextField = "EmployeeName";
            lstEmployees.DataValueField = "EmployeeID";
            lstEmployees.DataBind();

            ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();

            ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
            ddlLocation.DataTextField = "NAME";
            ddlLocation.DataValueField = "NAME";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("ALL", "0"));

            ddlFinancialYear.DataSource = objDB.GetFinancialYearsList(ref errorMsg);
            ddlFinancialYear.DataTextField = "FYName";
            ddlFinancialYear.DataValueField = "FYID";
            ddlFinancialYear.DataBind();
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            IEnumerable<string> emps = from ListItem item in lstEmployees.Items
                                       where item.Selected
                                       select item.Value;

            Session["EmployeeID"] = emps.ToList();
            DownloadPDFReport();
        }

        private void DownloadPDFReport()
		{
            try
            {
                Session["FYID"] = ddlFinancialYear.SelectedValue;
                Session["DeptID"] = Convert.ToInt16(ddlDepartment.SelectedValue);
                Session["DesgID"] = Convert.ToInt16(ddlDesignation.SelectedValue);
                Session["WorkDayID"] = txtWDID.Text;
                Session["Location"] = ddlLocation.SelectedValue;
                //Session["EmployeeID"] = ddlEmployee.SelectedValue;

                //Response.Redirect("~/finance/Reports/ReportForm.aspx");
                Server.Transfer("~/finance/Reports/ReportForm.aspx", true);
            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}