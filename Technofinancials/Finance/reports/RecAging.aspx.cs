﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.reports
{
    public partial class RecAging : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                GetData();
                BindDropdown();

            }
        }



        private void BindDropdown()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlClient.DataSource = objDB.GetAllApprovedClients(ref errorMsg);
            ddlClient.DataTextField = "ClientName";
            ddlClient.DataValueField = "ClientID";
            ddlClient.DataBind();
            ddlClient.Items.Insert(0, new ListItem("--- Select Client ---", "0"));
            ddlClient.SelectedIndex = 0;
        }


        private void GetData()
        {
            CheckSessions();

            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetReceivableAgingByCompanyID(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (gv != null)
            {
                if (gv.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                dt = objDB.GetReceivableAgingByCompanyID(ref errorMsg);
                gv.DataSource = Common.filterTable2(dt, ddlClient.SelectedValue,"ClientID");
                gv.DataBind();
                if (gv != null)
                {
                    if (gv.Rows.Count > 0)
                    {
                        divAlertMsg.Visible = false;
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gv.DataSource = null;
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                }
                else
                {
                    gv.DataSource = null;
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
            Common.addlog("Report", "Finance", "Rec Aging Report Viewed", "");
        }
        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
            {
                Response.Redirect("/login");
                return false;
            }
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
            {
                Response.Redirect("/login");
                return false;
            }
            return true;
        }


        protected void btnReport_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Session["ReportTitle"] = "Receivable Aging";
                Session["ReportDataTable"] = Common.GetTemplate(gv);
                Session["BackbtnLink"] = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/receivable-aging/view";
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/finance-report/generate");

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
}