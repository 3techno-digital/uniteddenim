﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JE_Report.aspx.cs" Inherits="Technofinancials.Finance.reports.JE_Report" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="~/Scripts/jquery-ui-1.12.1.min.js"></script>
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
     <style>
        .AD_btn_inn {
            padding: 3px 24px;
        }

        .tf-note-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-note-btn i {
                color: #fff !important;
            }

        .tf-disapproved-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-disapproved-btn i {
                color: #fff !important;
            }


        .tf-add-btn {
            background-color: #575757;
            padding: 4px 9px 4px 10px !important;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-add-btn i {
                color: #fff !important;
            }

        .tf-add-btn {
            background-color: #575757;
            padding: 12px 10px 8px 10px !important;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-add-btn i {
                color: #fff !important;
            }

        .tf-back-btn {
            background-color: #575757;
            padding: 10px 10px 10px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
            padding: 5px !important;
            height: 38px !important;
            width: 37px !important;
            display: inline-block !important;
            text-align: center !important;
        }

/*        div#gv_wrapper div#gv_filter{
            overflow-x: hidden !important;
            padding: 0px !important;
            display:none;
        }*/
        div#gv_wrapper .dt-buttons , div#gv_wrapper div#gv_info , div#gv_wrapper div#gv_paginate{
            padding: 0px !important;
        }
        .tf-back-btn i {
            color: #fff !important;
        }



            div#gv_wrapper::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            div#gv_wrapper::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }
table#gv {   
    width: 3000px;
}
.table-bordered > thead > tr > th{
    font-size:11px;
}
            div#gv_wrapper::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

        @media only screen and (max-width: 992px) and (min-width: 768px) {
            div#gv_wrapper{
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 5rem;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 993px) {
            div#gv_wrapper{
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 5rem;
            }
        }

        @media only screen and (max-width: 1280px) and (min-width: 800px) {
            div#gv_wrapper{
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 5rem;
            }
        }
    </style>
</head>

                
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">

            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">JE Report</h1>
                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-12 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                       <h4>Month
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtPayrollDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>

                                <%-- <input name="txtPayrollDate" type="text" id="txtPayrollDate" class="form-control" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />--%>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
                                    </div>
                                </div>

                                <div class="col-lg-6">
                            <%--<button class="AD_btn" "" type="button" runat="server" onclik="ShowReport();" data-original-"">View</button>--%>
                            <button class="AD_btn_inn" style="margin-top: 32px !important;" id="btnView" runat="server" validationgroup="btnView" onserverclick="ddlYear_SelectedIndexChangedNew" type="button">Standard</button>
                        <button class="AD_btn_inn" style="margin-top: 32px !important;" id="btnMini" runat="server" validationgroup="btnView" onserverclick="ddlbtnMini_SelectedIndexChangedNew" type="button">MiniPayroll</button>
                                        <button class="AD_btn_inn" style="margin-top: 32px !important;" id="btnFnF" runat="server" validationgroup="btnView" onserverclick="ddlbtnFnF_SelectedIndexChangedNew" type="button">FnF</button>
                                </div>
                            </div>
                          

                        </div>
                    </div>

              <div class="col-lg-2 col-md-4 col-sm-12">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

             <div class="row">
                       <div class="col-sm-12">
                            <div class="tab-content ">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                     <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv dataTable no-footer" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>

                                            <asp:TemplateField HeaderText="Entry No">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("entryno") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                              <asp:TemplateField HeaderText="Subsidiary">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("subsdiary") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                               
                                                 <asp:TemplateField HeaderText="Currency">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("currency") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Exchange Rate">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("exchangerate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                  <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("date") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                              <asp:TemplateField HeaderText="Account">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("description") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Debit">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("DR","{0:N}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Credit">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("CR","{0:N}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Memo">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("memo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Department">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("PlaceOfPost") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                              <asp:TemplateField HeaderText="Division">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("division") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    
                                            
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                       </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <!-- Modal -->
    </form>

</body>
</html>