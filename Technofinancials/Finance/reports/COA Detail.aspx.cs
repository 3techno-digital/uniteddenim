﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.reports
{
    public partial class COA_Detail : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";


        private DataTable dtCOA
        {
            get
            {
                if (ViewState["dtCOA"] != null)
                {
                    return (DataTable)ViewState["dtCOA"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtCOA"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                GetData();
                // BindDropdown();

            }
        }




        private void GetData()
        {
            CheckSessions();

            dtCOA = new DataTable();
            dtCOA.Columns.Add("ID");
            dtCOA.Columns.Add("ParentID");
            dtCOA.Columns.Add("Detail");
            dtCOA.Columns.Add("Debit");
            dtCOA.Columns.Add("Credit");
            dtCOA.Columns.Add("TransactionDate");
            dtCOA.Columns.Add("Balance");
            dtCOA.AcceptChanges();

            DataTable dtCat = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            dtCat = objDB.GetAllCOAs(ref errorMsg);
            int id = 1;
            int Catid = 0;

            if (dtCat != null)
            {
                for (int i = 0; i < dtCat.Rows.Count; i++)
                {
                        Catid = id;
                        dtCOA.Rows.Add(new object[] { id++, Catid, dtCat.Rows[i]["GL_CODE"].ToString()+" - "+dtCat.Rows[i]["SubAccount"].ToString(), "", "","", dtCat.Rows[i]["CurrentBalance"].ToString() });
                        dtCOA.AcceptChanges();
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                        objDB.COA_ID = Convert.ToInt32(dtCat.Rows[i]["COA_ID"].ToString());
                        DataTable dtSubCat = objDB.GetCOADetail(ref errorMsg);
                        if (dtSubCat != null)
                        {
                            for (int j = 0; j < dtSubCat.Rows.Count; j++)
                            {
                                
                                dtCOA.Rows.Add(new object[] { id++, Catid, dtSubCat.Rows[j]["TransactionDetail"], dtSubCat.Rows[j]["Debit"], dtSubCat.Rows[j]["Credit"], dtSubCat.Rows[j]["TransactionDate"], dtSubCat.Rows[j]["CurrentBalance"] });
                                dtCOA.AcceptChanges();

                            }
                        }

                        
                    
                }

                
            }


            gv.DataSource = dtCOA;
            gv.DataBind();
            if (gv != null)
            {
                if (gv.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            //try
            //{
            //    CheckSessions();
            //    DataTable dt = new DataTable();
            //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            //    dt = objDB.GetCOACreditDebitReport(ref errorMsg);
            //    gv.DataSource = Common.filterTable2(dt, ddlSupplier.SelectedValue);
            //    gv.DataBind();
            //    if (gv != null)
            //    {
            //        if (gv.Rows.Count > 0)
            //        {
            //            divAlertMsg.Visible = false;
            //            gv.UseAccessibleHeader = true;
            //            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            //        }
            //        else
            //        {
            //            gv.DataSource = null;
            //            divAlertMsg.Visible = true;
            //            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
            //            pAlertMsg.InnerHtml = "No Record Found";
            //        }
            //    }
            //    else
            //    {
            //        gv.DataSource = null;
            //        divAlertMsg.Visible = true;
            //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
            //        pAlertMsg.InnerHtml = "No Record Found";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    divAlertMsg.Visible = true;
            //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
            //    pAlertMsg.InnerHtml = ex.Message;
            //}
            //Common.addlog("Report", "Finance", "Chart Of Account Debit/Credit Report", "");
        }
        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

            return true;
        }


        protected void btnReport_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Session["ReportTitle"] = "COA Detail";
                Session["ReportDataTable"] = Common.GetTemplate(gv);
                Session["BackbtnLink"] = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/coa-detail/view";
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/finance-report/generate");
                //Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/balance-sheet/generate");

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void gv_PreRender(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gv.Rows)
            {
                if (dtCOA.Rows[row.RowIndex]["ParentID"].ToString() == dtCOA.Rows[row.RowIndex]["ID"].ToString())
                {
                    row.Attributes["class"] = "treegrid-" + dtCOA.Rows[row.RowIndex]["ID"];
                }
                else
                {
                    row.Attributes["class"] = "treegrid-" + dtCOA.Rows[row.RowIndex]["ID"] + " treegrid-parent-" + dtCOA.Rows[row.RowIndex]["ParentID"];
                }

            }
        }
    }
}