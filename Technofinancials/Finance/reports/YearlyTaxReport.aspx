﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="YearlyTaxReport.aspx.cs" Inherits="Technofinancials.Finance.reports.YearlyTaxReport" %>


<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=9">
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>
		div#gv {
			margin-bottom: 20%;
		}

		.content-header.second_heading .container-fluid {
			padding-left: 0px;
		}

		.content-header.second_heading h1 {
			margin-left: 0px !important;
		}

		button#btnView {
			padding: 4px 24px;
			margin-top: 15px;
		}
		button.multiselect.dropdown-toggle.custom-select.text-center span.multiselect-selected-text {
			white-space: nowrap;
		}

		.open > .dropdown-menu {
			display: block;
			width: 300px !important
		}

			.open > .dropdown-menu ::-webkit-scrollbar {
				width: 10px;
			}

		.dropdown-menu {
			box-shadow: none !important;
		}

		.multiselect-container.dropdown-menu {
			overflow: hidden;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center {
			width: 200px;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center {
			background-color: #fff;
			border: 1px solid #aaa;
			border-radius: 5px;
			padding-left: 8px;
			padding-right: 8px;
			padding: 3px 37px;
		}

		.open > .dropdown-menu {
			padding: 15px;
			max-height: 250px !important;
			border: 1px solid #aaa;
			border-bottom-left-radius: 5px;
			border-bottom-right-radius: 5px;
		}

		multiselect-filter {
			margin-bottom: 15px;
		}

		.multiselect-container.dropdown-menu {
			overflow: overlay !important;
		}

		.multiselect-container .multiselect-all .form-check, .multiselect-container .multiselect-group .form-check, .multiselect-container .multiselect-option .form-check {
			padding: 0px;
		}

		@media only screen and (max-width: 1280px) and (min-width: 800px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 25px !important;
			}
		}

		@media only screen and (max-width: 1440px) and (min-width: 900px) {

			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 0px !important;
				width: 170px;
			}
		}

		@media only screen and (max-width: 1024px) and (min-width: 993px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 15px !important;
				width: 130px;
			}
		}

		@media only screen and (max-width: 992px) and (min-width: 768px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 50px;
				width: 80px;
			}
		}

		@media only screen and (max-width: 1599px) and (min-width: 1201px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 37px;
			}
		}

		.multiselect-container.dropdown-menu {
			overflow: hidden auto !important;
		}

		.open > .dropdown-menu {
			max-height: 300px !important;
		}

	</style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">
			<div class="wrap">
				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
								<h1 class="m-0 text-dark">Yearly Tax Report</h1>
							</div>
							<!-- /.col -->

						</div>
						<!-- /.row -->
					</div>
					<!-- /.container-fluid -->
				</div>
				<section class="app-content">
					<div class="row">

						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Financial Year
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="ddlFinancialYear" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
										<asp:DropDownList ID="ddlFinancialYear" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6" style="display: none">
									<div class="form-group">
										<h4>Employee Name</h4>
										<asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2" data-plugin="select2">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee Name<span style="color: red !important;">*</span>
										<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="lstEmployees" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red" /></h4>
                                        <asp:ListBox SelectionMode="Multiple" ID="lstEmployees" runat="server"></asp:ListBox>
                                    </div>
                                </div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Location</h4>
										<asp:DropDownList ID="ddlLocation" runat="server" class="form-control select2 ">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Working Day ID</h4>
										<asp:TextBox ID="txtWDID" runat="server" class="form-control" type="number">
										</asp:TextBox>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">

								<div class="col-sm-6">
									<div class="form-group">
										<h4>Department Name</h4>
										<asp:DropDownList ID="ddlDepartment" runat="server" onchange="GetDesignationsByDepartmentIdAndCompanyId()" class="form-control select2" data-plugin="select2" AutoPostBack="false">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Designation Name</h4>
										<asp:DropDownList ID="ddlDesignation" runat="server" onchange="GetEmployeesByDesignationIdAndDepartmentId()" class="form-control select2" data-plugin="select2" AutoPostBack="false">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="clear-fix">&nbsp;</div>
									<div>
										<button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">
											View                                            
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="col-sm-12">
								<asp:UpdatePanel ID="UpdatePanel4" runat="server">
									<ContentTemplate>
										<div class="form-group" id="divAlertMsg" runat="server">
											<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
												<span>
													<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
												</span>
												<p id="pAlertMsg" runat="server">
												</p>
											</div>
										</div>
									</ContentTemplate>
								</asp:UpdatePanel>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">


							<div class="row">
								<div class="col-sm-12 gv-overflow-scrool">

									<rsweb:ReportViewer ID="ReportViewer1" runat="server" BackColor="" ClientIDMode="Static" Height="1000px" HighlightBackgroundColor="" InternalBorderColor="204, 204, 204" InternalBorderStyle="Solid" InternalBorderWidth="1px" LinkActiveColor="" LinkActiveHoverColor="" LinkDisabledColor="" PrimaryButtonBackgroundColor="" PrimaryButtonForegroundColor="" PrimaryButtonHoverBackgroundColor="" PrimaryButtonHoverForegroundColor="" SecondaryButtonBackgroundColor="" SecondaryButtonForegroundColor="" SecondaryButtonHoverBackgroundColor="" SecondaryButtonHoverForegroundColor="" SplitterBackColor="" ToolbarDividerColor="" ToolbarForegroundColor="" ToolbarForegroundDisabledColor="" ToolbarHoverBackgroundColor="" ToolbarHoverForegroundColor="" ToolBarItemBorderColor="" ToolBarItemBorderStyle="Solid" ToolBarItemBorderWidth="1px" ToolBarItemHoverBackColor="" ToolBarItemPressedBorderColor="51, 102, 153" ToolBarItemPressedBorderStyle="Solid" ToolBarItemPressedBorderWidth="1px" ToolBarItemPressedHoverBackColor="153, 187, 226" Width="1280px" Style="margin-bottom: 29px" Visible="False">
									</rsweb:ReportViewer>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- #dash-content -->
			</div>
			<div class="clearfix">&nbsp;</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>

		<script src="/business/scripts/ViewDesignations.js"></script>
		<script>
			$(function () {
				$('[id*=lstEmployees]').multiselect({
					includeSelectAllOption: true,
					buttonContainer: '<div class="btn-group" />',
					includeSelectAllIfMoreThan: 0,
					enableFiltering: true,
					filterPlaceholder: 'Search',
					filterBehavior: 'text',
					includeFilterClearBtn: true,
					enableCaseInsensitiveFiltering: true,
					numberDisplayed: 1,
					maxHeight: true,
					maxHeight: 350,
					templates: {
						button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span></button>',
						Container: '<div class="multiselect-container dropdown-menu"></div>',
						filter: '<div class="multiselect-filter"><div class="input-group input-group-sm p-1"><div class="input-group-prepend"></div><input class="form-control multiselect-search" type="text" /></div></div>',

						filterClearBtn: '<div class="input-group-append"><button class="multiselect-clear-filter input-group-text" type="button"><i class="fas fa-times"></i></button></div>',

						option: '<button class="multiselect-option dropdown-item"></button>',

						divider: '<div class="dropdown-divider"></div>',

						optionGroup: '<button class="multiselect-group dropdown-item"></button>',

						resetButton: '<div class="multiselect-reset text-center p-2"><button class="btn btn-sm btn-block btn-outline-secondary"></button></div>'
					}
				});
			});

		</script>

	</form>
</body>
</html>
