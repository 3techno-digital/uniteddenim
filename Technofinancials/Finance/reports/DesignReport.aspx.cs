﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.reports
{
    public partial class DesignReport : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        public string pageType
        {
            get
            {
                if (ViewState["pageType"] != null)
                {
                    return (string)ViewState["pageType"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["pageType"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                btnFinanceReport.Visible = false;
                btnAssetsListReport.Visible = false;
                btnExpenseVoucher.Visible = false;
                btnInvoice.Visible = false;

                divAlertMsg.Visible = false;


                if (HttpContext.Current.Items["PageType"] != null)
                {
                    pageType = HttpContext.Current.Items["PageType"].ToString();
                    pageHeading.InnerHtml = pageType;

                    if (pageType == "Finance Report")
                    {
                        imgHeading.Src = "/assets/images/Offer Letter Design.png";
                        btnFinanceReport.Visible = true;
                    }
                    else if (pageType == "Assets List")
                    {
                        imgHeading.Src = "/assets/images/asset-list.png";
                        btnAssetsListReport.Visible = true;
                    }
                    else if (pageType == "Expence Voucher")
                    {
                        imgHeading.Src = "/assets/images/asset-list.png";
                        btnExpenseVoucher.Visible = true;
                    }
                    else if (pageType == "Invoice")                              
                    {
                        imgHeading.Src = "/assets/images/asset-list.png";
                        btnInvoice.Visible = true;
                    }


                    getDocumentDesign();
                }
            }
        }

        private void getDocumentDesign()
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.DocType = pageType;
            dt = objDB.GetDocumentDesign(ref errorMsg);
            if (dt == null)
            {
                objDB.CompanyID = 2;
                objDB.DocType = pageType;
                dt = objDB.GetDocumentDesign(ref errorMsg);
            }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                    txtContent.Content = dt.Rows[0]["DocContent"].ToString();
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                }
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            string res = "";
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.DocType = pageType;
            objDB.DocHeader = txtHeader.Content;
            objDB.DocContent = txtContent.Content;
            objDB.DocFooter = txtFooter.Content;
            objDB.CreatedBy = Session["UserName"].ToString();
            res = objDB.AddDocumentDesgin();

            if (res == "Draft Design Updated")
            {
                res = "Documents Updated Successfully";
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }

    }
}