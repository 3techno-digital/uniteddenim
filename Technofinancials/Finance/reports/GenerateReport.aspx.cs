﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Text;
using SelectPdf;
using System.Globalization;


namespace Technofinancials.Finance.reports
{
    public partial class GenerateReport : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        public string DocName
        {
            get
            {
                if (ViewState["DocName"] != null)
                {
                    return (string)ViewState["DocName"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocName"] = value;
            }
        }

        public int PrimaryValue
        {
            get
            {
                if (ViewState["PrimaryValue"] != null)
                {
                    return (int)ViewState["PrimaryValue"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrimaryValue"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/dashboard";

                if (HttpContext.Current.Items["PrimaryValue"] != null && HttpContext.Current.Items["PageType"] != null)
                {
                    PrimaryValue = Convert.ToInt32(HttpContext.Current.Items["PrimaryValue"].ToString());
                    DocName = HttpContext.Current.Items["PageType"].ToString();
                    getReport(DocName, PrimaryValue);

                    if (DocName == "All Voucher")
                    {
                        reportsPageHeading.InnerHtml = "Voucher";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/dashboard";

                    }
                    if (DocName == "Expense Voucher")
                    {
                        reportsPageHeading.InnerHtml = "Expense Voucher";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/expenses";

                    }
                    else if (DocName == "Journal Voucher")
                    {
                        reportsPageHeading.InnerHtml = "Journal Voucher";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/purchase-orders/edit-purchase-order-" + PrimaryValue;
                    }

                    else if (DocName == "Expense Journal Voucher")
                    {
                        reportsPageHeading.InnerHtml = "Journal Voucher";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/expenses/edit-expense-" + PrimaryValue;
                    }

                    else if (DocName == "GRN Journal Voucher")
                    {
                        reportsPageHeading.InnerHtml = "Journal Voucher";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/goods-receive-notes/edit-goods-receive-notes-" + PrimaryValue;
                    }

                    else if (DocName == "Invoice Journal Voucher")
                    {
                        reportsPageHeading.InnerHtml = "Journal Voucher";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/invoice/edit-invoice-" + PrimaryValue;
                    }

                    else if (DocName == "Invoice")
                    {
                        reportsPageHeading.InnerHtml = "Invoice";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/invoice";
                    }

                    else if (DocName == "Payment Voucher")
                    {
                        reportsPageHeading.InnerHtml = "Payment Voucher";
                        if (Session["BackLink"] != null)
                        {
                            btnBack.HRef = Session["BackLink"].ToString();
                        }

                    }
                    else if (DocName == "Asset voucher")
                    {
                        reportsPageHeading.InnerHtml = "Asset Depreciation voucher";
                        if (Session["BackLink"] != null)
                        {
                            btnBack.HRef = Session["BackLink"].ToString();
                        }

                    }
                }
                else if (Session["ReportTitle"] != null && Session["ReportDataTable"] != null && Session["BackbtnLink"] != null && HttpContext.Current.Items["PageType"] != null)
                {
                    string s = @"<style>
.table{
margin: 0px auto;
}

.table tr:nth-last-child(odd) td {
                     background: #edf0f5;
color:#000;
border:1px solid white;
font-size: 12px;
padding: 5px 0px 4px 1px;
}

.table tr:nth-last-child(even) td {
  background: #fff;
color:#000;
border:1px solid white;
font-size: 12px;
padding: 5px 0px 4px 1px;
}

.table tr:first-child th {
  background: #188ae2;
  color:#fff;
font-size: 14px;
  font-weight:bold;
border:1px solid white;
}



        </style>";
                    getReport(HttpContext.Current.Items["PageType"].ToString(), s + Session["ReportDataTable"].ToString(), Session["ReportTitle"].ToString());
                    reportsPageHeading.InnerHtml = Session["ReportTitle"].ToString() + " Report";
                    btnBack.HRef = Session["BackbtnLink"].ToString();
                }
            }
        }

        private void getReport(string PageType, string ReportDT, string Title)
        {
            DataTable dt = new DataTable();
            objDB.DocType = PageType;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            dt = objDB.GetDocumentDesign(ref errorMsg);

            if (dt == null)
            {
                objDB.CompanyID = 2;
                objDB.DocType = PageType;
                dt = objDB.GetDocumentDesign(ref errorMsg);
            }

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                    string content = dt.Rows[0]["DocContent"].ToString();
                    content = content.Replace("##TITLE##", Title);

                    content = content.Replace("##TABLE##", ReportDT);
                    txtContent.Content = content;
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                }
            }
        }

        private void getReport(string PageType, int ID)
        {
            DataTable dt = new DataTable();
            string content = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.DocType = PageType;
            dt = objDB.GetDocumentDesign(ref errorMsg);
            if (dt == null)
            {
                objDB.CompanyID = 2;
                objDB.DocType = PageType;
                dt = objDB.GetDocumentDesign(ref errorMsg);

            }


            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    content = dt.Rows[0]["DocHeader"].ToString();


                    if (Session["CompanyName"].ToString() != null)
                    {
                        content = content.Replace("##COMPANY_NAME##", Session["CompanyName"].ToString());

                    }
                    txtHeader.Content = content;
                    content = dt.Rows[0]["DocContent"].ToString();
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                }
            }

            if (PageType == "Expense Voucher")
            {
                DataTable Contentdt = new DataTable();
                objDB.ExpencesID = PrimaryValue;
                Contentdt = objDB.GetExpensesByID(ref errorMsg);
                if (Contentdt != null)
                {
                    if (Contentdt.Rows.Count > 0)
                    {
                        //                        content = @"
                        //<div style=""display:block;width:100%;"">
                        //<div style=""display:inline-block;width:49.5%;text-align:left;"">
                        //<p><b>Date:</b> <span>##DATE##</span></p>
                        //</div>
                        //<div style=""display:inline-block;width:49.5%;text-align:left;"">
                        //<p style=""text-align:right;""><b>Voucher No:</b> <span>##VOUCHER_NO##</span></p>
                        //</div>

                        //</div>
                        //<br />

                        //<div style=""display:block;width:80%; margin: 0px auto;"">
                        //<div style=""display:inline-block;width:33%;text-align:left;"">
                        //<p><b>Title:</b> <span>##TITLE##</span></p>
                        //</div>
                        //<div style=""display:inline-block;width:33%;text-align:left;"">
                        //<p><b>Amount:</b> <span>##AMOUNT##</span></p>
                        //</div>
                        //<div style=""display:inline-block;width:32%;text-align:left;"">
                        //<p><b>COA:</b> <span>##COA##</span></p>
                        //</div>
                        //</div>
                        //<br />


                        //<div style=""display:block;width:80%; margin: 0px auto;"">
                        //<div style=""display:inline-block;width:33%;text-align:left;"">
                        //<p><b>Spent At:</b> <span>##SPENT_AT##</span></p>
                        //</div>


                        //<div style=""display:inline-block;width:33%;text-align:left;"">
                        //<p><b>Voucher Type:</b> <span>##VOUCHER_TYPE##</span></p>
                        //</div>

                        //</div>
                        //<br/>

                        //<div style=""display:block;width:80%; margin: 0px auto;"">


                        //##ITEMS##


                        //</div>
                        //<br />

                        //<br />

                        //<div style=""display:block;width:80%; margin: 0px auto;"">
                        //<div style=""display:inline-block;width:33%;text-align:left;"">
                        //<p><b>Prepared By:</b> <span>##PREPARED_BY##</span></p>
                        //</div>
                        //<div style=""display:inline-block;width:33%;text-align:left;"">
                        //<p><b>Reviewed By:</b> <span>##REVIEWED_BY##</span></p>
                        //</div>
                        //<div style=""display:inline-block;width:32%;text-align:left;"">
                        //<p><b>Approved By:</b> <span>##APPROVED_BY##</span></p>
                        //</div>
                        //</div>

                        //";

                        content = content.Replace("##DATE##", DateTime.Parse(Contentdt.Rows[0]["ExpenseDate"].ToString()).ToShortDateString());
                        content = content.Replace("##TITLE##", Contentdt.Rows[0]["Title"].ToString());
                        content = content.Replace("##AMOUNT##", Contentdt.Rows[0]["TotalBalance"].ToString());
                        content = content.Replace("##COA##", Contentdt.Rows[0]["SubAccount"].ToString());
                        content = content.Replace("##SPENT_AT##", Contentdt.Rows[0]["SupplierName"].ToString());
                        content = content.Replace("##VOUCHER_NO##", Contentdt.Rows[0]["VoucherNo"].ToString());
                        content = content.Replace("##VOUCHER_TYPE##", Contentdt.Rows[0]["NatureOFVoucher"].ToString());
                        content = content.Replace("##PREPARED_BY##", Contentdt.Rows[0]["CreatedBy"].ToString());
                        content = content.Replace("##REVIEWED_BY##", Contentdt.Rows[0]["ReviewedBy"].ToString());
                        content = content.Replace("##APPROVED_BY##", Contentdt.Rows[0]["ApprovedBy"].ToString());
                        content = content.Replace("##CHEQUE_NO##", Contentdt.Rows[0]["ChequeNo"].ToString());
                        content = content.Replace("##REF_NO##", Contentdt.Rows[0]["refNo"].ToString());
                        content = content.Replace("##CHEQUE_DATE##", Contentdt.Rows[0]["chequeDate"].ToString());
                        content = content.Replace("##PAID_TO##", Contentdt.Rows[0]["paidTo"].ToString());
                        content = content.Replace("##POSTING_DATE##", Contentdt.Rows[0]["ExpenseDate"].ToString());
                        content = content.Replace("##DESCRIPTION##", Contentdt.Rows[0]["Description"].ToString());

                        DataTable subTable = new DataTable();
                        objDB.ExpencesID = PrimaryValue;
                        content = content.Replace("##ITEM_TABLE##", objDB.GenerateExpenseItemTable());



                        //content = content.Replace("##NOTES##", Contentdt.Rows[0]["Notes"].ToString());

                        //         objDB.DocID = PrimaryValue;
                        //         objDB.DocType = "PurchasePlan";
                        //         content = content.Replace("##NOTES##", objDB.GetDocNotes());
                    }
                }

                txtContent.Content = content;
            }


            else if (PageType == "Journal Voucher")
            {
                objDB.POID = ID;
                DataTable POdt = objDB.GetPOsByID(ref errorMsg);
                if (POdt != null)
                {
                    content = content.Replace("##DATE##", DateTime.Parse(POdt.Rows[0]["ApprovedDate"].ToString()).ToShortDateString());
                    content = content.Replace("##VOUCHER_NO##", POdt.Rows[0]["POCode"].ToString());
                    content = content.Replace("##SERIAL_NO##", POdt.Rows[0]["JVSerialNo"].ToString());
                    string table = "";
                    table += "<table class='item-table' style = 'width:100%;'>";
                    table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";
                    double debitNetAmount = 0;
                    double creditNetAmount = 0;
                    if (POdt.Rows.Count > 0)
                    {
                        DataTable dtPOItems = new DataTable();
                        objDB.POID = ID;
                        dtPOItems = objDB.GetPODebitForVoucher(ref errorMsg);
                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {
                                    table += "<tr><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                    debitNetAmount += double.Parse(dtPOItems.Rows[i]["Debit"].ToString());
                                }

                            }
                        }
                        dtPOItems = new DataTable();
                        objDB.POID = ID;
                        dtPOItems = objDB.GetPOCreditForVoucher(ref errorMsg);
                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {

                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {
                                    table += "<tr><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                    creditNetAmount += double.Parse(dtPOItems.Rows[i]["Credit"].ToString());
                                }

                            }
                        }
                    }

                    table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + debitNetAmount + "</td><td>" + creditNetAmount + "</td></tr>";


                    table += "</table>";
                    content = content.Replace("##DETAIL_TABLE##", table);
                    txtContent.Content = content; ;
                }

            }



            else if (PageType == "All Vouchers" || PageType == "GRN Journal Voucher")
            {
                if (ID != 0)
                {
                    Common.PrintVoucher(ID);
                }

                //objDB.DirectPaymentID = ID;
                //DataTable dtVoucher = objDB.GetDirectPaymentDetailByDirectPaymentID(ref errorMsg);
                //double totAmount = 0;

                //if (dtVoucher != null && dtVoucher.Rows.Count > 0)
                //{

                //    content = content.Replace("##DATE##", dtVoucher.Rows[0]["Date"].ToString());
                //    content = content.Replace("##VOUCHER_NO##", dtVoucher.Rows[0]["VoucherNo"].ToString());
                //    content = content.Replace("##SERIAL_NO##", dtVoucher.Rows[0]["SerialNo"].ToString());
                //    txtHeader.Content = txtHeader.Content.Replace("##LOCATION_NAME##", dtVoucher.Rows[0]["LocationName"].ToString());
                //    txtHeader.Content = txtHeader.Content.Replace("##VOUCHER_TYPE##", Common.getCompleteVoucherName(dtVoucher.Rows[0]["VoucherType"].ToString()));
                //    content = content.Replace("##VOUCHER_TYPE##", Common.getCompleteVoucherName(dtVoucher.Rows[0]["VoucherType"].ToString()));
                //    string table = "";
                //    table += "<table class='item-table' style = 'width:100%; border-collapse: collapse;'>";
                //    table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";
                //    for (int i = 0; i < dtVoucher.Rows.Count; i++)
                //    {
                //        table += "<tr><td>" + dtVoucher.Rows[i]["COA_NO"].ToString() + "</td><td>" + dtVoucher.Rows[i]["COA_Title"].ToString() + "</td><td>" + dtVoucher.Rows[i]["Instrument"].ToString() + "</td><td>" + dtVoucher.Rows[i]["TransactionDetail"].ToString() + "</td><td>" + ((dtVoucher.Rows[i]["Debit"].ToString() == "0.00" || dtVoucher.Rows[i]["Debit"].ToString() == "0") ? "" : dtVoucher.Rows[i]["Debit"].ToString()) + "</td><td>" + ((dtVoucher.Rows[i]["Credit"].ToString() == "0.00" || dtVoucher.Rows[i]["Credit"].ToString() == "0")?"": dtVoucher.Rows[i]["Credit"].ToString()) + "</td></tr>";
                //        totAmount += double.Parse(dtVoucher.Rows[i]["Debit"].ToString(), NumberStyles.Currency);
                //    }

                //    table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + string.Format("{0:n2}", totAmount) + "</td><td>" + string.Format("{0:n2}", totAmount) + "</td></tr>";
                //    table += "</table>";
                //    table += "<p>"+Common.NumberToWords(totAmount)+"</p>";
                //    content = content.Replace("##DETAIL_TABLE##", table);
                //    txtContent.Content = content; ;
                //}

            }

            else if (PageType == "GRN Journal Voucher")
            {
                objDB.GRNID = ID;
                DataTable POdt = objDB.GetGRNsByIDFinance(ref errorMsg);
                if (POdt != null)
                {
                    content = content.Replace("##DATE##", DateTime.Parse(POdt.Rows[0]["GRNDate"].ToString()).ToShortDateString());
                    content = content.Replace("##VOUCHER_NO##", POdt.Rows[0]["GRNCode"].ToString());
                    content = content.Replace("##SERIAL_NO##", POdt.Rows[0]["JVSerialNo"].ToString());
                    string table = "";
                    table += "<table class='item-table' style = 'width:100%;'>";
                    table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";
                    if (POdt.Rows.Count > 0)
                    {
                        DataTable dtPOItems = new DataTable();
                        objDB.GRNID = ID;
                        dtPOItems = objDB.GetGRNDebitForVoucher(ref errorMsg);
                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {


                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {

                                    table += "<tr><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                }

                            }
                        }
                        dtPOItems = new DataTable();
                        objDB.GRNID = ID;
                        dtPOItems = objDB.GetGRNCreditForVoucher(ref errorMsg);
                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {

                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {

                                    table += "<tr><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                }

                            }
                        }
                    }

                    table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + POdt.Rows[0]["POAmount"] + "</td><td>" + POdt.Rows[0]["POAmount"] + "</td></tr>";


                    table += "</table>";
                    content = content.Replace("##DETAIL_TABLE##", table);
                    txtContent.Content = content; ;
                }

            }


            else if (PageType == "Depreciation Journal Voucher")
            {
                string table = "";
                table += "<table class='item-table' style = 'width:100%;'>";
                table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";

                DataTable dtPOItems = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                dtPOItems = objDB.GetDepreciationDebit(ref errorMsg);

                double totCredit = 0, totDebit = 0;

                if (dtPOItems != null)
                {
                    if (dtPOItems.Rows.Count > 0)
                    {


                        for (int i = 0; i < dtPOItems.Rows.Count; i++)
                        {
                            totDebit += Convert.ToDouble(dtPOItems.Rows[i]["Debit"].ToString());
                            table += "<tr><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                        }

                    }
                }
                dtPOItems = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                dtPOItems = objDB.GetDepreciationCredit(ref errorMsg);
                if (dtPOItems != null)
                {
                    if (dtPOItems.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtPOItems.Rows.Count; i++)
                        {
                            totCredit += Convert.ToDouble(dtPOItems.Rows[i]["Credit"].ToString());
                            table += "<tr><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                        }

                    }
                }

                table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + totDebit.ToString() + "</td><td>" + totCredit.ToString() + "</td></tr>";

                table += "</table>";
                content = content.Replace("##DETAIL_TABLE##", table);
                txtContent.Content = content; ;
            }

            else if (PageType == "Bank Voucher")
            {
                objDB.BCVoucherID = ID;
                DataTable POdt = objDB.GetBankCashVoucherByID(ref errorMsg);
                if (POdt != null)
                {
                    content = content.Replace("##DATE##", DateTime.Parse(POdt.Rows[0]["CreatedDate"].ToString()).ToShortDateString());
                    content = content.Replace("##VOUCHER_NO##", POdt.Rows[0]["VoucherNo"].ToString());
                    content = content.Replace("##SERIAL_NO##", POdt.Rows[0]["SerialNo"].ToString());
                    string table = "";
                    table += "<table class='item-table' style = 'width:100%;'>";
                    table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";

                    table += "<tr><td>" + POdt.Rows[0]["DebitCOACode"] + "</td><td>" + POdt.Rows[0]["DebitCOATitle"] + "</td><td>" + " " + "</td><td>" + POdt.Rows[0]["TransactionDetail"] + "</td><td>" + POdt.Rows[0]["Debit"] + "</td><td>" + "0" + "</td></tr>";

                    table += "<tr><td>" + POdt.Rows[0]["CreditCOACode"] + "</td><td>" + POdt.Rows[0]["CreditCOATitle"] + "</td><td>" + " " + "</td><td>" + POdt.Rows[0]["TransactionDetail"] + "</td><td>" + "0" + "</td><td>" + POdt.Rows[0]["Credit"] + "</td></tr>";
                    table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + POdt.Rows[0]["Debit"] + "</td><td>" + POdt.Rows[0]["Debit"] + "</td></tr>";


                    table += "</table>";
                    content = content.Replace("##DETAIL_TABLE##", table);
                }

                txtContent.Content = content; ;

            }

            else if (PageType == "Cash Voucher")
            {
                objDB.BCVoucherID = ID;
                DataTable POdt = objDB.GetBankCashVoucherByID(ref errorMsg);
                if (POdt != null)
                {
                    content = content.Replace("##DATE##", DateTime.Parse(POdt.Rows[0]["CreatedDate"].ToString()).ToShortDateString());
                    content = content.Replace("##VOUCHER_NO##", POdt.Rows[0]["VoucherNo"].ToString());
                    content = content.Replace("##SERIAL_NO##", POdt.Rows[0]["SerialNo"].ToString());
                    string table = "";
                    table += "<table class='item-table' style = 'width:100%;'>";
                    table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";

                    table += "<tr><td>" + POdt.Rows[0]["DebitCOACode"] + "</td><td>" + POdt.Rows[0]["DebitCOATitle"] + "</td><td>" + " " + "</td><td>" + POdt.Rows[0]["TransactionDetail"] + "</td><td>" + POdt.Rows[0]["Debit"] + "</td><td>" + "0" + "</td></tr>";

                    table += "<tr><td>" + POdt.Rows[0]["CreditCOACode"] + "</td><td>" + POdt.Rows[0]["CreditCOATitle"] + "</td><td>" + " " + "</td><td>" + POdt.Rows[0]["TransactionDetail"] + "</td><td>" + "0" + "</td><td>" + POdt.Rows[0]["Credit"] + "</td></tr>";
                    table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + POdt.Rows[0]["Debit"] + "</td><td>" + POdt.Rows[0]["Debit"] + "</td></tr>";


                    table += "</table>";
                    content = content.Replace("##DETAIL_TABLE##", table);
                }

                txtContent.Content = content; ;

            }

            else if (PageType == "Invoice")
            {

                objDB.InvoiceID = ID;
                double totAmount = 0;

                if (Session["CompanyID"].ToString() == "18" || Session["CompanyID"].ToString() == "56")
                {
                    content = content.Replace("##COMPANY_NAME##", Session["CompanyName"].ToString());
                    content = content.Replace("##COMPANY_LOGO##", Session["CompanyLogo"].ToString());

                    DataTable INdt = objDB.GetInvoiceByIDForReport(ref errorMsg);
                    if (INdt != null)
                    {
                        content = content.Replace("##CLIENT_NAME##", INdt.Rows[0]["ClientName"].ToString());
                        content = content.Replace("##CLIENT_ADDRESSLINE1##", INdt.Rows[0]["ClientAddress1"].ToString());
                        content = content.Replace("##CLIENT_ADDRESSLINE2##", INdt.Rows[0]["ClientAddress2"].ToString());
                        content = content.Replace("##INVOICE_NO##", INdt.Rows[0]["Code"].ToString());
                        content = content.Replace("##INVOICE_DATE##", DateTime.Parse(INdt.Rows[0]["ApprovedDate"].ToString()).ToShortDateString());
                        content = content.Replace("##NTN##", INdt.Rows[0]["NTN"].ToString());
                        content = content.Replace("##STN##", INdt.Rows[0]["STN"].ToString());
                        content = content.Replace("##STRN##", INdt.Rows[0]["STRN"].ToString());
                        content = content.Replace("##NTN1##", INdt.Rows[0]["ClientNTN"].ToString());
                        string table = "";
                        if (INdt.Rows.Count > 0)
                        {
                            DataTable dtINItems = new DataTable();

                            dtINItems = objDB.GetInvoiceItemsByInvoiceID(ref errorMsg);
                            if (dtINItems != null)
                            {
                                if (dtINItems.Rows.Count > 0)
                                {

                                    for (int i = 0; i < dtINItems.Rows.Count; i++)
                                    {
                                        table +=
                                @"
                            <tr style = 'width: 100%;' >
 
                            <td style = 'width: 45%;text-align: left; font-weight: 800;' > " + dtINItems.Rows[i]["ItemCode"] + @"</td>
             
                             <td style = 'width: 12%; text-align:right;padding-right:10px;font-weight: 800; ' > " + dtINItems.Rows[i]["Quantity"] + @" </td>

                              <td style = 'width: 15%; text-align:right;padding-right:10px; ' > " + dtINItems.Rows[i]["UnitPrice"] + @" </td>

                               <td style = 'width: 23%; text-align:right;padding-right:10px;' > " + dtINItems.Rows[i]["NetPrice"] + " </td></tr>";
                                        totAmount += Convert.ToDouble(dtINItems.Rows[i]["NetPrice"].ToString());
                                    }

                                }

                            }
                        }


                        float TotalTax = 0;

                        DataTable dtPOItems = new DataTable();
                        objDB.TableID = ID;
                        objDB.Table1 = "InvoiceTaxes";
                        objDB.Table2 = "Table3";
                        objDB.Table3 = "Table3";
                        dtPOItems = objDB.GetCreditForPayVoucher(ref errorMsg);

                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {

                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {

                                    TotalTax += float.Parse(dtPOItems.Rows[i]["Credit"].ToString());
                                }

                            }
                        }

                        content = content.Replace("##SUBTOTAL##", totAmount.ToString());
                        content = content.Replace("##SALES_TAX##", INdt.Rows[0]["Discount"].ToString());
                        content = content.Replace("##TAX_RATE##", TotalTax.ToString());
                        content = content.Replace("##TOTAL##", INdt.Rows[0]["Amount"].ToString());
                        content = content.Replace("##TABLE_CONTENT##", table);
                        txtContent.Content = content;
                    }
                }
                else
                {
                    DataTable INdt = objDB.GetInvoiceByIDForReport(ref errorMsg);
                    if (INdt != null)
                    {
                        content = content.Replace("##TITLE##", INdt.Rows[0]["Title"].ToString());
                        content = content.Replace("##CLIENT_NAME##", INdt.Rows[0]["ClientName"].ToString());
                        content = content.Replace("##CLIENT_ADDRESSLINE1##", INdt.Rows[0]["ClientAddress1"].ToString());
                        content = content.Replace("##CLIENT_ADDRESSLINE2##", INdt.Rows[0]["ClientAddress2"].ToString());
                        content = content.Replace("##CLIENT_CITY##", INdt.Rows[0]["ClientCity"].ToString());
                        content = content.Replace("##CLIENT_COUNTRY##", INdt.Rows[0]["ClientCountry"].ToString());
                        content = content.Replace("##SITE_NAME##", INdt.Rows[0]["SiteName"].ToString());
                        content = content.Replace("##INVOICE_NO##", INdt.Rows[0]["Code"].ToString());
                        content = content.Replace("##INVOICE_DATE##", INdt.Rows[0]["InvoiceDate"].ToString());
                        content = content.Replace("##QUOTE_NO##", INdt.Rows[0]["QuoteNo"].ToString());
                        content = content.Replace("##CLIENT_PHONE##", INdt.Rows[0]["ClientPhone"].ToString());
                        content = content.Replace("##INVOICE_DESCRIPTION##", INdt.Rows[0]["Description"].ToString());
                        content = content.Replace("##NTN_N0##", INdt.Rows[0]["NTN"].ToString());
                        content = content.Replace("##NTN_NO##", INdt.Rows[0]["ClientNTN"].ToString());
                        content = content.Replace("##AMOUNT_IN_WORDS##", Common.NumberToWords(INdt.Rows[0]["Amount"].ToString()));
                        string table = @"<table class='line-three'>
    <tbody>
      <tr>
        <th style='width:50px'>S NO</th>
        <th>Description</th>
        <th style='width:70px'>Rate</th>
        <th style='width:80px'>Quantity</th>
        <th style='width:80px'>Net Price</th>
       
      </tr>";
                        //table += "<table class='item-table' style = 'width:100%; padding:5px;'>";
                        //table += "<tr><th style='border: 1px solid black;  padding:5px;'>Sr #</th><th style='border: 1px solid black;  padding:5px;'>Item Code</th><th style='border: 1px solid black;  padding:5px;'>UOM</th><th style='border: 1px solid black;  padding:5px;'>Quantity</th><th style='border: 1px solid black;  padding:5px;'>Unit Price</th><th style='border: 1px solid black;  padding:5px;'>Net Price</th></tr>";
                        if (INdt.Rows.Count > 0)
                        {
                            DataTable dtINItems = new DataTable();

                            dtINItems = objDB.GetInvoiceItemsByInvoiceIDWithFormat(ref errorMsg);
                            if (dtINItems != null)
                            {
                                if (dtINItems.Rows.Count > 0)
                                {

                                    for (int i = 0; i < dtINItems.Rows.Count; i++)
                                    {

                                        table += "<tr><td style=''><span>" + (i + 1) + "</span></td><td style=''>" + dtINItems.Rows[i]["ItemCode"] + "</td><td style=''><span>" + dtINItems.Rows[i]["UnitPrice"] + "</span></td><td style=''><span>" + dtINItems.Rows[i]["Quantity"] + "</span></td><td style=''>" + dtINItems.Rows[i]["NetPrice"] + "</td></tr>";
                                        //table += "<tr><td style='border: 1px solid black;  padding:5px;'>" + (i + 1) + "</td><td style='border: 1px solid black;  padding:5px;'>" + dtINItems.Rows[i]["ItemCode"] + "</td><td style='border: 1px solid black;  padding:5px;'>" + dtINItems.Rows[i]["ItemUnit"] + "</td><td style='border: 1px solid black;  padding:5px;'>" + dtINItems.Rows[i]["Quantity"] + "</td><td style='border: 1px solid black;  padding:5px;'>" + dtINItems.Rows[i]["UnitPrice"] + "</td><td style='border: 1px solid black;  padding:5px;'>" + dtINItems.Rows[i]["NetPrice"] + "</td></tr>";
                                        totAmount += double.Parse(dtINItems.Rows[i]["NetPrice"].ToString(),NumberStyles.Currency);
                                    }

                                }

                            }
                        }

                        //DataTable dtPOItems = new DataTable();
                        //objDB.TableID = ID;
                        //objDB.Table1 = "InvoiceTaxes";
                        //objDB.Table2 = "Table3";
                        //objDB.Table3 = "Table3";
                        //dtPOItems = objDB.GetCreditForPayVoucher(ref errorMsg);
                        //table += "<tr><td colspan = '5' style = 'text-align:right;border: 1px solid black;  padding:5px;'>" + "Total Amount" + "</td><td style='border: 1px solid black;  padding:5px;'>" + totAmount + "</td></tr>";

                        //if (dtPOItems != null)
                        //{
                        //    if (dtPOItems.Rows.Count > 0)
                        //    {

                        //        for (int i = 0; i < dtPOItems.Rows.Count; i++)
                        //        {

                        //            table += "<tr><td colspan = '5' style = 'text-align:right;border: 1px solid black;  padding:5px;'>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td style = 'border: 1px solid black;  padding:5px;'>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                        //        }

                        //    }
                        //    else
                        //    {

                        //        table += "<tr><td colspan = '5' style = 'text-align:right;border: 1px solid black;  padding:5px;'>" + "Tax" + "</td><td style = 'border: 1px solid black;  padding:5px;'>" + "0" + "</td></tr>";

                        //    }
                        //}

                        //table += "<tr><td colspan = '5' style = 'text-align:right;border: 1px solid black;  padding:5px;'>" + "Net Total" + "</td><td style = 'border: 1px solid black;  padding:5px;'>" + INdt.Rows[0]["Amount"] + "</td></tr>";

                        table += "<tr><td style='padding: 5px 10px;'><span>&nbsp;</span></td><td style='padding: 5px 10px;' class='bold-text'>Sub-Total</td><td style='padding: 5px 10px;'><span>&nbsp;</span></td><td style='padding: 5px 10px;'><span>&nbsp;</span></td><td style='padding: 5px 10px;'><span>"+ string.Format("{0:n2}", double.Parse(INdt.Rows[0]["GrossAmount"].ToString())) + "</span></td></tr>";
                        DataTable dtTaxes = new DataTable();
                        dtTaxes = objDB.GetInvoiceTaxesByInvoiceID(ref errorMsg);
                        if (dtTaxes != null)
                        {
                            if (dtTaxes.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtTaxes.Rows.Count; i++)
                                {
                                    table += "<tr><td style='padding: 5px 10px;'><span>&nbsp;</span></td><td style='padding: 5px 10px;'>"+ dtTaxes.Rows[i]["TaxRateName"].ToString() + " </td><td style='padding: 5px 10px;'><span>"+ dtTaxes.Rows[i]["TaxRatePer"].ToString() + "%</span></td><td style='padding: 15px 10px;'><span>&nbsp;</span></td><td style='padding: 5px 10px;'><span>" + string.Format("{0:n2}", ((double.Parse(dtTaxes.Rows[i]["TaxRatePer"].ToString())/100) * double.Parse(INdt.Rows[0]["GrossAmount"].ToString()))) + "</span></td></tr>";
                                }
                            }
                        }
                        if (float.Parse(INdt.Rows[0]["Discount"].ToString()) > 0)
                        {
                            table += "<tr><td style='padding: 15px 10px;'><span>&nbsp;</span></td><td style='padding: 15px 10px;' colspan='3' class='bold-text'>Discount </td><td style='padding: 15px 10px;'><span>" + string.Format("{0:n2}", double.Parse(INdt.Rows[0]["Discount"].ToString())) + "</span></td></tr>";
                        }
                        table += "<tr><td style='padding: 15px 10px;'><span>&nbsp;</span></td><td style='padding: 15px 10px;' colspan='3' class='bold-text'>Grand Total</td><td style='padding: 15px 10px;'><span>" + string.Format("{0:n2}", double.Parse(INdt.Rows[0]["Amount"].ToString())) + "</span></td></tr>";
                        table += "</tbody></table>";
                        content = content.Replace("##ITEM_LIST##", table);
                        txtContent.Content = content;
                    }
                }
            }

            else if (PageType == "Expense Journal Voucher")
            {
                objDB.ExpencesID = ID;
                DataTable POdt = objDB.GetExpensesByID(ref errorMsg);
                if (POdt != null)
                {
                    content = content.Replace("##DATE##", DateTime.Parse(POdt.Rows[0]["ApprovedDate"].ToString()).ToShortDateString());
                    content = content.Replace("##VOUCHER_NO##", POdt.Rows[0]["VoucherNo"].ToString());
                    content = content.Replace("##SERIAL_NO##", POdt.Rows[0]["SerialNo"].ToString());
                    string table = "";
                    table += "<table class='item-table' style = 'width:100%;'>";
                    table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";
                    if (POdt.Rows.Count > 0)
                    {
                        DataTable dtPOItems = new DataTable();
                        objDB.TableID = ID;
                        objDB.Table1 = "Expenses";
                        objDB.Table2 = "ExpenseItems";
                        objDB.Table3 = "Table3";
                        dtPOItems = objDB.GetDebitForVoucher(ref errorMsg);
                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {


                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {

                                    table += "<tr><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                }

                            }
                        }
                        dtPOItems = new DataTable();
                        objDB.TableID = ID;
                        objDB.Table1 = "Expenses";
                        objDB.Table2 = "ExpenseItems";
                        objDB.Table3 = "Table3";
                        dtPOItems = objDB.GetCreditForVoucher(ref errorMsg);
                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {

                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {

                                    table += "<tr><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                }

                            }
                        }
                    }

                    table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + POdt.Rows[0]["TotalBalance"] + "</td><td>" + POdt.Rows[0]["TotalBalance"] + "</td></tr>";


                    table += "</table>";
                    content = content.Replace("##DETAIL_TABLE##", table);
                    txtContent.Content = content; ;
                }

            }

            else if (PageType == "Invoive Journal Voucher")
            {
                objDB.InvoiceID = ID;
                DataTable POdt = objDB.GetInvoiceByID(ref errorMsg);
                if (POdt != null)
                {
                    content = content.Replace("##DATE##", DateTime.Parse(POdt.Rows[0]["ApprovedDate"].ToString()).ToShortDateString());
                    content = content.Replace("##VOUCHER_NO##", POdt.Rows[0]["Code"].ToString());
                    content = content.Replace("##SERIAL_NO##", POdt.Rows[0]["SerialNo"].ToString());
                    string table = "";
                    table += "<table class='item-table' style = 'width:100%;'>";
                    table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";
                    if (POdt.Rows.Count > 0)
                    {
                        DataTable dtPOItems = new DataTable();
                        objDB.TableID = ID;
                        objDB.Table1 = "Invoices";
                        objDB.Table2 = "InvoiceItems";
                        objDB.Table3 = "InvoiceTaxes";
                        dtPOItems = objDB.GetDebitForVoucher(ref errorMsg);
                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {

                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {

                                    table += "<tr><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                }

                            }
                        }
                        dtPOItems = new DataTable();
                        objDB.TableID = ID;
                        objDB.Table1 = "Invoices";
                        objDB.Table2 = "InvoiceItems";
                        objDB.Table3 = "InvoiceTaxes";
                        dtPOItems = objDB.GetCreditForVoucher(ref errorMsg);
                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {

                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {

                                    table += "<tr><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                }

                            }
                        }
                    }

                    table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + POdt.Rows[0]["Amount"] + "</td><td>" + POdt.Rows[0]["Amount"] + "</td></tr>";


                    table += "</table>";
                    content = content.Replace("##DETAIL_TABLE##", table);
                    txtContent.Content = content; ;
                }

            }

            else if (PageType == "Payroll Journal Voucher")
            {
                objDB.NewPayrollID = ID;
                DataTable POdt = objDB.GetNewPayrollByID(ref errorMsg);
                if (POdt != null)
                {
                    content = content.Replace("##DATE##", DateTime.Parse(POdt.Rows[0]["ApprovedDate"].ToString()).ToShortDateString());
                    content = content.Replace("##VOUCHER_NO##", POdt.Rows[0]["PayrollMonth"].ToString());
                    content = content.Replace("##SERIAL_NO##", POdt.Rows[0]["SerialNo"].ToString());
                    string table = "";
                    table += "<table class='item-table' style = 'width:100%;'>";
                    table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";
                    double totCreadit = 0;
                    double totDebit = 0;

                    if (POdt.Rows.Count > 0)
                    {
                        DataTable dtPOItems = new DataTable();
                        objDB.TableID = ID;
                        objDB.Table1 = "Payroll";
                        objDB.Table2 = "Table2";
                        objDB.Table3 = "Table3";
                        dtPOItems = objDB.GetDebitForVoucher(ref errorMsg);
                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {

                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {
                                    totDebit += Convert.ToDouble(dtPOItems.Rows[i]["Debit"].ToString());
                                    table += "<tr><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                }

                            }
                        }
                        dtPOItems = new DataTable();
                        objDB.TableID = ID;
                        objDB.Table1 = "Payroll";
                        objDB.Table2 = "Table2";
                        objDB.Table3 = "Table3";
                        dtPOItems = objDB.GetCreditForVoucher(ref errorMsg);
                        if (dtPOItems != null)
                        {
                            if (dtPOItems.Rows.Count > 0)
                            {

                                for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                {
                                    totCreadit += Convert.ToDouble(dtPOItems.Rows[i]["Credit"].ToString());
                                    table += "<tr><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                }

                            }
                        }
                    }

                    table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + totDebit + "</td><td>" + totCreadit + "</td></tr>";


                    table += "</table>";
                    content = content.Replace("##DETAIL_TABLE##", table);
                    txtContent.Content = content; ;
                }

            }

            else if (PageType == "Payment Voucher")
            {
                if (Session["TableName"] != null)
                {

                    if (Session["TableName"].ToString() == "POPay")
                    {

                        objDB.POID = ID;
                        DataTable POdt = objDB.GetPOsByID(ref errorMsg);
                        if (POdt != null)
                        {
                            content = content.Replace("##DATE##", DateTime.Parse(POdt.Rows[0]["PODate"].ToString()).ToShortDateString());
                            content = content.Replace("##VOUCHER_NO##", POdt.Rows[0]["POCode"].ToString());
                            content = content.Replace("##SERIAL_NO##", POdt.Rows[0]["JVSerialNo"].ToString());
                            string table = "";
                            double NetTotal = 0;
                            table += "<table class='item-table' style = 'width:100%;'>";
                            table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";

                            if (POdt.Rows.Count > 0)
                            {
                                DataTable dtPOItems = new DataTable();
                                objDB.TableID = ID;
                                objDB.Table1 = "POPay";
                                objDB.Table2 = "Table3";
                                objDB.Table3 = "Table3";
                                dtPOItems = objDB.GetDebitForPayVoucher(ref errorMsg);
                                if (dtPOItems != null)
                                {
                                    if (dtPOItems.Rows.Count > 0)
                                    {


                                        for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                        {

                                            table += "<tr><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                            NetTotal += Convert.ToDouble(dtPOItems.Rows[i]["Debit"].ToString());

                                        }

                                    }
                                }
                                dtPOItems = new DataTable();
                                objDB.TableID = ID;
                                objDB.Table1 = "POPay";
                                objDB.Table2 = "Table3";
                                objDB.Table3 = "Table3";
                                dtPOItems = objDB.GetCreditForPayVoucher(ref errorMsg);
                                if (dtPOItems != null)
                                {
                                    if (dtPOItems.Rows.Count > 0)
                                    {

                                        for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                        {

                                            table += "<tr><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                        }

                                    }
                                }
                            }

                            table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + NetTotal + "</td><td>" + NetTotal + "</td></tr>";


                            table += "</table>";
                            content = content.Replace("##DETAIL_TABLE##", table);
                            txtContent.Content = content; ;
                        }

                    }
                    else if (Session["TableName"].ToString() == "InvoicePay")
                    {

                        objDB.InvoiceID = ID;
                        DataTable POdt = objDB.GetInvoiceByID(ref errorMsg);
                        if (POdt != null)
                        {
                            content = content.Replace("##DATE##", DateTime.Parse(POdt.Rows[0]["ApprovedDate"].ToString()).ToShortDateString());
                            content = content.Replace("##VOUCHER_NO##", POdt.Rows[0]["Code"].ToString());
                            content = content.Replace("##SERIAL_NO##", POdt.Rows[0]["SerialNo"].ToString());
                            string table = "";
                            double NetTotal = 0;
                            table += "<table class='item-table' style = 'width:100%;'>";
                            table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";
                            if (POdt.Rows.Count > 0)
                            {
                                DataTable dtPOItems = new DataTable();
                                objDB.TableID = ID;
                                objDB.Table1 = "InvoicePay";
                                objDB.Table2 = "Table2";
                                objDB.Table3 = "Table3";
                                dtPOItems = objDB.GetDebitForPayVoucher(ref errorMsg);
                                if (dtPOItems != null)
                                {
                                    if (dtPOItems.Rows.Count > 0)
                                    {

                                        for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                        {

                                            table += "<tr><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                            NetTotal += Convert.ToDouble(dtPOItems.Rows[i]["Debit"].ToString());
                                        }

                                    }
                                }
                                dtPOItems = new DataTable();
                                objDB.TableID = ID;
                                objDB.Table1 = "InvoicePay";
                                objDB.Table2 = "Table2";
                                objDB.Table3 = "Table3";
                                dtPOItems = objDB.GetCreditForPayVoucher(ref errorMsg);
                                if (dtPOItems != null)
                                {
                                    if (dtPOItems.Rows.Count > 0)
                                    {

                                        for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                        {

                                            table += "<tr><td>" + dtPOItems.Rows[i]["AccountTitle"] + "</td><td>" + dtPOItems.Rows[i]["AccountNum"] + "</td><td>" + " " + "</td><td>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td>" + dtPOItems.Rows[i]["Debit"] + "</td><td>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
                                        }

                                    }
                                }
                            }

                            table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + NetTotal + "</td><td>" + NetTotal + "</td></tr>";


                            table += "</table>";
                            content = content.Replace("##DETAIL_TABLE##", table);
                            txtContent.Content = content; ;
                        }

                    }
                    else if (Session["TableName"].ToString() == "GRNPay")
                    {

                        objDB.GRNID = ID;
                        DataTable POdt = objDB.GetGRNsByIDFinance(ref errorMsg);
                        if (POdt != null)
                        {
                            content = content.Replace("##DATE##", DateTime.Parse(POdt.Rows[0]["GRNDate"].ToString()).ToShortDateString());
                            content = content.Replace("##VOUCHER_NO##", POdt.Rows[0]["POCode"].ToString());
                            content = content.Replace("##SERIAL_NO##", POdt.Rows[0]["GRNCode"].ToString());
                            string table = "";
                            double NetTotal = 0;
                            table += "<table class='item-table' style = 'width:100%;'>";
                            table += "<tr><th>Date</th><th>Payment Type </th><th>Description</th><th>Amount</th></tr>";

                            if (POdt.Rows.Count > 0)
                            {
                                DataTable dtPOItems = new DataTable();
                                objDB.GRNID = ID;
                                dtPOItems = objDB.GetGRNPayHistory(ref errorMsg);
                                if (dtPOItems != null)
                                {
                                    if (dtPOItems.Rows.Count > 0)
                                    {


                                        for (int i = 0; i < dtPOItems.Rows.Count; i++)
                                        {

                                            table += "<tr><td>" + dtPOItems.Rows[i]["Date"] + "</td><td>" + dtPOItems.Rows[i]["PayType"] + "</td><td>" + dtPOItems.Rows[i]["Comments"] + "</td><td>" + dtPOItems.Rows[i]["Amount"] + "</td></tr>";
                                            NetTotal += Convert.ToDouble(dtPOItems.Rows[i]["Amount"].ToString());

                                        }

                                    }
                                }

                            }

                            table += "<tr><td colspan = '3' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + NetTotal + "</td></tr>";


                            table += "</table>";
                            content = content.Replace("##DETAIL_TABLE##", table);
                            txtContent.Content = content; ;
                        }

                    }



                }


            }


        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else 
            if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Common.generatePDF(txtHeader.Content, txtFooter.Content, txtContent.Content, "Technofinancials" + DocName.Replace(" ", "-"), "A4", "Portrait");
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


    }
}