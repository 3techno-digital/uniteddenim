﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.reports
{
    public partial class COAReports : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        private DataTable dtCOA
        {
            get
            {
                if (ViewState["dtCOA"] != null)
                {
                    return (DataTable)ViewState["dtCOA"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtCOA"] = value;
            }
        }

        private DataTable dtVoucher
        {
            get
            {
                if (ViewState["dtVoucher"] != null)
                {
                    return (DataTable)ViewState["dtVoucher"];
                }
                else
                {
                    return new DataTable();
                }
            }
            set
            {
                ViewState["dtVoucher"] = value;
            }
        }

        private string Locations
        {
            get
            {
                if (ViewState["Locations"] != null)
                {
                    return (string)ViewState["Locations"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["Locations"] = value;
            }
        }

        private float TotalCredit
        {
            get
            {
                if (ViewState["TotalCredit"] != null)
                {
                    return (float)ViewState["TotalCredit"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["TotalCredit"] = value;
            }
        }

        private float TotalDebit
        {
            get
            {
                if (ViewState["TotalDebit"] != null)
                {
                    return (float)ViewState["TotalDebit"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["TotalDebit"] = value;
            }
        }

        private float TotalBalance
        {
            get
            {
                if (ViewState["TotalBalance"] != null)
                {
                    return (float)ViewState["TotalBalance"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["TotalBalance"] = value;
            }
        }

        private string VoucherRows
        {
            get
            {
                if (ViewState["VoucherRows"] != null)
                {
                    return (string)ViewState["VoucherRows"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["VoucherRows"] = value;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                //GetData();
                BindDropdown();

            }
        }

        private void BindDropdown()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            lbLocations.DataSource = objDB.GetAllApprovedLocations(ref errorMsg);
            lbLocations.DataTextField = "SiteName";
            lbLocations.DataValueField = "SiteID";
            lbLocations.DataBind();
            lbLocations.Items.Insert(0, new ListItem("All", "All"));
        }


        private void GetData()
        {
            CheckSessions();

            dtCOA = new DataTable();
            dtCOA.Columns.Add("ID");
            dtCOA.Columns.Add("ParentID");
            dtCOA.Columns.Add("SubAccount");
            dtCOA.Columns.Add("GL_CODE");
            dtCOA.Columns.Add("OpeningBalance");
            dtCOA.Columns.Add("Debit");
            dtCOA.Columns.Add("Credit");
            dtCOA.Columns.Add("CurrentBalance");
            dtCOA.AcceptChanges();


            //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            //dtCOA = objDB.GetCOACreditDebitReport(ref errorMsg);
            DataTable dtCat = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            dtCat = objDB.GetCategoryCreditDebit(ref errorMsg);
            int id = 1;
            int Catid = 0;
            int SubCatid = 0;
            int Accid = 0;
            if (dtCat != null)
            {
                for (int i = 0; i < dtCat.Rows.Count; i++)
                {
                    Catid = id;
                    dtCOA.Rows.Add(new object[] { id++, Catid, dtCat.Rows[i]["CategoryName"], dtCat.Rows[i]["CategoryCode"], dtCat.Rows[i]["OpeningBalance"], dtCat.Rows[i]["Debit"], dtCat.Rows[i]["Credit"], dtCat.Rows[i]["CurrentBalance"] });
                    dtCOA.AcceptChanges();
                    objDB.CategoryID = Convert.ToInt32(dtCat.Rows[i]["CategoryID"]);
                    DataTable dtSubCat = objDB.GetSubCategoryCreditDebit(ref errorMsg);
                    if (dtSubCat != null)
                    {
                        for (int j = 0; j < dtSubCat.Rows.Count; j++)
                        {
                            SubCatid = id;
                            dtCOA.Rows.Add(new object[] { id++, Catid, dtSubCat.Rows[j]["SubCategoryName"], dtSubCat.Rows[j]["SubCategoryCode"], dtSubCat.Rows[j]["OpeningBalance"], dtSubCat.Rows[j]["Debit"], dtSubCat.Rows[j]["Credit"], dtSubCat.Rows[j]["CurrentBalance"] });
                            dtCOA.AcceptChanges();

                            objDB.SubCategoryID = Convert.ToInt32(dtSubCat.Rows[j]["SubCategoryID"]);
                            DataTable dtAccounts = objDB.GetAccountCreditDebit(ref errorMsg);
                            if (dtAccounts != null)
                            {
                                for (int k = 0; k < dtAccounts.Rows.Count; k++)
                                {
                                    Accid = id;
                                    dtCOA.Rows.Add(new object[] { id++, SubCatid, dtAccounts.Rows[k]["AccountName"], dtAccounts.Rows[k]["AccountCode"], dtAccounts.Rows[k]["OpeningBalance"], dtAccounts.Rows[k]["Debit"], dtAccounts.Rows[k]["Credit"], dtAccounts.Rows[k]["CurrentBalance"] });
                                    dtCOA.AcceptChanges();

                                    objDB.AccountID = Convert.ToInt32(dtAccounts.Rows[k]["AccountID"]);
                                    DataTable dtSubAccount = objDB.GetCOACreditDebit(ref errorMsg);
                                    if (dtSubAccount != null)
                                    {
                                        for (int l = 0; l < dtSubAccount.Rows.Count; l++)
                                        {
                                            dtCOA.Rows.Add(new object[] { id++, Accid, dtSubAccount.Rows[l]["COAName"], dtSubAccount.Rows[l]["COACode"], dtSubAccount.Rows[l]["OpeningBalance"], dtSubAccount.Rows[l]["Debit"], dtSubAccount.Rows[l]["Credit"], dtSubAccount.Rows[l]["CurrentBalance"] });
                                            dtCOA.AcceptChanges();
                                        }
                                    }

                                }
                            }
                        }
                    }

                }
            }



            gv.DataSource = dtCOA;
            gv.DataBind();
            if (gv != null)
            {
                if (gv.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string THLocationwhereClause = "";
                int index = 0;
                string temp = "";
                temp = "";
                foreach (ListItem li in lbLocations.Items)
                {
                    if (li.Selected == true)
                    {
                        if (li.Value == "All")
                        {
                            temp = "";
                            index = 0;
                            Locations = li.Text;
                            break;
                        }
                        if (index == 0)
                        {
                            temp += " AND (TransactionHistory.LocationID = " + li.Value;
                            index++;
                            Locations += ", " + li.Text;
                        }
                        else
                        {
                            temp += " or TransactionHistory.LocationID = " + li.Value;
                            Locations += ", " + li.Text;
                        }
                    }
                }

                if (temp != "")
                {
                    THLocationwhereClause += (temp + ")");
                    Locations = Locations.Substring(2);
                }
                objDB.THLocationwhereClause = THLocationwhereClause;
                objDB.StartDate = txtFromDate.Value;
                objDB.EndDate = txtToDate.Value;
                THLocationwhereClause = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                DataTable dt = objDB.GetTrialBalance(ref errorMsg);
                DataTable dtTemp = new DataTable();
                dtTemp.Columns.Add("A/c Code");
                dtTemp.Columns.Add("Title Of Account");
                dtTemp.Columns.Add("OpeningBal");
                dtTemp.Columns.Add("Debit");
                dtTemp.Columns.Add("Credit");
                dtTemp.Columns.Add("Closing Bal.");
                dtTemp.AcceptChanges();

                string tempCategoryCOA = "";
                string tempSubCategoryCOA = "";
                string tempAccountType = "";

                float TotalCatDebit = 0;
                float TotalCatCredit = 0;
                float TotalCatBalance = 0;
                float TotalCatOpeningBalance = 0;

                float TotalSubCatDebit = 0;
                float TotalSubCatCredit = 0;
                float TotalSubCatBalance = 0;
                float TotalSubCatOpeningBalance = 0;

                float TotalTypeDebit = 0;
                float TotalTypeCredit = 0;
                float TotalTypeBalance = 0;
                float TotalTypeOpeningBalance = 0;

                VoucherRows = "";
                //string tempCOA = "";
                if (dt != null)
                {
                    if (dt.Rows.Count > 0 )
                    {
                        txtFromDate.Value = dt.Rows[0]["StartDate"].ToString();
                        txtToDate.Value = dt.Rows[0]["EndDate"].ToString();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataRow dr = dtTemp.NewRow();
                            if (dt.Rows[i]["CategoryID"].ToString() != tempCategoryCOA)
                            {
                                if (i > 0)
                                {
                                    TotalCatBalance = TotalCatDebit - TotalCatCredit;
                                    TotalSubCatBalance = TotalSubCatDebit - TotalSubCatCredit;
                                    TotalTypeBalance = TotalTypeDebit - TotalTypeCredit;

                                    VoucherRows += "<tr><td colspan = '2' style = 'text-align:right;'>" + "SUB TOTAL: " + "</td><td>" + (TotalTypeOpeningBalance == 0 ? "" : string.Format("{0:n2}", TotalTypeOpeningBalance)) + "</td><td>" + (TotalTypeDebit == 0 ? "" : string.Format("{0:n2}", TotalTypeDebit)) + "</td><td>" + (TotalTypeCredit == 0 ? "" : string.Format("{0:n2}", TotalTypeCredit)) + "</td><td>" + (TotalTypeBalance == 0 ? "" : string.Format("{0:n2}", TotalTypeBalance)) + "</td></tr>";
                                    VoucherRows += "<tr><td colspan = '2' style = 'text-align:right;'>" + "SUB TOTAL: (" + dt.Rows[i - 1]["SubCategoryTitle"].ToString() + ")" + "</td><td>" + (TotalSubCatOpeningBalance == 0 ? "" : string.Format("{0:n2}", TotalSubCatOpeningBalance)) + "</td><td>" + (TotalSubCatDebit == 0 ? "" : string.Format("{0:n2}", TotalSubCatDebit)) + "</td><td>" + (TotalSubCatCredit == 0 ? "" : string.Format("{0:n2}", TotalSubCatCredit)) + "</td><td>" + (TotalSubCatBalance == 0 ? "" : string.Format("{0:n2}", TotalSubCatBalance)) + "</td></tr>";
                                    VoucherRows += "<tr><td colspan = '2' style = 'text-align:right;'>" + "SUB TOTAL: (" + dt.Rows[i - 1]["CategoryTitle"].ToString() + ")" + "</td><td>" + (TotalCatOpeningBalance == 0 ? "" : string.Format("{0:n2}", TotalCatOpeningBalance)) + "</td><td>" + (TotalCatDebit == 0 ? "" : string.Format("{0:n2}", TotalCatDebit)) + "</td><td>" + (TotalCatCredit == 0 ? "" : string.Format("{0:n2}", TotalCatCredit)) + "</td><td>" + (TotalCatBalance == 0 ? "" : string.Format("{0:n2}", TotalCatBalance)) + "</td></tr>";

                                    //TotalDebit = 0;
                                    //TotalCredit = 0;
                                    //TotalBalance = 0;

                                    TotalCatDebit = 0;
                                    TotalCatCredit = 0;
                                    TotalCatBalance = 0;

                                    TotalSubCatDebit = 0;
                                    TotalSubCatCredit = 0;
                                    TotalSubCatBalance = 0;

                                    TotalTypeDebit = 0;
                                    TotalTypeCredit = 0;
                                    TotalTypeBalance = 0;

                                    TotalCatOpeningBalance = 0;
                                    TotalSubCatOpeningBalance = 0;
                                    TotalTypeOpeningBalance = 0;
                                }
                                dr[0] = dt.Rows[i]["CategoryCode"].ToString();
                                dr[1] = dt.Rows[i]["CategoryTitle"].ToString();
                                dr[2] = "";
                                dr[3] = "";
                                dr[4] = "";
                                dr[5] = "";
                                tempCategoryCOA = dt.Rows[i]["CategoryID"].ToString();
                                dtTemp.Rows.Add(dr);
                                dtTemp.AcceptChanges();

                                dr = dtTemp.NewRow();
                                dr[0] = dt.Rows[i]["SubCategoryCode"].ToString();
                                dr[1] = dt.Rows[i]["SubCategoryTitle"].ToString();
                                dr[2] = "";
                                dr[3] = "";
                                dr[4] = "";
                                dr[5] = "";
                                tempSubCategoryCOA = dt.Rows[i]["SubCategoryID"].ToString();
                                dtTemp.Rows.Add(dr);
                                dtTemp.AcceptChanges();

                                dr = dtTemp.NewRow();
                                dr[0] = dt.Rows[i]["AccountTypeCode"].ToString();
                                dr[1] = dt.Rows[i]["AccountTypeTitle"].ToString();
                                dr[2] = "";
                                dr[3] = "";
                                dr[4] = "";
                                dr[5] = "";
                                tempAccountType = dt.Rows[i]["AccountTypeID"].ToString();
                                dtTemp.Rows.Add(dr);
                                dtTemp.AcceptChanges();
                                VoucherRows += "<tr><td colspan='6'>" + dt.Rows[i]["CategoryCode"].ToString() + " " + dt.Rows[i]["CategoryTitle"].ToString() + "</td></tr>";
                                VoucherRows += "<tr><td colspan='6'>" + dt.Rows[i]["SubCategoryCode"].ToString() + " " + dt.Rows[i]["SubCategoryTitle"].ToString() + "</td></tr>";
                                VoucherRows += "<tr><td colspan='6'>" + dt.Rows[i]["AccountTypeCode"].ToString() + " " + dt.Rows[i]["AccountTypeTitle"].ToString() + "</td></tr>";
                            }
                            else if (dt.Rows[i]["SubCategoryID"].ToString() != tempSubCategoryCOA)
                            {
                                if (i > 0)
                                {
                                    TotalSubCatBalance = TotalSubCatDebit - TotalSubCatCredit;
                                    TotalTypeBalance = TotalTypeDebit - TotalTypeCredit;

                                    VoucherRows += "<tr><td colspan = '2' style = 'text-align:right;'>" + "SUB TOTAL: " + "</td><td>" + (TotalTypeOpeningBalance == 0 ? "" : string.Format("{0:n2}", TotalTypeOpeningBalance)) + "</td><td>" + (TotalTypeDebit == 0 ? "" : string.Format("{0:n2}", TotalTypeDebit)) + "</td><td>" + (TotalTypeCredit == 0 ? "" : string.Format("{0:n2}", TotalTypeCredit)) + "</td><td>" + (TotalTypeBalance == 0 ? "" : string.Format("{0:n2}", TotalTypeBalance)) + "</td></tr>";
                                    VoucherRows += "<tr><td colspan = '2' style = 'text-align:right;'>" + "SUB TOTAL: (" + dt.Rows[i - 1]["SubCategoryTitle"].ToString() + ")" + "</td><td>" + (TotalSubCatOpeningBalance == 0 ? "" : string.Format("{0:n2}", TotalSubCatOpeningBalance)) + "</td><td>" + (TotalSubCatDebit == 0 ? "" : string.Format("{0:n2}", TotalSubCatDebit)) + "</td><td>" + (TotalSubCatCredit == 0 ? "" : string.Format("{0:n2}", TotalSubCatCredit)) + "</td><td>" + (TotalSubCatBalance == 0 ? "" : string.Format("{0:n2}", TotalSubCatBalance)) + "</td></tr>";

                                    //TotalDebit = 0;
                                    //TotalCredit = 0;
                                    //TotalBalance = 0;

                                    TotalSubCatDebit = 0;
                                    TotalSubCatCredit = 0;
                                    TotalSubCatBalance = 0;

                                    TotalTypeDebit = 0;
                                    TotalTypeCredit = 0;
                                    TotalTypeBalance = 0;

                                    TotalSubCatOpeningBalance = 0;
                                    TotalTypeOpeningBalance = 0;
                                }
                                dr = dtTemp.NewRow();
                                dr[0] = dt.Rows[i]["SubCategoryCode"].ToString();
                                dr[1] = dt.Rows[i]["SubCategoryTitle"].ToString();
                                dr[2] = "";
                                dr[3] = "";
                                dr[4] = "";
                                dr[5] = "";
                                tempSubCategoryCOA = dt.Rows[i]["SubCategoryID"].ToString();
                                dtTemp.Rows.Add(dr);
                                dtTemp.AcceptChanges();

                                dr = dtTemp.NewRow();
                                dr[0] = dt.Rows[i]["AccountTypeCode"].ToString();
                                dr[1] = dt.Rows[i]["AccountTypeTitle"].ToString();
                                dr[2] = "";
                                dr[3] = "";
                                dr[4] = "";
                                dr[5] = "";
                                tempAccountType = dt.Rows[i]["AccountTypeID"].ToString();
                                dtTemp.Rows.Add(dr);
                                dtTemp.AcceptChanges();
                                VoucherRows += "<tr><td colspan='6'>" + dt.Rows[i]["SubCategoryCode"].ToString() + " " + dt.Rows[i]["SubCategoryTitle"].ToString() + "</td></tr>";
                                VoucherRows += "<tr><td colspan='6'>" + dt.Rows[i]["AccountTypeCode"].ToString() + " " + dt.Rows[i]["AccountTypeTitle"].ToString() + "</td></tr>";
                            }
                            else if (dt.Rows[i]["AccountTypeID"].ToString() != tempAccountType)
                            {
                                if (i > 0)
                                {
                                    TotalTypeBalance = TotalTypeDebit - TotalTypeCredit;

                                    VoucherRows += "<tr><td colspan = '2' style = 'text-align:right;'>" + "SUB TOTAL: " + "</td><td>" + (TotalTypeOpeningBalance == 0 ? "" : string.Format("{0:n2}", TotalTypeOpeningBalance)) + "</td><td>" + (TotalTypeDebit == 0 ? "" : string.Format("{0:n2}", TotalTypeDebit)) + "</td><td>" + (TotalTypeCredit == 0 ? "" : string.Format("{0:n2}", TotalTypeCredit)) + "</td><td>" + (TotalTypeBalance == 0 ? "" : string.Format("{0:n2}", TotalTypeBalance)) + "</td></tr>";

                                    //TotalDebit = 0;
                                    //TotalCredit = 0;
                                    //TotalBalance = 0;

                                    TotalTypeDebit = 0;
                                    TotalTypeCredit = 0;
                                    TotalTypeBalance = 0;
                                    TotalTypeOpeningBalance = 0;
                                }
                                dr = dtTemp.NewRow();
                                dr[0] = dt.Rows[i]["AccountTypeCode"].ToString();
                                dr[1] = dt.Rows[i]["AccountTypeTitle"].ToString();
                                dr[2] = "";
                                dr[3] = "";
                                dr[4] = "";
                                dr[5] = "";
                                tempAccountType = dt.Rows[i]["AccountTypeID"].ToString();
                                dtTemp.Rows.Add(dr);
                                dtTemp.AcceptChanges();
                                VoucherRows += "<tr><td colspan='6'>" + dt.Rows[i]["AccountTypeCode"].ToString() + " " + dt.Rows[i]["AccountTypeTitle"].ToString() + "</td></tr>";
                            }
                            dr = dtTemp.NewRow();
                            dr[0] = dt.Rows[i]["COACode"].ToString();
                            dr[1] = dt.Rows[i]["COATitle"].ToString();
                            dr[2] = dt.Rows[i]["OpeningBalance"].ToString();
                            dr[3] = dt.Rows[i]["Debit"].ToString() == "0"?"": dt.Rows[i]["Debit"].ToString();
                            dr[4] = dt.Rows[i]["Credit"].ToString() == "0" ? "" : dt.Rows[i]["Credit"].ToString();
                            dr[5] = dt.Rows[i]["ClosedBalance"].ToString();
                            dtTemp.Rows.Add(dr);
                            dtTemp.AcceptChanges();
                            TotalDebit += float.Parse(dt.Rows[i]["Debit"].ToString());
                            TotalCredit += float.Parse(dt.Rows[i]["Credit"].ToString());

                            TotalCatDebit += float.Parse(dt.Rows[i]["Debit"].ToString());
                            TotalCatCredit += float.Parse(dt.Rows[i]["Credit"].ToString());

                            TotalSubCatDebit += float.Parse(dt.Rows[i]["Debit"].ToString());
                            TotalSubCatCredit += float.Parse(dt.Rows[i]["Credit"].ToString());

                            TotalTypeDebit += float.Parse(dt.Rows[i]["Debit"].ToString());
                            TotalTypeCredit += float.Parse(dt.Rows[i]["Credit"].ToString());

                            TotalCatOpeningBalance += float.Parse(dt.Rows[i]["OpeningBalance"].ToString());
                            TotalSubCatOpeningBalance += float.Parse(dt.Rows[i]["OpeningBalance"].ToString());
                            TotalTypeOpeningBalance += float.Parse(dt.Rows[i]["OpeningBalance"].ToString());

                            VoucherRows += ("<tr><td>" + dt.Rows[i]["COACode"].ToString() + "</td><td>" + dt.Rows[i]["COATitle"].ToString() + "</td><td>" + (dt.Rows[i]["OpeningBalance"].ToString() == "0" ? "" : string.Format("{0:n2}", double.Parse(dt.Rows[i]["OpeningBalance"].ToString()))) + "</td><td>" + (dt.Rows[i]["Debit"].ToString() == "0" ? "" : string.Format("{0:n2}", double.Parse(dt.Rows[i]["Debit"].ToString()))) + "</td><td>" + (dt.Rows[i]["Credit"].ToString() == "0" ? "" : string.Format("{0:n2}", double.Parse(dt.Rows[i]["Credit"].ToString()))) + "</td><td>" + (dt.Rows[i]["ClosedBalance"].ToString() == "0" ? "" : string.Format("{0:n2}", double.Parse(dt.Rows[i]["ClosedBalance"].ToString()))) + "</td></tr>");
                            //VoucherRows += ("<tr><td>" + dt.Rows[i]["COACode"].ToString() + "</td><td>" + dt.Rows[i]["COATitle"].ToString() + "</td><td>" +  string.Format("{0:n2}", double.Parse(dt.Rows[i]["OpeningBalance"].ToString())) + "</td><td>" +  string.Format("{0:n2}", double.Parse(dt.Rows[i]["Debit"].ToString())) + "</td><td>" + string.Format("{0:n2}", double.Parse(dt.Rows[i]["Credit"].ToString())) + "</td><td>" + string.Format("{0:n2}", double.Parse(dt.Rows[i]["ClosedBalance"].ToString())) + "</td></tr>");
                            if (i == dt.Rows.Count - 1)
                            {
                                TotalCatBalance = TotalCatDebit - TotalCatCredit;
                                TotalSubCatBalance = TotalSubCatDebit - TotalSubCatCredit;
                                TotalTypeBalance = TotalTypeDebit - TotalTypeCredit;

                                VoucherRows += "<tr><td colspan = '2' style = 'text-align:right;'>" + "SUB TOTAL: " + "</td><td>" + (TotalTypeOpeningBalance == 0?"": string.Format("{0:n2}", TotalTypeOpeningBalance)) + "</td><td>" + (TotalTypeDebit == 0 ? "" : string.Format("{0:n2}", TotalTypeDebit)) + "</td><td>" + (TotalTypeCredit == 0 ? "" : string.Format("{0:n2}", TotalTypeCredit)) + "</td><td>" + (TotalTypeBalance == 0 ? "" : string.Format("{0:n2}", TotalTypeBalance)) + "</td></tr>";
                                VoucherRows += "<tr><td colspan = '2' style = 'text-align:right;'>" + "SUB TOTAL: (" + dt.Rows[i - 1]["SubCategoryTitle"].ToString() + ")" + "</td><td>" + (TotalSubCatOpeningBalance == 0 ? "" : string.Format("{0:n2}", TotalSubCatOpeningBalance)) + "</td><td>" + (TotalSubCatDebit == 0 ? "" : string.Format("{0:n2}", TotalSubCatDebit)) + "</td><td>" + (TotalSubCatCredit == 0 ? "" : string.Format("{0:n2}", TotalSubCatCredit)) + "</td><td>" + (TotalSubCatBalance == 0 ? "" : string.Format("{0:n2}", TotalSubCatBalance)) + "</td></tr>";
                                VoucherRows += "<tr><td colspan = '2' style = 'text-align:right;'>" + "SUB TOTAL: (" + dt.Rows[i - 1]["CategoryTitle"].ToString() + ")" + "</td><td>" + (TotalCatOpeningBalance == 0 ? "" : string.Format("{0:n2}", TotalCatOpeningBalance)) + "</td><td>" + (TotalCatDebit == 0 ? "" : string.Format("{0:n2}", TotalCatDebit)) + "</td><td>" + (TotalCatCredit == 0 ? "" : string.Format("{0:n2}", TotalCatCredit)) + "</td><td>" + (TotalCatBalance == 0 ? "" : string.Format("{0:n2}", TotalCatBalance)) + "</td></tr>";
                                
                                //TotalDebit = 0;
                                //TotalCredit = 0;
                                //TotalBalance = 0;

                                TotalCatDebit = 0;
                                TotalCatCredit = 0;
                                TotalCatBalance = 0;

                                TotalSubCatDebit = 0;
                                TotalSubCatCredit = 0;
                                TotalSubCatBalance = 0;

                                TotalTypeDebit = 0;
                                TotalTypeCredit = 0;
                                TotalTypeBalance = 0;

                                TotalCatOpeningBalance = 0;
                                TotalSubCatOpeningBalance = 0;
                                TotalTypeOpeningBalance = 0;
                            }
                        }

                    }
                }
                
                dtVoucher = dtTemp;
                gv.DataSource = dtTemp;
                gv.DataBind();
                if (dtTemp != null)
                {
                    if (dtTemp.Rows.Count > 0)
                    {
                        divAlertMsg.Visible = false;
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gv.DataSource = null;
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                }
                else
                {
                    gv.DataSource = null;
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
            Common.addlog("Report", "Finance", "Trial Balance Report", "");
        }
        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
            return true;
        }

        protected void btnReport_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Session["ReportTitle"] = "Trial Balance";
                Session["ReportDataTable"] = Common.GetTemplate(gv);
                Session["BackbtnLink"] = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/coa-debit-credit/view";
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/finance-report/generate");
                //Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/coa-debit-credit/generate");

            }
            catch (Exception ex)
            {

            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void gv_PreRender(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gv.Rows)
            {
                if (dtCOA.Rows[row.RowIndex]["ParentID"].ToString() == dtCOA.Rows[row.RowIndex]["ID"].ToString())
                {
                    row.Attributes["class"] = "treegrid-" + dtCOA.Rows[row.RowIndex]["ID"];
                }
                else
                {
                    row.Attributes["class"] = "treegrid-" + dtCOA.Rows[row.RowIndex]["ID"] + " treegrid-parent-" + dtCOA.Rows[row.RowIndex]["ParentID"];
                }
            }
        }

        protected void GenVoucherPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                //string DocName = "Voucher";
                DataTable dt = new DataTable();
                string txtHeader = "";
                string txtContent = "";
                string txtFooter = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.DocType = "Trial Balance";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt == null)
                {
                    objDB.CompanyID = 2;
                    objDB.DocType = "";
                    dt = objDB.GetDocumentDesign(ref errorMsg);
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtHeader = dt.Rows[0]["DocHeader"].ToString();
                        if (Session["CompanyName"].ToString() != null)
                        {
                            txtHeader = txtHeader.Replace("##COMPANY_NAME##", Session["CompanyName"].ToString());
                        }
                        txtContent = dt.Rows[0]["DocContent"].ToString();
                        txtFooter = dt.Rows[0]["DocFooter"].ToString();
                    }
                }
                if (dtVoucher != null && dtVoucher.Rows.Count > 0)
                {
                    //DocName = Common.getCompleteVoucherName(dtVoucher.Rows[0]["VoucherType"].ToString());
                    txtContent = txtContent.Replace("##LOCATION_NAME##", Locations == "" ? "All" : Locations);
                    txtContent = txtContent.Replace("##FROM_DATE##", txtFromDate.Value == "" ? "-" : txtFromDate.Value);
                    txtContent = txtContent.Replace("##TO_DATE##", txtToDate.Value == "" ? "-" : txtToDate.Value);
                    //DocName = DocName + "-" + dtVoucher.Rows[0]["SerialNo"].ToString() + "-" + dtVoucher.Rows[0]["VoucherNo"].ToString();

                    string table = @"<table class='line-three'>
    <tbody>
      <tr>
        <th style='width:100px'>A/c Code</th>
        <th >Title of Account</th>
        <th style='width:100px'>Opening Bal.</th>
        <th style='width:100px'>Debit</th>
        <th style='width:100px'>Credit</th>
        <th style='width:100px'>Closing Bal.</th>
      </tr>";

                    //for (int i = 0; i < dtVoucher.Rows.Count; i++)
                    //{
                    table += VoucherRows;
                    //totAmount += double.Parse(dtVoucher.Rows[i]["Debit"].ToString());
                    //}

                    table += "";
                    table += "</tbody></table>";
                    //table += "<p>" + Common.NumberToWords(totAmount) + "</p>";
                    txtContent = txtContent.Replace("##DETAIL_TABLE##", table);

                    Common.generatePDF(txtHeader, txtFooter, txtContent, "Trial Balance", "A4", "Portrait");
                }
            }
            catch (Exception ex)
            {
            }
        }

    }
}