﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.reports
{
    public partial class BalanceSheet : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";


        private DataTable dtCOA
        {
            get
            {
                if (ViewState["dtCOA"] != null)
                {
                    return (DataTable)ViewState["dtCOA"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtCOA"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                GetData();
                // BindDropdown();

            }
        }




        private void GetData()
        {
            CheckSessions();

            dtCOA = new DataTable();
            dtCOA.Columns.Add("ID");
            dtCOA.Columns.Add("ParentID");
            dtCOA.Columns.Add("SubAccount");
            dtCOA.Columns.Add("GL_CODE");
            dtCOA.Columns.Add("OpeningBalance");
            dtCOA.Columns.Add("Debit");
            dtCOA.Columns.Add("Credit");
            dtCOA.Columns.Add("CurrentBalance");
            dtCOA.AcceptChanges();

            DataTable dtCat = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            dtCat = objDB.GetCategoryCreditDebit(ref errorMsg);
            int id = 1;
            int Catid = 0;
            int SubCatid = 0;
            int Accid = 0;
            double totalCredit = 0;
            double totalDebit = 0;
            double totalOpeningBalance = 0;
            double totalLiabAndEqu = 0;

            if (dtCat != null)
            {
                for (int i = 0; i < dtCat.Rows.Count; i++)
                {
                    if (dtCat.Rows[i]["CategoryCode"].ToString() == "1" || dtCat.Rows[i]["CategoryCode"].ToString() == "2" || dtCat.Rows[i]["CategoryCode"].ToString() == "3")
                    {
                    Catid = id;
                    dtCOA.Rows.Add(new object[] { id++, Catid, dtCat.Rows[i]["CategoryName"], dtCat.Rows[i]["CategoryCode"], dtCat.Rows[i]["OpeningBalance"], dtCat.Rows[i]["Debit"], dtCat.Rows[i]["Credit"], dtCat.Rows[i]["CurrentBalance"] });
                    dtCOA.AcceptChanges();
                    objDB.CategoryID = Convert.ToInt32(dtCat.Rows[i]["CategoryID"]);
                    DataTable dtSubCat = objDB.GetSubCategoryCreditDebit(ref errorMsg);
                    if (dtSubCat != null)
                    {
                        for (int j = 0; j < dtSubCat.Rows.Count; j++)
                        {
                            SubCatid = id;
                            dtCOA.Rows.Add(new object[] { id++, Catid, dtSubCat.Rows[j]["SubCategoryName"], dtSubCat.Rows[j]["SubCategoryCode"], dtSubCat.Rows[j]["OpeningBalance"], dtSubCat.Rows[j]["Debit"], dtSubCat.Rows[j]["Credit"], dtSubCat.Rows[j]["CurrentBalance"] });
                            dtCOA.AcceptChanges();

                            objDB.SubCategoryID = Convert.ToInt32(dtSubCat.Rows[j]["SubCategoryID"]);
                            DataTable dtAccounts = objDB.GetAccountCreditDebit(ref errorMsg);
                            if (dtAccounts != null)
                            {
                                for (int k = 0; k < dtAccounts.Rows.Count; k++)
                                {
                                    Accid = id;
                                    dtCOA.Rows.Add(new object[] { id++, SubCatid, dtAccounts.Rows[k]["AccountName"], dtAccounts.Rows[k]["AccountCode"], dtAccounts.Rows[k]["OpeningBalance"], dtAccounts.Rows[k]["Debit"], dtAccounts.Rows[k]["Credit"], dtAccounts.Rows[k]["CurrentBalance"] });
                                    dtCOA.AcceptChanges();

                                    objDB.AccountID = Convert.ToInt32(dtAccounts.Rows[k]["AccountID"]);
                                    DataTable dtSubAccount = objDB.GetCOACreditDebit(ref errorMsg);
                                    if (dtSubAccount != null)
                                    {
                                        for (int l = 0; l < dtSubAccount.Rows.Count; l++)
                                        {
                                            dtCOA.Rows.Add(new object[] { id++, Accid, dtSubAccount.Rows[l]["COAName"], dtSubAccount.Rows[l]["COACode"], dtSubAccount.Rows[l]["OpeningBalance"], dtSubAccount.Rows[l]["Debit"], dtSubAccount.Rows[l]["Credit"], dtSubAccount.Rows[l]["CurrentBalance"] });
                                            dtCOA.AcceptChanges();
                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (dtCat.Rows[i]["CategoryCode"].ToString() == "1")
                    {

                        //dtCOA.Rows.Add(new object[] { id++, (id - 1), "", "Total Asstes", dtCat.Rows[i]["OpeningBalance"], dtCat.Rows[i]["Debit"], dtCat.Rows[i]["Credit"], dtCat.Rows[i]["CurrentBalance"] });
                        dtCOA.Rows.Add(new object[] { id++, (id - 1), "", "", "", "", "", "" });
                        dtCOA.AcceptChanges();
                    }
                    else if (dtCat.Rows[i]["CategoryCode"].ToString() == "2" || dtCat.Rows[i]["CategoryCode"].ToString() == "3")
                    {
                        totalCredit += Convert.ToDouble(dtCat.Rows[i]["Credit"].ToString());
                        totalDebit += Convert.ToDouble(dtCat.Rows[i]["Debit"].ToString());
                        totalOpeningBalance += Convert.ToDouble(dtCat.Rows[i]["OpeningBalance"].ToString());
                        totalLiabAndEqu += Convert.ToDouble(dtCat.Rows[i]["CurrentBalance"].ToString());
                    }
                    }
                }


                dtCOA.Rows.Add(new object[] { id++, (id - 1), "", "Total Liabilities & Equity", totalOpeningBalance, totalDebit,totalCredit, totalLiabAndEqu });
                dtCOA.AcceptChanges();
            }



            gv.DataSource = dtCOA;
            gv.DataBind();
            if (gv != null)
            {
                if (gv.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            //try
            //{
            //    CheckSessions();
            //    DataTable dt = new DataTable();
            //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            //    dt = objDB.GetCOACreditDebitReport(ref errorMsg);
            //    gv.DataSource = Common.filterTable2(dt, ddlSupplier.SelectedValue);
            //    gv.DataBind();
            //    if (gv != null)
            //    {
            //        if (gv.Rows.Count > 0)
            //        {
            //            divAlertMsg.Visible = false;
            //            gv.UseAccessibleHeader = true;
            //            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            //        }
            //        else
            //        {
            //            gv.DataSource = null;
            //            divAlertMsg.Visible = true;
            //            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
            //            pAlertMsg.InnerHtml = "No Record Found";
            //        }
            //    }
            //    else
            //    {
            //        gv.DataSource = null;
            //        divAlertMsg.Visible = true;
            //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
            //        pAlertMsg.InnerHtml = "No Record Found";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    divAlertMsg.Visible = true;
            //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
            //    pAlertMsg.InnerHtml = ex.Message;
            //}
            //Common.addlog("Report", "Finance", "Chart Of Account Debit/Credit Report", "");
        }
        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
            return true;
        }


        protected void btnReport_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Session["ReportTitle"] = "Balance Sheet";
                Session["ReportDataTable"] = Common.GetTemplate(gv);
                Session["BackbtnLink"] = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/balance-sheet/view";
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/finance-report/generate");
                //Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/balance-sheet/generate");

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void gv_PreRender(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gv.Rows)
            {
                if (dtCOA.Rows[row.RowIndex]["ParentID"].ToString() == dtCOA.Rows[row.RowIndex]["ID"].ToString())
                {
                    row.Attributes["class"] = "treegrid-" + dtCOA.Rows[row.RowIndex]["ID"];
                }
                else
                {
                    row.Attributes["class"] = "treegrid-" + dtCOA.Rows[row.RowIndex]["ID"] + " treegrid-parent-" + dtCOA.Rows[row.RowIndex]["ParentID"];
                }

            }
        }
    }
}