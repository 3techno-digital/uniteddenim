﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IBFT_Report.aspx.cs" Inherits="Technofinancials.Finance.reports.IBFT_Report" %>


<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>

        button.dt-button.buttons-csv.buttons-html5 {
    margin: 0;
    background: #003780;
    padding: 3px 24px;
    color: #fff;
    border: 1px solid #003780;
    border-radius: 4px;
    font-weight: 900;
    transition: all .2s linear;
    cursor: pointer;
}
        .tf-note-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-note-btn i {
                color: #fff !important;
            }

        .tf-disapproved-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-disapproved-btn i {
                color: #fff !important;
            }


        .tf-add-btn {
            background-color: #575757;
            padding: 4px 9px 4px 10px !important;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-add-btn i {
                color: #fff !important;
            }

        .tf-add-btn {
            background-color: #575757;
            padding: 12px 10px 8px 10px !important;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-add-btn i {
                color: #fff !important;
            }

        .tf-back-btn {
            background-color: #575757;
            padding: 10px 10px 10px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
            padding: 5px !important;
            height: 38px !important;
            width: 37px !important;
            display: inline-block !important;
            text-align: center !important;
        }

        div#gv_wrapper div#gv_filter{
            overflow-x: hidden;
            padding: 0px;
            display:none;
        }
        div#gv_wrapper .dt-buttons , div#gv_wrapper div#gv_info , div#gv_wrapper div#gv_paginate{
            padding: 0px !important;
        }

        .tf-back-btn i {
            color: #fff !important;
        }

        div#gv_wrapper{
            overflow-x: scroll !important;
            overflow-y: hidden;
            padding-bottom: 5rem;
        }

            div#gv_wrapper::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            div#gv_wrapper::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }
table#gv {
    width: 3300px;
}
.table-bordered > thead > tr > th{
    font-size:10px;
}
            div#gv_wrapper::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

        @media only screen and (max-width: 992px) and (min-width: 768px) {
            div#gv_wrapper{
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 5rem;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 993px) {
            div#gv_wrapper{
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 5rem;
            }
        }

        @media only screen and (max-width: 1280px) and (min-width: 800px) {
            div#gv_wrapper{
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 5rem;
            }
        }
    </style>
</head>

                
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">

            <div class="wrap">
               
                               <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">IBFT CSV Report</h1>
                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                       <h4>Month
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtPayrollDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>

                                <%-- <input name="txtPayrollDate" type="text" id="txtPayrollDate" class="form-control" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />--%>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-12">
                            <%--<button class="AD_btn" "" type="button" runat="server" onclick="ShowReport();" data-original-"">View</button>--%>
                            <button class="AD_btn_inn" style="margin-top: 32px !important;" id="btnView" runat="server" validationgroup="btnView" onserverclick="ddlYear_SelectedIndexChangedNew" type="button">View</button>
                        </div>
                            </div>
                          

                        </div>
                    </div>
               
              <div class="col-lg-2 col-md-4 col-sm-12">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

             <div class="row">
                       <div class="col-sm-12">
                            <div class="tab-content ">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                     <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gvcsv dataTable no-footer" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                          

                                            <asp:TemplateField HeaderText="Identifier">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblEmployeeName" Text="P"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payment Type">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblAttendanceDate" Text="BT"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Processing Mode">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblTimeIn" Text="ON"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              
                                              <asp:TemplateField HeaderText="Customer Reference">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblTimeOut" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Debit A/C No.">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblLateReason" Text="1719797501"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Debit Currency">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblisOnLeave" Text="PKR"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Debit Bank Id">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblisOnLeave" Text="SCBLPKKXXXX"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Payment Date (dd/mm/yyyy)">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%#DateTime.Now.ToString("dd/MM/yyyy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Payee Bank Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text="SCB"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Payee Bank Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text="SCBLPKKXXXX"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Payee A/C No.">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Payment Currency">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text="PKR"></asp:Label>
                                                </ItemTemplate>
                                              </asp:TemplateField>
                                               
                                                <asp:TemplateField HeaderText="Net Payment">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label4" Text='<%# Eval("NetSalry","{0:N}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Payee Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Payee Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Payee Address 1">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Payee Address 2">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Payee Address 3">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Payment detail 1">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Payment detail 2">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Clearing Zone Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Emailid">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Corporate Cheque Number">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Purpose of Payment">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Cheque - Deliver To">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                <asp:TemplateField HeaderText="Cheque - Delivery Method">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                             <asp:TemplateField HeaderText="Cheque - Pickup Location">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    
                                              <asp:TemplateField HeaderText="Beneficiary ID type">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    

                                              <asp:TemplateField HeaderText="Beneficiary ID">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    

                                              <asp:TemplateField HeaderText="Beneficiary Contact Number">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Label5" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    
                                            
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                       </div>
                    </div>
                                        <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <!-- Modal -->
    </form>
</body>
</html>


