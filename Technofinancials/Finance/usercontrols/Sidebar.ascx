﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Sidebar.ascx.cs" Inherits="Technofinancials.Finance.usercontrols.Sidebar" %>
<aside id="menubar" class="menubar tf-sidebar">
	<div class="menubar-scroll">
		<div class="menubar-scroll-inner tf-sidebar-menu-items">
			<ul class="app-menu">
				<li class="tf-first-menu">
					<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/dashboard"); %>">
						<img src="/assets/images/white/Dashboard.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Dashboard</span>
					</a>
				</li>
					<li>
					<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/employees"); %>">
						<img src="/assets/images/hr-04__2.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Personnel Management</span>
					</a>
				</li>
				<li class="has-submenu ">
					<a href="javascript:void(0)" class="submenu-toggle">
						<img src="/assets/images/finance.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Employee Finance</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
					<ul class="submenu">
					<%--	<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/mini-payroll-New"); %>"><span class="menu-text">Mini Payroll</span></a></li>
						--%>
						<%--<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/payrolls2"); %>"><span class="menu-text">Payroll</span></a></li>--%>
				<%--		<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/IT3Approval"); %>"><span class="menu-text">IT-3 Approvals</span></a></li>--%>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/AddOnsExpenseNew"); %>"><span class="menu-text">Add-ons Approval</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/deduction"); %>"><span class="menu-text">Deductions  Approval</span></a></li>
					<%--	<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/Other-expense"); %>"><span class="menu-text">Other Expenses  Approval</span></a></li>--%>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/attendance-adjustment"); %>"><span class="menu-text">Attendance Adjustment</span></a></li>
						<%--<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/tracking"); %>"><span class="menu-text">Tracking</span></a></li>
					--%>
					</ul>
				</li>
				<li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/attendance-uploader"); %>">
                       <i class="fa fa-clock-o img-responsive tf-sidebar-icons" aria-hidden="true"></i>
                        <span class="menu-text">Attendance Uploader</span>
                    </a>
                </li>
				<li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/standard-payroll"); %>">
                       <i class="fa fa-calendar-check-o img-responsive tf-sidebar-icons" aria-hidden="true"></i>
                        <span class="menu-text">Standard Payroll</span>
                    </a>
                </li>
			<%--	<li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/teams"); %>">
                        <img src="/assets/images/users.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Teams</span>
                    </a>
                </li>--%>

<%--						<li class="has-submenu ">
					<a href="javascript:void(0)" class="submenu-toggle">
						  <img src="/assets/images/Assets__03.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Loan & Advance</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
					<ul class="submenu">
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/loansNadvance"); %>"><span class="menu-text">Employee Applications</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/UploadLoanSchedule"); %>"><span class="menu-text">Loan Schedule Uploader</span></a></li>

					
					</ul>
				</li>--%>
					
				
		<%--		<li class="has-submenu ">
					<a href="javascript:void(0)" class="submenu-toggle">
						<i class="fa fa-handshake-o img-responsive tf-sidebar-icons" aria-hidden="true"></i>
						<span class="menu-text">FnF Settlement</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
					<ul class="submenu">
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/employee-clearance"); %>"><span class="menu-text">Employee Clearance Approvals</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/employee-fnf-settlement"); %>"><span class="menu-text">Employee FnF Payment Lock</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/fnf-pendings"); %>"><span class="menu-text">Pending FnFs</span></a></li>
					
					</ul>
				</li>--%>
				<li class="has-submenu ">
					<a href="javascript:void(0)" class="submenu-toggle">
						<img src="/assets/images/reports-icon-02.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Reports</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
					<ul class="submenu">
					<%--	<li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/audit"); %>">
                     
                        <span class="menu-text">Authorization Logs</span>
                    </a>
                </li>--%>
					<%--	<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/fnf-detail"); %>"><span class="menu-text">FnF Report</span></a></li>
					--%>
						<%--	<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/assets-deductions"); %>"><span class="menu-text">Assets and Deductions</span></a></li>--%>
					  <li class="has-submenu">
							<a href="javascript:void(0)" class="submenu-toggle">
								<%--<img src="/assets/images/hr-04__2.png" class="img-responsive tf-sidebar-icons" />--%>
								<span class="menu-text">Attendance Reports</span>
								<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
							</a>
							<ul class="submenu">
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/clockwise-sheet"); %>">
										<%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
										<span class="menu-text">Clockwise Attendance</span>
									</a>
								</li>
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/attendance-sheet"); %>">
										<%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
										<span class="menu-text">Attendance Sheet</span>
									</a>
								</li>
								<li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/attendence-detail"); %>">
                                        <%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
                                        <span class="menu-text">Attendance Detail</span>
                                    </a>
                                </li>
								<%--<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/attendance-summmary"); %>">
										--%><%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
<%--										<span class="menu-text">Attendance Summary</span>
									</a>
								</li>--%>
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/Flag-wise-Employees"); %>">
										<span class="menu-text">Flag-Wise Employees Attendance Report</span>
									</a>
								</li>
							</ul>
						</li>
						
						
						<li class="has-submenu">
							<a href="javascript:void(0)" class="submenu-toggle">
								<span class="menu-text">Payroll</span>
								<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
							</a>
							<ul class="submenu">
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/payroll"); %>"><span class="menu-text">Payroll Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/payroll-detail-Report"); %>"><span class="menu-text">Payroll Detailed Report</span></a></li>
<%--								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/fnf-detail-Report"); %>"><span class="menu-text">FnF Settlement Report</span></a></li>--%>
					<%--			<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/minipayrolllist"); %>"><span class="menu-text">Mini Payroll Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/mini-payroll-detail-Report"); %>"><span class="menu-text">Mini Payroll Detailed Report</span></a></li>--%>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/payroll-Monthly-Tax-Report"); %>"><span class="menu-text">Tax Deduction Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/payroll-Provident-Fund-Detail"); %>"><span class="menu-text">Provident Fund Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/employee-eobi"); %>"><span class="menu-text">Employee EOBI Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/Employee-SESSI-IESSI-PESSI-report"); %>"><span class="menu-text">Employee IESSI/PESSI/SESSI Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/employee-WHT-report"); %>"><span class="menu-text">Employee With-Holding Tax Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/yearly-Tax-Report"); %>"><span class="menu-text">Yearly Tax Report</span></a></li>
							</ul>
						</li>

<%--						<li class="has-submenu">
							<a href="javascript:void(0)" class="submenu-toggle">
								<span class="menu-text">Bank Reports</span>
								<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
							</a>
							<ul class="submenu">
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/SCB"); %>"><span class="menu-text">SCB CSV Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/IBFT"); %>"><span class="menu-text">IBFT CSV Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/JEReport"); %>"><span class="menu-text">JE Report</span></a></li>
							</ul>
						</li>--%>
					</ul>
				</li>

				<li class="has-submenu">
					<a href="javascript:void(0)" class="submenu-toggle">
						<img src="/assets/images/payslip.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Payslip</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
					<ul class="submenu">
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/generate-pay-slip"); %>"><span class="menu-text">Generate Payslip</span></a></li>
						<%--<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/generate-minipay-slip"); %>"><span class="menu-text">Generate Mini-Payslip</span></a></li>--%>
					</ul>
				</li>
				<li class="has-submenu ">
					<a href="javascript:void(0)" class="submenu-toggle">
						<img src="/assets/images/Admin_setting.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Settings</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>

					<ul class="submenu">
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/PayrollDate/add-new-PayrollDate"); %>"><span class="menu-text">Payroll Cycle</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/provinent-funds/manage-provinent-funds"); %>"><span class="menu-text">Payroll Setup</span></a></li>
							<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/holidays"); %>"><span class="menu-text">Holidays</span></a></li>
<%--						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/coa"); %>"><span class="menu-text">Chart Of Account Setup</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/currency"); %>"><span class="menu-text">Currency</span></a></li>--%>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/AddOns"); %>"><span class="menu-text">Add-ons Heads</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/deduction-head"); %>"><span class="menu-text">Deduction Heads</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/overtime-group"); %>"><span class="menu-text">Overtime Group</span></a></li>
<%--						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/clearance-items"); %>"><span class="menu-text">Clearance Items</span></a></li>
					<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/auto-attendance"); %>"><span class="menu-text">Auto Attendance</span></a></li>--%>
					</ul>
				</li>
			</ul>
			<!-- .app-menu -->
		</div>
		<!-- .menubar-scroll-inner -->
	</div>
	<div class="tf-vno">tf-v2.9 <%Response.Write(DateTime.Now.ToString("yyyy")); %></div>
	<!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->
