﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance
{
    public partial class Dashboard : System.Web.UI.Page
    {
        string errorMsg;
        DBQueries objDB = new DBQueries();
        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                try
                {  
                    Session["UserAccess"] = Session["OldUserAccess"];
                    ddlCat.Visible = false;
                    BindAssetTypes();
                    txtMonth.Disabled = true;
                    generateWidget();
                   
                    //generateAssetCategoriesChart();
                    //generateAssetVsADChart();
                    //generateBookValueChart();
                    //GetYearWiseChartData();
                    //GetDeptWiseChartData();
                    //GetPayrollDataLocationWise();
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void GetYearWiseChartData()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = objDB.GetFinanceChartYearlyByCompanyID(ref errorMsg);

            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    string str2 = "<script> new Chart(document.getElementById('"+ "MonthlyPayrollChart" + "'), {  type: 'bar',  data: {    labels: ['"+"Jan"+ "', '" + "Feb" + "', '" + "Mar" + "', '" + "Apr" + "', '" + "May" + "', '" + "Jun" + "', '" + "Jul" + "', '" + "Aug" + "', '" + "Sep" + "', '" + "Oct" + "', '" + "Nov" + "', '" + "Dec" + "'],   datasets: [  {     label: '" + "Amount" + "',     backgroundColor: [ '#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850', '#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850', '#2eccc1', '#c45850'],    data: ["+ dt.Rows[0]["Column1"].ToString() + ", " + dt.Rows[1]["Column1"].ToString() + ", " + dt.Rows[0]["Column1"].ToString() + ", " + dt.Rows[2]["Column1"].ToString() + ", " + dt.Rows[0]["Column1"].ToString() + ", " + dt.Rows[0]["Column1"].ToString() + ", " + dt.Rows[0]["Column1"].ToString() + ", " + dt.Rows[0]["Column1"].ToString() + ", " + dt.Rows[2]["Column1"].ToString() + ", " + dt.Rows[1]["Column1"].ToString() + ", " + dt.Rows[0]["Column1"].ToString() + ", " + dt.Rows[0]["Column1"].ToString() + "]      } ]  }, options:  { legend: { display: false },  title:   {   display: true,   text: '' }}   }); </script>";
                    ltrMonthlywise.Text = str2.ToString();

                }

            }

        }

        //Done
        protected void GetDeptWiseChartData()
        {
            ltrDeptWise.Text = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            string months= DeptMonthyear.Text;
           // string testmonth = "Nov-2020";
            objDB.Month = months;
            
            DataTable dt = objDB.GetDepartWiseMonthlySalary(ref errorMsg);

            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    string[] DeptNames = dt.Rows.Cast<DataRow>().Select(row => "'" + row["DeptName"].ToString() + "'").ToArray();
                    string[] Totals = dt.Rows.Cast<DataRow>().Select(row =>  row["Total"].ToString() ).ToArray();
                    string[] Colors = new string[DeptNames.Length];
                    string strdept = string.Join(",", DeptNames);
                    string strtotal = string.Join(",", Totals);


                    Random rnd = new Random();
                    for (int i = 0; i < DeptNames.Length; i++)
                    {
                        Color randomColor = Color.FromArgb(rnd.Next(0, 256), rnd.Next(0, 256), rnd.Next(0, 256));
                        Colors[i] = "'#" + randomColor.Name + "'";
                    }
                    string color = string.Join(",", Colors);

                    string str2 = "<script> new Chart(document.getElementById('"+ "DepartmentWiseChart" + "'), {  type: 'horizontalBar',data: {  labels: ["+strdept+"], datasets: [   { label: '"+ "Population (millions)" + "',   backgroundColor: ["+ color + "],  data: ["+ strtotal + "]  }  ] },  options: {  legend: { display: false }, title: { display: true,   text: '' } } }); </script>";
                    ltrDeptWise.Text = str2.ToString();

                }

            }

        }

        //Done
        protected void GetPayrollDataLocationWise()
        {
            ltrLocationWise.Text = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
           // string months = DeptIDMonth.Value + "-" + DateTime.Now.ToString("yyyy");
            
            string testmonth = "Nov-2020";
            objDB.Month = txtLocationWise.Text;// testmonth;

            DataTable dt = objDB.GetPayrollDataLocationWiseSP(ref errorMsg);

            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    string[] Cities = dt.Rows.Cast<DataRow>().Select(row => "\""+ row["City"].ToString() + "\"").ToArray();
                    string[] Total = dt.Rows.Cast<DataRow>().Select(row => "\""+row["Total"].ToString() + "\"").ToArray();
                    string[] Colors = new string[Cities.Length];
                    string city = string.Join(",", Cities);
                    string total = string.Join(",", Total);


                     Random rnd = new Random();
                    for (int i = 0; i < Cities.Length; i++)
                    {
                        Color randomColor = Color.FromArgb(rnd.Next(0,256), rnd.Next(0, 256), rnd.Next(0, 256));
                        Colors[i] = "'#" +randomColor.Name + "'";
                    }
                    string color = string.Join(",", Colors);

                    string str2 = "<script> var ctx = document.getElementById('"+ "LocationWiseChart" + "').getContext('2d'); var myChart = new Chart(ctx, { type: 'pie', data:{ labels: ["+ city + "],datasets: [{  backgroundColor: [ "+ color + " ], data: [" + total + "] }] } }); </script>";
                    
                    ltrLocationWise.Text = str2.ToString();

                }

            }

        }

      
        //Done
        string[] colors = new string[] { "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#2eccc1", "#c45850" };
        
        private void BindAssetTypes()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCat.Items.Clear();
            ddlCat.DataSource = objDB.GetAllAssetTypes(ref errorMsg);
            ddlCat.DataTextField = "ASSET_TYPE_NAME";
            ddlCat.DataValueField = "ASSET_TYPE_ID";
            ddlCat.DataBind();
            ddlCat.Items.Insert(0, new ListItem("--- Select Type ---", "0"));

        }

        protected void generateWidget(string Months = "")
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                DateTime depStartDate = DateTime.Parse("01-JAN-" + DateTime.Now.Year);

                if (Months != "")
                {
                    depStartDate = DateTime.Now.AddMonths(Convert.ToInt32("-" + Months) + 1);
                }
                string StartDate = "01-JAN-" + DateTime.Now.Year.ToString();
                string EndDate = "01-JAN-" + (DateTime.Now.Year + 1);

                if (chkLastYear.Checked)
                {
                    dt = new DataTable();
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());

                    dt = objDB.GetAllAccountingDepreciations(ref errorMsg);

                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            depStartDate = DateTime.Parse(dt.Rows[0]["DEPRECIATION_START_DATE"].ToString());
                            StartDate = DateTime.Parse(dt.Rows[0]["DEPRECIATION_START_DATE"].ToString()).ToString("dd-MMM");
                            if (DateTime.Parse(StartDate).Month > DateTime.Now.Month)
                            {
                                StartDate = StartDate + "-" + (DateTime.Now.Year - 1).ToString();
                            }
                            else
                            {
                                StartDate = StartDate + "-" + DateTime.Now.Year.ToString();
                            }

                        }
                    }

                    EndDate = DateTime.Parse(StartDate).AddMonths(12).ToString("dd-MMM-yyyy");

                    objDB.StartDate = StartDate;
                    objDB.EndDate = EndDate;
                    dt = objDB.GetAdditionByDepDate(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            ltrAdditions.Text = dt.Rows[0]["ADDITIONS"].ToString();
                        }

                    }


                    objDB.StartDate = StartDate;
                    objDB.EndDate = EndDate;
                    dt = objDB.GetDisposalByDepDate(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            ltrDisposal.Text = dt.Rows[0]["ADDITIONS"].ToString();
                        }

                    }


                    objDB.StartDate = StartDate;
                    objDB.EndDate = EndDate;
                    dt = objDB.GetRevaluationByDepDate(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            ltrAssetRev.Text = dt.Rows[0]["ADDITIONS"].ToString();
                        }

                    }


                    objDB.StartDate = StartDate;
                    objDB.EndDate = EndDate;
                    dt = objDB.GetCWIPByDepDate(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            ltrCWIP.Text = dt.Rows[0]["ADDITIONS"].ToString();
                        }

                    }



                }
                else
                {
                    objDB.Month = ("-" + Months);
                    dt = objDB.GetAdditionByLastMonth(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            ltrAdditions.Text = dt.Rows[0]["ADDITIONS"].ToString();
                        }
                    }

                    objDB.Month = ("-" + Months);
                    dt = objDB.GetDisposalByLastMonth(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            ltrDisposal.Text = dt.Rows[0]["ADDITIONS"].ToString();
                        }
                    }

                    objDB.Month = ("-" + Months);
                    dt = objDB.GetRevaluationByLastMonth(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            ltrAssetRev.Text = dt.Rows[0]["ADDITIONS"].ToString();
                        }
                    }

                    objDB.Month = ("-" + Months);
                    dt = objDB.GetCWIPByLastMonth(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            ltrCWIP.Text = dt.Rows[0]["ADDITIONS"].ToString();
                        }
                    }

                }

            }
            catch (Exception ex)
            {
            }
        }

        protected void generateBookValueChart(string Months = "")
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            DateTime depStartDate = DateTime.Parse("01-JAN-" + DateTime.Now.Year);

            if (Months != "")
            {
                depStartDate = DateTime.Now.AddMonths(Convert.ToInt32("-" + Months) + 1);
            }

            if (chkLastYear.Checked)
            {
                dt = new DataTable();
                string StartDate = "01-JAN-" + DateTime.Now.Year.ToString();
                dt = objDB.GetAllAccountingDepreciations(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        depStartDate = DateTime.Parse(dt.Rows[0]["DEPRECIATION_START_DATE"].ToString());
                        StartDate = DateTime.Parse(dt.Rows[0]["DEPRECIATION_START_DATE"].ToString()).ToString("dd-MMM");
                        if (DateTime.Parse(StartDate).Month > DateTime.Now.Month)
                        {
                            StartDate = StartDate + "-" + (DateTime.Now.Year - 1).ToString();
                        }
                        else
                        {
                            StartDate = StartDate + "-" + DateTime.Now.Year.ToString();
                        }

                    }
                }

                string EndDate = DateTime.Parse(StartDate).AddMonths(12).ToString("dd-MMM-yyyy");

                objDB.StartDate = StartDate;
                objDB.EndDate = EndDate;
                dt = objDB.GetAssetCostBYDepDate(ref errorMsg);
            }
            else
            {
                objDB.Month = ("-" + Months);
                dt = objDB.GetAssetCostByLastMonth(ref errorMsg);
            }





            string[] Cost = new string[] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };
            string[] BookValue = new string[] { "0", "0", "0", "0", "0", "0", "0", "0", "10", "0", "200", "0", "0" };

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Cost[int.Parse(dt.Rows[i]["MON"].ToString())] = dt.Rows[i]["TOT_COST"].ToString();
                    }
                }
            }



            string script = "<div data-plugin=\"chart\" data-options=\"{";
            script += @"tooltip:
                {
                    trigger: 'axis'
                },
              legend:
                {
                    data:['Cost']
              },
              calculable: true,
              xAxis: [
                {
                    type: 'category',
                  data: [";
            for (int d = 0; d < 12; d++)
            {
                script += "'" + depStartDate.AddMonths(d).ToString("MMM") + "'";
                if (d < 11)
                {
                    script += ",";
                }
            }
            script += @"]
                }
              ],
              yAxis: [
                {
                    type: 'value'
                }
              ],
              series: [
                {
                    name: 'Cost',
                  type: 'bar',";
            script += "data:[";
            for (int d = 0; d < 12; d++)
            {
                script += Cost[Convert.ToInt16(depStartDate.AddMonths(d).ToString("MM"))];
                if (d < 11)
                {
                    script += ",";
                }
            }
            script += "],";
            script += @"markPoint:
                    {
                        data: [
                      { type: 'max', name: 'Max'},
                      { type: 'min', name: 'Min'}
                    ]
                  },
                  markLine:
                    {
                        data: [
                      { type: 'average', name: 'Average'}
                    ]
                  }
                }
              ]
            }";
            script += "\"style = \"height: 300px\" >";
            script += "</ div >";


            ltrMonthlywise.Text = script.ToString();

        }

        protected void generateAssetVsADChart(string Months = "")
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());

            DateTime depStartDate = DateTime.Parse("01-JAN-" + DateTime.Now.Year);

            if (Months != "")
            {
                depStartDate = DateTime.Now.AddMonths(Convert.ToInt32("-" + Months) + 1);
            }

            if (chkLastYear.Checked)
            {

                dt = new DataTable();
                string StartDate = "01-JAN-" + DateTime.Now.Year.ToString();
                dt = objDB.GetAllAccountingDepreciations(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        depStartDate = DateTime.Parse(dt.Rows[0]["DEPRECIATION_START_DATE"].ToString());
                        StartDate = DateTime.Parse(dt.Rows[0]["DEPRECIATION_START_DATE"].ToString()).ToString("dd-MMM");
                        if (DateTime.Parse(StartDate).Month > DateTime.Now.Month)
                        {
                            StartDate = StartDate + "-" + (DateTime.Now.Year - 1).ToString();
                        }
                        else
                        {
                            StartDate = StartDate + "-" + DateTime.Now.Year.ToString();
                        }

                    }
                }

                string EndDate = DateTime.Parse(StartDate).AddMonths(12).ToString("dd-MMM-yyyy");

                objDB.StartDate = StartDate;
                objDB.EndDate = EndDate;
                dt = objDB.GetAssetCostBYDepDate(ref errorMsg);
            }
            else
            {
                objDB.Month = ("-" + Months);
                dt = objDB.GetAssetCostByLastMonth(ref errorMsg);
            }
            string[] Cost = new string[] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };
            string[] AD = new string[] { "0", "0", "0", "0", "0", "0", "0", "0", "10", "0", "200", "0", "0" };

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Cost[int.Parse(dt.Rows[i]["MON"].ToString())] = dt.Rows[i]["TOT_COST"].ToString();
                    }
                }
            }


            string script = "<div data-plugin=\"chart\" data-options=\"{";
            script += @" tooltip : {
                trigger: 'axis'
              },
              legend: {
                data:['Cost']
              },
              calculable : true,
              xAxis : [
                {
                  type : 'category',
                  data : [";
            for (int d = 0; d < 12; d++)
            {
                script += "'" + depStartDate.AddMonths(d).ToString("MMM") + "'";
                if (d < 11)
                {
                    script += ",";
                }
            }
            script += @"]
                }
              ],
              yAxis : [
                {
                  type : 'value'
                }
              ],
              series : [
                {
                  name:'Cost',
                  type:'line',";
            script += "data:[";
            for (int d = 0; d < 12; d++)
            {
                script += Cost[Convert.ToInt16(depStartDate.AddMonths(d).ToString("MM"))];
                if (d < 11)
                {
                    script += ",";
                }
            }
            script += "],";

            script += @"markPoint : {
                    data : [
                      {type : 'max', name: 'Max'},
                      {type : 'min', name: 'Min'}
                    ]
                  },
                  markLine : {
                    data : [
                      {type : 'average', name: 'Average'}
                    ]
                  }
                }
              ]";
            script += " }\"";
            script += "style=\"height: 300px;\">";
            script += "</div>";



            ltrCostVsAcc.Text = script.ToString();


        }
        protected void generateAssetCategoriesChart(string Months = "")
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());

            DateTime depStartDate = DateTime.Parse("01-JAN-" + DateTime.Now.Year);

            if (Months != "")
            {
                depStartDate = DateTime.Now.AddMonths(Convert.ToInt32("-" + Months) + 1);
            }

            if (chkLastYear.Checked)
            {
                //string CurrentYear = DateTime.Now.Year.ToString();

                dt = new DataTable();
                string StartDate = "01-JAN-" + DateTime.Now.Year.ToString();
                dt = objDB.GetAllAccountingDepreciations(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        depStartDate = DateTime.Parse(dt.Rows[0]["DEPRECIATION_START_DATE"].ToString());
                        StartDate = DateTime.Parse(dt.Rows[0]["DEPRECIATION_START_DATE"].ToString()).ToString("dd-MMM");
                        if (DateTime.Parse(StartDate).Month > DateTime.Now.Month)
                        {
                            StartDate = StartDate + "-" + (DateTime.Now.Year - 1).ToString();
                        }
                        else
                        {
                            StartDate = StartDate + "-" + DateTime.Now.Year.ToString();
                        }

                    }
                }

                string EndDate = DateTime.Parse(StartDate).AddMonths(12).ToString("dd-MMM-yyyy");

                objDB.StartDate = StartDate;
                objDB.EndDate = EndDate;
                dt = objDB.GetAssetCatByDepDate(ref errorMsg);
            }
            else
            {
                objDB.Month = ("-" + Months);
                dt = objDB.GeTAssetCatByLastMonth(ref errorMsg);
            }

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    string script = "<div data-plugin=\"chart\" data-options=\"{";
                    script += "tooltip:";
                    script += "{";
                    script += "trigger: 'item',";
                    script += "formatter: '{a} <br/>{b}: {c} ({d}%)'";
                    script += "},";
                    script += "legend:";
                    script += "{";
                    script += "orient: 'vertical',";
                    script += "x: 'left',";
                    script += "data:[";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        script += ("&quot;" + dt.Rows[i]["ASSET_TYPE_NAME"].ToString() + "&quot;");
                        if (i < (dt.Rows.Count - 1))
                        {
                            script += ',';
                        }

                    }

                    script += "]";
                    script += "},";
                    script += "series: [";
                    script += "{";
                    script += "name: 'Assets Calculations',";
                    script += "type: 'pie',";
                    script += "radius: ['50%', '70%'],";
                    script += "avoidLabelOverlap: false,";
                    script += "label:";
                    script += "{";
                    script += "normal:";
                    script += "{";
                    script += "show: false,";
                    script += "position: 'center'";
                    script += "},";
                    script += "emphasis:";
                    script += "{";
                    script += "show: true,";
                    script += "textStyle:";
                    script += "{";
                    script += "fontSize: '30',";
                    script += "fontWeight: 'bold'";
                    script += "}";
                    script += "}";
                    script += "},";
                    script += "labelLine:";
                    script += "{";
                    script += "normal:";
                    script += "{";
                    script += "show: false";
                    script += "}";
                    script += "},";
                    script += "data:[";


                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        script += ("{value: " + dt.Rows[i]["TOT_ASSET"].ToString() + ", name:&quot;" + dt.Rows[i]["ASSET_TYPE_NAME"].ToString() + "&quot;}");
                        if (i < (dt.Rows.Count - 1))
                        {
                            script += ',';
                        }
                    }

                    script += "]";
                    script += "}";
                    script += "]";
                    script += "}";
                    script += "\" style = \"height: 300px; \"></div>";

                       // ltrAssetCatChart.Text = script.ToString();
                }
            }

        }

        protected void checkPercentage_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                if (chkOther.Checked)
                {
                    txtMonth.Disabled = false;

                }
                else
                {
                    txtMonth.Disabled = true;
                }

                if (chkLastYear.Checked)
                {
                    generateAssetVsADChart();
                    generateBookValueChart();
                    generateWidget();
                    generateAssetCategoriesChart();

                }
                else if (chkSixMonth.Checked)
                {
                    generateAssetVsADChart("6");
                    generateBookValueChart("6");
                    generateWidget("6");
                    generateAssetCategoriesChart("6");

                }
                else if (chkThreeMonth.Checked)
                {
                    generateAssetVsADChart("3");
                    generateBookValueChart("3");
                    generateWidget("3");
                    generateAssetCategoriesChart("3");

                }

            }
            catch (Exception ex)
            {
            }
        }

        protected void btnFilter2_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (chkOther.Checked)
                {
                    generateAssetVsADChart(txtMonth.Value);
                    generateBookValueChart(txtMonth.Value);
                    generateWidget(txtMonth.Value);
                    generateAssetCategoriesChart(txtMonth.Value);
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }

        protected void DeptMonthyear_TextChanged(object sender, EventArgs e)
        {
            GetDeptWiseChartData();
        }


        protected void txtLocationWise_TextChanged(object sender, EventArgs e)
        {
            GetPayrollDataLocationWise(); 
        }
        
    }
}