﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Technofinancials.Finance.Dashboard" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>

        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Finance </h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <section class="app-content">
                    <div class="row" style="display: none;">
                        <div class="col-sm-12 pull-right mg-bottom">
                            <div class="">
                                <a class="dropdown-toggle btn" data-toggle="dropdown" runat="server" id="btnFilter">Filters <i class="fa fa-sliders" aria-hidden="true"></i>
                                </a>

                            </div>
                        </div>
                        <div class="col-sm-12" id="btnFilter-info">

                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-3 label-text">
                                            <asp:RadioButton runat="server" ID="chkLastYear" OnCheckedChanged="checkPercentage_CheckedChanged" AutoPostBack="true" GroupName="inlineRadioOptions5" Text="Current year" Checked="true" />
                                        </div>
                                        <div class="col-sm-3 label-text">
                                            <asp:RadioButton runat="server" ID="chkSixMonth" OnCheckedChanged="checkPercentage_CheckedChanged" AutoPostBack="true" GroupName="inlineRadioOptions5" Text="Last 06 Month" />
                                        </div>
                                        <div class="col-sm-3 label-text">
                                            <asp:RadioButton runat="server" ID="chkThreeMonth" OnCheckedChanged="checkPercentage_CheckedChanged" AutoPostBack="true" GroupName="inlineRadioOptions5" Text="Last 03 Month" />
                                        </div>
                                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                            <ContentTemplate>
                                                <div class="col-sm-3 label-text">
                                                    <asp:RadioButton runat="server" ID="chkOther" OnCheckedChanged="checkPercentage_CheckedChanged" AutoPostBack="true" GroupName="inlineRadioOptions5" Text="Other" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>


                                    <%-- <div class="form-group">
                                        <label class="radio-inline">
                                            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
                                            Last Year
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
                                            Last 6 Months
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" />
                                            Last 3 Months
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option4" />
                                            Others
                                        </label>
                                    </div>--%>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group" id="otherContainer" runat="server">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <div class="col-sm-9">
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtMonth" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="FilterValidater" ForeColor="Red" />
                                                    <input type="text" id="txtMonth" runat="server" class="form-control" placeholder="Please enter No. of Months" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div class="col-sm-3">
                                            <button id="btnFilter2" class="tf-save-btn" runat="server" onserverclick="btnFilter2_ServerClick" validationgroup="FilterValidater" type="button" data-original-title="Filter"><i class="fas fa-filter"></i></button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row" style="display: none;">
                        <div class="col-md-3 col-sm-6">
                            <div class="widget stats-widget">
                                <div class="widget-body clearfix">
                                    <div class="pull-left">
                                        <h3 class="widget-title text-primary text-Addition"><span class="counter" data-plugin="counterUp">
                                            <asp:Literal Text="text" ID="ltrAdditions" runat="server" /></span></h3>
                                        <small class="text-color">Additions</small>
                                    </div>
                                    <span class="pull-right big-icon watermark"><i class="fa fa-paperclip"></i></span>
                                </div>
                                <footer class="widget-footer bg-primary bg-Addition">
                                    <small></small>
                                    <span class="small-chart pull-right" data-plugin="sparkline" data-options="[4,3,5,2,1], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }">
                                        <canvas width="33" height="16" style="display: inline-block; width: 33px; height: 16px; vertical-align: top;"></canvas>
                                    </span>
                                </footer>
                            </div>
                            <!-- .widget -->
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="widget stats-widget">
                                <div class="widget-body clearfix">
                                    <div class="pull-left">
                                        <h3 class="widget-title text-danger"><span class="counter" data-plugin="counterUp">
                                            <asp:Literal Text="text" ID="ltrDisposal" runat="server" /></span></h3>
                                        <small class="text-color">Disposals</small>
                                    </div>
                                    <span class="pull-right big-icon watermark"><i class="fa fa-ban"></i></span>
                                </div>
                                <footer class="widget-footer bg-danger">
                                    <small></small>
                                    <span class="small-chart pull-right" data-plugin="sparkline" data-options="[1,2,3,5,4], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }">
                                        <canvas width="33" height="16" style="display: inline-block; width: 33px; height: 16px; vertical-align: top;"></canvas>
                                    </span>
                                </footer>
                            </div>
                            <!-- .widget -->
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="widget stats-widget">
                                <div class="widget-body clearfix">
                                    <div class="pull-left">
                                        <h3 class="widget-title text-success"><span class="counter" data-plugin="counterUp">
                                            <asp:Literal Text="text" ID="ltrCWIP" runat="server" /></span></h3>
                                        <small class="text-color">CWIP</small>
                                    </div>
                                    <span class="pull-right big-icon watermark"><i class="fa fa-unlock-alt"></i></span>
                                </div>
                                <footer class="widget-footer bg-success">
                                    <small></small>
                                    <span class="small-chart pull-right" data-plugin="sparkline" data-options="[2,4,3,4,3], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }">
                                        <canvas width="33" height="16" style="display: inline-block; width: 33px; height: 16px; vertical-align: top;"></canvas>
                                    </span>
                                </footer>
                            </div>
                            <!-- .widget -->
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="widget stats-widget">
                                <div class="widget-body clearfix">
                                    <div class="pull-left">
                                        <h3 class="widget-title text-warning"><span class="counter" data-plugin="counterUp">
                                            <asp:Literal Text="text" ID="ltrAssetRev" runat="server" /></span></h3>
                                        <small class="text-color">Revalued Assets</small>
                                    </div>
                                    <span class="pull-right big-icon watermark"><i class="fa fa-file-text-o"></i></span>
                                </div>
                                <footer class="widget-footer bg-warning">
                                    <small></small>
                                    <span class="small-chart pull-right" data-plugin="sparkline" data-options="[5,4,3,5,2],{ type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }">
                                        <canvas width="33" height="16" style="display: inline-block; width: 33px; height: 16px; vertical-align: top;"></canvas>
                                    </span>
                                </footer>
                            </div>
                            <!-- .widget -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget">
                                <header class="panel-heading">
                                    <div class="col-sm-9">
                                        <div class="heading">
                                            Payroll Break-Up
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Month</label>
                                        <asp:TextBox runat="server" CssClass="form-control month-picker" ID="txtPayrollBreakup" />

                                    </div>
                                </header>
                                <!-- .widget-header -->
                                <div class="widget-body">
                                    <canvas id="BreakupPayrollChart" width="800" height="450"></canvas>


                                </div>
                                <!-- .widget-body -->
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="widget">
                                <header class="panel-heading">
                                    <div class="col-sm-7">
                                        <div class="heading">
                                            Monthly Payroll
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <label>Year</label>
                                        <asp:TextBox runat="server" CssClass="form-control year-picker" ID="txtMonthly" />

                                    </div>
                                </header>
                                <!-- .widget-header -->
                                <div class="widget-body" style="height: 360px; padding-top: 0px;">
                                    <canvas id="MonthlyPayrollChart" width="800" height="450"></canvas>
                                    <%--                                 <asp:Literal Text="text" ID="ltrBookValueChart" runat="server" />--%>
                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                        <div class="col-md-6">
                            <asp:DropDownList runat="server" ID="ddlCat">
                            </asp:DropDownList>
                            <div class="widget">
                                <header class="panel-heading">
                                    <div class="col-sm-7">
                                        <div class="heading">
                                            Location Wise Payroll
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <label>Year</label>
                                        <asp:TextBox runat="server" CssClass="form-control year-picker" ID="txtLocationWise" AutoPostBack="true" OnTextChanged="txtLocationWise_TextChanged" />

                                    </div>
                                </header>
                                <!-- .widget-header -->
                                <div class="widget-body" style="height: 360px;">
                                    <canvas id="LocationWiseChart"></canvas>


                                </div>
                            </div>
                            <!-- .widget-body -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="widget">

                                <header class="panel-heading">
                                    <div class="col-sm-7">
                                        <div class="heading">
                                            Full And Final Settlement
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <label>Year</label>
                                        <asp:TextBox runat="server" CssClass="form-control year-picker" ID="txtFullAndFinalSettlement" AutoPostBack="true" />

                                    </div>
                                </header>
                                <!-- .widget-header -->
                                <div class="widget-body" style="height: 400px; padding-top: 0px;">
                                    <script src="https://code.highcharts.com/highcharts.js"></script>
                                    <script src="https://code.highcharts.com/modules/exporting.js"></script>
                                    <script src="https://code.highcharts.com/modules/export-data.js"></script>
                                    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

                                    <figure class="highcharts-figure">
                                        <div id="container"></div>
                                    </figure>

                                    <asp:Literal Text="text" ID="ltrCostVsAcc" runat="server" />

                                </div>
                                <!-- .widget-body -->
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="widget">
                                <header class="panel-heading">
                                    <div class="col-sm-7">
                                        <div class="heading">
                                            Department Wise Payroll
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <label>Month</label>
                                        <asp:TextBox runat="server" CssClass="form-control month-picker" ID="DeptMonthyear" AutoPostBack="true" OnTextChanged="DeptMonthyear_TextChanged" />

                                    </div>
                                </header>
                                <!-- .widget-header -->
                                <div class="widget-body" style="height: 400px;">
                                    <canvas id="DepartmentWiseChart" width="500" height="450"></canvas>

                                </div>
                                <!-- .widget-body -->
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>



                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>

        <style>
            label {
                margin: 0;
                font-weight: 700;
                padding-right: 10px;
                margin-top: 5px;
            }

            .col-sm-3 {
                display: inline-flex;
            }

            canvas#canvas {
                height: 360px !important;
            }

            .col-sm-5 {
                display: inline-flex
            }

            #btnFilter {
                padding: 10px 15px;
                background: #575757;
                color: #fff;
                cursor: pointer;
                display: inline-block
            }

            canvas#BreakupPayrollChart {
                height: 450px !important;
            }

            span.pull-right {
                line-height: 0.6;
            }

            canvas#DepartmentWiseChart {
                height: 370px !important;
            }

            span.pull-right {
                font-size: 20px;
                color: #003780;
            }

            #btnFilter-info {
                display: none;
                text-align: center;
                padding-bottom: 20px;
            }

            .bg-warning, .warning {
                background-color: #999 !important;
                color: #fff;
            }

            .text-warning {
                color: #999 !important;
            }

            .bg-success, .success {
                background-color: #8d98b3 !important;
                color: #fff;
            }

            .text-success {
                color: #8d98b3 !important;
            }


            .bg-danger, .danger {
                background-color: #b6a2de !important;
                color: #fff;
            }

            .text-danger {
                color: #b6a2de !important;
            }

            .bg-Addition {
                background-color: #2ec7c9 !important;
            }

            .text-Addition {
                color: #2ec7c9 !important;
            }

            .label-text label {
                /*display: inline-block !important;*/
                margin-top: 0px !important;
            }

            #txtMonth {
                margin-top: 5px;
            }

            #btnFilter2 {
                padding: 0px !important;
                height: 36px !important;
                width: 36px !important;
                border-radius: 0px !important;
                margin-top: 6px;
                float: left;
            }

            g.highcharts-exporting-group {
                display: none !important;
            }

            .container {
                width: 80%;
                margin: 15px auto;
            }

            text.highcharts-credits {
                display: none;
            }

            .img-responsive {
                display: inline-block;
            }

            .widget {
                box-shadow: 0 2px 3px 0px #ccc;
                background: #e8e8e8;
                border-radius: 20px;
            }

            .heading {
                color: #212529 !important;
                margin-top: 0px;
                font-size: 1.25rem;
                padding-top: 0px !important;
                padding-left: 0px !important;
                font-weight: 700;
                padding: 15px;
                text-transform: capitalize;
            }
        </style>

        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js%20%20Copy"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0-rc.1/Chart.js"></script>
        <script>
            function DoPostBack(id) {
                __doPostBack(id, "TextChanged");
            }

        </script>
        <asp:Literal ID="ltrMonthlywise" runat="server"> </asp:Literal>
        <asp:Literal ID="ltrLocationWise" runat="server">
        </asp:Literal>

        <asp:Literal ID="ltrDeptWise" runat="server"></asp:Literal>



        <asp:Literal ID="ltrBreakup" runat="server"></asp:Literal>
        <script> 
            var payrollchart = new Chart(document.getElementById('BreakupPayrollChart'),
                {
                    type: 'bar',
                    data:
                    {
                        labels: ['LeaveEncashment', 'OverTimeTotalAmount', 'ExemptGraduity', 'IESSI', 'GrossSalary', 'Commission', 'OtherExpenses', 'HouseRent', 'Tax', 'OtherBonuses', 'TaxableIncome', 'AdditionalTax', 'EmployeePF', 'TotalDeductions', 'Basic Salary', 'Severance', 'EOBI', 'OvertimeGeneralHours', 'UtilityAllowence', 'Advance', 'NetSalry', 'CompanyPF', 'TotalBonuses', 'OverTimeTotalHours', 'EidBonus', 'MedicalAllowence', 'Graduity', 'Arrears', 'CompanyEOBI', 'Deductions', 'Spiffs', 'OvertimeHolidayHours'],
                        datasets: [{
                            label: '',
                            backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850', '#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850', '#2eccc1', '#c45850', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd'],
                            data: [1267047, 5012211, 0, 0, 104930281, 48556550, 5424000, 24485040, 3609683, 2129271, 0, 5955953, 5234823, 15008009, 69956986, 0, 207550, 0, 3492593, 0, 157501827, 5234823, 3087548, 0, 0, 6995662, 3545799, 0, 1037750, 0, 686400, 0]
                        }]
                    },
                    options: {
                        legend: { display: false }, scales: { xAxes: [{ ticks: { autoSkip: false } }] },
                        title: { display: true, text: '' }
                    }
                }); </script>
        <script>    


            $(document).ready(function () {
                $("#btnFilter").click(function () {
                    $("#btnFilter-info").slideToggle(1000);
                });
            });
        </script>
        <script>
            Highcharts.chart('container', {
                chart: {
                    type: ''
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Full And Final Settlement',
                    data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                }]
            });


        </script>
    </form>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function () {
        $(".month-pickercustom").datetimepicker({
            constrainInput: true,
            format: 'MMM-YYYY'
        });
    });
    $(document).ready(function () {      
        var BreakupPRChart =  new Chart(document.getElementById('BreakupPayrollChart'),
            {
                type: 'bar',
                data:
                {
                    labels: [],
                    datasets: [{
                        label: '',
                        backgroundColor: [],
                        data: [],
                    }]
                },
                options: {
                    legend: { display: false }, scales: { xAxes: [{ ticks: { autoSkip: false } }] },
                    title: { display: true, text: '' }
                }
            });
        var monthlyPRChart = new Chart(document.getElementById('MonthlyPayrollChart'),
            {
                type: 'bar',
                data: {
                    labels: [],
                    datasets: [{
                        label: '',
                        backgroundColor: [],
                        data: []
                    }]
                },
                options: {
                    legend: { display: false },
                    scales: { xAxes: [{ ticks: { autoSkip: false } }] }, title: { display: true, text: '' }
                }
            });
        var LocationWisePRChart = new Chart(document.getElementById('LocationWiseChart'),
            {
                type: 'pie',
                data: {
                    labels: [],
                    datasets: [{
                        backgroundColor: [],
                        data: [],
                    }]
                }
            });
        var DepartmentWisePRChart = new Chart(document.getElementById('DepartmentWiseChart'),
            {
                type: 'horizontalBar',
                data: {
                    labels: [],
                    datasets: [{
                        label: 'd',
                        backgroundColor: [],
                        data: []
                    }]
                }, options: { legend: { display: false }, title: { display: true, text: '' } }
            });
        $('#txtPayrollBreakup').on('change dp.change', function (e) {
            debugger;
            var val = this.value;
            $.ajax  (
                {
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json',
                    url: '/business/services/EssService.asmx/GetBreakupChartData',
                    data: '{ month: ' + JSON.stringify(val) + '}',
                    success:
                        function (response) {
                            debugger;
                            var a = JSON.parse(response.d);
                            BreakupPRChart.data.labels = a.catagory;
                            BreakupPRChart.data.datasets[0].data = a.Totals;
                            BreakupPRChart.data.datasets[0].backgroundColor = a.Colors;
                            BreakupPRChart.update();                             
                          
                        },
                    error:
                        function (response) {
                            debugger;
                            console.writeline("Error: " + response);
                        }
                });

        });
        $('#txtMonthly').on('change dp.change', function (e) {
            debugger;
            var val = this.value;
            $.ajax(
                {
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json',
                    url: '/business/services/EssService.asmx/GetMonthlyWiseChartData',
                    data: '{ year: ' + JSON.stringify(val) + '}',
                    success:
                        function (response) {
                            debugger;
                            var a = JSON.parse(response.d);
                            monthlyPRChart.data.labels = a.months;
                            monthlyPRChart.data.datasets[0].data = a.Totals;
                            monthlyPRChart.data.datasets[0].backgroundColor = a.Colors;
                            monthlyPRChart.update();                             
                        },
                    error:
                        function (response) {
                            debugger;
                            console.writeline("Error: " + response);
                        }
                });

            //__doPostBack('txtMonthly', '');
        });
        $('#txtLocationWise').on('change dp.change', function (e) {
            debugger;
            var val = this.value;
            $.ajax(
                {
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json',
                    url: '/business/services/EssService.asmx/GetPayrollDataLocationWise',
                    data: '{ month: ' + JSON.stringify(val) + '}',
                    success:
                        function (response) {
                            debugger;
                            var a = JSON.parse(response.d);   
                            LocationWisePRChart.data.labels = a.cities;
                            LocationWisePRChart.data.datasets[0].data = a.Totals;
                            LocationWisePRChart.data.datasets[0].backgroundColor = a.Colors;
                            LocationWisePRChart.update();                             
                        },
                    error:
                        function (response) {
                            debugger;
                            console.writeline("Error: " + response);
                        }
                });
            //__doPostBack('txtLocationWise', '');
        });
        $('#DeptMonthyear').on('change dp.change', function (e) {
            debugger;
            var val = this.value;
            $.ajax(
                {
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json',
                    url: '/business/services/EssService.asmx/GetDeptWiseChartData',
                    data: '{ month: ' + JSON.stringify(val) + '}',
                    success:
                        function (response) {
                            debugger;
                            var a = JSON.parse(response.d);
                            DepartmentWisePRChart.data.labels = a.departments;
                            DepartmentWisePRChart.data.datasets[0].data = a.Totals;
                            DepartmentWisePRChart.data.datasets[0].backgroundColor = a.Colors;
                            DepartmentWisePRChart.update();                                                        
                        },
                    error:
                        function (response) {
                            debugger;
                            console.writeline("Error: " + response);
                        }
                });
            //__doPostBack('DeptMonthyear', '');
        });
    });
</script>
