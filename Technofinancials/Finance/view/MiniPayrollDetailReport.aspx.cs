﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.view
{
	public partial class MiniPayrollDetailReport : System.Web.UI.Page
	{
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                BindEmployeeDropdown();
                divAlertMsg.Visible = false;

            }
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void BindEmployeeDropdown()
        {
            try
            {
                ddlEmployee.DataSource = null;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyIDonly(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void GetData(object sender, EventArgs e)
        {
            CheckSessions();
            DataTable dt = new DataTable();
            if (ddlEmployee.SelectedValue != "0")
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.PayrollDate = txtPayrollDate.Text;
                dt = objDB.GetPayMiniSlipKTByEmployeeID(ref errorMsg);
                gv.DataSource = dt;
                gv.DataBind();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                }
                Common.addlog("ViewAll", "Finance", "All MiniPayroll Viewed", "MiniPayroll");
            }


        }

        protected void ddlYear_SelectedIndexChangedNew(object sender, EventArgs e)
        {
            CheckSessions();
            DataTable dt = new DataTable();
            if (ddlEmployee.SelectedValue != "0")
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.PayrollDate = txtPayrollDate.Text;
                dt = objDB.GetPaySlipKTByEmployeeID(ref errorMsg);
                gv.DataSource = dt;
                gv.DataBind();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                }
                Common.addlog("ViewAll", "Finance", "All NewPayroll Report Viewed", "NewPayroll");
            }
            else
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.PayrollDate = txtPayrollDate.Text;
                dt = objDB.GetPaySlipKTByEmployeeIDNew(ref errorMsg);
                gv.DataSource = dt;
                gv.DataBind();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                }
                Common.addlog("ViewAll", "Finance", "All NewPayroll Report Viewed", "NewPayroll");
            }
        }
    }
}