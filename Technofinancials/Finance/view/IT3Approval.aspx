﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IT3Approval.aspx.cs" Inherits="Technofinancials.Finance.view.IT3Approval" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        div#myModal2 {
            background-color: #3b3e4796 !important;
        }

        .modal-content .content-header {
            border-top-left-radius: 11px;
            border-top-right-radius: 11px;
            padding-top: 10px;
        }

        div#myModal2 .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
            line-height: 1.3 !important;
        }

        .AD_btnn.two {
            font-size: 14px !important;
            margin: 0 !important;
            padding: 0 !important;
            cursor: pointer;
        }

        .AD_btnn:hover {
            color: #000;
            border-bottom: 2px solid #000 !important;
        }

        .AD_btnn {
            margin: 0 10px;
            font-size: 18px;
            font-weight: 700;
            color: #003780;
            background: none;
            border: 0;
            border-bottom: 2px solid transparent !important;
            border-radius: 0;
            padding: 5px;
        }

        button.multiselect.dropdown-toggle.custom-select.text-center span.multiselect-selected-text {
            white-space: nowrap;
        }

        .open > .dropdown-menu {
            display: block;
            width: 300px !important
        }

            .open > .dropdown-menu ::-webkit-scrollbar {
                width: 10px;
            }

        .dropdown-menu {
            box-shadow: none !important;
        }

        .multiselect-container.dropdown-menu {
            overflow: hidden;
        }

        button.multiselect.dropdown-toggle.custom-select.text-center {
            width: 200px;
        }

        button.multiselect.dropdown-toggle.custom-select.text-center {
            background-color: #fff;
            border: 1px solid #aaa;
            border-radius: 5px;
            padding-left: 8px;
            padding-right: 8px;
            padding: 3px 37px;
        }

        .open > .dropdown-menu {
            padding: 15px;
            max-height: 250px !important;
            border: 1px solid #aaa;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        multiselect-filter {
            margin-bottom: 15px;
        }

        .multiselect-container.dropdown-menu {
            overflow: overlay !important;
        }

        .multiselect-container .multiselect-all .form-check, .multiselect-container .multiselect-group .form-check, .multiselect-container .multiselect-option .form-check {
            padding: 0px;
        }

        label.form-check-label.font-weight-bold {
            margin: 0px;
        }

        span label {
            font-size: 14px !important;
            margin: 0px;
        }

        @media only screen and (max-width: 1280px) and (min-width: 800px) {
            section.app-content .col-lg-4.col-md-6.col-sm-12 {
                width: 40%;
            }

            button.multiselect.dropdown-toggle.custom-select.text-center {
                padding: 3px 25px !important;
            }
        }

        @media only screen and (max-width: 1440px) and (min-width: 900px) {

            button.multiselect.dropdown-toggle.custom-select.text-center {
                padding: 3px 0px !important;
                width: 170px;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 993px) {
            button.multiselect.dropdown-toggle.custom-select.text-center {
                padding: 3px 15px !important;
                width: 130px;
            }
        }

        @media only screen and (max-width: 992px) and (min-width: 768px) {
            button.multiselect.dropdown-toggle.custom-select.text-center {
                padding: 3px 50px;
                width: 80px;
            }

            .app-content .form-group h4 {
                font-size: 10px !important;
            }
        }

        @media only screen and (max-width: 1599px) and (min-width: 1201px) {
            button.multiselect.dropdown-toggle.custom-select.text-center {
                padding: 3px 37px;
            }
        }

        .multiselect-container.dropdown-menu {
            overflow: hidden auto !important;
        }

        .navbar-toolbar > li > .dropdown-menu {
            width: 190px !important;
        }

        .open > .dropdown-menu {
            max-height: 300px !important;
        }

        .dash_User {
            width: 190px !important;
            border-radius: 10px;
            padding: 0 !important;
            border: 1px solid rgba(0,0,0,.15);
        }

        /*        .content-header .AD_btn {
            font-size: 15px;
        }*/

        a#lnkReject {
            font-weight: bold !important;
        }

        .navbar-toolbar > li > .dropdown-menu {
            box-shadow: 0 3px 12px rgb(0 0 0 / 18%) !important;
        }

        @media only screen and (max-width: 992px) and (min-width: 768px) {
            h1.m-0.text-dark {
                font-size: 17px !important;
            }
        }
    </style>
</head>


<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <div class="modal fade M_set in" id="myModal2" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <h1 class="m-0 text-dark">IT-3 Approvals</h1>
                                <input name="AttendanceID" type="hidden" id="txtAttendanceID" value="0" />
                                <input name="It3ID2" id="It3ID2" type="hidden" value="0" />

                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <div style="text-align: right;">
                                    <span class="AD_btnn two">Close</span>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="start-date">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Name</h4>
                                <br />

                                <div class="input-group date">
                                    <input class="form-control" name="EmployeeName" style="width: 100%;" type="text" id="EmployeeName" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Payroll Cycle</h4>
                                <br />
                                <div class="input-group date">
                                    <input class="form-control" name="PayrollCycle" type="text" id="PayrollCycless" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Total Claim</h4>
                                <br />
                                <div class="input-group date">
                                    <input class="form-control" name="TotalCalim" type="text" id="TotalCalim" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Remarks</h4>
                                <br />
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="RamarksAdjust" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">&nbsp;</div>

                <div class="SaveBTn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <center><a class="AD_btn_inn" onclick="AddAdjustment()" id="modalbtnSave">Approve</a></center>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>

            </div>
        </div>

    </div>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">


            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">IT-3 Approvals</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4 col-md-8" style="text-align: right;">
                                <div style="text-align: right;">
                                    <button class="AD_btn" visible="false" id="btnApprove" runat="server" onserverclick="btnApprove_ServerClick" type="button"><%--<i class="fa fa-thumbs-up"></i>--%> Approve</button>
                                    <button class="AD_btn"  visible="false"  id="btnDisapprove" runat="server" onserverclick="btnDisApprove_ServerClick" type="button" title="Reject"><%--<i class="fa fa-thumbs-down"></i>--%> Reject</button>

                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                 
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
             
                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="col-md-6">
                                     
                                         <div class="form-group">

                                        <h4>Application Month  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="subdate" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="subdate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />

                                    </div>
                                     </div>
                                     <div class="col-sm-6">
                                            <div class="clear-fix">&nbsp;</div>
                                            <div>
                                                <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>

                                            </div>
                                        </div>
                                    <%--<div class="col-sm-6">
                                        <div class="form-group">
                                            <h4>Adjustment Status</h4>
                                            <asp:DropDownList ID="ddlAdjustmentStatus" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>--%>
                                   <%-- <div class="col-sm-6">
                                        <div class="form-group">
                                            <h4>Employee<span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="lstEmployee" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            <br />
                                            <asp:ListBox SelectionMode="Multiple" ID="lstEmployee" runat="server"></asp:ListBox>
                                        </div>
                                    </div>--%>


                                </div>
                             
                            </div>
                            <div class="row">
                            </div>
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12">
                                        <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv dataTable no-footer" ClientIDMode="Static" AutoGenerateColumns="false">
                                            <Columns>



                                                <asp:TemplateField HeaderText="Sr.No.">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                               <%-- <asp:TemplateField HeaderText="">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chckchanged" ID="checkAll" ToolTip="Click To Select All"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" Enabled='<%# Eval("Status").ToString()=="Submitted"||Eval("Status").ToString()=="Saved as Draft"? true : false %>' ID="check"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="IT-3 ID">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="IT3ID" Text='<%# Eval("IT3_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="LblName" Text='<%# Eval("Created_By") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Submition Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="SubmissionDate" Text='<%# Eval("Created_Date") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Total Claim">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="TotalClaim" Text='<%# Eval("Total_Claim") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Payroll Cycle">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="PayrollCycle" Text='<%# Eval("submissionDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblDocStatus" Text='<%# Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeID" Text='<%# Eval("Employee_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="File">
                                                    <ItemTemplate>
                                                        <a href='<%# Eval("File_Path") %>'>Downlaod</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                    <%--    <button id="btnAdjustment<%# Eval("IT3_ID") %>" class="AD_stock btnAdjustment<%# Eval("IT3_ID") %>" onclick="PopUpModal('<%#  Eval("IT3_ID") %>','<%#  Eval("Created_By") %>','<%#  Eval("submissionDate") %>','<%# Eval("Total_Claim") %>')">Approve</button>--%>
                                                    <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/finance/manage/IT3Approval/edit-IT3Approval-" + Eval("IT3_ID") %>" class="AD_stock">View </a>
                                                        </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                        </asp:GridView>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <br />
                                        <br />
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </section>
                 
                    <%--<Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>--%>
          
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script>
            function PopUpModal(IT3ID, Name, PayrollCycle, TotalClaim) {

                var modal2 = document.getElementById("myModal2");
                modal2.style.display = "block";
                $("#It3ID2").val(IT3ID);
                $("#EmployeeName").val(Name);
                $("#PayrollCycless").val(PayrollCycle);
                $("#TotalCalim").val(TotalClaim);

            }

            function AddAdjustment() {
                var IT3_ID = $("#It3ID2").val();
                var remarks = $("#RamarksAdjust").val();


                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/ApproveIT3',
                    data: '{IT3_IT: ' + JSON.stringify(IT3_ID) + ', Remarks: ' + JSON.stringify(remarks) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var modal2 = document.getElementById("myModal2");
                        modal2.style.display = "none";
                        alert('Approved Successfully');
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });



            }
        </script>

        <script>

            var modal2 = document.getElementById("myModal2");
            var btn2 = document.getElementById("adjust");
            var span2 = document.getElementsByClassName("AD_btnn two")[0];

            span2.onclick = function () {
                modal2.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal2) {
                    modal2.style.display = "none";
                }
            }
        </script>
        <script type="text/javascript">  
            $(document).ready(function () {

                BindEmployees();
            });

            function BindEmployees() {
                $('[id*=lstEmployee]').multiselect({
                    includeSelectAllOption: true,
                    buttonContainer: '<div class="btn-group" />',
                    includeSelectAllIfMoreThan: 0,
                    enableFiltering: true,
                    filterPlaceholder: 'Search',
                    filterBehavior: 'text',
                    includeFilterClearBtn: true,
                    enableCaseInsensitiveFiltering: true,
                    numberDisplayed: 1,
                    maxHeight: true,
                    maxHeight: 350,
                    templates: {
                        button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span></button>',
                        //<a href = "https://www.jqueryscript.net/tags.php?/popup/" > popup</a>
                        Container: '<div class="multiselect-container dropdown-menu"></div>',
                        filter: '<div class="multiselect-filter"><div class="input-group input-group-sm p-1"><div class="input-group-prepend"></div><input class="form-control multiselect-search" type="text" /></div></div>',

                        filterClearBtn: '<div class="input-group-append"><button class="multiselect-clear-filter input-group-text" type="button"><i class="fas fa-times"></i></button></div>',

                        option: '<button class="multiselect-option dropdown-item"></button>',

                        divider: '<div class="dropdown-divider"></div>',

                        optionGroup: '<button class="multiselect-group dropdown-item"></button>',

                        resetButton: '<div class="multiselect-reset text-center p-2"><button class="btn btn-sm btn-block btn-outline-secondary"></button></div>'
                    }
                });
            };
		</script>
    </form>
</body>
</html>
