﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Absents.aspx.cs" Inherits="Technofinancials.Finance.view.Absents" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
  <style>
        th {
position: sticky;
    top: 0;
    background: #f7f7f7;
    color: #000;
        }
.green-background{
            background-color: rgb(152, 251, 152);
            color: rgb(255, 255, 255);
            font-weight: 600;
        }
        div#myModal2 {
            background-color: #3b3e4796 !important;
        }

        .modal-dialog {
            z-index: 9999;
            opacity: 1;
        }

        #modalbtnSave {
            cursor: pointer;
        }

        .modal-open {
            overflow: hidden;
        }

        .AD_btnn.two {
            font-size: 14px !important;
            margin: 0 !important;
            padding: 0 !important;
            cursor: pointer;
        }

        .AD_btnn {
            margin: 0 10px;
            font-size: 18px;
            font-weight: 700;
            color: #003780;
            background: none;
            border: 0;
            border-bottom: 2px solid transparent !important;
            border-radius: 0;
            padding: 5px;
        }

            .AD_btnn:hover {
                color: #000;
                border-bottom: 2px solid #000 !important;
            }

        .SaveBTn {
            padding: 20px 30px !important;
            margin: 0px !important;
        }

        div#myModal2 .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
            line-height: 1.3 !important;
        }

        .Time-in h4, .Time-out h4, .Breaks h4, .BreakIN h4, .Breakout h4, .Rmarks h4 {
            color: #000;
            font-weight: 600;
            margin: 10px 0 5px;
            text-align: left;
        }

        .Time-in, .Time-out, .Breaks, .BreakIN, .Breakout, .Rmarks {
            padding: 0px 15px;
            text-align: left;
        }
        .content-header h1 {
            margin-left: 8px !important;
        }

        #chartdiv {
            width: 100%;
            height: 500px;
        }

        .color_infoP h3 {
            font-size: 15px;
            margin: 0 !important;
        }

        .attendance-wrapper {
            text-align: right;
        }

        .color_infoP h3 span {
            margin-left: 15px;
        }

        .pr_0 {
            padding-right: 0 !important;
        }

        input#txtStartDate, input#txtEndDate {
            border-radius: 5px;
        }

        .form-group {
            display: block;
        }

        div#myModal .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
        }

        #LeavesGuage {
            width: 100%;
            height: 500px;
        }

        .table-bordered > tbody > tr > th {
            border-top-style: none !important;
        }

        #PieChart {
            width: 100%;
            height: 500px;
        }

        #LeavesPieChart {
            width: 100%;
            height: 500px;
        }

        .widget-body {
            padding-top: 0;
        }

        .calender {
        }

        .calenderHeading {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .form-group.color_infoP {
            width: fit-content;
            display: inline-block;
            font-size: 16px;
        }

        .content-header h1 {
            font-size: 20px !important;
        }

        .calenderCell {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .wrap.p-t-0.tf-navbar.tf-footer-wrapper {
            right: 0 !important;
        }

        g[aria-labelledby="id-66-title"] {
            display: none;
        }

        div#myModal .modal-content {
            top: 122px;
            width: 20%;
            position: fixed;
            z-index: 1050;
            left: 80%;
            height: 70%;
            box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
        }

        .modal-content .content-header {
            border-top-left-radius: 11px;
            border-top-right-radius: 11px;
            padding-top: 10px;
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
                left: 70%;
                width: 30%;
            }

            table#gvAttendanceSummary {
                width: 991px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
                left: 70%;
                width: 30%;
            }

            table#gvAttendanceSummary {
                width: 1024px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }

            table#gvAttendanceSummary {
                width: 1024px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }
        }

        @media only screen and (max-width: 1439px) and (min-width: 1200px) {
            .single-key h3 {
                font-size: 15px;
                padding: 5px;
            }
        }

        @media only screen and (max-width: 1505px) and (min-width: 1440px) {
            .single-key h3 {
                font-size: 18px;
            }
        }

        .mini-stat.clearfix.present {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #1fb5ac !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            margin-top: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.absent {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #fa8564 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.leave {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #a48ad4 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.holiday {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f4b9b9 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.missing {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f9c851 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.short {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #aec785 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat-info {
            font-size: 12px;
            padding-top: 2px;
            color: #767676 !important;
            font-weight: 700;
        }

            .mini-stat-info span {
                display: block;
                font-size: 24px;
                font-weight: 600;
                color: #767676 !important;
            }



        .mini-stat {
            border-radius: 10px !important;
        }

        .btnAdjustmentTrue {
            display: none;
        }
        .gv-overflow-scrool div {
            overflow: auto;
            overflow-y: auto;
            height: 400px;
            padding-right: 5px;
        }

        .AD_btn {
            font-size: 16px;
            border: 0;
            padding: 0px;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold theme-primary menubar-light">
    <div class="modal fade M_set in" id="myModal2" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <h1 class="m-0 text-dark">Attendance Adjustment</h1>
                                <input name="AttendanceID" type="hidden" id="txtAttendanceID" value="0" />
                                <input name="AttendanceID" type="hidden" id="txtEmployeeID" value="0" />
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <div style="text-align: right;">
                                    <span class="AD_btnn two">Close</span>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="start-date">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Date</h4>
                                <br />

                                <div class="input-group date">
                                    <input name="DateAdjust" type="text" id="DateAdjust" class="form-control datetime-picker" data-date-format="DD-MMM-YYYY" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Time In</h4>
                                <br />
                                <div class="input-group date">
                                    <input name="TimeinAdjust" type="text" id="TimeinAdjust" class="form-control datetime-picker" data-date-format="hh:mm A" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="Time-out">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Time Out</h4>
                                <br />
                                <div class="input-group date">
                                    <input name="TimeoutAdjust" type="text" id="TimeoutAdjust" class="form-control datetime-picker" data-date-format="hh:mm A" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Remarks</h4>
                                <br />
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="RamarksAdjust" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="SaveBTn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <center><a class="AD_btn_inn" onclick="AddAdjustment()" id="modalbtnSave">Save</a></center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div class="modal fade M_set in" id="BreakAdjustmentModel" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <h1 class="m-0 text-dark">Break Adjustment</h1>
                                <input name="AttendanceID" type="hidden" id="txtBreakAttendanceID" value="0" />
                                <input name="AttendanceID" type="hidden" id="txtBreakEmployeeID" value="0" />
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <div style="text-align: right;">
                                    <span class="AD_btnn three">Close</span>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="start-date">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Date</h4>
                                <br />

                                <div class="input-group date">
                                    <input name="DateAdjust" type="text" id="BreakDateAdjust" class="form-control datetime-picker" data-date-format="DD-MMM-YYYY" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Break Time In</h4>
                                <br />
                                <div class="input-group date">
                                    <input name="TimeinAdjust" type="text" id="BreakTimeinAdjust" class="form-control datetime-picker" data-date-format="hh:mm A" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="Time-out">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Break Time Out</h4>
                                <br />
                                <div class="input-group date">
                                    <input name="TimeoutAdjust" type="text" id="BreakTimeoutAdjust" class="form-control datetime-picker" data-date-format="hh:mm A" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Remarks</h4>
                                <br />
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="BreakRamarksAdjust" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="SaveBTn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <center><a class="AD_btn_inn" onclick="AddBreakAdjustment()" id="breakModalbtnSave">Save</a></center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>

        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <main id="app-main" class="app-main">
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <h1 class="m-0 text-dark">Attendance Detail</h1>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                <div style="text-align: right;">
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="app-content">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 pr_0">
                            <div class="mini-stat clearfix present">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblPresent" runat="server"></asp:Literal>
                                    </span>
                                    Present Days
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 pr_0">
                            <div class="mini-stat clearfix absent">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblAbsent" runat="server"></asp:Literal>
                                    </span>
                                    Absent Days
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 pr_0">
                            <div class="mini-stat clearfix leave">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblLeave" runat="server"></asp:Literal>
                                    </span>
                                    Leaves
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 pr_0 ">
                            <div class="mini-stat clearfix holiday">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblHoliday" runat="server"></asp:Literal>
                                    </span>
                                    Holidays
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 pr_0">
                            <div class="mini-stat clearfix missing">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblMissingPunches" runat="server"></asp:Literal>
                                    </span>
                                    Missing Punches
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 ">
                            <div class="mini-stat clearfix short">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblWorkingHour" runat="server"></asp:Literal>
                                    </span>
                                    Working Hours
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 pr_0">
                            <div class="mini-stat clearfix holiday ">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblExpectedHours" runat="server"></asp:Literal>
                                    </span>
                                    Expected Hours
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 pr_0">
                            <div class="mini-stat clearfix missing">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblShortHour" runat="server"></asp:Literal>
                                    </span>
                                    Short Hours
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 pr_0">
                            <div class="mini-stat clearfix present">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblWorkingDay" runat="server"></asp:Literal>
                                    </span>
                                    Working Days
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 pr_0">
                            <div class="mini-stat clearfix short">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblEarlyLeft" runat="server"></asp:Literal>
                                    </span>
                                    Early Leave
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2 pr_0">
                            <div class="mini-stat clearfix leave">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblLate" runat="server"></asp:Literal>
                                    </span>
                                    Late Arrivals
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-sx-12 col-lg-2">
                            <div class="mini-stat clearfix absent">
                                <div class="mini-stat-info">
                                    <span data-to="0" data-speed="1000">
                                        <asp:Literal ID="lblHalfDay" runat="server"></asp:Literal>
                                    </span>
                                    Half Days
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <div class="pull-left">
                                <button id="myBtn" class="dt-button" tabindex="0" aria-controls="gvAnnouncement" type="button">
                                    <span><i class="fa fa-filter" aria-hidden="true"></i></span>
                                </button>
                                <div id="myModal" class="modal">
                                    <!-- Modal content -->
                                    <div class="modal-content">
                                        <div class="content-header">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <h1 class="m-0 text-dark">Filter</h1>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <div style="text-align: right;">
                                                            <span class="AD_btn two">Close</span>
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </div>
                                        <div class="employee-name">
                                            <div class="row">
                                                <div class="col-md-12 form-group" data-select2-id="414">
                                                    <h4>Employees Name </h4>
                                                    <br />
                                                    <%--<select name="ddlDocType" id="ddlDocType" class="form-control select2 js-example-basic-single select2-hidden-accessible" data-plugin="select2" data-select2-id="ddlDocType" tabindex="-1" aria-hidden="true">
                                                        <option value="0" data-select2-id="32">--- Please Select ---</option>
                                                        <option value="Resume" data-select2-id="376">Ali</option>
                                                        <option value="Education Certificate" data-select2-id="377">Junaid</option>
                                                        <option value="Experience Letter" data-select2-id="378">Abid</option>
                                                    </select>--%>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlEmployee" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnSearch" ForeColor="Red">*</asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlEmployee" runat="server"  style="width:100% !important;" class="form-control select2 js-example-basic-single select2-hidden-accessible" data-plugin="select2" data-select2-id="ddlDocType" TabIndex="-1" aria-hidden="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="start-date">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <h4>Start Date</h4>
                                                        <br />

                                                        <div class='input-group date'>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtStartDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnSearch" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            <input type='text' id="txtStartDate"  runat="server" class="form-control datetime-picker" data-date-format="DD-MMM-YYYY" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="end-date">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <h4>End Date</h4>
                                                        <br />
                                                        <div class='input-group date'>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtEndDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnSearch" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            <input type='text' class="form-control datetime-picker" data-date-format="DD-MMM-YYYY" id="txtEndDate" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="buttons">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                        <contenttemplate>
                                                            <div class="form-group" id="divAlertMsg" runat="server" visible="false">
                                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                                    <span>
                                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                                    </span>
                                                                    <p id="pAlertMsg" runat="server">
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </contenttemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <asp:LinkButton ID="btnSearch" runat="server" class="AD_btn_inn" OnClick="btnSearch_Click">Search</asp:LinkButton> 
                                                <%--<a href="#"><span class="AD_stock"><i class="fa fa-search" aria-hidden="true"></i>Search</span></a>--%>
                                                <%--<div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                                            <a href="#"><span class="AD_stock">Refresh</span></a>
                                                        </div>--%>
                                            </div>
                                            <a href="#"></a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                            <div class="pull-right ">
                                <div class="attendance-wrapper">
                                    <div class="form-group color_infoP">
                                        <h3><span class="present-option">P</span> - Present</h3>
                                    </div>
                                    <%--<div class="form-group color_infoP">
                                        <h3><span class="overtime-option">O</span> - Overtime</h3>
                                    </div>--%>
                                    <div class="form-group color_infoP">
                                        <h3><span class="missing-option">M</span> - Missing</h3>
                                    </div>
                                    <div class="form-group color_infoP">
                                        <h3><span class="absent-option">A</span> - Absent</h3>
                                    </div>
                                    <div class="form-group color_infoP">
                                        <h3><span class="short-option">S</span> - Short Hours</h3>
                                    </div>
                                    <div class="form-group color_infoP">
                                        <h3><span class="day-option">D</span> - Day Off</h3>
                                    </div>
                                    <div class="form-group color_infoP">
                                        <h3><span class="rest-option">L</span> - Leaves</h3>
                                    </div>
                                    <div class="form-group color_infoP">
                                        <h3><span class="weekend-option">H</span> - Holidays</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="clearfix">&nbsp;</div>
                         <div class="row">
                             <div class="col-md-12">
                           <div class="tab-content">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                                                
                                     <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"  OnRowDataBound="gvAttendanceSummary_RowDataBound">
                                         <Columns>

                                                <asp:TemplateField HeaderText="Sr.No">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date" SortExpression="Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%#  Eval("Date") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Day Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCold1" Text='<%#  Eval("DayName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2_e" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Shift Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("ShiftName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Time In">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("TimeIn") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Time Out">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("TimeOut") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Time In">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCola3" Text='<%# Eval("AdjPreTimeIn") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Time Out">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCola4" Text='<%# Eval("AdjPreTimeOut") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Actual Time In">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblColp3" Text='<%# Eval("PreTimeIn") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Actual Time Out">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblColp4" Text='<%# Eval("PreTimeOut") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Time In Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCheckInStatus" Text='<%# Eval("CheckInStatus") %>'></asp:Label>
                                                        <asp:Label runat="server" ID="lblTimeIn" style="display: flex;" ></asp:Label>
                                                        <%--<span class="<%# Eval("wh") %>-option"><%# (Eval("CheckInStatus") == "Holiday")? "H":"M" %></span>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Time Out Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCheckOutStatus" Text='<%# Eval("TimeOutStatus") %>'></asp:Label>
                                                        <asp:Label runat="server" ID="lblTimeout" style="display: flex;" ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Working Hour">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblWorkingHours" Text='<%# Eval("wh") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Break Time">
                                             
                                                    <ItemTemplate>
                                                        <span  style="cursor:pointer; font-weight:bold; "  onclick="PopUpBreakModal('<%# Eval("EmployeeID") %>','<%# Eval("AttendanceID") %>','<%# (Eval("Date") == DBNull.Value ? Eval("TimeIn2") : Eval("Date")) %>','<%#  (Eval("BreakTime") == DBNull.Value ? "00:00:00" : Convert.ToDateTime(Eval("BreakTime")).ToString("hh:mm tt")) %>','<%# (Eval("BreakEndTime") == DBNull.Value ? "00:00:00" : Convert.ToDateTime(Eval("BreakEndTime")).ToString("hh:mm tt")) %>')" title="Click to request break adjustment" >   <asp:Label runat="server" ID="lblCol5" Text='<%# Eval("bt") %>'></asp:Label></a></span>
                                                         
                                                       <%-- <asp:Label runat="server" ID="lblTotalBreak" style='<%# ((Convert.ToInt32(Eval("TotalBreakTime")) / 3600) > 1 && Eval("BreakAdjustmentStatus") != DBNull.Value && Eval("BreakAdjustmentStatus") != "") ?"cursor: pointer;":"display:none"  %>' ToolTip='<%# Eval("BreakAdjustmentStatus") %>' Text='<%# Eval("bt") %>'></asp:Label>
                                                        <asp:Label runat="server" ID="Label1" style='<%# ((Convert.ToInt32(Eval("TotalBreakTime")) / 3600) > 1 ) ?"display:none":""  %>' Text='<%# Eval("bt") %>'></asp:Label>--%>
                                                    </ItemTemplate>
                                                   
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Over Time">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblOvertime" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Adjustment Approved By">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("AdjApprovedBy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Adjustments">
                                                    <ItemTemplate>
                                                            <button id="btnAdjustment<%# Eval("AttendanceID") %>" style='<%# !(Eval("Adjustmentstatus") == DBNull.Value || Eval("Adjustmentstatus") == "") ?"display:none":""  %>' class="AD_stock btn btnAdjustment<%# Eval("isAdjusted") %>" type="button" onclick="PopUpModal('<%#  Eval("EmployeeID") %>','<%#  Eval("AttendanceID") %>','<%#  (Eval("Date") == DBNull.Value ? Eval("TimeIn2") : Eval("Date")) %>','<%#  Convert.ToDateTime(Eval("TimeIn2")).ToString("hh:mm tt") %>','<%# (Eval("TimeOut2") == DBNull.Value ? "" : Convert.ToDateTime(Eval("TimeOut2")).ToString("hh:mm tt")) %>')">Add</button>
                                                            <span id="lblStatus<%# Eval("AttendanceID") %>" ><%# Eval("Adjustmentstatus") %></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                             <asp:TemplateField Visible="false">
                                                 <ItemTemplate>
                                                      <asp:Label runat="server" ID="lblAttResultant" ></asp:Label>
                                                 </ItemTemplate>
                                             </asp:TemplateField>
                                             <asp:TemplateField Visible="false">
                                                 <ItemTemplate>
                                                      <asp:Label runat="server" ID="lblShortHours" Text='<%# Eval("ShortHours") %>'></asp:Label>
                                                 </ItemTemplate>
                                             </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                                                

                                    </div>
                                </div>
                            <%--<asp:UpdatePanel runat="server">
                                <contenttemplate>
                                    <div class="container-fuild">
                                        
                                    </div>
                                </contenttemplate>
                            </asp:UpdatePanel>--%>
                        </div>
            </div>
                    </div>
            </div>
            </div>
        </main>

        <div class="clearfix">&nbsp;</div>
        <uc:Footer ID="footer1" runat="server"></uc:Footer>
        <!-- Resources -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
         
        <!-- Chart code -->
        <script>
            // Get the modal
            var modal = document.getElementById("myModal");
			var modal2 = document.getElementById("myModal2");
			var modal3 = document.getElementById("BreakAdjustmentModel");

            // Get the button that opens the modal
            var btn = document.getElementById("myBtn");
            var btn2 = document.getElementById("adjust");
           

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("AD_btn two")[0];
            var span2 = document.getElementsByClassName("AD_btnn two")[0];
			var span3 = document.getElementsByClassName("AD_btnn three")[0];




            // When the user clicks the button, open the modal 
            btn.onclick = function () {
                modal.style.display = "block";
            }

            

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
            }
             span2.onclick = function () {
                modal2.style.display = "none";
            }

			span3.onclick = function () {
				modal3.style.display = "none";
			}

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
                if (event.target == modal2) {
                    modal2.style.display = "none";
                }
            }
		</script>
        <!-- HTML -->
        <script>

            $(document).ready(function () {
                //$('#gv tr td').each(function () {
                //    if ($(this).text().trim() == 'Off') {
                //        $(this).parent().css('background-color', '#DCDCDC');               
                //    }
                //});

				$('#gv td:nth-child(13)').each(function () {
					debugger;
					if ($(this).text().trim() == 'D') {
						$(this).parent().css('background-color', '#DCDCDC');
					}
				});

                $('#gv td:nth-child(16)').each(function () {
                    
                    if ($(this).text().trim() >= "02:00:00") {
                        $(this).css('color', '#FF7F50');
                        $(this).css('font-weight', '600');
                    }
                });

				$('#gv td:nth-child(15)').each(function () {

					if ($(this).text().trim() >= "02:00:00") {
						$(this).css('color', '#FF7F50');
						$(this).css('font-weight', '600');
					}
				});

            });

            function PopUpModal(EmpID, AttID, Adjdate, timein2, timeout2) {
                var modal2 = document.getElementById("myModal2");

                modal2.style.display = "block";
                $("#txtEmployeeID").val(EmpID);
                $("#txtAttendanceID").val(AttID);
                $("#DateAdjust").val(Adjdate);
                $("#DateAdjust").prop("disabled", true);
                $("#TimeinAdjust").val(timein2);
                $("#TimeoutAdjust").val(timeout2);
            }

  
        function AddAdjustment()
            {
                var date=  $("#DateAdjust").val();
                var AttID = '#btnAdjustment' + $("#txtAttendanceID").val();
                var StatusLabel = '#lblStatus' + $("#txtAttendanceID").val();
                var Empid = $("#txtEmployeeID").val();
                var timein=  $("#TimeinAdjust").val();
                var timeout=  $("#TimeoutAdjust").val();
                var remarks=  $("#RamarksAdjust").val();
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/AttendanceAdjustment',
                    data: '{DateAdjust: ' + JSON.stringify(date) + ', timein: ' + JSON.stringify(timein) + ',timeout: ' + JSON.stringify(timeout) + ',remarks: ' + JSON.stringify(remarks) + ',EmpID: ' + JSON.stringify(Empid) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response)
                    {
                        
                        if (response.d == 'New Attendance Adjustment Added')
                        {
                            var modal2 = document.getElementById("myModal2");
                            modal2.style.display = "none";
                            $("#txtAttendanceID").val(0);
                            $(StatusLabel).html('Data Submitted for Review');
                            $(AttID).css("display", "none");
                        }

                        alert(response.d)
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });           
            }

			function PopUpBreakModal(EmpID, AttID, Adjdate, breakTimeIn, breakTimeOut) {
				var modal2 = document.getElementById("BreakAdjustmentModel");

				modal2.style.display = "block";

				$("#txtBreakEmployeeID").val(EmpID);
				$("#txtBreakAttendanceID").val(AttID);
				$("#BreakDateAdjust").val(Adjdate);
				$("#BreakTimeinAdjust").val(breakTimeIn);
				$("#BreakTimeoutAdjust").val(breakTimeOut);
				$("#BreakDateAdjust").prop("disabled", true);
			};


			function AddBreakAdjustment() {
				var date = $("#BreakDateAdjust").val();
				var AttID = '#btnAdjustment' + $("#txtBreakAttendanceID").val();
				var StatusLabel = '#lblStatus' + $("#txtBreakAttendanceID").val();
				var Empid = $("#txtBreakEmployeeID").val();
				var timein = $("#BreakTimeinAdjust").val();
				var timeout = $("#BreakTimeoutAdjust").val();
				var remarks = $("#BreakRamarksAdjust").val();
                debugger;

				$.ajax({
					type: "POST",
					url: '/business/services/EssService.asmx/BreakAdjustment',
					data: '{DateAdjust: ' + JSON.stringify(date) + ', breakTimeIn: ' + JSON.stringify(timein) + ',breakTimeOut: ' + JSON.stringify(timeout) + ',remarks: ' + JSON.stringify(remarks) + ',EmpID: ' + JSON.stringify(Empid) + '}',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response) {

						if (response.d == 'New Break Adjustment Added') {
							var modal2 = document.getElementById("BreakAdjustmentModel");
							modal2.style.display = "none";
							$("#txtAttendanceID").val(0);
							$(StatusLabel).html('Data Submitted for Review');
							$(AttID).css("display", "none");
						}

						alert(response.d);
						modal2.style.display = "none";
					},
					error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
				});
			};


		</script>

    </form>

</body>


</html>
