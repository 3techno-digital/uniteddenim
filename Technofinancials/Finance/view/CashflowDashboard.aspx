﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CashflowDashboard.aspx.cs" Inherits="Technofinancials.Finance.view.CashflowDashboard" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>

        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">



                    <div class="row">
                        <div class="col-sm-12 pull-right mg-bottom">
                            <div class="">
                                <a class="dropdown-toggle btn" data-toggle="dropdown" runat="server" id="btnFilter">Filters <i class="fa fa-sliders" aria-hidden="true"></i>
                                </a>

                            </div>


                        </div>

                        <div class="col-sm-12" id="btnFilter-info">

                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-3 label-text">
                                            <asp:RadioButton runat="server" ID="chkLastYear" OnCheckedChanged="checkPercentage_CheckedChanged" AutoPostBack="true" GroupName="inlineRadioOptions5" Text="Current year" Checked="true" />
                                        </div>
                                        <div class="col-sm-3 label-text">
                                            <asp:RadioButton runat="server" ID="chkSixMonth" OnCheckedChanged="checkPercentage_CheckedChanged" AutoPostBack="true" GroupName="inlineRadioOptions5" Text="Last 06 Month" />
                                        </div>
                                        <div class="col-sm-3 label-text">
                                            <asp:RadioButton runat="server" ID="chkThreeMonth" OnCheckedChanged="checkPercentage_CheckedChanged" AutoPostBack="true" GroupName="inlineRadioOptions5" Text="Last 03 Month" />
                                        </div>
                                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                            <ContentTemplate>
                                                <div class="col-sm-3 label-text">
                                                    <asp:RadioButton runat="server" ID="chkOther" OnCheckedChanged="checkPercentage_CheckedChanged" AutoPostBack="true" GroupName="inlineRadioOptions5" Text="Other" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>


                                    <%-- <div class="form-group">
                                        <label class="radio-inline">
                                            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
                                            Last Year
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
                                            Last 6 Months
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" />
                                            Last 3 Months
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option4" />
                                            Others
                                        </label>
                                    </div>--%>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group" id="otherContainer" runat="server">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <div class="col-sm-9">
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtMonth" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="FilterValidater" ForeColor="Red" />
                                                    <input type="text" id="txtMonth" runat="server" class="form-control" placeholder="Please enter No. of Months" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div class="col-sm-3">
                                            <button id="btnFilter2" class="tf-save-btn" runat="server" "Filter" onserverclick="btnFilter2_ServerClick" validationgroup="FilterValidater" type="button" data-original-"Filter"><i class="fas fa-filter"></i></button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>


                    </div>





                    <div class="row">

                        <div class="col-md-12">

                            <div class="widget">
                                <header class="widget-header">
                                    <h4 class="widget-title">Accounts</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">

                                    <asp:Literal ID="ltrAccounts" runat="server"></asp:Literal>

                                </div>
                                <!-- .widget-body -->
                            </div>
                        </div>

                        <!-- END column -->
                       
                    </div>
            


                    <div class="clearfix">&nbsp;</div>



                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>
        <style>
            #btnFilter {
                padding: 10px 15px;
                background: #575757;
                color: #fff;
                cursor: pointer;
                display: inline-block
            }

            #btnFilter-info {
                display: none;
                text-align: center;
                padding-bottom: 20px;
            }

            .bg-warning, .warning {
                background-color: #999 !important;
                color: #fff;
            }

            .text-warning {
                color: #999 !important;
            }

            .bg-success, .success {
                background-color: #8d98b3 !important;
                color: #fff;
            }

            .text-success {
                color: #8d98b3 !important;
            }


            .bg-danger, .danger {
                background-color: #b6a2de !important;
                color: #fff;
            }

            .text-danger {
                color: #b6a2de !important;
            }

            .bg-Addition {
                background-color: #2ec7c9 !important;
            }

            .text-Addition {
                color: #2ec7c9 !important;
            }

            .label-text label {
                /*display: inline-block !important;*/
                margin-top: 0px !important;
            }

            #txtMonth {
                margin-top: 5px;
            }

            #btnFilter2 {
                padding: 0px !important;
                height: 36px !important;
                width: 36px !important;
                border-radius: 0px !important;
                margin-top: 6px;
                float: left;
            }
        </style>

        <uc:Scripts ID="script1" runat="server"></uc:Scripts>



        <script>    

            $(document).ready(function () {
                $("#btnFilter").click(function () {
                    $("#btnFilter-info").slideToggle(1000);
                });
            });
        </script>


    </form>
</body>
</html>

