﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Technofinancials.business.classes;

namespace Technofinancials.Finance.view
{
    public partial class FnFMonthWise : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int NewPayrollID
        {
            get
            {
                if (ViewState["NewPayrollID"] != null)
                {
                    return (int)ViewState["NewPayrollID"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["NewPayrollID"] = value;
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false;
                    clearFields();
                    ViewState["dtEmployeeSalaries"] = null;

                    btnApproveByFinance.Visible =
                    lnkDelete.Visible =
                    btnSubForReview.Visible =
                    btnDisapprove.Visible = false;
					if (HttpContext.Current.Items["PayrollID"] != null)
					{
					
						NewPayrollID = Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString());
						getFnFPayrollByID(NewPayrollID);
						txtPayrollDate.Enabled = false;
						


				}


				}
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
		private void getFnFPayrollByID(int NewPayrollID)
		{
			DataTable dt = new DataTable();
			objDB.NewPayrollID = NewPayrollID;
			dt = objDB.GetFnFPayrollByID(ref errorMsg);
			if (dt != null)
			{
				if (dt.Rows.Count > 0)
				{
					txtPayrollDate.Text = DateTime.Parse(dt.Rows[0]["Payrollofmonth"].ToString()).ToString("MMM-yyyy");
					gvSalaries.DataSource = dt;
					gvSalaries.DataBind();
				}
			}

			Common.addlog("View", "Finanace", "FnFPayroll \"" + txtPayrollDate.Text + "\" Viewed", "FnFPayroll", objDB.NewPayrollID);

		}

	  protected void btnView_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            GetEmployeeFnFData();
        }

      


        //private void CheckAccess()
        //{
        //    try
        //    {
        //        btnApproveByFinance.Visible =
        //        lnkDelete.Visible =
        //        btnSubForReview.Visible =
        //        btnDisapprove.Visible = false;

        //        objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
        //        objDB.TableName = "EmployeeSeperationDetails";
        //        objDB.PrimaryColumnnName = "SeperationDetailsID";
        //        objDB.PrimaryColumnValue = NewPayrollID.ToString();
        //        objDB.DocName = "Payroll";

        //        string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
        //        if (chkAccessLevel == "Can Edit")
        //        {
        //            lnkDelete.Visible =
        //            btnSubForReview.Visible = true;
        //        }
        //        if (chkAccessLevel == "Can Edit & Approve")
        //        {
        //            btnApproveByFinance.Visible =
        //            btnDisapprove.Visible = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }
        //}

        private DataTable dtEmployeeSalaries
        {
            get
            {
                if (ViewState["dtEmployeeSalaries"] != null)
                {
                    return (DataTable)ViewState["dtEmployeeSalaries"];
                }
                else
                {
                    return new DataTable();
                }
            }
            set
            {
                ViewState["dtEmployeeSalaries"] = value;
            }
        }

        private void clearFields()
        {
            txtPayrollDate.Text = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = string.Empty;
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                
                if (btn.ID.ToString() == "btnApproveByFinance" || btn.ID.ToString() == "btnRevApproveByFinance")
                {
                    DataTable dtEmployees = (DataTable)gvSalaries.DataSource;
                    List<string> lstEmployees = dtEmployees.AsEnumerable().Select(x => x[0].ToString()).ToList();
                    res = objDB.ApproveEmployeeClearance(lstEmployees);
                    GetEmployeeFnFData();
                }
                else if (btn.ID.ToString() == "btnDisapprove" || btn.ID.ToString() == "btnRejDisApprove")
                {
                    objDB.NewPayrollID = NewPayrollID;
                    //string ss = objDB.ResetAddOnsStatus();
                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Common.addlogNew(res, "Finance", "Payroll of ID\"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/employee-fnf-settlement", "", "Payroll of Date \"" + txtPayrollDate.Text + "\"", "EmployeeSeperationDetails", "FnF Settlement", Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString()));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
            //CheckAccess();
        }
        
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "EmployeeSeperationDetails", "SeperationDetailsID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "NewPayroll of ID \"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" deleted", "EmployeeSeperationDetails", NewPayrollID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {

                int index = e.RowIndex;

                //dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
                dtEmployeeSalaries.Rows[index].SetField("TotalDeduction", (Convert.ToDouble(((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("OtherDeductionsInput")).Text) + Convert.ToDouble(((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("TotalDeduction")).Text)).ToString());
                //dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
                //dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
                dtEmployeeSalaries.AcceptChanges();

                //dtEmployeeSalaries.Rows[index]["NetSalry"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
                //dtEmployeeSalaries.AcceptChanges();

                gvSalaries.EditIndex = -1;


                BindData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void RowUpdating(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = (sender as LinkButton).NamingContainer as GridViewRow;
                int index = row.RowIndex;

                //dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
                dtEmployeeSalaries.Rows[index].SetField("TotalDeduction", (Convert.ToDouble(((TextBox)gvSalaries.Rows[row.RowIndex].FindControl("OtherDeductionsInput")).Text) + Convert.ToDouble(((TextBox)gvSalaries.Rows[row.RowIndex].FindControl("TotalDeduction")).Text)).ToString());
                //dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
                //dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
                dtEmployeeSalaries.AcceptChanges();

                //dtEmployeeSalaries.Rows[index]["NetSalry"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
                //dtEmployeeSalaries.AcceptChanges();

                gvSalaries.EditIndex = -1;


                BindData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        //protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    try
        //    {
        //        int index = e.RowIndex;

        //        dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("AdditionalTax", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditTax")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
        //        dtEmployeeSalaries.AcceptChanges();

        //        dtEmployeeSalaries.Rows[index]["NetSalary"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
        //        dtEmployeeSalaries.AcceptChanges();

        //        gvSalaries.EditIndex = -1;
        //        BindData();
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }

        //}
        protected void gvSalaries_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSalaries.EditIndex = -1;
            BindData();
        }
        protected void gvSalaries_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSalaries.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void GetEmployeeFnFData()
        {
            objDB.PayrollDate = txtPayrollDate.Text;
            objDB.PayrollDate = "01-" + txtPayrollDate.Text;
            dtEmployeeSalaries = objDB.GetKTPayrollDetailsForLeftEmployeesByNewPayrollID(ref errorMsg);
            BindData();
        }
        protected void BindData()
        {
            gvSalaries.DataSource = dtEmployeeSalaries;
            gvSalaries.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    gvSalaries.UseAccessibleHeader = true;
                    gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            gvNormalSal.DataSource = dtEmployeeSalaries;
            gvNormalSal.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    gvNormalSal.UseAccessibleHeader = true;
                    gvNormalSal.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

	


		protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
		{
            int a = 0;

		}
        protected void lnkUpdate_Command(object sender, CommandEventArgs e)
        {

            ////dt.Columns.Add("SrNo");
            ////dt.Columns.Add("ExpType");
            ////dt.Columns.Add("Description");
            ////dt.Columns.Add("Amount");
            ////dt.Columns.Add("FilePath");
            //LinkButton lnk = (LinkButton)sender as LinkButton;
            //string delSr = lnk.CommandArgument.ToString();
            //for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            //{
            //    if (dtExpDetails.Rows[i][0].ToString() == delSr)
            //    {
            //        ddlExpType.SelectedValue = dtExpDetails.Rows[i]["ExpType"].ToString();
            //        txtDescriptionDetail.Value = dtExpDetails.Rows[i]["Description"].ToString();
            //        txtAmountDetail.Value = dtExpDetails.Rows[i]["Amount"].ToString();
            //        imgLogo.Src = dtExpDetails.Rows[i]["FilePath"].ToString();
            //        hdnExpenseSrNO.Value = delSr;
            //        btnUpdateDiv.Visible = true;
            //        btnAddDiv.Visible = false;
            //    }
            //}
            //for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            //{
            //    dtExpDetails.Rows[i].SetField(0, i + 1);
            //    dtExpDetails.AcceptChanges();
            //}

            //ExpDetailsSrNo = dtExpDetails.Rows.Count + 1;
            //BindExpDetailsTable();
        }
        private string printPaySlip()
        {

            string content = "<div> <style> .B_class {padding: 20px; border: 1px solid #000;background: #fff; } .B_class h1, .B_class h2, .B_class h3, .B_class h4, .B_class p, .B_class td {margin: 0;font-family: lato;color: #000; } .B_class table {border: 1px solid;margin-top: -25px; } .B_class table td, .B_class table th {padding: 3.4px; } .B_class .slip-table thead tr th {background-color: #ffffff;border-bottom: 1px solid #000000 !important;border-top: 1px solid #000000 !important;color: #000;padding: 0 10px; } .slip-table-bordered thead tr th, .slip-table-bordered thead tr td, .slip-table-bordered tbody tr th, .slip-table-bordered tbody tr td {border-top: 1px solid transparent !important;border: 0;vertical-align: middle;padding: .1rem;font-size: 14px; } .slip-table thead tr th, .slip-table thead tr td, .slip-table tbody tr th, .slip-table tbody tr td, .slip-table tfoot tr th, .slip-table tfoot tr td {height: 32px;line-height: 1.428571429;vertical-align: middle;border-top: 1px solid #ddd;padding: 0 10px; } .slip-table {width: 100%;max-width: 100%;background-color: transparent;border-collapse: collapse;border-spacing: 0; } .slip-table-bordered tfoot tr th, .slip-table-bordered tfoot tr td {border-top: 1px solid #000 !important;border: 0;vertical-align: middle;padding: .1rem;font-size: 14px;padding: 0 10px; } .B_class .slip-table thead tr th {text-align: right; } .B_class .slip-table thead tr th:nth-child(1) {text-align: left; } .B_class .slip-table tbody tr td {text-align: right;height: 28px; } .B_class .slip-table tbody tr td:nth-child(1) {text-align: left; } .B_class .slip-table tfoot tr td {text-align: right;height: 34px; } .B_class .slip-table tfoot tr td:nth-child(1) {text-align: left;width: 200px !important; } .B_class hr {margin-top: 20px;margin-bottom: 10px;border: 0;border-top: 1px solid #000000; } .tableP {height: 34px;padding: 6.4px;border: 1px solid;margin-top: -6px !important;width: 99.9%; } .tableP span {margin-left: 40px;text-transform: capitalize; } .AddressSide {height: 90px;color: #000;text-align: center; } .AddressSide h1 {font-size: 18px;text-transform: capitalize;font-weight: 700;margin: 5px 0;color: #000; } .AddressSide h2 {font-size: 18px;text-align: right;font-weight: 400;color: #000;margin: 5px 0; } .AddressSide h3 {font-size: 18px;text-transform: capitalize;font-weight: 700;margin: 5px 0;color: #000; } .AddressSide h4 {font-size: 18px;text-transform: capitalize;font-weight: 700;margin-bottom: 15px;color: #000;margin: 5px 0; } .AddressSide p {font-size: 16px;font-weight: 500;max-width: 350px;text-transform: capitalize;color: #000; } .bigBox {width: 100%;border: 1px solid #000;height: 400px;margin: 50px 0 20px; } .Box01 {width: 45.5%;display: inline-block;padding: 10px;color: #000; } .Box02 {width: 46%;display: inline-block;padding: 10px 20px; margin-left: -5px; color: #000; } .Box03 {width: 50%;display: inline-block; } .Box04 {width: 50%;margin-left: -5px;display: inline-block; } .Box05 {width: 100%;display: block; } .OneLine {width: 100%;height: 28px; } .OneLine h3 {display: inline-block;font-size: 16px;font-weight: 700;width: 35%;margin: 3px 0;color: #000; } .OneLine p {display: inline-block;font-size: 16px;width: 60%;margin: 3px 0;color: #000; } .totalSalaries {font-weight: bold !important;color: #188ae2 !important;font-size: 16px !important; } .line-four {width: 99.9%;display: block; } #tblPDF td { border: 1px solid; border-right:none; border-top:none; } </style> <div class='B_class' id='ReportPayroll'><img src='##COMPANY_LOGO##' alt='Company Logo' style='width: 6%; position: absolute;' /> <div class='AddressSide'> <h1>##COMPANY_NAME##</h1><h3>Payslip</h3><h4>For the Month Of ##PAYROLL_MONTH##</h4></div> <div style='width: 100%; display: block; border: 1px solid #000;'><div class='Box01'> <div class='OneLine'> <h3>Employee Code :</h3> <p class=''>##EMPLOYEE_CODE##</p> </div> <div class='OneLine'> <h3>Location :</h3> <p class=''>##LOCATION##</p> </div> <div class='OneLine'> <h3>Designation :</h3> <p class=''>##DESIGNATION## </p> </div> <div class='OneLine'> <h3>Date of Joining :</h3> <p class=''>##DATE_OF_JOINING## </p> </div> <div class='OneLine'> <h3>Monthly Salary :</h3> <p class=''>##BASIC_SALARY##</p> </div> <div class='OneLine'> <h3>Account :</h3> <p class=''>##ACCOUNT_NO##</p> </div></div> <div class='Box02'> <div class='OneLine'> <h3>Name :</h3> <p class=''>EMPLOYEE_NAME</p> </div> <div class='OneLine'> <h3>Department :</h3> <p class=''>##DEPARTMENT##</p> </div> <div class='OneLine'> <h3>Employee Type :</h3> <p class=''>##EMPLOYEE_TYPE##</p> </div> <div class='OneLine'> <h3>Date of Birth :</h3> <p class=''>##DATE_OF_BIRTH##</p> </div> <div class='OneLine'> <h3>CNIC :</h3> <p class=''>##CNIC##</p> </div> <div class='OneLine'> <h3>Bank :</h3> <p class=''>##BANK_NAME##</p> </div></div> </div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div style='width: 100%;'> <div class='Box03' style='width: 50%; float: left;'> ##ALLOWANCE_TABLE## </div> <div class='Box04' style='width: 50%; float: right;'> ##DEDUCTION_TABLE## </div> </div> <div class='clearfix'>&nbsp;</div> <div class='Box03' style='width: 50%; float: left;'> ##ALLOWANCE_TOTAL## </div> <div class='Box04' style='width: 50%; float: right;'> ##DEDUCTION_TOTAL## </div> <div class='Box05'><table class='slip-table slip-table-bordered ' style='width: 100%; margin-top: 0px;'> <tbody> <tr> <td style='width: 50%'><b>Net Amount </b></td> <td style='width: 50%'><b><span> ##NET_AMOUNT##</span></b></td> </tr> </tbody></table> </div> <div class='Box05'><table class='slip-table slip-table-bordered ' style='width: 100%; margin-top: 0px;'> <tbody> <tr> <td><b>Amount in Words</b></td> <td style='width: 50%'><span>Pak Rupees ##TOTAL_AMOUNT_IN_WORDS##</span></td> </tr> </tbody></table> </div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div class='Box03' style='width: 100%; display: block;'><div class=''> <table style='background-color: #c0c0c0; width: 100%; '> <tbody> <tr> <td style='font-weight: bold;'>Income Tax Details</td> </tr> </tbody> </table> ##TAX_TABLE## </div> </div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div class='Box04' style='width: 100%; display: block; margin-left: 0px;'> <table style='background-color: #c0c0c0; width: 100%; '> <tbody> <tr> <td style='font-weight: bold;'>Provident Fund Details</td> </tr> </tbody> </table> ##PF_TABLE## </div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div> <p style='text-align: center;'>Techno Financials - This is auto generated payslip and needs no signature.</p> </div> </div> </div>";

            try
            {
				string footer = "";
				string header = "";
				decimal totalYTDminus = 0;
				decimal sumTaxableAmount2 = 0;
				decimal AnnualTaxableIncome2 = 0;
				decimal minusValue = 0;
				decimal CalSum = 0;
				decimal sumYTDTAX = 0;
				decimal sumYTDAddTax = 0;
				double TITL = 0;
				DataTable dt2 = new DataTable();
				dt2 = objDB.GetFnFSlipEmployeeByID(ref errorMsg);
				var serializeobject = Newtonsoft.Json.JsonConvert.SerializeObject(dt2);
				var myobjList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FnFSlipModel>>(serializeobject);
				var myObj = myobjList[0];

				


				content = content.Replace("##COMPANY_NAME##", "ABS - LABS (PVT.) LIMITED");
				content = content.Replace("##COMPANY_LOGO##", Session["CompanyLogo"].ToString());
				content = content.Replace("##PAYROLL_MONTH##", "");
				content = content.Replace("##LOCATION##", myObj.location);
				content = content.Replace("##EMPLOYEE_CODE##", myObj.wdid.ToString());
				content = content.Replace("##DESIGNATION##", myObj.designation.ToString());
				content = content.Replace("##DATE_OF_JOINING##", myObj.DateOfJoining.ToString());
				content = content.Replace("##DATE_OF_BIRTH##", myObj.dob.ToString());
				content = content.Replace("##ACCOUNT_NO##", myObj.Accountno.ToString());
				content = content.Replace("EMPLOYEE_NAME", myObj.Employeename.ToString());
				content = content.Replace("##DEPARTMENT##", myObj.department.ToString());
				content = content.Replace("##EMPLOYEE_TYPE##", myObj.EmploymentType.ToString());
				content = content.Replace("##CNIC##", myObj.cnic.ToString());
				content = content.Replace("##BANK_NAME##", myObj.Bank.ToString());




				              double total, total2, NetAmount;
				               content = content.Replace("##BASIC_SALARY##", myObj.BasicSalary.ToString("N2"));
				                string basicSalary = myObj.BasicSalary.ToString();
				             string SalesComissions = myObj.commission.ToString();
				                string houseRents = myObj.HouseRent.ToString();
				               string UtilityAllows = myObj.UtilityAllowance.ToString();
			                 string mdeicalAllowance = myObj.MedicalAllowance.ToString();
				                 string ExpenseReims = myObj.otherexpense.ToString();
				                 string EidBonuss = myObj.EidBonus.ToString();
				                 string Spiffss = myObj.spiffs.ToString();
				                string Overtimes = myObj.Overtime.ToString(); // need to recheck
				                string OvertimeHolidayss = 0.ToString();// need to rechec
				                string IncomeTaxs = myObj.IncomeTax.ToString();
				                string AddIncomeTaxs = myObj.AdditionalTaxpermonth.ToString();
				                string EOBIs = myObj.EOBI.ToString();
				                 string OtherAllowances =0.ToString();// need to recheck
				               string PFNew = myObj.PFDeduction.ToString(); // recheck spelling
				                string OtherDeduction = 0.ToString(); // need to recheck
				               string TotSal2 = myObj.StandardGross.ToString();
				                 double totalsumm = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss) + double.Parse(EidBonuss);
				                TotSal2 = totalsumm.ToString();
				                double totalgs = double.Parse(basicSalary) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows);
				                decimal tts = decimal.Parse(totalgs.ToString());
				                 tts = decimal.Round(tts, 2, MidpointRounding.AwayFromZero);
				                 sumTaxableAmount2 = decimal.Parse(basicSalary) + decimal.Parse(houseRents) + decimal.Parse(UtilityAllows);
				                 total = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss);
				                total2 = double.Parse(IncomeTaxs) + double.Parse(EOBIs) + double.Parse(PFNew) + double.Parse(OtherDeduction) + double.Parse(AddIncomeTaxs);
				                NetAmount = total - total2;

				                 decimal bs = decimal.Parse(basicSalary.ToString()).RoundDecimalTwoDigit();
				                 decimal ttt = decimal.Parse(TotSal2.ToString()).RoundDecimalTwoDigit();
				                 decimal sc = decimal.Parse(SalesComissions.ToString()).RoundDecimalTwoDigit();
				                 decimal hr = decimal.Parse(houseRents.ToString()).RoundDecimalTwoDigit();
				                 decimal ua = decimal.Parse(UtilityAllows.ToString()).RoundDecimalTwoDigit();
				                 decimal er = decimal.Parse(ExpenseReims.ToString()).RoundDecimalTwoDigit();
				                 decimal eb = decimal.Parse(EidBonuss.ToString()).RoundDecimalTwoDigit();
				                 decimal sp = decimal.Parse(Spiffss.ToString()).RoundDecimalTwoDigit();
				                 decimal ot = decimal.Parse(Overtimes.ToString()).RoundDecimalTwoDigit();
				                 decimal oth = decimal.Parse(OvertimeHolidayss.ToString()).RoundDecimalTwoDigit();
				                 decimal it = decimal.Parse(IncomeTaxs.ToString()).RoundDecimalTwoDigit();
				                 decimal ait = decimal.Parse(AddIncomeTaxs.ToString()).RoundDecimalTwoDigit();
				                 decimal eobi = decimal.Parse(EOBIs.ToString()).RoundDecimalTwoDigit();
				                 decimal ma = decimal.Parse(mdeicalAllowance.ToString()).RoundDecimalTwoDigit();
				                 decimal ollow = decimal.Parse(OtherAllowances.ToString()).RoundDecimalTwoDigit();
				                 decimal pf = decimal.Parse(PFNew.ToString()).RoundDecimalTwoDigit();
				                 decimal odnew = decimal.Parse(OtherDeduction.ToString()).RoundDecimalTwoDigit();
				string date = "01-Jul-2021";
				objDB.PayrollDate = date;
				DateTime dateTime12 = Convert.ToDateTime(date);
				int months = int.Parse(dateTime12.ToString("MM"));
				int RemainingMonths = 0;
				double TotalSalary2 = 0;

				RemainingMonths = months > 6 ? (18 - months) : 6 - months;

				if (RemainingMonths != 0)
				{
					double subtract = double.Parse(myObj.StandardGross.ToString()) - double.Parse(myObj.MedicalAllowance.ToString());
					TotalSalary2 = RemainingMonths * subtract;
				}

				DataTable dt3 = objDB.GetPaySlipByEmployeeID_YTD(ref errorMsg);
				double sumYTD = 0;
				double SumI = 0;
				double AnnualTaxableIncome = 0;

				decimal bsytd = decimal.Parse(dt3.Rows[0]["BasicSalaryYTD"].ToString()).RoundDecimalTwoDigit();
				decimal scytd = decimal.Parse(dt3.Rows[0]["CommissionYTD"].ToString()).RoundDecimalTwoDigit();
				decimal HRYTD = decimal.Parse(dt3.Rows[0]["HouseRentYTD"].ToString()).RoundDecimalTwoDigit();
				decimal MAYTD = decimal.Parse(dt3.Rows[0]["MedicalAllowenceYTD"].ToString()).RoundDecimalTwoDigit();
				decimal UAYTD = decimal.Parse(dt3.Rows[0]["UtilityAllowenceYTD"].ToString()).RoundDecimalTwoDigit();
				decimal OEYTD = decimal.Parse(dt3.Rows[0]["OtherExpensesYTD"].ToString()).RoundDecimalTwoDigit();
				decimal ebytd = decimal.Parse(dt3.Rows[0]["EidBonusYTD"].ToString()).RoundDecimalTwoDigit();
				decimal sytd = decimal.Parse(dt3.Rows[0]["SpiffsYTD"].ToString()).RoundDecimalTwoDigit();
				decimal otytd = decimal.Parse(dt3.Rows[0]["OverTimeYTD"].ToString()).RoundDecimalTwoDigit();
				decimal ebhytd = decimal.Parse(dt3.Rows[0]["OvertimeHolidayYTD"].ToString()).RoundDecimalTwoDigit();
				decimal itytd = decimal.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString()).RoundDecimalTwoDigit();
				decimal eobiytd = decimal.Parse(dt3.Rows[0]["EobiYTD"].ToString()).RoundDecimalTwoDigit();
				decimal overtimetoalytd = decimal.Parse(dt3.Rows[0]["OverTimeTotalAmountsYTD"].ToString());
				decimal otytds = decimal.Parse(dt3.Rows[0]["Other_AllowancesYTD"].ToString()).RoundDecimalTwoDigit();
				decimal pfytd = decimal.Parse(dt3.Rows[0]["PFYTD"].ToString()).RoundDecimalTwoDigit();
				decimal odytd = decimal.Parse(dt3.Rows[0]["OtherDeductionsYTD"].ToString()).RoundDecimalTwoDigit();
				decimal totalgsYTD = bsytd + HRYTD + MAYTD + UAYTD;
				decimal TOTYTD = decimal.Parse(dt3.Rows[0]["TotalSalry2"].ToString()).RoundDecimalTwoDigit();

				double totalsummytd = double.Parse(bsytd.ToString())
					+ double.Parse(otytds.ToString())
					+ double.Parse(scytd.ToString())
					+ double.Parse(HRYTD.ToString())
					+ double.Parse(MAYTD.ToString())
					+ double.Parse(UAYTD.ToString())
					+ double.Parse(OEYTD.ToString())
					+ double.Parse(sytd.ToString())
					+ double.Parse(ebytd.ToString())
					+ double.Parse(overtimetoalytd.ToString());

				totalYTDminus = decimal.Parse(totalsummytd.ToString());

				string TotSal2ytds = totalsummytd.ToString();
				double adtytdd = Convert.ToDouble(dt3.Rows[0]["AddIncomeTaxYTD"].ToString());

				SumI = double.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString()) + double.Parse(dt3.Rows[0]["EobiYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherDeductionsYTD"].ToString()) + double.Parse(dt3.Rows[0]["PFYTD"].ToString()) + double.Parse(dt3.Rows[0]["AddIncomeTaxYTD"].ToString());
				sumYTD = double.Parse(dt3.Rows[0]["BasicSalaryYTD"].ToString()) + double.Parse(dt3.Rows[0]["Other_AllowancesYTD"].ToString()) + double.Parse(dt3.Rows[0]["CommissionYTD"].ToString()) + double.Parse(dt3.Rows[0]["HouseRentYTD"].ToString()) + double.Parse(dt3.Rows[0]["MedicalAllowenceYTD"].ToString()) + double.Parse(dt3.Rows[0]["UtilityAllowenceYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherExpensesYTD"].ToString()) + double.Parse(dt3.Rows[0]["EidBonusYTD"].ToString()) + double.Parse(dt3.Rows[0]["SpiffsYTD"].ToString()) + double.Parse(dt3.Rows[0]["OverTimeYTD"].ToString()) + double.Parse(dt3.Rows[0]["OvertimeHolidayYTD"].ToString());
				sumYTDTAX = itytd + decimal.Parse(adtytdd.ToString());
				minusValue = totalYTDminus - MAYTD;
				CalSum = RemainingMonths == 0 ? sumTaxableAmount2 : sumTaxableAmount2 * RemainingMonths;
				AnnualTaxableIncome2 = minusValue + CalSum;
				AnnualTaxableIncome2 = Math.Round(AnnualTaxableIncome2, 2);

				AnnualTaxableIncome = double.Parse(dt3.Rows[0]["TotalSalry2"].ToString()) + TotalSalary2;
				AnnualTaxableIncome = Math.Round(AnnualTaxableIncome, 2);
				double itp = Convert.ToDouble(dt3.Rows[0]["IncomeTaxYTD"].ToString());

				objDB.PayAmount = float.Parse(AnnualTaxableIncome.ToString());
				objDB.PayAmount = float.Parse(AnnualTaxableIncome2.ToString());
				objDB.PayrollDate = date;

				DataTable dt4 = objDB.GetAnnualTaxableIncomePayslips(ref errorMsg);

				if (dt4 != null && dt4.Rows.Count > 0)
				{
					TITL = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
					//TotalIncomeTaxLia.Text = TITL.ToString("N2");
					sumYTDAddTax = decimal.Parse(TITL.ToString()) - sumYTDTAX;
				}

				double incometaxLia = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
				double reamainingIncomeTax = incometaxLia - double.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString());
				DataTable dt5 = objDB.GetPaySlipPFKTByEmployeeID(ref errorMsg);

				//RemIncomeTaxPayable.Text = sumYTDAddTax.ToString("N2");
				//EmployeePF.Text = dt5.Rows[0]["OpeningPF_Employee"].ToString();
				//EmployeerPF.Text = dt5.Rows[0]["OpeningPF_Company"].ToString();
				//Label47.Text = dt5.Rows[0]["CurrentPF_Employee"].ToString();
				//Label48.Text = dt5.Rows[0]["closingEmployee"].ToString();
				//Label49.Text = dt5.Rows[0]["CurrentPF_Company"].ToString();
				//Label50.Text = dt5.Rows[0]["ClosingEmployeer"].ToString();
				//Label51.Text = dt5.Rows[0]["sumcurrent"].ToString();
				//Label52.Text = dt5.Rows[0]["totalsum2"].ToString();
				//TotalPF.Text = dt5.Rows[0]["SumOpening"].ToString();













				//FnFSlipModel fnfObj = Newtonsoft.Json.JsonConvert.DeserializeObject<FnFSlipModel>(serializeobject);

				//string footer = "";
				//            string header = "";
				//            decimal totalYTDminus = 0;
				//            decimal sumTaxableAmount2 = 0;
				//            decimal AnnualTaxableIncome2 = 0;
				//            decimal minusValue = 0;
				//            decimal CalSum = 0;
				//            decimal sumYTDTAX = 0;
				//            decimal sumYTDAddTax = 0;
				//            double TITL = 0;

				string st = "";
				DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(3206);//Convert.ToInt32(Session["EmployeeID"].ToString());
                //objDB.PayrollDate = txtPayrollDate.Text;
                objDB.DocType = "Pay Slip";

                dt = objDB.GetDocumentDesign(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = "<div> <style> .B_class {padding: 20px; border: 1px solid #000;background: #fff; } .B_class h1, .B_class h2, .B_class h3, .B_class h4, .B_class p, .B_class td {margin: 0;font-family: lato;color: #000; } .B_class table {border: 1px solid;margin-top: -25px; } .B_class table td, .B_class table th {padding: 3.4px; } .B_class .slip-table thead tr th {background-color: #ffffff;border-bottom: 1px solid #000000 !important;border-top: 1px solid #000000 !important;color: #000;padding: 0 10px; } .slip-table-bordered thead tr th, .slip-table-bordered thead tr td, .slip-table-bordered tbody tr th, .slip-table-bordered tbody tr td {border-top: 1px solid transparent !important;border: 0;vertical-align: middle;padding: .1rem;font-size: 14px; } .slip-table thead tr th, .slip-table thead tr td, .slip-table tbody tr th, .slip-table tbody tr td, .slip-table tfoot tr th, .slip-table tfoot tr td {height: 32px;line-height: 1.428571429;vertical-align: middle;border-top: 1px solid #ddd;padding: 0 10px; } .slip-table {width: 100%;max-width: 100%;background-color: transparent;border-collapse: collapse;border-spacing: 0; } .slip-table-bordered tfoot tr th, .slip-table-bordered tfoot tr td {border-top: 1px solid #000 !important;border: 0;vertical-align: middle;padding: .1rem;font-size: 14px;padding: 0 10px; } .B_class .slip-table thead tr th {text-align: right; } .B_class .slip-table thead tr th:nth-child(1) {text-align: left; } .B_class .slip-table tbody tr td {text-align: right;height: 28px; } .B_class .slip-table tbody tr td:nth-child(1) {text-align: left; } .B_class .slip-table tfoot tr td {text-align: right;height: 34px; } .B_class .slip-table tfoot tr td:nth-child(1) {text-align: left;width: 200px !important; } .B_class hr {margin-top: 20px;margin-bottom: 10px;border: 0;border-top: 1px solid #000000; } .tableP {height: 34px;padding: 6.4px;border: 1px solid;margin-top: -6px !important;width: 99.9%; } .tableP span {margin-left: 40px;text-transform: capitalize; } .AddressSide {height: 90px;color: #000;text-align: center; } .AddressSide h1 {font-size: 18px;text-transform: capitalize;font-weight: 700;margin: 5px 0;color: #000; } .AddressSide h2 {font-size: 18px;text-align: right;font-weight: 400;color: #000;margin: 5px 0; } .AddressSide h3 {font-size: 18px;text-transform: capitalize;font-weight: 700;margin: 5px 0;color: #000; } .AddressSide h4 {font-size: 18px;text-transform: capitalize;font-weight: 700;margin-bottom: 15px;color: #000;margin: 5px 0; } .AddressSide p {font-size: 16px;font-weight: 500;max-width: 350px;text-transform: capitalize;color: #000; } .bigBox {width: 100%;border: 1px solid #000;height: 400px;margin: 50px 0 20px; } .Box01 {width: 45.5%;display: inline-block;padding: 10px;color: #000; } .Box02 {width: 46%;display: inline-block;padding: 10px 20px; margin-left: -5px; color: #000; } .Box03 {width: 50%;display: inline-block; } .Box04 {width: 50%;margin-left: -5px;display: inline-block; } .Box05 {width: 100%;display: block; } .OneLine {width: 100%;height: 28px; } .OneLine h3 {display: inline-block;font-size: 16px;font-weight: 700;width: 35%;margin: 3px 0;color: #000; } .OneLine p {display: inline-block;font-size: 16px;width: 60%;margin: 3px 0;color: #000; } .totalSalaries {font-weight: bold !important;color: #188ae2 !important;font-size: 16px !important; } .line-four {width: 99.9%;display: block; } #tblPDF td { border: 1px solid; border-right:none; border-top:none; } </style> <div class='B_class' id='ReportPayroll'><img src='##COMPANY_LOGO##' alt='Company Logo' style='width: 6%; position: absolute;' /> <div class='AddressSide'> <h1>##COMPANY_NAME##</h1><h3>Payslip</h3><h4>For the Month Of ##PAYROLL_MONTH##</h4></div> <div style='width: 100%; display: block; border: 1px solid #000;'><div class='Box01'> <div class='OneLine'> <h3>Employee Code :</h3> <p class=''>##EMPLOYEE_CODE##</p> </div> <div class='OneLine'> <h3>Location :</h3> <p class=''>##LOCATION##</p> </div> <div class='OneLine'> <h3>Designation :</h3> <p class=''>##DESIGNATION## </p> </div> <div class='OneLine'> <h3>Date of Joining :</h3> <p class=''>##DATE_OF_JOINING## </p> </div> <div class='OneLine'> <h3>Monthly Salary :</h3> <p class=''>##BASIC_SALARY##</p> </div> <div class='OneLine'> <h3>Account :</h3> <p class=''>##ACCOUNT_NO##</p> </div></div> <div class='Box02'> <div class='OneLine'> <h3>Name :</h3> <p class=''>EMPLOYEE_NAME</p> </div> <div class='OneLine'> <h3>Department :</h3> <p class=''>##DEPARTMENT##</p> </div> <div class='OneLine'> <h3>Employee Type :</h3> <p class=''>##EMPLOYEE_TYPE##</p> </div> <div class='OneLine'> <h3>Date of Birth :</h3> <p class=''>##DATE_OF_BIRTH##</p> </div> <div class='OneLine'> <h3>CNIC :</h3> <p class=''>##CNIC##</p> </div> <div class='OneLine'> <h3>Bank :</h3> <p class=''>##BANK_NAME##</p> </div></div> </div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div style='width: 100%;'> <div class='Box03' style='width: 50%; float: left;'> ##ALLOWANCE_TABLE## </div> <div class='Box04' style='width: 50%; float: right;'> ##DEDUCTION_TABLE## </div> </div> <div class='clearfix'>&nbsp;</div> <div class='Box03' style='width: 50%; float: left;'> ##ALLOWANCE_TOTAL## </div> <div class='Box04' style='width: 50%; float: right;'> ##DEDUCTION_TOTAL## </div> <div class='Box05'><table class='slip-table slip-table-bordered ' style='width: 100%; margin-top: 0px;'> <tbody> <tr> <td style='width: 50%'><b>Net Amount </b></td> <td style='width: 50%'><b><span> ##NET_AMOUNT##</span></b></td> </tr> </tbody></table> </div> <div class='Box05'><table class='slip-table slip-table-bordered ' style='width: 100%; margin-top: 0px;'> <tbody> <tr> <td><b>Amount in Words</b></td> <td style='width: 50%'><span>Pak Rupees ##TOTAL_AMOUNT_IN_WORDS##</span></td> </tr> </tbody></table> </div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div class='Box03' style='width: 100%; display: block;'><div class=''> <table style='background-color: #c0c0c0; width: 100%; '> <tbody> <tr> <td style='font-weight: bold;'>Income Tax Details</td> </tr> </tbody> </table> ##TAX_TABLE## </div> </div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div class='Box04' style='width: 100%; display: block; margin-left: 0px;'> <table style='background-color: #c0c0c0; width: 100%; '> <tbody> <tr> <td style='font-weight: bold;'>Provident Fund Details</td> </tr> </tbody> </table> ##PF_TABLE## </div> <div class='clearfix'>&nbsp;</div> <div class='clearfix'>&nbsp;</div> <div> <p style='text-align: center;'>Techno Financials - This is auto generated payslip and needs no signature.</p> </div> </div> </div>";
						footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

				#region MyRegion
				#region Allowances Section

				st += @"  <table style='width: 100%;'>
				       <tbody>
				         <tr style='border - bottom: 2px solid #000;'>
				           <td>Allowance</td>
				           <td style='text-align:center;'>Amount</td>
				           <td style='text-align:center;'>YTD</td>
				         </tr>
				         <tr>
				           <td><span id=''>BasicSalary</span></td>
				           <td style = 'text-align: right;'><span id=''>" + bs.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + bsytd.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td><span id=''>House Rent Allowance</span></td>
				           <td style = 'text-align: right;'><span id=''>" + hr.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + HRYTD.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td><span id=''>Medical Allowance</span></td>
				           <td style = 'text-align: right;'><span id=''>" + ma.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + MAYTD.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td><span id=''>Utility Allowance</span></td>
				           <td style = 'text-align: right;'><span id=''>" + ua.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + UAYTD.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td ><b><span id=''>Total </span></b></td>
				           <td style='border-top: 1px solid; border-bottom: 1px solid; text-align: right;'><b><span id=''>" + tts.ToString("N2") + @"</span></b></td>
				           <td style='border-top: 1px solid; border-bottom: 1px solid; text-align: right;'><b><span id=''>" + totalgsYTD.ToString("N2") + @"</span></b></td>
				         </tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				         <tr>
				           <td><span id=''>Sales Commission</span></td>
				           <td style = 'text-align: right;'><span id=''>" + sc.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + scytd.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td><span id=''>Other Allowance</span></td>
				           <td style = 'text-align: right;'><span id=''>" + ollow.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + otytds.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td><span id=''>Expense Reimbursement</span></td>
				           <td style = 'text-align: right;'><span id=''>" + er.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + OEYTD.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td><span id=''>Eid Bouns</span></td>
				           <td style = 'text-align: right;'><span id=''>" + eb.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + ebytd.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td><span id=''>Spiffs</span></td>
				           <td style = 'text-align: right;'><span id=''>" + sp.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + sytd.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td><span id=''>Overtime Hours - General</span></td>
				           <td style = 'text-align: right;'><span id=''>" + ot.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + otytd.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td><span id=''>Overtime Holidays</span></td>
				           <td style = 'text-align: right;'><span id=''>" + oth.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + ebhytd.ToString("N2") + @"</span></td>
				         </tr>
				     </tbody>
				     </table>";

				content = content.Replace("##ALLOWANCE_TABLE##", st);
				#endregion

				#region Allowances' Total Section
				st = "";
				st += @"
				                   <table class='slip-table slip-table-bordered' style='width: 100%; margin-top: 0px;'>
				                     <tbody>
				                     <tr>
				                       <td style='width: 58%;'><b><span id=''>Total</span></b></td>
				                         <td  style='text-align: left;'><b><span id=''>" + ttt.ToString("N2") + @"</span></b></td>
				                       <td><b><span id=''>" + totalsummytd.ToString("N2") + @"</span></b></td>
				                     </tr>
				                 </tbody>
				                 </table>";

				content = content.Replace("##ALLOWANCE_TOTAL##", st);
				#endregion

				#region Deduction Section
				st = "";
				st += @"
				 <table style='width: 100%; padding-bottom: 6.2px;'>
				       <tbody>
				         <tr style='border-bottom: 2px solid #000;'>
				           <td scope='col'>Deductions</td>
				           <td scope='col' style='text-align:center;'>Amount</td>
				           <td scope='col' style='text-align:center;'>YTD</td>
				         </tr>
				             <tr>
				           <td><span id=''>Income Tax</span></td>
				           <td style = 'text-align: right;'><span id=''>" + it.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + itytd.ToString("N2") + @"</span></td>
				         </tr>
				          <tr>
				               <td><span id=''>Additional Income Tax</span></td>
				               <td style = 'text-align: right;'><span id=''>" + ait.ToString("N2") + @"</span></td>
				               <td style = 'text-align: right;'><span id=''>" + adtytdd.ToString("N2") + @"</span></td>
				             </tr>
				              <tr>
				           <td><span id=''>EOBI Deduction</span></td>
				           <td style = 'text-align: right;'><span id=''>" + eobi.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + eobiytd.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td><span id=''>Employee PF</span></td>
				           <td style = 'text-align: right;'><span id=''>" + pf.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + pfytd.ToString("N2") + @"</span></td>
				         </tr>
				          <tr>
				           <td><span id=''>Others</span></td>
				           <td style = 'text-align: right;'><span id=''>" + odytd.ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + odytd.ToString("N2") + @"</span></td>
				         </tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				<tr>
					<td></td><td></td><td></td>
				</tr>
				     </tbody>          
				     </table>";

				content = content.Replace("##DEDUCTION_TABLE##", st);
				#endregion

				#region Deductions and Net Total Section
				st = "";
				st += @"
				                   <table class='slip-table slip-table-bordered' style='width: 100%; margin-top: 0px;'>
				                     <tbody>
				                     <tr>
				                       <td style='width: 58%;'><b><span id=''>Total</span></b></td>
				                       <td><b><span id=''>" + total2.ToString("N2") + @"</span></b></td>
				                       <td><b><span id=''>" + SumI.ToString("N2") + @"</span></b></td>
				                     </tr>
				                 </tbody>
				                 </table>";

				content = content.Replace("##DEDUCTION_TOTAL##", st);
				content = content.Replace("##NET_AMOUNT##", (total - total2).ToString("N2"));
				content = content.Replace("##TOTAL_AMOUNT_IN_WORDS##", Common.NumberToWords((total - total2)));
				#endregion

				#region Tax Details Section
				st = "";
				st += @"
				     <table style='margin-top: -1px; width: 100%;'>
				       <tbody>
				         <tr>
				           <td style='font-weight: bold;'><span id=''>Annual Taxable Income</span></td>
				           <td style='text-align: right;'><span id=''>" + AnnualTaxableIncome2.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td style='font-weight: bold;'><span id=''>Total Income Tax Liability</span></td>
				           <td style='text-align: right;'><span id=''>" + TITL.ToString("N2") + @"</span></td>

				         </tr>
				         <tr>
				           <td style='font-weight: bold;'><span id=''>Income Tax Paid</span></td>
				           <td style='text-align: right;'><span id=''>" + sumYTDTAX.ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td style='font-weight: bold;'><span id=''>Advance Tax Adjustment</span></td>
				           <td style='text-align: right;'><span id=''>-</span></td>
				         </tr>
				         <tr>
				           <td style='font-weight: bold;'><span id=''>Tax Credit</span></td>
				           <td style='text-align: right;'><span id=''>-</span></td>
				         </tr>
				         <tr>
				           <td style='font-weight: bold;'><span id=''>Remaining Income Tax Payable</span></td>
				           <td style='text-align: right;'><span id=''>" + sumYTDAddTax.ToString("N2") + @"</span></td>
				         </tr> 
				     </tbody>
				     </table>";

				content = content.Replace("##TAX_TABLE##", st);
				#endregion

				#region PF Section
				st = "";
				total = 0;
				st += @"
				      <table style='width: 100%; border-right: 1px solid; border-top:none; margin-top: -29px;' id='tblPDF'>
				       <tfoot>
				         <tr>
				         <td style='font-weight: bold; border-left: none;'></td>
				         <td style = 'text-align:center;font-weight:bold;'> Opening </td>
				         <td style = 'text-align:center;font-weight:bold;'> Current Month </td>
				         <td style = 'text-align:center;font-weight:bold;'> Closing </td>
				         </tr>                
				         <tr>
				           <td style = 'font-weight: bold;border-left: none;'><span id=''>Employee Contribution</span></td>
				           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["OpeningPF_Employee"]).ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["CurrentPF_Employee"]).ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["closingEmployee"]).ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td style = 'font-weight: bold; border-left: none;'><span id=''>Employer Contribution</span></td>
				           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["OpeningPF_Company"]).ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["CurrentPF_Company"]).ToString("N2") + @"</span></td>
				           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["ClosingEmployeer"]).ToString("N2") + @"</span></td>
				         </tr>
				         <tr>
				           <td style = 'font-weight: bold; border-left: none; border-bottom: none;'><span id=''>Total</span></td>
				           <td style = 'text-align: right; border-bottom: none;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["SumOpening"]).ToString("N2") + @"</span></td>
				           <td style = 'text-align: right; border-bottom: none;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["sumcurrent"]).ToString("N2") + @"</span></td>
				           <td style = 'text-align: right; border-bottom: none;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["totalsum2"]).ToString("N2") + @"</span></td>
				         </tr>            
				       </tfoot>
				     </table>";

				content = content.Replace("##PF_TABLE##", st);
				#endregion

		
				#endregion
			}
			catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
                //ReportPayroll.Visible = false;
                //ReportPayrollFotter.Visible = false;
            }

            return content;
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string content = "";
                string header = "";
                string footer = "";
				content = printPaySlip();

                Common.generatePDF(header, footer, content, "Pay Slip-" + Session["UserName"].ToString() + " (" + txtPayrollDate.Text + ")", "A4", "Portrait", 30, 30);
            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }
        }

    }
}