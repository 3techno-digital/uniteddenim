﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FnFDetails.aspx.cs" Inherits="Technofinancials.Finance.view.FnFDetails" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

   <title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style type="text/css">
		.table-1.B_class table {
			border: 0px !important;
			margin: 0px !important;
		}

		.table-1.B_class {
			border: 0px !important;
			padding: 0px !important;
			border-bottom: 0px !important;
		}

			.table-1.B_class td:nth-child(4) {
				text-align: right;
			}

			.table-1.B_class td:nth-child(2) {
				text-align: left;
			}

		@media only screen and (max-width: 768px) {
			.Box04.col-md-6 tr:nth-child(7) {
				height: 30px !important;
			}

			div#ReportPayroll img {
				width: 149px !important;
			}

			.OneLine h3 {
				font-size: 12px !important;
				font-weight: 600 !important;
			}

			.Box01 {
				padding-right: 0px !important;
			}

			.Box02 {
				padding-right: 0px !important;
				padding-left: 10px !important;
			}

			.OneLine span {
				font-size: 12px;
				line-height: 1.2;
			}

			.Box03 tbody {
				display: inherit !important;
				max-width: 150px !important;
				overflow: hidden !important;
			}

			.Box03 table {
				display: inherit !important;
				overflow: hidden !important;
			}
		}

		@media only screen and (max-width: 800px) {
			div#ReportPayroll img {
				width: 149px !important;
			}

			.OneLine h3 {
				font-size: 12px !important;
				font-weight: 600 !important;
			}

			.Box01 {
				padding-right: 0px !important;
			}

			.Box02 {
				padding-right: 0px !important;
				padding-left: 10px !important;
			}

			.OneLine span {
				font-size: 12px;
				line-height: 1.2;
			}

			.Box03 tbody {
				display: inherit !important;
				max-width: 150px !important;
				overflow: hidden !important;
			}

			.Box03 table {
				display: inherit !important;
				overflow: hidden !important;
			}
		}

		@media only screen and (max-width: 875px) {
			div#ReportPayroll img {
				width: 149px !important;
			}

			.OneLine h3 {
				font-size: 12px !important;
				font-weight: 600 !important;
			}

			.Box01 {
				padding-right: 0px !important;
			}

			.Box02 {
				padding-right: 0px !important;
				padding-left: 10px !important;
			}

			.OneLine span {
				font-size: 12px;
				line-height: 1.2;
			}

			.Box03 tbody, .Box04 tbody {
				display: inherit !important;
				max-width: 150px !important;
				overflow: hidden !important;
			}

			.Box03 table, .Box04 table {
				display: inherit !important;
				overflow: hidden !important;
			}
		}

		@media only screen and (max-width: 990px) and (min-width: 1000px) {
			.Box04.col-md-6.col-lg-6.col-sm-6 tr:nth-child(7) {
				height: 11.2rem !important;
			}

			div#ReportPayroll img {
				width: 149px !important;
			}

			.OneLine h3 {
				font-size: 12px !important;
				font-weight: 600 !important;
			}

			.Box01 {
				padding-right: 0px !important;
			}

			.Box02 {
				padding-right: 0px !important;
				padding-left: 10px !important;
			}

			.OneLine span {
				font-size: 12px;
				line-height: 1.2;
			}

			.Box03 tbody, .Box04 tbody {
				display: inherit !important;
				max-width: 150px !important;
				overflow: hidden !important;
			}

			.Box03 table {
				display: inherit !important;
				overflow: hidden !important;
			}
		}


		@media only screen and (max-width: 992px) {
			.Box04.col-md-6 tr:nth-child(7) {
				height: 106px !important;
			}

			.Box06 {
				margin-top: 23px !important;
			}

			.Box06 {
				margin-left: -0.2%;
			}
		}

		@media only screen and (max-width: 1020px) and (min-width: 1000px) {
			.Box04.col-md-6 tr:nth-child(7) {
				height: 35px !important;
			}

			.Box06 {
				margin-top: 23px !important;
			}

			.Box06 {
				margin-left: -0.2%;
			}
		}

		@media only screen and (max-width: 1200px) and (min-width: 1025px) {
			.Box04.col-md-6 tr:nth-child(6) {
				height: 28px !important;
			}

			.Box06 {
				margin-top: 23px !important;
			}
		}

		@media only screen and (max-width: 1599px) and (min-width: 1201px) {
			.Box04.col-md-6 tr:nth-child(6) {
				height: 28px !important;
			}

			.Box06 {
				margin-top: 24px !important;
			}
		}

		@media only screen and (max-width: 1024px) {
			.Box04.col-md-6 tr:nth-child(7) {
				height: 30px;
			}
		}

		.B_class {
			padding: 20px;
			/*width: 800px;
         margin: 0 auto;
         display: block;*/
			border: 2px solid #000;
			background: #fff;
		}

			.B_class h1,
			.B_class h2,
			.B_class h3,
			.B_class h4,
			.B_class p,
			.B_class td {
				margin: 0;
				font-family: lato;
				color: #000;
			}

			.B_class table {
				border: 2px solid;
				margin-top: -25px;
				width: 100%;
				text-align: left;
			}

				.B_class table td,
				.B_class table th {
					padding: 6.4px;
				}

			.B_class .slip-table thead tr th {
				background-color: #ffffff;
				border-bottom: 2px solid #000000 !important;
				border-top: 2px solid #000000 !important;
				color: #000;
				padding: 0 10px;
			}

		.slip-table-bordered thead tr th,
		.slip-table-bordered thead tr td,
		.slip-table-bordered tbody tr th,
		.slip-table-bordered tbody tr td {
			border-top: 2px solid transparent !important;
			border: 0;
			vertical-align: middle;
			padding: .1rem;
			font-size: 14px;
		}

		.slip-table thead tr th,
		.slip-table thead tr td,
		.slip-table tbody tr th,
		.slip-table tbody tr td,
		.slip-table tfoot tr th,
		.slip-table tfoot tr td {
			height: 32px;
			line-height: 1.428571429;
			vertical-align: middle;
			border-top: 2px solid #ddd;
			padding: 0 10px;
		}

		.slip-table {
			width: 100%;
			max-width: 100%;
			background-color: transparent;
			border-collapse: collapse;
			border-spacing: 0;
		}

		.slip-table-bordered tfoot tr th,
		.slip-table-bordered tfoot tr td {
			border-top: 2px solid #000 !important;
			border: 0;
			vertical-align: middle;
			padding: .1rem;
			font-size: 14px;
			padding: 0 10px;
		}

		.B_class .slip-table thead tr th {
			text-align: right;
		}

			.B_class .slip-table thead tr th:nth-child(1) {
				text-align: left;
			}

		.B_class .slip-table tbody tr td {
			text-align: right;
			height: 28px;
		}

			.B_class .slip-table tbody tr td:nth-child(1) {
				text-align: left;
			}

		.B_class .slip-table tfoot tr td {
			text-align: right;
			height: 34px;
		}

			.B_class .slip-table tfoot tr td:nth-child(1) {
				text-align: left;
				width: 200px !important;
			}

		.B_class hr {
			margin-top: 20px;
			margin-bottom: 10px;
			border: 0;
			border-top: 2px solid #000000;
		}

		.tableP {
			height: 34px;
			padding: 6.4px;
			border: 2px solid;
			margin-top: -6px !important;
			width: 99.9%;
		}

			.tableP span {
				margin-left: 40px;
				text-transform: capitalize;
			}

		.AddressSide {
			height: 90px;
			color: #000;
			text-align: center;
		}

			.AddressSide h1 {
				font-size: 18px;
				text-transform: capitalize;
				font-weight: 700;
				margin: 5px 0;
				color: #000;
			}

			.AddressSide h2 {
				font-size: 18px;
				text-align: right;
				font-weight: 400;
				color: #000;
				margin: 5px 0;
			}

			.AddressSide h3 {
				font-size: 18px;
				text-transform: capitalize;
				font-weight: 700;
				margin: 5px 0;
				color: #000;
			}

			.AddressSide h4 {
				font-size: 18px;
				text-transform: capitalize;
				font-weight: 700;
				margin-bottom: 15px;
				color: #000;
				margin: 5px 0;
			}

			.AddressSide p {
				font-size: 16px;
				font-weight: 500;
				max-width: 350px;
				text-transform: capitalize;
				color: #000;
			}

		.bigBox {
			width: 100%;
			border: 2px solid #000;
			height: 400px;
			margin: 50px 0 20px;
		}

		.Box01 {
			border: 2px solid #000;
			height: 412px;
			display: inline-block;
			padding: 10px;
			/* position: relative; */
			color: #000;
			border-right: 0;
		}

		.Box02 {
			border: 2px solid #000;
			height: 412px;
			display: inline-block;
			padding: 10px 20px;
			/* margin-right: -10px; */
			margin-left: -5px;
			/* position: relative; */
			color: #000;
			/* top: -34px; */
			border-left: 0;
		}

		.Box03 {
			display: inline-block;
		}

		.Box04 {
			margin-left: -0.1%;
			display: inline-block;
			padding-right: 0.1rem;
			padding-left: 0px;
		}

		.Box05 {
			margin-top: 24px;
			padding-left: 0px;
			padding-right: 0px;
		}

		.Box06 {
			margin-top: 23px;
			margin-left: -0.1%;
			display: inline-block;
			padding-right: 0.1rem;
			padding-left: 0px;
		}

		.OneLine {
			width: 100%;
			height: 28px;
		}

			.OneLine h3 {
				display: inline-block;
				font-size: 16px;
				font-weight: 700;
				margin: 3px 0;
				color: #000;
			}

			.OneLine p {
				display: inline-block;
				font-size: 16px;
				width: 60%;
				margin: 3px 0;
				color: #000;
			}

		.totalSalaries {
			font-weight: bold !important;
			color: #188ae2 !important;
			font-size: 16px !important;
		}

		.Box08 {
			height: 519px;
			padding-left: 0px;
			padding-right: 0.1rem;

		}
		.Box11 {
			height: 200px;
			padding-left: 0px;
			padding-right: 0.1rem;
		}
		.Box09 {
			padding-left: 0px;
			padding-right: 0px;
			margin-top: 23px;
		}

		.Box07 {
			margin-top: 23px;
			padding-left: 0px;
			padding-right: 0.1rem;
			width: 99.9%;
		}

		.Box04.col-md-6 tr:nth-child(6) {
			height: 28px;
		}

		.line-four {
			width: 99.9%;
			display: block;
		}

		.table-1 th:nth-child(2), .table-1 td:nth-child(2), .table-1 td:nth-child(3), .table-1 th:nth-child(3) {
			text-align: right;
		}

		.Box07 .table-1 td:nth-child(2) {
			text-align: left;
		}

		.Box09.col-md-6.col-lg-6.col-sm-6.main table {
			border-top: 1px solid #fff !important;
		}

		.table-1.B_class table {
			border: 0px !important;
			margin: 0px !important;
		}

		.table-1.B_class {
			border: 0px !important;
			padding: 0px !important;
			border-bottom: 0px !important;
		}

			.table-1.B_class td:nth-child(1) {
				width: 50%;
			}

		.Box09 table {
			border-top-color: #fff !important;
			border-bottom-color: #fff !important;
		}

		.Box03 table {
			border-bottom-color: #fff !important;
		}

		.extra table, .Box04 table {
			border-bottom-color: #fff !important;
		}

		.Box06 table {
			border-bottom-color: #fff !important;
		}

		.table-1.B_class td:nth-child(4) {
			text-align: right;
		}

		.table-1.B_class td:nth-child(2) {
			text-align: left;
		}

		.Box04 tr:nth-child(7) td {
			height: 17px;
		}
	</style>
    <style>
        .scroller{
            overflow-x: scroll !important;
   
        }
        th {
            position: sticky;
            top: 0; /* Don't forget this, required for the stickiness */
            background: #f7f7f7;
            color: #343a40;
        }

        .col-lg-4.col-md-6.col-sm-12 button#btnView {
            padding: 3px 24px;
            margin-top: 3px !important;
        }
     
        

        .green-background {
            background-color: rgb(152, 251, 152);
            color: rgb(255, 255, 255);
            font-weight: 600;
        }

        div#myModal2 {
            background-color: #3b3e4796 !important;
        }

        .modal-dialog {
            z-index: 9999;
            opacity: 1;
        }

        #modalbtnSave {
            cursor: pointer;
        }

        .modal-open {
            overflow: hidden;
        }

        .AD_btnn.two {
            font-size: 14px !important;
            margin: 0 !important;
            padding: 0 !important;
            cursor: pointer;
        }

        .AD_btnn {
            margin: 0 10px;
            font-size: 18px;
            font-weight: 700;
            color: #003780;
            background: none;
            border: 0;
            border-bottom: 2px solid transparent !important;
            border-radius: 0;
            padding: 5px;
        }

            .AD_btnn:hover {
                color: #000;
                border-bottom: 2px solid #000 !important;
            }

        .SaveBTn {
            padding: 20px 30px !important;
            margin: 0px !important;
        }

        div#myModal2 .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
            line-height: 1.3 !important;
        }

        .Time-in h4, .Time-out h4, .Breaks h4, .BreakIN h4, .Breakout h4, .Rmarks h4 {
            color: #000;
            font-weight: 600;
            margin: 10px 0 5px;
            text-align: left;
        }

        .Time-in, .Time-out, .Breaks, .BreakIN, .Breakout, .Rmarks {
            padding: 0px 15px;
            text-align: left;
        }

        .content-header h1 {
            margin-left: 8px !important;
        }

        #chartdiv {
            width: 100%;
            height: 500px;
        }

        .color_infoP h3 {
            font-size: 15px;
            margin: 0 !important;
        }

        .attendance-wrapper {
            text-align: right;
        }

        .color_infoP h3 span {
            margin-left: 15px;
        }

        .pr_0 {
            padding-right: 0 !important;
        }

        input#txtStartDate, input#txtEndDate {
            border-radius: 5px;
        }

        .form-group {
            display: block;
        }

        div#myModal .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
        }

        #LeavesGuage {
            width: 100%;
            height: 500px;
        }

        .table-bordered > tbody > tr > th {
            border-top-style: none !important;
        }

        #PieChart {
            width: 100%;
            height: 500px;
        }

        #LeavesPieChart {
            width: 100%;
            height: 500px;
        }

        .widget-body {
            padding-top: 0;
        }

        .calender {
        }

        .calenderHeading {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .form-group.color_infoP {
            width: fit-content;
            display: inline-block;
            font-size: 16px;
        }

        .content-header h1 {
            font-size: 20px !important;
        }

        .calenderCell {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .wrap.p-t-0.tf-navbar.tf-footer-wrapper {
            right: 0 !important;
        }

        g[aria-labelledby="id-66-title"] {
            display: none;
        }

        div#myModal .modal-content {
            top: 122px;
            width: 20%;
            position: fixed;
            z-index: 1050;
            left: 80%;
            height: 70%;
            box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
        }

        .modal-content .content-header {
            border-top-left-radius: 11px;
            border-top-right-radius: 11px;
            padding-top: 10px;
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
                left: 70%;
                width: 30%;
            }

            table#gvAttendanceSummary {
                width: 991px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
                left: 70%;
                width: 30%;
            }

            table#gvAttendanceSummary {
                width: 1024px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }

            table#gvAttendanceSummary {
                width: 1024px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }
        }

        @media only screen and (max-width: 1439px) and (min-width: 1200px) {
            .single-key h3 {
                font-size: 15px;
                padding: 5px;
            }
        }

        @media only screen and (max-width: 1505px) and (min-width: 1440px) {
            .single-key h3 {
                font-size: 18px;
            }
        }

        .mini-stat.clearfix.present {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #1fb5ac !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            margin-top: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.absent {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #fa8564 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.leave {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #a48ad4 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.holiday {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f4b9b9 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.missing {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f9c851 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.short {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #aec785 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat-info {
            font-size: 12px;
            padding-top: 2px;
            color: #767676 !important;
            font-weight: 700;
        }

            .mini-stat-info span {
                display: block;
                font-size: 24px;
                font-weight: 600;
                color: #767676 !important;
            }



        .mini-stat {
            border-radius: 10px !important;
        }

        .btnAdjustmentTrue {
            display: none;
        }

       /* .gv-overflow-scrool div {
            overflow-x: auto;
            overflow-y: auto;
            height: 450px;
            padding-right: 5px;
        }*/

        .AD_btn {
            font-size: 16px;
            border: 0;
            padding: 0px;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">

            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">FnF Details</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div style="text-align: right;">
                                    <button class="AD_btn" id="btnApproveByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Paycycle Lock</button>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                    <button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Dis Approve</button>
                                    <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                   
                                  
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="pull-right flex">
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade M_set" id="notes-modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="m-0 text-dark">Notes</h1>
                                        <div class="add_new">
                                            <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                            <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                        </p>
                                        <p>
                                            <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes..." class="form-control"></textarea>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">

                              <%--  <div class="col-sm-6">
                                    <div class="form-group">

                                        <h4>Payroll Date  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPayrollDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />

                                    </div>
                                </div>--%>
                                     <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee<span style="color: red !important;">*</span></h4>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control select2" data-plugin="select2">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h4>&nbsp;</h4>
                                    <button class="AD_btn_inn" id="btnView" runat="server" onserverclick="ddlYear_SelectedIndexChangedNew" type="button">View</button>
                                </div>
                            
								

                            </div>


                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12"></div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12 gv-overflow-scrool">
                                        <asp:HiddenField ID="hdnRowNo" runat="server" />
                                          <asp:GridView ID="gvSalaries" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" ShowFooter="true" AutoGenerateColumns="true"  ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found">
                                       <Columns>
                                          

                               
                                       </Columns>
                                        </asp:GridView>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row" style="display: none;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <asp:GridView ID="gvNormalSal" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("EmployeeSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Over Time Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTimeAmount" Text='<%# Eval("OverTimeAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Commission">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Spiffs">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Bonuses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Leave Encashment">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PF Contribution">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Graduity">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Other Expenses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOtherExpenses" Text='<%# Eval("OtherExpenses","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Gross Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblGrossSallary" Text='<%# Eval("GrossSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Deductions">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("Deductions","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Absent Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAbsentAmount" Text='<%# Eval("AbsentAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Tax","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="E.O.B.I">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEOBI" Text='<%# Eval("EOBI","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Provident Fund">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeePF" Text='<%# Eval("EmployeePF","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ClearanceDeduction">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("ClearanceDeductionAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("NetAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
       
                   
                </section>

            </div>


            	
			<div id="elementH"></div>
            <!-- #dash-content -->
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
               <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

		<!-- jQuery library -->


<!-- jsPDF library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
        <script>

		
            function ViewSlip(empid) {
				var hosting = window.location.hostname;
				console.log(hosting);
                var url = '/3techno-digital/finance/view/fnf-Report-' + empid;
              
				window.open(url, '_blank').focus();
             
			}


			function GetFnFData() {

				$.ajax({
					type: "POST",
					url: '/business/services/EssService.asmx/GetFnFReport',
					data: '{month: ' + JSON.stringify($('#txtPayrollDate').val()) + '}',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response) {
					
                        console.log(response.d);
                        var data = response.d;
						var len = data.length;
						var txt = "";
                        if (len > 0) {
							for (var i = 0; i < len; i++) {
								


								txt += "<tr><td><input type=\"checkbox\" onclick=\"checkme(" + data[i].empid + ")\" id=\"" + data[i].empid + "\" /></td><td>" + data[i].wdid + "</td><td>" + data[i].employeename + "</td><td>" + data[i].deptName + "</td><td>" + data[i].EmployeeSeperationDate + "</td><td>" + data[i].PFContribution + "</td><td>" + data[i].Gratuity + "</td><td>" + data[i].ClearanceDeductionAmount + "</td><td>" + data[i].AdonsDeductions + "</td><td>" + data[i].AdditionalTaxperMonth + "</td><td>" + data[i].TotalDeduction + "</td><td>" + data[i].NetAmount + "</td><td><button onclick=\"ViewSlip(" + data[i].empid +")\" class=\"AD_btn\" id=\"" + data[i].SeperationDetailsID+"\"  type=\"bUpdaten\">View</button></td></tr>";

                            }
                            if (txt != "") {
                              
								$("#table").empty();
								var header = "<tr><th><input type=\"checkbox\" onclick=\"checkall(this)\" /></th><th> WDID</th><th>EmployeeName</th><th>Department</th><th>Separation date</th><th>PFContribution</th><th>Gratuity</th><th>ClearanceDeductionAmount</th><th>AdonsDeductions</th><th>AdditionalTaxperMonth</th><th>TotalDeduction</th><th>NetAmount</th><th>Action</th></tr>";
								$("#table").append(header);
                             
                                $("#table").append(txt);
                            }
                        }
					},
					
					error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
				});
			}

		</script>
    </form>
</body>
</html>
