﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttendanceAdjustment.aspx.cs" Inherits="Technofinancials.Finance.view.AttendanceAdjustment" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>
		.tf-note-btn {
			background-color: #575757;
			padding: 10px 8px 7px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-note-btn i {
				color: #fff !important;
			}

		.tf-disapproved-btn {
			background-color: #575757;
			padding: 10px 8px 7px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-disapproved-btn i {
				color: #fff !important;
			}

		div#gv_wrapper {
			margin-bottom: 20%;
		}

		.tf-add-btn {
			background-color: #575757;
			padding: 4px 9px 4px 10px !important;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-add-btn i {
				color: #fff !important;
			}

		.tf-add-btn {
			background-color: #575757;
			padding: 12px 10px 8px 10px !important;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center span.multiselect-selected-text {
			white-space: nowrap;
		}

		.tf-add-btn i {
			color: #fff !important;
		}

		.tf-back-btn {
			background-color: #575757;
			padding: 10px 10px 10px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-back-btn i {
				color: #fff !important;
			}

		.tf-back-btn {
			background-color: #575757 !important;
			border-radius: 0px !important;
			box-shadow: 0 8px 6px -5px #cacaca !important;
			height: 38px !important;
			width: 37px !important;
			display: inline-block !important;
			padding: 5px !important;
			position: relative !important;
			text-align: center !important;
		}

		.tf-back-btn {
			background-color: #575757;
			padding: 10px 10px 10px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

		.open > .dropdown-menu {
			display: block;
			width: 300px !important
		}

			.open > .dropdown-menu ::-webkit-scrollbar {
				width: 10px;
			}

		.dropdown-menu {
			box-shadow: none !important;
		}

		.multiselect-container.dropdown-menu {
			overflow: hidden;
		}

		.tf-back-btn i {
			color: #fff !important;
		}

		.dropdown-menu {
			border-radius: 0px;
		}

		.total {
			font-weight: bold;
			font-size: 20px;
			color: #188ae2;
		}

		button#btnView {
			padding: 4px 24px;
			margin-top: 15px;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center {
			width: 200px;
			text-align: left !important;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center {
			background-color: #fff;
			border: 1px solid #aaa;
			border-radius: 5px;
			padding-left: 8px;
			padding-right: 8px;
			padding: 3px 37px;
			text-align: left !important;
		}

		.open > .dropdown-menu {
			padding: 15px;
			max-height: 250px !important;
			border: 1px solid #aaa;
			border-bottom-left-radius: 5px;
			border-bottom-right-radius: 5px;
		}

		multiselect-filter {
			margin-bottom: 15px;
		}

		.multiselect-container.dropdown-menu {
			overflow: overlay !important;
		}

		.multiselect-container .multiselect-all .form-check, .multiselect-container .multiselect-group .form-check, .multiselect-container .multiselect-option .form-check {
			padding: 0px;
		}

		label.form-check-label.font-weight-bold {
			margin: 0px;
		}

		span label {
			font-size: 14px !important;
			margin: 0px;
		}

		@media only screen and (max-width: 1280px) and (min-width: 800px) {
			section.app-content .col-lg-4.col-md-6.col-sm-12 {
				width: 40%;
			}

			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 25px !important;
			}
		}

		@media only screen and (max-width: 1440px) and (min-width: 900px) {

			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 0px !important;
				width: 170px;
			}
		}

		@media only screen and (max-width: 1024px) and (min-width: 993px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 15px !important;
				width: 130px;
			}
		}

		@media only screen and (max-width: 992px) and (min-width: 768px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 50px;
				width: 80px;
			}

			.app-content .form-group h4 {
				font-size: 10px !important;
			}
		}

		@media only screen and (max-width: 1599px) and (min-width: 1201px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 37px;
			}
		}

		.multiselect-container.dropdown-menu {
			overflow: hidden auto !important;
		}

		.navbar-toolbar > li > .dropdown-menu {
			width: 190px !important;
		}

		.open > .dropdown-menu {
			max-height: 300px !important;
		}

		.dash_User {
			width: 190px !important;
			border-radius: 10px;
			padding: 0 !important;
			border: 1px solid rgba(0,0,0,.15);
		}

		/*        .content-header .AD_btn {
            font-size: 15px;
        }*/

		a#lnkReject {
			font-weight: bold !important;
		}

		.navbar-toolbar > li > .dropdown-menu {
			box-shadow: 0 3px 12px rgb(0 0 0 / 18%) !important;
		}

		@media only screen and (max-width: 992px) and (min-width: 768px) {
			h1.m-0.text-dark {
				font-size: 17px !important;
			}
		}
	</style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">
			<input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
			<div class="wrap">
				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
								<h1 class="m-0 text-dark">Attendance Adjustments</h1>
							</div>

							<!-- /.col -->
							<div class="col-md-4 col-sm-6">
								<div style="text-align: right;">
									<button class="AD_btn" id="btnApprove" runat="server" onserverclick="btnApprove_ServerClick" validationgroup="btnValidate" type="button"><%--<i class="fa fa-thumbs-up"></i>--%> Approve</button>
									<asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn" CommandArgument='Reject' data-toggle="modal" data-target="#notes-modal" value="Add Note"><%--<i class="fa fa-thumbs-down"></i>--%> Reject</asp:LinkButton>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/attendance-adjustment/add-new-attendance-adjustment"); %>" class="AD_btn">Add</a>
								</div>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.container-fluid -->
				</div>

				<!-- Modal -->
				<div class="modal fade M_set" id="notes-modal" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h1 class="m-0 text-dark">Notes</h1>
								<div class="add_new">
									<button type="button" class="AD_btn" data-dismiss="modal">Close</button>
									<button type="button" class="AD_btn" id="Reject" runat="server" onserverclick="btnApprove_ServerClick" data-dismiss="modal">Save</button>
								</div>
							</div>
							<div class="modal-body">
								<p>
									<asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
								</p>
								<p>
									<textarea id="txtAdjustmentNote" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
								</p>
							</div>
						</div>
					</div>
				</div>

				<section class="app-content">

					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>From Date
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
										<asp:TextBox runat="server" CssClass="form-control" ID="txtFromDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>To Date
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
										<asp:TextBox runat="server" CssClass="form-control" ID="txtToDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Department Name</h4>
										<asp:DropDownList ID="ddlDepartment" runat="server" onchange="GetDesignationsByDepartmentIdAndCompanyId()" class="form-control select2" data-plugin="select2" AutoPostBack="false">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Designation Name</h4>
										<asp:DropDownList ID="ddlDesignation" runat="server" OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged" class="form-control select2" data-plugin="select2" AutoPostBack="false">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6" style="display: none">
									<h4>Employee<span style="color: red !important;">*</span>
										<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlEmployee" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
									<asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged"></asp:DropDownList>
								</div>
							</div>

						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Adjustment Status</h4>
										<asp:DropDownList ID="ddlAdjustmentStatus" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Employee Name<span style="color: red !important;">*</span>
											<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="ddlEmployee" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
										<br />
										<asp:ListBox SelectionMode="Multiple" ID="lstEmployees" runat="server"></asp:ListBox>
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Employee Code</h4>
										<asp:TextBox ID="txtKTId" runat="server" class="form-control">
										</asp:TextBox>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="clear-fix">&nbsp;</div>
									<div>
										<button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="tab-content ">
								<div class="tab-pane active">
									<asp:GridView ID="gv" runat="server" CssClass="table table-bordered table-resposive gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
										<Columns>

											<asp:TemplateField HeaderText="Sr.No.">
												<ItemTemplate>
													<%#Container.DataItemIndex+1 %>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="">
												<HeaderTemplate>
													<asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chckchanged" ID="checkAll" ToolTip="Click To Select All"></asp:CheckBox>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:CheckBox runat="server" Enabled='<%# Eval("DocStatus").ToString()=="Data Submitted for Review"||Eval("DocStatus").ToString()=="Saved as Draft"? true : false %>' ID="check"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Employee Code">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblCode" Text='<%# Eval("WorkDayID") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Employee">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Date">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblAttendanceDate" Text='<%# Convert.ToDateTime( Eval("AttendanceDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Time In">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblTimeIn" Text='<%# Convert.ToDateTime( Eval("TimeIn")).ToString("hh:mm tt") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Time Out">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblTimeOut" Text='<%# Convert.ToDateTime( Eval("TimeOut")).ToString("hh:mm tt") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
												<asp:TemplateField HeaderText="Prepared Date">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblPrepareDate" Text='<%# Convert.ToDateTime( Eval("PreparedDate")).ToString("dd-MMM-yyy") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Status">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblDocStatus" Text='<%# Eval("DocStatus") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

										
										
											      <asp:TemplateField HeaderText="Status By">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblApproveby" Text='<%#  Eval("ApprovedBy") ==  DBNull.Value ? "" : Eval("ApprovedBy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Status Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblApprovedDate" Text='<%# Eval("ApprovedDate") ==  DBNull.Value ? "" : Convert.ToDateTime( Eval("ApprovedDate")).ToString("dd-MMM-yyy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
											<asp:TemplateField HeaderText="View">
												<ItemTemplate>
													<a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/finance/manage/attendance-adjustment/edit-attendance-adjustment-" + Eval("AttendanceAdjustmentID") %>" class="AD_stock">View </a>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField Visible="false">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblResultant" Text='<%#  Eval("Resultant") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField Visible="false">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblEmployeeID" Text='<%# Eval("EmployeeID") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField Visible="false">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblBreakStart" Text='<%# Convert.ToDateTime( Eval("BreakTime")).ToString("hh:mm tt") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField Visible="false">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblBreakEnd" Text='<%# Convert.ToDateTime( Eval("BreakEndTime")).ToString("hh:mm tt") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField Visible="false">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblAttendanceAdjustmentID" Text='<%# Eval("AttendanceAdjustmentID") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</div>
							</div>
						</div>
						<div class="">
							<br />
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-md-12">
									<asp:UpdatePanel ID="UpdatePanel4" runat="server">
										<ContentTemplate>
											<div class="form-group" id="divAlertMsg" runat="server">
												<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
													<span>
														<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
													</span>
													<p id="pAlertMsg" runat="server">
													</p>
												</div>
											</div>
										</ContentTemplate>
									</asp:UpdatePanel>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- #dash-content -->
			</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>
		<script src="/business/scripts/ViewDesignations.js"></script>

		<script src="/business/scripts/ViewDesignations.js"></script>
		<script type="text/javascript">  
			$(function () {
				$('[id*=lstEmployees]').multiselect({
					includeSelectAllOption: true,
					buttonContainer: '<div class="btn-group" />',
					includeSelectAllIfMoreThan: 0,
					enableFiltering: true,
					filterPlaceholder: 'Search',
					filterBehavior: 'text',
					includeFilterClearBtn: true,
					enableCaseInsensitiveFiltering: true,
					numberDisplayed: 1,
					maxHeight: true,
					maxHeight: 350,
					templates: {
						button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span></button>',
						Container: '<div class="multiselect-container dropdown-menu"></div>',
						filter: '<div class="multiselect-filter"><div class="input-group input-group-sm p-1"><div class="input-group-prepend"></div><input class="form-control multiselect-search" type="text" /></div></div>',

						filterClearBtn: '<div class="input-group-append"><button class="multiselect-clear-filter input-group-text" type="button"><i class="fas fa-times"></i></button></div>',

						option: '<button class="multiselect-option dropdown-item"></button>',

						divider: '<div class="dropdown-divider"></div>',

						optionGroup: '<button class="multiselect-group dropdown-item"></button>',

						resetButton: '<div class="multiselect-reset text-center p-2"><button class="btn btn-sm btn-block btn-outline-secondary"></button></div>'
					}
				});
			});

		</script>
	</form>
</body>
</html>
