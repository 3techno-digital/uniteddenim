﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.view
{
    public partial class OvertimeGroup : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                GetData();

            }
        }

       
        private void GetData()
        {
            CheckSessions();
            DataTable dt = new DataTable();


            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            dt = objDB.GetOverTimeGroup(ref errorMsg);

            gv.DataSource = null;
            gv.DataBind();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.DataSource = dt;
                    gv.DataBind();
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

            Common.addlog("ViewAll", "ESS", "All employee-OverTime Viewed", "EmployeeAttendance");
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }
      

        protected void btnApprove_ServerClick(object sender, EventArgs e)
        {

        }

       
    }
}