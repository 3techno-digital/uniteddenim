﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OtherExpenses.aspx.cs" Inherits="Technofinancials.Finance.view.OtherExpenses" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">

                 <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Other Expenses Approval</h1>
                            </div>
                            <!-- /.col -->
                         <%--   <div class="col-sm-4">
                                <div style="text-align: right;">
                                        <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/Other-expense/add-new-Other-expense"); %>" class="AD_btn">Add</a>
                            
                                    </div>
                            </div>--%>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>



                <section class="app-content">
                   	<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>From Date
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
										<asp:TextBox runat="server" CssClass="form-control" ID="txtFromDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>To Date
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
										<asp:TextBox runat="server" CssClass="form-control" ID="txtToDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
									</div>
								</div>
									<div class="col-sm-6">
										<div class="form-group">
									<div>
										<button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
									</div>
                                            </div>
								</div>
							
							
							</div>

						</div>
					
					</div>
					
                    <div class="row ">
                      <div class="col-sm-12">
                                 <div class="tab-content ">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                <asp:GridView ID="gvAnnouncement" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Employee Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName")  %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblOtherExpenseDate" Text='<%#Convert.ToDateTime(Eval("OtherExpenseDate")).ToString("dd/MM/yyyy")  %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Exp. Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblOtherExpenseCode" Text='<%# Eval("OtherExpenseCode") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                            
                                            <asp:TemplateField HeaderText="Title">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Amount">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblNetAmount" Text='<%# Eval("NetAmount","{0:N}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol" Text='<%# Eval("DocStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Reviewed By">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol" Text='<%# Eval("ReviewedBy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Finalized By">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblApproveby" Text='<%# Eval("ApprovedBy") ==  DBNull.Value ? "" : Eval("ApprovedBy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Finalized On">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblApprovedDate" Text='<%# Eval("ApprovedDate") ==  DBNull.Value ? "" : Convert.ToDateTime( Eval("ApprovedDate")).ToString("dd-MMM-yyy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/finance/manage/Other-expense/edit-Other-expense-" + Eval("OtherExpenseID") %>" class="AD_stock" > View </a>
                                                    <asp:LinkButton ID="lnkDelete" style="display:none" runat="server" CssClass="delete-class" CommandArgument='<%# Eval("OtherExpenseID") %>' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

        <style>
            .content-header.second_heading .container-fluid {
    padding-left: 0px!important;
}
.content-header.second_heading h1 {
    margin-left: 0px !important;
}
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
                .tf-back-btn {
    background-color: #575757 !important;
    border-radius: 0px !important;
    box-shadow: 0 8px 6px -5px #cacaca !important;
    height: 38px!important;
    width: 37px!important;
    display: inline-block!important;
    padding: 5px !important;
    position: relative!important;
    text-align: center!important;
}
                @media only screen and (max-width: 1390px) and (min-width: 1200px) {
                 col-sm-12 textarea#txtNotes {
    margin-left: 35px;
}
                    .col-sm-12 h4 {
                       padding-left: 35px;
                    }
                }
        </style>
    </form>
</body>
</html>
