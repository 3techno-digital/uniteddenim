﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.view
{
	public partial class Deduction : System.Web.UI.Page
	{
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int AddOnsExpenseID
        {
            get
            {
                if (ViewState["AddOnsExpenseID"] != null)
                {
                    return (int)ViewState["AddOnsExpenseID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AddOnsExpenseID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetAllDeductionsByCompanyID(ref errorMsg);
            gvAddOns.DataSource = dt;
            gvAddOns.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvAddOns.UseAccessibleHeader = true;
                    gvAddOns.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All Currency Viewed", "Currency");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            int ID = Convert.ToInt32(btn.CommandArgument);
            objDB.AddOnsExpenseID = ID;
            objDB.DeletedBy = Session["UserName"].ToString();

            objDB.DeleteAddOnsExpenseByID();
            GetData();
        }

    }
}