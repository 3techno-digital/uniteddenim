﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tax.aspx.cs" Inherits="Technofinancials.Finance.view.tax" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>

        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <img src="/assets/images/Admin_tax_rates.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Tax Rate</h3>
                        </div>
                        <div class="col-sm-4" style="margin-top: 10px;">
                            <div class="pull-right">
                                <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/tax/add-tax"); %>" class="tf-add-btn" "Add"><i class="far fa-plus-circle"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div>&nbsp;</div>
                            <div class="col-sm-12 table-09-main-div">
                                <div class="add-table-div gv-overflow-scrool">
                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Tax Rate Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("TaxRateName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                       
                                            <asp:TemplateField HeaderText="Ancronym">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblShortCode" Text='<%# Eval("TaxRateShortName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                      
                                            <asp:TemplateField HeaderText="Tax Percentage">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblUserEmail" Text='<%# Eval("TaxRatePer") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                       
                                            <asp:TemplateField HeaderText="Prepared Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblUserCommeasdfasfasnts" Text='<%# Eval("PreparedDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--<asp:TemplateField HeaderText="Reviewed By">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblUserCommeasfasnts" Text='<%# Eval("ReviewedBy") + " - " + Eval("ReviewedDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Approved By">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblUserCommesssnts" Text='<%# Eval("ApprovedBy") + " - " + Eval("ApprovedDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>

                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblStauss" Text='<%# Eval("DocStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="lnkCommand" CommandArgument='<%# Eval("TaxRateID") %>' OnClick="lnkCommand_Click"> View</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }
        </style>
    </form>
</body>
</html>
