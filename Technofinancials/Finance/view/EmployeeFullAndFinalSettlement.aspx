﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeFullAndFinalSettlement.aspx.cs" Inherits="Technofinancials.Finance.view.EmployeeFullAndFinalSettlement" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        .scroller{
            overflow-x: scroll !important;
   
        }
        th {
            position: sticky;
            top: 0; /* Don't forget this, required for the stickiness */
            background: #f7f7f7;
            color: #343a40;
        }

        .col-lg-4.col-md-6.col-sm-12 button#btnView {
            padding: 3px 24px;
            margin-top: 3px !important;
        }
     
        

        .green-background {
            background-color: rgb(152, 251, 152);
            color: rgb(255, 255, 255);
            font-weight: 600;
        }

        div#myModal2 {
            background-color: #3b3e4796 !important;
        }

        .modal-dialog {
            z-index: 9999;
            opacity: 1;
        }

        #modalbtnSave {
            cursor: pointer;
        }

        .modal-open {
            overflow: hidden;
        }

        .AD_btnn.two {
            font-size: 14px !important;
            margin: 0 !important;
            padding: 0 !important;
            cursor: pointer;
        }

        .AD_btnn {
            margin: 0 10px;
            font-size: 18px;
            font-weight: 700;
            color: #003780;
            background: none;
            border: 0;
            border-bottom: 2px solid transparent !important;
            border-radius: 0;
            padding: 5px;
        }

            .AD_btnn:hover {
                color: #000;
                border-bottom: 2px solid #000 !important;
            }

        .SaveBTn {
            padding: 20px 30px !important;
            margin: 0px !important;
        }

        div#myModal2 .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
            line-height: 1.3 !important;
        }

        .Time-in h4, .Time-out h4, .Breaks h4, .BreakIN h4, .Breakout h4, .Rmarks h4 {
            color: #000;
            font-weight: 600;
            margin: 10px 0 5px;
            text-align: left;
        }

        .Time-in, .Time-out, .Breaks, .BreakIN, .Breakout, .Rmarks {
            padding: 0px 15px;
            text-align: left;
        }

        .content-header h1 {
            margin-left: 8px !important;
        }

        #chartdiv {
            width: 100%;
            height: 500px;
        }

        .color_infoP h3 {
            font-size: 15px;
            margin: 0 !important;
        }

        .attendance-wrapper {
            text-align: right;
        }

        .color_infoP h3 span {
            margin-left: 15px;
        }

        .pr_0 {
            padding-right: 0 !important;
        }

        input#txtStartDate, input#txtEndDate {
            border-radius: 5px;
        }

        .form-group {
            display: block;
        }

        div#myModal .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
        }

        #LeavesGuage {
            width: 100%;
            height: 500px;
        }

        .table-bordered > tbody > tr > th {
            border-top-style: none !important;
        }

        #PieChart {
            width: 100%;
            height: 500px;
        }

        #LeavesPieChart {
            width: 100%;
            height: 500px;
        }

        .widget-body {
            padding-top: 0;
        }

        .calender {
        }

        .calenderHeading {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .form-group.color_infoP {
            width: fit-content;
            display: inline-block;
            font-size: 16px;
        }

        .content-header h1 {
            font-size: 20px !important;
        }

        .calenderCell {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .wrap.p-t-0.tf-navbar.tf-footer-wrapper {
            right: 0 !important;
        }

        g[aria-labelledby="id-66-title"] {
            display: none;
        }

        div#myModal .modal-content {
            top: 122px;
            width: 20%;
            position: fixed;
            z-index: 1050;
            left: 80%;
            height: 70%;
            box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
        }

        .modal-content .content-header {
            border-top-left-radius: 11px;
            border-top-right-radius: 11px;
            padding-top: 10px;
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
                left: 70%;
                width: 30%;
            }

            table#gvAttendanceSummary {
                width: 991px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
                left: 70%;
                width: 30%;
            }

            table#gvAttendanceSummary {
                width: 1024px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }

            table#gvAttendanceSummary {
                width: 1024px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }
        }

        @media only screen and (max-width: 1439px) and (min-width: 1200px) {
            .single-key h3 {
                font-size: 15px;
                padding: 5px;
            }
        }

        @media only screen and (max-width: 1505px) and (min-width: 1440px) {
            .single-key h3 {
                font-size: 18px;
            }
        }

        .mini-stat.clearfix.present {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #1fb5ac !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            margin-top: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.absent {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #fa8564 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.leave {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #a48ad4 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.holiday {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f4b9b9 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.missing {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f9c851 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.short {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #aec785 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat-info {
            font-size: 12px;
            padding-top: 2px;
            color: #767676 !important;
            font-weight: 700;
        }

            .mini-stat-info span {
                display: block;
                font-size: 24px;
                font-weight: 600;
                color: #767676 !important;
            }



        .mini-stat {
            border-radius: 10px !important;
        }

        .btnAdjustmentTrue {
            display: none;
        }

       /* .gv-overflow-scrool div {
            overflow-x: auto;
            overflow-y: auto;
            height: 450px;
            padding-right: 5px;
        }*/

        .AD_btn {
            font-size: 16px;
            border: 0;
            padding: 0px;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">

            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">Full And Final Settlement Lock </h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div style="text-align: right;">
                                    <button class="AD_btn" id="btnApproveByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Paycycle Lock</button>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                    <button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Dis Approve</button>
                                    <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                   
                                    <h4>&nbsp;</h4>
                                    <button style="display:none;float: right;" class="AD_btn" id="btnRelease"  onclick="ReleaseFnF()" type="button">Disburse</button>
                                 <button style="display:none;float: right;" class="AD_btn" id="btnLockPayment"  onclick="LockFnF()" type="button">Lock</button>
                                
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="pull-right flex">
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade M_set" id="notes-modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="m-0 text-dark">Notes</h1>
                                        <div class="add_new">
                                            <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                            <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                        </p>
                                        <p>
                                            <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes..." class="form-control"></textarea>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <h4>Payroll Date  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPayrollDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />

                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <h4>&nbsp;</h4>
                                    <button class="AD_btn_inn" id="btnView"  onclick="GetFnFData()" type="button">View</button>
                                </div>
                            



                            </div>


                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12"></div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12 gv-overflow-scrool">
                                        <asp:HiddenField ID="hdnRowNo" runat="server" />
                                        <asp:GridView ID="gvSalaries" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" ShowFooter="true" AutoGenerateColumns="false"  ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr. No">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex + 1%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Code">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Salary">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeSalary" Text='<%# Eval("EmployeeSalary","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Over Time">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblOverTime" Text='<%# Eval("OverTimeAmount","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Commission">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Spiffs">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Bonuses">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Leave Encashment">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("LeaveEncashment","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="PF Contribution">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("PFContribution","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Gratuity">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("Graduity","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Other Expenses">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblOtherExpenses" Text='<%# Eval("OtherExpenses","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Gross Salary">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("GrossSalary","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="Number" ID="txtEditBasicSalary" CssClass="form-control" Text='<%# Eval("GrossSalary") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Deductions">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("Deductions","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Absent Amount">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblAbsentAmount" Text='<%# Eval("AbsentAmount","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Tax">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Tax","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="Number" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("Tax") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Variable Tax">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTax" Text='<%# Eval("AdditionalTax","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="Number" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("AdditionalTax") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="E.O.B.I">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEOBI" Text='<%# Eval("EOBI","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="Number" ID="txtEditEOBI" CssClass="form-control" Text='<%# Eval("EOBI") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Provident Fund">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeePF" Text='<%# Eval("EmployeePF","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="Number" ID="txtEditEmployeePF" CssClass="form-control" Text='<%# Eval("EmployeePF") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ClearanceDeduction">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("ClearanceDeductionAmount","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Total Deduction">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lbltotalded" Text='<%# Eval("TotalDeduction","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Other Deductions">
                                                        <ItemTemplate>  
                        <asp:TextBox runat="server" TextMode="Number" ID="txtotherded" CssClass="form-control" Text='<%# Eval("OtherDeductionsInput") %>'></asp:TextBox>
                                      
             
                    </ItemTemplate>  
                  
                             
                                    
                                      
                                    </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Net Amount">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("NetAmount","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                             <asp:TemplateField>  
                    <ItemTemplate>  
                        <asp:LinkButton ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" OnClick="RowUpdating" />  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update"/>  
                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel"/>  
                    </EditItemTemplate>  
                </asp:TemplateField>  
             
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row" style="display: none;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <asp:GridView ID="gvNormalSal" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("EmployeeSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Over Time Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTimeAmount" Text='<%# Eval("OverTimeAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Commission">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Spiffs">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Bonuses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Leave Encashment">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PF Contribution">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Graduity">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Other Expenses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOtherExpenses" Text='<%# Eval("OtherExpenses","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Gross Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblGrossSallary" Text='<%# Eval("GrossSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Deductions">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("Deductions","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Absent Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAbsentAmount" Text='<%# Eval("AbsentAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Tax","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="E.O.B.I">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEOBI" Text='<%# Eval("EOBI","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Provident Fund">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeePF" Text='<%# Eval("EmployeePF","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ClearanceDeduction">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("ClearanceDeductionAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("NetAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <table id="table" class="table table-bordered gv">
    <tr>
      
       <th><input type="checkbox" onclick="checkall()" id="checkall"/></th> <th>WDID</th><th>EmployeeName</th><th>Department</th><th>Separation date</th><th>PFContribution</th><th>Gratuity</th><th>ClearanceDeductionAmount</th><th>AdonsDeductions</th><th>AdditionalTaxperMonth</th><th>OtherDeductions</th><th>TotalDeduction</th><th>OtherDeductionsInput</th><th>Remarks</th><th>NetAmount</th><th>Action</th>
      
    </tr>
</table>
                   
                </section>

            </div>
            <!-- #dash-content -->
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
               <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>
			function checkall(obj) {
                if ($(obj).prop("checked") == true) {
                    $('input:checkbox').attr('checked', 'checked');

                }
                else {
					$('input:checkbox').attr('checked', 'checked');
                
				}
               

            } 

            function LockFnF() {
				$("input:checkbox").prop('checked', true);
				array = [];
				$("input:checkbox").each(function () {
					var $this = $(this);

					if ($this.is(":checked")) {
						array.push($this.attr("id"));
					} else {

					}
				});

				if (array.length == 0) {
					alert('Kindly check all Employees');
				}
                else {
				
					$.ajax({
						type: "POST",
						url: '/business/services/EssService.asmx/LockPaymentFnF',
						data: '{month: ' + JSON.stringify($('#txtPayrollDate').val()) + ', employeeIDs: ' + JSON.stringify(array) + '}',
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						success: function (response) {

							//GetFnFData();
							$("#btnLockPayment").css("display", "none");
							$("#btnRelease").css("display", "none");
							$("#table").empty();
                            alert('FnF Locked');
						


						},

						error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); alert(errorThrown) }
					});

				}
				console.log(array);
			}

            function ReleaseFnF()
            {   
                array = [];
				$("input:checkbox").each(function () {
					var $this = $(this);

					if ($this.is(":checked")) {
						array.push($this.attr("id"));
					} else {
						
					}
                });

                if (array.length == 0) {
					alert('Kindly select Employee');
                }
                else {
					$.ajax({
						type: "POST",
						url: '/business/services/EssService.asmx/ReleaseFnF',
						data: '{month: ' + JSON.stringify($('#txtPayrollDate').val()) + ', employeeIDs: ' + JSON.stringify(array) + '}',
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						success: function (response) {

							GetFnFData();
							alert('FnF Settled');


						},

						error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); alert(errorThrown) }
					});

				}
				console.log(array);
			}

            function InsertClearanceDeduction(separationID, employeeid)
            {
				var inputdeduction = $("#inputDeduction_" + employeeid+"").val();
				var remarksdeduction = $("#RemarksDeduction_" + employeeid + "").val();
				$.ajax({
					type: "POST",
					url: '/business/services/EssService.asmx/InsertClearanceDeduction',
					data: '{employeeid: ' + JSON.stringify(employeeid) + ',amount: ' + JSON.stringify(inputdeduction) + ',remarks: ' + JSON.stringify(remarksdeduction) + ',sepID: ' + JSON.stringify(separationID) + '}',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response) {

						console.log(response.d);
                        GetFnFData();
                        alert('Updated');
						
					},

					error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
				});
            }
            var array = [];
            function checkme(employeeid) {
                var obj = document.getElementById(employeeid);

				if ($(obj).prop("checked") == true) {
					//array.push(employeeid);
				}
				else if ($(obj).prop("checked") == false) {

                    for (var i = 0; i < array.length; i++)
                    {
                        if (employeeid == array[i]) {
							//array.splice(i, 1);
						}
					}
                }
              //  console.log(array);

            }
          
		
			function GetFnFData() {

				$.ajax({
					type: "POST",
					url: '/business/services/EssService.asmx/GetFnFDetails',
					data: '{month: ' + JSON.stringify($('#txtPayrollDate').val()) + '}',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
                    success: function (response) {
                        console.log(response);
                        console.log(response.d);
                        var data = response.d;
                        var len = data.length;
                        var txt = "";
                        if (len > 0) {
                            for (var i = 0; i < len; i++) {


                                txt += "<tr><td><input type=\"checkbox\" onclick=\"checkme(" + data[i].empid + ")\" id=\"" + data[i].empid + "\" /></td><td><button onclick=\"InsertClearanceDeduction(" + data[i].SeperationDetailsID + "," + data[i].empid + ")\" class=\"AD_btn\" id=\"" + data[i].SeperationDetailsID + "\"  type=\"button\">Update</button></td><td>" + data[i].wdid + "</td><td>" + data[i].employeename + "</td><td>" + data[i].deptName + "</td><td>" + data[i].EmployeeSeperationDate + "</td><td>" + data[i].HRStatus + "</td><td>" + data[i].FinanceStatus + "</td><td>" + data[i].ITStatus + "</td><td>" + data[i].ClearanceDeductionAmount + "</td><td>" + data[i].OtherDeduction + "</td><td><input id=\"inputDeduction_" + data[i].empid + "\" type=\"number\" /></td><td><input id=\"RemarksDeduction_" + data[i].empid + "\" type=\"text\" /></td><td>" + data[i].AdonsDeductions + "</td><td>" + data[i].TotalDeduction + "</td><td>" + data[i].NetAmount + "</td><td>" + data[i].TotalDays_ + "</td><td>" + data[i].GrossSalary + "</td><td>" + data[i].TotalOverTimeAmount + "</td><td>" + data[i].TotalIncrementedAmount + "</td><td>" + data[i].TotalDecrementedAmount + "</td><td>" + data[i].Commission_ + "</td><td>" + data[i].SalesComission_ + "</td><td>" + data[i].Arrears_ + "</td><td>" + data[i].Spiffs_ + "</td><td>" + data[i].Severance_ + "</td><td>" + data[i].Reimbursement_ + "</td><td>" + data[i].LeaveEncashment_ + "</td><td>" + data[i].PreviousDaysAmount_ + "</td><td>" + data[i].BonusTotal_ + "</td><td>" + data[i].ExpenseReimb_ + "</td><td>" + data[i].AddonOverTime_ + "</td><td>" + data[i].AttendanceAdjustment_ + "</td><td>" + data[i].PaidLeaves_ + "</td><td>" + data[i].TotalTaxableAddons_ + "</td><td>" + data[i].AbsentDaysDeduction_ + "</td><td>" + data[i].AddonEOBI_ + "</td><td>" + data[i].EOBI_ + "</td><td>" + data[i].MonthlyTax + "</td><td>" + data[i].PFContribution + "</td><td>" + data[i].Gratuity + "</td><td>" + data[i].AdditionalTaxperMonth + "</td></tr>";
                                console.log(txt);
                            }
                            if (txt != "") {
                                $("#btnLockPayment").css("display", "block");
								$("#btnRelease").css("display", "none");
                                $("#table").empty();
                                var header = "<tr><th><input type=\"checkbox\" onclick=\"checkall(this)\" /></th><th>Action</th><th> WDID</th><th>EmployeeName</th><th>Department</th><th>Separation date</th><th>HrClearance</th><th>FinanceClearance</th><th>I.TClearance</th><th>ClearanceDeductionAmount</th><th>OtherDeductions</th><th>OtherDeductionsInput</th><th>Remarks</th><th>AdonsDeductions</th><th>TotalDeduction</th><th>NetAmount</th><th>SalaryOfDays</th><th>GrossSalary</th><th>TotalOverTimeAmount</th><th>TotalIncrementedAmount</th><th>TotalDecrementedAmount</th><th>Commission</th><th>SalesComission</th><th>Arrears</th><th>Spiffs</th><th>Severance</th><th>Reimbursement</th><th>LeaveEncashment</th><th>PreviousDaysAmount</th><th>BonusTotal</th><th>ExpenseReimb</th><th>AddonOverTime</th><th>AddonAttendanceAdjustment</th><th>AddonPaidLeaves</th><th>TotalTaxableAddons</th><th>AbsentDaysDeduction</th><th>AddonEOBI</th><th>EOBI</th><th>MonthlyTax</th><th>PFContribution</th><th>Gratuity</th><th>AdditionalTaxperMonth</th></tr>";
                                $("#table").append(header);

                                $("#table").append(txt);
                            }
                        }
                        else {
                            $("#btnLockPayment").css("display", "none");
							$("#btnRelease").css("display", "none");
                            $("#table").empty();
                            alert('No Records Found');
						}
					},
					
					error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
				});
			}

		</script>
    </form>
</body>
</html>
