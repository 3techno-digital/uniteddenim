﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DebitNote.aspx.cs" Inherits="Technofinancials.Finance.view.DebitNote" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section>
            <div class="container-fluid">
                                                                                  <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <img src="/assets/images/Admin_Debit_Notes.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Debit Note</h3>
                        </div>
                        <div class="col-sm-4">
                            <div class="pull-right flex">
                              <%--  <button class="tf-approved-btn" "Approved"><i class="far fa-thumbs-up"></i></button>
                                <button class="tf-back-btn" "Back"><i class="fas fa-arrow-left"></i></button>
                                <button class="tf-disapproved-btn" "Dispproved"><i class="far fa-thumbs-down"></i></button>
                                 <button class="tf-excel-btn" "Excel"></button>

                                <button class="tf-save-btn" "Save"><i class="far fa-save"></i></button>
                                <button class="tf-send-btn" "Send"><i class="fas fa-paper-plane"></i></button>
                                <button class="tf-upload-btn" "Upload"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                <button class="tf-view-btn" "View"><i class="far fa-eye"></i></button>
                                <button class="tf-add-btn" "Add"><i class="far fa-plus-circle"></i></button>--%>
                                <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/debit-note/add-debit-note"); %>" class="tf-add-btn" "Add"><i class="far fa-plus-circle"></i></a>

                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                <div class="row">
<%--                    <div class="col-sm-12">
                   <div class="pull-left">
                       <a href="/Procurement Management/manage/DBNs.aspx" class="btn btn-primary btn-add-items green-btn">Add Debit Note</a>
                   </div>
                </div>--%>
                    <div class="col-sm-12 table-09-main-div">
                        <div class="add-table-div gv-overflow-scrool">
                            <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DBN Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblATRCode" Text='<%# Eval("DBNCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               
                                    <asp:TemplateField HeaderText="Generarte Date">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPRCOde" Text='<%# Eval("DBNDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               
                                    <asp:TemplateField HeaderText="MRN Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCompanyName" Text='<%# Eval("MRNCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                
                                    <asp:TemplateField HeaderText="PO Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSiteNzme" Text='<%# Eval("POCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               
                                    <asp:TemplateField HeaderText="Supplier Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblJobID" Text='<%# Eval("SupplierName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblJssobID" Text='<%# Eval("AmountOfInvoice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                              
                                            <asp:TemplateField HeaderText="Prepared Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblUserCommeasdfasfasnts" Text='<%# Eval("PreparedDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                           <%-- <asp:TemplateField HeaderText="Reviewed By">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblUserCommeasfasnts" Text='<%# Eval("ReviewedBy") + " - " + Eval("ReviewedDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Approved By">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblUserCommesssnts" Text='<%# Eval("ApprovedBy") + " - " + Eval("ApprovedDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>

                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblStauss" Text='<%# Eval("DocStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                  <%--  <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblComments" Text='<%# Eval("Comments") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                                <Columns>
                                     <asp:TemplateField HeaderText="Generate Report">
                                                <ItemTemplate>
                                                      <a id="lnkGenerateReport" runat="server" href='<%# "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/finance/manage/reports/generate-debit-notes-report-" + Eval("DBNID") %>' >Generate</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lnkCommand" CommandArgument='<%# Eval("DBNID") %>' OnClick="lnkCommand_Click"> View</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    <style>
        table#gv thead tr th {
    padding: 4px!important;
    font-size: 12px!important;
}
 table#gv tbody tr td {
    padding: 4px!important;
    font-size: 12px!important;
}

 .tf-add-btn {
  
    padding: 6px 11px !important;

}
    </style>
</body>
</html>
