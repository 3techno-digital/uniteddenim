﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace Technofinancials.Finance.view
{
    public partial class EmployeeFullAndFinalSettlement : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int NewPayrollID
        {
            get
            {
                if (ViewState["NewPayrollID"] != null)
                {
                    return (int)ViewState["NewPayrollID"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["NewPayrollID"] = value;
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false;
                    clearFields();
                    ViewState["dtEmployeeSalaries"] = null;

                    btnApproveByFinance.Visible =
                    lnkDelete.Visible =
                    btnSubForReview.Visible =
                    btnDisapprove.Visible = false;

                    if (HttpContext.Current.Items["PayrollMonth"] != null)
                    {
                        txtPayrollDate.Text = HttpContext.Current.Items["PayrollMonth"].ToString();
                        GetEmployeeFnFData();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        //private void CheckAccess()
        //{
        //    try
        //    {
        //        btnApproveByFinance.Visible =
        //        lnkDelete.Visible =
        //        btnSubForReview.Visible =
        //        btnDisapprove.Visible = false;

        //        objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
        //        objDB.TableName = "EmployeeSeperationDetails";
        //        objDB.PrimaryColumnnName = "SeperationDetailsID";
        //        objDB.PrimaryColumnValue = NewPayrollID.ToString();
        //        objDB.DocName = "Payroll";

        //        string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
        //        if (chkAccessLevel == "Can Edit")
        //        {
        //            lnkDelete.Visible =
        //            btnSubForReview.Visible = true;
        //        }
        //        if (chkAccessLevel == "Can Edit & Approve")
        //        {
        //            btnApproveByFinance.Visible =
        //            btnDisapprove.Visible = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }
        //}

        private DataTable dtEmployeeSalaries
        {
            get
            {
                if (ViewState["dtEmployeeSalaries"] != null)
                {
                    return (DataTable)ViewState["dtEmployeeSalaries"];
                }
                else
                {
                    return new DataTable();
                }
            }
            set
            {
                ViewState["dtEmployeeSalaries"] = value;
            }
        }

        private void clearFields()
        {
            txtPayrollDate.Text = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = string.Empty;
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                
                if (btn.ID.ToString() == "btnApproveByFinance" || btn.ID.ToString() == "btnRevApproveByFinance")
                {
                    DataTable dtEmployees = (DataTable)gvSalaries.DataSource;
                    List<string> lstEmployees = dtEmployees.AsEnumerable().Select(x => x[0].ToString()).ToList();
                    res = objDB.ApproveEmployeeClearance(lstEmployees);
                    GetEmployeeFnFData();
                }
                else if (btn.ID.ToString() == "btnDisapprove" || btn.ID.ToString() == "btnRejDisApprove")
                {
                    objDB.NewPayrollID = NewPayrollID;
                    //string ss = objDB.ResetAddOnsStatus();
                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Common.addlogNew(res, "Finance", "Payroll of ID\"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/employee-fnf-settlement", "", "Payroll of Date \"" + txtPayrollDate.Text + "\"", "EmployeeSeperationDetails", "FnF Settlement", Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString()));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
            //CheckAccess();
        }
        
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "EmployeeSeperationDetails", "SeperationDetailsID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "NewPayroll of ID \"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" deleted", "EmployeeSeperationDetails", NewPayrollID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {

                int index = e.RowIndex;

                //dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
                dtEmployeeSalaries.Rows[index].SetField("TotalDeduction", (Convert.ToDouble(((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("OtherDeductionsInput")).Text) + Convert.ToDouble(((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("TotalDeduction")).Text)).ToString());
                //dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
                //dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
                dtEmployeeSalaries.AcceptChanges();

                //dtEmployeeSalaries.Rows[index]["NetSalry"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
                //dtEmployeeSalaries.AcceptChanges();

                gvSalaries.EditIndex = -1;


                BindData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void RowUpdating(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = (sender as LinkButton).NamingContainer as GridViewRow;
                int index = row.RowIndex;

                //dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
                dtEmployeeSalaries.Rows[index].SetField("TotalDeduction", (Convert.ToDouble(((TextBox)gvSalaries.Rows[row.RowIndex].FindControl("OtherDeductionsInput")).Text) + Convert.ToDouble(((TextBox)gvSalaries.Rows[row.RowIndex].FindControl("TotalDeduction")).Text)).ToString());
                //dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
                //dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
                dtEmployeeSalaries.AcceptChanges();

                //dtEmployeeSalaries.Rows[index]["NetSalry"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
                //dtEmployeeSalaries.AcceptChanges();

                gvSalaries.EditIndex = -1;


                BindData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        //protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    try
        //    {
        //        int index = e.RowIndex;

        //        dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("AdditionalTax", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditTax")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
        //        dtEmployeeSalaries.AcceptChanges();

        //        dtEmployeeSalaries.Rows[index]["NetSalary"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
        //        dtEmployeeSalaries.AcceptChanges();

        //        gvSalaries.EditIndex = -1;
        //        BindData();
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }

        //}
        protected void gvSalaries_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSalaries.EditIndex = -1;
            BindData();
        }
        protected void gvSalaries_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSalaries.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void GetEmployeeFnFData()
        {
            objDB.PayrollDate = txtPayrollDate.Text;
            objDB.PayrollDate = "01-" + txtPayrollDate.Text;
            dtEmployeeSalaries = objDB.GetKTPayrollDetailsForLeftEmployeesByNewPayrollID(ref errorMsg);
            BindData();
        }
        protected void BindData()
        {
            gvSalaries.DataSource = dtEmployeeSalaries;
            gvSalaries.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    gvSalaries.UseAccessibleHeader = true;
                    gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            gvNormalSal.DataSource = dtEmployeeSalaries;
            gvNormalSal.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    gvNormalSal.UseAccessibleHeader = true;
                    gvNormalSal.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

		protected void btnView_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            GetEmployeeFnFData();
        }


		protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
		{
            int a = 0;

		}
        protected void lnkUpdate_Command(object sender, CommandEventArgs e)
        {

            ////dt.Columns.Add("SrNo");
            ////dt.Columns.Add("ExpType");
            ////dt.Columns.Add("Description");
            ////dt.Columns.Add("Amount");
            ////dt.Columns.Add("FilePath");
            //LinkButton lnk = (LinkButton)sender as LinkButton;
            //string delSr = lnk.CommandArgument.ToString();
            //for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            //{
            //    if (dtExpDetails.Rows[i][0].ToString() == delSr)
            //    {
            //        ddlExpType.SelectedValue = dtExpDetails.Rows[i]["ExpType"].ToString();
            //        txtDescriptionDetail.Value = dtExpDetails.Rows[i]["Description"].ToString();
            //        txtAmountDetail.Value = dtExpDetails.Rows[i]["Amount"].ToString();
            //        imgLogo.Src = dtExpDetails.Rows[i]["FilePath"].ToString();
            //        hdnExpenseSrNO.Value = delSr;
            //        btnUpdateDiv.Visible = true;
            //        btnAddDiv.Visible = false;
            //    }
            //}
            //for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            //{
            //    dtExpDetails.Rows[i].SetField(0, i + 1);
            //    dtExpDetails.AcceptChanges();
            //}

            //ExpDetailsSrNo = dtExpDetails.Rows.Count + 1;
            //BindExpDetailsTable();
        }

    }
}