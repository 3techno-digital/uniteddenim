﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttendanceUploader.aspx.cs" Inherits="Technofinancials.Finance.view.AttendanceUploader" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>

		<style>
			div#gv_wrapper {
				margin-bottom: 7%;
			}
		</style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">
			<div class="wrap">
				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
								<h1 class="m-0 text-dark">Attendance Files</h1>
							</div>
							  <div class="col-sm-4">
                                <div style="text-align: right;">
                                  <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/attendance-uploader/add-new-attendance-file"); %>" class="AD_btn">Add</a>
                                </div>
                            </div>
						</div>
						<!-- /.row -->
					</div>
					<!-- /.container-fluid -->
				</div>

				<section class="app-content">
					<div class="row ">
						<div class="col-md-4 col-lg-3 col-sm-6" style="float: right;">
							<asp:UpdatePanel ID="UpdatePanel4" runat="server">
								<ContentTemplate>
									<div class="form-group" id="divAlertMsg" runat="server">
										<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
											<span>
												<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
											</span>
											<p id="pAlertMsg" runat="server">
											</p>
										</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>
						<div class="tab-content ">
							<div class="tab-pane active">
								<div class="col-sm-12 adminpanel-padding-bottom">
									<asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
										<Columns>
											<asp:TemplateField HeaderText="Sr.No.">
												<ItemTemplate>
													<%#Container.DataItemIndex+1 %>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Sheet No.">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblName" Text='<%# Eval("MasterID") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Sheet Name">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblName" Text='<%# Eval("SheetName") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Attendance Month">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblName" Text='<%# Eval("AttendanceMonth") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Successful Update">
												<ItemTemplate>
													<asp:CheckBox runat="server" AutoPostBack="false" Enabled='<%# Eval("isDataUpdated") %>' ID="chkIsSuccessful" Checked='<%# Eval("isDataUpdated") %>'></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateField>
												<asp:TemplateField HeaderText="Uploaded By ">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblName" Text='<%# Eval("CreatedBy") %>'></asp:Label></ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Uploaded On ">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblDate" Text='<%# Convert.ToDateTime(Eval("CreatedDate")).ToString("dd-MM-yyyy HH:mm tt") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="View">
												<ItemTemplate>
													<a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/finance/manage/attendance-uploader/edit-attendance-file-" + Eval("MasterID") %>" class="edit-class">View</i></a>
												<%--	<asp:LinkButton ID="lnkDelete" runat="server" CssClass="delete-class" OnClick="lnkDelete_Click" CommandArgument='<%# Eval("MasterID") %>'>Delete</asp:LinkButton>--%>
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- #dash-content -->
			</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>

	</form>
</body>
</html>