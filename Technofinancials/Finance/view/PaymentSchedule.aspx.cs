﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net;

namespace Technofinancials.Finance.view
{
    public partial class PaymentSchedule : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected string EmployeeName
        {
            get
            {
                if (ViewState["EmployeeName"] != null)
                {
                    return (string)ViewState["EmployeeName"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["EmployeeName"] = value;
            }
        }
        protected int loanID
        {
            get
            {
                if (ViewState["loanID"] != null)
                {
                    return (int)ViewState["loanID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["loanID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtPayrollDate.Visible = false;
                //CheckSessions();
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false;
                    clearFields();
                    ViewState["dtEmployeeSalaries"] = null;
                 
                    slip.Visible = true;
                    txtPayrollDate.Visible = false;
                    hdnCompanyName.InnerText = Session["CompanyName"].ToString();
                    hdnCompanyNamefooter.Text = Session["CompanyName"].ToString();
                    hdnCompanyNamecopyrights.Text = Session["CompanyName"].ToString();
                    hdnCompanyLogo.Src = Session["CompanyLogo"].ToString();

                    if (HttpContext.Current.Items["LoanID"] != null)
                    {
                        loanID = Convert.ToInt32(HttpContext.Current.Items["LoanID"].ToString());
                        
                        GetPaymentScheduleReport(loanID);

                    }

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string content = "";
                string header = "";
                string footer = "";

                var sb = new StringBuilder();
                slip.RenderControl(new HtmlTextWriter(new StringWriter(sb)));

                string s = sb.ToString();

                // fix CSS for FNF SLIP
                string css = "<div> <style> .B_class { padding: 20px; border: 1px solid #000; background: #fff; } .B_class h1, .B_class h2, .B_class h3, .B_class h4, .B_class p, .B_class td { margin: 0; font-family: lato; color: #000; } .B_class table { border: 1px solid; margin-top: -25px; } .B_class table td, .B_class table th { padding: 3.4px; } .B_class .slip-table thead tr th { background-color: #ffffff; border-bottom: 1px solid #000000 !important; border-top: 1px solid #000000 !important; color: #000; padding: 0 10px; } .slip-table-bordered thead tr th, .slip-table-bordered thead tr td, .slip-table-bordered tbody tr th, .slip-table-bordered tbody tr td { border-top: 1px solid transparent !important; border: 0; vertical-align: middle; padding: 0.1rem; font-size: 14px; } .slip-table thead tr th, .slip-table thead tr td, .slip-table tbody tr th, .slip-table tbody tr td, .slip-table tfoot tr th, .slip-table tfoot tr td { height: 32px; line-height: 1.428571429; vertical-align: middle; border-top: 1px solid #ddd; padding: 0 10px; } .slip-table { width: 100%; max-width: 100%; background-color: transparent; border-collapse: collapse; border-spacing: 0; } .slip-table-bordered tfoot tr th, .slip-table-bordered tfoot tr td { border-top: 1px solid #000 !important; border: 0; vertical-align: middle; padding: 0.1rem; font-size: 14px; padding: 0 10px; } .B_class .slip-table thead tr th { text-align: right; } .B_class .slip-table thead tr th:nth-child(1) { text-align: left; } .B_class .slip-table tbody tr td { text-align: right; height: 28px; } .B_class .slip-table tbody tr td:nth-child(1) { text-align: left; } .B_class .slip-table tfoot tr td { text-align: right; height: 34px; } .B_class .slip-table tfoot tr td:nth-child(1) { text-align: left; width: 200px !important; } .B_class hr { margin-top: 20px; margin-bottom: 10px; border: 0; border-top: 1px solid #000000; } .tableP { height: 34px; padding: 6.4px; border: 1px solid; margin-top: -6px !important; width: 99.9%; } .tableP span { margin-left: 40px; text-transform: capitalize; } .AddressSide { height: 53px; color: #000; text-align: center; } .AddressSide h1 { font-size: 18px; text-transform: capitalize; font-weight: 700; margin: 5px 0; color: #000; } .AddressSide h2 { font-size: 18px; text-align: right; font-weight: 400; color: #000; margin: 5px 0; } .AddressSide h3 { font-size: 18px; text-transform: capitalize; font-weight: 700; margin: 5px 0; color: #000; } .AddressSide h4 { font-size: 18px; text-transform: capitalize; font-weight: 700; margin-bottom: 15px; color: #000; margin: 5px 0; } .AddressSide p { font-size: 16px; font-weight: 500; max-width: 350px; text-transform: capitalize; color: #000; } .bigBox { width: 100%; border: 1px solid #000; height: 400px; margin: 50px 0 20px; } .Box01 { width: 45.5%; display: inline-block; padding: 10px; color: #000; } .Box02 { width: 46%; display: inline-block; padding: 10px 20px; margin-left: -5px; color: #000; } .Box03 { width: 50%; display: inline-block; } .Box04 { width: 50%; margin-left: -5px; display: inline-block; } .Box05 { width: 100%; display: block; } .OneLine { width: 100%; height: 28px; } .OneLine h3 { display: inline-block; font-size: 16px; font-weight: 700; width: 35%; margin: 3px 0; color: #000; } .OneLine p { display: inline-block; font-size: 16px; width: 60%; margin: 3px 0; color: #000; } .totalSalaries { font-weight: bold !important; color: #188ae2 !important; font-size: 16px !important; } .line-four { width: 99.9%; display: block; } #tblPDF td { border: 1px solid; border-right: none; border-top: none; } </style>";
                content = s;
                content = css + s + "</div>";

                Common.generatePDF(header, footer, content, "FundReport_" + EmployeeName + " ", "A4", "Portrait", 30, 30);
            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }
        }
        private void GetPaymentScheduleReport(int LoanID)
        {
            try
            {
                DataSet ds = new DataSet();

                objDB.LoanID = LoanID;
                ds = objDB.GetPaymentScheduleReport(ref errorMsg);

                DataTable dt = ds.Tables[0];
                DataTable schedule = ds.Tables[3];
            


                if (ds != null)
                {
                    employeecode.Text = dt.Rows[0]["wdid"].ToString();
                    Employeename.Text = dt.Rows[0]["employeename"].ToString();
                    EmployeeName = dt.Rows[0]["employeename"].ToString();
                    location.Text= dt.Rows[0]["placeofpost"].ToString();
                    Loanid.Text = dt.Rows[0]["loanid"].ToString();
                    loantype.Text = dt.Rows[0]["title"].ToString();
                    loanamount.Text= Convert.ToDecimal(dt.Rows[0]["amount"].ToString()).ToString("N2");
                    noi.Text= dt.Rows[0]["noofinstallment"].ToString();
                    deductpermonth.Text= Convert.ToDecimal(dt.Rows[0]["salarydedper"].ToString()).ToString("N2");
                    receiveins.Text= ds.Tables[2].Rows[0]["receiveins"].ToString();
                    totaldues.Text= Convert.ToDecimal(ds.Tables[1].Rows[0]["totaldues"].ToString()).ToString("N2");
                    totalpaid.Text= Convert.ToDecimal(ds.Tables[2].Rows[0]["totalpaid"].ToString()).ToString("N2");
                    effectivefrom.Text=Convert.ToDateTime(dt.Rows[0]["effectivefrom"].ToString()).ToString("dd-MMM-yyyy");

                    payschedule.DataSource = schedule;
                    payschedule.DataBind();


                 

                }
             //   Common.addlog("View", "Finance", "Loan \"" + txtLoanTitle.Value + "\" Viewed", "Loan", loanID);


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " Million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

    
        private MemoryStream PDFGenerate(string message, string ImagePath)
        {
            MemoryStream output = new MemoryStream();
            Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, output);
            pdfDoc.Open();
            Paragraph Text = new Paragraph(message);
            pdfDoc.Add(Text);
            byte[] file;
            file = System.IO.File.ReadAllBytes(ImagePath);
            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(file);
            jpg.ScaleToFit(550F, 200F);
            pdfDoc.Add(jpg);
            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            output.Position = 0;
            return output;
        }
		//protected void btnEmail_ServerClick(object sender, EventArgs e)
		//{
		//    try
		//    {
		//         string smtpAddress = "smtp.gmail.com";
		//         int portNumber = 587;
		//         bool enableSSL = true;
		//         string emailFromAddress = "techsupport@3techno.com"; //Sender Email Address  
		//         string password = "Create*0519!"; //Sender Password  
		//         string emailToAddress = "athar@3techno.digital"; //Receiver Email Address  
		//         string subject = "Hello";
		//         string body = "Hello, This is Email sending test using gmail.";


		//        using (MailMessage mail = new MailMessage())
		//        {
		//            mail.From = new MailAddress(emailFromAddress);
		//            mail.To.Add(emailToAddress);
		//            mail.Subject = subject;
		//            mail.Body = body;
		//            mail.IsBodyHtml = true;

		//            MemoryStream file = new MemoryStream(PDFGenerate("This is fnf file", Server.MapPath("Images/photo.jpg")).ToArray());

		//            file.Seek(0, SeekOrigin.Begin);
		//            Attachment data = new Attachment(file, "RunTime_Attachment.pdf", "application/pdf");
		//            ContentDisposition disposition = data.ContentDisposition;
		//            disposition.CreationDate = System.DateTime.Now;
		//            disposition.ModificationDate = System.DateTime.Now;
		//            disposition.DispositionType = DispositionTypeNames.Attachment;
		//            mail.Attachments.Add(data);//Attach the file  

		//            //mail.Attachments.Add(new Attachment("D:\\TestFile.txt"));//--Uncomment this to send any attachment  
		//            using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
		//            {
		//                smtp.Credentials = new NetworkCredential(emailFromAddress, password);
		//                smtp.EnableSsl = enableSSL;
		//                smtp.Send(mail);
		//            }
		//        }
		//    }











		//        //MailMessage message = new MailMessage();
		//        //message.To.Add("athar@3techno.digital");// Email-ID of Receiver  
		//        //message.Subject ="FnF";// Subject of Email  
		//        //message.From = new System.Net.Mail.MailAddress("techsupport@3techno.com");// Email-ID of Sender  
		//        //message.IsBodyHtml = true;

		//        //MemoryStream file = new MemoryStream(PDFGenerate("This is pdf file text", Server.MapPath("Images/photo.jpg")).ToArray());

		//        //file.Seek(0, SeekOrigin.Begin);
		//        //Attachment data = new Attachment(file, "RunTime_Attachment.pdf", "application/pdf");
		//        //ContentDisposition disposition = data.ContentDisposition;
		//        //disposition.CreationDate = System.DateTime.Now;
		//        //disposition.ModificationDate = System.DateTime.Now;
		//        //disposition.DispositionType = DispositionTypeNames.Attachment;
		//        //message.Attachments.Add(data);//Attach the file  

		//        //message.Body = "Settlement";
		//        //SmtpClient SmtpMail = new SmtpClient();
		//        //SmtpMail.Host = "smtp.gmail.com";//name or IP-Address of Host used for SMTP transactions  
		//        //SmtpMail.Port = 587;//Port for sending the mail  
		//        //SmtpMail.Credentials = new System.Net.NetworkCredential("", "");//username/password of network, if apply  
		//        //SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
		//        //SmtpMail.EnableSsl = false;
		//        //SmtpMail.ServicePoint.MaxIdleTime = 0;
		//        //SmtpMail.ServicePoint.SetTcpKeepAlive(true, 2000, 2000);
		//        //message.BodyEncoding = Encoding.Default;
		//        //message.Priority = MailPriority.High;
		//        //SmtpMail.Send(message); //Smtpclient to send the mail message  
		//        //Response.Write("Email has been sent");

		//    catch (Exception ex)
		//    { Response.Write("Failed"); }
		//}

		



        //private void CheckAccess()
        //{
        //    try
        //    {
        //        btnApproveByFinance.Visible =
        //        lnkDelete.Visible =
        //        btnSubForReview.Visible =
        //        btnDisapprove.Visible = false;

        //        objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
        //        objDB.TableName = "EmployeeSeperationDetails";
        //        objDB.PrimaryColumnnName = "SeperationDetailsID";
        //        objDB.PrimaryColumnValue = NewPayrollID.ToString();
        //        objDB.DocName = "Payroll";

        //        string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
        //        if (chkAccessLevel == "Can Edit")
        //        {
        //            lnkDelete.Visible =
        //            btnSubForReview.Visible = true;
        //        }
        //        if (chkAccessLevel == "Can Edit & Approve")
        //        {
        //            btnApproveByFinance.Visible =
        //            btnDisapprove.Visible = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }
        //}

        private DataTable dtEmployeeSalaries
        {
            get
            {
                if (ViewState["dtEmployeeSalaries"] != null)
                {
                    return (DataTable)ViewState["dtEmployeeSalaries"];
                }
                else
                {
                    return new DataTable();
                }
            }
            set
            {
                ViewState["dtEmployeeSalaries"] = value;
            }
        }

        private void clearFields()
        {
            txtPayrollDate.Text = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

   
     
 
        //protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    try
        //    {
        //        int index = e.RowIndex;

        //        dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("AdditionalTax", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditTax")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
        //        dtEmployeeSalaries.AcceptChanges();

        //        dtEmployeeSalaries.Rows[index]["NetSalary"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
        //        dtEmployeeSalaries.AcceptChanges();

        //        gvSalaries.EditIndex = -1;
        //        BindData();
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }

        //}
      

    


	
        protected void lnkUpdate_Command(object sender, CommandEventArgs e)
        {

            ////dt.Columns.Add("SrNo");
            ////dt.Columns.Add("ExpType");
            ////dt.Columns.Add("Description");
            ////dt.Columns.Add("Amount");
            ////dt.Columns.Add("FilePath");
            //LinkButton lnk = (LinkButton)sender as LinkButton;
            //string delSr = lnk.CommandArgument.ToString();
            //for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            //{
            //    if (dtExpDetails.Rows[i][0].ToString() == delSr)
            //    {
            //        ddlExpType.SelectedValue = dtExpDetails.Rows[i]["ExpType"].ToString();
            //        txtDescriptionDetail.Value = dtExpDetails.Rows[i]["Description"].ToString();
            //        txtAmountDetail.Value = dtExpDetails.Rows[i]["Amount"].ToString();
            //        imgLogo.Src = dtExpDetails.Rows[i]["FilePath"].ToString();
            //        hdnExpenseSrNO.Value = delSr;
            //        btnUpdateDiv.Visible = true;
            //        btnAddDiv.Visible = false;
            //    }
            //}
            //for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            //{
            //    dtExpDetails.Rows[i].SetField(0, i + 1);
            //    dtExpDetails.AcceptChanges();
            //}

            //ExpDetailsSrNo = dtExpDetails.Rows.Count + 1;
            //BindExpDetailsTable();
        }
        

    }
}