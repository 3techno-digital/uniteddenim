﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net;

namespace Technofinancials.Finance.view
{
    public partial class FnFReport : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int NewPayrollID
        {
            get
            {
                if (ViewState["NewPayrollID"] != null)
                {
                    return (int)ViewState["NewPayrollID"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["NewPayrollID"] = value;
            }
        }
        protected string EmployeeName
        {
            get
            {
                if (ViewState["EmployeeName"] != null)
                {
                    return (string)ViewState["EmployeeName"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["EmployeeName"] = value;
            }
        }
        protected string PersonelEmail
        {
            get
            {
                if (ViewState["PersonelEmail"] != null)
                {
                    return (string)ViewState["PersonelEmail"];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["PersonelEmail"] = value;
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }
        //protected void btnPDF_ServerClick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string content = "";
        //        string header = "";
        //        string footer = "";
        //        content = "<div><div id='ReportPayroll' class='B_class'><img src='/assets/images/abs-labs-pvt-limited_32002.png' alt='Company Logo' style='width: 200px; position: absolute;'><div class='AddressSide'><h1>ABS - LABS (PVT.) LIMITED</h1><h3>Payslip</h3><h4>For the Month of <span id='Month'>Jul-2021</span></h4></div><div style='width: 100%; display: block;'><div class='row'><div class='Box01 col-md-6 col-lg-6 col-sm-6'><div class='OneLine'><h3>Employee Code :</h3><span id='empCode'>100694</span></div><div class='OneLine'><h3>Location :</h3><span id='empLocation'>Islamabad</span></div><div class='OneLine'><h3>Designation :</h3><span id='empDesg'>Senior Accountant, Corporate Accounting</span></div><div class='OneLine'><h3>Date of Birth :</h3><span id='DOB'>13-10-1988</span></div><div class='OneLine'><h3>Account :</h3><span id='empAccount'></span></div><div class='OneLine'><h3>Total Days :</h3><span id='lblTotalDays'>31</span></div><div class='OneLine'><h3>Gross Salary :</h3><span id='BasicSalary'>152,308.00</span></div></div><div class='Box02 col-md-6 col-lg-6 col-sm-6'><div class='OneLine'><h3>Name :</h3><span id='empname'>Raphael Ostin</span></div><div class='OneLine'><h3>Department :</h3><span id='empDpt'>Finance</span></div><div class='OneLine'><h3>Employee Type :</h3><span id='empType'>Regular</span></div><div class='OneLine'><h3>Date of Joining :</h3><span id='DateofJoin'>06-04-2018</span></div><div class='OneLine'><h3>CNIC :</h3><span id='CNIC'>1453223561785</span></div><div class='OneLine'><h3>Bank :</h3><span id='BankName'></span></div><div class='OneLine'><h3>Present Days :</h3><span id='lblPresentDays'>31</span></div></div></div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='row'><div class='Box03 col-md-6 col-lg-6 col-sm-6'><div class='table-1'><table><tbody><tr style='border-bottom: 2px solid #000;'><th>Allowance</th><th>Amount</th><th>YTD</th></tr><tr><td>Basic Salary</td><td><span id='basicSalry'>101,543.97</span></td><td><span id='basicSalryYTD'>101,543.97</span></td></tr><tr><td>House Rent</td><td><span id='houseRent'>3,554.04</span></td><td><span id='houseRentYTD'>3,554.04</span></td></tr><tr><td>Medical Allowance</td><td><span id='mdecialAllow'>10,154.40</span></td><td><span id='mdecialAllowYTD'>10,154.40</span></td></tr><tr><td>Utility Allowance</td><td><span id='UtilityAllow'>37,055.93</span></td><td><span id='UtilityAllowYTD'>37,055.93</span></td></tr><tr><th><b>Total</b></th><th><span id='TotalGross' class='payslip-totalcol-bar'>152,308.33</span></th><th><span id='TotalGrossYTD' class='payslip-totalcol-bar'>152,308.34</span></th></tr><tr><td></td><td></td><td></td></tr><tr><td>Sales Commission</td><td><span id='SalesComission'>-</span></td><td><span id='SalesComissionYTD'>-</span></td></tr><tr><td>Other Allowance</td><td><span id='OtherAllowance'>-0.33</span></td><td><span id='OtherAllowanceYTD'>-0.33</span></td></tr><tr><td>Expense Reimbursement</td><td><span id='ExpenseReim'>4,800.00</span></td><td><span id='ExpenseReimYTD'>4,800.00</span></td></tr><tr><td>Eid Bouns</td><td><span id='EidBonus'>-</span></td><td><span id='EidBonusYTD'>-</span></td></tr><tr><td>Spiffs</td><td><span id='Spiffs'>-</span></td><td><span id='SpiffsYTD'>-</span></td></tr><tr><td>Overtime Hours - General</td><td><span id='Overtime'>21,290.41</span></td><td><span id='OvertimeYTD'>21,290.41</span></td></tr><tr><td>Overtime Holidays</td><td><span id='OvertimeHolidays'>49,131.59</span></td><td><span id='OvertimeHolidaysYTD'>49,131.59</span></td></tr></tbody></table></div></div><div class='Box04 col-md-6 col-lg-6 col-sm-6'><div class='table-1'><table><tbody><tr style='border-bottom: 2px solid #000;'><th>Deduction</th><th>Amount</th><th>YTD</th></tr><tr><td>Income Tax</td><td><span id='IncomeTax'>6,715.00</span></td><td><span id='IncomeTaxYTD'>6,715.00</span></td></tr><tr><td>Addition Income Tax</td><td><span id='AddIncomeTax'>7,042.00</span></td><td><span id='AddIncomeTaxYTD'>7,042.00</span></td></tr><tr><td>EOBI Deduction</td><td><span id='EOBI'>175.00</span></td><td><span id='EOBIYTD'>175.00</span></td></tr><tr><td>Provident Fund</td><td><span id='PFtxt'>10,154.00</span></td><td><span id='PFYTD'>10,154.00</span></td></tr><tr><td>Other Deductions</td><td><span id='OtherDeductiontxt'>-</span></td><td><span id='OtherDeductiontxtYTD'>-</span></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr></tr></tbody></table></div></div></div><div class='row'><div class='Box05 col-md-6 col-lg-6 col-sm-6'><div class='table-1'><table><tbody><tr><td style='width: 58%;'>Total</td><td style='text-align: left;'><span id='Total1'>227,530.00</span></td><td><span id='Total2'>227,530.01</span></td></tr></tbody></table></div></div><div class='Box06 col-md-6 col-lg-6 col-sm-6' style='margin-top: 23.5px;'><div class='table-1'><table><tbody><tr><td style='width: 66%;'>Total</td><td><span id='Total3'>24,086.00</span></td><td><span id='Total4'>24,086.00</span></td></tr></tbody></table></div></div></div><div class='row'><div class='Box09 col-md-6 col-lg-6 col-sm-6 main'><div class='table-1'><table><tbody><tr><td style='font-weight: bold;'>Net Amount:</td></tr></tbody></table></div></div><div class='Box06 col-md-6 col-lg-6 col-sm-6 extra'><div class='table-1'><table><tbody><tr><td></td><td></td><td></td><td style='font-weight: bold; text-align: right;'><span id='NetAmnt'>203,444.00</span></td></tr></tbody></table></div></div></div><div class='row'><div class='Box07 col-md-12 col-lg-12 col-sm-12'><div class='table-1'><table><tbody><tr><td style='font-weight: bold; width: 25%;'>Amount of Words</td><td><span id='AmountWrds'>Two Hundred and Three Thousand Four Hundred and Forty-Four only-</span></td></tr></tbody></table></div></div></div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='row'><div class='Box08 col-md-12 col-lg-12 col-sm-12'><div class='table-1'><table style='background-color: #c0c0c0;'><tbody><tr><td style='font-weight: bold;'>Income Tax Details:</td></tr></tbody></table><table style='margin-top: 0px; border-top: none;'><tbody><tr><td style='font-weight: bold;'>Annual Taxable Income</td><td></td><td><span id='AnnualTaxInc'>1,791,223.31</span></td></tr><tr><td style='font-weight: bold;'>Total Income Tax Liability</td><td></td><td><span id='TotalIncomeTaxLia'>89,122.00</span></td></tr><tr><td style='font-weight: bold;'>Income Tax Paid</td><td></td><td><span id='IncomeTaxPaid'>13,757.00</span></td></tr><tr><td style='font-weight: bold;'>Advance Tax Adjustment</td><td></td><td>-</td></tr><tr><td style='font-weight: bold;'>Tax Credit </td><td></td><td>-</td></tr><tr><td style='font-weight: bold;'>Remaining Income Tax Payable</td><td></td><td><span id='RemIncomeTaxPayable'>75,365.00</span></td></tr></tbody></table></div></div></div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='row'><div class='Box11 col-md-12 col-lg-12 col-sm-12'><div class='table-1'><table style='background-color: #c0c0c0;'><tbody><tr><td style='font-weight: bold;'>Provident Fund Deatils:</td></tr></tbody></table><table style='margin-top: 0px; border-top: none;'><tfoot><tr><td style='font-weight: bold; border: 2px solid; border-left: none; border-top: none;'></td><td style='text-align: center; font-weight: bold; border: 2px solid; border-top: none;'>Opening</td><td style='text-align: center; font-weight: bold; border: 2px solid; border-top: none;'>Current Month</td><td style='text-align: center; font-weight: bold; border-right: none; border: 2px solid; border-top: none;'>Closing</td></tr><tr><td style='font-weight: bold; border: 2px solid; border-left: none; border-top: none;'>Employee Contribution</td><td style='border: 2px solid; border-left: none; border-top: none;'><span id='EmployeePF'>0</span></td><td style='border: 2px solid; border-left: none; border-top: none;'><span id='Label47'>10154</span></td><td style='border: 2px solid; border-left: none; border-top: none; text-align: right;'><span id='Label48'>10154</span></td></tr><tr><td style='font-weight: bold; border: 2px solid; border-left: none; border-top: none;'>Employer Contribution</td><td style='border: 2px solid; border-left: none; border-top: none;'><span id='EmployeerPF'>0</span></td><td style='border: 2px solid; border-left: none; border-top: none;'><span id='Label49'>10154</span></td><td style='border: 2px solid; border-left: none; border-top: none; text-align: right;'><span id='Label50'>10154</span></td></tr><tr><td style='font-weight: bold; border: 2px solid; border-left: none; border-top: none;'>Total</td><td style='border: 2px solid; border-left: none; border-top: none;'><span id='TotalPF'>0</span></td><td style='border: 2px solid; border-left: none; border-top: none;'><span id='Label51'>20308</span></td><td style='border: 2px solid; border-left: none; border-top: none; text-align: right;'><span id='Label52'>20308</span></td></tr></tfoot></table></div></div></div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div class='clearfix'>&nbsp;</div><div><p style='text-align: center;'>Techno Financials - This is auto generated pay slip and needs no signature.</p></div></div></div><style type='text/css'></style><div id='ReportPayrollFotter' class='table-1 B_class'><table><tbody><tr><td>KeepTruckin - Human Capital Management</td><td>© Keep Truckin</td><td><span id='Datetime'>13/Oct/2021 01:15:10</span></td><td style='display: none;'>USER:<span id='username'>Raphael Ostin</span></td></tr></tbody></table></div></div>";

        //        Common.generatePDF(header, footer, content, "Pay Slip-" + Session["UserName"].ToString() + " (" + txtPayrollDate.Text + ")", "A4", "Portrait", 30, 30);

        //    }
        //    catch (Exception ex)
        //    {
        //        //divAlertMsg.Visible = true;
        //        //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        //pAlertMsg.InnerHtml = ex.Message;
        //    }
        //}
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtPayrollDate.Visible = false;
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false;
                    clearFields();
                    ViewState["dtEmployeeSalaries"] = null;
                    sendemail.Visible = false;
                    btnApproveByFinance.Visible =
                    lnkDelete.Visible =
                    btnSubForReview.Visible =
                    btnDisapprove.Visible = false;
                    slip.Visible = false;
                    txtPayrollDate.Visible = false;
                    hdnCompanyName.InnerText = Session["CompanyName"].ToString();
                    hdnCompanyNamefooter.Text = Session["CompanyName"].ToString();
                    hdnCompanyNamecopyrights.Text = Session["CompanyName"].ToString();
                    hdnCompanyLogo.Src = Session["CompanyLogo"].ToString();
                    if (HttpContext.Current.Items["EmpID"] != null)
                    {
                        //  txtPayrollDate.Text = HttpContext.Current.Items["EmpID"].ToString();
                        string emp = HttpContext.Current.Items["EmpID"].ToString();

                        objDB.EmployeeID = Convert.ToInt32(emp);
                        DataTable dt = objDB.GetEmployeePersonalDetails(ref errorMsg);
                        EmployeeName = dt.Rows[0]["EmployeeName"].ToString();
                        PersonelEmail = dt.Rows[0]["SecEmail"].ToString();
						if (PersonelEmail != "")
						{
                            sendemail.Visible = true;
                            sendemail.HRef= "https://mail.google.com/mail/?view=cm&fs=1&to="+ PersonelEmail +"&su=FnFSlip&body=PFA";

                        }

                        GetSlip(HttpContext.Current.Items["EmpID"].ToString());


                    }

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " Million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

        private void GetSlip(string employeeid)
        {
            try
            {
                decimal totalMedicalYTD = 0;
                decimal totalYTDminus = 0;
                decimal sumTaxableAmount2 = 0;
                decimal AnnualTaxableIncome2 = 0;
                decimal minusValue = 0;
                decimal CalSum = 0;
                decimal sumYTDTAX = 0;
                decimal sumYTDAddTax = 0;

                //objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);// Convert.ToInt32(Session["EmployeeID"]);
                objDB.PayrollDate = txtPayrollDate.Text;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.SelectedEmp = employeeid.ToString();
                DataTable dt = new DataTable();
                dt = objDB.GetFnFSlipEmployeeByID(ref errorMsg);

                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        slip.Visible = true;
                        // PrintBtn.Visible = true;
                        // Month.Text = txtPayrollDate.Text;
                        employeecode.Text = dt.Rows[0]["wdid"].ToString();
                        location.Text = dt.Rows[0]["location"].ToString();
                        designation.Text = dt.Rows[0]["designation"].ToString();
                        Section.Text = dt.Rows[0]["Section"].ToString();
                        DateofJoining.Text = dt.Rows[0]["DateOfJoining"].ToString() == "" ? "" : Convert.ToDateTime(dt.Rows[0]["DateOfJoining"]).ToString("dd-MM-yyyy");
                        Address.Text = dt.Rows[0]["Address"].ToString();
                        Bank.Text = dt.Rows[0]["Bank"].ToString();
                        branch.Text = dt.Rows[0]["branch"].ToString();
                        NTN.Text = dt.Rows[0]["NTN"].ToString();
                        Leave.Text = dt.Rows[0]["Leave"].ToString();
                        Resign.Text = dt.Rows[0]["Resign"].ToString();
                        NoticeDays.Text = dt.Rows[0]["NoticeDays"].ToString();
                        Employeename.Text = dt.Rows[0]["Employeename"].ToString();
                        department.Text = dt.Rows[0]["department"].ToString();
                        EmploymentType.Text = dt.Rows[0]["EmploymentType"].ToString();
                        Grade.Text = dt.Rows[0]["Grade"].ToString();
                        //DateofConfirmation.Text = dt.Rows[0]["DateofConfirmation"].ToString() =="" ? "" : Convert.ToDateTime(dt.Rows[0]["DateofConfirmation"]).ToString("dd-MM-yyyy");
                        DateofConfirmation.Text = "";
                        DOB.Text = Convert.ToDateTime(dt.Rows[0]["DOB"]).ToString("dd-MM-yyyy") == "" ? "" : Convert.ToDateTime(dt.Rows[0]["DOB"]).ToString("dd-MM-yyyy");
                        AccountNo.Text = dt.Rows[0]["AccountNo"].ToString();
                        //AccountNo.Text = dt.Rows[0]["department"].ToString();
                        //DateofConf.Text = dt.Rows[0]["DateOfJoining"].ToString();
                        CNIC.Text = dt.Rows[0]["CNIC"].ToString();

                        Shift.Text = dt.Rows[0]["Shift"].ToString();
                        StandardGross.Text = dt.Rows[0]["StandardGross"].ToString();
                        lastworkingday.Text = dt.Rows[0]["lastworkingday"].ToString();

                        Tenure.Text = dt.Rows[0]["Tenure"].ToString();

                        BasicSalary.Text = dt.Rows[0]["BasicSalary"].ToString();
                        MedicalAllowance.Text = dt.Rows[0]["MedicalAllowance"].ToString();
                        UtilityAllowance.Text = dt.Rows[0]["UtilityAllowance"].ToString();
                        HouseRent.Text = dt.Rows[0]["HouseRent"].ToString();
                        Severance.Text = dt.Rows[0]["Severance"].ToString();
                        Arrears.Text = dt.Rows[0]["Arrears"].ToString();
                        Paidleaves.Text = dt.Rows[0]["Paidleaves"].ToString();
                        commission.Text = dt.Rows[0]["commission"].ToString();
                        EidBonus.Text = dt.Rows[0]["EidBonus"].ToString();
                        otherbonuses.Text = dt.Rows[0]["otherbonuses"].ToString();
                        leaveencashment.Text = dt.Rows[0]["leaveencashment"].ToString();
                        otherexpense.Text = dt.Rows[0]["otherexpense"].ToString();
                        spiffs.Text = dt.Rows[0]["spiffs"].ToString();
                        Overtime.Text = dt.Rows[0]["Overtime"].ToString();
                        gratuity.Text = dt.Rows[0]["gratuity"].ToString();
                        Contribution.Text = dt.Rows[0]["PFContribution"].ToString();
                        taxcredit.Text = dt.Rows[0]["taxcredit"].ToString();
                        //   employeePF.Text = dt.Rows[0]["employeePF"].ToString();
                        // CompanyPF.Text = dt.Rows[0]["CompanyPF"].ToString();

                        double SumofBenefits =
                             Convert.ToDouble(dt.Rows[0]["BasicSalary"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["MedicalAllowance"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["UtilityAllowance"].ToString()) +
                             
                             Convert.ToDouble(dt.Rows[0]["HouseRent"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["commission"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["EidBonus"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["otherbonuses"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["leaveencashment"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["otherexpense"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["spiffs"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["Severance"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["Arrears"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["Paidleaves"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["Overtime"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["gratuity"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["PFContribution"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["taxcredit"].ToString());

                        SumofBenefitsnAllowances.Text = SumofBenefits.ToString();
                        AbsentAmount.Text = dt.Rows[0]["AbsentDaysAmount"].ToString();
                        IncomeTax.Text = dt.Rows[0]["IncomeTax"].ToString();
                        AdditionalTaxpermonth.Text = dt.Rows[0]["AdditionalTaxpermonth"].ToString();
                        EOBI.Text = dt.Rows[0]["EOBI"].ToString();
                        EOBIArrears.Text = dt.Rows[0]["EOBIArrears"].ToString();
                        otherdeduction.Text = dt.Rows[0]["otherdeductions"].ToString();
                        PFDeduction.Text = dt.Rows[0]["PFDeduction"].ToString();
                        advance.Text = dt.Rows[0]["deductadvancamount"].ToString(); 



                        double sumOfDeductions = Convert.ToDouble(AbsentAmount.Text)+
                           Convert.ToDouble(IncomeTax.Text) +
                             Convert.ToDouble(AdditionalTaxpermonth.Text) +
                             Convert.ToDouble(EOBI.Text) +
                             Convert.ToDouble(EOBIArrears.Text) +Convert.ToDouble(otherdeduction.Text) + Convert.ToDouble(advance.Text)+
                             Convert.ToDouble(PFDeduction.Text);

                        SumOfDeductions.Text = sumOfDeductions.ToString();
                        GridView1.DataSource = dt;
                        GridView1.DataBind();
                        double clearanceDeduct = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            clearanceDeduct += Convert.ToDouble(dt.Rows[i]["Itemamount"].ToString());


                        }
                        clearanceDeduction.Text = clearanceDeduct.ToString();

                        double NetAmount = (SumofBenefits) - (clearanceDeduct + sumOfDeductions);
                        Net.Text = Convert.ToInt32(Math.Round(NetAmount, 0)).ToString();
                        AmountWrds.Text = NumberToWords(Convert.ToInt32(Math.Round(NetAmount, 0)));
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                    ReportPayroll.Visible = false;
                    ReportPayrollFotter.Visible = false;
                }

     
                divAlertMsg.Visible = false;
                ReportPayroll.Visible = true;
                ReportPayrollFotter.Visible = true;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                ReportPayroll.Visible = false;
                ReportPayrollFotter.Visible = false;
            }
        }

        private MemoryStream PDFGenerate(string message, string ImagePath)
        {
            MemoryStream output = new MemoryStream();
            Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, output);
            pdfDoc.Open();
            Paragraph Text = new Paragraph(message);
            pdfDoc.Add(Text);
            byte[] file;
            file = System.IO.File.ReadAllBytes(ImagePath);
            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(file);
            jpg.ScaleToFit(550F, 200F);
            pdfDoc.Add(jpg);
            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            output.Position = 0;
            return output;
        }
		//protected void btnEmail_ServerClick(object sender, EventArgs e)
		//{
		//    try
		//    {
		//         string smtpAddress = "smtp.gmail.com";
		//         int portNumber = 587;
		//         bool enableSSL = true;
		//         string emailFromAddress = "techsupport@3techno.com"; //Sender Email Address  
		//         string password = "Create*0519!"; //Sender Password  
		//         string emailToAddress = "athar@3techno.digital"; //Receiver Email Address  
		//         string subject = "Hello";
		//         string body = "Hello, This is Email sending test using gmail.";


		//        using (MailMessage mail = new MailMessage())
		//        {
		//            mail.From = new MailAddress(emailFromAddress);
		//            mail.To.Add(emailToAddress);
		//            mail.Subject = subject;
		//            mail.Body = body;
		//            mail.IsBodyHtml = true;

		//            MemoryStream file = new MemoryStream(PDFGenerate("This is fnf file", Server.MapPath("Images/photo.jpg")).ToArray());

		//            file.Seek(0, SeekOrigin.Begin);
		//            Attachment data = new Attachment(file, "RunTime_Attachment.pdf", "application/pdf");
		//            ContentDisposition disposition = data.ContentDisposition;
		//            disposition.CreationDate = System.DateTime.Now;
		//            disposition.ModificationDate = System.DateTime.Now;
		//            disposition.DispositionType = DispositionTypeNames.Attachment;
		//            mail.Attachments.Add(data);//Attach the file  

		//            //mail.Attachments.Add(new Attachment("D:\\TestFile.txt"));//--Uncomment this to send any attachment  
		//            using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
		//            {
		//                smtp.Credentials = new NetworkCredential(emailFromAddress, password);
		//                smtp.EnableSsl = enableSSL;
		//                smtp.Send(mail);
		//            }
		//        }
		//    }











		//        //MailMessage message = new MailMessage();
		//        //message.To.Add("athar@3techno.digital");// Email-ID of Receiver  
		//        //message.Subject ="FnF";// Subject of Email  
		//        //message.From = new System.Net.Mail.MailAddress("techsupport@3techno.com");// Email-ID of Sender  
		//        //message.IsBodyHtml = true;

		//        //MemoryStream file = new MemoryStream(PDFGenerate("This is pdf file text", Server.MapPath("Images/photo.jpg")).ToArray());

		//        //file.Seek(0, SeekOrigin.Begin);
		//        //Attachment data = new Attachment(file, "RunTime_Attachment.pdf", "application/pdf");
		//        //ContentDisposition disposition = data.ContentDisposition;
		//        //disposition.CreationDate = System.DateTime.Now;
		//        //disposition.ModificationDate = System.DateTime.Now;
		//        //disposition.DispositionType = DispositionTypeNames.Attachment;
		//        //message.Attachments.Add(data);//Attach the file  

		//        //message.Body = "Settlement";
		//        //SmtpClient SmtpMail = new SmtpClient();
		//        //SmtpMail.Host = "smtp.gmail.com";//name or IP-Address of Host used for SMTP transactions  
		//        //SmtpMail.Port = 587;//Port for sending the mail  
		//        //SmtpMail.Credentials = new System.Net.NetworkCredential("", "");//username/password of network, if apply  
		//        //SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
		//        //SmtpMail.EnableSsl = false;
		//        //SmtpMail.ServicePoint.MaxIdleTime = 0;
		//        //SmtpMail.ServicePoint.SetTcpKeepAlive(true, 2000, 2000);
		//        //message.BodyEncoding = Encoding.Default;
		//        //message.Priority = MailPriority.High;
		//        //SmtpMail.Send(message); //Smtpclient to send the mail message  
		//        //Response.Write("Email has been sent");

		//    catch (Exception ex)
		//    { Response.Write("Failed"); }
		//}

		protected void btnPDF_ServerClick(object sender, EventArgs e)
		{
			try
            {
                string content = "";
                string header = "";
                string footer = "";
               
                var sb = new StringBuilder();
                slip.RenderControl(new HtmlTextWriter(new StringWriter(sb)));

                string s = sb.ToString();
                
                // fix CSS for FNF SLIP
                string css = "<div> <style> .B_class { padding: 20px; border: 1px solid #000; background: #fff; } .B_class h1, .B_class h2, .B_class h3, .B_class h4, .B_class p, .B_class td { margin: 0; font-family: lato; color: #000; } .B_class table { border: 1px solid; margin-top: -25px; } .B_class table td, .B_class table th { padding: 3.4px; } .B_class .slip-table thead tr th { background-color: #ffffff; border-bottom: 1px solid #000000 !important; border-top: 1px solid #000000 !important; color: #000; padding: 0 10px; } .slip-table-bordered thead tr th, .slip-table-bordered thead tr td, .slip-table-bordered tbody tr th, .slip-table-bordered tbody tr td { border-top: 1px solid transparent !important; border: 0; vertical-align: middle; padding: 0.1rem; font-size: 14px; } .slip-table thead tr th, .slip-table thead tr td, .slip-table tbody tr th, .slip-table tbody tr td, .slip-table tfoot tr th, .slip-table tfoot tr td { height: 32px; line-height: 1.428571429; vertical-align: middle; border-top: 1px solid #ddd; padding: 0 10px; } .slip-table { width: 100%; max-width: 100%; background-color: transparent; border-collapse: collapse; border-spacing: 0; } .slip-table-bordered tfoot tr th, .slip-table-bordered tfoot tr td { border-top: 1px solid #000 !important; border: 0; vertical-align: middle; padding: 0.1rem; font-size: 14px; padding: 0 10px; } .B_class .slip-table thead tr th { text-align: right; } .B_class .slip-table thead tr th:nth-child(1) { text-align: left; } .B_class .slip-table tbody tr td { text-align: right; height: 28px; } .B_class .slip-table tbody tr td:nth-child(1) { text-align: left; } .B_class .slip-table tfoot tr td { text-align: right; height: 34px; } .B_class .slip-table tfoot tr td:nth-child(1) { text-align: left; width: 200px !important; } .B_class hr { margin-top: 20px; margin-bottom: 10px; border: 0; border-top: 1px solid #000000; } .tableP { height: 34px; padding: 6.4px; border: 1px solid; margin-top: -6px !important; width: 99.9%; } .tableP span { margin-left: 40px; text-transform: capitalize; } .AddressSide { height: 53px; color: #000; text-align: center; } .AddressSide h1 { font-size: 18px; text-transform: capitalize; font-weight: 700; margin: 5px 0; color: #000; } .AddressSide h2 { font-size: 18px; text-align: right; font-weight: 400; color: #000; margin: 5px 0; } .AddressSide h3 { font-size: 18px; text-transform: capitalize; font-weight: 700; margin: 5px 0; color: #000; } .AddressSide h4 { font-size: 18px; text-transform: capitalize; font-weight: 700; margin-bottom: 15px; color: #000; margin: 5px 0; } .AddressSide p { font-size: 16px; font-weight: 500; max-width: 350px; text-transform: capitalize; color: #000; } .bigBox { width: 100%; border: 1px solid #000; height: 400px; margin: 50px 0 20px; } .Box01 { width: 45.5%; display: inline-block; padding: 10px; color: #000; } .Box02 { width: 46%; display: inline-block; padding: 10px 20px; margin-left: -5px; color: #000; } .Box03 { width: 50%; display: inline-block; } .Box04 { width: 50%; margin-left: -5px; display: inline-block; } .Box05 { width: 100%; display: block; } .OneLine { width: 100%; height: 28px; } .OneLine h3 { display: inline-block; font-size: 16px; font-weight: 700; width: 35%; margin: 3px 0; color: #000; } .OneLine p { display: inline-block; font-size: 16px; width: 60%; margin: 3px 0; color: #000; } .totalSalaries { font-weight: bold !important; color: #188ae2 !important; font-size: 16px !important; } .line-four { width: 99.9%; display: block; } #tblPDF td { border: 1px solid; border-right: none; border-top: none; } </style>";
                content = s;
                content = css + s+"</div>";

                Common.generatePDF(header, footer, content, "FnF_Slip_" + EmployeeName + " ", "A4", "Portrait", 30, 30);
            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }
        }
        protected void ddlYear_SelectedIndexChangedNew(object sender, EventArgs e)
        {
            try
            {
                decimal totalMedicalYTD = 0;
                decimal totalYTDminus = 0;
                decimal sumTaxableAmount2 = 0;
                decimal AnnualTaxableIncome2 = 0;
                decimal minusValue = 0;
                decimal CalSum = 0;
                decimal sumYTDTAX = 0;
                decimal sumYTDAddTax = 0;

                //objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);// Convert.ToInt32(Session["EmployeeID"]);
                objDB.PayrollDate = txtPayrollDate.Text;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.SelectedEmp = 3206.ToString();
                DataTable dt = new DataTable();
                dt = objDB.GetFnFSlipEmployeeByID(ref errorMsg);

                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        slip.Visible = true;
                       // PrintBtn.Visible = true;
                       // Month.Text = txtPayrollDate.Text;
                        employeecode.Text = dt.Rows[0]["wdid"].ToString();
                        location.Text = dt.Rows[0]["location"].ToString();
                        designation.Text = dt.Rows[0]["designation"].ToString();
                        Section.Text = dt.Rows[0]["Section"].ToString();
                        DateofJoining.Text = dt.Rows[0]["DateOfJoining"].ToString()=="" ? "" : Convert.ToDateTime(dt.Rows[0]["DateOfJoining"]).ToString("dd-MM-yyyy");
                        Address.Text = dt.Rows[0]["Address"].ToString();
                        Bank.Text = dt.Rows[0]["Bank"].ToString();
                        branch.Text = dt.Rows[0]["branch"].ToString();
                        NTN.Text = dt.Rows[0]["NTN"].ToString();
                        Leave.Text = dt.Rows[0]["Leave"].ToString();
                        Resign.Text = dt.Rows[0]["Resign"].ToString();
                        NoticeDays.Text = dt.Rows[0]["NoticeDays"].ToString();
                        Employeename.Text = dt.Rows[0]["Employeename"].ToString();
                        department.Text = dt.Rows[0]["department"].ToString();
                        EmploymentType.Text = dt.Rows[0]["EmploymentType"].ToString();
                        Grade.Text = dt.Rows[0]["Grade"].ToString();
                        //DateofConfirmation.Text = dt.Rows[0]["DateofConfirmation"].ToString() =="" ? "" : Convert.ToDateTime(dt.Rows[0]["DateofConfirmation"]).ToString("dd-MM-yyyy");
                        DateofConfirmation.Text = "";
                        DOB.Text = Convert.ToDateTime(dt.Rows[0]["DOB"]).ToString("dd-MM-yyyy")==""? "" : Convert.ToDateTime(dt.Rows[0]["DOB"]).ToString("dd-MM-yyyy");
                        AccountNo.Text = dt.Rows[0]["AccountNo"].ToString();
                        //AccountNo.Text = dt.Rows[0]["department"].ToString();
                        //DateofConf.Text = dt.Rows[0]["DateOfJoining"].ToString();
                        CNIC.Text = dt.Rows[0]["CNIC"].ToString();
                      
                        Shift.Text = dt.Rows[0]["Shift"].ToString();
                        StandardGross.Text = dt.Rows[0]["StandardGross"].ToString();
                        lastworkingday.Text = dt.Rows[0]["lastworkingday"].ToString();
                     
                        Tenure.Text = dt.Rows[0]["Tenure"].ToString();

                        BasicSalary.Text= dt.Rows[0]["BasicSalary"].ToString();
                        MedicalAllowance.Text= dt.Rows[0]["MedicalAllowance"].ToString();
                        UtilityAllowance.Text= dt.Rows[0]["UtilityAllowance"].ToString();
                        BasicSalary.Text= dt.Rows[0]["HouseRent"].ToString();
                        HouseRent.Text= dt.Rows[0]["BasicSalary"].ToString();
                        commission.Text= dt.Rows[0]["commission"].ToString();
                        EidBonus.Text= dt.Rows[0]["EidBonus"].ToString();
                        otherbonuses.Text= dt.Rows[0]["otherbonuses"].ToString();
                        leaveencashment.Text= dt.Rows[0]["leaveencashment"].ToString();
                        otherexpense.Text= dt.Rows[0]["otherexpense"].ToString();
                        spiffs.Text= dt.Rows[0]["spiffs"].ToString();
                        Overtime.Text= dt.Rows[0]["Overtime"].ToString();
                        gratuity.Text= dt.Rows[0]["gratuity"].ToString();
                        //employeePF.Text= dt.Rows[0]["employeePF"].ToString();
                       // CompanyPF.Text= dt.Rows[0]["CompanyPF"].ToString();

                        double SumofBenefits =
                             Convert.ToDouble(dt.Rows[0]["BasicSalary"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["MedicalAllowance"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["UtilityAllowance"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["BasicSalary"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["HouseRent"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["commission"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["EidBonus"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["otherbonuses"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["leaveencashment"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["otherexpense"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["spiffs"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["Overtime"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["gratuity"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["employeePF"].ToString()) +
                             Convert.ToDouble(dt.Rows[0]["CompanyPF"].ToString());

                        SumofBenefitsnAllowances.Text = SumofBenefits.ToString();

                        IncomeTax.Text= dt.Rows[0]["IncomeTax"].ToString();
                        AdditionalTaxpermonth.Text= dt.Rows[0]["AdditionalTaxpermonth"].ToString();
                        EOBI.Text= dt.Rows[0]["EOBI"].ToString();
                        EOBIArrears.Text= dt.Rows[0]["EOBIArrears"].ToString();
                     
                        PFDeduction.Text = dt.Rows[0]["PFDeduction"].ToString();



                        double sumOfDeductions =
                           Convert.ToDouble(IncomeTax.Text) +
                             Convert.ToDouble(AdditionalTaxpermonth.Text) +
                             Convert.ToDouble(EOBI.Text) +
                             Convert.ToDouble(EOBIArrears.Text) +
                             Convert.ToDouble(PFDeduction.Text);

                        SumOfDeductions.Text = sumOfDeductions.ToString();
                        GridView1.DataSource = dt;
                        GridView1.DataBind();
                        double clearanceDeduct = 0;
                        for (int i=0; i< dt.Rows.Count;i++)
						{
                            clearanceDeduct += Convert.ToDouble(dt.Rows[i]["Itemamount"].ToString());


                        }
                        clearanceDeduction.Text = clearanceDeduct.ToString();

                        double NetAmount = (SumofBenefits) - (clearanceDeduct + sumOfDeductions);
                        Net.Text = NetAmount.ToString();
                        AmountWrds.Text = NumberToWords(Convert.ToInt32(Math.Round(NetAmount,0)));
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                    ReportPayroll.Visible = false;
                    ReportPayrollFotter.Visible = false;
                }

                //DataTable dt2 = objDB.GetPaySlipKTByEmployeeID(ref errorMsg);
                //if (dt2 != null && dt2.Rows.Count > 0)
                //{
                //    try
                //    {
                //        double total, total2, NetAmount;

                //        username.Text = dt.Rows[0]["EmployeeName"].ToString();
                //        Datetime.Text = DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss");
                //        BasicSalary.Text = Convert.ToDecimal(dt2.Rows[0]["EmployeeSalary"]).ToString("N2");
                //        lblPresentDays.Text = (Convert.ToInt16(dt2.Rows[0]["TotalDays"]) - Convert.ToInt16(dt2.Rows[0]["AbsentDays"])).ToString();
                //        lblTotalDays.Text = dt2.Rows[0]["TotalDays"].ToString();

                //        string basicSalary = dt2.Rows[0]["BasicSalary"].ToString();
                //        string SalesComissions = dt2.Rows[0]["Commission"].ToString();
                //        string houseRents = dt2.Rows[0]["HouseRent"].ToString();
                //        string UtilityAllows = dt2.Rows[0]["UtilityAllowence"].ToString();
                //        string mdeicalAllowance = dt2.Rows[0]["MedicalAllowence"].ToString();
                //        string ExpenseReims = dt2.Rows[0]["OtherExpenses"].ToString();
                //        string EidBonuss = dt2.Rows[0]["EidBonus"].ToString();
                //        string Spiffss = dt2.Rows[0]["Spiffs"].ToString();
                //        string Overtimes = dt2.Rows[0]["OverTimeGeneralAmount"].ToString();
                //        string OvertimeHolidayss = dt2.Rows[0]["OvertimeHolidayAmount2"].ToString();
                //        string IncomeTaxs = dt2.Rows[0]["Tax"].ToString();
                //        string AddIncomeTaxs = dt2.Rows[0]["AdditionalTax"].ToString();
                //        string EOBIs = dt2.Rows[0]["EOBI"].ToString();
                //        string OtherAllowances = dt2.Rows[0]["Other_Allowances"].ToString();
                //        string PFNew = dt2.Rows[0]["PF"].ToString();
                //        string OtherDeduction = dt2.Rows[0]["OtherDeductions"].ToString();
                //        string TotSal2 = dt2.Rows[0]["NetSalry2"].ToString();
                //        double totalsumm = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss) + double.Parse(EidBonuss);
                //        TotSal2 = totalsumm.ToString();
                //        double totalgs = double.Parse(basicSalary) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows);
                //        decimal tts = decimal.Parse(totalgs.ToString()).RoundDecimalTwoDigit();
                //        sumTaxableAmount2 = decimal.Parse(basicSalary) + decimal.Parse(houseRents) + decimal.Parse(UtilityAllows);
                //        total = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss);
                //        total2 = double.Parse(IncomeTaxs) + double.Parse(EOBIs) + double.Parse(PFNew) + double.Parse(OtherDeduction) + double.Parse(AddIncomeTaxs);
                //        NetAmount = total - total2;

                //        decimal bs = decimal.Parse(basicSalary.ToString()).RoundDecimalTwoDigit();
                //        decimal ttt = decimal.Parse(TotSal2.ToString()).RoundDecimalTwoDigit();
                //        decimal sc = decimal.Parse(SalesComissions.ToString()).RoundDecimalTwoDigit();
                //        decimal hr = decimal.Parse(houseRents.ToString()).RoundDecimalTwoDigit();
                //        decimal ua = decimal.Parse(UtilityAllows.ToString()).RoundDecimalTwoDigit();
                //        decimal er = decimal.Parse(ExpenseReims.ToString()).RoundDecimalTwoDigit();
                //        decimal eb = decimal.Parse(EidBonuss.ToString()).RoundDecimalTwoDigit();
                //        decimal sp = decimal.Parse(Spiffss.ToString()).RoundDecimalTwoDigit();
                //        decimal ot = decimal.Parse(Overtimes.ToString()).RoundDecimalTwoDigit();
                //        decimal oth = decimal.Parse(OvertimeHolidayss.ToString()).RoundDecimalTwoDigit();
                //        decimal it = decimal.Parse(IncomeTaxs.ToString()).RoundDecimalTwoDigit();
                //        decimal ait = decimal.Parse(AddIncomeTaxs.ToString()).RoundDecimalTwoDigit();
                //        decimal eobi = decimal.Parse(EOBIs.ToString()).RoundDecimalTwoDigit();
                //        decimal ma = decimal.Parse(mdeicalAllowance.ToString()).RoundDecimalTwoDigit();
                //        decimal ollow = decimal.Parse(OtherAllowances.ToString()).RoundDecimalTwoDigit();
                //        decimal pf = decimal.Parse(PFNew.ToString()).RoundDecimalTwoDigit();
                //        decimal odnew = decimal.Parse(OtherDeduction.ToString()).RoundDecimalTwoDigit();

                //        OtherDeductiontxt.Text = odnew.ToString("N2").GetDashWhenZero();
                //        PFtxt.Text = pf.ToString("N2").GetDashWhenZero();
                //        basicSalry.Text = bs.ToString("N2").GetDashWhenZero();
                //        SalesComission.Text = sc.ToString("N2").GetDashWhenZero();
                //        houseRent.Text = hr.ToString("N2").GetDashWhenZero();
                //        UtilityAllow.Text = ua.ToString("N2").GetDashWhenZero();
                //        ExpenseReim.Text = er.ToString("N2").GetDashWhenZero();
                //        EidBonus.Text = eb.ToString("N2").GetDashWhenZero();
                //        Spiffs.Text = sp.ToString("N2").GetDashWhenZero();
                //        Overtime.Text = ot.ToString("N2").GetDashWhenZero();
                //        OvertimeHolidays.Text = oth.ToString("N2").GetDashWhenZero();
                //        IncomeTax.Text = it.ToString("N2").GetDashWhenZero();
                //        AddIncomeTax.Text = ait.ToString("N2").GetDashWhenZero();
                //        EOBI.Text = eobi.ToString("N2").GetDashWhenZero();
                //        mdecialAllow.Text = ma.ToString("N2").GetDashWhenZero();
                //        Total1.Text = ttt.ToString("N2").GetDashWhenZero();
                //        Total3.Text = total2.ToString("N2").GetDashWhenZero();
                //        NetAmnt.Text = NetAmount.ToString("N2").GetDashWhenZero();
                //        TotalGross.Text = tts.ToString("N2").GetDashWhenZero();
                //        AmountWrds.Text = NumberToWords(Convert.ToInt32(NetAmount)) + " only-";
                //        OtherAllowance.Text = ollow.ToString("N2").GetDashWhenZero();
                //    }
                //    catch (Exception ex)
                //    {
                //        throw;
                //    }
                //}
                //else
                //{
                //    divAlertMsg.Visible = true;
                //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //    pAlertMsg.InnerHtml = "No Record Found";

                //    ReportPayroll.Visible = false;
                //    ReportPayrollFotter.Visible = false;
                //    return;
                //}

                //string date = "01-" + txtPayrollDate.Text;
                //objDB.PayrollDate = date;
                //DateTime dateTime12 = Convert.ToDateTime(date);
                //int months = int.Parse(dateTime12.ToString("MM"));
                //int RemainingMonths = 0;
                //double TotalSalary2 = 0;

                //RemainingMonths = months > 6 ? 18 - months : 6 - months;

                //if (RemainingMonths != 0)
                //{
                //    double subtract = double.Parse(dt.Rows[0]["GrossAmount"].ToString()) - double.Parse(dt.Rows[0]["MedicalAllowance"].ToString());
                //    TotalSalary2 = RemainingMonths * subtract;
                //}

                //DataTable dt3 = objDB.GetPaySlipByEmployeeID_YTD(ref errorMsg);
                //double sumYTD = 0;
                //double SumI = 0;
                //double AnnualTaxableIncome = 0;
                //if (dt3 != null && dt3.Rows.Count > 0)
                //{
                //    decimal bsytd = decimal.Parse(dt3.Rows[0]["BasicSalaryYTD"].ToString().ToString()).RoundDecimalTwoDigit();
                //    decimal scytd = decimal.Parse(dt3.Rows[0]["CommissionYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal HRYTD = decimal.Parse(dt3.Rows[0]["HouseRentYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal MAYTD = decimal.Parse(dt3.Rows[0]["MedicalAllowenceYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal UAYTD = decimal.Parse(dt3.Rows[0]["UtilityAllowenceYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal OEYTD = decimal.Parse(dt3.Rows[0]["OtherExpensesYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal ebytd = decimal.Parse(dt3.Rows[0]["EidBonusYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal sytd = decimal.Parse(dt3.Rows[0]["SpiffsYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal otytd = decimal.Parse(dt3.Rows[0]["OverTimeYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal ebhytd = decimal.Parse(dt3.Rows[0]["OvertimeHolidayYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal itytd = decimal.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal eobiytd = decimal.Parse(dt3.Rows[0]["EobiYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal overtimetoalytd = decimal.Parse(dt3.Rows[0]["OverTimeTotalAmountsYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal otytds = decimal.Parse(dt3.Rows[0]["Other_AllowancesYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal pfytd = decimal.Parse(dt3.Rows[0]["PFYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal odytd = decimal.Parse(dt3.Rows[0]["OtherDeductionsYTD"].ToString()).RoundDecimalTwoDigit();
                //    decimal totalgsYTD = bsytd + HRYTD + MAYTD + UAYTD;
                //    decimal TOTYTD = decimal.Parse(dt3.Rows[0]["TotalSalry2"].ToString()).RoundDecimalTwoDigit();

                //    double totalsummytd = double.Parse(bsytd.ToString())
                //        + double.Parse(otytds.ToString())
                //        + double.Parse(scytd.ToString())
                //        + double.Parse(HRYTD.ToString())
                //        + double.Parse(MAYTD.ToString())
                //        + double.Parse(UAYTD.ToString())
                //        + double.Parse(OEYTD.ToString())
                //        + double.Parse(sytd.ToString())
                //         + double.Parse(ebytd.ToString())
                //        + double.Parse(overtimetoalytd.ToString());

                //    totalYTDminus = decimal.Parse(totalsummytd.ToString());
                //    string TotSal2ytds = totalsummytd.ToString();

                //    TotalGrossYTD.Text = totalgsYTD.ToString("N2").GetDashWhenZero();
                //    OtherDeductiontxtYTD.Text = odytd.ToString("N2").GetDashWhenZero();
                //    PFYTD.Text = pfytd.ToString("N2").GetDashWhenZero();
                //    OtherAllowanceYTD.Text = otytds.ToString("N2").GetDashWhenZero();
                //    basicSalryYTD.Text = bsytd.ToString("N2").GetDashWhenZero();
                //    SalesComissionYTD.Text = scytd.ToString("N2").GetDashWhenZero();
                //    houseRentYTD.Text = HRYTD.ToString("N2").GetDashWhenZero();
                //    mdecialAllowYTD.Text = MAYTD.ToString("N2").GetDashWhenZero();
                //    UtilityAllowYTD.Text = UAYTD.ToString("N2").GetDashWhenZero();
                //    ExpenseReimYTD.Text = OEYTD.ToString("N2").GetDashWhenZero();
                //    EidBonusYTD.Text = ebytd.ToString("N2").GetDashWhenZero();
                //    SpiffsYTD.Text = sytd.ToString("N2").GetDashWhenZero();
                //    OvertimeYTD.Text = otytd.ToString("N2").GetDashWhenZero();
                //    OvertimeHolidaysYTD.Text = ebhytd.ToString("N2").GetDashWhenZero();
                //    IncomeTaxYTD.Text = itytd.ToString("N2").GetDashWhenZero();
                //    EOBIYTD.Text = eobiytd.ToString("N2").GetDashWhenZero();

                //    double adtytdd = Convert.ToDouble(dt3.Rows[0]["AddIncomeTaxYTD"].ToString());
                //    AddIncomeTaxYTD.Text = adtytdd.ToString("N2").GetDashWhenZero();
                //    SumI = double.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString()) + double.Parse(dt3.Rows[0]["EobiYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherDeductionsYTD"].ToString()) + double.Parse(dt3.Rows[0]["PFYTD"].ToString()) + double.Parse(dt3.Rows[0]["AddIncomeTaxYTD"].ToString());
                //    sumYTD = double.Parse(dt3.Rows[0]["BasicSalaryYTD"].ToString()) + double.Parse(dt3.Rows[0]["Other_AllowancesYTD"].ToString()) + double.Parse(dt3.Rows[0]["CommissionYTD"].ToString()) + double.Parse(dt3.Rows[0]["HouseRentYTD"].ToString()) + double.Parse(dt3.Rows[0]["MedicalAllowenceYTD"].ToString()) + double.Parse(dt3.Rows[0]["UtilityAllowenceYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherExpensesYTD"].ToString()) + double.Parse(dt3.Rows[0]["EidBonusYTD"].ToString()) + double.Parse(dt3.Rows[0]["SpiffsYTD"].ToString()) + double.Parse(dt3.Rows[0]["OverTimeYTD"].ToString()) + double.Parse(dt3.Rows[0]["OvertimeHolidayYTD"].ToString());
                //    Total2.Text = totalsummytd.ToString("N2").GetDashWhenZero();
                //    Total4.Text = SumI.ToString("N2").GetDashWhenZero();
                //    sumYTDTAX = itytd + decimal.Parse(adtytdd.ToString());
                //    minusValue = totalYTDminus - totalMedicalYTD;

                //    if (RemainingMonths != 0)
                //    {
                //        CalSum = sumTaxableAmount2 * RemainingMonths;
                //    }
                //    else if (RemainingMonths == 0)
                //    {
                //        CalSum = sumTaxableAmount2;
                //    }

                //    AnnualTaxableIncome2 = minusValue + CalSum;
                //    AnnualTaxableIncome2 = Math.Round(AnnualTaxableIncome2, 2);

                //    AnnualTaxableIncome = double.Parse(dt3.Rows[0]["TotalSalry2"].ToString()) + TotalSalary2;
                //    AnnualTaxableIncome = Math.Round(AnnualTaxableIncome, 2);
                //    AnnualTaxInc.Text = AnnualTaxableIncome2.ToString("N2").GetDashWhenZero();
                //    double itp = Convert.ToDouble(dt3.Rows[0]["IncomeTaxYTD"].ToString());
                //    IncomeTaxPaid.Text = sumYTDTAX.ToString("N2").GetDashWhenZero();
                //}

                //objDB.PayAmount = float.Parse(AnnualTaxableIncome.ToString());
                //objDB.PayAmount = float.Parse(AnnualTaxableIncome2.ToString());
                //objDB.PayrollDate = date;
                //DataTable dt4 = objDB.GetAnnualTaxableIncomePayslips(ref errorMsg);
                //if (dt4 != null && dt4.Rows.Count > 0)
                //{
                //    double TITL = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
                //    TotalIncomeTaxLia.Text = TITL.ToString("N2").GetDashWhenZero();
                //    sumYTDAddTax = decimal.Parse(TITL.ToString()) - sumYTDTAX;
                //}

                //double incometaxLia = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
                //double reamainingIncomeTax = incometaxLia - double.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString());

                //RemIncomeTaxPayable.Text = sumYTDAddTax.ToString("N2").GetDashWhenZero();

                //DataTable dt5 = objDB.GetPaySlipPFKTByEmployeeID(ref errorMsg);
                //if (dt5 != null && dt5.Rows.Count > 0)
                //{
                //    EmployeePF.Text = dt5.Rows[0]["OpeningPF_Employee"].ToString();
                //    EmployeerPF.Text = dt5.Rows[0]["OpeningPF_Company"].ToString();
                //    Label47.Text = dt5.Rows[0]["CurrentPF_Employee"].ToString();
                //    Label48.Text = dt5.Rows[0]["closingEmployee"].ToString();
                //    Label49.Text = dt5.Rows[0]["CurrentPF_Company"].ToString();
                //    Label50.Text = dt5.Rows[0]["ClosingEmployeer"].ToString();
                //    Label51.Text = dt5.Rows[0]["sumcurrent"].ToString();
                //    Label52.Text = dt5.Rows[0]["totalsum2"].ToString();
                //    TotalPF.Text = dt5.Rows[0]["SumOpening"].ToString();
                //}

                divAlertMsg.Visible = false;
                ReportPayroll.Visible = true;
                ReportPayrollFotter.Visible = true;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                ReportPayroll.Visible = false;
                ReportPayrollFotter.Visible = false;
            }
        }




        //private void CheckAccess()
        //{
        //    try
        //    {
        //        btnApproveByFinance.Visible =
        //        lnkDelete.Visible =
        //        btnSubForReview.Visible =
        //        btnDisapprove.Visible = false;

        //        objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
        //        objDB.TableName = "EmployeeSeperationDetails";
        //        objDB.PrimaryColumnnName = "SeperationDetailsID";
        //        objDB.PrimaryColumnValue = NewPayrollID.ToString();
        //        objDB.DocName = "Payroll";

        //        string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
        //        if (chkAccessLevel == "Can Edit")
        //        {
        //            lnkDelete.Visible =
        //            btnSubForReview.Visible = true;
        //        }
        //        if (chkAccessLevel == "Can Edit & Approve")
        //        {
        //            btnApproveByFinance.Visible =
        //            btnDisapprove.Visible = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }
        //}

        private DataTable dtEmployeeSalaries
        {
            get
            {
                if (ViewState["dtEmployeeSalaries"] != null)
                {
                    return (DataTable)ViewState["dtEmployeeSalaries"];
                }
                else
                {
                    return new DataTable();
                }
            }
            set
            {
                ViewState["dtEmployeeSalaries"] = value;
            }
        }

        private void clearFields()
        {
            txtPayrollDate.Text = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = string.Empty;
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                
                if (btn.ID.ToString() == "btnApproveByFinance" || btn.ID.ToString() == "btnRevApproveByFinance")
                {
                    DataTable dtEmployees = (DataTable)gvSalaries.DataSource;
                    List<string> lstEmployees = dtEmployees.AsEnumerable().Select(x => x[0].ToString()).ToList();
                    res = objDB.ApproveEmployeeClearance(lstEmployees);
                    GetEmployeeFnFData();
                }
                else if (btn.ID.ToString() == "btnDisapprove" || btn.ID.ToString() == "btnRejDisApprove")
                {
                    objDB.NewPayrollID = NewPayrollID;
                    //string ss = objDB.ResetAddOnsStatus();
                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Common.addlogNew(res, "Finance", "Payroll of ID\"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/employee-fnf-settlement", "", "Payroll of Date \"" + txtPayrollDate.Text + "\"", "EmployeeSeperationDetails", "FnF Settlement", Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString()));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
            //CheckAccess();
        }
        
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "EmployeeSeperationDetails", "SeperationDetailsID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "NewPayroll of ID \"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" deleted", "EmployeeSeperationDetails", NewPayrollID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {

                int index = e.RowIndex;

                //dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
                dtEmployeeSalaries.Rows[index].SetField("TotalDeduction", (Convert.ToDouble(((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("OtherDeductionsInput")).Text) + Convert.ToDouble(((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("TotalDeduction")).Text)).ToString());
                //dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
                //dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
                dtEmployeeSalaries.AcceptChanges();

                //dtEmployeeSalaries.Rows[index]["NetSalry"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
                //dtEmployeeSalaries.AcceptChanges();

                gvSalaries.EditIndex = -1;


                BindData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void RowUpdating(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = (sender as LinkButton).NamingContainer as GridViewRow;
                int index = row.RowIndex;

                //dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
                dtEmployeeSalaries.Rows[index].SetField("TotalDeduction", (Convert.ToDouble(((TextBox)gvSalaries.Rows[row.RowIndex].FindControl("OtherDeductionsInput")).Text) + Convert.ToDouble(((TextBox)gvSalaries.Rows[row.RowIndex].FindControl("TotalDeduction")).Text)).ToString());
                //dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
                //dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
                dtEmployeeSalaries.AcceptChanges();

                //dtEmployeeSalaries.Rows[index]["NetSalry"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
                //dtEmployeeSalaries.AcceptChanges();

                gvSalaries.EditIndex = -1;


                BindData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        //protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    try
        //    {
        //        int index = e.RowIndex;

        //        dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("AdditionalTax", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditTax")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
        //        dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
        //        dtEmployeeSalaries.AcceptChanges();

        //        dtEmployeeSalaries.Rows[index]["NetSalary"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
        //        dtEmployeeSalaries.AcceptChanges();

        //        gvSalaries.EditIndex = -1;
        //        BindData();
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }

        //}
        protected void gvSalaries_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSalaries.EditIndex = -1;
            BindData();
        }
        protected void gvSalaries_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSalaries.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void GetEmployeeFnFData()
        {
            objDB.PayrollDate = txtPayrollDate.Text;
            objDB.PayrollDate = "01-" + txtPayrollDate.Text;
            dtEmployeeSalaries = objDB.GetKTPayrollDetailsForLeftEmployeesByNewPayrollID(ref errorMsg);
            BindData();
        }
        protected void BindData()
        {
            gvSalaries.DataSource = dtEmployeeSalaries;
            gvSalaries.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    gvSalaries.UseAccessibleHeader = true;
                    gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            gvNormalSal.DataSource = dtEmployeeSalaries;
            gvNormalSal.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    gvNormalSal.UseAccessibleHeader = true;
                    gvNormalSal.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

		protected void btnView_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            GetEmployeeFnFData();
        }


		protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
		{
            int a = 0;

		}
        protected void lnkUpdate_Command(object sender, CommandEventArgs e)
        {

            ////dt.Columns.Add("SrNo");
            ////dt.Columns.Add("ExpType");
            ////dt.Columns.Add("Description");
            ////dt.Columns.Add("Amount");
            ////dt.Columns.Add("FilePath");
            //LinkButton lnk = (LinkButton)sender as LinkButton;
            //string delSr = lnk.CommandArgument.ToString();
            //for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            //{
            //    if (dtExpDetails.Rows[i][0].ToString() == delSr)
            //    {
            //        ddlExpType.SelectedValue = dtExpDetails.Rows[i]["ExpType"].ToString();
            //        txtDescriptionDetail.Value = dtExpDetails.Rows[i]["Description"].ToString();
            //        txtAmountDetail.Value = dtExpDetails.Rows[i]["Amount"].ToString();
            //        imgLogo.Src = dtExpDetails.Rows[i]["FilePath"].ToString();
            //        hdnExpenseSrNO.Value = delSr;
            //        btnUpdateDiv.Visible = true;
            //        btnAddDiv.Visible = false;
            //    }
            //}
            //for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            //{
            //    dtExpDetails.Rows[i].SetField(0, i + 1);
            //    dtExpDetails.AcceptChanges();
            //}

            //ExpDetailsSrNo = dtExpDetails.Rows.Count + 1;
            //BindExpDetailsTable();
        }
        

    }
}