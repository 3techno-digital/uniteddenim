﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.finance.view
{
	public partial class ClearanceItems : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
    
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            { 
                BindDepartmentDropDown();
                GetData();

            }
        }
        private void BindDepartmentDropDown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                ddlDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                ddlDept.DataTextField = "DeptName";
                ddlDept.DataValueField = "DeptID";
                ddlDept.DataBind();
                ddlDept.Items.Insert(0, new ListItem("--- All ---", "0"));


            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void GetData()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DeptID = Convert.ToInt32(ddlDept.SelectedValue);
            dt = objDB.GetAllClearanceItemsByCopmanyAndDepartmentId(ref errorMsg);
            gvCurrency.DataSource = FilterData(dt);
            gvCurrency.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvCurrency.UseAccessibleHeader = true;
                    gvCurrency.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "Finance", "All ClearanceItems Viewed", "Currency");
        }


        private DataTable FilterData(DataTable dt)
        {
            DataTable dtFilter = new DataTable();

            if (dt == null)
            {
                return dt;
            }

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            //     objDB.DocName = "Currency";
            objDB.DocName = "EmployeeLists";
            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");

                    if (dtTemp != null)
                        dtFilter.Merge(dtTemp);

                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }

                }
            }

            return dtFilter;
        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            int ID = Convert.ToInt32(btn.CommandArgument);
            objDB.ClearanceItemID = ID;
            objDB.DeletedBy = Session["UserName"].ToString();

            objDB.DeleteAddOnsExpenseByID();
            GetData();
        }
    }
}