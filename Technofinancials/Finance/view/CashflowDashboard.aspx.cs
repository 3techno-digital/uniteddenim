﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.view
{
    public partial class CashflowDashboard : System.Web.UI.Page
    {
        string errorMsg;
        DBQueries objDB = new DBQueries();
        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                try
                {   
                    txtMonth.Disabled = true;
                    generateAccountChart();
                }
                catch (Exception ex)
                {

                }
            }
        }

       
        protected void generateAccountChart(string Months = "")
        {
           string StartDate = "01-JAN-" + DateTime.Now.Year.ToString();

            if (Months != "")
            {
                StartDate = DateTime.Now.AddMonths(Convert.ToInt32("-" + Months) + 1).ToShortDateString();
            }

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.StartDate = StartDate;
            dt = objDB.GetAccountReport(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    string script = "<div data-plugin=\"chart\" data-options=\"{";
                    script += "tooltip:";
                    script += "{";
                    script += "trigger: 'item',";
                    script += "formatter: '{a} <br/>{b}: {c} ({d}%)'";
                    script += "},";
                    script += "legend:";
                    script += "{";
                    script += "orient: 'vertical',";
                    script += "x: 'left',";
                    script += "data:[";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        script += ("&quot;" + dt.Rows[i]["AccountTitle"].ToString() + "&quot;");
                        if (i < (dt.Rows.Count - 1))
                        {
                            script += ',';
                        }

                    }

                    script += "]";
                    script += "},";
                    script += "series: [";
                    script += "{";
                    script += "name: 'Account',";
                    script += "type: 'pie',";
                    script += "radius: ['50%', '70%'],";
                    script += "avoidLabelOverlap: false,";
                    script += "label:";
                    script += "{";
                    script += "normal:";
                    script += "{";
                    script += "show: false,";
                    script += "position: 'center'";
                    script += "},";
                    script += "emphasis:";
                    script += "{";
                    script += "show: true,";
                    script += "textStyle:";
                    script += "{";
                    script += "fontSize: '30',";
                    script += "fontWeight: 'bold'";
                    script += "}";
                    script += "}";
                    script += "},";
                    script += "labelLine:";
                    script += "{";
                    script += "normal:";
                    script += "{";
                    script += "show: false";
                    script += "}";
                    script += "},";
                    script += "data:[";


                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        script += ("{value: " + dt.Rows[i]["balance"].ToString() + ", name:&quot;" + dt.Rows[i]["AccountTitle"].ToString() + "&quot;}");
                        if (i < (dt.Rows.Count - 1))
                        {
                            script += ',';
                        }
                    }

                    script += "]";
                    script += "}";
                    script += "]";
                    script += "}";
                    script += "\" style = \"height: 300px; \"></div>";

                    ltrAccounts.Text = script.ToString();
                }
            }

        }

        protected void checkPercentage_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                if (chkOther.Checked)
                {
                    txtMonth.Disabled = false;

                }
                else
                {
                    txtMonth.Disabled = true;
                }

                if (chkLastYear.Checked)
                {
                    generateAccountChart();
                   

                }
                else if (chkSixMonth.Checked)
                {
                    generateAccountChart("6");
                   

                }
                else if (chkThreeMonth.Checked)
                {
                    generateAccountChart("3");
                   

                }

            }
            catch (Exception ex)
            {
            }
        }

        protected void btnFilter2_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (chkOther.Checked)
                {
                    generateAccountChart(txtMonth.Value);
                  
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }
    }
}