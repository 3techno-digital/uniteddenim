﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tracking.aspx.cs" Inherits="Technofinancials.Finance.View.Tracking" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        .edit-class {
            border: 0px !important;
            background-color: transparent !important;
            color: #003780 !important;
            font-weight: 800 !important;
        }

        .table_out_data {
            margin: 10px 0 0 0;
        }

            .table_out_data input {
                float: none;
                width: 10%;
            }

        td label {
            margin-top: 0px;
        }

        .table_out_data label {
            display: inline-block;
            margin-top: -6px !important;
        }

        textarea#txtNotes {
            width: 565px !important;
            height: 101px !important;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sb2" runat="server" />
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">

            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Tracking</h1>
                            </div>
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-md-4">
                        </div>

                        <div class="col-md-4">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </div>
                      <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>From Date
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtFromDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>To Date
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtToDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                   
                                                                <div class="col-md-12">
                                    <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
                                </div>
                            </div>
                        </div>
                      <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                               
                                     <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee</h4>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2" data-plugin="select2">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                       
                            </div>
                        </div>
                    <div class="row ">
                        <asp:HiddenField ID="hdnIsRedirect" runat="server" />
                        <asp:HiddenField ID="hdnLinkRedirect" runat="server" />
                        <div class="col-sm-12">
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12 gv-overflow-scrool">
                                        <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No.">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="WDID">
                                                    <ItemTemplate>
                                                           <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("WDID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EmployeeName">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="Status" Text='<%# Eval("Status") %> '></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Latitude">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="Latitude" Text='<%# Eval("Latitude") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Longitude">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="Longitude" Text='<%# Eval("Longitude") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Date/Time">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="CreatedOn" Text='<%# Eval("CreatedOn") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                     
                                                        <button type="button" id="btnEditText<%# Eval("EmployeeID")%>" onclick="btnShowPopup('<%# Eval("Latitude") %>', '<%# Eval("Longitude") %>','<%# Eval("EmployeeName") %>')"  data-toggle="modal" data-target="#MapGrid" class="AD_stock">View</button>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                    </div>

                                </div>
                            </div>
                        </div>
                           
                 <div class="modal fade M_set" id="MapGrid" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="m-0 text-dark">Google Map <label id="MapOf" text=""></label></h1>
                                    <div class="add_new">
                                      
                                        <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                    </div>
                                </div>
                                <div class="modal-body">
                                         <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="row ">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                        <div class="col-sm-12">
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12">
                               
                                       <div id="GridView1" style="width: 100%; height: 300px;"></div>
                                    </div>
                                 </div>
                            </div>
                        </div>

                        <div class="clearfix">&nbsp;</div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="clearfix">&nbsp;</div>
                    </div>
                                </div>
                            </div>
                       
                                </div>
                            </div>
                        </div>
                    </div>

                        <div class="clearfix">&nbsp;</div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="clearfix">&nbsp;</div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
              </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
     
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBO7Sc7EMPYfEgZ9H9przSNgOtC4brvy4&libraries=places"></script>
        <script>
            var latitude = 0;
            var longitude = 0;
            function myMap() {
				$('#GridView1').empty();
				var mapProp = {
					center: new google.maps.LatLng(lat, long),
					zoom: 5,
				};
				var map = new google.maps.Map(document.getElementById("GridView1"), mapProp);
			}
            function btnShowPopup(lat, long, emp) {

				$('#GridView1').empty();
                $('#MapGrid').show();
                $('#MapOf').text(emp);
				var googleEmbebdedMap = '<iframe src="https://maps.google.com/maps?q=' + lat + ',' + long +'&z=15&output=embed" width="100%" height="300px" frameborder="0" style="border:0"></iframe>';
			

				$('#GridView1').append(googleEmbebdedMap);



				//$('#GridView1').empty();
    //            $('#MapGrid').show();
                
				//$('#MapOf').text(emp);
				//var latlng = new google.maps.LatLng(lat, long);
				//var map = new google.maps.Map(document.getElementById('GridView1'), {
				//	center: latlng,
				//	zoom: 13
				//});
				//var marker = new google.maps.Marker({
				//	map: map,
				//	position: latlng,
				//	draggable: false,
				//	anchorPoint: new google.maps.Point(0, -29)
				//});
				//var infowindow = new google.maps.InfoWindow();
				//google.maps.event.addListener(marker, 'click', function () {
				//	var iwContent = '<div id="iw_container">' +
				//		'<div class="iw_title"><b>Location</b> : Noida</div></div>';
				//	// including content to the infowindow
				//	infowindow.setContent(iwContent);
				//	// opening the infowindow in the current map and at the current marker location
				//	infowindow.open(map, marker);
				//});
			
				

				//google.maps.event.addDomListener(window, 'load', initialize);

			}

           


		</script>
    </form>
</body>
</html>

