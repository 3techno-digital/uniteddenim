﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GRN.aspx.cs" Inherits="Technofinancials.Finance.view.GRN" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
               <section class="app-content">
                      <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <img src="/assets/images/Admin_Purchase_Orders.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Goods Recieve Notes</h3>
                        </div>
                        <div class="col-sm-4">
                            <div class="pull-right flex">
                              <%--  <button class="tf-approved-btn" "Approved"><i class="far fa-thumbs-up"></i></button>
                                <button class="tf-back-btn" "Back"><i class="fas fa-arrow-left"></i></button>
                                <button class="tf-disapproved-btn" "Dispproved"><i class="far fa-thumbs-down"></i></button>
                                 <button class="tf-excel-btn" "Excel"></button>

                                <button class="tf-save-btn" "Save"><i class="far fa-save"></i></button>
                                <button class="tf-send-btn" "Send"><i class="fas fa-paper-plane"></i></button>
                                <button class="tf-upload-btn" "Upload"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                <button class="tf-view-btn" "View"><i class="far fa-eye"></i></button>
                                <button class="tf-add-btn" "Add"><i class="far fa-plus-circle"></i></button>--%>
                                <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/goods-receive-notes/add-new-goods-receive-notes"); %>" class="tf-add-btn" "Add"><i class="far fa-plus-circle"></i></a>
                                
                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
<%--                   <div class="pull-left">
                       <a href="/Procurement Management/manage/POs.aspx" class="btn btn-primary btn-add-items green-btn">Add PO</a>
                   </div>--%>
                    <%--<button type="button" class="btn btn-primary btn-add-items">Send Statements</button>--%>
                    
                         <%--<div class="pull-right">
                             <button type="button" class="btn btn-primary btn-add-items"><i class="fas fa-plus"></i>Import</button>
                    <button type="button" class="btn btn-primary btn-add-items"><i class="fas fa-angle-down"></i>Export</button>
                        </div>--%>
                </div>
                    <div>&nbsp;</div>
                    <div class="col-sm-12 table-09-main-div">
                        <div class="add-table-div">
                            <asp:GridView ID="gv" runat="server" CssClass="gv table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                <Columns>

                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="GRN Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPOCode" Text='<%# Eval("GRNCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               
                                    <asp:TemplateField HeaderText="Against PO Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPODate" Text='<%# Eval("POCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                
                                    <asp:TemplateField HeaderText="Sales Invoice No.">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblRFQCode" Text='<%# Eval("SalesInvoiceNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               
                                    <asp:TemplateField HeaderText="Supplier Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSupplierName" Text='<%# Eval("SupplierName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    
                                    <%--<asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPOAmount" Text='<%# Eval("POAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Prepared Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblUserCommeasdfasfasnts" Text='<%#  Eval("PreparedDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblStauss" Text='<%# Eval("DocStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Generate Report">
                                                <ItemTemplate>
                                                      <a id="lnkGenerateReport" runat="server" href='<%# "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/finance/manage/reports/generate-grn-report-" + Eval("GRNID") %>' >Generate</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lnkCommand" CommandArgument='<%# Eval("GRNID") %>' OnClick="lnkCommand_Click"> View</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            
                        </div>
                        <%--<div class="col-sm-12">
                        <a href="/cpo/manage/items/add-new-item" class="btn btn-primary btn-add-items-09 pull-right"><i class="fas fa-plus"></i>New Item</a>
                    </div>--%>
                    </div>

                </div>
            </div>
        </section>
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
</body>
    <style>
        .add-table-div {
    margin-bottom: 60px;
}

        .tf-add-btn {
   
    padding: 7px 11px !important;
  
}
    </style>
</html>
