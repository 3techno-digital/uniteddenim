﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.view
{
    public partial class IT3Approval : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    
                    //BindDropdown();
                    GetData();
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        void BindDropdown()
        {
            //ddlAdjustmentStatus.DataSource = objDB.GetIT3AdjustmentStatus(ref errorMsg);
            //ddlAdjustmentStatus.DataTextField = "AdjustmentStatus";
            //ddlAdjustmentStatus.DataValueField = "AdjustmentStatus";
            //ddlAdjustmentStatus.DataBind();
            //ddlAdjustmentStatus.Items.Insert(0, new ListItem("ALL", "0"));

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            //lstEmployee.DataSource = objDB.GetAllEmployeesByCompanyID(ref errorMsg);
            //lstEmployee.DataTextField = "EmployeeName";
            //lstEmployee.DataValueField = "EmployeeID";
            //lstEmployee.DataBind();
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void GetData()
        {
            CheckSessions();

            DataTable dt = new DataTable();
            //objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.Month = subdate.Text;
            dt = objDB.GetIT3_SubmissionForApproval(ref errorMsg);

            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

            // Common.addlog("ViewAll", "HR", "All Employees Viewed", "Employees");

        }
   //     private void GetData()
   //     {
   //         CheckSessions();
   //         btnApprove.Visible = btnDisapprove.Visible = true;
   //         DataTable dt = new DataTable();
   //         //objDB.Status = ddlAdjustmentStatus.SelectedValue;
   //         List<string> lstEmpIds = new List<string>();

			//foreach (ListItem item in lstEmployee.Items)
			//{
			//	if (item.Selected)
			//	{
			//		lstEmpIds.Add(item.Value);
			//	}
			//}

			//dt = objDB.GetAllIT3Forms(lstEmpIds, ref errorMsg);
   //         gv.DataSource = dt;
   //         gv.DataBind();
   //         if (dt != null)
   //         {
   //             if (dt.Rows.Count > 0)
   //             {
   //                 gv.UseAccessibleHeader = true;
   //                 gv.HeaderRow.TableSection = TableRowSection.TableHeader;
   //             }
   //         }
   //     }
        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetData();
            }
            catch (Exception ex)
            {
            }
        }

        protected void chckchanged(object sender, EventArgs e)

        {
            btnApprove.Visible = btnDisapprove.Visible = true;
            CheckBox chckheader = (CheckBox)gv.HeaderRow.FindControl("checkAll");

            foreach (GridViewRow row in gv.Rows)
            {
                if (((Label)row.FindControl("lblDocStatus")).Text == "Submitted" || ((Label)row.FindControl("lblDocStatus")).Text == "Saved as Draft")
                {
                    ((CheckBox)row.FindControl("check")).Checked = chckheader.Checked;
                }
            }
        }
        protected void btnApprove_ServerClick(object sender, EventArgs e)
        {
            btnApprove.Visible = true;
            CheckSessions();
            string alertMsg = "";
            int recordsCount = 0;
            List<string> IT3IDs = new List<string>();
            foreach (GridViewRow gvr in gv.Rows)
            {
                if (((CheckBox)gvr.FindControl("check")).Checked)
                {

                    string employeeID = ((Label)gvr.FindControl("lblEmployeeID")).Text;
                    string IT3ID = Convert.ToString(((Label)gvr.FindControl("IT3ID")).Text);

                    IT3IDs.Add(IT3ID);
                    recordsCount++;

                }
            }
            objDB.ApprovedBy = Session["UserName"].ToString();
            objDB.Status = "Approved";
            objDB.UpdateIT3Status(IT3IDs, ref errorMsg);

            GetData();
            alertMsg = recordsCount > 0 ? "" + recordsCount + " Records Updated Successfully" : "No Records Updated ";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + alertMsg + "')", true);
        }

        protected void btnDisApprove_ServerClick(object sender, EventArgs e)
        {
            btnApprove.Visible = true;
            CheckSessions();
            string alertMsg = "";
            int recordsCount = 0;
            List<string> IT3IDs = new List<string>();
            foreach (GridViewRow gvr in gv.Rows)
            {
                if (((CheckBox)gvr.FindControl("check")).Checked)
                {
                    string employeeID = ((Label)gvr.FindControl("lblEmployeeID")).Text;
                    string IT3ID = Convert.ToString(((Label)gvr.FindControl("IT3ID")).Text);

                    IT3IDs.Add(IT3ID);
                    recordsCount++;
                }
            }
            objDB.ApprovedBy = Session["UserName"].ToString();
            objDB.Status = "Rejected";
            objDB.UpdateIT3Status(IT3IDs, ref errorMsg);
            GetData();
            alertMsg = recordsCount > 0 ? "" + recordsCount + " Records Rejected Successfully" : "No Records Updated ";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + alertMsg + "')", true);
        }
    }
}