﻿using System;
using System.Data;
using System.IO;

using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.view
{
    public partial class Teams : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        string filePath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            hdnIsRedirect.Value = "0";
            if (!Page.IsPostBack)
            {
                //GetData();
                GetDirectReportingPesons();
                divAlertMsg.Visible = false;
            }
            
        }

     
        private DataTable FilterData(DataTable dt)
        {
           if (dt == null)
            {
                return dt;
            }
            DataTable dtFilter = new DataTable();

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.DocName = "EmployeeLists";

            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");

                    if (dtTemp != null)
                        dtFilter.Merge(dtTemp);

                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }

                }
            }

            return dtFilter;
        }

        private void GetData()
        {
            CheckSessions();

            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetAllEmployeesByCompanyID(ref errorMsg);

            gv.DataSource = dt;// FilterData(dt);
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

           
            Common.addlog("ViewAll", "HR", "All Employees Viewed", "Employees");

        }
        private void GetDirectReportingPesons()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            dt = objDB.GetDirectReportingPerson(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.DataSource = dt;
                    gv.DataBind();
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                   


                }
            }



            Common.addlog("ViewAll", "HR", "All Reporting Persons Viewed", "Teams");

        }
    private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

    
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            int ID = Convert.ToInt32(btn.CommandArgument);
            objDB.CandidateID = ID;
            objDB.DeletedBy = Session["UserName"].ToString();
            objDB.DeleteEmployeeByID();
            GetData();
        }

      
    }
}