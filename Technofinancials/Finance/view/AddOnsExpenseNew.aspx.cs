﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.view
{
	public partial class AddOnsExpenseNew : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int AddOnsExpenseID
        {
            get
            {
                if (ViewState["AddOnsExpenseID"] != null)
                {
                    return (int)ViewState["AddOnsExpenseID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AddOnsExpenseID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                GetData();

            }
        }
        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                //DateTime fdate = DateTime.Parse(txtFromDate.Text);
                //DateTime tdate = DateTime.Parse(txtToDate.Text);
                //if (fdate > tdate)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('From Date should not be greater than To Date')", true);
                //    return;
                //}

               
                GetData();
            }
            catch (Exception ex)
            {
            }
        }

        private void GetData()
        {

            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.FromDate = "01-" + txtPayrollDate.Text;
            objDB.ToDate = "01-" + txtPayrollDate.Text;
            dt = objDB.GetAllAddOnsExpenseByCompanyID(ref errorMsg);
            gvAddOns.DataSource = FilterData(dt);
            gvAddOns.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvAddOns.UseAccessibleHeader = true;
                    gvAddOns.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All Currency Viewed", "Currency");

        }


        private DataTable FilterData(DataTable dt)
        {
            DataTable dtFilter = new DataTable();

            if (dt == null)
            {
                return dt;
            }

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.DocName = "PayRoll";

            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");

                    if (dtTemp != null)
                        dtFilter.Merge(dtTemp);

                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }

                }
            }

            return dtFilter;
        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            int ID = Convert.ToInt32(btn.CommandArgument);
            objDB.AddOnsExpenseID = ID;
            objDB.DeletedBy = Session["UserName"].ToString();

            objDB.DeleteAddOnsExpenseByID();
            GetData();
        }

    }
}