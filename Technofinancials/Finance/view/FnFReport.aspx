﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FnFReport.aspx.cs" Inherits="Technofinancials.Finance.view.FnFReport" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

   <title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
 <style>
 
        .B_class {
            padding: 20px;
            border: 1px solid #000;
            background: #fff;
        }
        .B_class h1,
        .B_class h2,
        .B_class h3,
        .B_class h4,
        .B_class p,
        .B_class td {
            margin: 0;
            font-family: lato;
            color: #000;
        }
        .B_class table {
            border: 1px solid;
            margin-top: -25px;
        }
        .B_class table td,
        .B_class table th {
            padding: 3.4px;
        }
        .B_class .slip-table thead tr th {
            background-color: #ffffff;
            border-bottom: 1px solid #000000 !important;
            border-top: 1px solid #000000 !important;
            color: #000;
            padding: 0 10px;
        }
        .slip-table-bordered thead tr th,
        .slip-table-bordered thead tr td,
        .slip-table-bordered tbody tr th,
        .slip-table-bordered tbody tr td {
            border-top: 1px solid transparent !important;
            border: 0;
            vertical-align: middle;
            padding: 0.1rem;
            font-size: 14px;
        }
        .slip-table thead tr th,
        .slip-table thead tr td,
        .slip-table tbody tr th,
        .slip-table tbody tr td,
        .slip-table tfoot tr th,
        .slip-table tfoot tr td {
            height: 32px;
            line-height: 1.428571429;
            vertical-align: middle;
            border-top: 1px solid #ddd;
            padding: 0 10px;
        }
        .slip-table {
            width: 100%;
            max-width: 100%;
            background-color: transparent;
            border-collapse: collapse;
            border-spacing: 0;
        }
        .slip-table-bordered tfoot tr th,
        .slip-table-bordered tfoot tr td {
            border-top: 1px solid #000 !important;
            border: 0;
            vertical-align: middle;
            padding: 0.1rem;
            font-size: 14px;
            padding: 0 10px;
        }
        .B_class .slip-table thead tr th {
            text-align: right;
        }
        .B_class .slip-table thead tr th:nth-child(1) {
            text-align: left;
        }
        .B_class .slip-table tbody tr td {
            text-align: right;
            height: 28px;
        }
        .B_class .slip-table tbody tr td:nth-child(1) {
            text-align: left;
        }
        .B_class .slip-table tfoot tr td {
            text-align: right;
            height: 34px;
        }
        .B_class .slip-table tfoot tr td:nth-child(1) {
            text-align: left;
            width: 200px !important;
        }
        .B_class hr {
            margin-top: 20px;
            margin-bottom: 10px;
            border: 0;
            border-top: 1px solid #000000;
        }
        .tableP {
            height: 34px;
            padding: 6.4px;
            border: 1px solid;
            margin-top: -6px !important;
            width: 99.9%;
        }
        .tableP span {
            margin-left: 40px;
            text-transform: capitalize;
        }
        .AddressSide {
            height: 53px;
            color: #000;
            text-align: center;
        }
        .AddressSide h1 {
            font-size: 18px;
            text-transform: capitalize;
            font-weight: 700;
            margin: 5px 0;
            color: #000;
        }
        .AddressSide h2 {
            font-size: 18px;
            text-align: right;
            font-weight: 400;
            color: #000;
            margin: 5px 0;
        }
        .AddressSide h3 {
            font-size: 18px;
            text-transform: capitalize;
            font-weight: 700;
            margin: 5px 0;
            color: #000;
        }
        .AddressSide h4 {
            font-size: 18px;
            text-transform: capitalize;
            font-weight: 700;
            margin-bottom: 15px;
            color: #000;
            margin: 5px 0;
        }
        .AddressSide p {
            font-size: 16px;
            font-weight: 500;
            max-width: 350px;
            text-transform: capitalize;
            color: #000;
        }
        .bigBox {
            width: 100%;
            border: 1px solid #000;
            height: 400px;
            margin: 50px 0 20px;
        }
        .Box01 {
            width: 45.5%;
            display: inline-block;
            padding: 10px;
            color: #000;
        }
        .Box02 {
            width: 46%;
            display: inline-block;
            padding: 10px 20px;
            margin-left: -5px;
            color: #000;
        }
        .Box03 {
            width: 50%;
            display: inline-block;
        }
        .Box04 {
            width: 50%;
            margin-left: -5px;
            display: inline-block;
        }
        .Box05 {
            width: 100%;
            display: block;
        }
        .OneLine {
            width: 100%;
            height: 28px;
        }
        .OneLine h3 {
            display: inline-block;
            font-size: 16px;
            font-weight: 700;
            width: 35%;
            margin: 3px 0;
            color: #000;
        }
        .OneLine p {
            display: inline-block;
            font-size: 16px;
            width: 60%;
            margin: 3px 0;
            color: #000;
        }
        .totalSalaries {
            font-weight: bold !important;
            color: #188ae2 !important;
            font-size: 16px !important;
        }
        .line-four {
            width: 99.9%;
            display: block;
        }
        #tblPDF td {
            border: 1px solid;
            border-right: none;
            border-top: none;
        }
    </style>
    
	
	<style>
        .scroller{
            overflow-x: scroll !important;
   
        }
        th {
            position: sticky;
            top: 0; /* Don't forget this, required for the stickiness */
            background: #f7f7f7;
            color: #343a40;
        }

        .col-lg-4.col-md-6.col-sm-12 button#btnView {
            padding: 3px 24px;
            margin-top: 3px !important;
        }
     
        

        .green-background {
            background-color: rgb(152, 251, 152);
            color: rgb(255, 255, 255);
            font-weight: 600;
        }

        div#myModal2 {
            background-color: #3b3e4796 !important;
        }

        .modal-dialog {
            z-index: 9999;
            opacity: 1;
        }

        #modalbtnSave {
            cursor: pointer;
        }

        .modal-open {
            overflow: hidden;
        }

        .AD_btnn.two {
            font-size: 14px !important;
            margin: 0 !important;
            padding: 0 !important;
            cursor: pointer;
        }

        .AD_btnn {
            margin: 0 10px;
            font-size: 18px;
            font-weight: 700;
            color: #003780;
            background: none;
            border: 0;
            border-bottom: 2px solid transparent !important;
            border-radius: 0;
            padding: 5px;
        }

            .AD_btnn:hover {
                color: #000;
                border-bottom: 2px solid #000 !important;
            }

        .SaveBTn {
            padding: 20px 30px !important;
            margin: 0px !important;
        }

        div#myModal2 .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
            line-height: 1.3 !important;
        }

        .Time-in h4, .Time-out h4, .Breaks h4, .BreakIN h4, .Breakout h4, .Rmarks h4 {
            color: #000;
            font-weight: 600;
            margin: 10px 0 5px;
            text-align: left;
        }

        .Time-in, .Time-out, .Breaks, .BreakIN, .Breakout, .Rmarks {
            padding: 0px 15px;
            text-align: left;
        }

        .content-header h1 {
            margin-left: 8px !important;
        }

        #chartdiv {
            width: 100%;
            height: 500px;
        }

        .color_infoP h3 {
            font-size: 15px;
            margin: 0 !important;
        }

        .attendance-wrapper {
            text-align: right;
        }

        .color_infoP h3 span {
            margin-left: 15px;
        }

        .pr_0 {
            padding-right: 0 !important;
        }

        input#txtStartDate, input#txtEndDate {
            border-radius: 5px;
        }

        .form-group {
            display: block;
        }

        div#myModal .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
        }

        #LeavesGuage {
            width: 100%;
            height: 500px;
        }

        .table-bordered > tbody > tr > th {
            border-top-style: none !important;
        }

        #PieChart {
            width: 100%;
            height: 500px;
        }

        #LeavesPieChart {
            width: 100%;
            height: 500px;
        }

        .widget-body {
            padding-top: 0;
        }

        .calender {
        }

        .calenderHeading {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .form-group.color_infoP {
            width: fit-content;
            display: inline-block;
            font-size: 16px;
        }

        .content-header h1 {
            font-size: 20px !important;
        }

        .calenderCell {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .wrap.p-t-0.tf-navbar.tf-footer-wrapper {
            right: 0 !important;
        }

        g[aria-labelledby="id-66-title"] {
            display: none;
        }

        div#myModal .modal-content {
            top: 122px;
            width: 20%;
            position: fixed;
            z-index: 1050;
            left: 80%;
            height: 70%;
            box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
        }

        .modal-content .content-header {
            border-top-left-radius: 11px;
            border-top-right-radius: 11px;
            padding-top: 10px;
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
                left: 70%;
                width: 30%;
            }

            table#gvAttendanceSummary {
                width: 991px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
                left: 70%;
                width: 30%;
            }

            table#gvAttendanceSummary {
                width: 1024px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }

            table#gvAttendanceSummary {
                width: 1024px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }
        }

        @media only screen and (max-width: 1439px) and (min-width: 1200px) {
            .single-key h3 {
                font-size: 15px;
                padding: 5px;
            }
        }

        @media only screen and (max-width: 1505px) and (min-width: 1440px) {
            .single-key h3 {
                font-size: 18px;
            }
        }

        .mini-stat.clearfix.present {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #1fb5ac !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            margin-top: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.absent {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #fa8564 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.leave {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #a48ad4 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.holiday {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f4b9b9 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.missing {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f9c851 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.short {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #aec785 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat-info {
            font-size: 12px;
            padding-top: 2px;
            color: #767676 !important;
            font-weight: 700;
        }

            .mini-stat-info span {
                display: block;
                font-size: 24px;
                font-weight: 600;
                color: #767676 !important;
            }



        .mini-stat {
            border-radius: 10px !important;
        }

        .btnAdjustmentTrue {
            display: none;
        }

       /* .gv-overflow-scrool div {
            overflow-x: auto;
            overflow-y: auto;
            height: 450px;
            padding-right: 5px;
        }*/

        .AD_btn {
            font-size: 16px;
            border: 0;
            padding: 0px;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">

            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">FnF Report</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div style="text-align: right;">
                                    <button class="AD_btn" id="btnApproveByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Paycycle Lock</button>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                    <button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Dis Approve</button>
                                    <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                       <button class="AD_btn" id="btnView" runat="server" onserverclick="btnPDF_ServerClick" type="button">PDF</button>
                                    <a class="AD_btn" runat="server" id="sendemail" href=".." target="_blank">Email</a>
                                    <h4>&nbsp;</h4>
                                    <button style="display:none;float: right;" class="AD_btn" id="btnRelease"  onclick="ReleaseFnF()" type="button">Disburse</button>
                                
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="pull-right flex">
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade M_set" id="notes-modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="m-0 text-dark">Notes</h1>
                                        <div class="add_new">
                                            <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                            <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                        </p>
                                        <p>
                                            <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes..." class="form-control"></textarea>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
								<div class="col-md-3">
                                    <h4>&nbsp;</h4>
                                
                                </div>
                             
                      
                                	
                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <h4>  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPayrollDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />

                                    </div>
                                </div>

                                
                            
								<div class="col-sm-4">
								
							</div>


                            </div>


                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12"></div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12 gv-overflow-scrool">
                                        <asp:HiddenField ID="hdnRowNo" runat="server" />
                                        <asp:GridView ID="gvSalaries" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" ShowFooter="true" AutoGenerateColumns="false"  ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr. No">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex + 1%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Code">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Salary">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeSalary" Text='<%# Eval("EmployeeSalary","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Over Time">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblOverTime" Text='<%# Eval("OverTimeAmount","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Commission">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Spiffs">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Bonuses">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Leave Encashment">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("LeaveEncashment","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="PF Contribution">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("PFContribution","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Gratuity">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("Graduity","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Other Expenses">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblOtherExpenses" Text='<%# Eval("OtherExpenses","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Gross Salary">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("GrossSalary","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="Number" ID="txtEditBasicSalary" CssClass="form-control" Text='<%# Eval("GrossSalary") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Deductions">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("Deductions","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Absent Amount">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblAbsentAmount" Text='<%# Eval("AbsentAmount","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Tax">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Tax","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="Number" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("Tax") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Variable Tax">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTax" Text='<%# Eval("AdditionalTax","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="Number" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("AdditionalTax") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="E.O.B.I">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEOBI" Text='<%# Eval("EOBI","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="Number" ID="txtEditEOBI" CssClass="form-control" Text='<%# Eval("EOBI") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Provident Fund">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeePF" Text='<%# Eval("EmployeePF","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="Number" ID="txtEditEmployeePF" CssClass="form-control" Text='<%# Eval("EmployeePF") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ClearanceDeduction">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("ClearanceDeductionAmount","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Total Deduction">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lbltotalded" Text='<%# Eval("TotalDeduction","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Other Deductions">
                                                        <ItemTemplate>  
                        <asp:TextBox runat="server" TextMode="Number" ID="txtotherded" CssClass="form-control" Text='<%# Eval("OtherDeductionsInput") %>'></asp:TextBox>
                                      
             
                    </ItemTemplate>  
                  
                             
                                    
                                      
                                    </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Net Amount">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("NetAmount","{0:N}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                             <asp:TemplateField>  
                    <ItemTemplate>  
                        <asp:LinkButton ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" OnClick="RowUpdating" />  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update"/>  
                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel"/>  
                    </EditItemTemplate>  
                </asp:TemplateField>  
             
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row" style="display: none;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <asp:GridView ID="gvNormalSal" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("EmployeeSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Over Time Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTimeAmount" Text='<%# Eval("OverTimeAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Commission">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Spiffs">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Bonuses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Leave Encashment">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PF Contribution">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Graduity">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Other Expenses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOtherExpenses" Text='<%# Eval("OtherExpenses","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Gross Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblGrossSallary" Text='<%# Eval("GrossSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Deductions">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("Deductions","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Absent Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAbsentAmount" Text='<%# Eval("AbsentAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Tax","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="E.O.B.I">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEOBI" Text='<%# Eval("EOBI","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Provident Fund">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeePF" Text='<%# Eval("EmployeePF","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ClearanceDeduction">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("ClearanceDeductionAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("NetAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
         <%--           <table id="table" class="table table-bordered gv">
    <tr>
      
       <th><input type="checkbox" onclick="checkall()" id="checkall"/></th> <th>WDID</th><th>EmployeeName</th><th>Department</th><th>Separation date</th><th>PFContribution</th><th>Gratuity</th><th>ClearanceDeductionAmount</th><th>AdonsDeductions</th><th>AdditionalTaxperMonth</th><th>TotalDeduction</th><th>NetAmount</th><th>Action</th>
      
    </tr>
</table>--%>
                   
                </section>

            </div>
	
            	<div class="container-fuild" id="slip"  runat="server">
						<div>

							<div class="B_class" id="ReportPayroll" runat="server">
								<img id="hdnCompanyLogo"  runat="server" src="/assets/images/abs-labs-pvt-limited_32002.png" alt="Company Logo" style="width: 46px; position: absolute;" />
								<div class="AddressSide">
									<h1 runat="server" id="hdnCompanyName"></h1>
									<h3>Full and Final Settlement</h3>
								
								</div>
								<div style="width: 100%; display: block; border: 1px solid #000;">
									<div class="">
										<div class="Box01 col-md-6 col-lg-6 col-sm-6">
											<div class="OneLine">
												<h3>Employee Code :</h3>
												<asp:Label ID="employeecode" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Location :</h3>
												<asp:Label ID="location" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Designation :</h3>
												<asp:Label ID="designation" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Section :</h3>
												<asp:Label ID="Section" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Date of Joining :</h3>
												<asp:Label ID="DateofJoining" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Address :</h3>
												<asp:Label ID="Address" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Bank :</h3>
												<asp:Label ID="Bank" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Branch Name :</h3>
												<asp:Label ID="branch" runat="server"></asp:Label>
											</div>
											
											<div class="OneLine">
												<h3>NTN :</h3>
												<asp:Label ID="NTN" runat="server"></asp:Label>
											</div><div class="OneLine">
												<h3>Leave :</h3>
												<asp:Label ID="Leave" runat="server"></asp:Label>
											</div><div class="OneLine">
												<h3>Date of Resign :</h3>
												<asp:Label ID="Resign" runat="server"></asp:Label>
											</div><div class="OneLine">
												<h3>Notice Days :</h3>
												<asp:Label ID="NoticeDays" runat="server"></asp:Label>
											</div>
										</div>
										<div class="Box02 col-md-6 col-lg-6 col-sm-6">
											<div class="OneLine">
												<h3>Name :</h3>

												<asp:Label ID="Employeename" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Department :</h3>
												<asp:Label ID="department" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Employee Type :</h3>
												<asp:Label ID="EmploymentType" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Grade :</h3>
												<asp:Label ID="Grade" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Date of Confirmation :</h3>
												<asp:Label ID="DateofConfirmation" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>DOB :</h3>
												<asp:Label ID="DOB" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Account no# :</h3>
												<asp:Label ID="AccountNo" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>CNIC :</h3>
												<asp:Label ID="CNIC" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Shift :</h3>
												<asp:Label ID="Shift" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Standard Gross :</h3>
												<asp:Label ID="StandardGross" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Last Working Day:</h3>
												<asp:Label ID="lastworkingday" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Tenure :</h3>
												<asp:Label ID="Tenure" runat="server"></asp:Label>
											</div>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
								<div width="100%">
									<div class="">
											<div class="Box03" style="width: 50%; float: left;">
											<div class="table-1">
													<table style="background-color: #c0c0c0; width:100%;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Benefit(s) & Allowance(s)</td>
														</tr>
													</tbody>
												</table>
											<table style="margin-top: 0px; border-top: none; width:100%;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Basic Salary</td>
															<td></td>
															<td>
																<asp:Label ID="BasicSalary" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Commission</td>
															<td></td>
															<td>
																<asp:Label ID="commission" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Bonus</td>
															<td></td>
															<td>
																<asp:Label ID="otherbonuses" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">House Rent Allowance</td>
															<td></td>
															<td><asp:Label ID="HouseRent" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Medical Allowance</td>
															<td></td>
															<td><asp:Label ID="MedicalAllowance" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Utility Allowance</td>
															<td></td>
															<td>
																<asp:Label ID="UtilityAllowance" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Leave Encashment</td>
															<td></td>
															<td>
																<asp:Label ID="leaveencashment" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Expense Reimbursement</td>
															<td></td>
															<td>
																<asp:Label ID="otherexpense" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Eid Bonus</td>
															<td></td>
															<td>
																<asp:Label ID="EidBonus" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Spiffs</td>
															<td></td>
															<td>
																<asp:Label ID="spiffs" runat="server"></asp:Label></td>
														</tr>
                                                        	<tr>
															<td style="font-weight: bold;">Severance</td>
															<td></td>
															<td>
																<asp:Label ID="Severance" runat="server"></asp:Label></td>
														</tr>
                                                        <tr>
															<td style="font-weight: bold;">Arrears</td>
															<td></td>
															<td>
																<asp:Label ID="Arrears" runat="server"></asp:Label></td>
														</tr>
                                                         <tr>
															<td style="font-weight: bold;">Paid leaves</td>
															<td></td>
															<td>
																<asp:Label ID="Paidleaves" runat="server"></asp:Label></td>
														</tr>
														
															<tr>
															<td style="font-weight: bold;">Overtime General+Weekend+Holiday</td>
															<td></td>
															<td>
																<asp:Label ID="Overtime" runat="server"></asp:Label></td>
														</tr>
															<tr>
															<td style="font-weight: bold;">Gratuity</td>
															<td></td>
															<td>
																<asp:Label ID="gratuity" runat="server"></asp:Label></td>
														</tr>
															<tr>
															<td style="font-weight: bold;">PF Contribution</td>
															<td></td>
															<td>
																<asp:Label ID="Contribution" runat="server"></asp:Label></td>
														</tr>
														
                                                       
                                                         <tr>
															<td style="font-weight: bold;">Tax Credit</td>
															<td></td>
															<td>
																<asp:Label ID="taxcredit" runat="server"></asp:Label></td>
														</tr>

													</tbody>
												</table>
											</div>
                                             	<div class="clearfix">&nbsp;</div>   
										<div class="table-1">
												<table style="background-color: #c0c0c0;width:100%;">
													<tbody>
														<tr>
															<td style="font-weight: bold;"></td>
														</tr>
													</tbody>
												</table>
													<table style="margin-top: 0px; border-top: none; width:100%;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Total</td>
																		<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td>
																<asp:Label ID="SumofBenefitsnAllowances" runat="server"></asp:Label></td>
														</tr>
														
														
													</tbody>
												</table>
											</div>
										
										</div>
									</div>
									
									
								

									<div class="">
										<div class="Box04" style="width: 50%; float: right;">
											<div class="table-1">
												<table style="background-color: #c0c0c0;width:100%;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Deduction(s)</td>
														</tr>
													</tbody>
												</table>
													<table style="margin-top: 0px; border-top: none;width:100%;">
													<tbody>
                                                        <tr>
															<td style="font-weight: bold;">Absent Deduction</td>
															<td></td>
															<td>
																<asp:Label ID="AbsentAmount" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Income Tax</td>
															<td></td>
															<td>
																<asp:Label ID="IncomeTax" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Additional Income Tax</td>
															<td></td>
															<td>
																<asp:Label ID="AdditionalTaxpermonth" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">EOBI Deduction</td>
															<td></td>
															<td>
																<asp:Label ID="EOBI" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">EOBI Arrears</td>
															<td></td>
															<td><asp:Label ID="EOBIArrears" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">PF Deduction </td>
															<td></td>
															<td><asp:Label ID="PFDeduction" runat="server"></asp:Label></td>
														</tr>
                                                        <tr>
															<td style="font-weight: bold;">Advance </td>
															<td></td>
															<td><asp:Label ID="advance" runat="server"></asp:Label></td>
														</tr>
                                                         <tr>
															<td style="font-weight: bold;">Other(s)</td>
															<td></td>
															<td>
																<asp:Label ID="otherdeduction" runat="server"></asp:Label></td>
														</tr>
													
													</tbody>
												</table>
                                                <div class="clearfix">&nbsp;</div>
												<div class="table-1">
												<table style="background-color: #c0c0c0;width:100%;">
													<tbody>
														<tr>
															<td style="font-weight: bold;"></td>
														</tr>
													</tbody>
												</table>
											<table style="margin-top: 0px; border-top: none;width:100%;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Total</td>
														<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
															<td></td><td></td>
															<td>
																<asp:Label ID="SumOfDeductions" runat="server"></asp:Label></td>
														</tr>
															<tr>
															<td style="font-weight: bold;"></td>
															<td></td>
															<td>
																<span></span></td>
														</tr>
														<tr>
															<td style="font-weight: bold;"></td>
															<td></td>
															<td>
																<span></span></td>
														</tr><tr>
															<td style="font-weight: bold;"></td>
															<td></td>
															<td>
																<span></span></td>
														</tr>
													</tbody>
												</table>
											</div>
										
											</div>
										</div>
									</div>
                                    <br />
									<div class="">
										<div class="Box07"  style="width: 50%; float: right;">
											<div class="table-1">
													<table style="background-color: #c0c0c0;width:100%;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Deductions From Clearance</td>
														</tr>
													</tbody>
												</table>
                                                 
												  <br />
												  <asp:GridView Width="100%" ID="GridView1" runat="server"  ClientIDMode="Static" AutoGenerateColumns="false" >

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("itemname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Deduct By">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("deductingDept") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("Itemamount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                            

                                    

                                </Columns>
                            </asp:GridView>
											</div>
										</div>
									</div>
										<div class="">
                                             <br />
										<div class="Box07"  style="width: 50%; float: right;">
											<div class="table-1">
                                                <br />
												<table style="background-color: #c0c0c0;width:100%;">
													<tbody>
														<tr>
															<td style="font-weight: bold;"></td>
														</tr>
													</tbody>
												</table>
												<table style="margin-top: 0px; border-top: none;width:100%;">
													<tbody>
                                                       
                                                         <tr>
															<td style="font-weight: bold;"></td>
															<td></td>
															<td>
																<span></span></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Total</td>
																<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
															<td>
																<asp:Label ID="clearanceDeduction" runat="server"></asp:Label></td>
														</tr>
														
														
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="">
										<div class="Box07" style="width: 50%; float: right; ">
                                            <br /> <br /> <br />
											<div class="table-1">
													<table  style="width: 100%; border:none;">
													<tbody>
														<tr>
															<td style="font-weight: bold; width: 25%;">Net Amount</td>
															<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
															<td></td>
															<td>
																<asp:Label ID="Net" runat="server"></asp:Label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="">
										<div class="Box07" style="width: 50%; float: right;">
                                            	<br/>
											<div class="table-1">
													<table style="width: 100%; border:none;">
													<tbody>
														<tr>
															<td style="font-weight: bold; width: 25%;">Amount in Words</td>
															<td>
																<asp:Label ID="AmountWrds" runat="server"></asp:Label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>

                                    </div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div>
										<p style="text-align: center;">Techno Financials - This is auto generated pay slip and needs no signature.</p>
									</div>
								</div>
							</div>
							<style type="text/css">
								
							</style>
							<div class="table-1 B_class" id="ReportPayrollFotter" runat="server">
								<table>
									<tbody>
										<tr>
											<td><asp:Label runat="server" ID="hdnCompanyNamefooter"> </asp:Label> - Human Capital Management</td>
											<td>© <asp:Label runat="server" ID="hdnCompanyNamecopyrights"> </asp:Label></td>
											<td>
												<asp:Label ID="Datetime" runat="server"></asp:Label></td>
											<td style="display: none;">USER:
												<asp:Label ID="username" runat="server"></asp:Label></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>


			<div id="elementH"></div>

            <!-- #dash-content -->
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
               <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

		<!-- jQuery library -->


<!-- jsPDF library -->
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>--%>

        <script>
				//	$("#pdf").click(function () {
				//		var pdf = new jsPDF("p", "mm", "a4");
					

				//		var width = pdf.internal.pageSize.getWidth();
				//		var height = pdf.internal.pageSize.getHeight();
				//// source can be HTML-formatted string, or a reference
				//// to an actual DOM element from which the text will be scraped.
				//source = $('#slip')[0];

				//// we support special element handlers. Register them with jQuery-style 
				//// ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
				//// There is no support for any other type of selectors 
				//// (class, of compound) at this time.
				//specialElementHandlers = {
				//	// element with id of "bypass" - jQuery style selector
				//	'#elementH': function (element, renderer) {
				//		// true = "handled elsewhere, bypass text extraction"
				//		return true
				//	}
				//};
				//margins = {
				//	top: 40,
				//	bottom: 60,
				//	left: 40,
				//	width: width
				//};
				//// all coords and widths are in jsPDF instance's declared units
				//// 'inches' in this case
				//pdf.fromHTML(
				//	source, // HTML string or DOM elem ref.
				//	margins.left, // x coord
				//	margins.top, { // y coord
				//	'width': margins.width, // max width of content on PDF
				//	'elementHandlers': specialElementHandlers
				//},

				//	function (dispose) {
				//		// dispose: object with X, Y of the last line add to the PDF 
				//		//          this allow the insertion of new lines after html
				//		pdf.save('Test.pdf');
				//	}, margins
				//);

			
				//	});


























			//$("#pdf").click(function () {
			//	var pdf = new jsPDF('p', 'pt', 'letter');
			//	// source can be HTML-formatted string, or a reference
			//	// to an actual DOM element from which the text will be scraped.
			//	source = $('#slip')[0];

			//	// we support special element handlers. Register them with jQuery-style 
			//	// ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
			//	// There is no support for any other type of selectors 
			//	// (class, of compound) at this time.
			//	specialElementHandlers = {
			//		// element with id of "bypass" - jQuery style selector
			//		'#elementH': function (element, renderer) {
			//			// true = "handled elsewhere, bypass text extraction"
			//			return true
			//		}
			//	};
			//	margins = {
			//		top: 80,
			//		bottom: 60,
			//		left: 40,
			//		/*width: 522*/
			//		width: 700
			//	};
			//	// all coords and widths are in jsPDF instance's declared units
			//	// 'inches' in this case
			//	pdf.fromHTML(
			//		source, // HTML string or DOM elem ref.
			//		margins.left, // x coord
			//		margins.top, { // y coord
			//		'width': margins.width, // max width of content on PDF
			//		'elementHandlers': specialElementHandlers
			//	},

			//		function (dispose) {
			//			// dispose: object with X, Y of the last line add to the PDF 
			//			//          this allow the insertion of new lines after html
			//			pdf.save('Test.pdf');
			//		}, margins
			//	);
			
			//			});


			//var pdf = new jsPDF('p', 'pt', 'a4');
			//$("#pdf").click(function () {
		
			//	window.html2canvas = html2canvas
			//	const doc = document.getElementsByTagName('div')[0];

			//		console.log("div is ");
			//		console.log(doc);
			//		console.log("hellowww");



			//		pdf.html(document.getElementById('slip'), {
			//			callback: function (pdf) {
			//				pdf.save('DOC.pdf');
			//			}
			//		})
				
				
			//		});
			//$("#pdf").click(function () {
			//	var doc = new jsPDF({
			//		orientation: 'portrait'
			//	});
			//var elementHTML = $('#slip').html();
			//var specialElementHandlers = {
			//	'#elementH': function (element, renderer) {
			//		return true;
			//	}
			//};
			//doc.fromHTML(elementHTML, 15, 15, {
			//	'width': 170,
			//	'elementHandlers': specialElementHandlers
			//});

			//// Save the PDF
			//doc.save('sample-document.pdf');
			//});

			//$("#pdf").click(function () {
			//	var htmlString = $("#slip").html();



				

				for (var i = 0; i < 2; i++) {
					$.ajax({
						type: "POST",
						url: '/business/services/EssService.asmx/FnFGeneratePDF',
						data: '{content: ' + JSON.stringify(htmlString) + '}',
						contentType: "application/octet-stream; charset=utf-8",
						dataType: "json",
						success: function (response) {


			//			},

			//			error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);  }
			//		});
			//	}
				

			//});

			function checkall(obj) {
                if ($(obj).prop("checked") == true) {
                    $('input:checkbox').attr('checked', 'checked');

                }
                else {
                    $('input:checkbox').removeAttr('checked');
                
				}
               

			}
		  function ReleaseFnF()
            {   
                array = [];
				$("input:checkbox").each(function () {
					var $this = $(this);

					if ($this.is(":checked")) {
						array.push($this.attr("id"));
					} else {
						
					}
                });

               
        
				console.log(array);
				$.ajax({
					type: "POST",
					url: '/business/services/EssService.asmx/ReleaseFnF',
					data: '{month: ' + JSON.stringify($('#txtPayrollDate').val()) + ', employeeIDs: ' + JSON.stringify(array) + '}',
                    contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response) {

                        GetFnFData();
                        alert('FnF Settled');
						

					},

					error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); alert(errorThrown) }
				});
			}

            function InsertClearanceDeduction(separationID, employeeid)
            {
				var inputdeduction = $("#inputDeduction_" + employeeid+"").val();
				var remarksdeduction = $("#RemarksDeduction_" + employeeid + "").val();
				$.ajax({
					type: "POST",
					url: '/business/services/EssService.asmx/InsertClearanceDeduction',
					data: '{employeeid: ' + JSON.stringify(employeeid) + ',amount: ' + JSON.stringify(inputdeduction) + ',remarks: ' + JSON.stringify(remarksdeduction) + ',sepID: ' + JSON.stringify(separationID) + '}',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response) {

						console.log(response.d);
                        GetFnFData();
                        alert('Updated');
						
					},

					error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
				});
            }
            var array = [];
            function checkme(employeeid) {
                var obj = document.getElementById(employeeid);

				if ($(obj).prop("checked") == true) {
					//array.push(employeeid);
				}
				else if ($(obj).prop("checked") == false) {

                    for (var i = 0; i < array.length; i++)
                    {
                        if (employeeid == array[i]) {
							//array.splice(i, 1);
						}
					}
                }
              //  console.log(array);

            }
          
		
			function GetFnFData() {

				$.ajax({
					type: "POST",
					url: '/business/services/EssService.asmx/GetFnFReport',
					data: '{month: ' + JSON.stringify($('#txtPayrollDate').val()) + '}',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response) {
					
                        console.log(response.d);
                        var data = response.d;
						var len = data.length;
						var txt = "";
                        if (len > 0) {
							for (var i = 0; i < len; i++) {
								

								txt += "<tr><td><input type=\"checkbox\" onclick=\"checkme(" + data[i].empid + ")\" id=\"" + data[i].empid+"\" /></td><td>" + data[i].wdid + "</td><td>" + data[i].employeename + "</td><td>" + data[i].deptName + "</td><td>" + data[i].EmployeeSeperationDate + "</td><td>" + data[i].PFContribution + "</td><td>" + data[i].Gratuity + "</td><td>" + data[i].ClearanceDeductionAmount + "</td><td>" + data[i].AdonsDeductions + "</td><td>" + data[i].AdditionalTaxperMonth + "</td><td>" + data[i].TotalDeduction + "</td><td><input id=\"inputDeduction_" + data[i].empid + "\" type=\"number\" /></td><td><input id=\"RemarksDeduction_" + data[i].empid +"\" type=\"text\" /></td><td>" + data[i].NetAmount + "</td><td><button onclick=\"InsertClearanceDeduction(" + data[i].SeperationDetailsID + "," + data[i].empid +")\" class=\"AD_btn\" id=\"" + data[i].SeperationDetailsID+"\"  type=\"button\">Update</button></td></tr>";

                            }
                            if (txt != "") {
                              
								$("#table").empty();
								var header = "<tr><th><input type=\"checkbox\" onclick=\"checkall(this)\" /></th><th> WDID</th><th>EmployeeName</th><th>Department</th><th>Separation date</th><th>PFContribution</th><th>Gratuity</th><th>ClearanceDeductionAmount</th><th>AdonsDeductions</th><th>AdditionalTaxperMonth</th><th>TotalDeduction</th><th>OtherDeductionsInput</th><th>Remarks</th><th>NetAmount</th><th>Action</th></tr>";
								$("#table").append(header);
                             
                                $("#table").append(txt);
                            }
                        }
					},
					
					error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
				});
			}

		</script>
    </form>
</body>
</html>
