﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace Technofinancials.Finance.view
{
    public partial class tax : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                GetAllUsers();
            }
        }

        private DataTable FilterData(DataTable dt)
        {
            DataTable dtFilter = new DataTable();

            if (dt == null)
            {
                return dt;
            }

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.DocName = "Tax";

            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");

                    if (dtTemp != null)
                        dtFilter.Merge(dtTemp);

                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }

                }
            }

            return dtFilter;
        }


        private void GetAllUsers()
        {
            CheckSessions();

            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetAllTaxRates(ref errorMsg);
            gv.DataSource = FilterData(dt);
            gv.DataBind();
            if (gv != null)
            {
                if (gv.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "Finance", "All Tax  Viewed", "TaxRates");
        }


        protected bool CheckSessions()
        {
            if (Session["UserName"] == null)
            {
                Response.Redirect("/login?url=" + HttpContext.Current.Request.Url.AbsolutePath.ToString());
                return false;
            }
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
            {
                Response.Redirect("/login");
                return false;
            }
            return true;
        }

        protected void lnkCommand_Click(object sender, EventArgs e)
        {
            if (CheckSessions())
            {
                LinkButton btn = (LinkButton)sender as LinkButton;
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/tax/edit-tax-" + btn.CommandArgument.ToString());
            }
        }
    }
}