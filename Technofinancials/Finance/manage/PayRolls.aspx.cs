﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class PayRolls : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int NewPayrollID
        {
            get
            {
                if (ViewState["NewPayrollID"] != null)
                {
                    return (int)ViewState["NewPayrollID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["NewPayrollID"] = value;
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/payrolls";

                    divAlertMsg.Visible = false;
                    clearFields();
                    BindCOA();
                    BindCOAPN();
                    BindAccount();
                    ViewState["SalarySrNo"] = null;
                    ViewState["dtEmployeeSalaries"] = null;

                    btnApproveByFinance.Visible = false;
                    btnReviewByFinance.Visible = false;
                    btnRevApproveByFinance.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReviewByFinance.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;
                    btnGenVoucher.Visible = false;

                    //btnPayNow.Visible = false;
                    accDiv.Visible = false;

                    if (HttpContext.Current.Items["PayrollID"] != null)
                    {
                        txtDatePN.Value = DateTime.Now.ToString("dd-MMM-yyyy");
                        NewPayrollID = Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString());
                        getNewPayrollByID(NewPayrollID);
                        txtPayrollDate.Enabled = false;
                        btnView.Visible = false;
                        CheckAccess();
                    }
                    else
                    {
                        //SalarySrNo = 1;
                        //calculateEmployeeSalaries();
                        //CheckAccess();
                    }
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        

        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApproveByFinance.Visible = false;
            btnReviewByFinance.Visible = false;
            btnRevApproveByFinance.Visible = false;
            lnkReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReviewByFinance.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;



            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "NewPayroll";
            objDB.PrimaryColumnnName = "NewPayrollID";
            objDB.PrimaryColumnValue = NewPayrollID.ToString();
            objDB.DocName = "Payroll";

            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
            if (chkAccessLevel == "Can Edit")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReviewByFinance.Visible = true;
            }
            if (chkAccessLevel == "Can Edit Finance")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReviewByFinance.Visible = true;
            }
            if (chkAccessLevel == "Can Edit & Review Finance")
            {
                btnSave.Visible = true;
                btnReviewByFinance.Visible = true;
                lnkReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve Finance")
            {
                btnSave.Visible = true;
                btnApproveByFinance.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve Finance")
            {
                btnSave.Visible = true;
                btnRevApproveByFinance.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit Finance")
            {
               // btnSave.Visible = true;
            }
        }



        private DataTable dtEmployeeSalaries
        {
            get
            {
                if (ViewState["dtEmployeeSalaries"] != null)
                {
                    return (DataTable)ViewState["dtEmployeeSalaries"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtEmployeeSalaries"] = value;
            }
        }
        protected int SalarySrNo
        {
            get
            {
                if (ViewState["SalarySrNo"] != null)
                {
                    return (int)ViewState["SalarySrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["SalarySrNo"] = value;
            }
        }
        private DataTable createEmployeeSalariesTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("EmployeeCode");
            dt.Columns.Add("EmployeeName");
            dt.Columns.Add("Department");
            dt.Columns.Add("Designation");
            dt.Columns.Add("BasicSalary");
            dt.Columns.Add("Allowances");
            dt.Columns.Add("Deductions");
            dt.Columns.Add("NetSalary");
            dt.Columns.Add("BankDetails");
            dt.Columns.Add("EmployeeID");
            dt.Columns.Add("Taxes");
            dt.Columns.Add("PFEmployee");
            dt.Columns.Add("PFCompany");
            dt.Columns.Add("PFID");
            dt.Columns.Add("GrossAmount");
            dt.Columns.Add("COA_ID");
            dt.Columns.Add("PayrolllExpCOA");
            dt.AcceptChanges();

            return dt;
        }


        private void BindCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOA.Items.Clear();
            DataTable dt = objDB.GetAllApprovedCOAs(ref errorMsg); 
            ddlCOA.DataSource = dt;
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("--- Select COA- --", "0")); 
            
            // COA TAX
            
            ddlCOATax.Items.Clear();
            ddlCOATax.DataSource = dt;
            ddlCOATax.DataTextField = "CodeTitle";
            ddlCOATax.DataValueField = "COA_ID";
            ddlCOATax.DataBind();
            ddlCOATax.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

        }

        private void BindCOAPN()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOAPN.Items.Clear();
            ddlCOAPN.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOAPN.DataTextField = "CodeTitle";
            ddlCOAPN.DataValueField = "COA_ID";
            ddlCOAPN.DataBind();
            ddlCOAPN.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

        }
        private void BindAccount()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlAccounts.DataSource = objDB.GetAllApprovedAccount(ref errorMsg);
            ddlAccounts.DataTextField = "AccountTitle";
            ddlAccounts.DataValueField = "AccountID";
            ddlAccounts.DataBind();
            ddlAccounts.Items.Insert(0, new ListItem("--- Select---", "0"));
            ddlAccounts.SelectedIndex = 0;
        }

        protected void BindSalariesTable()
        {
            try
            {

                if (ViewState["dtEmployeeSalaries"] == null)
                {
                    dtEmployeeSalaries = createEmployeeSalariesTable();
                    ViewState["dtEmployeeSalaries"] = dtEmployeeSalaries;
                }

                gvSalaries.DataSource = dtEmployeeSalaries;
                gvSalaries.DataBind();

                gvSalaries.UseAccessibleHeader = true;
                if (gvSalaries.Rows.Count > 0)
                {
                    gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    float totalBasicSalaries = 0;
                    float totalAllowances = 0;
                    float totalDeductions = 0;
                    float totalPFEmployee = 0;
                    float totalPFCompany = 0;
                    float totalNetSalaries = 0;
                    float totalTaxes = 0;

                    for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                    {
                        totalBasicSalaries += float.Parse(dtEmployeeSalaries.Rows[i]["BasicSalary"].ToString());
                        totalAllowances += float.Parse(dtEmployeeSalaries.Rows[i]["Allowances"].ToString());
                        totalDeductions += float.Parse(dtEmployeeSalaries.Rows[i]["Deductions"].ToString());
                        totalPFEmployee += float.Parse(dtEmployeeSalaries.Rows[i]["PFEmployee"].ToString());
                        totalPFCompany += float.Parse(dtEmployeeSalaries.Rows[i]["PFCompany"].ToString());
                        totalNetSalaries += float.Parse(dtEmployeeSalaries.Rows[i]["NetSalary"].ToString());
                        totalTaxes += float.Parse(dtEmployeeSalaries.Rows[i]["Taxes"].ToString());
                    }



                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalBasicSalaries")).Text = string.Format("{0:f2}", totalBasicSalaries);
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalAllowances")).Text = string.Format("{0:f2}", totalAllowances);
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalGrossSallay")).Text = string.Format("{0:f2}", (totalBasicSalaries + totalAllowances));
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalDeductions")).Text = string.Format("{0:f2}", totalDeductions);
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalProvidentFund")).Text = string.Format("{0:f2}", totalPFEmployee);
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalNetSalaries")).Text = "Net Amount : " + string.Format("{0:f2}", totalNetSalaries);
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalTaxes")).Text = "Total Taxes : " + string.Format("{0:f2}", totalTaxes);

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvSalaries_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    hdnRowNo.Value = e.Row.RowIndex.ToString();
                }
            }
        }
        protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {

                int index = e.RowIndex;

                dtEmployeeSalaries.Rows[index].SetField(5, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
                dtEmployeeSalaries.Rows[index].SetField(6, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditAllowances")).Text);
                dtEmployeeSalaries.Rows[index].SetField(7, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditDeductions")).Text);
                dtEmployeeSalaries.Rows[index].SetField(8, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditNetSalary")).Text);
                // dtEmployeeSalaries.Rows[index].SetField(9, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditTaxes")).Text);
                dtEmployeeSalaries.Rows[index].SetField(11, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditTaxes")).Text);
                dtEmployeeSalaries.Rows[index].SetField(12, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditProvidentFund")).Text);




                dtEmployeeSalaries.AcceptChanges();

                gvSalaries.EditIndex = -1;
                BindSalariesTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvSalaries_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSalaries.EditIndex = -1;
            BindSalariesTable();
        }
        protected void gvSalaries_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSalaries.EditIndex = e.NewEditIndex;
            BindSalariesTable();
        }

        private void getNewPayrollByID(int NewPayrollID)
        {
            DataTable dt = new DataTable();
            objDB.NewPayrollID = NewPayrollID;
            dt = objDB.GetNewPayrollByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    txtPayrollDate.Text = DateTime.Parse(dt.Rows[0]["NewPayrollDate"].ToString()).ToString("MMM-yyyy");
                    ddlCOA.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                    ddlCOATax.SelectedValue = dt.Rows[0]["Tax_COA_ID"].ToString();

                    if (dt.Rows[0]["DocStatus"].ToString() == "Approved By Finance")
                    {
                        btnGenVoucher.Visible = true;


                    }
                    else
                    {
                        btnGenVoucher.Visible = false;

                    }
                    if (dt.Rows[0]["isPaid"].ToString() == "True")
                    {
                        btnPayNow.Visible = false;
                    }

                    txtRemainingAmount.Value = dt.Rows[0]["RemainingAmountTotal"].ToString();

                    //objDB.DocID = NewPayrollID;
                    //objDB.DocType = "NewPayroll";
                    //ltrNotesTable.Text = objDB.GetDocNotes();

                    getNewPayrollDetailsByParollID(Convert.ToInt32(dt.Rows[0]["NewPayrollID"].ToString()));
                    GetNewPayrollDetailsByNewPayrollIDForPay(Convert.ToInt32(dt.Rows[0]["NewPayrollID"].ToString()));
                }
            }
            Common.addlog("View", "HR", "NewPayroll \"" + txtPayrollDate.Text + "\" Viewed", "NewPayroll", objDB.NewPayrollID);

        }

        protected void ddlPayBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPayBy.SelectedValue == "Bank")
            {
                accDiv.Visible = true;
            }
            else
            {
                accDiv.Visible = false;

            }
        }

        private void getNewPayrollDetailsByParollID(int NewPayrollID)
        {
            DataTable dt = new DataTable();
            objDB.NewPayrollID = NewPayrollID;
            dt = objDB.GetNewPayrollDetailsByNewPayrollID(ref errorMsg);
            dtEmployeeSalaries = dt;
            gvSalaries.DataSource = dt;
            gvSalaries.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvSalaries.UseAccessibleHeader = true;
                    gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }
        
        private void GetNewPayrollDetailsByNewPayrollIDForPay(int NewPayrollID)
        {
            DataTable dt = new DataTable();
            objDB.NewPayrollID = NewPayrollID;
            dt = objDB.GetNewPayrollDetailsByNewPayrollIDForPay(ref errorMsg);
            dtEmployeePayroll = dt;
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        private void clearFields()
        {
            txtPayrollDate.Text = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.PayrollDate = "01-" + txtPayrollDate.Text;
                objDB.ChartOfAccountID = int.Parse(ddlCOA.SelectedValue);
                objDB.Tax_COA_ID = int.Parse(ddlCOATax.SelectedValue);

                if (HttpContext.Current.Items["PayrollID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.NewPayrollID = NewPayrollID;

                    objDB.UpdateNewPayrollFinance();
                    res = "NewPayroll Data Updated";
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    NewPayrollID = Convert.ToInt32(objDB.AddNewPayrollFinance());
                    res = "New NewPayroll Added";
                }


                objDB.DocType = "NewPayroll";
                objDB.DocID = NewPayrollID;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();
                objDB.NewPayrollID = NewPayrollID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteNewPayrollDetails();

                dtEmployeeSalaries = (DataTable)ViewState["dtEmployeeSalaries"] as DataTable;
                if (dtEmployeeSalaries != null)
                {
                    if (dtEmployeeSalaries.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                        {


                            objDB.NewPayrollID = Convert.ToInt32(NewPayrollID);
                            objDB.EmployeeID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["EmployeeID"].ToString());
                            objDB.EmployeeCode = (dtEmployeeSalaries.Rows[i]["EmployeeCode"].ToString());

                            objDB.BasicSalary = float.Parse(dtEmployeeSalaries.Rows[i]["BasicSalary"].ToString());
                            objDB.EmployeeName = (dtEmployeeSalaries.Rows[i]["EmployeeName"].ToString());
                            objDB.OverTime = float.Parse(dtEmployeeSalaries.Rows[i]["OverTime"].ToString());
                            objDB.OverTimeAmount = float.Parse(dtEmployeeSalaries.Rows[i]["OverTimeAmount"].ToString());
                            objDB.Maintenance = float.Parse(dtEmployeeSalaries.Rows[i]["Maintenance"].ToString());
                            objDB.FoodAllowence = float.Parse(dtEmployeeSalaries.Rows[i]["FoodAllowence"].ToString());
                            objDB.Maintenance = float.Parse(dtEmployeeSalaries.Rows[i]["Maintenance"].ToString());
                            objDB.Fuel = float.Parse(dtEmployeeSalaries.Rows[i]["Fuel"].ToString());
                            objDB.AbsentAmount = float.Parse(dtEmployeeSalaries.Rows[i]["AbsentAmount"].ToString());
                            objDB.GrossSalary = float.Parse(dtEmployeeSalaries.Rows[i]["GrossSalary"].ToString());
                            objDB.Tax = float.Parse(dtEmployeeSalaries.Rows[i]["Tax"].ToString());
                            objDB.GrossSalaryWithOT = float.Parse(dtEmployeeSalaries.Rows[i]["GrossSalaryWithOT"].ToString());
                            objDB.EOBI = float.Parse(dtEmployeeSalaries.Rows[i]["EOBI"].ToString());
                            objDB.Advance = float.Parse(dtEmployeeSalaries.Rows[i]["Advance"].ToString());
                            objDB.TotalSalry = float.Parse(dtEmployeeSalaries.Rows[i]["TotalSalry"].ToString());
                            objDB.TotalSalryWithOT = float.Parse(dtEmployeeSalaries.Rows[i]["TotalSalryWithOT"].ToString());
                            objDB.COA_ID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["COA_ID"].ToString());
                            objDB.PayrolllExpCOA = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PayrolllExpCOA"].ToString());
                            objDB.EMP_COA = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["EMP_COA"].ToString());

                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddNewPayrollDetails();
                        }
                    }
                }


                if (res == "New NewPayroll Added" || res == "NewPayroll Data Updated")
                {
                    if (res == "New NewPayroll Added") { Common.addlog("Add", "HR", "New NewPayroll \"" + objDB.PayrollDate + "\" Added", "NewPayroll"); }
                    if (res == "NewPayroll Data Updated") { Common.addlog("Update", "HR", "NewPayroll \"" + objDB.PayrollDate + "\" Updated", "NewPayroll", objDB.NewPayrollID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnPDF_ServerClickOld(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "NewPayroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##DATE##", DateTime.Now.ToString("dd-MMM-yyyy"));
                //content = content.Replace("##MONTH##", ddlMonth.SelectedItem.Text);
                //content = content.Replace("##YEAR##", ddlYear.SelectedItem.Text);

                StringBuilder sbTable = new StringBuilder();
                sbTable.Append("<table style='width:100%;'>");
                sbTable.Append("<thead><tr><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Sr. No</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Employee Code</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Employee</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Department</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Designation</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Bank Details</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Basic Salary</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Allowances</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Deductions</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Net Salary</td></tr></thead>");
                if (dtEmployeeSalaries != null)
                {
                    if (dtEmployeeSalaries.Rows.Count > 0)
                    {
                        float totalBasicSalaries = 0;
                        float totalAllowances = 0;
                        float totalDeductions = 0;
                        float totalNetSalaries = 0;
                        sbTable.Append("<tbody>");
                        for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                        {
                            sbTable.Append("<tr><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + (i + 1) + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["EmployeeCode"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["EmployeeName"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Department"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Designation"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["BankDetails"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["BasicSalary"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Allowances"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Deductions"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["NetSalary"] + "</td></tr>");
                            totalBasicSalaries += float.Parse(dtEmployeeSalaries.Rows[i][5].ToString());
                            totalAllowances += float.Parse(dtEmployeeSalaries.Rows[i][6].ToString());
                            totalDeductions += float.Parse(dtEmployeeSalaries.Rows[i][7].ToString());
                            totalNetSalaries += float.Parse(dtEmployeeSalaries.Rows[i][8].ToString());
                        }
                        sbTable.Append("</tbody>");
                        sbTable.Append("<tfoot>");
                        sbTable.Append("<tr><td colspan='6' style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Net Calculations:</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalBasicSalaries + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalAllowances + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalDeductions + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left; font-weight:bold;'>" + totalNetSalaries + "</td></tr>");
                        sbTable.Append("</tfoot>");
                    }
                }
                sbTable.Append("</table>");
                content = content.Replace("##TABLE##", sbTable.ToString());

                //IronPdf.HtmlToPdf Renderer = new IronPdf.HtmlToPdf();
                //Renderer.PrintOptions.Header = new HtmlHeaderFooter()
                //{
                //    HtmlFragment = header
                //};
                //Renderer.PrintOptions.Footer = new HtmlHeaderFooter()
                //{
                //    HtmlFragment = footer
                //};
                //Renderer.PrintOptions.MarginTop = 50;
                //Renderer.PrintOptions.MarginLeft = 20;
                //Renderer.PrintOptions.MarginRight = 20;
                //Renderer.PrintOptions.MarginBottom = 20;

                //Renderer.PrintOptions.FitToPaperWidth = true;

                //Renderer.RenderHtmlAsPdf(content).Print();
                Common.addlog("Report", "HR", "NewPayroll Report Generated", "NewPayroll");

                Common.generatePDF(header, footer, content, "NewPayroll Sheet (" + txtPayrollDate.Text + ")", "A4", "Portrait");

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }




        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Payroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##DATE##", txtPayrollDate.Text);

                string sbTable = Common.GetTemplate(gvSalaries);

                content = content.Replace("##TABLE##", sbTable.ToString());

                Common.addlog("Report", "HR", "NewPayroll Report Generated", "NewPayroll");

                Common.generatePDF(header, footer, content, "NewPayroll Sheet", "A4", "Landscape");


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "NewPayroll", "NewPayrollID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "Finance", "NewPayroll of ID\"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/payrolls", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/payrolls/edit-spotlit-payroll-" + HttpContext.Current.Items["PayrollID"].ToString(), "Payroll of \"" + txtPayrollDate.Text + "\"", "NewPayroll", "Payroll", Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString()));

                //Common.addlog(res, "HR", "NewPayroll of ID\"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" Status Changed", "NewPayroll", NewPayrollID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;

                if (btn.ID.ToString() == "btnApproveByFinance" || btn.ID.ToString() == "btnRevApproveByFinance")
                {
                    objDB.NewPayrollID = NewPayrollID;
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.RunPayroll();

                    //float totAmount = 0;
                    //float TotTax = 0;
                    //dtEmployeeSalaries = (DataTable)ViewState["dtEmployeeSalaries"] as DataTable;
                    //if (dtEmployeeSalaries != null)
                    //{
                    //    if (dtEmployeeSalaries.Rows.Count > 0)
                    //    {

                    //        for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                    //        {
                    //            objDB.TableName = "NewPayrollDetails";
                    //            objDB.TableID = 0;
                    //            objDB.ParentID = NewPayrollID;
                    //            if (dtEmployeeSalaries.Rows[i]["TotalSalryWithOT"].ToString() != null && dtEmployeeSalaries.Rows[i]["TotalSalryWithOT"].ToString() != "")
                    //            {
                    //                objDB.Debit = float.Parse(dtEmployeeSalaries.Rows[i]["TotalSalryWithOT"].ToString());
                    //                totAmount += float.Parse(dtEmployeeSalaries.Rows[i]["TotalSalryWithOT"].ToString());
                    //            }
                    //            else
                    //            {
                    //                objDB.Debit = 0;
                    //            }

                    //            if (dtEmployeeSalaries.Rows[i]["Tax"].ToString() != null && dtEmployeeSalaries.Rows[i]["Tax"].ToString() != "")
                    //            {
                    //                TotTax += float.Parse(dtEmployeeSalaries.Rows[i]["Tax"].ToString());
                    //            }
                    //            else
                    //            {
                    //                objDB.Debit = 0;
                    //            }


                    //            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    //            objDB.CreatedBy = Session["UserName"].ToString();
                    //            if (dtEmployeeSalaries.Rows[i]["COA_ID"].ToString() != null && dtEmployeeSalaries.Rows[i]["COA_ID"].ToString() != "")
                    //            {
                    //                objDB.ChartOfAccountID = int.Parse(dtEmployeeSalaries.Rows[i]["COA_ID"].ToString());
                    //            }
                    //            else
                    //            {
                    //                objDB.ChartOfAccountID = 0;
                    //            }

                    //            objDB.TransactionDetail = ("Payable to " + dtEmployeeSalaries.Rows[i]["EmployeeName"].ToString() + " against PO # " + txtPayrollDate.Text);
                    //            objDB.AddTransactionHistoryDebit();

                    //        }

                    //    }
                    //}


                    //objDB.TableName = "NewPayrollTax";
                    //objDB.TableID = NewPayrollID;
                    //objDB.Credit = TotTax;
                    //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    //objDB.CreatedBy = Session["UserName"].ToString();
                    //objDB.ChartOfAccountID = Convert.ToInt32(ddlCOATax.SelectedValue);
                    //objDB.TransactionDetail = ("Payable against Payroll " + txtPayrollDate.Text);

                    //objDB.AddTransactionHistoryDebit();


                    //objDB.TableName = "NewPayroll";
                    //objDB.TableID = NewPayrollID;
                    //objDB.Credit = (totAmount+ TotTax);
                    //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    //objDB.CreatedBy = Session["UserName"].ToString();
                    //objDB.ChartOfAccountID = Convert.ToInt32(ddlCOA.SelectedValue);
                    //objDB.TransactionDetail = ("Payable against Payroll " + txtPayrollDate.Text);

                    //objDB.AddTransactionHistoryCredit();


                    btnGenVoucher.Visible = true;
                     AddVoucher();

                    


                    //if (dtEmployeeSalaries != null)
                    //{
                    //    for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                    //    {

                    //        objDB.PFID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PFID"].ToString());
                    //        objDB.PFCompanyAmount = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PFCompany"].ToString());
                    //        objDB.PFEmpoyeeAmount = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PFEmployee"].ToString());
                    //        objDB.PFYear = ddlYear.SelectedValue;
                    //        objDB.PFMonth = ddlMonth.SelectedValue;
                    //        objDB.CreatedBy = Session["UserName"].ToString();
                    //        objDB.AddPFDeduction();
                    //    }

                    //}
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void AddVoucher()
        {
            CheckSessions();

            string JVID = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.VoucherNo = txtPayrollDate.Text;
            objDB.Against = "Payroll";
            objDB.CreatedBy = Session["UserName"].ToString();
            JVID = objDB.AddFinanceVoucher();
            objDB.NewPayrollID = NewPayrollID;
            objDB.JVID = Convert.ToInt32(JVID);
            objDB.AddJVIDinPayroll();
        }

        protected void btnPayVoucher_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Session["TableName"] = "PayrollPay";
                Session["BackLink"] = ("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/payrolls/edit-payroll-" + NewPayrollID);

                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/payment-voucher/generate-" + NewPayrollID);
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnGenVoucher_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/payroll-journal-voucher/generate-" + NewPayrollID);
            }
            catch (Exception ex)
            {

            }
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "NewPayroll", "NewPayrollID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "NewPayroll of ID \"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" deleted", "NewPayroll", NewPayrollID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }



        }

        protected void txtPayrollDate_ServerChange(object sender, EventArgs e)
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"]);
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.PayrollDate = ("01-" + txtPayrollDate.Text);
            dt = objDB.GeneratePayroll(ref errorMsg);
            dtEmployeeSalaries = dt;

            gvSalaries.DataSource = dt;
            gvSalaries.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvSalaries.UseAccessibleHeader = true;
                    gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOATaxBal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlCOATax.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOATaxBal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private DataTable dtEmployeePayroll
        {
            get
            {
                if (ViewState["dtEmployeePayroll"] != null)
                {
                    return (DataTable)ViewState["dtEmployeePayroll"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtEmployeePayroll"] = value;
            }
        }

        protected void btnPay_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                int i = 0;
                string res = "";
                foreach (GridViewRow gvrow in gv.Rows)
                {
                    objDB.EmployeeID = Convert.ToInt32(dtEmployeePayroll.Rows[i]["EmployeeID"].ToString());
                    objDB.PayAmount = float.Parse(((TextBox)gvrow.FindControl("txtPayAmount")).Text == ""?"0": ((TextBox)gvrow.FindControl("txtPayAmount")).Text);
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.PayDate = txtDatePN.Value;
                    //objDB.PayAmount = float.Parse(txtAmountPN.Value);
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.PayCOAID = int.Parse(ddlCOAPN.SelectedValue);
                    objDB.POID = NewPayrollID;
                    objDB.AccountID = int.Parse(ddlAccounts.SelectedValue);
                    objDB.PayType = ddlPayBy.SelectedValue;

                    res = objDB.AddPayrollPay();

                    if (res == "Paid")
                    {
                        objDB.TableName = "PayrollPay";
                        objDB.TableID = 0;
                        objDB.ParentID = NewPayrollID;
                        objDB.Debit = objDB.PayAmount;
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetEmployeeCOAIDByID());
                        objDB.TransactionDetail = "Paid against Payroll# " + NewPayrollID;
                        objDB.AddTransactionHistoryDebit();

                        objDB.TableName = "PayrollPay";
                        objDB.TableID = 0;
                        objDB.ParentID = NewPayrollID;
                        objDB.Credit = objDB.PayAmount;
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.ChartOfAccountID = Convert.ToInt32(ddlCOAPN.SelectedValue);
                        objDB.TransactionDetail = "Paid against Payroll# " + NewPayrollID;
                        objDB.AddTransactionHistoryCredit();

                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                        pAlertMsg.InnerHtml = res;
                        addBCVoucher(NewPayrollID);

                    }
                    else
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = res;
                    }

                    i++;
                }
                if (res == "Paid")
                {
                    getNewPayrollDetailsByParollID(NewPayrollID);
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void addBCVoucher(int ID)
        {
            try
            {
                int BCVoucherID = 0;
                CheckSessions();
                objDB.VouchrType = ddlPayBy.SelectedValue;
                objDB.VoucherNo = ID.ToString();
                objDB.Against = "Payroll";
                objDB.TableName = "Payroll";
                objDB.TableID = ID;
                //objDB.VoucherAmount = Convert.ToDouble(txtAmountPN.Value);
                objDB.CreditCOA = int.Parse(ddlCOAPN.SelectedValue);
                objDB.DebitCOA = int.Parse(ddlCOA.SelectedValue);
                objDB.TransactionDetail = "Paid amount against Payroll# " + ID;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                BCVoucherID = Convert.ToInt32(objDB.AddBankCashVoucher());
                //VouchrType, VoucherNo, Against, TableName, TableID, VoucherAmount,CreditCOA,DebitCOA,TransactionDetail, CreatedBy, CompanyID 

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


    }

}