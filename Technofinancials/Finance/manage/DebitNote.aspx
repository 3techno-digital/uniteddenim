﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DebitNote.aspx.cs" Inherits="Technofinancials.Finance.manage.DebitNote" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
	 <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
			   <ProgressTemplate>
			   <div class="upd_panel" >
				   <div class="center">
					   <img src="/assets/images/Loading.gif" />
				   </div>
			   </div>
			   </ProgressTemplate>
		   </asp:UpdateProgress>


            <div class="wrap">


                <section class="app-content">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img src="/assets/images/Admin_Debit_Notes.png" class="img-responsive tf-page-heading-img" />
                                <h3 class="tf-page-heading-text">Debit Note</h3>
                            </div>

                            	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

	       <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                            <ContentTemplate>


                            <div class="col-sm-4">
                                <div class="pull-right flex">
                                    <button type="button" data-toggle="modal" data-target="#notes-modal" class="tf-note-btn-new " value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                    <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                    <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                    <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                    <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class" "Delete" CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                    <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                    <button class="tf-save-btn" "Reject & Disapprove" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                    <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>

                                    <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSubmit_ServerClick" validationgroup="btnValidate"  type="button"><i class="far fa-save"></i></button>
                                    <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                </div>

                            </div>
											
											 </ContentTemplate>
                 <Triggers>
                                <asp:PostBackTrigger ControlID="btnSave"  />
                            </Triggers>
                                            </asp:UpdatePanel>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid my-container">
                        <div class="dashboard-left inner-page">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>MRN  <span style="color:red !important;">*</span> </label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="ddlMRN" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control js-example-basic-single" ID="ddlMRN" OnSelectedIndexChanged="ddlMRN_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>PO  <span style="color:red !important;">*</span> </label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="ddlPOs" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control js-example-basic-single" ID="ddlPOs"  OnSelectedIndexChanged="ddlRFQ_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Supplier  <span style="color:red !important;">*</span> </label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlSupplier" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control js-example-basic-single"  ID="ddlSupplier">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Debit Note Code  <span style="color:red !important;">*</span> </label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtDBNCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Debite Note Code" id="txtDBNCode" disabled runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Generate Date  <span style="color:red !important;">*</span> </label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="29-12-1993" id="txtDate" disabled runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Sales Invoice No  <span style="color:red !important;">*</span> </label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtInvoice" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Sales Invoice No" id="txtInvoice" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Total Amount  <span style="color:red !important;">*</span> </label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtTotalAmount" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="0" id="txtTotalAmount" disabled runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                                                        <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            COA
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="ddlCOA" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <asp:Label CssClass="lbl-COA-Balance"  Text="Balance: 0" ID="lblCOABal" runat="server" />
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlCOA_SelectedIndexChanged" ID="ddlCOA">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                    </div>
                                    <div>&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="add-table-div">
                                                <asp:HiddenField ID="hdnRowNo" runat="server" />
                                                <asp:GridView ID="gv" runat="server" CssClass="table table-bordered" OnPreRender="gv_PreRender" ClientIDMode="Static" ShowHeaderWhenEmpty="True" OnRowDataBound="gv_RowDataBound" OnRowUpdated="gv_RowUpdated" OnRowUpdating="gv_RowUpdating" OnRowCancelingEdit="gv_RowCancelingEdit" AutoGenerateColumns="False" ShowFooter="True" OnRowEditing="gv_RowEditing">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSrNo" runat="server" Text='<%# Eval("SrNo")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Item Code <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblItemCode" runat="server" Text='<%# Eval("ItemCode")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="ddlItems" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                <asp:DropDownList ID="ddlItems" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single"></asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Item Unit  <span style='color:red !important;'>*</span> ">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblUnit" runat="server" Text='<%# Eval("ItemUnit")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <%--<asp:TextBox ID="txtEditUnit" CssClass="form-control" runat="server" Text='<%# Eval("ItemUnit")%>'></asp:TextBox>--%>
                                                                <asp:DropDownList ID="ddlEditUnit" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single" SelectedValue='<%# Bind("ItemUnit") %>'>
                                                                    <asp:ListItem Value="N/A">--- Select Unit ---</asp:ListItem>
                                                                    <asp:ListItem Value="Unit">Unit</asp:ListItem>
                                                                    <asp:ListItem Value="Box">Box</asp:ListItem>
                                                                    <asp:ListItem Value="Carton">Carton</asp:ListItem>
                                                                    <asp:ListItem Value="Ounce">Ounce</asp:ListItem>
                                                                    <asp:ListItem Value="Pound">Pound</asp:ListItem>
                                                                    <asp:ListItem Value="Gram">Gram</asp:ListItem>
                                                                    <asp:ListItem Value="KG">KG</asp:ListItem>
                                                                    <asp:ListItem Value="Ton">Ton</asp:ListItem>
                                                                    <asp:ListItem Value="Centimeter">Centimeter</asp:ListItem>
                                                                    <asp:ListItem Value="Inch">Inch</asp:ListItem>
                                                                    <asp:ListItem Value="Foot">Foot</asp:ListItem>
                                                                    <asp:ListItem Value="Yard">Yard</asp:ListItem>
                                                                    <asp:ListItem Value="Meter">Meter</asp:ListItem>
                                                                    <asp:ListItem Value="Kilometer">Kilometer</asp:ListItem>
                                                                    <asp:ListItem Value="Mile">Mile</asp:ListItem>
                                                                    <asp:ListItem Value="Pieces">Pieces</asp:ListItem>
                                                                     <asp:ListItem Value="Job">Job</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                               <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" ControlToValidate="ddlUnit" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                <%--<asp:TextBox ID="txtUnit" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                                                <asp:DropDownList ID="ddlUnit" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single">
                                                                    <asp:ListItem Value="N/A">--- Select Unit ---</asp:ListItem>
                                                                    <asp:ListItem Value="Unit">Unit</asp:ListItem>
                                                                    <asp:ListItem Value="Box">Box</asp:ListItem>
                                                                    <asp:ListItem Value="Carton">Carton</asp:ListItem>
                                                                    <asp:ListItem Value="Ounce">Ounce</asp:ListItem>
                                                                    <asp:ListItem Value="Pound">Pound</asp:ListItem>
                                                                    <asp:ListItem Value="Gram">Gram</asp:ListItem>
                                                                    <asp:ListItem Value="KG">KG</asp:ListItem>
                                                                    <asp:ListItem Value="Ton">Ton</asp:ListItem>
                                                                    <asp:ListItem Value="Centimeter">Centimeter</asp:ListItem>
                                                                    <asp:ListItem Value="Inch">Inch</asp:ListItem>
                                                                    <asp:ListItem Value="Foot">Foot</asp:ListItem>
                                                                    <asp:ListItem Value="Yard">Yard</asp:ListItem>
                                                                    <asp:ListItem Value="Meter">Meter</asp:ListItem>
                                                                    <asp:ListItem Value="Kilometer">Kilometer</asp:ListItem>
                                                                    <asp:ListItem Value="Mile">Mile</asp:ListItem>
                                                                    <asp:ListItem Value="Pieces">Pieces</asp:ListItem>
                                                                     <asp:ListItem Value="Job">Job</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Quantity <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblQty" runat="server" Text='<%# Eval("Quantity")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditQty" TextMode="Number" CssClass="form-control" runat="server" Text='<%# Eval("Quantity")%>' AutoPostBack="true" OnTextChanged="txtEditUnitPrice_TextChanged"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator14" ControlToValidate="txtQty"  ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                <asp:TextBox ID="txtQty" TextMode="Number" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtNetPrice_TextChanged"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Unit Price (PKR) <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Eval("UnitPrice")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditUnitPrice" TextMode="Number" CssClass="form-control" runat="server" Text='<%# Eval("UnitPrice")%>' AutoPostBack="true" OnTextChanged="txtEditUnitPrice_TextChanged"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator15" ControlToValidate="txtUnitPrice"  ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                <asp:TextBox ID="txtUnitPrice" TextMode="Number" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtNetPrice_TextChanged"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Net Price (PKR)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNetPrice" runat="server" Text='<%# Eval("NetPrice")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditNetPrice" TextMode="Number" CssClass="form-control" runat="server" disabled Text='<%# Eval("NetPrice")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtNetPrice" TextMode="Number" runat="server" disabled CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Remarks">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditRemarks" CssClass="form-control" runat="server" Text='<%# Eval("Remarks")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAdd" runat="server" CssClass="form-control" ValidationGroup="btnValidatef" CausesValidation="true" Text="Add" OnClick="btnAdd_Click"></asp:Button>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemove_Command"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div>&nbsp;</div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group add-atr-file-upload">
                                        <asp:FileUpload ID="itemAttachments" class="btn btn-primary" ClientIDMode="Static" runat="server" AllowMultiple="true" Style="display: none;"></asp:FileUpload>
                                        <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('itemAttachments').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                        Include Files (Attach Here)
                                    </div>
                                </div>

                            </div>
           
                            <div>&nbsp;</div>
                        </div>

                        <div class="dashboard-left inner-page" id="divAttachments" runat="server">
                            <h4>Attachments</h4>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <asp:GridView ID="gvAttachments" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeader="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="File Name">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("AttachmentPath") %>'><%# "File - " + Eval("DBNAttachmentID") %></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div>&nbsp;</div>


                    <!-- Modal -->
                    <div class="modal fade" id="notes-modal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Notes</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>


            </div>
            <uc:Footer ID="footer1" runat="server" />
            <!-- .wrap -->
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
</body>

<style>
    select#ddlPOs {
        padding: 8px;
        width: 100%;
    }

    select#ddlSupplier {
        padding: 8px;
        width: 100%;
    }

    .tf-back-btn {
        padding: 6px !important;
    }

    button.tf-save-btn {
        padding: 6px !important;
    }
    .tf-note-btn-new {
    padding: 6px!important;
    height: 40px!important;
    width: 40px!important;
    background-color: #575757!important;
    border-radius: 0px!important;
    border: none;
    box-shadow: 0 8px 6px -5px #cacaca !important;
    display: inline-block;
    float: left;
    margin-right: 4px!important;
}
.tf-note-btn-new i{
    color:#fff!important;
}

.tf-add-btn {
  
    padding: 6px 11px !important;

}
</style>
</html>
