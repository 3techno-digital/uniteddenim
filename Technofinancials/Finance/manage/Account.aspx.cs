﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.manage
{
    public partial class Account : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int AccID
        {
            get
            {
                if (ViewState["AccID"] != null)
                {
                    return (int)ViewState["AccID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AccID"] = value;
            }
        }
     
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {
                    CheckSessions();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/accounts";
                    BindCOA();
                    divAlertMsg.Visible = false;
                    clearFields();
                   

                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["AccountID"] != null)
                    {
                        AccID = Convert.ToInt32(HttpContext.Current.Items["AccountID"].ToString());
                        getDataByID(AccID);
                        CheckAccess();

                    }

                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void BindCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOA.Items.Clear();
            ddlCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

        }


        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.AccountID = ID;
            dt = objDB.GetAccountByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    txtTitle.Value = dt.Rows[0]["AccountTitle"].ToString();
                    txtAccountNo.Value = dt.Rows[0]["AccountNumber"].ToString();

                    txtBank.Value = dt.Rows[0]["Bank"].ToString();
                    ddlAccountType.SelectedValue=dt.Rows[0]["AccountType"].ToString(); 
                    txtBranch.Value = dt.Rows[0]["Branch"].ToString();    
                    ddlNature.SelectedValue = dt.Rows[0]["Nature"].ToString();
                    ddlCOA.SelectedValue = dt.Rows[0]["COA"].ToString();
                    txtManagerName.Value = dt.Rows[0]["ManagerName"].ToString();
                    txtContactNo.Value = dt.Rows[0]["ContactNo"].ToString();
                    if (dt.Rows[0]["isOnline"].ToString() == "True")
                    {
                        chkisOnline.Checked = true;
                    }
                    else
                    {
                        chkisOnline.Checked = false;
                    }
                }
            }
            Common.addlog("View", "Finance", "Account \"" + txtTitle.Value +" "+ txtAccountNo.Value + "\" Viewed", "Accounts", objDB.AccountID);


        }

        private void clearFields()
        {

            txtTitle.Value = "";
            txtAccountNo.Value = "";
            txtBank.Value ="";
            ddlAccountType.SelectedIndex = -1;
            txtBranch.Value ="";
            ddlNature.SelectedIndex = -1;
            ddlCOA.SelectedIndex = -1;
            txtManagerName.Value ="";
            txtContactNo.Value ="" ;
            chkisOnline.Checked = false;

        }

        private void CheckSessions()
        {
            if (Session["userid"] == null)
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.AccountTitle = txtTitle.Value;
                objDB.AccountNo = txtAccountNo.Value;

                objDB.AccountType = ddlAccountType.SelectedValue;
                objDB.BankName = txtBank.Value;
                objDB.BranchName = txtBranch.Value;
                objDB.NatureOfAccount = ddlNature.SelectedValue;
                objDB.ChartOfAccountID = Convert.ToInt32(ddlCOA.SelectedValue);
                objDB.ManagerName = txtManagerName.Value;
                objDB.ContactNo = txtContactNo.Value;
                objDB.isOnline = chkisOnline.Checked;
                objDB.Balance = 0;


                if (HttpContext.Current.Items["AccountID"] != null)
                {
                    objDB.AccountID = AccID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateAccount();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddAccount();

                    clearFields();
                }

                if (res == "Account Added" || res == "Account Updated")
                {
                    if (res == "Account Added") { Common.addlog("Add", "Finance", "New Account \"" + objDB.AccountTitle+" "+ objDB.AccountNo + "\" Added", "Accounts"); }
                    if (res == "Account Updated") { Common.addlog("Update", "Finance", "Account \"" + objDB.AccountTitle +" "+ objDB.AccountNo + "\" Updated", "Accounts", objDB.AccountID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "Accounts";
                objDB.PrimaryColumnnName = "AccountID";
                objDB.PrimaryColumnValue = AccID.ToString();
                objDB.DocName = "Accounts";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "Accounts", "AccountID", HttpContext.Current.Items["AccountID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "Finance", "Account of ID\"" + HttpContext.Current.Items["AccountID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/accounts", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/accounts/edit-account-" + HttpContext.Current.Items["AccountID"].ToString(), "Account \"" + txtTitle.Value + "\"", "Accounts", "Accounts", Convert.ToInt32(HttpContext.Current.Items["AccountID"].ToString()));

                //Common.addlog(res, "Finance", "Account of ID\"" + HttpContext.Current.Items["AccountID"].ToString() + "\" Status Changed", "Accounts", AccID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Accounts", "AccountID", HttpContext.Current.Items["AccountID"].ToString(), Session["UserName"].ToString());

                Common.addlog("Delete", "Finance", "Account of ID \"" + HttpContext.Current.Items["AccountID"].ToString() + "\" deleted", "Accounts", AccID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


    }
}