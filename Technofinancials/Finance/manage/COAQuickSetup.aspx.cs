﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class COAQuickSetup : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COABalTraID
        {
            get
            {
                if (ViewState["COABalTraID"] != null)
                {
                    return (int)ViewState["COABalTraID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COABalTraID"] = value;
            }
        }

        protected DataTable dtCOA
        {
            get
            {
                if (ViewState["dtCOA"] != null)
                {
                    return (DataTable)ViewState["dtCOA"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                ViewState["dtCOA"] = value;
            }
        }

        protected DataTable dtAccount
        {
            get
            {
                if (ViewState["dtAccount"] != null)
                {
                    return (DataTable)ViewState["dtAccount"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["dtAccount"] = value;
            }
        }

        string TMStatus
        {
            get
            {
                if (ViewState["TMStatus"] != null)
                {
                    return ViewState["TMStatus"].ToString();
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["TMStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/COAQuickSetup";
                    divAlertMsg.Visible = false;
                    getQuickCOA();
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void getQuickCOA()
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            DataTable dt = objDB.GetAllCOAQuickSetupByCompanyID(ref errorMsg);

            if (dt != null && dt.Rows.Count > 0)
            {
                if (dt.Rows.Count < 10)
                {
                    BindDropdown("All");
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    BindDropdown(dt.Rows[i]["Title"].ToString(), dt.Rows[i]["COAID"].ToString(), dt.Rows[i]["isNew"].ToString());
                }

            }
            else
            {
                BindDropdown("All");
            }

        }


        private void CheckSessions()
        {
            if (Session["userid"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
                objDB.CreatedBy = Session["UserName"].ToString();

                //Discount Start

                objDB.COAID = Convert.ToInt32(ddlDiscount.SelectedValue);
                objDB.isNew = false;
                objDB.Title = "Discount";
                res = objDB.AddCOAQuickSetup();
                if (res != "COA Quick Setup Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                    return;
                }

                //Discount End


                //WOT Start

                objDB.COAID = Convert.ToInt32(ddlWOT.SelectedValue);
                objDB.isNew = false;
                objDB.Title = "WOT";
                res = objDB.AddCOAQuickSetup();
                if (res != "COA Quick Setup Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                    return;
                }

                //WOT End




                //STWH Start

                objDB.COAID = Convert.ToInt32(ddlSTWH.SelectedValue);
                objDB.isNew = false;
                objDB.Title = "STWH";
                res = objDB.AddCOAQuickSetup();
                if (res != "COA Quick Setup Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                    return;
                }

                //STWH End

                //Utilization Start

                objDB.COAID = Convert.ToInt32(ddlUtilization.SelectedValue);
                objDB.isNew = false;
                objDB.Title = "Utilization";
                res = objDB.AddCOAQuickSetup();
                if (res != "COA Quick Setup Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                    return;
                }

                //Utilization End

                //Clients Start

                objDB.COAID = Convert.ToInt32(ddlClients.SelectedValue);
                objDB.isNew = Clients.Checked;
                objDB.Title = "Clients";
                res = objDB.AddCOAQuickSetup();
                if (res != "COA Quick Setup Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                    return;
                }

                //Clients End


                //Suppliers Start

                objDB.COAID = Convert.ToInt32(ddlSuppliers.SelectedValue);
                objDB.isNew = Suppliers.Checked;
                objDB.Title = "Suppliers";
                res = objDB.AddCOAQuickSetup();
                if (res != "COA Quick Setup Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                    return;
                }

                //Suppliers End


                //Banks Start

                objDB.COAID = Convert.ToInt32(ddlBanks.SelectedValue);
                objDB.isNew = Banks.Checked;
                objDB.Title = "Banks";
                res = objDB.AddCOAQuickSetup();
                if (res != "COA Quick Setup Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                    return;
                }

                //Banks End

                //Conssumable Start

                objDB.COAID = Convert.ToInt32(ddlConsumables.SelectedValue);
                objDB.isNew = Consumables.Checked;
                objDB.Title = "Consumables";
                res = objDB.AddCOAQuickSetup();
                if(res != "COA Quick Setup Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                    return;
                }

                //Conssumable End


                //Assets Start

                objDB.COAID = Convert.ToInt32(ddlAssets.SelectedValue);
                objDB.isNew = Assets.Checked;
                objDB.Title = "Assets";
                res = objDB.AddCOAQuickSetup();
                if (res != "COA Quick Setup Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                    return;
                }

                //Assets End


                //Services Start

                objDB.COAID = Convert.ToInt32(ddlServices.SelectedValue);
                objDB.isNew = Services.Checked;
                objDB.Title = "Services";
                res = objDB.AddCOAQuickSetup();
                if (res != "COA Quick Setup Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                    return;
                }

                //Services End


                if (res == "COA Quick Setup Added") { Common.addlog("Add", "Finance", " Balance Transfer by ID\"" + (objDB.COAQuickSetupID).ToString() + "\" Added", "COABalanceTransfer"); }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;

                if (Session["From"] != null)
                {
                    Session["From"] = null;
                    Response.Redirect(Session["FromURL"].ToString());
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void bindDataTables()
        {
            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            dtCOA = objDB.GetAllApprovedCOAs(ref errorMsg);
            dtAccount = objDB.GetAllCOA_Account(ref errorMsg);

        }

        protected void BindDropdown(string title, string ddlval = "0", string chkval = "No")
        {

            if (dtCOA == null || dtAccount == null)
            {
                bindDataTables();
            }

            if (title == "All" || title == "Discount")
            {
                ddlDiscount.Items.Clear();
                ddlDiscount.DataSource = dtCOA;
                ddlDiscount.DataTextField = "CodeTitle";
                ddlDiscount.DataValueField = "COA_ID";
                ddlDiscount.DataBind();
                ddlDiscount.Items.Insert(0, new ListItem("--- Select COA ---", "0"));

                if (title != "All")
                {
                    ddlDiscount.SelectedValue = ddlval;
                }
            }

            if (title == "All" || title == "WOT")
            {
                ddlWOT.Items.Clear();
                ddlWOT.DataSource = dtCOA;
                ddlWOT.DataTextField = "CodeTitle";
                ddlWOT.DataValueField = "COA_ID";
                ddlWOT.DataBind();
                ddlWOT.Items.Insert(0, new ListItem("--- Select COA ---", "0"));

                if (title != "All")
                {
                    ddlWOT.SelectedValue = ddlval;
                }
            }

            if (title == "All" || title == "STWH")
            {
                ddlSTWH.Items.Clear();
                ddlSTWH.DataSource = dtCOA;
                ddlSTWH.DataTextField = "CodeTitle";
                ddlSTWH.DataValueField = "COA_ID";
                ddlSTWH.DataBind();
                ddlSTWH.Items.Insert(0, new ListItem("--- Select COA ---", "0"));

                if (title != "All")
                {
                    ddlSTWH.SelectedValue = ddlval;
                }
            }

            if (title == "All" || title == "Utilization")
            {
                ddlUtilization.Items.Clear();
                ddlUtilization.DataSource = dtCOA;
                ddlUtilization.DataTextField = "CodeTitle";
                ddlUtilization.DataValueField = "COA_ID";
                ddlUtilization.DataBind();
                ddlUtilization.Items.Insert(0, new ListItem("--- Select COA ---", "0"));

                if (title != "All")
                {
                    ddlUtilization.SelectedValue = ddlval;
                }
            }

            if (title == "All" || title == "Clients")
            {
                if (chkval == "True" || chkval == "true")
                {
                    Clients.Checked = true;
                }
                else if (chkval == "False" || chkval == "false")
                {
                    Clients.Checked = false;
                }

                if (Clients.Checked)
                {
                    ddlClients.Items.Clear();
                    ddlClients.DataSource = dtAccount;
                    ddlClients.DataTextField = "Name";
                    ddlClients.DataValueField = "AccountID";
                    ddlClients.DataBind();
                    ddlClients.Items.Insert(0, new ListItem("-- Select COA Head --", "0"));
                }
                else
                {
                    ddlClients.Items.Clear();
                    ddlClients.DataSource = dtCOA;
                    ddlClients.DataTextField = "CodeTitle";
                    ddlClients.DataValueField = "COA_ID";
                    ddlClients.DataBind();
                    ddlClients.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
                }

                if (title != "All")
                {
                    ddlClients.SelectedValue = ddlval;
                }

            }




            if (title == "All" || title == "Suppliers")
            {
                if (chkval == "True" || chkval == "true")
                {
                    Suppliers.Checked = true;
                }
                else if (chkval == "False" || chkval == "false")
                {
                    Suppliers.Checked = false;
                }

                if (Suppliers.Checked)
                {
                    ddlSuppliers.Items.Clear();
                    ddlSuppliers.DataSource = dtAccount;
                    ddlSuppliers.DataTextField = "Name";
                    ddlSuppliers.DataValueField = "AccountID";
                    ddlSuppliers.DataBind();
                    ddlSuppliers.Items.Insert(0, new ListItem("-- Select COA Head --", "0"));
                }
                else
                {
                    ddlSuppliers.Items.Clear();
                    ddlSuppliers.DataSource = dtCOA;
                    ddlSuppliers.DataTextField = "CodeTitle";
                    ddlSuppliers.DataValueField = "COA_ID";
                    ddlSuppliers.DataBind();
                    ddlSuppliers.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
                }

                if (title != "All")
                {
                    ddlSuppliers.SelectedValue = ddlval;
                }

            }

            if (title == "All" || title == "Banks")
            {
                if (chkval == "True" || chkval == "true")
                {
                    Banks.Checked = true;
                }
                else if (chkval == "False" || chkval == "false")
                {
                    Banks.Checked = false;
                }

                if (Banks.Checked)
                {
                    ddlBanks.Items.Clear();
                    ddlBanks.DataSource = dtAccount;
                    ddlBanks.DataTextField = "Name";
                    ddlBanks.DataValueField = "AccountID";
                    ddlBanks.DataBind();
                    ddlBanks.Items.Insert(0, new ListItem("-- Select COA Head --", "0"));
                }
                else
                {
                    ddlBanks.Items.Clear();
                    ddlBanks.DataSource = dtCOA;
                    ddlBanks.DataTextField = "CodeTitle";
                    ddlBanks.DataValueField = "COA_ID";
                    ddlBanks.DataBind();
                    ddlBanks.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
                }

                if (title != "All")
                {
                    ddlBanks.SelectedValue = ddlval;
                }

            }




            if (title == "All" || title == "Consumables")
            {
                if (chkval == "True" || chkval == "true")
                {
                    Consumables.Checked = true;
                }
                else if (chkval == "False" || chkval == "false")
                {
                    Consumables.Checked = false;
                }

                if (Consumables.Checked)
                {
                    ddlConsumables.Items.Clear();
                    ddlConsumables.DataSource = dtAccount;
                    ddlConsumables.DataTextField = "Name";
                    ddlConsumables.DataValueField = "AccountID";
                    ddlConsumables.DataBind();
                    ddlConsumables.Items.Insert(0, new ListItem("-- Select COA Head --", "0"));
                }
                else
                {
                    ddlConsumables.Items.Clear();
                    ddlConsumables.DataSource = dtCOA;
                    ddlConsumables.DataTextField = "CodeTitle";
                    ddlConsumables.DataValueField = "COA_ID";
                    ddlConsumables.DataBind();
                    ddlConsumables.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
                }

                if (title != "All")
                {
                    ddlConsumables.SelectedValue = ddlval;
                }
                
            }




            if (title == "All" || title == "Assets")
            {
                if (chkval == "True" || chkval == "true")
                {
                    Assets.Checked = true;
                }
                else if (chkval == "False" || chkval == "false")
                {
                    Assets.Checked = false;
                }

                if (Assets.Checked)
                {
                    ddlAssets.Items.Clear();
                    ddlAssets.DataSource = dtAccount;
                    ddlAssets.DataTextField = "Name";
                    ddlAssets.DataValueField = "AccountID";
                    ddlAssets.DataBind();
                    ddlAssets.Items.Insert(0, new ListItem("-- Select COA Head --", "0"));
                }
                else
                {
                    ddlAssets.Items.Clear();
                    ddlAssets.DataSource = dtCOA;
                    ddlAssets.DataTextField = "CodeTitle";
                    ddlAssets.DataValueField = "COA_ID";
                    ddlAssets.DataBind();
                    ddlAssets.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
                }

                if (title != "All")
                {
                    ddlAssets.SelectedValue = ddlval;
                }

            }



            if (title == "All" || title == "Services")
            {
                if (chkval == "True" || chkval == "true")
                {
                    Services.Checked = true;
                }
                else if (chkval == "False" || chkval == "false")
                {
                    Services.Checked = false;
                }

                if (Services.Checked)
                {
                    ddlServices.Items.Clear();
                    ddlServices.DataSource = dtAccount;
                    ddlServices.DataTextField = "Name";
                    ddlServices.DataValueField = "AccountID";
                    ddlServices.DataBind();
                    ddlServices.Items.Insert(0, new ListItem("-- Select COA Head --", "0"));
                }
                else
                {
                    ddlServices.Items.Clear();
                    ddlServices.DataSource = dtCOA;
                    ddlServices.DataTextField = "CodeTitle";
                    ddlServices.DataValueField = "COA_ID";
                    ddlServices.DataBind();
                    ddlServices.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
                }

                if (title != "All")
                {
                    ddlServices.SelectedValue = ddlval;
                }

            }




        }

        protected void chkboxAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindDropdown(((CheckBox)sender as CheckBox).ID);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }

}