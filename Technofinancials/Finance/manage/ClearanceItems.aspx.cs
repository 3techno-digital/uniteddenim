﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.finance.Manage
{
	public partial class ClearanceItems : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int ClearanceItemID
        {
            get
            {
                if (ViewState["ClearanceItemID"] != null)
                {
                    return (int)ViewState["ClearanceItemID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["ClearanceItemID"] = value;
            }
        }


        private void BindDepartmentDropDown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                ddlDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                ddlDept.DataTextField = "DeptName";
                ddlDept.DataValueField = "DeptID";
                ddlDept.DataBind();
                ddlDept.Items.Insert(0, new ListItem("--- Select---", "0"));


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    BindDepartmentDropDown();
                    ViewState["ClearanceItemID"] = null;
                    btnDelete.Visible = false;
                    
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/clearance-items";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["ClearanceItemID"] != null)
                    {
                        ClearanceItemID = Convert.ToInt32(HttpContext.Current.Items["ClearanceItemID"].ToString());
                        GetClearanceItemByID(ClearanceItemID);
                        ViewState["ClearanceItemID"] = null;
                        btnDelete.Visible = true;
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
               
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "ClearanceItems";
                objDB.PrimaryColumnnName = "ClearanceItemID";
                objDB.PrimaryColumnValue = ClearanceItemID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void GetClearanceItemByID(int clearanceItemID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.ClearanceItemID = clearanceItemID;
                dt = objDB.GetClearanceItemsByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtTitle.Value = dt.Rows[0]["Title"].ToString();
                        ddlDept.SelectedValue = dt.Rows[0]["DepartmentID"].ToString();
                        txtNotes.Value = dt.Rows[0]["Note"].ToString();
                        txtTitle.Disabled = true;
                    }
                }
                Common.addlog("View", "Finance", "Clearance Item \"" + txtTitle.Value + "\" Viewed", "Clearance Item", objDB.ClearanceItemID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Title = txtTitle.Value;
                objDB.Notes = txtNotes.Value;
                objDB.DeptID = Convert.ToInt32(ddlDept.SelectedValue);

                if (HttpContext.Current.Items["ClearanceItemID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.ClearanceItemID = ClearanceItemID;
                    res = objDB.UpdateClearanceItem();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddClearanceItem();
                }

                clearFields();
                if (res == "New Clearance Item Added Successfully" || res == "ClearanceItems Data Updated")
                {
                    if (res == "New Clearance Item Added Successfully") { Common.addlog("Add", "Finance", "New Clearance Item \"" + objDB.Title + "\" Added", "ClearanceItems"); }
                    if (res == "Clearance Item Data Updated") { Common.addlog("Update", "Finance", "ClearanceItems \"" + objDB.Title + "\" Updated", "Clearance Item", objDB.ClearanceItemID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtTitle.Value =
            txtNotes.Value = "";
            ddlDept.SelectedValue = "0";          
        }

        protected void btnDelete_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();

                string res = "";

                if (HttpContext.Current.Items["ClearanceItemID"] != null)
                {
                    objDB.ClearanceItemID = Convert.ToInt32(HttpContext.Current.Items["ClearanceItemID"]);
                    res = objDB.DeleteClearanceItemByID();
                    clearFields();
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
	}
}