﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.finance.Manage
{
    public partial class AddOns : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int AddOnsID
        {
            get
            {
                if (ViewState["AddOnsID"] != null)
                {
                    return (int)ViewState["AddOnsID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AddOnsID"] = value;
            }
        }

        private void BindAddonTypes()
		{
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlType.DataSource = objDB.GetAllAddonsHead(ref errorMsg);
            ddlType.DataTextField = "Types";
            ddlType.DataValueField = "Types";
            ddlType.DataBind();
            ddlType.Items.Insert(0, new ListItem("--- Select ---", "0"));
        }
        private void BindDepartmentDropDown()
        {
            CheckSessions();
            try
            {   
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                ddlDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                ddlDept.DataTextField = "DeptName";
                ddlDept.DataValueField = "DeptID";
                ddlDept.DataBind();
                ddlDept.Items.Insert(0, new ListItem("--- All ---", "0"));


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {
                    CheckSessions();
                    BindDepartmentDropDown();
                    BindAddonTypes();
                    ViewState["AddOnsID"] = null;
                    BindCurrencyDropdown();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/AddOns";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["AddOnsID"] != null)
                    {
                        AddOnsID = Convert.ToInt32(HttpContext.Current.Items["AddOnsID"].ToString());
                        getAddOnsByID(AddOnsID);
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "AddOnsHead";
                objDB.PrimaryColumnnName = "AddOnsID";
                objDB.PrimaryColumnValue = AddOnsID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getAddOnsByID(int AddOnsID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.AddOnsID = AddOnsID;
                dt = objDB.GetAddOnsByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtTitle.Value = dt.Rows[0]["Tittle"].ToString();
                        ddlType.SelectedValue = dt.Rows[0]["Types"].ToString();
                        ddlDept.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                        txtNotes.Value = dt.Rows[0]["Note"].ToString();
                        chkAllow.Checked = Convert.ToBoolean(dt.Rows[0]["isAllow"].ToString());
                        ddlCurrency.SelectedValue = dt.Rows[0]["CurrencyID"].ToString();

                    }
                }
                Common.addlog("View", "HR", "Addonshead \"" + txtTitle.Value + "\" Viewed", "Addonshead", objDB.AddOnsID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                int isAllow = 0;
                if (chkAllow.Checked == true)
                {
                    isAllow = 1;
                }
                objDB.IsAllow = isAllow;
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Title = txtTitle.Value;
                objDB.Notes = txtNotes.Value;
                objDB.CurrencyID = Convert.ToInt32(ddlCurrency.SelectedValue);
                objDB.IsDeduction= ddlType.SelectedItem.Text=="Deductions" ?  true : false;
              
                objDB.TypeID = ddlType.SelectedValue;
                objDB.DeptID =  Convert.ToInt32(ddlDept.SelectedValue);

                if (HttpContext.Current.Items["AddOnsID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.AddOnsID = AddOnsID;
                    res = objDB.UpdateAddOns();
                    //res = "Add On Head Data Updated";
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddAddOns();
                    clearFields();
                    //res = "New Add On Head Added";
                }



                if (res == "New AddOns Head Added Successfully" || res == "AddOns head Data Updated")
                {
                    if (res == "New AddOns Head Added Successfully") { Common.addlog("Add", "Finance", "New AddOns \"" + objDB.Title + "\" Added", "AddOnsHead"); }
                    if (res == "AddOns head Data Updated") { Common.addlog("Update", "Finance", "AddOns \"" + objDB.Title + "\" Updated", "AddOnsHead", objDB.AddOnsID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtTitle.Value = "";
            ddlCurrency.SelectedValue = "0";
            ddlDept.SelectedValue = "0";
            chkAllow.Checked = false;
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                    System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                    string res = Common.addAccessLevels(btn.ID.ToString(), "AddOnsHead", "AddOnsID", HttpContext.Current.Items["AddOnsID"].ToString(), Session["UserName"].ToString());
                    Common.addlogNew(res, "HR", "AddOn of ID\"" + HttpContext.Current.Items["AddOnsID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/AddOns", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/AddOns/edit-AddOns-" + HttpContext.Current.Items["AddOnsID"].ToString(), "AddOn \"" + txtTitle.Value + "\"", "AddOnsHead", "Announcement", Convert.ToInt32(HttpContext.Current.Items["AddOnsID"].ToString()));

                //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                //{
                //    objDB.AddOnsID = AddOnsID;
                //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //    objDB.AddCurrencyToEmployeeAttendance();

                //}

                //Common.addlog("Delete", "HR", "Currency of ID \"" + objDB.AddOnsID + "\" deleted", "Currency", objDB.AddOnsID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "AddOnsHead", "AddOnsID", HttpContext.Current.Items["AddOnsID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "Finance", "AddOns of ID\"" + HttpContext.Current.Items["AddOnsID"].ToString() + "\" Status Changed", "AddOnsHead", AddOnsID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.AddOnsID = AddOnsID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteCurrency();
                Common.addlog("AddOns Rejected", "Finance", "AddOns of ID\"" + HttpContext.Current.Items["AddOnsID"].ToString() + "\" AddOns Rejected", "AddOnsHead", AddOnsID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "AddonsHead Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void BindCurrencyDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlCurrency.DataSource = objDB.GetAllCurrencyByCompanyID(ref errorMsg);
                ddlCurrency.DataTextField = "CurrencyName";
                ddlCurrency.DataValueField = "CurrencyID";
                ddlCurrency.DataBind();
                ddlCurrency.Items.Insert(0, new ListItem("--- Select Currency ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
    }
}