﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.Manage
{
    public partial class UploadLoanSchedule : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int SheetID
        {
            get
            {
                if (ViewState["SheetID"] != null)
                {
                    return (int)ViewState["SheetID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["SheetID"] = value;
            }
        }
        protected int LoanID
        {
            get
            {
                if (ViewState["LoanID"] != null)
                {
                    return (int)ViewState["LoanID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["LoanID"] = value;
            }
        }
        protected DataTable Attachment
        {
            get
            {
                if (ViewState["Attachment"] != null)
                {
                    return (DataTable)ViewState["Attachment"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["Attachment"] = value;
            }
        }

     
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {

                    divAlertMsg.Visible = false;
                    downloadsuccess.Visible = false;
              
                    CheckSessions();
                    GetLoanApplicationEmployees();
                    if (HttpContext.Current.Items["SheetID"] != null)
                    {
                        SheetID = Convert.ToInt32(HttpContext.Current.Items["SheetID"].ToString());
                        GetData();
                        btnSave.Visible = false;
                        Button1.Visible = false;
                        FileUpload1.Visible = false;
            
                        div1.Visible = false;
                        template.Visible = false;


                    }
                    else
                    {



                    }


                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }
        private void GetLoanApplicationEmployees()
		{
           
            ddlEmployee.DataSource = objDB.GetAllLoanApplicationEmployees(ref errorMsg);
            ddlEmployee.DataTextField = "employeename";
            ddlEmployee.DataValueField = "loanid";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("--- Select ---", "0"));
        }
        protected DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            StreamReader sr = new StreamReader(strFilePath);
            string[] headers = sr.ReadLine().Split( ',' );
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }

           

            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(),  ";(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                DataRow dr = dt.NewRow();
                for (int i = 0; i < headers.Length; i++)
                {
                    dr[i] = rows[i];
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public static DataTable ConvertExcelToDataTable(string FileName)
        {
            DataTable dtResult = null;
            int totalSheet = 0; //No of sheets on excel file  
            using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1;';"))
            {
                objConn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = string.Empty;
                if (dt != null)
                {
                    var tempDataTable = (from dataRow in dt.AsEnumerable()
                                         where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                         select dataRow).CopyToDataTable();
                    dt = tempDataTable;
                    totalSheet = dt.Rows.Count;
                    sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                }
                cmd.Connection = objConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, "excelData");
                dtResult = ds.Tables["excelData"];
                objConn.Close();
                return dtResult; //Returning Dattable  
            }
        }

        public bool isConvertable(string filepath)
        {
			try
			{
                string fileName = filepath;
                DataTable dt = new DataTable("LoanPaymentSchedule");
                string[] columns = null;

                var lines = File.ReadAllLines(fileName);

                // assuming the first row contains the columns information
                if (lines.Count() > 0)
                {
                    columns = lines[0].Split(new char[] { ',' });

                    foreach (var column in columns)
                        dt.Columns.Add(column);

                 
                }

                // reading rest of the data
                for (int i = 1; i < lines.Count(); i++)
                {
                    DataRow dr = dt.NewRow();
                    string[] values = lines[i].Split(new char[] { ',' });

                    for (int j = 0; j < values.Count() && j < columns.Count(); j++)
                        dr[j] = values[j];

                    dt.Rows.Add(dr);
                }
                return true;
            }
            catch(Exception ex)
			{
                return false;
            }
           
        }
        public  DataTable GetDataSourceFromFile(string filepath)
        {
            string fileName = filepath;
            DataTable dt = new DataTable("LoanPaymentSchedule");
            string[] columns = null;

            var lines = File.ReadAllLines(fileName);

            // assuming the first row contains the columns information
            if (lines.Count() > 0)
            {
                columns = lines[0].Split(new char[] { ',' });

                foreach (var column in columns)
                    dt.Columns.Add(column);
                DataColumn Col = dt.Columns.Add("MasterID", typeof(System.String));


                Col.SetOrdinal(12);// to put the column in position 0;
                Col.DefaultValue = SheetID;
                Col = dt.Columns.Add("LoanID", typeof(System.String));


                Col.SetOrdinal(13);// to put the column in position 0;
                Col.DefaultValue = LoanID;

            }

            // reading rest of the data
            for (int i = 1; i < lines.Count(); i++)
            {
                DataRow dr = dt.NewRow();
                string[] values = lines[i].Split(new char[] { ',' });

                for (int j = 0; j < values.Count() && j < columns.Count(); j++)
                    dr[j] = values[j];

                dt.Rows.Add(dr);
            }
            return dt;
        }
        private DataTable SuccessRecords
        {
            get
            {
                if (ViewState["SuccessRecords"] != null)
                {
                    return (DataTable)ViewState["SuccessRecords"];
                }
                else
                {
                    return new DataTable();
                }
            }
            set
            {
                ViewState["SuccessRecords"] = value;
            }
        }

        private DataTable FailedRecords
        {
            get
            {
                if (ViewState["FailedRecords"] != null)
                {
                    return (DataTable)ViewState["FailedRecords"];
                }
                else
                {
                    return new DataTable();
                }
            }
            set
            {
                ViewState["FailedRecords"] = value;
            }
        }


        protected void ButtonSave_ServerClick(object sender, EventArgs e)
        {
			try
			{
               
                objDB.DeletePaymentScheduleSheet(ref errorMsg);
                //creating object of SqlBulkCopy    

                SqlBulkCopy objbulk = new SqlBulkCopy(DBManager.GetConnection().ConnectionString);


                //  assigning Destination table name    
                objbulk.DestinationTableName = "ScrubLoanPaymentSheet";
                //Mapping Table column    
                objbulk.ColumnMappings.Add("MasterID", "MasterID");
                objbulk.ColumnMappings.Add("LoanID", "loanid");
                objbulk.ColumnMappings.Add("noofinstallment", "noofinstallment");
                objbulk.ColumnMappings.Add("period", "period");
                objbulk.ColumnMappings.Add("opening", "opening");
                objbulk.ColumnMappings.Add("addition", "addition");
                objbulk.ColumnMappings.Add("lumpsum", "lumpsum");
                objbulk.ColumnMappings.Add("writeOff", "writeOff");
                objbulk.ColumnMappings.Add("closing", "closing");
                objbulk.ColumnMappings.Add("baseamount", "baseamount");
                objbulk.ColumnMappings.Add("additionalamount", "additionalamount");
                objbulk.ColumnMappings.Add("deduction", "deduction");
                objbulk.ColumnMappings.Add("outstanding", "outstanding");
                objbulk.ColumnMappings.Add("status", "status");
                ////inserting Datatable Records to DataBase    

                objbulk.WriteToServer(Attachment);
                
                objDB.CreatedBy = Session["Username"].ToString();
                objDB.SheetID = SheetID;
                objDB.LoanID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                DataSet ds = objDB.RunPaymentScheduleFile(ref errorMsg);
                SuccessRecords = ds.Tables[0];
               
                downloadsuccess.Visible = true;
              
                btnSave.Visible = false;
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Submitted";

            }
         
          catch(Exception ex)
            {
				divAlertMsg.Visible = true;
				divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
				pAlertMsg.InnerHtml = ex.Message;
				


			}
        }


        protected void btnsuccess_ServerClick(object sender, EventArgs e)
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(SuccessRecords, "AddonsUploaded");

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=PaymentScheduleloaded_" + ddlEmployee.SelectedItem.Text + ".xlsx");
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }

        }


     
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoanID = Convert.ToInt32(ddlEmployee.SelectedValue);
				if (LoanID != 0)
				{

                    CultureInfo provider = CultureInfo.InvariantCulture;
                    Random rand = new Random((int)DateTime.Now.Ticks);
                    int randnum = 0;
                    randnum = rand.Next(1, 100000);

                    if (FileUpload1.HasFile)
                    {


                        if (FileUpload1.PostedFile.ContentLength > 2097152)
                        {
                            LblMessage.Text = "File Size(2MB) Exceeded";
                            LblMessage.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                        else if (FileUpload1.FileName.Contains(".csv"))
                        {
                            string destDir = Server.MapPath("~/assets/Attachments/LoanPaymentSchedule/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");

                            string fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;
                            if (!Directory.Exists(destDir))
                            {
                                Directory.CreateDirectory(destDir);
                            }
                            string filenam = FileUpload1.FileName.ToString();
                            string path = "";
                            path = destDir + fn + filenam;
                            FileUpload1.PostedFile.SaveAs(path);

                            if (isConvertable(path))
                            {
                                objDB.SheetName = filenam;
                                objDB.SheetType = "LoanPaymentSchedule";
                                objDB.CreatedBy = Session["Username"].ToString();
                                objDB.uploadedby = Convert.ToInt32(Session["EmployeeID"]);

                                string id = objDB.AddnewSheetLoanPaymentSchedule(ref errorMsg);
                                SheetID = Convert.ToInt32(id);
                                LoanID = Convert.ToInt32(ddlEmployee.SelectedValue);

                                DataTable DT = GetDataSourceFromFile(path);

                                Attachment = DT;
                                gv.DataSource = DT;
                                gv.DataBind();
                                if (DT != null)
                                {
                                    if (DT.Rows.Count > 0)
                                    {
                                        gv.UseAccessibleHeader = true;
                                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                                        btnSave.Visible = true;
                                    }
                                }

                                divAlertMsg.Visible = true;
                                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                                pAlertMsg.InnerHtml = "Uploaded";

                            }

                            else
                            {
                                divAlertMsg.Visible = true;
                                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                                pAlertMsg.InnerHtml = "Kinldy remove COMMA (,) from uploaded file";
                            }



                        }
                        else
                        {
                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                            pAlertMsg.InnerHtml = "only .csv file allowed";

                        }
                    }
                    // objDB.PayRollCycle= 


                }

				else
				{
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Select Employee";

                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
            div1.Visible = false;
            template.Visible = false;

        }

        private void GetData()
        {
            CheckSessions();

            DataSet dt = new DataSet();
            objDB.SheetID =SheetID;
            dt = objDB.GetPaymentScheduleRecords(ref errorMsg);

            gv.DataSource = dt.Tables[0];
            gv.DataBind();
         //   PayrollCycle.Text = (dt.Tables[0].Rows[0]["AddonMonth"].ToString());
            SuccessRecords = dt.Tables[0];
            ddlEmployee.SelectedValue = dt.Tables[0].Rows[0]["loanid"].ToString();
            ddlEmployee.Enabled = false;
            downloadsuccess.Visible = true;
           

          
           // Common.addlog("ViewAll", "HR", "All Employees Viewed", "Employees");

        }


    }
}