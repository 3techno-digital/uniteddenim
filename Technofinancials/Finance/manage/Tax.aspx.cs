﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class Tax : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int taxRateID
        {
            get
            {
                if (ViewState["taxRateID"] != null)
                {
                    return (int)ViewState["taxRateID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["taxRateID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            { 
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;
                divAlertMsg.Visible = false;

                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/tax";
                BindCOA();
                clearFields();
                if (HttpContext.Current.Items["TaxRateID"] != null)
                {
                    taxRateID = Convert.ToInt32(HttpContext.Current.Items["TaxRateID"].ToString());
                    getTaxRateByID(taxRateID);
                    CheckAccess();
                }
            }
        }
        private void BindCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlAssAcc.Items.Clear();
            ddlAssAcc.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlAssAcc.DataTextField = "CodeTitle";
            ddlAssAcc.DataValueField = "COA_ID";
            ddlAssAcc.DataBind();
            ddlAssAcc.Items.Insert(0, new ListItem("--- Select Account- --", "0"));
        }
        private void clearFields()
        {
            txtName.Value = "";
            txtShortCode.Value = "";
            txtPer.Value = "0";
            ddlAssAcc.SelectedIndex = -1;
            txtDescription.Value = "";
        }

        private void getTaxRateByID(int taxRateID)
        {
            if (CheckSessions())
            {
                DataTable dt = new DataTable();
                objDB.TaxRateID = taxRateID;
                dt = objDB.GetTaxRatesByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtName.Value = dt.Rows[0]["TaxRateName"].ToString();
                        txtShortCode.Value = dt.Rows[0]["TaxRateShortName"].ToString();
                        txtPer.Value = dt.Rows[0]["TaxRatePer"].ToString();
                        txtDescription.Value = dt.Rows[0]["Descriptions"].ToString();
                        ddlAssAcc.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                        ddlTaxType.SelectedValue = (dt.Rows[0]["TaxType"].ToString() == null)? "0" : dt.Rows[0]["TaxType"].ToString();
                        if (dt.Rows[0]["isRecoverable"].ToString() == "True")
                        {
                            chkIsRecoverable.Checked = true;
                        }
                        else
                        {
                            chkIsRecoverable.Checked = false;
                        }
                    }
                }
            }
            Common.addlog("View", "Finance", "Tax Rates \"" + txtName.Value + "\" Viewed", "TaxRates", objDB.TaxRateID);

        }

        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApprove.Visible = false;
            btnReview.Visible = false;
            btnRevApprove.Visible = false;
            lnkReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReview.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;



            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "TaxRates";
            objDB.PrimaryColumnnName = "TaxRateID";
            objDB.PrimaryColumnValue = taxRateID.ToString();
            objDB.DocName = "Tax";

            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

            if (chkAccessLevel == "Can Edit")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReview.Visible = true;
            }
            if (chkAccessLevel == "Can Edit & Review")
            {
                btnSave.Visible = true;
                btnReview.Visible = true;
                lnkReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve")
            {
                btnSave.Visible = true;
                btnApprove.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve")
            {
                btnSave.Visible = true;
                btnRevApprove.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit")
            {
                btnSave.Visible = true;
            }
        }

        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
            return true;
        }


        protected void btnSubmit_ServerClick1(object sender, EventArgs e)
        {
            if (CheckSessions())
            {
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.TaxRateName = txtName.Value;
                objDB.TaxRateShortName = txtShortCode.Value;
                objDB.TaxRatePer = float.Parse(txtPer.Value);
                objDB.ChartOfAccountID = Convert.ToInt32(ddlAssAcc.SelectedValue);
                objDB.Description = txtDescription.Value;
                objDB.TaxRateType = ddlTaxType.SelectedValue;

                if (chkIsRecoverable.Checked)
                {
                    objDB.IsRecoverable = true;
                }
                else
                {
                    objDB.IsRecoverable = false;
                }
                objDB.ModifiedBy = Session["UserName"].ToString();

                if (HttpContext.Current.Items["TaxRateID"] != null)
                {
                    objDB.TaxRateID = taxRateID;
                    objDB.Status = "Active"; //this would be changed
                    res = objDB.UpdateTaxRates();
                }
                else
                {
                    res = objDB.AddTaxRates();
                    clearFields();
                }

                if (res == "New Tax Rates Added Successfully" || res == "Tax Rate Updated Successfully")
                {
                    if (res == "New Tax Rates Added Successfully") { Common.addlog("Add", "Finance", " Tax Rates \"" + objDB.TaxRateName + "\" Added", "TaxRates"); }
                    if (res == "Tax Rate Updated Successfully") { Common.addlog("Update", "Finance", "Tax Rates \"" + objDB.TaxRateName + "\" Updated", "TaxRates", objDB.TaxRateID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "TaxRates", "TaxRateID", HttpContext.Current.Items["TaxRateID"].ToString(), Session["UserName"].ToString());
            Common.addlogNew(res, "Finance", "Tax Rates of ID\"" + HttpContext.Current.Items["TaxRateID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/tax", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/tax/edit-tax-" + HttpContext.Current.Items["TaxRateID"].ToString(), "Tax Rates \"" + txtName.Value + "\"", "TaxRates", "Tax", Convert.ToInt32(HttpContext.Current.Items["TaxRateID"].ToString()));

            //Common.addlog(res, "Finance", "Tax Rates of ID \"" + HttpContext.Current.Items["TaxRateID"].ToString() + "\" Status Changed", "TaxRates", taxRateID /* ID From Page Top  */);/* Button1_ServerClick  */
            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;

            CheckAccess();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            string type = btn.CommandArgument;
            string res = Common.addAccessLevels(type, "TaxRates", "TaxRateID", HttpContext.Current.Items["TaxRateID"].ToString(), Session["UserName"].ToString());
            Common.addlog("Delete", "Finance", "Tax Rates of ID \"" + HttpContext.Current.Items["TaxRateID"].ToString() + "\" deleted", "TaxRates", taxRateID /* ID From Page Top  */);/* lnkDelete_Click  */
            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            Response.Redirect(btnBack.HRef);

        }

    }
}