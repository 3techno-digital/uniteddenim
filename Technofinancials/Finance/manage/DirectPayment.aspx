﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DirectPayment.aspx.cs" Inherits="Technofinancials.Finance.manage.DirectPayment" %>


<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">
                <section class="app-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img src="/assets/images/Admin_Purchase_Orders.png" class="img-responsive tf-page-heading-img" />
                                <h3 class="tf-page-heading-text">Voucher</h3>
                            </div>

                            <div class="col-md-4">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group" id="divAlertMsg" runat="server">
                                            <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                <span>
                                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                </span>
                                                <p id="pAlertMsg" runat="server">
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                <ContentTemplate>

                                    <div class="col-sm-4">
                                        <div class="pull-right flex">
                                           
                                            <asp:HiddenField ID="hdnTransactionID" Value="0" runat="server" />
                                            <%--<button type="button" data-toggle="modal" data-target="#notes-modal" class="tf-note-btn-new" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>--%>
                                            <button class="tf-save-btn" "Review & Approve" id="btnRevApproveByFinance" runat="server" onserverclick="btnSubmit_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                            <button class="tf-save-btn" "Review" id="btnReviewByFinance" runat="server" onserverclick="btnSubmit_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                            <button class="tf-save-btn" "Approve" id="btnApproveByFinance" runat="server" onserverclick="btnSubmit_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class" "Delete" CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                            <button class="tf-save-btn" "Reject" id="btnReject" runat="server" onserverclick="btnSubmit_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                            <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="btnSubmit_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                            <button class="tf-save-btn" "Reject & Disapprove" id="btnRejDisApprove" runat="server" onserverclick="btnSubmit_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                            <button class="tf-save-btn" "Submit for Review" id="btnSubForReviewByFinance" runat="server" onserverclick="btnSubmit_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                            <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSubmit_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                            <a class="tf-save-btn" "Print" id="btnGenVoucher" runat="server"><i class="fas fa-print"></i></a>
                                            <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                             <div style="display: none">
                                                <button type="button" runat="server" id="GenVoucherPDF" onserverclick="GenVoucherPDF_ServerClick"></button>
                                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid my-container">
                        <div class="dashboard-left inner-page">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Date  <span style="color: red !important;">*</span></label>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtDate" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control datetime-picker" data-date-format="DD-MMM-YYYY" id="txtDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-4" runat="server">
                                            <label>Voucher Type <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlType" InitialValue="0" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control js-example-basic-single" ID="ddlType" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                                    <asp:ListItem Value="JV">Journal Voucher</asp:ListItem>
                                                    <asp:ListItem Value="CPV">Cash Payment</asp:ListItem>
                                                    <asp:ListItem Value="BPV">Bank Payment</asp:ListItem>
                                                    <asp:ListItem Value="CRV">Cash Receipt</asp:ListItem>
                                                    <asp:ListItem Value="BRV">Bank Receipt</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Voucher NO  <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtVoucherNO" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="txtVoucherNO" runat="server" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Serial NO  <span style="color: red !important;">*</span><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator30" ControlToValidate="txtSerialNO" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="txtSerialNo" runat="server" disabled />
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Project <span style="color: red !important;">*</span><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" InitialValue="0" ControlToValidate="ddlLocation" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control js-example-basic-single" ID="ddlLocation">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-4" runat="server">
                                            <label>Payment Against <span style="color: red !important;">*</span><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" InitialValue="0" ControlToValidate="ddlAgaints" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" class="select2 form-control" ID="ddlAgaints" OnSelectedIndexChanged="ddlAgaints_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="Direct">Direct Payment</asp:ListItem>
                                                    <asp:ListItem Value="GRN">GRN</asp:ListItem>
                                                    <asp:ListItem Value="PO">PO</asp:ListItem>
                                                    <asp:ListItem Value="INV">Invoice</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-4" runat="server" id="GRNDIV">
                                            <label>GRN# <span style="color: red !important;" runat="server" id="spnReqGRN">*</span></label>
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" class="select2 form-control" ID="ddlGRN" OnSelectedIndexChanged="ddlGRN_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-4" runat="server" id="PODIV">
                                            <label>PO# <span style="color: red !important;" runat="server" id="spnReqPO">*</span></label>
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" class="select2 form-control" ID="ddlPO" OnSelectedIndexChanged="ddlPO_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-4" runat="server" id="INVDIV">
                                            <label>Invoice# <span style="color: red !important;" runat="server" id="spnReqInvoice">*</span></label>
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" class="select2 form-control" ID="ddlInvoice" OnSelectedIndexChanged="ddlInvoice_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Ref NO </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="txtRefNO" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix">&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="add-table-div">
                                                <asp:HiddenField ID="hdnRowNo" runat="server" />
                                                <asp:GridView ID="gv" runat="server" CssClass="table table-bordered" OnPreRender="gv_PreRender" ClientIDMode="Static" ShowHeaderWhenEmpty="True" OnRowDataBound="gv_RowDataBound" OnRowUpdated="gv_RowUpdated" OnRowUpdating="gv_RowUpdating" OnRowCancelingEdit="gv_RowCancelingEdit" AutoGenerateColumns="False" ShowFooter="True" OnRowEditing="gv_RowEditing">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSrNo" runat="server" Text='<%# Eval("SrNo")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="A/C Code  <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCOACode" runat="server" Text='<%# Eval("COACode")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="ddlCOA" InitialValue="0" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                <asp:DropDownList ID="ddlCOA" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single"></asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Instrument#  <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblInstrument" runat="server" Text='<%# Eval("Instrument")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditInstrument" CssClass="form-control" runat="server" Text='<%# Eval("Instrument")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator14" ControlToValidate="txtInstrument" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />--%>
                                                                <asp:TextBox ID="txtInstrument" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Transaction Detail  <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTransactionDetail" runat="server" Text='<%# Eval("TransactionDetail")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditTransactionDetail" CssClass="form-control" runat="server" Text='<%# Eval("TransactionDetail")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator31" ControlToValidate="txtTransactionDetail" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />--%>
                                                                <asp:TextBox ID="txtTransactionDetail" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Debit  <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDebit" runat="server" Text='<%# Eval("Debit")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditDebit" TextMode="Number" CssClass="form-control" runat="server" Text='<%# Eval("Debit")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator32" ControlToValidate="txtDebit" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />--%>
                                                                <asp:TextBox ID="txtDebit" TextMode="Number" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Credit  <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCredit" runat="server" Text='<%# Eval("Credit")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditCredit" TextMode="Number" CssClass="form-control" runat="server" Text='<%# Eval("Credit")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator15" ControlToValidate="txtCredit" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />--%>
                                                                <asp:TextBox ID="txtCredit" TextMode="Number" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ShowHeader="False">
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAdd" runat="server" CssClass="form-control" Text="Add" ValidationGroup="btnValidatef" CausesValidation="true" OnClick="btnAdd_Click"></asp:Button>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField FooterStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemove_Command"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lblTotal" CssClass="total" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Description  </label>
                                            <div class="form-group">
                                                <textarea rows="2" class="form-control" id="txtDescription" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <label>Total Debit   <span style="color: red !important;">*</span><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator37" ControlToValidate="txtTotalDebit" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="0" id="txtTotalDebit" disabled="disabled" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <label>Total Credit   <span style="color: red !important;">*</span><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator38" ControlToValidate="txtTotalCredit" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="0" id="txtTotalCredit" disabled="disabled" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </section>
            </div>
            <uc:Footer ID="footer1" runat="server" />
            <!-- .wrap -->
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    <style>
        .tf-back-btn {
            padding: 6px !important;
        }

        button.tf-save-btn {
            padding: 6px !important;
        }

        .tf-note-btn-new {
            padding: 6px !important;
            height: 40px !important;
            width: 40px !important;
            background-color: #575757 !important;
            border-radius: 0px !important;
            border: none;
            box-shadow: 0 8px 6px -5px #cacaca !important;
            display: inline-block;
            float: left;
            margin-right: 4px !important;
        }

            .tf-note-btn-new i {
                color: #fff !important;
            }

        /*.datepicker, .table-condensed {
                width: 250px;
            }
        .datepicker table {
            width: 100% !important;
        }*/
    </style>

</body>
</html>


