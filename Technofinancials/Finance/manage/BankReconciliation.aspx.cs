﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class BankReconciliation : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        string filePath = "";

        protected int BRID
        {
            get
            {
                if (ViewState["BRID"] != null)
                {
                    return (int)ViewState["BRID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["BRID"] = value;
            }
        }
        protected DataTable dtReconcile
        {
            get
            {
                if (ViewState["dtReconcile"] != null)
                {
                    return (DataTable)ViewState["dtReconcile"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtReconcile"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/bank-reconciliation";

                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;
                divAlertMsg.Visible = false;
                BindAccounts();
                BindExpense();
                BindInvoices();
                gvget.Visible = false;
                if (HttpContext.Current.Items["BRID"] != null)
                {
                    txtFromDate.Disabled = true;
                    txtToDate.Disabled = true;
                    ddlAccount.Attributes["data-plugin"] = "";
                    ddlAccount.Attributes["class"] = "form-control";
                    ddlAccount.Enabled = false;
                    divUpd.Visible = false;

                    gv.Visible = false;
                    gvget.Visible = true;
                    BRID = Convert.ToInt32(HttpContext.Current.Items["BRID"].ToString());
                    GetBankReconcilationByID(BRID);
                    CheckAccess();
                }
            }
        }
        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

            return true;
        }
        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApprove.Visible = false;
            btnReview.Visible = false;
            btnRevApprove.Visible = false;
            lnkReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReview.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;



            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "BankReconcilation";
            objDB.PrimaryColumnnName = "BankReconcilationID";
            objDB.PrimaryColumnValue = BRID.ToString();
            objDB.DocName = "BankReconciliation";

            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

            if (chkAccessLevel == "Can Edit")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReview.Visible = true;
            }
            if (chkAccessLevel == "Can Edit & Review")
            {
                btnSave.Visible = true;
                btnReview.Visible = true;
                lnkReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve")
            {
                btnSave.Visible = true;
                btnApprove.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve")
            {
                btnSave.Visible = true;
                btnRevApprove.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit")
            {
                btnSave.Visible = true;
            }
        }
        private void BindAccounts()
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlAccount.Items.Clear();
            ddlAccount.DataSource = objDB.GetAllApprovedAccount(ref errorMsg);
            ddlAccount.DataTextField = "AccountTitle";
            ddlAccount.DataValueField = "AccountID";
            ddlAccount.DataBind();
            ddlAccount.Items.Insert(0, new ListItem("--- Select Account ---", "0"));
        }
        private void BindExpense()
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlExpense.Items.Clear();
            ddlExpense.DataSource = objDB.GetAllApprovedExpensesBank(ref errorMsg);
            ddlExpense.DataTextField = "ExpenseNOTitle";
            ddlExpense.DataValueField = "ExpenseID";
            ddlExpense.DataBind();
            ddlExpense.Items.Insert(0, new ListItem("--- Select Expense ---", "0"));
        }
        private void BindInvoices()
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlInvoices.Items.Clear();
            ddlInvoices.DataSource = objDB.GetAllApprovedInvoices(ref errorMsg);
            ddlInvoices.DataTextField = "InvoiceCodeTitle";
            ddlInvoices.DataValueField = "InvoiceID";
            ddlInvoices.DataBind();
            ddlInvoices.Items.Insert(0, new ListItem("--- Select Invoice ---", "0"));
        }
        private void GetBankReconcilationByID(int BRID)

        {
            CheckSessions();

            DataTable dt = new DataTable();
            objDB.BankReconcilationID = BRID;
            dt = objDB.GetBankReconcilationByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtFromDate.Value = DateTime.Parse(dt.Rows[0]["FromDate"].ToString()).ToString("dd-MMM-yyyy");
                    txtToDate.Value = DateTime.Parse(dt.Rows[0]["ToDate"].ToString()).ToString("dd-MMM-yyyy");
                    ddlAccount.SelectedValue = dt.Rows[0]["AccountID"].ToString();

                }

            }
            Common.addlog("View", "Finance", "Bank Reconcilation of ID\"" + (objDB.BankReconcilationID).ToString() + "\" Viewed", "BankReconcilation", objDB.BankReconcilationID);

            GetBankReconcilationDetail(BRID);
        }
        private void GetBankReconcilationDetail(int BRID)
        {
            CheckSessions();

            objDB.BankReconcilationID = BRID;
            DataTable dt = new DataTable();
            dt = objDB.GetAllBankReconcilationDetailByBankReconcilationID(ref errorMsg);


            gvget.DataSource = dt;
            gvget.DataBind();




        }
        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (FileUpload1.HasFile || FileUpload1.HasFile)
                {
                    Random rand = new Random((int)DateTime.Now.Ticks);
                    int randnum = 0;

                    string fn = "";
                    string ext = "";
                    //string ConStr = "";

                    string destDir = Server.MapPath("~/assets/files/temp/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
                    randnum = rand.Next(1, 100000);
                    fn = Common.RemoveSpecialCharacter(FileUpload1.FileName).ToLower().Replace(" ", "-") + "_" + randnum;

                    if (!Directory.Exists(destDir))
                    {
                        Directory.CreateDirectory(destDir);
                    }

                    string fname = Path.GetFileName(FileUpload1.PostedFile.FileName);
                    ext = Path.GetExtension(FileUpload1.PostedFile.FileName);
                    filePath = destDir + fn + ext;
                    FileUpload1.PostedFile.SaveAs(filePath);

                    //Read the contents of CSV file.
                    string csvData = File.ReadAllText(filePath);

                    dtReconcile = createTempTable();

                    //Execute a loop over the rows.
                    foreach (string row in csvData.Split('\n'))
                    {
                        if (!string.IsNullOrEmpty(row))
                        {
                            dtReconcile.Rows.Add();
                            int i = 0;

                            string rawData = row.Remove(row.Length - 1);

                            //Execute a loop over the columns.
                            dtReconcile.Rows[dtReconcile.Rows.Count - 1][i] = (dtReconcile.Rows.Count - 1).ToString();
                            i++;
                            foreach (string cell in rawData.Split(','))
                            {
                                dtReconcile.Rows[dtReconcile.Rows.Count - 1][i] = cell.Replace("\"", "");
                                i++;
                            }

                            dtReconcile.AcceptChanges();
                        }
                    }

                    dtReconcile.Rows[0].Delete();
                    dtReconcile.AcceptChanges();


                    DataTable dtE = new DataTable();
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                    objDB.FromDate = txtFromDate.Value;
                    objDB.ToDate = txtToDate.Value;
                    objDB.AccountID = Convert.ToInt32(ddlAccount.SelectedValue);
                    dtE = objDB.GetAllExpensesBank(ref errorMsg);
                    if (dtE != null)
                    {
                        if (dtE.Rows.Count > 0)
                        {

                            for (int InvLength = 0; InvLength < dtE.Rows.Count; InvLength++)
                            {
                                for (int statementLength = 0; statementLength < dtReconcile.Rows.Count; statementLength++)
                                {
                                    string expDate = DateTime.Parse(dtE.Rows[InvLength]["DateOfExpense"].ToString()).ToString("dd-MMM-yyyy");
                                    string transictionDate = DateTime.Parse(dtReconcile.Rows[statementLength]["TransictionDate"].ToString().Trim()).ToString("dd-MMM-yyyy");

                                    float expAmount = float.Parse(dtE.Rows[InvLength]["TotalBalance"].ToString());
                                    float transictionAmount = dtReconcile.Rows[statementLength]["Credit"].ToString() == "" ? 0 : float.Parse(dtReconcile.Rows[statementLength]["Credit"].ToString());

                                    //add another condition to match amount
                                    if (expDate == transictionDate && expAmount == transictionAmount)
                                    {
                                        dtReconcile.Rows[statementLength].SetField(5, "Matched by Voucher NO:" + dtE.Rows[InvLength]["ExpenseNOTitle"].ToString());
                                        dtReconcile.Rows[statementLength].SetField(6, dtE.Rows[InvLength]["ExpenseID"].ToString());

                                        dtReconcile.AcceptChanges();
                                        break;
                                    }
                                }
                            }
                        }
                    }


                    DataTable dtI = new DataTable();
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                    objDB.FromDate = txtFromDate.Value;
                    objDB.ToDate = txtToDate.Value;
                    objDB.AccountID = Convert.ToInt32(ddlAccount.SelectedValue);
                    dtI = objDB.GetAllInvoicesPayHistoryBank(ref errorMsg);
                    if (dtI != null)
                    {
                        if (dtI.Rows.Count > 0)
                        {

                            for (int expLength = 0; expLength < dtI.Rows.Count; expLength++)
                            {
                                for (int statementLength = 0; statementLength < dtReconcile.Rows.Count; statementLength++)
                                {
                                    string expDate = DateTime.Parse(dtI.Rows[expLength]["LastReceived"].ToString()).ToString("dd-MMM-yyyy");
                                    string transictionDate = DateTime.Parse(dtReconcile.Rows[statementLength]["TransictionDate"].ToString().Trim()).ToString("dd-MMM-yyyy");

                                    float expAmount = float.Parse(dtI.Rows[expLength]["TotRecivedAmount"].ToString());
                                    float transictionAmount = dtReconcile.Rows[statementLength]["Debit"].ToString() == "" ? 0 : float.Parse(dtReconcile.Rows[statementLength]["Debit"].ToString());

                                    //add another condition to match amount
                                    if (expDate == transictionDate && expAmount == transictionAmount)
                                    {
                                        dtReconcile.Rows[statementLength].SetField(5, "Matched by Invoice NO:" + dtI.Rows[expLength]["InvoiceCodeTitle"].ToString());
                                        dtReconcile.Rows[statementLength].SetField(6, dtI.Rows[expLength]["InvoiceID"].ToString());
                                        dtReconcile.AcceptChanges();
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    gv.DataSource = dtReconcile;
                    gv.DataBind();
                }
            }
            catch (FormatException ex)
            {
                pAlertMsg.InnerHtml = "Please use dd-MMM-yyyy as Transiction Date Format";
                divAlertMsg.Visible = true;
            }
            catch (Exception ex)
            {

            }
        }

        private DataTable createTempTable()
        {
            DataTable dt = new DataTable();
            dt.TableName = "BankStatement";
            dt.Columns.Add("SrNO");
            dt.Columns.Add("TransictionDate");
            dt.Columns.Add("Description");
            dt.Columns.Add("Debit");
            dt.Columns.Add("Credit");
            dt.Columns.Add("Reconciled");
            dt.Columns.Add("Id");

            //dt.Columns.Add("BlankColumn");

            dt.AcceptChanges();
            return dt;
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "BankReconcilation", "BankReconcilationID", HttpContext.Current.Items["BRID"].ToString(), Session["UserName"].ToString());
            Common.addlogNew(res, "Finance", "Bank Reconcialition of ID\"" + HttpContext.Current.Items["BRID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/bank-reconciliation", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/bank-reconciliation/edit-bank-reconciliation-" + HttpContext.Current.Items["BRID"].ToString(), "Bank Reconcialition \"" + ddlAccount.SelectedItem.Text + "\"", "BankReconcilation", "BankReconciliation", Convert.ToInt32(HttpContext.Current.Items["BRID"].ToString()));

            //Common.addlog(res, "Finance", "Bank Reconcialition of ID \"" + HttpContext.Current.Items["BRID"].ToString() + "\" Status Changed", "BankReconcilation", BRID);
            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;

            CheckAccess();
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();

                string res = "";
                if (HttpContext.Current.Items["BRID"] != null)
                {

                    res = "Bank Reconcilation updated";
                }
                else
                {
                    objDB.AccountID = Convert.ToInt32(ddlAccount.SelectedValue);
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.FromDate = txtFromDate.Value;
                    objDB.ToDate = txtToDate.Value;
                    objDB.Path = filePath;
                    objDB.CreatedBy = Session["UserName"].ToString();

                    string ID = objDB.AddBankReconcilation();


                    if (ID != null && ID != "")
                    {
                        res = "Bank Reconcilation Added Successfully";
                    }
                    if (dtReconcile != null)
                    {
                        if (dtReconcile.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtReconcile.Rows.Count; i++)
                            {
                                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                                objDB.BankReconcilationID = Convert.ToInt32(ID);
                                objDB.GenrationDate = (dtReconcile.Rows[i]["TransictionDate"].ToString());
                                objDB.Description = (dtReconcile.Rows[i]["Description"].ToString());
                                if (dtReconcile.Rows[i]["Credit"] != null && dtReconcile.Rows[i]["Credit"].ToString() != "")
                                {
                                    objDB.Credit = float.Parse(dtReconcile.Rows[i]["Credit"].ToString());
                                }
                                else
                                {
                                    objDB.Credit = null;
                                }
                                if (dtReconcile.Rows[i]["Debit"] != null && dtReconcile.Rows[i]["Debit"].ToString() != "")
                                {
                                    objDB.Debit = float.Parse(dtReconcile.Rows[i]["Debit"].ToString());
                                }
                                else
                                {
                                    objDB.Debit = null;
                                }
                                if (dtReconcile.Rows[i]["Id"] != null && dtReconcile.Rows[i]["Id"].ToString() != "")
                                {
                                    objDB.MatchBy = Convert.ToInt32(dtReconcile.Rows[i]["Id"].ToString());
                                }
                                else
                                {
                                    objDB.MatchBy = null;
                                }
                                objDB.Comments = (dtReconcile.Rows[i]["Reconciled"].ToString());
                                objDB.CreatedBy = Session["UserName"].ToString();

                                objDB.AddBankReconcilationDetails();
                            }
                        }
                    }
                }
                if (res == "Bank Reconcilation Added Successfully" || res == "Bank Reconcilation updated")
                {
                    if (res == "Bank Reconcilation Added Successfully") { Common.addlog("Add", "Finance", " Bank Reconcilation  \"" + objDB.Description + "\" Added", "BankReconcilation"); }
                    if (res == "Bank Reconcilation updated") { Common.addlog("Update", "Finance", "Bank Reconcilation \"" + objDB.Description + "\" Updated", "BankReconcilation", objDB.BankReconcilationID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            string type = btn.CommandArgument;
            string res = Common.addAccessLevels(type, "BankReconcilation", "BankReconcilationID", HttpContext.Current.Items["BRID"].ToString(), Session["UserName"].ToString());
            Common.addlog("Delete", "Finance", "Bank Reconcialition of ID \"" + HttpContext.Current.Items["BRID"].ToString() + "\" deleted", "BankReconcilation", BRID);
            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            Response.Redirect(btnBack.HRef);

        }



        protected void ddlExpense_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            objDB.ExpencesID = Convert.ToInt32(ddlExpense.SelectedValue);
            dt = objDB.GetExpensesByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtDate.Value = DateTime.Parse(dt.Rows[0]["ExpenseDate"].ToString()).ToString("dd-MMM-yyyy");

                    txtAmount.Value = dt.Rows[0]["TotalBalance"].ToString();

                    txtDescription.Value = dt.Rows[0]["Description"].ToString();

                }

            }
        }

        protected void ddlInvoices_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            objDB.InvoiceID = Convert.ToInt32(ddlInvoices.SelectedValue);
            dt = objDB.GetInvoiceByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtDateInvoice.Value = DateTime.Parse(dt.Rows[0]["Date"].ToString()).ToString("dd-MMM-yyyy");

                    txtamountInvoice.Value = dt.Rows[0]["Amount"].ToString();

                    txtDescriptionInvoice.Value = dt.Rows[0]["Description"].ToString();

                }

            }

        }

        protected void btnReconcileInvoice_ServerClick(object sender, EventArgs e)
        {
            if (HttpContext.Current.Items["BRID"] != null)
            {
                string res;
                objDB.BankReconcilationDetailID = Convert.ToInt32(hdnID.Value);
                objDB.MatchBy = Convert.ToInt32(ddlInvoices.SelectedValue);
                objDB.Comments = "Matched by Invoice NO:" + (ddlInvoices.SelectedItem.Text).ToString();
                objDB.ModifiedBy = Session["UserName"].ToString();
                res = objDB.UpdateBankReconcilationDetails();
                if (res == "Bank Reconcilation Details updated")
                {
                    Common.addlog("Update", "Finance", "Bank Reconcilation Details of ID\"" + (objDB.BankReconcilationDetailID).ToString() + "\" Updated", "BankReconcilation", objDB.BankReconcilationDetailID);

                    GetBankReconcilationDetail(BRID);
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            else
            {
                dtReconcile.Rows[Convert.ToInt32(hdnID.Value) - 1].SetField(5, "Matched by Invoice NO:" + (ddlInvoices.SelectedItem.Text).ToString());

                dtReconcile.Rows[Convert.ToInt32(hdnID.Value) - 1].SetField(6, (ddlInvoices.SelectedValue).ToString());
                dtReconcile.AcceptChanges();
                gv.DataSource = dtReconcile;
                gv.DataBind();
            }
        }
        protected void btnReconcile_ServerClick(object sender, EventArgs e)
        {
            if (HttpContext.Current.Items["BRID"] != null)
            {
                string res;
                objDB.BankReconcilationDetailID = Convert.ToInt32(hdnID.Value);
                objDB.MatchBy = Convert.ToInt32(ddlExpense.SelectedValue);
                objDB.Comments = "Matched by Expense NO:" + (ddlExpense.SelectedItem.Text).ToString();
                objDB.ModifiedBy = Session["UserName"].ToString();
                res = objDB.UpdateBankReconcilationDetails();
                if (res == "Bank Reconcilation Details updated")
                {
                    Common.addlog("Update", "Finance", "Bank Reconcilation Details of ID\"" + (objDB.BankReconcilationDetailID).ToString() + "\" Updated", "BankReconcilation", objDB.BankReconcilationDetailID);
                    GetBankReconcilationDetail(BRID);
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;

                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;

                }
            }
            else
            {
                dtReconcile.Rows[Convert.ToInt32(hdnID.Value) - 1].SetField(5, "Matched by Expense NO:" + (ddlExpense.SelectedItem.Text).ToString());

                dtReconcile.Rows[Convert.ToInt32(hdnID.Value) - 1].SetField(6, (ddlExpense.SelectedValue).ToString());
                dtReconcile.AcceptChanges();
                gv.DataSource = dtReconcile;
                gv.DataBind();
            }

        }

    }
}