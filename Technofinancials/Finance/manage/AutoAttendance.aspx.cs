﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class AutoAttendance : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                   
                    
                    BindDropdown();
                    btnSave.Visible = true;
                    //btnApprove.Visible = false;
                    //btnReview.Visible = false;
                    //btnRevApprove.Visible = false;
                    //lnkReject.Visible = false;
                    //lnkDelete.Visible = false;
                    //btnSubForReview.Visible = false;
                    //btnDisapprove.Visible = false;
                    //btnRejDisApprove.Visible = false;

                    //btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/AddOnsExpenseNew";

                    divAlertMsg.Visible = false;

                   
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
              

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "AddOns";
                objDB.PrimaryColumnnName = "AddOnsID";
          
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                //if (chkAccessLevel == "Can Edit")
                //{
                //    btnSave.Visible = true;
                //    lnkDelete.Visible = true;
                //    btnSubForReview.Visible = true;
                //}
                //if (chkAccessLevel == "Can Edit & Review")
                //{
                //    btnSave.Visible = true;
                //    btnReview.Visible = true;
                //    lnkReject.Visible = true;

                //}
                //if (chkAccessLevel == "Can Edit & Approve")
                //{
                //    btnSave.Visible = true;
                //    btnApprove.Visible = true;
                //    btnDisapprove.Visible = true;
                //}
                //if (chkAccessLevel == "Can Edit, Review & Approve")
                //{
                //    btnSave.Visible = true;
                //    btnRevApprove.Visible = true;

                //    btnRejDisApprove.Visible = true;
                //}
                //if (chkAccessLevel == "View & Edit")
                //{
                //    //btnSave.Visible = true;
                //}
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

      
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

       
      

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                divAlertMsg.Visible = false;
                bool flag = autoattendance.Checked == true ? true : false;
                List<string> emps = new List<string>();
				foreach (ListItem item in lstEmployees.Items)
				{
					if (!item.Selected)
					{
						continue;
					}



                    emps.Add(item.Value);
                    Common.addlog("AutoAttendance", "Finance", "Department of ID \"" + ddldept.SelectedItem.Text + "\" updated - " + flag + " By user: -" + Session["Username"].ToString() + "", "Employees",Convert.ToInt32(item.Value));


                }


               
                string res =""; 
                if(res== "Auto Attendance")
				{
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = "Auto attendance flag updated";
                   
                }
                else
				{
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            

               
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

     
    
       protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "AddOns", "AddOnsID", HttpContext.Current.Items["AddOnsExpenseID"].ToString(), Session["UserName"].ToString());
                
                //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                //{
                //    objDB.AddOnsExpenseID = AddOnsExpenseID;
                //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //    objDB.AddCurrencyToEmployeeAttendance();
                //}

                //Common.addlog("Delete", "HR", "Currency of ID \"" + objDB.AddOnsExpenseID + "\" deleted", "Currency", objDB.AddOnsExpenseID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }

    

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
             
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteAddOnsExpenseByID();
               
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "AddOns Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

     
        private void BindDropdown()
        {
            try
            {
                
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                ddldept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                ddldept.DataTextField = "DeptName";
                ddldept.DataValueField = "DeptID";
                ddldept.DataBind();
                ddldept.Items.Insert(0, new ListItem("ALL", "0"));


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

      
      

    


        protected void ddldept_SelectedIndexChanged(object sender, EventArgs e)
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DeptID = Convert.ToInt32(ddldept.SelectedValue);


            ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyIDandDptIDonly(ref errorMsg);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
            lstEmployees.DataSource = objDB.GetAllApproveEmployeesByCompanyIDandDptIDonly(ref errorMsg);
            lstEmployees.DataTextField = "EmployeeName";
            lstEmployees.DataValueField = "EmployeeID";
            lstEmployees.DataBind();


            DataTable dr = objDB.GetAllAutoAttendanceEmployeesByDeptID(ref errorMsg);
            gv.DataSource = dr;
            gv.DataBind();
            //lstEmployees.Items.Insert(0, new ListItem("All", "0"));
            // generateCode();
        }






    }
}