﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class Quotation : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        int QuoteID
        {
            get
            {
                if (ViewState["QuoteID"] != null)
                {
                    return (int)ViewState["QuoteID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["QuoteID"] = value;
            }
        }
        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                txtDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");
                BindDropDowns();

                ViewState["dtTaxTable"] = null;

                txtCode.Disabled = true;

                ViewState["srNo"] = null;
                ViewState["dtRFQDetails"] = null;
                ViewState["showFirstRow"] = null;
                dtRFQDetails = createTable();


                BindCOA();
                divAttachments.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;
                divAlertMsg.Visible = false;

                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/qoutation";

                BindTaxesListBox();
               // txtRFQCode.Value = objDB.GenerateRFQCode();
                showFirstRow = false;
                DataTable dtCode = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                dtCode = objDB.GetQuatationCode(ref errorMsg);
                if (dtCode != null)
                {
                    if (dtCode.Rows.Count > 0)
                    {
                        txtCode.Value = dtCode.Rows[0][0].ToString();
                    }
                }
                if (HttpContext.Current.Items["QuoteID"] != null)
                {
                    QuoteID = Convert.ToInt32(HttpContext.Current.Items["QuoteID"].ToString());
                    getQuotationDetails(QuoteID);
                    showFirstRow = true;
                    CheckAccess();
                }

                if (HttpContext.Current.Items["ItemID"] != null)
                {
                    RFQDIV.Visible = false;
                    int itemID = Convert.ToInt32(HttpContext.Current.Items["ItemID"].ToString());
                    getItemDetails(itemID);
                    showFirstRow = true;

                }

                BindData();

            }
        }
        private void BindCOA()
        {
            //objDB.CategoriesList = "\'CGS/COS\',\'Revenue\',\'Other Rev.\'";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOA.Items.Clear();
            //ddlCOA.DataSource = objDB.GetCOAByCategory(ref errorMsg);
            ddlCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

        }
        private void getItemDetails(int itemID)
        {
            if (CheckSessions())
            {
                DataTable dt = new DataTable();
                objDB.ItemID = itemID;
                dt = objDB.GetProductAndServiceByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        //ddlSupplier.SelectedValue = dt.Rows[0]["PreferredSupplierID"].ToString();
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                       // txtCode.Value = objDB.GenereatePOCodeForQuickPurchase();


                        if (dtRFQDetails.Rows[0][0].ToString() == "")
                        {
                            dtRFQDetails.Rows[0].Delete();
                            dtRFQDetails.AcceptChanges();
                        }

                        DataRow dr = dtRFQDetails.NewRow();
                        dr[0] = "1";
                        dr[1] = dt.Rows[0]["ItemCode"].ToString();
                        dr[2] = dt.Rows[0]["ItemID"].ToString();
                        dr[3] = dt.Rows[0]["MeasurmentUnit"].ToString();
                        dr[4] = "0";
                        dr[5] = dt.Rows[0]["UnitPrice"].ToString();
                        dr[6] = "0";
                        dr[7] = "";
                        srNo = 2;

                        dtRFQDetails.Rows.Add(dr);
                        dtRFQDetails.AcceptChanges();

                    }
                }
            }
        }

        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApprove.Visible = false;
            btnReview.Visible = false;
            btnRevApprove.Visible = false;
            lnkReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReview.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;



            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "Quotations";
            objDB.PrimaryColumnnName = "QuoteID";
            objDB.PrimaryColumnValue = QuoteID.ToString();
            objDB.DocName = "Quotation";

            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

            if (chkAccessLevel == "Can Edit")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReview.Visible = true;
            }
            if (chkAccessLevel == "Can Edit & Review")
            {
                btnSave.Visible = true;
                btnReview.Visible = true;
                lnkReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve")
            {
                btnSave.Visible = true;
                btnApprove.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve")
            {
                btnSave.Visible = true;
                btnRevApprove.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit")
            {
                btnSave.Visible = true;
            }
        }

        protected DataTable dtTaxTable
        {
            get
            {
                if (ViewState["dtTaxTable"] != null)
                {
                    return (DataTable)ViewState["dtTaxTable"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtTaxTable"] = value;
            }
        }

        private void BindTaxesListBox()
        {
            if (CheckSessions())
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                dtTaxTable = objDB.GetAllTaxRates(ref errorMsg);
                lbTaxes.DataSource = dtTaxTable;
                lbTaxes.DataTextField = "TaxRateShortName";
                lbTaxes.DataValueField = "TaxRateID";
                lbTaxes.DataBind();
            }
        }



        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
            return true;
        }

        private void getQuotationDetails(int QuoteID)
        {
            DataTable dt = new DataTable();
            objDB.QuoteID = QuoteID;
            dt = objDB.GetQuotationByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlClient.SelectedValue = dt.Rows[0]["ClientID"].ToString();
                    ddlLocation.SelectedValue = dt.Rows[0]["SiteID"].ToString();
                    ddlCOA.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                    txtCode.Value = dt.Rows[0]["Code"].ToString();
                    txtDate.Value = dt.Rows[0]["Date"].ToString();
                    txtNetAmount.Value = dt.Rows[0]["Amount"].ToString();
                    txtNotes.Value = dt.Rows[0]["Notes"].ToString();

                    getQuotationItemsByQuoteID(QuoteID);
                    getQuotationAttachmentsByQuoteID(QuoteID);
                    GetQuotationTaxesByQuoteID(QuoteID);
                }
            }
            Common.addlog("View", "Finance", "Quotation \"" + txtCode.Value + "\" Viewed", "Quotations", objDB.QuoteID);

        }

        private void GetQuotationTaxesByQuoteID(int QuoteID)
        {
            if (CheckSessions())
            {
                DataTable dt = new DataTable();
                objDB.QuoteID = QuoteID;
                dt = objDB.GetQuotationTaxesByQuoteID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            foreach (ListItem li in lbTaxes.Items)
                            {
                                if (dt.Rows[i]["TaxRateID"].ToString() == li.Value)
                                    li.Selected = true;
                            }
                        }
                    }
                }
            }
        }

        private void getQuotationAttachmentsByQuoteID(int QuoteID)
        {
            DataTable dt = new DataTable();
            objDB.QuoteID = QuoteID;
            dt = objDB.GetQuotationAttachmentsByQuoteID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    divAttachments.Visible = true;
                    gvAttachments.DataSource = dt;
                    gvAttachments.DataBind();
                }
            }
        }

        private void getQuotationItemsByQuoteID(int QuoteID)
        {
            DataTable dt = new DataTable();
            objDB.QuoteID = QuoteID;
            dt = objDB.GetQuotationItemsByQuoteID(ref errorMsg);
            dtRFQDetails = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }
        }

        private void BindDropDowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlLocation.DataSource = objDB.GetAllApprovedLocations(ref errorMsg);
            ddlLocation.DataTextField = "SiteName";
            ddlLocation.DataValueField = "SiteID";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("--- Select Location ---", "0"));
            ddlLocation.SelectedIndex = 0;

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlClient.DataSource = objDB.GetAllApprovedClients(ref errorMsg);
            ddlClient.DataTextField = "ClientName";
            ddlClient.DataValueField = "ClientID";
            ddlClient.DataBind();
            ddlClient.Items.Insert(0, new ListItem("--- Select Client ---", "0"));
            ddlClient.SelectedIndex = 0;

            //ddlPRs.DataSource = objDB.GetActivePurchaseRequisite(ref errorMsg);
            //ddlPRs.DataTextField = "PRCode";
            //ddlPRs.DataValueField = "PRID";
            //ddlPRs.DataBind();
            //ddlPRs.Items.Insert(0, new ListItem("--- Select PR ---", "0"));
            //ddlPRs.Items.Insert(1, new ListItem("--- None ---", "0"));
            //ddlPRs.SelectedIndex = 0;
        }

        protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            //code for generating pr code goes here
        }

        protected DataTable dtRFQDetails
        {
            get
            {
                if (ViewState["dtRFQDetails"] != null)
                {
                    return (DataTable)ViewState["dtRFQDetails"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtRFQDetails"] = value;
            }
        }
        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }

        protected void BindData()
        {
            if (ViewState["dtRFQDetails"] == null)
            {
                dtRFQDetails = createTable();
                ViewState["dtRFQDetails"] = dtRFQDetails;
            }
            else if (dtRFQDetails.Rows.Count == 0)
            {
                dtRFQDetails = createTable();
                showFirstRow = false;
            }
            gv.DataSource = dtRFQDetails;
            gv.DataBind();

            if (showFirstRow)
                gv.Rows[0].Visible = true;
            else
                gv.Rows[0].Visible = false;

            gv.UseAccessibleHeader = true;
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;

            calcTotal();
        }

        private DataTable createTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("ItemCode");
            dt.Columns.Add("ProductAndServicesID");
            dt.Columns.Add("ItemUnit");
            dt.Columns.Add("Quantity");
            dt.Columns.Add("UnitPrice");
            dt.Columns.Add("NetPrice");
            dt.Columns.Add("Remarks");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();


            return dt;

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                DropDownList ddlItemCode = e.Row.FindControl("ddlItems") as DropDownList;
                ddlItemCode.DataSource = objDB.GetAllApprovedProductAndService(ref errorMsg);
                ddlItemCode.DataTextField = "Name";
                ddlItemCode.DataValueField = "ProductAndServicesID";
                ddlItemCode.DataBind();
                ddlItemCode.Items.Insert(0, new ListItem("--- Select---", "0"));


                //DropDownList ddlSuppliers = e.Row.FindControl("ddlSuppliers") as DropDownList;
                //ddlSuppliers.DataSource = objDB.GetActiveSupplier(ref errorMsg);
                //ddlSuppliers.DataTextField = "SupplierName";
                //ddlSuppliers.DataValueField = "SupplierID";
                //ddlSuppliers.DataBind();
                //ddlSuppliers.Items.Insert(0, new ListItem("--- None ---", "0"));


                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = srNo.ToString();

                e.Row.Cells[7].ColumnSpan = 2;
                e.Row.Cells.RemoveAt(8);
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //gv.UseAccessibleHeader = true;
                //e.Row.TableSection = TableRowSection.TableHeader;
                e.Row.Cells[7].ColumnSpan = 2;
                e.Row.Cells.RemoveAt(8);
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    //DropDownList ddList = (DropDownList)e.Row.FindControl("ddlEditSuppliers");
                    //ddList.DataSource = objDB.GetActiveSupplier(ref errorMsg);
                    //ddList.DataTextField = "SupplierName";
                    //ddList.DataValueField = "SupplierID";
                    //ddList.DataBind();

                    //DataRowView dr = e.Row.DataItem as DataRowView;
                    //ddList.Items.IndexOf(ddList.Items.FindByText(dr[2].ToString()));
                    //ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));

                    hdnRowNo.Value = e.Row.RowIndex.ToString();
                }
            }

            //if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
            //{
            //    if (!string.IsNullOrEmpty(e.Row.Cells[6].Text.ToString()))
            //    {
            //        total_Amount += float.Parse(e.Row.Cells[6].Text.ToString());
            //    }
            //}

            //txtTotalAmount.Value = total_Amount.ToString();
        }
        protected void calcTotal()
        {
            float total_Amount = 0;
            if (dtRFQDetails != null)
            {
                if (dtRFQDetails.Rows.Count > 0)
                {
                    foreach (DataRow row in dtRFQDetails.Rows)
                    {
                        if (!string.IsNullOrEmpty(row[6].ToString()))
                        {
                            total_Amount += float.Parse(row[6].ToString());
                        }
                    }
                    float tempAmount = total_Amount;
                    txtTotalAmount.Value = total_Amount.ToString();
                    for (int i = 0; i < lbTaxes.Items.Count; i++)
                    {

                        if (lbTaxes.Items[i].Selected == true)
                        {
                            float per = float.Parse(dtTaxTable.Rows[i]["TaxRatePer"].ToString());
                            total_Amount = (total_Amount + ((tempAmount * per) / 100.0f));
                        }
                    }

                    txtTotalTax.Value = (total_Amount - tempAmount).ToString();
                }
            }
            txtNetAmount.Value = total_Amount.ToString();
            
            ((Label)gv.FooterRow.FindControl("lblTotal")).Text = "Total : " + string.Format("{0:f2}", total_Amount);
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CheckSessions();
            bool similar = false;
            foreach (DataRow row in dtRFQDetails.Rows)
            {
                if (row[1].ToString() == ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Text)
                {
                    //string supplier = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Text;
                    //string supplierID = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Value;
                    string itemUnit = ((DropDownList)gv.FooterRow.FindControl("ddlUnit")).SelectedItem.Text;
                    string qty = (Convert.ToInt32(row[4]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtQty")).Text)).ToString();
                    string unitPrice = (Convert.ToInt32(row[5]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtUnitPrice")).Text)).ToString();
                    string totalPrice = (Convert.ToInt32(row[6]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtNetPrice")).Text)).ToString();

                    string remarks = row[7].ToString() + "," + ((TextBox)gv.FooterRow.FindControl("txtRemarks")).Text;

                    //row.SetField(3, supplier);
                    //row.SetField(4, supplierID);
                    row.SetField(3, itemUnit);
                    row.SetField(4, qty);
                    row.SetField(5, unitPrice);
                    row.SetField(6, totalPrice);
                    row.SetField(7, remarks);

                    dtRFQDetails.AcceptChanges();

                    similar = true;
                    break;
                }


            }

            if (!similar)
            {
                DataRow dr = dtRFQDetails.NewRow();
                dr[0] = srNo.ToString();
                dr[1] = ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Text;
                dr[2] = ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Value;
                //dr[3] = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Text;
                //dr[4] = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Value;
                dr[3] = ((DropDownList)gv.FooterRow.FindControl("ddlUnit")).SelectedItem.Text;
                dr[4] = ((TextBox)gv.FooterRow.FindControl("txtQty")).Text;
                dr[5] = ((TextBox)gv.FooterRow.FindControl("txtUnitPrice")).Text;
                dr[6] = ((TextBox)gv.FooterRow.FindControl("txtNetPrice")).Text;
                dr[7] = ((TextBox)gv.FooterRow.FindControl("txtRemarks")).Text;
                dtRFQDetails.Rows.Add(dr);
                dtRFQDetails.AcceptChanges();

                srNo += 1;
            }
            BindData();


            ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
            calcTotal();

        }

        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void gv_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {

        }

        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            //dtRFQDetails.Rows[index].SetField(3, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditSuppliers")).SelectedItem.Text);
            //dtRFQDetails.Rows[index].SetField(4, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditSuppliers")).SelectedItem.Value);
            dtRFQDetails.Rows[index].SetField(3, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditUnit")).SelectedItem.Text);
            dtRFQDetails.Rows[index].SetField(4, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditQty")).Text);
            dtRFQDetails.Rows[index].SetField(5, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditUnitPrice")).Text);
            dtRFQDetails.Rows[index].SetField(6, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditNetPrice")).Text);
            dtRFQDetails.Rows[index].SetField(7, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditRemarks")).Text);


            dtRFQDetails.AcceptChanges();

            gv.EditIndex = -1;
            BindData();
            calcTotal();
        }

        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindData();
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {

            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
            {
                if (dtRFQDetails.Rows[i][0].ToString() == delSr)
                {
                    dtRFQDetails.Rows[i].Delete();
                    dtRFQDetails.AcceptChanges();
                }
            }
            for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
            {
                dtRFQDetails.Rows[i].SetField(0, i);
                dtRFQDetails.AcceptChanges();
            }
            srNo = dtRFQDetails.Rows.Count;
            BindData();
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                BindData();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Code = txtCode.Value;
                objDB.ClientID = Convert.ToInt32(ddlClient.SelectedItem.Value);
                objDB.SiteID = Convert.ToInt32(ddlLocation.SelectedItem.Value);
                objDB.Date = txtDate.Value;
                objDB.TotalAmount = float.Parse(txtNetAmount.Value);
                objDB.Notes = txtNotes.Value;
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.ChartOfAccountID = int.Parse(ddlCOA.SelectedValue);

                if (HttpContext.Current.Items["QuoteID"] != null)
                {
                    objDB.QuoteID = QuoteID;
                    objDB.Status = "Active"; //this would be changed
                    res = objDB.UpdateQuotation();
                    //getRFQAttachmentsByRFQID(RFQID);
                    //showAttachments = true;

                    foreach (ListItem li in lbTaxes.Items)
                    {
                        objDB.TaxRateID = Convert.ToInt32(li.Value);
                        if (li.Selected == true)
                        {
                            objDB.AddQuotationTax();
                        }
                        else
                        {
                            objDB.DelQuotationTaxes();
                        }
                    }
                }
                else
                {
                    string returnedID = objDB.AddQuotation();
                    QuoteID = Convert.ToInt32(returnedID);

                    res = "New Quotation Added Successfully";

                    foreach (ListItem li in lbTaxes.Items)
                    {
                        objDB.QuoteID = QuoteID;
                        objDB.TaxRateID = Convert.ToInt32(li.Value);
                        if (li.Selected == true)
                        {
                            objDB.AddQuotationTax();
                        }
                    }



                    clearFields();


                }

                if (itemAttachments.HasFile || itemAttachments.HasFiles)
                {
                    foreach (HttpPostedFile file in itemAttachments.PostedFiles)
                    {
                        Random rand = new Random((int)DateTime.Now.Ticks);
                        int randnum = 0;
                        randnum = rand.Next(1, 100000);

                        string fn = "";
                        string exten = "";

                        string destDir = Server.MapPath("~/assets/files/Quotations/");
                        randnum = rand.Next(1, 100000);
                        fn = txtCode.Value + "_" + randnum;

                        if (!Directory.Exists(destDir))
                        {
                            Directory.CreateDirectory(destDir);
                        }

                        string fname = Path.GetFileName(file.FileName);
                        exten = Path.GetExtension(file.FileName);
                        file.SaveAs(destDir + fn + exten);

                        objDB.QuoteID = Convert.ToInt32(QuoteID);
                        objDB.AttachmentPath = "http://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/Quotations/" + fn + exten;
                        objDB.AddQuotationAttachments();
                    }
                }

                objDB.QuoteID = QuoteID;
                objDB.DeleteQuotationItemsByQuoteID();

                dtRFQDetails = (DataTable)ViewState["dtRFQDetails"] as DataTable;
                if (dtRFQDetails != null)
                {
                    if (dtRFQDetails.Rows.Count > 0)
                    {
                        if (dtRFQDetails.Rows[0][0].ToString() == "")
                        {
                            dtRFQDetails.Rows[0].Delete();
                            dtRFQDetails.AcceptChanges();
                        }

                        for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
                        {
                            objDB.QuoteID = Convert.ToInt32(QuoteID);
                            objDB.ItemID = Convert.ToInt32(dtRFQDetails.Rows[i]["ProductAndServicesID"].ToString());
                            objDB.ItemUnit = dtRFQDetails.Rows[i]["ItemUnit"].ToString();
                            objDB.Qty = Convert.ToInt32(dtRFQDetails.Rows[i]["Quantity"].ToString());
                            objDB.UnitPrice = Convert.ToInt32(dtRFQDetails.Rows[i]["UnitPrice"].ToString());
                            objDB.NetPrice = Convert.ToInt32(dtRFQDetails.Rows[i]["NetPrice"].ToString());
                            objDB.Remarks = dtRFQDetails.Rows[i]["Remarks"].ToString();
                            objDB.ModifiedBy = Session["UserName"].ToString();

                            objDB.AddQuotationItems();
                        }
                    }
                }


                if (!showFirstRow)
                {
                    ViewState["dtRFQDetails"] = null;
                    srNo = 1;
                }

                BindData();

                if (res == "New Quotation Added Successfully" || res == "Quatation Updated Successfully")
                {
                    if (res == "New Quotation Added Successfully") { Common.addlog("Add", "Finance", " Quotation \"" + objDB.Code + "\" Added", "Quotations"); }
                    if (res == "Quatation Updated Successfully") { Common.addlog("Update", "Finance", "Quotation \"" + objDB.Code + "\" Updated", "Quotations", objDB.QuoteID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        private void clearFields()
        {
            txtNotes.Value = "";
            //dtRFQDetails = createTable();
            ddlClient.SelectedIndex = 0;
            ddlCOA.SelectedIndex = 0;
            ddlLocation.SelectedIndex = 0;
            txtCode.Value = "";
            txtTotalAmount.Value = "0";
            txtTotalTax.Value = "0";
            txtNetAmount.Value = "0";
            txtNotes.Value = "";
            DataTable dtCode = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            dtCode = objDB.GetQuatationCode(ref errorMsg);
            if (dtCode != null)
            {
                if (dtCode.Rows.Count > 0)
                {
                    txtCode.Value = dtCode.Rows[0][0].ToString();
                }
            }
            foreach (ListItem li in lbTaxes.Items)
            {
                li.Selected = false;
            }

        }

        protected void ddlRFQ_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            if (ddlLocation.SelectedIndex != 0)
            {
                objDB.RFQID = Convert.ToInt32(ddlLocation.SelectedItem.Value);
                txtCode.Value = objDB.GenereatePOCodeByRFQID();
                getRFQItemsByRFQID(Convert.ToInt32(ddlLocation.SelectedItem.Value));
                calcTotal();
            }
            else
            {
                txtCode.Value = "";
                dtRFQDetails = createTable();
            }
            BindData();
        }

        protected void txtNetPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            TextBox txtQty = gv.FooterRow.FindControl("txtQty") as TextBox;
            string qty = txtQty.Text;
            TextBox txtUnitPrice = gv.FooterRow.FindControl("txtUnitPrice") as TextBox;
            string unitPrice = txtUnitPrice.Text;
            TextBox txtRemarks = gv.FooterRow.FindControl("txtRemarks") as TextBox;

            if (!string.IsNullOrEmpty(qty) && !string.IsNullOrEmpty(unitPrice))
            {
                TextBox txtNetPrice = gv.FooterRow.FindControl("txtNetPrice") as TextBox;
                string netPrice = (Convert.ToInt32(qty) * Convert.ToInt32(unitPrice)).ToString();

                txtNetPrice.Text = netPrice;
            }
            TextBox txtSender = (TextBox)sender as TextBox;
            if (txtSender.ID == "txtQty")
                txtUnitPrice.Focus();
            else if (txtSender.ID == "txtUnitPrice")
                txtRemarks.Focus();

        }

        protected void txtEditUnitPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            int rowNum = Convert.ToInt32(hdnRowNo.Value);
            TextBox txtQty = gv.Rows[rowNum].FindControl("txtEditQty") as TextBox;
            string qty = txtQty.Text;
            TextBox txtUnitPrice = gv.Rows[rowNum].FindControl("txtEditUnitPrice") as TextBox;
            string unitPrice = txtUnitPrice.Text;

            if (!string.IsNullOrEmpty(qty) && !string.IsNullOrEmpty(unitPrice))
            {
                TextBox txtNetPrice = gv.Rows[rowNum].FindControl("txtEditNetPrice") as TextBox;
                string netPrice = (Convert.ToInt32(qty) * Convert.ToInt32(unitPrice)).ToString();

                txtNetPrice.Text = netPrice;
            }

            TextBox txtSender = (TextBox)sender as TextBox;
            txtSender.Focus();
        }

        protected void gv_PreRender(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
                gv.UseAccessibleHeader = true;
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void getRFQItemsByRFQID(int RFQID)
        {
            DataTable dt = new DataTable();
            objDB.RFQID = RFQID;
            dt = objDB.GetRFQItemsByRFQID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dtRFQDetails.Rows.Add(new object[] {
                            row[0],
                            row[1],
                            row[2],
                            row[3],
                            row[4],
                            "0",
                            "0",
                            row[5]
                        });

                        dtRFQDetails.AcceptChanges();
                    }
                    BindData();

                }
            }
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "Quotations", "QuoteID", HttpContext.Current.Items["QuoteID"].ToString(), Session["UserName"].ToString());
            Common.addlogNew(res, "Finance", "Quotation of ID\"" + HttpContext.Current.Items["QuoteID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/qoutation", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/quotation/edit-quotation-" + HttpContext.Current.Items["QuoteID"].ToString(), "Quotation \"" + txtCode.Value + "\"", "Quotations", "Quotation", Convert.ToInt32(HttpContext.Current.Items["QuoteID"].ToString()));

            //Common.addlog(res, "Finance", "Quotation of ID \"" + HttpContext.Current.Items["QuoteID"].ToString() + "\" Status Changed", "Quotations", QuoteID /* ID From Page Top  */);/* Button1_ServerClick  */
            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;

            CheckAccess();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            string type = btn.CommandArgument;
            string res = Common.addAccessLevels(type, "Quotations", "QuoteID", HttpContext.Current.Items["QuoteID"].ToString(), Session["UserName"].ToString());
            Common.addlog("Delete", "Finance", "Quotation of ID \"" + HttpContext.Current.Items["QuoteID"].ToString() + "\" deleted", "Quotations", QuoteID /* ID From Page Top  */);/* lnkDelete_Click  */
            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            Response.Redirect(btnBack.HRef);

        }

        protected void lbTaxes_SelectedIndexChanged(object sender, EventArgs e)
        {
            calcTotal();
        }

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

    }
}