﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.manage.Backup
{
    public partial class COAQuickSetup : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COABalTraID
        {
            get
            {
                if (ViewState["COABalTraID"] != null)
                {
                    return (int)ViewState["COABalTraID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COABalTraID"] = value;
            }
        }

        string TMStatus
        {
            get
            {
                if (ViewState["TMStatus"] != null)
                {
                    return ViewState["TMStatus"].ToString();
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["TMStatus"] = value;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/COAQuickSetup";

                    divAlertMsg.Visible = false;
                    clearFields();
                    BindToCOA();

                    lnkDelete.Visible = false;
                    spnMessage.Visible = false;

                    if (Session["From"] != null)
                    {
                        spnMessage.Visible = true;
                        ddlTitle.SelectedValue = Session["COATitle"].ToString();
                        spnMessage.InnerHtml = "Please add Chart of account for \"" + ddlTitle.SelectedItem.Text + "\" otherwise you can not operate Invoice, Purchaser Order and GRN Page.";
                    }

                    if (HttpContext.Current.Items["COAQuickSetupID"] != null)
                    {
                        COABalTraID = Convert.ToInt32(HttpContext.Current.Items["COAQuickSetupID"].ToString());
                        getDataByID(COABalTraID);

                    }

                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void BindToCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlToCOA.Items.Clear();
            ddlToCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlToCOA.DataTextField = "CodeTitle";
            ddlToCOA.DataValueField = "COA_ID";
            ddlToCOA.DataBind();
            ddlToCOA.Items.Insert(0, new ListItem("--- Select COA ---", "0"));

        }





        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.COAQuickSetupID = ID;
            dt = objDB.GetAllCOAQuickSetupByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlToCOA.SelectedValue = dt.Rows[0]["COAID"].ToString();
                    ddlTitle.SelectedValue = dt.Rows[0]["Title"].ToString();
                }
            }
            Common.addlog("View", "Finance", "Balance Transfer by ID \"" + (objDB.COAQuickSetupID).ToString() + "\" Viewed", "COABalanceTransfer", objDB.COAQuickSetupID);

        }

        private void clearFields()
        {
            ddlToCOA.SelectedIndex = -1;
            ddlTitle.SelectedValue = "0";

        }

        private void CheckSessions()
        {
            if (Session["userid"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.COAID = Convert.ToInt32(ddlToCOA.SelectedValue);
                objDB.Title = ddlTitle.SelectedValue;

                if (HttpContext.Current.Items["COAQuickSetupID"] != null)
                {
                    objDB.COAQuickSetupID = COABalTraID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOAQuickSetup();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOAQuickSetup();
                    if (res == "COA Quick Setup Added")
                    {
                        clearFields();
                        if (Session["From"] != null)
                        {
                            if (Session["COATitle"].ToString() == "Discount")
                            {
                                ddlTitle.SelectedValue = "WOT";
                                spnMessage.InnerHtml = "Please add Chart of account for \"withholding tax\" otherwise you can not operate Invoice, Purchaser Order and GRN Page.";
                            }
                            else if (Session["COATitle"].ToString() == "WOT")
                            {
                                ddlTitle.SelectedValue = "STWH";
                                spnMessage.InnerHtml = "Please add Chart of account for \"Sales tax withholding \" otherwise you can not operate Invoice, Purchaser Order and GRN Page.";
                            }
                            else
                            {
                                Session["From"] = null;
                                Response.Redirect(Session["FromURL"].ToString());
                            }
                        }
                    }
                }


                if (res == "COA Quick Setup Added" || res == "COA Quick Setup Updated")
                {
                    if (res == "COA Quick Setup Added") { Common.addlog("Add", "Finance", " Balance Transfer by ID\"" + (objDB.COAQuickSetupID).ToString() + "\" Added", "COABalanceTransfer"); }
                    if (res == "COA Quick Setup Updated") { Common.addlog("Update", "Finance", "Balance Transfer by ID \"" + (objDB.COAQuickSetupID).ToString() + "\" Updated", "COABalanceTransfer", objDB.COAQuickSetupID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "COABalanceTransfer", "COAQuickSetupID", HttpContext.Current.Items["COAQuickSetupID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "Balance Transfer of ID \"" + HttpContext.Current.Items["COAQuickSetupID"].ToString() + "\" deleted", "COABalanceTransfer", COABalTraID /* ID From Page Top  */);/* lnkDelete_Click  */

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        protected void ddlToCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlToCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }

}