﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.manage.Backup
{
    public partial class COASetup : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/payrolls";

                    divAlertMsg.Visible = false;
                    BindCOA();



                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }

        private void BindCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOA.Items.Clear();
            DataTable dt = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOA.DataSource = dt;
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

            // COA TAX

            ddlCOATax.Items.Clear();
            ddlCOATax.DataSource = dt;
            ddlCOATax.DataTextField = "CodeTitle";
            ddlCOATax.DataValueField = "COA_ID";
            ddlCOATax.DataBind();
            ddlCOATax.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

            //PFCOA

            ddlCOAPF.Items.Clear();
            ddlCOAPF.DataSource = dt;
            ddlCOAPF.DataTextField = "CodeTitle";
            ddlCOAPF.DataValueField = "COA_ID";
            ddlCOAPF.DataBind();
            ddlCOAPF.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.lblCOA = "EOBI COA";
                objDB.COA_ID = int.Parse(ddlCOA.SelectedValue);
                objDB.CreatedBy = Session["UserName"].ToString();
                res = objDB.AddPayrollCOA();

                objDB.lblCOA = "Tax COA";
                objDB.COA_ID = int.Parse(ddlCOATax.SelectedValue);
                res = objDB.AddPayrollCOA();

                objDB.lblCOA = "PF COA";
                objDB.COA_ID = int.Parse(ddlCOAPF.SelectedValue);
                res = objDB.AddPayrollCOA();

                if (res == "Updated" || res == "Added")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = "Data " + res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
    }
}