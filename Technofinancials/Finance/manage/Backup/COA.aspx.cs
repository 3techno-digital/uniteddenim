﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage.Backup
{
    public partial class COA : System.Web.UI.Page
    {


        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COAID
        {
            get
            {
                if (ViewState["COAID"] != null)
                {
                    return (int)ViewState["COAID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COAID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    BindCompanies();
                    BindAccounts();
                    BindTax();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/coa";

                    divAlertMsg.Visible = false;
                    clearFields();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["COAID"] != null)
                    {
                        COAID = Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString());
                        getDataByID(COAID);
                        CheckAccess();
                    }
                    else
                    {

                    }

                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "FAM_COA";
                objDB.PrimaryColumnnName = "COA_ID";
                objDB.PrimaryColumnValue = COAID.ToString();
                objDB.DocName = "ConfigureCOA";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.ChartOfAccountID = COAID;
            dt = objDB.GetCOAByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlCompany.SelectedValue = dt.Rows[0]["CompanyID"].ToString();
                    BindLocations(Convert.ToInt32(dt.Rows[0]["CompanyID"].ToString()));
                    ddlLocation.SelectedValue = dt.Rows[0]["Locations"].ToString();
                    ddlTax.SelectedValue = dt.Rows[0]["TaxRateID"].ToString();
                    ddlAccount.SelectedValue = dt.Rows[0]["AccountTypeID"].ToString();
                    txtAccountTitle.Value = dt.Rows[0]["SubAccount"].ToString();
                    txtAccountCode.Text = dt.Rows[0]["SubAccountCode"].ToString();
                    txtGLCode.Value = dt.Rows[0]["GL_CODE"].ToString();
                    txtOpeningBalance.Value = dt.Rows[0]["OpeningBalance"].ToString();
                    txtDescription.Value = dt.Rows[0]["DESCRIPTIONS"].ToString();

                }
            }
            Common.addlog("View", "Finance", "COA \"" + txtGLCode.Value + "\" Viewed", "FAM_COA", objDB.ChartOfAccountID);
        }

        private void clearFields()
        {
            txtAccountCode.Text = "";
            txtAccountTitle.Value = "";
            txtNotes.Value = "";
            generateCode();
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
                objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
                objDB.COA_AccountTypeID = Convert.ToInt32(ddlAccount.SelectedValue);
                objDB.TaxRateID = Convert.ToInt32(ddlTax.SelectedValue);
                objDB.Account = txtAccountTitle.Value;
                objDB.AccountCode = txtAccountCode.Text;
                objDB.GLCode = txtGLCode.Value;
                objDB.Description = txtDescription.Value;
                objDB.OpeningBalance = float.Parse((txtOpeningBalance.Value == "") ? "0" : txtOpeningBalance.Value);


                objDB.Notes = txtNotes.Value;


                if (HttpContext.Current.Items["COAID"] != null)
                {
                    // update coding
                    objDB.ChartOfAccountID = COAID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOA();
                }
                else
                {
                    // add coding
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOA();
                    clearFields();
                }

                if (res == "New COA Added" || res == "COA Updated")
                {
                    if (res == "New COA Added") { Common.addlog("Add", "Finance", " COA \"" + objDB.GLCode + "\" Added", "FAM_COA"); }
                    if (res == "COA Updated") { Common.addlog("Update", "Finance", "COA\"" + objDB.GLCode + "\" Updated", "FAM_COA", objDB.ChartOfAccountID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "Finance", "COA of ID\"" + HttpContext.Current.Items["COAID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/coa", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/coa/edit-coa-" + HttpContext.Current.Items["COAID"].ToString(), "COA \"" + txtAccountCode.Text + "\"", "FAM_COA", "ConfigureCOA", Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString()));

                //Common.addlog(res, "Finance", "COA of ID \"" + HttpContext.Current.Items["COAID"].ToString() + "\" Status Changed", "FAM_COA", COAID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "COA of ID \"" + HttpContext.Current.Items["COAID"].ToString() + "\" deleted", "FAM_COA", COAID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        private void BindCompanies()
        {
            ddlCompany.Items.Clear();
            objDB.ParentCompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCompany.DataSource = objDB.GetCompaniesByParentID(ref errorMsg);
            ddlCompany.DataTextField = "CompanyName";
            ddlCompany.DataValueField = "CompanyID";
            ddlCompany.DataBind();
            ddlCompany.Items.Insert(0, new ListItem("-- Select --", "0"));
        }


        private void BindTax()
        {
            ddlTax.Items.Clear();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlTax.DataSource = objDB.GetAllTaxRates(ref errorMsg);
            ddlTax.DataTextField = "TaxRateName";
            ddlTax.DataValueField = "TaxRateID";
            ddlTax.DataBind();
            ddlTax.Items.Insert(0, new ListItem("-- Select --", "0"));
        }


        private void BindLocations(int comID)
        {
            ddlLocation.Items.Clear();
            objDB.CompanyID = comID;
            ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
            ddlLocation.DataTextField = "NAME";
            ddlLocation.DataValueField = "LOCATION_ID";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("-- Select --", "0"));
        }


        private void BindAccounts()
        {
            ddlAccount.Items.Clear();
            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            ddlAccount.DataSource = objDB.GetAllCOA_Account(ref errorMsg);
            ddlAccount.DataTextField = "Name";
            ddlAccount.DataValueField = "AccountID";
            ddlAccount.DataBind();
            ddlAccount.Items.Insert(0, new ListItem("-- Select --", "0"));
        }

        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocations(Convert.ToInt32(ddlCompany.SelectedValue));
            generateCode();
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            generateCode();
        }


        protected void txtAccountCode_TextChanged(object sender, EventArgs e)
        {
            generateCode(false);
        }

        protected void ddlAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            generateCode();
        }

        void generateCode(bool isGenerateCode = true)
        {
            try
            {
                CheckSessions();

                if (ddlAccount.SelectedValue != "0" && ddlCompany.SelectedValue != "0" && ddlLocation.SelectedValue != "0" && isGenerateCode)
                {
                    txtAccountCode.Text = "";

                    objDB.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
                    objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
                    objDB.COA_AccountTypeID = int.Parse(ddlAccount.SelectedValue);

                    DataTable dtCode = objDB.Gen_COA_SubAccount(ref errorMsg);
                    if (dtCode != null)
                    {
                        if (dtCode.Rows.Count > 0)
                        {
                            txtAccountCode.Text = dtCode.Rows[0]["NewCode"].ToString();
                        }
                    }
                }


                if (ddlAccount.SelectedValue == "0" || ddlCompany.SelectedValue == "0" || ddlLocation.SelectedValue == "0" || txtAccountCode.Text == "")
                {
                    txtGLCode.Value = "";
                }
                else
                {
                    objDB.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
                    objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
                    objDB.COA_AccountTypeID = Convert.ToInt32(ddlAccount.SelectedValue);
                    DataTable coa_code = new DataTable();
                    coa_code = objDB.GetCOA_AccountCode(ref errorMsg);
                    if (coa_code != null)
                    {
                        txtGLCode.Value = coa_code.Rows[0][0].ToString() + txtAccountCode.Text;
                    }
                    else
                    {
                        txtGLCode.Value = "";
                    }

                }

            }
            catch (Exception ex)
            {
                txtGLCode.Value = "";
            }
        }

    }
}