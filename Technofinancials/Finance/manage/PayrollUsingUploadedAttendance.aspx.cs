﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClosedXML.Excel;

namespace Technofinancials.Finance.manage
{
    public partial class PayrollUsingUploadedAttendance : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int NewPayrollID
        {
            get
            {
                if (ViewState["NewPayrollID"] != null)
                {
                    return (int)ViewState["NewPayrollID"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["NewPayrollID"] = value;
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/payrolls2";
                    btnFnFSettlement.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/employee-fnf-settlement";
                    divAlertMsg.Visible = false;

                    clearFields();

                    ViewState["dtEmployeeSalaries"] = null;
                    btnFnFSettlement.Visible = 
                    btnReleaseActiveEmployees.Visible = 
                    btnApproveByHR.Visible =
                    btnReview.Visible =
                    btnRevApproveByHR.Visible = 
                    lnkDelete.Visible = 
                    btnSubForReview.Visible = 
                    btnDisapprove.Visible = false;
                    noPayroll.Visible = false;
                    downloadpayroll.Visible = false;



					if (HttpContext.Current.Items["PayrollID"] != null)
					{
						CheckAccess();
						NewPayrollID = Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString());
						getNewPayrollByID(NewPayrollID);
						txtPayrollDate.Enabled = false;
						btnApproveByHR.Visible = false;


						btnFnFSettlement.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/employee_fnf_settlement_" + txtPayrollDate.Text;
					}
				}
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApproveByHR.Visible = false;
                btnReview.Visible = false;
                btnRevApproveByHR.Visible = false;
                //lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                //btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "NewPayroll";
                objDB.PrimaryColumnnName = "NewPayrollID";
                objDB.PrimaryColumnValue = NewPayrollID.ToString();
                objDB.DocName = "Payroll";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = dtEmployeeSalaries.Rows.Count == 0;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = dtEmployeeSalaries.Rows.Count == 0;
                    btnReview.Visible = true;
                    //lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = dtEmployeeSalaries.Rows.Count == 0;
                    btnApproveByHR.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = dtEmployeeSalaries.Rows.Count == 0;
                    btnRevApproveByHR.Visible = true;

                    //btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private DataTable dtEmployeeSalaries
        {
            get
            {
                if (ViewState["dtEmployeeSalaries"] != null)
                {
                    return (DataTable)ViewState["dtEmployeeSalaries"];
                }
                else
                {
                    return new DataTable();
                }
            }
            set
            {
                ViewState["dtEmployeeSalaries"] = value;
            }
        }

        private DataTable NoSalary
        {
            get
            {
                if (ViewState["NoSalary"] != null)
                {
                    return (DataTable)ViewState["NoSalary"];
                }
                else
                {
                    return new DataTable();
                }
            }
            set
            {
                ViewState["NoSalary"] = value;
            }
        }

        private void getNewPayrollByMonth(string NewpayrollMoNTH)
        {
            DataTable dt = new DataTable();
            objDB.NewPayrollID = NewPayrollID;
            dt = objDB.GetNewPayrollByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtPayrollDate.Text = DateTime.Parse(dt.Rows[0]["NewPayrollDate"].ToString()).ToString("MMM-yyyy");
                    //btnReleaseActiveEmployees.Visible = !Convert.ToBoolean(dt.Rows[0]["IsPaid"].ToString());
                    getNewPayrollDetailsByParollID(Convert.ToInt32(dt.Rows[0]["NewPayrollID"].ToString()), dt.Rows[0]["DocStatus"].ToString(), Convert.ToBoolean(dt.Rows[0]["IsPaid"].ToString()));
                }
            }

            Common.addlog("View", "Finanace", "NewPayroll \"" + txtPayrollDate.Text + "\" Viewed", "NewPayroll", objDB.NewPayrollID);

        }
        private void getNewPayrollByID(int NewPayrollID)
        {
            DataTable dt = new DataTable();
            objDB.NewPayrollID = NewPayrollID;
            dt = objDB.GetNewPayrollByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtPayrollDate.Text = DateTime.Parse(dt.Rows[0]["NewPayrollDate"].ToString()).ToString("MMM-yyyy");
                    //btnReleaseActiveEmployees.Visible = !Convert.ToBoolean(dt.Rows[0]["IsPaid"].ToString());
                    getNewPayrollDetailsByParollID(Convert.ToInt32(dt.Rows[0]["NewPayrollID"].ToString()), dt.Rows[0]["DocStatus"].ToString(), Convert.ToBoolean(dt.Rows[0]["IsPaid"].ToString()));
                }
            }

            Common.addlog("View", "Finanace", "NewPayroll \"" + txtPayrollDate.Text + "\" Viewed", "NewPayroll", objDB.NewPayrollID);

        }
        private void getNewPayrollDetailsByParollID(int NewPayrollID, string docStatus, bool isPaid)
        {
            objDB.NewPayrollID = NewPayrollID;
            objDB.PayrollDate = txtPayrollDate.Text;
            objDB.isMiniPayroll = false;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataSet dss = objDB.GetKTPayrollnNoPayroll(ref errorMsg);
            dtEmployeeSalaries = dss.Tables[0];
            NoSalary = dss.Tables[1];
            noPayrollCount.InnerText = NoSalary.Rows.Count.ToString();
            PayrollCount.InnerText = dtEmployeeSalaries.Rows.Count.ToString();
            gvSalaries.DataSource = dtEmployeeSalaries;
            gvSalaries.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {

                    gvSalaries.UseAccessibleHeader = true;
                    gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

            


            gvNormalSal.DataSource = dtEmployeeSalaries;
            gvNormalSal.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    gvNormalSal.UseAccessibleHeader = true;
                    gvNormalSal.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            if (docStatus != "Approved")
            {
                //btnFnFSettlement.Visible = true;
                //btnReleaseActiveEmployees.Visible = !isPaid;
                noPayroll.Visible = true;
                downloadpayroll.Visible = true;
                btnSave.Visible = true;
                
                        btnView.Visible = true;


            }
            else
            {
                
                        btnView.Visible = false;
                noPayroll.Visible = true;
                downloadpayroll.Visible = true;
                btnSave.Visible = false;
                //dtEmployeeSalaries = objDB.GetKTPayrollDetailsByNewPayrollID(ref errorMsg);
            }

           // BindData();
            btnFnFSettlement.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/employee-fnf-settlement-" + txtPayrollDate.Text;
        }

        private void clearFields()
        {
            txtPayrollDate.Text = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }


        protected void btnPayroll_ServerClick(object sender, EventArgs e)
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtEmployeeSalaries, "Payroll");

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=Payroll_" + txtPayrollDate.Text + ".xlsx");
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }

        }


        protected void btnnoPayroll_ServerClick(object sender, EventArgs e)
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(NoSalary, "NoPayroll");

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=NoPayroll_" + DateTime.Now.ToShortDateString() + ".xlsx");
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
          
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";
                string errorMsgs = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.PayrollDate = "01-" + txtPayrollDate.Text;
                objDB.isMiniPayroll = false;

                if (HttpContext.Current.Items["PayrollID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.NewPayrollID = NewPayrollID;
                    objDB.UpdateNewPayroll();
                    res = "NewPayroll Data Updated";
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    NewPayrollID = Convert.ToInt32(objDB.AddNewPayroll());
                    res = "New NewPayroll Added";
                }

                dtEmployeeSalaries = (DataTable)ViewState["dtEmployeeSalaries"] as DataTable;

                if (dtEmployeeSalaries == null || dtEmployeeSalaries.Rows.Count == 0)
                {
                    return;
                }

                objDB.DocType = "NewPayroll";
                objDB.DocID = NewPayrollID;
                //objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();
                objDB.NewPayrollID = NewPayrollID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteNewPayrollDetailsKT();


                if (dtEmployeeSalaries != null)
                {
                    if (dtEmployeeSalaries.Rows.Count > 0)
                    {
                        objDB.NewPayrollID = Convert.ToInt32(NewPayrollID);
                        objDB.PayrollDate = Convert.ToDateTime($"01-{txtPayrollDate.Text}").ToString("MM-dd-yyyy");
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.SaveNewPayrollDetailsKT();

                        //for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                        //{
                        //    try
                        //    {
                        //        objDB.NewPayrollID = Convert.ToInt32(NewPayrollID);
                        //        objDB.EmployeeID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["EmployeeID"].ToString());
                        //        objDB.DesgID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["DesgID"].ToString());
                        //        objDB.DeptID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["DeptID"].ToString());
                        //        objDB.CostCenterID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["CostCenterID"].ToString());
                        //        objDB.GradeID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["GradeID"].ToString());
                        //        objDB.EmployeeSalary = float.Parse(dtEmployeeSalaries.Rows[i]["EmployeeSalary"].ToString());
                        //        objDB.BasicSalary = float.Parse(dtEmployeeSalaries.Rows[i]["BasicSalary"].ToString());
                        //        objDB.HouseAllownace = float.Parse(dtEmployeeSalaries.Rows[i]["HouseAllownace"].ToString());
                        //        objDB.UtitlityAllowance = float.Parse(dtEmployeeSalaries.Rows[i]["UtitlityAllowance"].ToString());
                        //        objDB.MedicalAllowance = float.Parse(dtEmployeeSalaries.Rows[i]["MedicalAllowance"].ToString());
                        //        objDB.OverTimeGeneralAmount = float.Parse(dtEmployeeSalaries.Rows[i]["OverTimeGeneralAmount"].ToString());
                        //        objDB.OvertimeHolidayAmount = float.Parse(dtEmployeeSalaries.Rows[i]["OvertimeHolidayAmount"].ToString());
                        //        objDB.OverTimeTotalAmount = float.Parse(dtEmployeeSalaries.Rows[i]["OverTimeAmount"].ToString());
                        //        objDB.Commissions = float.Parse(dtEmployeeSalaries.Rows[i]["Commission"].ToString());
                        //        objDB.Spiffs = float.Parse(dtEmployeeSalaries.Rows[i]["Spiffs"].ToString());
                        //        objDB.Arrears = float.Parse(dtEmployeeSalaries.Rows[i]["Arrears"].ToString());
                        //        objDB.LeaveEncashment = float.Parse(dtEmployeeSalaries.Rows[i]["LeaveEncashment"].ToString());
                        //        objDB.Graduity = float.Parse(dtEmployeeSalaries.Rows[i]["ExemptGraduity"].ToString());
                        //        objDB.Severance = float.Parse(dtEmployeeSalaries.Rows[i]["Severance"].ToString());
                        //        objDB.OtherBonuses = float.Parse(dtEmployeeSalaries.Rows[i]["OtherBonuses"].ToString());
                        //        objDB.TotalBonuses = float.Parse(dtEmployeeSalaries.Rows[i]["TotalBonus"].ToString());
                        //        objDB.OtherExpensesAmount = float.Parse(dtEmployeeSalaries.Rows[i]["OtherExpenses"].ToString());
                        //        objDB.GrossSalary = float.Parse(dtEmployeeSalaries.Rows[i]["GrossSalary"].ToString());
                        //        objDB.AbsentAmount = float.Parse(dtEmployeeSalaries.Rows[i]["AbsentAmount"].ToString());
                        //        objDB.Tax = float.Parse(dtEmployeeSalaries.Rows[i]["Tax"].ToString());
                        //        objDB.EOBI = float.Parse(dtEmployeeSalaries.Rows[i]["EOBI"].ToString());
                        //        objDB.Advance = float.Parse(dtEmployeeSalaries.Rows[i]["Advance"].ToString());
                        //        objDB.Deductions = float.Parse(dtEmployeeSalaries.Rows[i]["Deductions"].ToString());
                        //        objDB.EmployeePFNeww = float.Parse(dtEmployeeSalaries.Rows[i]["EmployeePF"].ToString());
                        //        objDB.TotalDeductions = float.Parse(dtEmployeeSalaries.Rows[i]["TotalDeductions"].ToString());
                        //        objDB.NetSalary = float.Parse(dtEmployeeSalaries.Rows[i]["NetSalary"].ToString());
                        //        objDB.TaxableIncome = float.Parse(dtEmployeeSalaries.Rows[i]["TaxableIncome"].ToString());
                        //        objDB.ExemptGraduity = float.Parse(dtEmployeeSalaries.Rows[i]["ExemptGraduity"].ToString());
                        //        objDB.CompanyPF = float.Parse(dtEmployeeSalaries.Rows[i]["CompanyPF"].ToString());
                        //        objDB.OvertimeHolidayHours = float.Parse(dtEmployeeSalaries.Rows[i]["HolidayOverTimeHours"].ToString());
                        //        objDB.OvertimeGeneralHours = float.Parse(dtEmployeeSalaries.Rows[i]["GeneralOverTimeHours"].ToString());
                        //        objDB.TotalDays = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["TotalDays"].ToString());
                        //        objDB.WorkingDays = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["WorkingDays"].ToString());
                        //        objDB.AbsentDays = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["AbsentDays"].ToString());
                        //        objDB.PayrolllExpCOA = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PayrolllExpCOA"].ToString());
                        //        objDB.EmployeeCOA = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["EMP_COA"].ToString());
                        //        objDB.IESSIEmployeeRatio = 0;
                        //        objDB.AdditionalTax = float.Parse(dtEmployeeSalaries.Rows[i]["AdditionalTax"].ToString());
                        //        objDB.CreatedBy = Session["UserName"].ToString();
                        //        string ss = objDB.AddNewKTPayrollDetailsKT();

                        //        if (ss != "Added")
                        //        {
                        //            errorMsgs += ss;
                        //            errorMsgs += ", ";
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        errorMsgs += ex.Message;
                        //        errorMsgs += ", ";
                        //    }
                        //}
                    
                    
                    }
                }


                if (res == "New NewPayroll Added" || res == "NewPayroll Data Updated")
                {
                    if (res == "New NewPayroll Added") { Common.addlog("Add", "Financa", "New NewPayroll \"" + objDB.PayrollDate + "\" Added", "NewPayroll"); }
                    if (res == "NewPayroll Data Updated") { Common.addlog("Update", "Financa", "NewPayroll \"" + objDB.PayrollDate + "\" Updated", "NewPayroll", objDB.NewPayrollID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                    btnReleaseActiveEmployees.Visible = true;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnPDF_ServerClickOld(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";
                StringBuilder sbTable = new StringBuilder();
                DataTable dt = new DataTable();

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "NewPayroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##DATE##", DateTime.Now.ToString("dd-MMM-yyyy"));

                sbTable.Append("<table style='width:100%;'>");
                sbTable.Append("<thead><tr><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Sr. No</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Employee Code</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Employee</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Department</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Designation</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Bank Details</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Basic Salary</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Allowances</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Deductions</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Net Salary</td></tr></thead>");

                if (dtEmployeeSalaries != null)
                {
                    if (dtEmployeeSalaries.Rows.Count > 0)
                    {
                        float totalBasicSalaries = 0;
                        float totalAllowances = 0;
                        float totalDeductions = 0;
                        float totalNetSalaries = 0;
                        sbTable.Append("<tbody>");
                        for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                        {
                            sbTable.Append("<tr><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + (i + 1) + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["EmployeeCode"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["EmployeeName"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Department"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Designation"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["BankDetails"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["BasicSalary"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Allowances"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Deductions"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["NetSalary"] + "</td></tr>");
                            totalBasicSalaries += float.Parse(dtEmployeeSalaries.Rows[i][5].ToString());
                            totalAllowances += float.Parse(dtEmployeeSalaries.Rows[i][6].ToString());
                            totalDeductions += float.Parse(dtEmployeeSalaries.Rows[i][7].ToString());
                            totalNetSalaries += float.Parse(dtEmployeeSalaries.Rows[i][8].ToString());
                        }
                        sbTable.Append("</tbody>");
                        sbTable.Append("<tfoot>");
                        sbTable.Append("<tr><td colspan='6' style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Net Calculations:</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalBasicSalaries + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalAllowances + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalDeductions + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left; font-weight:bold;'>" + totalNetSalaries + "</td></tr>");
                        sbTable.Append("</tfoot>");
                    }
                }
                sbTable.Append("</table>");
                content = content.Replace("##TABLE##", sbTable.ToString());

                Common.addlog("Report", "HR", "NewPayroll Report Generated", "NewPayroll");
                Common.generatePDF(header, footer, content, "NewPayroll Sheet (" + txtPayrollDate.Text + ")", "A4", "Portrait");

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Payroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##MONTH##", txtPayrollDate.Text);

                string sbTable = Common.GetTemplate(gvSalaries);

                content = content.Replace("##TABLE##", sbTable.ToString());

                Common.addlog("Report", "HR", "NewPayroll Report Generated", "NewPayroll");

                Common.generatePDF(header, footer, content, "NewPayroll Sheet", "A4", "Landscape");


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnPDFNormalSal_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Payroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##MONTH##", txtPayrollDate.Text);

                string sbTable = Common.GetTemplate(gvNormalSal);

                content = content.Replace("##TABLE##", sbTable.ToString());

                Common.addlog("Report", "HR", "NewPayroll Report Generated", "NewPayroll");
                Common.generatePDF(header, footer, content, "NewPayroll Sheet", "A4", "Landscape");


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnPDFOverTime_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Payroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##MONTH##", txtPayrollDate.Text);

                string sbTable = Common.GetTemplate(gvOverTime);

                content = content.Replace("##TABLE##", sbTable.ToString());

                Common.addlog("Report", "HR", "NewPayroll Report Generated", "NewPayroll");
                Common.generatePDF(header, footer, content, "NewPayroll Sheet", "A4", "Landscape");
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "NewPayroll", "NewPayrollID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "Finance", "Payroll of ID\"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/payrolls", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/Payrolls/edit-payroll-" + HttpContext.Current.Items["PayrollID"].ToString(), "Payroll of Date \"" + txtPayrollDate.Text + "\"", "NewPayroll", "Payroll", Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString()));

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;

                if (btn.ID.ToString() == "btnApproveByHR" || btn.ID.ToString() == "btnRevApproveByHR")
                {
                    objDB.NewPayrollID = NewPayrollID;
                    string ss = objDB.UpdateAddOnsPaidStatus();
                    objDB.PayrollDate = txtPayrollDate.Text;
                    dtEmployeeSalaries = objDB.GetKTPayrollDetailsForActiveEmployeesByNewPayrollID(ref errorMsg);
                    BindData();
                }
                else if (btn.ID.ToString() == "btnDisapprove" || btn.ID.ToString() == "btnRejDisApprove")
                {
                    objDB.NewPayrollID = NewPayrollID;
                    string ss = objDB.ResetAddOnsStatus();
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
            CheckAccess();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "NewPayroll", "NewPayrollID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "NewPayroll of ID \"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" deleted", "NewPayroll", NewPayrollID);

                objDB.NewPayrollID = NewPayrollID;
                string ss = objDB.UpdateAddOnsStatusByPayrollID();

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void txtPayrollDate_ServerChange(object sender, EventArgs e)
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
          //  objDB.ParentCompanyID =77;
            objDB.CreatedBy = Session["UserName"].ToString();

            objDB.PayrollDate = Convert.ToDateTime($"01-{txtPayrollDate.Text}").ToString("dd-MMM-yyyy");
            // if payroll locked /paid 

            DataTable dt = objDB.IsPayrollPaid(ref errorMsg);
			if (dt.Rows.Count > 0)
			{
                btnSave.Visible = false;
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "Payroll already exist";
            }
			else
			{
                btnSave.Visible = true;
                divAlertMsg.Visible = false;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "";
                objDB.PayrollDate = Convert.ToDateTime($"01-{txtPayrollDate.Text}").ToString("MM-dd-yyyy");
                DataSet ds = objDB.GenerateStandardPayrollnNoPayroll(ref errorMsg);

                dtEmployeeSalaries = ds.Tables[0];
                NoSalary = ds.Tables[1];
                noPayrollCount.InnerText = NoSalary.Rows.Count.ToString();
                PayrollCount.InnerText = dtEmployeeSalaries.Rows.Count.ToString();
                BindData();

            }







        }

       


        protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                int index = e.RowIndex;

                dtEmployeeSalaries.Rows[index].SetField("GrossSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
                dtEmployeeSalaries.Rows[index].SetField("AdditionalTax", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditTax")).Text);
                dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
                dtEmployeeSalaries.Rows[index].SetField("EmployeePF", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEmployeePF")).Text);
             
                dtEmployeeSalaries.Rows[index].SetField("NetSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("NetSalary")).Text);
                //dtEmployeeSalaries.Rows[index]["NetSalary"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EmployeePF"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Deductions"].ToString());
                
                dtEmployeeSalaries.AcceptChanges();

                gvSalaries.EditIndex = -1;
                BindData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvSalaries_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSalaries.EditIndex = -1;
            BindData();
        }
        protected void gvSalaries_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSalaries.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void gvSalaries_lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();

            if (dtEmployeeSalaries != null)
            {
                for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                {
                    if (dtEmployeeSalaries.Rows[i]["EmployeeID"].ToString() == delSr)
                    {
                        dtEmployeeSalaries.Rows[i].Delete();
                        dtEmployeeSalaries.AcceptChanges();
                        break;
                    }
                }
            }
            BindData();
        }

        protected void BindData()
        {
            gvSalaries.DataSource = dtEmployeeSalaries;
            gvSalaries.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    gvSalaries.UseAccessibleHeader = true;
                    gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

            noPayroll.Visible = true;
            downloadpayroll.Visible = true;
            btnSave.Visible = true;
           

            gvNormalSal.DataSource = dtEmployeeSalaries;
			gvNormalSal.DataBind();
			if (dtEmployeeSalaries != null)
			{
				if (dtEmployeeSalaries.Rows.Count > 0)
				{
					gvNormalSal.UseAccessibleHeader = true;
					gvNormalSal.HeaderRow.TableSection = TableRowSection.TableHeader;
				}
			}

		}

		protected void btnReleaseActiveEmployees_ServerClick(object sender, EventArgs e)
        {
            objDB.ModifiedBy = Session["UserName"].ToString();
            objDB.NewPayrollID = NewPayrollID;
            objDB.PayrollDate = Convert.ToDateTime($"01-{txtPayrollDate.Text}").ToString("dd-MMM-yyyy");
            objDB.DisburseNewPayroll();
           
            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = "Salaries Disbursed";
            BindData();
            btnReleaseActiveEmployees.Visible = false;
            btnSave.Visible = false;
        }
    }
}