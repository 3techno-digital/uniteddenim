﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class GRN : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        int GRNID
        {
            get
            {
                if (ViewState["GRNID"] != null)
                {
                    return (int)ViewState["GRNID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["GRNID"] = value;
            }
        }
        bool isFromFinance
        {
            get
            {
                if (ViewState["isFromFinance"] != null)
                {
                    return (bool)ViewState["isFromFinance"];
                }
                else
                {
                    return true;
                }
            }

            set
            {
                ViewState["isFromFinance"] = value;
            }
        }
        string GRNStatus
        {
            get
            {
                if (ViewState["GRNStatus"] != null)
                {
                    return ViewState["GRNStatus"].ToString();
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["GRNStatus"] = value;
            }
        }
        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                txtDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");
                //txtDeliveryDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);



                BindDropDowns();

                ViewState["dtTaxTable"] = null;


                ViewState["srNo"] = null;
                ViewState["dtRFQDetails"] = null;
                ViewState["showFirstRow"] = null;
                dtRFQDetails = createTable();
                BindCOAPN();
                BindAccount();
                BindCOA();
                divAttachments.Visible = false;
                btnApproveByFinance.Visible = false;
                btnReviewByFinance.Visible = false;
                btnRevApproveByFinance.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReviewByFinance.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;
                divAlertMsg.Visible = false;
                BindCountriesDropDown();
                BindStatesDropDown(166);
                accDiv.Visible = false;
                btnGenVoucher.Visible = false;
                btnPayVoucher.Visible = false;

                btnPayNow.Visible = false;
                //divRecurrig.Visible = false;


                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/goods-receive-notes";

                BindTaxesListBox();
                //txtRFQCode.Value = objDB.GenerateRFQCode();
                showFirstRow = false;
                if (HttpContext.Current.Items["GRNID"] != null)
                {
                    GRNID = Convert.ToInt32(HttpContext.Current.Items["GRNID"].ToString());
                    getGRNDetails(GRNID);
                    showFirstRow = true;
                    CheckAccess();
                }

                //if (HttpContext.Current.Items["ItemID"] != null)
                //{
                //    RFQDIV.Visible = false;
                //    int itemID = Convert.ToInt32(HttpContext.Current.Items["ItemID"].ToString());
                //    getItemDetails(itemID);
                //    showFirstRow = true;

                //}

                BindData();

            }
        }
        private void BindCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOA.Items.Clear();
            ddlCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("--- Select COA---", "0"));

        }
        private void BindCOAPN()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOAPN.Items.Clear();
            ddlCOAPN.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOAPN.DataTextField = "CodeTitle";
            ddlCOAPN.DataValueField = "COA_ID";
            ddlCOAPN.DataBind();
            ddlCOAPN.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

        }
        private void BindAccount()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlAccounts.DataSource = objDB.GetAllApprovedAccount(ref errorMsg);
            ddlAccounts.DataTextField = "AccountTitle";
            ddlAccounts.DataValueField = "AccountID";
            ddlAccounts.DataBind();
            ddlAccounts.Items.Insert(0, new ListItem("--- Select---", "0"));
            ddlAccounts.SelectedIndex = 0;
        }
        //private void getItemDetails(int itemID)
        //{
        //    if (CheckSessions())
        //    {
        //        DataTable dt = new DataTable();
        //        objDB.ItemID = itemID;
        //        dt = objDB.GetItemsByID(ref errorMsg);
        //        if (dt != null)
        //        {
        //            if (dt.Rows.Count > 0)
        //            {
        //                ddlSupplier.SelectedValue = dt.Rows[0]["PreferredSupplierID"].ToString();
        //                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
        //                txtGRNCode.Value = objDB.GenereateGRNCodeForQuickPurchase();


        //                if (dtRFQDetails.Rows[0][0].ToString() == "")
        //                {
        //                    dtRFQDetails.Rows[0].Delete();
        //                    dtRFQDetails.AcceptChanges();
        //                }

        //                DataRow dr = dtRFQDetails.NewRow();
        //                dr[0] = "1";
        //                dr[1] = dt.Rows[0]["ItemCode"].ToString();
        //                dr[2] = dt.Rows[0]["ItemID"].ToString();
        //                dr[3] = dt.Rows[0]["MeasurmentUnit"].ToString();
        //                dr[4] = "0";
        //                dr[5] = dt.Rows[0]["UnitPrice"].ToString();
        //                dr[6] = "0";
        //                dr[7] = "";
        //                srNo = 2;

        //                dtRFQDetails.Rows.Add(dr);
        //                dtRFQDetails.AcceptChanges();

        //            }
        //        }
        //    }
        //}

        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApproveByFinance.Visible = false;
            btnReviewByFinance.Visible = false;
            btnRevApproveByFinance.Visible = false;
            lnkReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReviewByFinance.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;



            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "GRNs";
            objDB.PrimaryColumnnName = "GRNID";
            objDB.PrimaryColumnValue = GRNID.ToString();
            objDB.DocName = "Good Receive Note";

            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
            if (chkAccessLevel == "Can Edit")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReviewByFinance.Visible = true;
            }
            if (chkAccessLevel == "Can Edit Finance")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReviewByFinance.Visible = true;
            }
            if (chkAccessLevel == "Can Edit & Review Finance")
            {
                btnSave.Visible = true;
                btnReviewByFinance.Visible = true;
                lnkReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve Finance")
            {
                btnSave.Visible = true;
                btnApproveByFinance.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve Finance")
            {
                btnSave.Visible = true;
                btnRevApproveByFinance.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit Finance")
            {
                btnSave.Visible = true;
            }
        }

        protected DataTable dtTaxTable
        {
            get
            {
                if (ViewState["dtTaxTable"] != null)
                {
                    return (DataTable)ViewState["dtTaxTable"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtTaxTable"] = value;
            }
        }

        private void BindTaxesListBox()
        {
            if (CheckSessions())
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                dtTaxTable = objDB.GetAllTaxRates(ref errorMsg);
                lbTaxes.DataSource = dtTaxTable;
                lbTaxes.DataTextField = "TaxRateShortName";
                lbTaxes.DataValueField = "TaxRateID";
                lbTaxes.DataBind();
            }
        }

        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
            return true;
        }

        private void getPODetails(int POID)
        {
            DataTable dt = new DataTable();
            objDB.POID = POID;
            dt = objDB.GetPOsByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlSupplier.SelectedValue = dt.Rows[0]["SupplierID"].ToString();
                    ddlCOA.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                    getPOItemsByPOID(POID);
                    showFirstRow = true;
                    BindData();
                }
            }
            Common.addlog("View", "Finance", "GRN \"" + txtGRNCode.Value + "\" Viewed", "GRNs", objDB.GRNID);

        }

        private void getGRNDetails(int GRNID)
        {
            DataTable dt = new DataTable();
            objDB.GRNID = GRNID;
            dt = objDB.GetGRNsByIDFinance(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    GRNStatus = dt.Rows[0]["DocStatus"].ToString();
                    if (GRNStatus != "Saved as Draft")
                    {
                        BindAllPO();
                        ddlPO.Enabled = false;
                    }

                    if (dt.Rows[0]["POID"].ToString() != null && dt.Rows[0]["POID"].ToString() != "")
                    {
                        ddlPO.SelectedValue = dt.Rows[0]["POID"].ToString();

                    }
                    else
                    {
                        ddlPO.SelectedValue = "0";
                    }
                    ddlSupplier.SelectedValue = dt.Rows[0]["SupplierID"].ToString();
                    ddlCOA.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                    txtGRNCode.Value = dt.Rows[0]["GRNCode"].ToString();
                    txtDate.Value = dt.Rows[0]["GRNDate"].ToString();
                    txtNetAmount.Value = dt.Rows[0]["POAmount"].ToString();
                    txtNotes.Value = dt.Rows[0]["Notes"].ToString();
                    //isFromFinance = Convert.ToBoolean(dt.Rows[0]["isFromFinance"].ToString());
                    txtRemainingAmount.Value = dt.Rows[0]["RemainingAmount"].ToString();
                    txtSales.Value = dt.Rows[0]["SalesInvoiceNo"].ToString();

                    //if (isFromFinance == false)
                    //{

                    //    gv.ShowFooter = false;
                    //}

                    if (dt.Rows[0]["DocStatus"].ToString() == "Approved By Finance" && dt.Rows[0]["isPaid"].ToString() != "True")
                    {
                        btnPayNow.Visible = true;
                        txtDatePN.Value = DateTime.Now.ToString("dd-MMM-yyyy");

                    }
                    else
                    {
                        btnPayNow.Visible = false;

                    }

                    if (dt.Rows[0]["DocStatus"].ToString() == "Approved By Finance")
                    {
                        btnGenVoucher.Visible = true;
                        btnPayVoucher.Visible = true;


                    }
                    else
                    {
                        btnGenVoucher.Visible = false;
                        btnPayVoucher.Visible = false;

                    }
                    getGRNItemsByGRNID(GRNID);
                    getGRNAttachmentsByGRNID(GRNID);
                    GetGRNTaxessByGRNID(GRNID);
                }
            }
            Common.addlog("View", "Finance", "GRN \"" + txtGRNCode.Value + "\" Viewed", "GRNs", objDB.GRNID);

        }

        private void getGRNAttachmentsByGRNID(int GRNID)
        {
            DataTable dt = new DataTable();
            objDB.GRNID = GRNID;
            dt = objDB.GetGRNAttachmentsByGRNID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    divAttachments.Visible = true;
                    gvAttachments.DataSource = dt;
                    gvAttachments.DataBind();
                }
            }
        }

        private void getGRNItemsByGRNID(int GRNID)
        {
            DataTable dt = new DataTable();
            objDB.GRNID = GRNID;
            dt = objDB.GetGRNItemsByGRNIDFinance(ref errorMsg);
            dtRFQDetails = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }
        }

        private void GetGRNTaxessByGRNID(int GRNID)
        {
            if (CheckSessions())
            {
                DataTable dt = new DataTable();
                objDB.GRNID = GRNID;
                dt = objDB.GetGRNTaxesByGRNID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            foreach (ListItem li in lbTaxes.Items)
                            {
                                if (dt.Rows[i]["TaxRateID"].ToString() == li.Value)
                                    li.Selected = true;
                            }
                        }
                    }
                }
            }
        }


        private void getPOItemsByPOID(int POID)
        {

            DataTable dt = new DataTable();
            objDB.POID = POID;
            dt = objDB.GetPOItemsByPOID(ref errorMsg);
            dtRFQDetails = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }


        }


        protected void BindAllPO() 
        {
            ddlPO.Items.Clear();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlPO.DataSource = objDB.GetActivePOs(ref errorMsg);
            //ddlPO.DataSource = objDB.GetAllPOsApprovedByFinance(ref errorMsg);
            ddlPO.DataTextField = "poCode";
            ddlPO.DataValueField = "POID";
            ddlPO.DataBind();
            ddlPO.Items.Insert(0, new ListItem("--- Select PO ---", "0"));
            ddlPO.SelectedIndex = 0;
        }

        private void BindDropDowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            //ddlPO.DataSource = objDB.GetActivePOs(ref errorMsg);
            ddlPO.DataSource = objDB.GetAllPOsApprovedByFinance(ref errorMsg);
            ddlPO.DataTextField = "poCode";
            ddlPO.DataValueField = "POID";
            ddlPO.DataBind();
            ddlPO.Items.Insert(0, new ListItem("--- Select PO ---", "0"));
            ddlPO.SelectedIndex = 0;

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlSupplier.DataSource = objDB.GetAllSupplier(ref errorMsg);
            ddlSupplier.DataTextField = "SupplierName";
            ddlSupplier.DataValueField = "SupplierID";
            ddlSupplier.DataBind();
            ddlSupplier.Items.Insert(0, new ListItem("--- Select Supplier ---", "0"));
            ddlSupplier.SelectedIndex = 0;


            //ddlPRs.DataSource = objDB.GetActivePurchaseRequisite(ref errorMsg);
            //ddlPRs.DataTextField = "PRCode";
            //ddlPRs.DataValueField = "PRID";
            //ddlPRs.DataBind();
            //ddlPRs.Items.Insert(0, new ListItem("--- Select PR ---", "0"));
            //ddlPRs.Items.Insert(1, new ListItem("--- None ---", "0"));
            //ddlPRs.SelectedIndex = 0;
        }

        protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            //code for generating pr code goes here
        }

        protected DataTable dtRFQDetails
        {
            get
            {
                if (ViewState["dtRFQDetails"] != null)
                {
                    return (DataTable)ViewState["dtRFQDetails"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtRFQDetails"] = value;
            }
        }
        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }

        protected void BindData()
        {
            if (ViewState["dtRFQDetails"] == null)
            {
                dtRFQDetails = createTable();
                ViewState["dtRFQDetails"] = dtRFQDetails;
            }
            else if (dtRFQDetails.Rows.Count == 0)
            {
                dtRFQDetails = createTable();
                showFirstRow = false;
            }
            gv.DataSource = dtRFQDetails;
            gv.DataBind();

            if (showFirstRow)
                gv.Rows[0].Visible = true;
            else
                gv.Rows[0].Visible = false;

            gv.UseAccessibleHeader = true;
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;

            calcTotal();
        }

        private DataTable createTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("ItemCode");
            dt.Columns.Add("ItemID");
            dt.Columns.Add("ItemUnit");
            dt.Columns.Add("Quantity");
            dt.Columns.Add("UnitPrice");
            dt.Columns.Add("NetPrice");
            dt.Columns.Add("Remarks");
            dt.Columns.Add("COA_ID");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();


            return dt;

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            //if (isFromFinance == false)
            //{
            //    e.Row.Cells[7].Visible = false;
            //    e.Row.Cells[8].Visible = false;
            //}

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                DropDownList ddlItemCode = e.Row.FindControl("ddlItems") as DropDownList;
                ddlItemCode.DataSource = objDB.GetAllItems(ref errorMsg);
                ddlItemCode.DataTextField = "ItemName";
                ddlItemCode.DataValueField = "ItemID";
                ddlItemCode.DataBind();
                ddlItemCode.Items.Insert(0, new ListItem("--- Select Item ---", "0"));

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                DropDownList ddlExpenseCOA = e.Row.FindControl("ddlExpCOA") as DropDownList;
                ddlExpenseCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
                ddlExpenseCOA.DataTextField = "CodeTitle";
                ddlExpenseCOA.DataValueField = "COA_ID";
                ddlExpenseCOA.DataBind();
                ddlExpenseCOA.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
                //DropDownList ddlSuppliers = e.Row.FindControl("ddlSuppliers") as DropDownList;
                //ddlSuppliers.DataSource = objDB.GetActiveSupplier(ref errorMsg);
                //ddlSuppliers.DataTextField = "SupplierName";
                //ddlSuppliers.DataValueField = "SupplierID";
                //ddlSuppliers.DataBind();
                //ddlSuppliers.Items.Insert(0, new ListItem("--- None ---", "0"));


                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = srNo.ToString();

                e.Row.Cells[7].ColumnSpan = 2;
                e.Row.Cells.RemoveAt(8);
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {

                //gv.UseAccessibleHeader = true;
                //e.Row.TableSection = TableRowSection.TableHeader;
                e.Row.Cells[7].ColumnSpan = 2;
                e.Row.Cells.RemoveAt(8);
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {


                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                    DropDownList ddlExpenseCOA = e.Row.FindControl("ddlEditExpCOA") as DropDownList;
                    ddlExpenseCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
                    ddlExpenseCOA.DataTextField = "CodeTitle";
                    ddlExpenseCOA.DataValueField = "COA_ID";
                    ddlExpenseCOA.DataBind();
                    ddlExpenseCOA.Items.Insert(0, new ListItem("--- Select COA ---", "0"));



                    hdnRowNo.Value = e.Row.RowIndex.ToString();
                    int rowIndex = e.Row.RowIndex;

                    ddlExpenseCOA.Items.IndexOf(ddlExpenseCOA.Items.FindByText(dtRFQDetails.Rows[rowIndex][6].ToString()));
                }
            }

            //if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
            //{
            //    if (!string.IsNullOrEmpty(e.Row.Cells[6].Text.ToString()))
            //    {
            //        total_Amount += float.Parse(e.Row.Cells[6].Text.ToString());
            //    }
            //}

            //txtTotalAmount.Value = total_Amount.ToString();
        }
        protected void calcTotal()
        {
            float total_Amount = 0;
            if (dtRFQDetails != null)
            {
                if (dtRFQDetails.Rows.Count > 0)
                {
                    foreach (DataRow row in dtRFQDetails.Rows)
                    {
                        if (!string.IsNullOrEmpty(row[6].ToString()))
                        {
                            total_Amount += float.Parse(row[6].ToString());
                        }
                    }
                    float tempAmount = total_Amount;
                    txtTotalAmount.Value = total_Amount.ToString();
                    for (int i = 0; i < lbTaxes.Items.Count; i++)
                    {

                        if (lbTaxes.Items[i].Selected == true)
                        {
                            float per = float.Parse(dtTaxTable.Rows[i]["TaxRatePer"].ToString());
                            total_Amount = (total_Amount + ((tempAmount * per) / 100.0f));
                        }
                    }
                    txtTotalTax.Value = (total_Amount - tempAmount).ToString();
                }
            }
            txtNetAmount.Value = total_Amount.ToString();
            ((Label)gv.FooterRow.FindControl("lblTotal")).Text = "Total : " + string.Format("{0:f2}", total_Amount);
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CheckSessions();
            bool similar = false;
            foreach (DataRow row in dtRFQDetails.Rows)
            {
                if (row[1].ToString() == ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Text && row[7].ToString() == ((DropDownList)gv.FooterRow.FindControl("ddlExpCOA")).SelectedItem.Text)
                {
                    //string supplier = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Text;
                    //string supplierID = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Value;
                    string itemUnit = ((DropDownList)gv.FooterRow.FindControl("ddlUnit")).SelectedItem.Text;
                    string qty = (Convert.ToInt32(row[4]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtQty")).Text)).ToString();
                    string unitPrice = (Convert.ToInt32(row[5]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtUnitPrice")).Text)).ToString();
                    string totalPrice = (Convert.ToInt32(row[6]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtNetPrice")).Text)).ToString();

                    //string remarks = ((TextBox)gv.FooterRow.FindControl("txtRemarks")).Text;

                    //row.SetField(3, supplier);
                    //row.SetField(4, supplierID);
                    row.SetField(3, itemUnit);
                    row.SetField(4, qty);
                    row.SetField(5, unitPrice);
                    row.SetField(6, totalPrice);
                    // row.SetField(7, remarks);

                    dtRFQDetails.AcceptChanges();

                    similar = true;
                    break;
                }


            }

            if (!similar)
            {
                DataRow dr = dtRFQDetails.NewRow();
                dr[0] = srNo.ToString();
                dr[1] = ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Text;
                dr[2] = ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Value;
                //dr[3] = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Text;
                //dr[4] = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Value;
                dr[3] = ((DropDownList)gv.FooterRow.FindControl("ddlUnit")).SelectedItem.Text;
                dr[4] = ((TextBox)gv.FooterRow.FindControl("txtQty")).Text;
                dr[5] = ((TextBox)gv.FooterRow.FindControl("txtUnitPrice")).Text;
                dr[6] = ((TextBox)gv.FooterRow.FindControl("txtNetPrice")).Text;
                dr[7] = ((DropDownList)gv.FooterRow.FindControl("ddlExpCOA")).SelectedItem.Text;
                dr[8] = ((DropDownList)gv.FooterRow.FindControl("ddlExpCOA")).SelectedItem.Value;
                dtRFQDetails.Rows.Add(dr);
                dtRFQDetails.AcceptChanges();

                srNo += 1;
            }
            BindData();


            ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
            calcTotal();

        }

        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void gv_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {

        }

        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            //dtRFQDetails.Rows[index].SetField(3, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditSuppliers")).SelectedItem.Text);
            //dtRFQDetails.Rows[index].SetField(4, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditSuppliers")).SelectedItem.Value);
            dtRFQDetails.Rows[index].SetField(3, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditUnit")).SelectedItem.Text);
            dtRFQDetails.Rows[index].SetField(4, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditQty")).Text);
            dtRFQDetails.Rows[index].SetField(5, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditUnitPrice")).Text);
            dtRFQDetails.Rows[index].SetField(6, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditNetPrice")).Text);
            dtRFQDetails.Rows[index].SetField(7, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditExpCOA")).SelectedItem.Text);
            dtRFQDetails.Rows[index].SetField(8, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditExpCOA")).SelectedItem.Value);


            dtRFQDetails.AcceptChanges();

            gv.EditIndex = -1;
            BindData();
            calcTotal();
        }

        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindData();
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {

            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
            {
                if (dtRFQDetails.Rows[i][0].ToString() == delSr)
                {
                    dtRFQDetails.Rows[i].Delete();
                    dtRFQDetails.AcceptChanges();
                }
            }
            for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
            {
                dtRFQDetails.Rows[i].SetField(0, i);
                dtRFQDetails.AcceptChanges();
            }
            srNo = dtRFQDetails.Rows.Count;
            BindData();
        }


        private bool checkCO_ID(out string msg)
        {
            msg = "success";

            bool isValid = false;
            dtRFQDetails = (DataTable)ViewState["dtRFQDetails"] as DataTable;
            if (dtRFQDetails != null)
            {
                if ((dtRFQDetails.Rows.Count > 0 && dtRFQDetails.Rows[0][1].ToString() != "") || (dtRFQDetails.Rows.Count > 1 && dtRFQDetails.Rows[0][1].ToString() == ""))
                {
                    if (dtRFQDetails.Rows[0][1].ToString() == "")
                    {
                        dtRFQDetails.Rows[0].Delete();
                        dtRFQDetails.AcceptChanges();
                    }

                    for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
                    {
                        if (dtRFQDetails.Rows[i]["COA_ID"].ToString() == "" || dtRFQDetails.Rows[i]["COA_ID"].ToString() == "0")
                        {
                            isValid = true;
                            msg = "Items COA can not be empty";
                            break;
                        }
                        else if (dtRFQDetails.Rows[i]["COA_ID"].ToString() == ddlCOA.SelectedValue)
                        {
                            isValid = true;
                            msg = "Item COA can not be same as Order COA";
                            break;
                        }
                    }
                }
                else
                {
                    isValid = true;
                    msg = "Please Insert Order Items";


                }
            }
            else
            {
                isValid = true;
                msg = "Please Insert Order Items";

            }

            return isValid;
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();

                if (checkCO_ID(out errorMsg))
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = errorMsg;
                    return;
                }

                BindData();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.GRNCode = txtGRNCode.Value;
                objDB.POID = Convert.ToInt32(ddlPO.SelectedItem.Value);
                objDB.SupplierID = Convert.ToInt32(ddlSupplier.SelectedItem.Value);
                //objDB.TermsAndConditionID = Convert.ToInt32(ddlTermsAndConditions.SelectedItem.Value);
                objDB.GRNDate = txtDate.Value;
                //objDB.DeliveryDate = txtDeliveryDate.Value;
                objDB.POAmount = float.Parse(txtNetAmount.Value);
                objDB.Notes = txtNotes.Value;
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.ChartOfAccountID = int.Parse(ddlCOA.SelectedValue);
                if (HttpContext.Current.Items["GRNID"] != null)
                {
                    objDB.GRNID = GRNID;
                    objDB.Status = "Active"; //this would be changed
                    res = objDB.UpdateGRNsFinance();
                    if (res == "GRN Updated Successfully" && GRNStatus == "Approved By Finance")
                    {
                        objDB.TableName = "GRNs";
                        objDB.TableID = GRNID;
                        objDB.Credit = float.Parse(txtNetAmount.Value);
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.TransactionDetail = ("Payable to " + ddlSupplier.SelectedItem.Text + " against PO # " + txtGRNCode.Value);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.ChartOfAccountID = Convert.ToInt32(ddlCOA.SelectedValue);
                        objDB.AddTransactionHistoryCredit();
                    }
                    //getRFQAttachmentsByRFQID(RFQID);
                    //showAttachments = true;

                    foreach (ListItem li in lbTaxes.Items)
                    {
                        objDB.TaxRateID = Convert.ToInt32(li.Value);
                        if (li.Selected == true)
                        {
                            objDB.AddGRNTax();
                        }
                        else
                        {
                            objDB.DelGRNTax();
                        }
                    }
                }
                else
                {
                    string returnedID = objDB.AddGRNsFinance();
                    GRNID = Convert.ToInt32(returnedID);

                    res = "New GRN Added Successfully";

                    foreach (ListItem li in lbTaxes.Items)
                    {
                        objDB.GRNID = GRNID;
                        objDB.TaxRateID = Convert.ToInt32(li.Value);
                        if (li.Selected == true)
                        {
                            objDB.AddGRNTax();
                        }
                    }

                }

                if (itemAttachments.HasFile || itemAttachments.HasFiles)
                {
                    foreach (HttpPostedFile file in itemAttachments.PostedFiles)
                    {
                        Random rand = new Random((int)DateTime.Now.Ticks);
                        int randnum = 0;
                        randnum = rand.Next(1, 100000);

                        string fn = "";
                        string exten = "";

                        string destDir = Server.MapPath("~/assets/files/GRNs/");
                        randnum = rand.Next(1, 100000);
                        fn = txtGRNCode.Value + "_" + randnum;

                        if (!Directory.Exists(destDir))
                        {
                            Directory.CreateDirectory(destDir);
                        }

                        string fname = Path.GetFileName(file.FileName);
                        exten = Path.GetExtension(file.FileName);
                        file.SaveAs(destDir + fn + exten);

                        objDB.GRNID = Convert.ToInt32(GRNID);
                        objDB.AttachmentPath = "http://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/GRNs/" + fn + exten;
                        objDB.AddPOAttachments();
                    }
                }
                objDB.GRNID = GRNID;
                // objDB.DeletePOItemsByGRNID();
                objDB.ParentID = GRNID;
                objDB.TableName = "GRNItems";
                dtRFQDetails = (DataTable)ViewState["dtRFQDetails"] as DataTable;

                objDB.DeleteGRNItemsByGRNID();
                objDB.DeleteTransactionHistoryByParentID();

                for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
                {
                    if (res == "GRN Updated Successfully" && GRNStatus == "Approved By Finance")
                    {
                        objDB.TableName = "GRNItems";
                        objDB.TableID = 0;
                        objDB.ParentID = GRNID;
                        objDB.Debit = float.Parse(dtRFQDetails.Rows[i]["NetPrice"].ToString());
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        if (dtRFQDetails.Rows[i]["COA_ID"].ToString() != null && dtRFQDetails.Rows[i]["COA_ID"].ToString() != "")
                        {
                            objDB.ChartOfAccountID = Convert.ToInt32(dtRFQDetails.Rows[i]["COA_ID"].ToString());
                        }
                        else
                        {
                            objDB.ChartOfAccountID = 0;
                        }
                        objDB.TransactionDetail = ("Payable to " + ddlSupplier.SelectedItem.Text + " against PO # " + txtGRNCode.Value);
                        objDB.AddTransactionHistoryDebit();
                    }
                    objDB.GRNID = Convert.ToInt32(GRNID);
                    objDB.ItemID = Convert.ToInt32(dtRFQDetails.Rows[i]["ItemID"].ToString());
                    objDB.ItemUnit = dtRFQDetails.Rows[i]["ItemUnit"].ToString();
                    objDB.Qty = Convert.ToInt32(dtRFQDetails.Rows[i]["Quantity"].ToString());
                    objDB.UnitPrice = Convert.ToInt32(dtRFQDetails.Rows[i]["UnitPrice"].ToString());
                    objDB.NetPrice = Convert.ToInt32(dtRFQDetails.Rows[i]["NetPrice"].ToString());
                    objDB.Remarks = dtRFQDetails.Rows[i]["Remarks"].ToString();
                    if (dtRFQDetails.Rows[i]["COA_ID"].ToString() != null && dtRFQDetails.Rows[i]["COA_ID"].ToString() != "")
                    {
                        objDB.ChartOfAccountID = Convert.ToInt32(dtRFQDetails.Rows[i]["COA_ID"].ToString());
                    }
                    else
                    {
                        objDB.ChartOfAccountID = 0;
                    }

                    objDB.ModifiedBy = Session["UserName"].ToString();

                    objDB.AddGRNItemsFinance();
                }

                if (!showFirstRow)
                {
                    ViewState["dtRFQDetails"] = null;
                    srNo = 1;
                }

                BindData();


                objDB.ParentID = GRNID;
                objDB.TableName = "GRNTaxes";
                for (int i = 0; i < lbTaxes.Items.Count; i++)
                {
                    if (lbTaxes.Items[i].Selected == true && res == "GRN Updated Successfully" && GRNStatus == "Approved By Finance")
                    {
                        objDB.DeleteTransactionHistoryByParentID();

                        objDB.TableName = "GRNTaxes";
                        objDB.TableID = 0;
                        objDB.ParentID = GRNID;
                        float per = float.Parse(dtTaxTable.Rows[i]["TaxRatePer"].ToString());
                        objDB.Debit = ((float.Parse(txtTotalAmount.Value) * per) / 100.0f);
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        if (dtTaxTable.Rows[i]["COA_ID"].ToString() != null && dtTaxTable.Rows[i]["COA_ID"].ToString() != "")
                        {
                            objDB.ChartOfAccountID = Convert.ToInt32(dtTaxTable.Rows[i]["COA_ID"].ToString());
                        }
                        else
                        {
                            objDB.ChartOfAccountID = 0;
                        }

                        objDB.TransactionDetail = dtTaxTable.Rows[i]["TaxRateName"].ToString() + " @ " + dtTaxTable.Rows[i]["TaxRatePer"].ToString() + "% on " + txtTotalAmount.Value + "/- from " + ddlSupplier.SelectedItem.Text + " against PO # " + txtGRNCode.Value;
                        objDB.AddTransactionHistoryDebit();
                    }
                }

                if (res == "New GRN Added Successfully" || res == "GRN Updated Successfully")
                {
                    if (res == "New GRN Added Successfully") { Common.addlog("Add", "Finance", " GRN \"" + objDB.GRNCode + "\" Added", "GRNs"); clearFields(); }
                    if (res == "GRN Updated Successfully") { Common.addlog("Update", "Finance", "GRN \"" + objDB.GRNCode + "\" Updated", "GRNs", objDB.GRNID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;

                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        private void clearFields()
        {
            txtNotes.Value = "";
            //dtRFQDetails = createTable();
            ddlPO.SelectedIndex = 0;
            ddlSupplier.SelectedIndex = 0;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            //txtGRNCode.Value = objDB.GenereateGRNCode();
            //txtDeliveryDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");

            txtTotalAmount.Value = "0";
            txtNetAmount.Value = "0";
            txtTotalTax.Value = "0";
            txtNotes.Value = "";

            foreach (ListItem li in lbTaxes.Items)
            {
                li.Selected = false;
            }

        }

        //protected void ddlRFQ_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    CheckSessions();

        //    if (ddlRFQ.SelectedIndex != 0)
        //    {
        //        objDB.RFQID = Convert.ToInt32(ddlRFQ.SelectedItem.Value);
        //        txtGRNCode.Value = objDB.GenereateGRNCodeByRFQID();
        //        getRFQItemsByRFQID(Convert.ToInt32(ddlRFQ.SelectedItem.Value));
        //        calcTotal();
        //    }
        //    else
        //    {
        //        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

        //        txtGRNCode.Value = objDB.GenereateGRNCode();
        //        dtRFQDetails = createTable();
        //    }
        //    BindData();
        //    //ddlRFQ.SelectedIndex = 0;
        //}

        protected void txtNetPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            TextBox txtQty = gv.FooterRow.FindControl("txtQty") as TextBox;
            string qty = txtQty.Text;
            TextBox txtUnitPrice = gv.FooterRow.FindControl("txtUnitPrice") as TextBox;
            string unitPrice = txtUnitPrice.Text;
            DropDownList txtRemarks = gv.FooterRow.FindControl("ddlExpCOA") as DropDownList;

            if (!string.IsNullOrEmpty(qty) && !string.IsNullOrEmpty(unitPrice))
            {
                TextBox txtNetPrice = gv.FooterRow.FindControl("txtNetPrice") as TextBox;
                string netPrice = (Convert.ToInt32(qty) * Convert.ToInt32(unitPrice)).ToString();

                txtNetPrice.Text = netPrice;
            }
            TextBox txtSender = (TextBox)sender as TextBox;
            if (txtSender.ID == "txtQty")
                txtUnitPrice.Focus();
            else if (txtSender.ID == "txtUnitPrice")
                txtRemarks.Focus();

        }

        protected void txtEditUnitPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            int rowNum = Convert.ToInt32(hdnRowNo.Value);
            TextBox txtQty = gv.Rows[rowNum].FindControl("txtEditQty") as TextBox;
            string qty = txtQty.Text;
            TextBox txtUnitPrice = gv.Rows[rowNum].FindControl("txtEditUnitPrice") as TextBox;
            string unitPrice = txtUnitPrice.Text;

            if (!string.IsNullOrEmpty(qty) && !string.IsNullOrEmpty(unitPrice))
            {
                TextBox txtNetPrice = gv.Rows[rowNum].FindControl("txtEditNetPrice") as TextBox;
                string netPrice = (Convert.ToInt32(qty) * Convert.ToInt32(unitPrice)).ToString();

                txtNetPrice.Text = netPrice;
            }

            TextBox txtSender = (TextBox)sender as TextBox;
            txtSender.Focus();
        }

        protected void gv_PreRender(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
                gv.UseAccessibleHeader = true;
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void getRFQItemsByRFQID(int RFQID)
        {
            DataTable dt = new DataTable();
            objDB.RFQID = RFQID;
            dt = objDB.GetRFQItemsByRFQID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dtRFQDetails.Rows.Add(new object[] {
                            row[0],
                            row[1],
                            row[2],
                            row[3],
                            row[4],
                            "0",
                            "0",
                            row[5]
                        });

                        dtRFQDetails.AcceptChanges();
                    }
                    BindData();

                }
            }
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            if (checkCO_ID(out errorMsg))
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = errorMsg;
                return;
            }

            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "GRNs", "GRNID", HttpContext.Current.Items["GRNID"].ToString(), Session["UserName"].ToString());
            Common.addlog(res, "Finance", "GRNs of ID \"" + HttpContext.Current.Items["GRNID"].ToString() + "\" Status Changed", "GRNs", GRNID /* ID From Page Top  */);/* Button1_ServerClick  */
            objDB.POID = Convert.ToInt32(ddlPO.SelectedValue);
            objDB.ProgressStatus = "Partial";
            objDB.UpdatePOsProgressStatus();

            if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
            {
                objDB.POID = Convert.ToInt32(ddlPO.SelectedValue);
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.UpdatePOsProgressStatus();

                objDB.TableName = "GRNs";
                objDB.TableID = GRNID;
                objDB.Credit = float.Parse(txtNetAmount.Value);
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.ChartOfAccountID = Convert.ToInt32(ddlCOA.SelectedValue);
                objDB.TransactionDetail = ("Payable to " + ddlSupplier.SelectedItem.Text + " against GRN # " + txtGRNCode.Value);

                objDB.AddTransactionHistoryCredit();

               
                dtRFQDetails = (DataTable)ViewState["dtRFQDetails"] as DataTable;
                if (dtRFQDetails != null)
                {
                    if (dtRFQDetails.Rows.Count > 0)
                    {

                        for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
                        {
                            objDB.TableName = "GRNItems";
                            objDB.TableID = 0;
                            objDB.ParentID = GRNID;
                            if (dtRFQDetails.Rows[i]["NetPrice"].ToString() != null && dtRFQDetails.Rows[i]["NetPrice"].ToString() != "")
                            {
                                objDB.Debit = float.Parse(dtRFQDetails.Rows[i]["NetPrice"].ToString());
                            }
                            else
                            {
                                objDB.Debit = 0;
                            }

                            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                            objDB.CreatedBy = Session["UserName"].ToString();
                            if (dtRFQDetails.Rows[i]["COA_ID"].ToString() != null && dtRFQDetails.Rows[i]["COA_ID"].ToString() != "")
                            {
                                objDB.ChartOfAccountID = int.Parse(dtRFQDetails.Rows[i]["COA_ID"].ToString());
                            }
                            else
                            {
                                objDB.ChartOfAccountID = 0;
                            }

                            objDB.TransactionDetail = ("Payable to " + ddlSupplier.SelectedItem.Text + " against GRN # " + txtGRNCode.Value);
                            objDB.AddTransactionHistoryDebit();

                        }

                    }
                }

                objDB.ParentID = GRNID;
                objDB.TableName = "GRNTaxes";
                objDB.DeleteTransactionHistoryByParentID();
                for (int i = 0; i < lbTaxes.Items.Count; i++)
                {
                    if (lbTaxes.Items[i].Selected == true)
                    {
                        objDB.TableName = "GRNTaxes";
                        objDB.TableID = 0;
                        objDB.ParentID = GRNID;
                        float per = float.Parse(dtTaxTable.Rows[i]["TaxRatePer"].ToString());
                        objDB.Debit = ((float.Parse(txtTotalAmount.Value) * per) / 100.0f);
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.CreatedBy = Session["UserName"].ToString();

                        if (dtTaxTable.Rows[i]["COA_ID"].ToString() != null && dtTaxTable.Rows[i]["COA_ID"].ToString() != "")
                        {
                            objDB.ChartOfAccountID = Convert.ToInt32(dtTaxTable.Rows[i]["COA_ID"].ToString());
                        }
                        else
                        {
                            objDB.ChartOfAccountID = 0;
                        }

                        //objDB.ChartOfAccountID = Convert.ToInt32(dtTaxTable.Rows[i]["COA_ID"].ToString());
                        objDB.TransactionDetail = (dtTaxTable.Rows[i]["TaxRateName"].ToString() + " @ " + dtTaxTable.Rows[i]["TaxRatePer"].ToString() + "% on " + txtTotalAmount.Value + "/- from " + ddlSupplier.SelectedItem.Text + " against GRN # " + txtGRNCode.Value);
                        objDB.AddTransactionHistoryDebit();
                    }
                }
                btnGenVoucher.Visible = true;
                btnPayVoucher.Visible = true;
                btnPayNow.Visible = true;
                txtDatePN.Value = DateTime.Now.ToString("dd-MMM-yyyy");

                AddVoucher();

            }
            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;

            CheckAccess();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            string type = btn.CommandArgument;
            string res = Common.addAccessLevels(type, "GRNs", "GRNID", HttpContext.Current.Items["GRNID"].ToString(), Session["UserName"].ToString());

            Common.addlog("Delete", "Finance", "GRNs of ID \"" + HttpContext.Current.Items["GRNID"].ToString() + "\" deleted", "GRNs", GRNID /* ID From Page Top  */);/* lnkDelete_Click  */

            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            Response.Redirect(btnBack.HRef);

        }



        protected void AddVoucher()
        {
            CheckSessions();

            string JVID = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.VoucherNo = txtGRNCode.Value;
            objDB.Against = "GRN";
            objDB.CreatedBy = Session["UserName"].ToString();
            JVID = objDB.AddFinanceVoucher();
            objDB.GRNID = GRNID;
            objDB.JVID = Convert.ToInt32(JVID);
            objDB.AddJVIDinGRN();
        }

        protected void addBCVoucher(int ID)
        {
            try
            {
                int BCVoucherID = 0;
                CheckSessions();
                objDB.VouchrType = ddlPayBy.SelectedValue;
                objDB.VoucherNo = txtGRNCode.Value;
                objDB.Against = "GRN";
                objDB.TableName = "GRNs";
                objDB.TableID = ID;
                objDB.VoucherAmount = Convert.ToDouble(txtAmountPN.Value);
                objDB.CreditCOA = int.Parse(ddlCOAPN.SelectedValue);
                objDB.DebitCOA = int.Parse(ddlCOA.SelectedValue);
                objDB.TransactionDetail = "Paid amount against GRN# " + txtGRNCode.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                BCVoucherID = Convert.ToInt32(objDB.AddBankCashVoucher());
                //VouchrType, VoucherNo, Against, TableName, TableID, VoucherAmount,CreditCOA,DebitCOA,TransactionDetail, CreatedBy, CompanyID 

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        protected void lbTaxes_SelectedIndexChanged(object sender, EventArgs e)
        {
            calcTotal();
        }
        //protected void chkIsRecurring_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkIsRecurring.Checked)
        //    {
        //        divRecurrig.Visible = true;
        //    }
        //    else
        //    {
        //        divRecurrig.Visible = false;

        //    }
        //}



        protected void btnPayNow_ServerClick1(object sender, EventArgs e)
        {
            //if (!divPay.Visible)
            //{
            //    divPay.Visible = true;
            //    txtDatePN.Value = DateTime.Now.ToString("dd-MMM-yyyy");
            //    BindCOAPN();
            //}
            //else
            //{
            //    divPay.Visible = false;
            //    ddlCOAPN.SelectedIndex = 0;
            //    txtAmountPN.Value = "0";
            //}
        }

        protected void btnPay_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.PayDate = txtDatePN.Value;
                objDB.PayAmount = float.Parse(txtAmountPN.Value);
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.PayCOAID = int.Parse(ddlCOAPN.SelectedValue);
                objDB.GRNID = GRNID;
                objDB.AccountID = int.Parse(ddlAccounts.SelectedValue);
                objDB.PayType = ddlPayBy.SelectedValue;
                res = objDB.AddGRNPay();

                if (res == "Paid")
                {
                    objDB.TableName = "GRNPay";
                    objDB.TableID = 0;
                    objDB.ParentID = GRNID;
                    objDB.Debit = float.Parse(txtAmountPN.Value);
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.ChartOfAccountID = Convert.ToInt32(ddlCOA.SelectedValue);
                    objDB.TransactionDetail = "Paid against GRN# " + txtGRNCode.Value;
                    objDB.AddTransactionHistoryDebit();



                    objDB.TableName = "GRNPay";
                    objDB.TableID = 0;
                    objDB.ParentID = GRNID;
                    objDB.Credit = float.Parse(txtAmountPN.Value);
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.ChartOfAccountID = Convert.ToInt32(ddlCOAPN.SelectedValue);
                    objDB.TransactionDetail = "Paid against GRN# " + txtGRNCode.Value;
                    objDB.AddTransactionHistoryCredit();



                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                    addBCVoucher(GRNID);
                    getGRNDetails(GRNID);

                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }



        protected void ddlPayBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPayBy.SelectedValue == "Bank")
            {
                accDiv.Visible = true;
            }
            else
            {
                accDiv.Visible = false;

            }
        }


        #region ModalSupplier
        protected void btnAddSupplier_ServerClick(object sender, EventArgs e)
        {
            if (CheckSessions())
            {
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.SupplierName = txtName.Value;
                objDB.SupplierPhone = txtPhone.Value;
                objDB.SupplierEmail = txtEmail.Value;
                objDB.CountryName = ddlCountries.SelectedItem.Text;
                objDB.StateName = ddlStates.SelectedItem.Text;
                objDB.CityName = ddlCities.SelectedItem.Text;
                objDB.AddressLine1 = txtAddressLine1.Value;
                objDB.AddressLine2 = txtAddressLine2.Value;
                objDB.ZIPCode = txtZipCode.Value;
                objDB.BusinessName = txtBusinessName.Value;
                objDB.NTN = txtNTN.Value;
                objDB.AccountNo = txtAccountNo.Value;
                objDB.BankName = txtBankName.Value;
                objDB.ModifiedBy = Session["UserName"].ToString();


                int sid = Convert.ToInt32(objDB.AddSupplier());

                res = "New Supplier Added";





                if (res == "New Supplier Added")
                {
                    Common.addlog("Add", "Finance", "New Supplier \"" + objDB.SupplierName + "\" Added From PO Page", "Supplier");

                    //divAlertMsg.Visible = true;
                    //divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    //pAlertMsg.InnerHtml = res;
                    txtName.Value = "";
                    txtPhone.Value = "";
                    txtEmail.Value = "";
                    ddlCountries.SelectedIndex = 0;
                    ddlStates.SelectedIndex = 0;
                    ddlCities.SelectedIndex = 0;
                    txtAddressLine1.Value = "";
                    txtAddressLine2.Value = "";
                    txtZipCode.Value = "";
                    txtBusinessName.Value = "";
                    txtNTN.Value = "";
                    txtAccountNo.Value = "";
                    txtBankName.Value = "";
                    BindDropDowns();

                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
        }
        private void BindCountriesDropDown()
        {
            ddlCountries.DataSource = objDB.GetAllCounties(ref errorMsg);
            ddlCountries.DataTextField = "Name";
            ddlCountries.DataValueField = "ID";
            ddlCountries.DataBind();
            ddlCountries.Items.Insert(0, new ListItem("--- Select Country ---", "0"));
            ddlCountries.SelectedValue = "166";
        }

        private void BindStatesDropDown(int CountryID)
        {
            objDB.CountryID = CountryID;
            ddlStates.DataSource = objDB.GetAllStatesByCountryID(ref errorMsg);
            ddlStates.DataTextField = "Name";
            ddlStates.DataValueField = "ID";
            ddlStates.DataBind();
            ddlStates.Items.Insert(0, new ListItem("--- Select State ---", "0"));
        }

        private void BindCitiesDropDown(int StateID)
        {
            objDB.StateID = StateID;
            ddlCities.DataSource = objDB.GetAllCitiesByStateID(ref errorMsg);
            ddlCities.DataTextField = "Name";
            ddlCities.DataValueField = "Name";
            ddlCities.DataBind();
            ddlCities.Items.Insert(0, new ListItem("--- Select City ---", "0"));
        }

        protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CheckSessions())
            {
                BindCitiesDropDown(Convert.ToInt32(ddlStates.SelectedValue));
            }
        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CheckSessions())
            {
                BindStatesDropDown(Convert.ToInt32(ddlCountries.SelectedValue));
            }
        }
        #endregion

        protected void btnGenVoucher_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/journal-voucher/generate-" + GRNID);
            }
            catch (Exception ex)
            {

            }
        }

        //protected void Button1_ServerClick1(object sender, EventArgs e)
        //{

        //}

        protected void btnPayVoucher_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Session["TableName"] = "GRNPay";
                Session["BackLink"] = ("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/purchase-orders/edit-purchase-order-" + GRNID);

                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/payment-voucher/generate-" + GRNID);
            }
            catch (Exception ex)
            {

            }
        }

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlPO_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            if (ddlPO.SelectedIndex != 0)
            {
                objDB.POID = Convert.ToInt32(ddlPO.SelectedItem.Value);
                txtGRNCode.Value = objDB.GenereateGRNCodeByPOID();
                //getPOItemsByPOID(Convert.ToInt32(ddlPO.SelectedItem.Value));
                getPODetails(Convert.ToInt32(ddlPO.SelectedItem.Value));
            }
            else
            {
                ddlSupplier.SelectedIndex = 0;
                txtGRNCode.Value = "";
                dtRFQDetails = createTable();
            }
            BindData();
        }
    }
}