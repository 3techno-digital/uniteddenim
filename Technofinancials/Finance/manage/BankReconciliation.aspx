﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BankReconciliation.aspx.cs" Inherits="Technofinancials.Finance.manage.BankReconciliation" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/BankReconciliation.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Bank Reconciliation</h3>
                        </div>

                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>


                                <div class="col-sm-4">
                                    <div class="pull-right flex">
                                        <button type="button" class="tf-save-btn" "Download Sample Sheet" value="Downlaod Sample Sheet" onclick="window.location = '/assets/SampleStatement.CSV';"><i class="fa fa-cloud-download"></i></button>
                                        <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                        <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                        <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class"  CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                        <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                        <button class="tf-save-btn" "Reject & Disapproved" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>


                                        <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <h4>From Date
                                        <span style="color:red !important;">*</span> 

                                        </h4>
                                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtFromDate"  ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                    
                                        <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtFromDate" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <h4>To Date
                                        <span style="color:red !important;">*</span> 
                                        </h4>
                                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate"  ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>

                                        <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtToDate" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group input-group-lg">
                                        <h4>Bank Account
                                        <span style="color:red !important;">*</span> 
                                        </h4>
                                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlAccount" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>

                                        <asp:DropDownList ClientIDMode="Static" runat="server" class="form-control select2" data-plugin="select2" ID="ddlAccount">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                    <div class="col-sm-1">
                             
                                </div>


                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row ">
                                <div class="col-md-6">

                                    <div class="input-group input-group-lg flex" id="divUpd" runat="server">
                                        <asp:FileUpload ID="FileUpload1" class="btn btn-primary" ClientIDMode="Static" runat="server" AllowMultiple="false" Style="display: none;"></asp:FileUpload>
                                        <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('FileUpload1').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                        <button class="tf-upload-btn readBtn"  id="btnRead" runat="server" onserverclick="Unnamed_ServerClick" type="button">Read</button>
                                    </div>
                                </div>



                            </div>
                            <div class="clearfix">&nbsp;</div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <%--<asp:GridView ID="gv" runat="server"></asp:GridView>--%>
                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No.">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lbl12" Text='<%# Eval("SrNO") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Generarte Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblATRCode" Text='<%# Eval("TransictionDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblPRCOde" Text='<%# Eval("Description") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Debit">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCompanyName" Text='<%# Eval("Debit") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Credit">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblSiteNzme" Text='<%# Eval("Credit") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Reconciled">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblJobID" Text='<%# Eval("Reconciled") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--                                <asp:TemplateField HeaderText="De-Idle">
                                                <ItemTemplate>
                                                    <a onclick="showModal('<%# Eval("ASSET_ID") %>')" data-toggle="modal" class="edit-class" data-target="#myModal">De-Idle</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <a onclick="showModal('<%# Eval("SrNo") %>')" data-target="<%# Eval("Debit").ToString()==""?"#myModal":"#myModalInvoice"%>" data-toggle="modal" "Reconcile" <%# Eval("Reconciled").ToString()==""?"":"hidden='hidden'"%>>Reconcile</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                    <asp:GridView ID="gvget" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Generarte Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblATRCode" Text='<%# Eval("GenDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblPRCOde" Text='<%# Eval("Description") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Debit">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCompanyName" Text='<%# Eval("Debit") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Credit">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblSiteNzme" Text='<%# Eval("Credit") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Reconciled">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblJobID" Text='<%# Eval("Comments") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                            
                                                                                        <asp:TemplateField>
                                                <ItemTemplate>
                                                    <a onclick="showModal('<%# Eval("BankReconcilationDetailID") %>')" data-target="<%# Eval("Debit").ToString()==""?"#myModal":"#myModalInvoice"%>" data-toggle="modal" "Reconcile" <%# Eval("Comments").ToString()==""?"":"hidden='hidden'"%>>Reconcile</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>


                            <div class="clearfix">&nbsp;</div>



                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnRead" />
                        </Triggers>
                    </asp:UpdatePanel>

                </section>


                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }

            .readBtn{
                margin-left:10px;
            }
        </style>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Reconcilation</h4>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="modal-body">

                                <asp:HiddenField ID="hdnID" runat="server" ClientIDMode="Static" />
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Expense
                                            </label>
                                            <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control select2" ID="ddlExpense" OnSelectedIndexChanged="ddlExpense_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Date</label>
                                            <input type="text" class="form-control" placeholder="Date" disabled="disabled" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" name="companyShortName" id="txtDate" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Amount</label>
                                            <input type="text" class="form-control" id="txtAmount" runat="server" disabled="disabled" placeholder="Name" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <h4>Description</h4>
                                            <textarea class="form-control" rows="3" disabled="disabled" name="Description" id="txtDescription" runat="server"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <%--    <p>
                            Attachment 
                              <asp:FileUpload ID="updAttachment" runat="server" ClientIDMode="Static" Style="display: none;" accept=".pdf,.doc,.docx,.jpg,jpeg,.png,gif" />
                            <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('updAttachment').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                        </p>--%>
                            </div>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                    <div class="modal-footer">
                        <div class="pull-left">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/expenses"); %>"><span class="menu-text">View All Expenses</span></a>

                        </div>
                        <button type="button" class="btn btn-default" id="btnReconcile" runat="server" onserverclick="btnReconcile_ServerClick">Reconcile</button>
                    </div>
                </div>
            </div>
        </div>






        <div class="modal fade" id="myModalInvoice" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Reconcilation</h4>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div class="modal-body">

                                <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static" />
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Invoice
                                            </label>
                                            <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control select2" ID="ddlInvoices" OnSelectedIndexChanged="ddlInvoices_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Date</label>
                                            <input type="text" class="form-control" placeholder="Date" disabled="disabled" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" name="companyShortName" id="txtDateInvoice" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Amount</label>
                                            <input type="text" class="form-control" id="txtamountInvoice" runat="server" disabled="disabled" placeholder="Name" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <h4>Description</h4>
                                            <textarea class="form-control" rows="3" disabled="disabled" name="Description" id="txtDescriptionInvoice" runat="server"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <%--    <p>
                            Attachment 
                              <asp:FileUpload ID="updAttachment" runat="server" ClientIDMode="Static" Style="display: none;" accept=".pdf,.doc,.docx,.jpg,jpeg,.png,gif" />
                            <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('updAttachment').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                        </p>--%>
                            </div>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                    <div class="modal-footer">
                        <div class="pull-left">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/invoice"); %>"><span class="menu-text">View All Invoices</span></a>

                        </div>
                        <button type="button" class="btn btn-default" id="btnReconcileInvoice" runat="server" onserverclick="btnReconcileInvoice_ServerClick">Reconcile</button>
                    </div>
                </div>
            </div>
        </div>

        <style>
  .aspNetDisabled {
                   width: 100% !important;
    background: #eeeeee;
    height: 38px;
    padding: 2px 6px;
    font-size: 16px;
    line-height: 1.3333333;
    border-radius: 3px;
    border-color: #ccc;
    outline: none;
    box-shadow: none;
            }

        </style>

    </form>
</body>
</html>
<script>
    function showModal(id) {
        $('#hdnID').val(id);
        //$('#myModal').show();
    }
</script>
