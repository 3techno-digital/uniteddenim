﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.manage
{
    public partial class COAKT : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COAID
        {
            get
            {
                if (ViewState["COAID"] != null)
                {
                    return (int)ViewState["COAID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COAID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    BindDropdowns();
                    //BindAccounts();
                    //BindTax();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/coa";

                    divAlertMsg.Visible = false;
                    clearFields();
                    //btnApprove.Visible = false;
                    //btnReview.Visible = false;
                    //btnRevApprove.Visible = false;
                    //lnkReject.Visible = false;
                    //lnkDelete.Visible = false;
                    //btnSubForReview.Visible = false;
                    //btnDisapprove.Visible = false;
                    //btnRejDisApprove.Visible = false;

                    ViewState["srNo"] = null;
                    ViewState["dtOpeningBalance"] = null;
                    ViewState["showFirstRow"] = null;

                    showFirstRow = false;
                    if (HttpContext.Current.Items["COAID"] != null)
                    {
                        COAID = Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString());
                        getDataByID(COAID);
                        CheckAccess();
                    }
                    else
                    {

                    }
                    //BindData();
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                //btnApprove.Visible = false;
                //btnReview.Visible = false;
                //btnRevApprove.Visible = false;
                //lnkReject.Visible = false;
                //lnkDelete.Visible = false;
                //btnSubForReview.Visible = false;
                //btnDisapprove.Visible = false;
                //btnRejDisApprove.Visible = false;

                objDB.TableName = "FAM_COA";
                objDB.PrimaryColumnnName = "COA_ID";
                objDB.PrimaryColumnValue = COAID.ToString();
                objDB.DocName = "ConfigureCOA";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    //lnkDelete.Visible = true;
                    //btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                   // btnReview.Visible = true;
                    //lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                   // btnApprove.Visible = true;
                   // btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                   // btnRevApprove.Visible = true;

                    //btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.ChartOfAccountID = COAID;
            dt = objDB.GetCOAByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {


                    acctype.Value= dt.Rows[0]["description"].ToString();
                    memo.Value= dt.Rows[0]["memo"].ToString();
                    ddlFor.SelectedValue = dt.Rows[0]["flag"].ToString();
                    ddlmapping.SelectedValue = dt.Rows[0]["catagory"].ToString();
                    ddlmapping.Enabled = false;
                    //ddlCurrency.SelectedValue = dt.Rows[0]["Currency"].ToString();
                    //txtDescription.Value= dt.Rows[0]["Description"].ToString();

                    //for (int i2 = 0; i2 < dt.Rows.Count; i2++)
                    //{
                    //    for (int i = 0; i < ddlLocation.Items.Count; i++)
                    //    {
                    //        if (dt.Rows[i2]["location_id"].ToString().Trim() == ddlLocation.Items[i].Value.ToString().Trim())
                    //        {
                    //            ddlLocation.Items[i].Selected = true;
                    //        }
                    //    }
                    //}


                    //ddlCompany.SelectedValue = dt.Rows[0]["CompanyID"].ToString();
                    //BindLocations(Convert.ToInt32(dt.Rows[0]["CompanyID"].ToString()));
                    //BindLocations(Convert.ToInt32(Session["CompanyID"].ToString()));
                    //ddlLocation.SelectedValue = dt.Rows[0]["Locations"].ToString();
                    //ddlTax.SelectedValue = dt.Rows[0]["TaxRateID"].ToString();
                    //ddlAccount.SelectedValue = dt.Rows[0]["AccountTypeID"].ToString();
                    // txtAccountTitle.Value = dt.Rows[0]["SubAccount"].ToString();
                    // txtAccountCode.Text = dt.Rows[0]["SubAccountCode"].ToString();
                    //txtGLCode.Value = dt.Rows[0]["GL_CODE"].ToString();
                    //txtOpeningBalance.Value = dt.Rows[0]["OpeningBalance"].ToString();
                    //txtDescription.Value = dt.Rows[0]["DESCRIPTIONS"].ToString();
                    //ddlFor.SelectedValue = dt.Rows[0]["COAFor"].ToString();


                    //GetCOAOpeningBalCOAID(COAID);
                }
            }
            Common.addlog("View", "Finance", "COA \"" + acctype.Value + "\" Viewed", "FAM_COA", objDB.ChartOfAccountID);
        }
        private void GetCOAOpeningBalCOAID(int COAID)
        {
            DataTable dt = new DataTable();
            objDB.ChartOfAccountID = COAID;
            dt = objDB.GetCOAOpeningBalCOAID(ref errorMsg);
            dtOpeningBalance = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                    showFirstRow = true;
                }
            }
        }

        private void clearFields()
        {
            acctype.Value = "";
            memo.Value = "";
            txtNotes.Value = "";
            txtDescription.Value = "";
           // generateCode();
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                //objDB.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
                objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
                //objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
                
                //objDB.COA_AccountTypeID = Convert.ToInt32(ddlAccount.SelectedValue);
                //objDB.TaxRateID = Convert.ToInt32(ddlTax.SelectedValue);
                //objDB.Account = txtAccountTitle.Value;
                //objDB.AccountCode = txtAccountCode.Text;
                //objDB.GLCode = txtGLCode.Value;
                //objDB.Description = txtDescription.Value;
                //objDB.OpeningBalance = float.Parse((txtOpeningBalance.Value == "") ? "0" : txtOpeningBalance.Value);
                //objDB.COAFor = ddlFor.SelectedValue;
                //List<string> locations = new List<string>();
                //foreach (ListItem item in ddlLocation.Items)
                //{
                //    if (!item.Selected)
                //    {
                //        continue;
                //    }



                //        locations.Add(item.Value);
                        
                        
                //}

                objDB.Coatype = acctype.Value;
                objDB.Memo = memo.Value;
                objDB.accountfor = ddlFor.SelectedValue;
                //objDB.CurrencyName = ddlCurrency.SelectedItem.Text;
                objDB.Description = txtDescription.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.Mapping = ddlmapping.SelectedValue;

                //objDB.Notes = txtNotes.Value;


                if (HttpContext.Current.Items["COAID"] != null)
                {
                    // update coding
                    objDB.ChartOfAccountID = COAID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOA();
                }
                else
                {

                    // add coding
                    //objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOANewMapping();
                    int COAID = 0;
                   
                    clearFields();
                }
              
                if (res == "New COA Added" || res == "COA Updated")
                {
                    if (res == "New COA Added") { Common.addlog("Add", "Finance", " COA \"" + objDB.GLCode + "\" Added", "FAM_COA"); }
                    if (res == "COA Updated") { Common.addlog("Update", "Finance", "COA\"" + objDB.GLCode + "\" Updated", "FAM_COA", objDB.ChartOfAccountID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "Finance", "COA of ID\"" + HttpContext.Current.Items["COAID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/coa", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/coa/edit-coa-" + HttpContext.Current.Items["COAID"].ToString(), "COA \"" + acctype.Value + "\"", "FAM_COA", "ConfigureCOA", Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString()));

                //Common.addlog(res, "Finance", "COA of ID \"" + HttpContext.Current.Items["COAID"].ToString() + "\" Status Changed", "FAM_COA", COAID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "COA of ID \"" + HttpContext.Current.Items["COAID"].ToString() + "\" deleted", "FAM_COA", COAID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

		//private void BindCompanies()
		//{
		//    ddlCompany.Items.Clear();
		//    objDB.ParentCompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
		//    ddlCompany.DataSource = objDB.GetCompaniesByParentID(ref errorMsg);
		//    ddlCompany.DataTextField = "CompanyName";
		//    ddlCompany.DataValueField = "CompanyID";
		//    ddlCompany.DataBind();
		//    ddlCompany.Items.Insert(0, new ListItem("-- Select --", "0"));
		//}


		//private void BindTax()
		//{
		//    ddlTax.Items.Clear();
		//    objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
		//    ddlTax.DataSource = objDB.GetAllTaxRates(ref errorMsg);
		//    ddlTax.DataTextField = "TaxRateName";
		//    ddlTax.DataValueField = "TaxRateID";
		//    ddlTax.DataBind();
		//    ddlTax.Items.Insert(0, new ListItem("-- Select --", "0"));
		//}


		private void BindDropdowns()
		{
			//ddlLocation.Items.Clear();
			//objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]); ;
			//ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
			//ddlLocation.DataTextField = "NAME";
			//ddlLocation.DataValueField = "LOCATION_ID";
			//ddlLocation.DataBind();

            ddlCurrency.Items.Clear();
            ddlCurrency.DataSource = objDB.GetAllCurrencyByCompanyID(ref errorMsg);
            ddlCurrency.DataTextField = "CurrencyName";
            ddlCurrency.DataValueField = "CurrencyName";
            ddlCurrency.DataBind();


            ddlmapping.Items.Clear();
            ddlmapping.DataSource = objDB.GetPayrollFields(ref errorMsg);
            ddlmapping.DataTextField = "Column_name";
            ddlmapping.DataValueField = "Column_name";
            ddlmapping.DataBind();


            //ddlCurrency.Items.Insert(0, new ListItem("--- Select Currency ---", "0"));
        }


		//private void BindAccounts()
		//{
		//    ddlAccount.Items.Clear();
		//    objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
		//    ddlAccount.DataSource = objDB.GetAllCOA_Account(ref errorMsg);
		//    ddlAccount.DataTextField = "Name";
		//    ddlAccount.DataValueField = "AccountID";
		//    ddlAccount.DataBind();
		//    ddlAccount.Items.Insert(0, new ListItem("-- Select --", "0"));
		//}

		//protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
		//{
		//    BindLocations(Convert.ToInt32(ddlCompany.SelectedValue));
		//    generateCode();
		//}

		//protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
		//{
		//    generateCode();
		//}


		protected void txtAccountCode_TextChanged(object sender, EventArgs e)
        {
           // generateCode(false);
        }

        protected void ddlAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
           // generateCode();
        }


        //void generateCode(bool isGenerateCode = true)
        //{
        //    try
        //    {
        //        CheckSessions();

        //        if (ddlAccount.SelectedValue != "0" && isGenerateCode)
        //        {
        //            txtAccountCode.Text = "";

        //            objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
        //            //objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
        //            objDB.COA_AccountTypeID = int.Parse(ddlAccount.SelectedValue);

        //            DataTable dtCode = objDB.Gen_COA_SubAccount(ref errorMsg);
        //            if (dtCode != null)
        //            {
        //                if (dtCode.Rows.Count > 0)
        //                {
        //                    txtAccountCode.Text = dtCode.Rows[0]["NewCode"].ToString();
        //                }
        //            }
        //        }


        //        if (ddlAccount.SelectedValue == "0" || txtAccountCode.Text == "")
        //        {
        //            txtGLCode.Value = "";
        //        }
        //        else
        //        {
        //            objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
        //            //objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
        //            objDB.COA_AccountTypeID = Convert.ToInt32(ddlAccount.SelectedValue);
        //            DataTable coa_code = new DataTable();
        //            coa_code = objDB.GetCOA_AccountCode(ref errorMsg);
        //            if (coa_code != null)
        //            {
        //                txtGLCode.Value = coa_code.Rows[0][0].ToString() + txtAccountCode.Text;
        //            }
        //            else
        //            {
        //                txtGLCode.Value = "";
        //            }

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        txtGLCode.Value = "";
        //    }
        //}

       

        protected DataTable dtOpeningBalance
        {
            get
            {
                if (ViewState["dtOpeningBalance"] != null)
                {
                    return (DataTable)ViewState["dtOpeningBalance"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtOpeningBalance"] = value;
            }
        }
        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }
        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        //protected void BindData()
        //{
        //    if (ViewState["dtOpeningBalance"] == null)
        //    {
        //        dtOpeningBalance = createTable();
        //        ViewState["dtOpeningBalance"] = dtOpeningBalance;
        //    }
        //    else if (dtOpeningBalance.Rows.Count == 0)
        //    {
        //        dtOpeningBalance = createTable();
        //        showFirstRow = false;
        //    }
        //    gv.DataSource = dtOpeningBalance;
        //    gv.DataBind();

        //    if (showFirstRow)
        //        gv.Rows[0].Visible = true;
        //    else
        //        gv.Rows[0].Visible = false;

        //    gv.UseAccessibleHeader = true;
        //    gv.HeaderRow.TableSection = TableRowSection.TableHeader;

        //}
        //private DataTable createTable()
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("SrNo");
        //    dt.Columns.Add("LocationName");
        //    dt.Columns.Add("LocationID");
        //    dt.Columns.Add("Balance");
        //    dt.AcceptChanges();
        //    DataRow dr = dt.NewRow();
        //    dt.Rows.Add(dr);
        //    dt.AcceptChanges();
        //    return dt;
        //}

        //protected void gv_PreRender(object sender, EventArgs e)
        //{
        //    if (gv.Rows.Count > 0)
        //    {
        //        gv.UseAccessibleHeader = true;
        //        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        //    }
        //}
        //protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Footer)
        //    {
        //        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //        DropDownList ddlItemCode = e.Row.FindControl("ddlLocation") as DropDownList;
        //        ddlItemCode.DataSource = objDB.GetAllApprovedLocations(ref errorMsg);
        //        ddlItemCode.DataTextField = "SiteName";
        //        ddlItemCode.DataValueField = "SiteID";
        //        ddlItemCode.DataBind();
        //        ddlItemCode.Items.Insert(0, new ListItem("--- Select Location ---", "0"));

        //        Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
        //        txtSrNo.Text = srNo.ToString();

        //        //e.Row.Cells[3].ColumnSpan = 2;
        //        //e.Row.Cells.RemoveAt(4);
        //    }
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {

        //        //e.Row.Cells[3].ColumnSpan = 2;
        //        //e.Row.Cells.RemoveAt(4);
        //    }
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        if ((e.Row.RowState & DataControlRowState.Edit) > 0)
        //        {
        //            hdnRowNo.Value = e.Row.RowIndex.ToString();
        //            int rowIndex = e.Row.RowIndex;
        //        }
        //    }
        //}
        

        //protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    gv.EditIndex = e.NewEditIndex;
        //    BindData();
        //}

        //protected void gv_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        //{

        //}

        //protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    int index = e.RowIndex;
        //    dtOpeningBalance.Rows[index].SetField(3, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditBalance")).Text);
        //    dtOpeningBalance.AcceptChanges();
        //    gv.EditIndex = -1;
        //    BindData();
        //}

        //protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        //{
        //    gv.EditIndex = -1;
        //    BindData();
        //}

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtOpeningBalance.Rows.Count; i++)
            {
                if (dtOpeningBalance.Rows[i][0].ToString() == delSr)
                {
                    dtOpeningBalance.Rows[i].Delete();
                    dtOpeningBalance.AcceptChanges();
                }
            }
            for (int i = 0; i < dtOpeningBalance.Rows.Count; i++)
            {
                dtOpeningBalance.Rows[i].SetField(0, i);
                dtOpeningBalance.AcceptChanges();
            }
            srNo = dtOpeningBalance.Rows.Count;
           // BindData();
        }

        //protected void btnAdd_Click1(object sender, EventArgs e)
        //{
        //    CheckSessions();
        //    bool similar = false;
        //    foreach (DataRow row in dtOpeningBalance.Rows)
        //    {
        //        if (row[2].ToString() == ((DropDownList)gv.FooterRow.FindControl("ddlLocation")).SelectedItem.Value)
        //        {
        //            string Balance = (float.Parse(row[3].ToString()) + float.Parse(((TextBox)gv.FooterRow.FindControl("txtBalance")).Text)).ToString();
        //            row.SetField(3, Balance);
        //            dtOpeningBalance.AcceptChanges();
        //            similar = true;
        //            break;
        //        }
        //    }
        //    if (!similar)
        //    {
        //        DataRow dr = dtOpeningBalance.NewRow();
        //        dr[0] = srNo.ToString();
        //        dr[1] = ((DropDownList)gv.FooterRow.FindControl("ddlLocation")).SelectedItem.Text;
        //        dr[2] = ((DropDownList)gv.FooterRow.FindControl("ddlLocation")).SelectedItem.Value;
        //        dr[3] = ((TextBox)gv.FooterRow.FindControl("txtBalance")).Text;
        //        dtOpeningBalance.Rows.Add(dr);
        //        dtOpeningBalance.AcceptChanges();
        //        srNo += 1;
        //    }
        //    BindData();
        //    ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
        //}



        //void generateCode(bool isGenerateCode = true)
        //{
        //    try
        //    {
        //        CheckSessions();

        //        if (ddlAccount.SelectedValue != "0" && ddlCompany.SelectedValue != "0" && ddlLocation.SelectedValue != "0" && isGenerateCode)
        //        {
        //            txtAccountCode.Text = "";

        //            objDB.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        //            objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
        //            objDB.COA_AccountTypeID = int.Parse(ddlAccount.SelectedValue);

        //            DataTable dtCode = objDB.Gen_COA_SubAccount(ref errorMsg);
        //            if (dtCode != null)
        //            {
        //                if (dtCode.Rows.Count > 0)
        //                {
        //                    txtAccountCode.Text = dtCode.Rows[0]["NewCode"].ToString();
        //                }
        //            }
        //        }


        //        if (ddlAccount.SelectedValue == "0" || ddlCompany.SelectedValue == "0" || ddlLocation.SelectedValue == "0" || txtAccountCode.Text == "")
        //        {
        //            txtGLCode.Value = "";
        //        }
        //        else
        //        {
        //            objDB.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        //            objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
        //            objDB.COA_AccountTypeID = Convert.ToInt32(ddlAccount.SelectedValue);
        //            DataTable coa_code = new DataTable();
        //            coa_code = objDB.GetCOA_AccountCode(ref errorMsg);
        //            if (coa_code != null)
        //            {
        //                txtGLCode.Value = coa_code.Rows[0][0].ToString() + txtAccountCode.Text;
        //            }
        //            else
        //            {
        //                txtGLCode.Value = "";
        //            }

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        txtGLCode.Value = "";
        //    }
        //}

    }
}