﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IT3Approval.aspx.cs" Inherits="Technofinancials.Finance.Manage.IT3Approval" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
         <asp:HiddenField ID="hdnPFAmount" runat="server" ClientIDMode="Static" />
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
         <%--   <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="UpdatePanel3"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>

            <div class="wrap">




                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <h1 class="m-0 text-dark">IT3 Application</h1>
                                            
                                    </div>      
                                      <div class="col-sm-4">
                                                <div style="text-align: right;">
                                                   <a class="AD_btn_inn" href="../google" target="_blank" runat="server" id="downloadIT3" download="download" > Application<i class="fa fa-cloud-download" aria-hidden="true"></i> </a>
                                                    <br />   
                                                    </div>
                                            </div>
                                       <br />
                                    <!-- /.col -->
                                    <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                        <ContentTemplate>
                                              
                                
                                            <div class="col-sm-4">
                                                  <br />
                                                <div style="text-align: right;">
                                                
                                                    <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Review</button>
                                                          <button class="AD_btn" id="btnApprove" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                                    <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                                    <button class="AD_btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button><%--Disapprove--%>
                                              
                                                    
                                                    <button class="AD_btn" id="modalReject" runat="server"  data-toggle="modal" data-target="#notes-modal" validationgroup="btnValidate" type="button" title="Reject"><%--<i class="fa fa-thumbs-down"></i>--%> Reject</button>
                                                   
                                               
                                                        <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                            
                             
                                            <%--        <button  runat="server" class="AD_btn" id="btnDownloadForm"  onserverclick="ButtonDownload_ServerClick" type="button" validationgroup="btnDownloadForm">
                                                         
                                            <h3 class=""  style="margin: -4px;"><i class="fa fa-cloud-download" aria-hidden="true"></i>
                                               Application </h3>
                                        </button>--%>
                                                 
                                                </div>
                                            </div>
                                               
                                            <!-- Modal -->
                                            <div class="modal fade M_set" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="m-0 text-dark">Reason</h1>
                                                <div class="add_new">
                                                    <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                                                    <button type="button" class="AD_btn" runat="server"   id="btnReject"  onserverclick="btnReject_ServerClick" data-dismiss="modal">Save</button>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNote" runat="server" rows="5" placeholder="Reason.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>


                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                          <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                           <div class="col-md-6 form-group">
                                            <h4>WDID </h4>
                                            <input class="form-control" id="WDID" placeholder="Loan Title" readonly="true" type="text" runat="server" />
                                        </div>
                                          <div class="col-md-6 form-group">
                                            <h4>Employee Name </h4>
                                            <input class="form-control" id="employeename" placeholder="Loan Title" readonly="true" type="text" runat="server" />
                                        </div>
                                            <div class="col-md-6 form-group">
                                            <h4>Submission date </h4>
                                            <input class="form-control" id="subdate" readonly="true" type="text" runat="server" />
                                        </div>
                                          <div class="col-md-6 form-group">
                                        <h4>Effective From  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txteffective" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txteffective" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
                                    </div>
                                        <div class="col-md-6 form-group">
                                            <h4>Amount Claim </h4>
                                            <input class="form-control" id="amountclaim" readonly="true" type="text" runat="server" />
                                        </div>
                                      <div class="col-md-6 form-group">
                                            <h4>Amount Approved</h4>
                                            <input class="form-control" id="amountapp"   type="text" runat="server" />
                                        </div>
                                        

                                     
                                      
                                         <div class="col-sm-6 form-group">
												<div class="checkbox checkbox-primary">
                                                    <br />
															<input type="checkbox" id="allowcredit" readonly="true" runat="server" name="returnedLockerKeys" />
															<label for="allowcredit">Allow Credit</label>	
											</div>		
								
							</div>	
                                       
                                      
                                   
                                     
                                              </ContentTemplate>
                                
                                </asp:UpdatePanel>
                                    </div>
                                </div>
                                  <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                            <span>
                                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                            </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                 <div  class="col-lg-8 col-md-8 col-sm-12" >
                      
                                 
  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                
                                      
                                              <div class="row">
                               <div class="">
                        
                                       
                                       <div class="col-md-8">
                                          
                                           <div class="form-group">
                                        <h4>Transactions</h4>
                                    </div>
                                           
                                        
                                               
                                            <asp:GridView ID="b" runat="server" CssClass="table table-bordered" ClientIDMode="Static" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
                                            <Columns>
                                              

                                                <asp:TemplateField HeaderText="Transaction ID">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("transid") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Payroll Month">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("payrollmonth") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Amount Deduct">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("deductamount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 
                                            


                                            </Columns>
                                        </asp:GridView>
                                          
                                       </div>
                                 
                                      
                                       
                                 
                               </div>
                             
                              
                              
                            </div>
                        </ContentTemplate>
        
                                         <%-- <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAddExpDetails" />
                                    <asp:PostBackTrigger ControlID="btnUpdateExpDetails" />
                                </Triggers>--%>

                    </asp:UpdatePanel>
                               
                            </div> 
                              
                            </div>
                        </section>
                  
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
        </style>
        <!-- Modal -->
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>



		</script>
    </form>
    <style>


    </style>
</body>
</html>
