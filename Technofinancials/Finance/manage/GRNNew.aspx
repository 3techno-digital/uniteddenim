﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GRNNew.aspx.cs" Inherits="Technofinancials.Finance.manage.GRNNew" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">
                <section class="app-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img src="/assets/images/Admin_Purchase_Orders.png" class="img-responsive tf-page-heading-img" />
                                <h3 class="tf-page-heading-text">Goods Recieve Notes</h3>
                            </div>

                            <div class="col-md-4">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group" id="divAlertMsg" runat="server">
                                            <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                <span>
                                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                </span>
                                                <p id="pAlertMsg" runat="server">
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                <ContentTemplate>

                                    <div class="col-sm-4">
                                        <div class="pull-right flex">
                                            <button type="button" data-toggle="modal" data-target="#notes-modal" class="tf-note-btn-new" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                            <%--<button type="button" class="tf-save-btn" value="Print Payment JV" "Print Payment JV" id="btnPayVoucher" runat="server" onserverclick="btnPayVoucher_ServerClick"><i class="fa fa-dollar"></i></button>--%>
                                            <button type="button" data-toggle="modal" data-target="#voucher-modal" class="tf-save-btn" value="Vouchers" "Vouchers" id="btnGenVoucher" runat="server"><i class="fa fa-file-pdf-o"></i></button>
                                            <button type="button" data-toggle="modal" data-target="#paynow-modal" class="tf-save-btn" value="Pay Noe" "Pay Now" id="btnPayNow" runat="server"><i class="fa fa-money"></i></button>
                                            <button class="tf-save-btn" "Review & Approve" id="btnRevApproveByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                            <button class="tf-save-btn" "Review" id="btnReviewByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                            <button class="tf-save-btn" "Approve" id="btnApproveByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                            <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class" "Delete" CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                            <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                            <button class="tf-save-btn" "Reject & Disapprove" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                            <button class="tf-save-btn" "Submit for Review" id="btnSubForReviewByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                            <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSubmit_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                            <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid my-container">
                        <div class="dashboard-left inner-page">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Against PO  <span style="color: red !important;">*</span> </label>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlPO" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" class="select2 form-control" ID="ddlPO" OnSelectedIndexChanged="ddlPO_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Supplier  <span style="color: red !important;">*</span> </label>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="ddlSupplier" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server" class="form-control js-example-basic-single" ID="ddlSupplier" Enabled="false">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>GRN Code  <span style="color: red !important;">*</span> </label>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtGRNCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="GRN Code" id="txtGRNCode" disabled runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Generation Date  <span style="color: red !important;">*</span> </label>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator23" ControlToValidate="txtDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="29-12-1993" id="txtDate" disabled runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="display: none;">
                                            <label>Sales Invoice No.</label><%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtSales" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />--%>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Sales Invoice No" id="txtSales" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <label>Taxes Applied</label>
                                            <div class="form-group">
                                                <asp:ListBox ID="lbTaxes" runat="server" CssClass="select2" ClientIDMode="Static" SelectionMode="Multiple" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="lbTaxes_SelectedIndexChanged"></asp:ListBox>
                                            </div>
                                        </div>

                                        <div class="col-md-4" style="display: none;">
                                            <div class="input-group input-group-lg">
                                                <label>
                                                    COA
                                            <span style="color: red !important;">*</span>
                                                    <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" InitialValue="0" ControlToValidate="ddlCOA" ErrorMessage="COA Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />--%>
                                                    <asp:Label CssClass="lbl-COA-Balance" Text="Balance: 0" ID="lblCOABal" runat="server" />
                                                </label>
                                                <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlCOA_SelectedIndexChanged" ID="ddlCOA">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Location <span style="color: red !important;">*</span><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator24" InitialValue="0" ControlToValidate="ddlLocation" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <div class="form-group">
                                                <asp:DropDownList ClientIDMode="Static" runat="server"  class="select2 form-control" ID="ddlLocation" >
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>

                                    </div>
                                    <%--           <div class="row" id="divPayNow" runat="server">
                                        <div class="col-md-4">

                                            <button type="button" id="btnPayNow" "Pay NOW" runat="server" onserverclick="btnPayNow_ServerClick1">Pay NOW</button>
                                        </div>

                                    </div>
                                    <div id="divPay" runat="server">

                                      
                                    </div>--%>
                                    <div class="clearfix">&nbsp;</div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="add-table-div">
                                                <asp:HiddenField ID="hdnRowNo" runat="server" />
                                                <asp:GridView ID="gv" runat="server" CssClass="table table-bordered" OnPreRender="gv_PreRender" ClientIDMode="Static" ShowHeaderWhenEmpty="True" OnRowDataBound="gv_RowDataBound" OnRowUpdated="gv_RowUpdated" OnRowUpdating="gv_RowUpdating" OnRowCancelingEdit="gv_RowCancelingEdit" AutoGenerateColumns="False" ShowFooter="True" OnRowEditing="gv_RowEditing">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSrNo" runat="server" Text='<%# Eval("SrNo")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Item Code  <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblItemCode" runat="server" Text='<%# Eval("ItemCode")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="ddlItems" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                <asp:DropDownList ID="ddlItems" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single"></asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Item Unit  <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblUnit" runat="server" Text='<%# Eval("ItemUnit")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <%--<asp:TextBox ID="txtEditUnit" CssClass="form-control" runat="server" Text='<%# Eval("ItemUnit")%>'></asp:TextBox>--%>
                                                                <asp:DropDownList ID="ddlEditUnit" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single" SelectedValue='<%# Bind("ItemUnit") %>'>
                                                                    <asp:ListItem Value="N/A">--- Select Unit ---</asp:ListItem>
                                                                    <asp:ListItem Value="Unit">Unit</asp:ListItem>
                                                                    <asp:ListItem Value="Box">Box</asp:ListItem>
                                                                    <asp:ListItem Value="Carton">Carton</asp:ListItem>
                                                                    <asp:ListItem Value="Ounce">Ounce</asp:ListItem>
                                                                    <asp:ListItem Value="Pound">Pound</asp:ListItem>
                                                                    <asp:ListItem Value="Gram">Gram</asp:ListItem>
                                                                    <asp:ListItem Value="KG">KG</asp:ListItem>
                                                                    <asp:ListItem Value="Ton">Ton</asp:ListItem>
                                                                    <asp:ListItem Value="Centimeter">Centimeter</asp:ListItem>
                                                                    <asp:ListItem Value="Inch">Inch</asp:ListItem>
                                                                    <asp:ListItem Value="Foot">Foot</asp:ListItem>
                                                                    <asp:ListItem Value="Yard">Yard</asp:ListItem>
                                                                    <asp:ListItem Value="Meter">Meter</asp:ListItem>
                                                                    <asp:ListItem Value="Kilometer">Kilometer</asp:ListItem>
                                                                    <asp:ListItem Value="Mile">Mile</asp:ListItem>
                                                                    <asp:ListItem Value="Pieces">Pieces</asp:ListItem>
                                                                    <asp:ListItem Value="Job">Job</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" ControlToValidate="ddlUnit" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                <%--<asp:TextBox ID="txtUnit" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                                                <asp:DropDownList ID="ddlUnit" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single">
                                                                    <asp:ListItem Value="N/A">--- Select Unit ---</asp:ListItem>
                                                                    <asp:ListItem Value="Unit">Unit</asp:ListItem>
                                                                    <asp:ListItem Value="Box">Box</asp:ListItem>
                                                                    <asp:ListItem Value="Carton">Carton</asp:ListItem>
                                                                    <asp:ListItem Value="Ounce">Ounce</asp:ListItem>
                                                                    <asp:ListItem Value="Pound">Pound</asp:ListItem>
                                                                    <asp:ListItem Value="Gram">Gram</asp:ListItem>
                                                                    <asp:ListItem Value="KG">KG</asp:ListItem>
                                                                    <asp:ListItem Value="Ton">Ton</asp:ListItem>
                                                                    <asp:ListItem Value="Centimeter">Centimeter</asp:ListItem>
                                                                    <asp:ListItem Value="Inch">Inch</asp:ListItem>
                                                                    <asp:ListItem Value="Foot">Foot</asp:ListItem>
                                                                    <asp:ListItem Value="Yard">Yard</asp:ListItem>
                                                                    <asp:ListItem Value="Meter">Meter</asp:ListItem>
                                                                    <asp:ListItem Value="Kilometer">Kilometer</asp:ListItem>
                                                                    <asp:ListItem Value="Mile">Mile</asp:ListItem>
                                                                    <asp:ListItem Value="Pieces">Pieces</asp:ListItem>
                                                                    <asp:ListItem Value="Job">Job</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Quantity  <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblQty" runat="server" Text='<%# Eval("Quantity")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditQty" TextMode="Number" CssClass="form-control" runat="server" Text='<%# Eval("Quantity")%>' AutoPostBack="true" OnTextChanged="txtEditUnitPrice_TextChanged"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator14" ControlToValidate="txtQty" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                <asp:TextBox ID="txtQty" TextMode="Number" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtNetPrice_TextChanged"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Unit Price (PKR)  <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Eval("UnitPrice")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditUnitPrice" TextMode="Number" CssClass="form-control" runat="server" Text='<%# Eval("UnitPrice")%>' AutoPostBack="true" OnTextChanged="txtEditUnitPrice_TextChanged"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator15" ControlToValidate="txtUnitPrice" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                <asp:TextBox ID="txtUnitPrice" TextMode="Number" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtNetPrice_TextChanged"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Net Price (PKR)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNetPrice" runat="server" Text='<%# Eval("NetPrice")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditNetPrice" TextMode="Number" CssClass="form-control" runat="server" disabled Text='<%# Eval("NetPrice")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtNetPrice" TextMode="Number" runat="server" disabled CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField HeaderText="Remarks">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditRemarks" CssClass="form-control" runat="server" Text='<%# Eval("Remarks")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>--%>

                                                        <asp:TemplateField HeaderText="Expense COA  <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlEditExpCOA" ClientIDMode="Static" data-plugin="select2" runat="server" CssClass="form-control js-example-basic-single select2"></asp:DropDownList>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidatorExpCOA" ControlToValidate="ddlExpCOA" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />--%>
                                                                <asp:DropDownList ID="ddlExpCOA" ClientIDMode="Static" data-plugin="select2" runat="server" CssClass="form-control js-example-basic-single select2"></asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAdd" runat="server" CssClass="form-control" Text="Add" ValidationGroup="btnValidatef" CausesValidation="true" OnClick="btnAdd_Click"></asp:Button>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField FooterStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemove_Command"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lblTotal" CssClass="total" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Total Amount  <span style="color: red !important;">*</span></label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtTotalAmount" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="0" id="txtTotalAmount" disabled="disabled" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Tax   <span style="color: red !important;">*</span></label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator20" ControlToValidate="txtTotalTax" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="0" id="txtTotalTax" disabled="disabled" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Net Amount   <span style="color: red !important;">*</span></label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator21" ControlToValidate="txtNetAmount" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="0" id="txtNetAmount" disabled="disabled" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <div>&nbsp;</div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group add-atr-file-upload">
                                        <asp:FileUpload ID="itemAttachments" class="btn btn-primary" ClientIDMode="Static" runat="server" AllowMultiple="true" Style="display: none;"></asp:FileUpload>
                                        <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('itemAttachments').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                        Include Files (Attach Here)
                                    </div>
                                </div>
                            </div>

                            <div>&nbsp;</div>
                        </div>
                        <div class="dashboard-left inner-page" id="divAttachments" runat="server">
                            <h4>Attachments</h4>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <asp:GridView ID="gvAttachments" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeader="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="File Name">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("AttachmentPath") %>'><%# "File - " + Eval("POAttachmentID") %></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                    <!-- Modal -->
                    <div class="modal fade" id="notes-modal" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Notes</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="paynow-modal" role="dialog">
                        <div class="modal-dialog" style="width:900px; max-width:100%">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Receive Now</h4>
                                </div>
                                <div class="modal-body">

                                    <asp:UpdatePanel ID="UPPayNow" runat="server">
                                        <ContentTemplate>


                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Remaining Amount</label>
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" value="0" placeholder="0" id="txtRemainingAmount" disabled="disabled" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Date</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="DD-MM-YYYY" id="txtDatePN" disabled="disabled" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Amount   <span style="color: red !important;">*</span></label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtAmountPN" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" />
                                                    <asp:CompareValidator runat="server" ID="cmpNumbers" ControlToValidate="txtAmountPN" ControlToCompare="txtRemainingAmount" Operator="LessThanEqual" Type="Double" ErrorMessage="The amount must be less then or equal to remaining amount" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" />
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" value="0" placeholder="0" id="txtAmountPN" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>WOT   <span style="color: red !important;">*</span></label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator26" ControlToValidate="txtWOT" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" />
                                                    <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="txtAmountPN" ControlToCompare="txtRemainingAmount" Operator="LessThanEqual" Type="Double" ErrorMessage="The amount must be less then or equal to remaining amount" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" />
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" value="0" placeholder="0" id="txtWOT" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>STWH <span style="color: red !important;">*</span></label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtWOT" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" />
                                                    <asp:CompareValidator runat="server" ID="CompareValidator2" ControlToValidate="txtAmountPN" ControlToCompare="txtRemainingAmount" Operator="LessThanEqual" Type="Double" ErrorMessage="The amount must be less then or equal to remaining amount" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" />
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" value="0" placeholder="0" id="txtSTWH" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Instrument</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"  placeholder="ref." id="txtInstrument" runat="server" />
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <label>Pay By <span style="color: red !important;">*</span> </label>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlPayBy" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" />
                                                    <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2"  ID="ddlPayBy">
                                                        <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                                        <asp:ListItem Value="Bank">Bank</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-3" id="accDiv" runat="server" style="display:none;">
                                                    <label>Account</label>
                                                    <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlAccounts" >
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-3" runat="server" id="divForCOA">
                                                    <label>COA <span style="color: red !important;">*</span></label>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" InitialValue="0" ControlToValidate="ddlCOAPN" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" />
                                                    <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlCOAPN">
                                                    </asp:DropDownList>
                                                </div>


                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <h4>Transaction Detail</h4>
                                                        <textarea class="form-control" rows="3"  id="txtTransactionDetail" runat="server"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="btnPay" "Pay" runat="server" onserverclick="btnPay_ServerClick" validationgroup="btnValidatePN" class="btn btn-default" data-dismiss="modal">Pay</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal fade" id="voucher-modal" role="dialog">
                        <div class="modal-dialog" style="width: fit-content; max-width: 100%;">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div style="display: none">
                                        <button type="button" runat="server" id="GenVoucherPDF" onserverclick="GenVoucherPDF_ServerClick"></button>

                                    </div>
                                    <button type="button" "Close" style="float: right;" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <h4 class="modal-title">Vouchers</h4>
                                </div>
                                <div class="modal-body">

                                    <asp:UpdatePanel ID="UpVoucherModel" runat="server">
                                        <ContentTemplate>
                                    <asp:HiddenField ID="hdnTransactionID" Value="0" runat="server" />

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <asp:GridView ID="gvVoucherModel" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="Serial No">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lclSerialNo" Text='<%# Eval("SerialNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Voucher No">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblVoucherNo" Text='<%# Eval("VoucherNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Voucher Type">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblType" Text='<%# Eval("Type") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Total Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblVoucherAmount" Text='<%#  Eval("VoucherAmount") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Date">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblCol3" Text='<%#  Eval("VoucherDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("DocStatus") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Print">
                                                                <ItemTemplate>
                                                                    <a target="_blank" href="<%# "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/grn-journal-voucher/generate-" + Eval("DirectPaymentID") %>" class="edit-class" "Print">Print</a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="View">
                                                                <ItemTemplate>
                                                                    <a target="_blank" href="<%# "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/direct-payment/edit-direct-payment-" + Eval("DirectPaymentID") %>" class="edit-class" "View">View</a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>





                    <div class="modal fade" id="SupplierModel" role="dialog">

                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Add Supplier</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="input-group input-group-lg">
                                                <label>Busniess Name </label>
                                                <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator22" ControlToValidate="txtBusinessName" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="sbtnValidate" ForeColor="Red" />
                                                <input type="text" class="form-control form-text" placeholder="Business Name" id="txtBusinessName" runat="server">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <label>NTN </label>
                                            <div class="input-group input-group-lg">
                                                <input class="form-control phone-no-val" placeholder="NTN" type="text" id="txtNTN" runat="server">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <div class="input-group input-group-lg">
                                                <label>Point of Contact </label>
                                                <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtName" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="sbtnValidate" ForeColor="Red" />
                                                <input type="text" class="form-control form-text" placeholder="Name" id="txtName" runat="server">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <label>Phone </label>
                                            <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="txtPhone" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="sbtnValidate" ForeColor="Red" />
                                            <div class="input-group input-group-lg">
                                                <input class="form-control phone-no-val" placeholder="(XXX) XXX-XXXX" maxlength="14" type="text" id="txtPhone" runat="server">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <label>Email</label>
                                            <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtEmail" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="sbtnValidate" ForeColor="Red" />
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic" runat="server" ControlToValidate="txtEmail" ForeColor="Red" ErrorMessage=" Email Address Not Valid" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="btnValidate"></asp:RegularExpressionValidator>--%>

                                            <div class="input-group input-group-lg">

                                                <input type="text" class="form-control form-text" placeholder="Email" id="txtEmail" runat="server">
                                            </div>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="row">
                                                <div class="col-md-4 col-sm-12">
                                                    <label>Select Country</label>
                                                    <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator16" ControlToValidate="ddlCountries" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="sbtnValidate" ForeColor="Red" />
                                                    <div class="input-group input-group-lg">
                                                        <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control form-text input-group js-example-basic-single" ID="ddlCountries" AutoPostBack="true" OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged">
                                                            <asp:ListItem>--- Select Country ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <label>Select State</label>
                                                    <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator17" ControlToValidate="ddlStates" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="sbtnValidate" ForeColor="Red" />
                                                    <div class="input-group input-group-lg">
                                                        <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control form-text input-group js-example-basic-single" ID="ddlStates" AutoPostBack="true" OnSelectedIndexChanged="ddlStates_SelectedIndexChanged">
                                                            <asp:ListItem>--- Select State ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <label>Select City</label>
                                                    <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator18" ControlToValidate="ddlCities" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="sbtnValidate" ForeColor="Red" />
                                                    <div class="input-group input-group-lg">
                                                        <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control form-text input-group js-example-basic-single" ID="ddlCities">
                                                            <asp:ListItem>--- Select City ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <label>Adress Line 1</label>
                                            <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator19" ControlToValidate="txtAddressLine1" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="sbtnValidate" ForeColor="Red" />
                                            <div class="input-group input-group-lg">
                                                <input type="text" class="form-control form-text" placeholder="Address Line 1" id="txtAddressLine1" runat="server">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <label>Adress Line 2</label>
                                            <div class="input-group input-group-lg">
                                                <input type="text" class="form-control form-text" placeholder="Address Line 2" id="txtAddressLine2" runat="server">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <label>Zip / Postal Code</label>
                                            <div class="input-group input-group-lg">
                                                <input type="Number" class="form-control form-text" placeholder="ZIP Code / Postal Code" id="txtZipCode" runat="server">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="input-group input-group-lg">
                                                <label>Bank Name</label>
                                                <input type="text" class="form-control form-text" placeholder="Bank Name" id="txtBankName" runat="server">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <label>Account No </label>
                                            <div class="input-group input-group-lg">
                                                <input class="form-control phone-no-val" placeholder="Account No" type="text" id="txtAccountNo" runat="server">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" id="btnAddSupplier" validationgroup="sbtnValidate" runat="server" onserverclick="btnAddSupplier_ServerClick">Add</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
            <uc:Footer ID="footer1" runat="server" />
            <!-- .wrap -->
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>


            function showModal() {
                //  $('#hdnAssetID').val(id);
                //$('#myModal').show();
            }

            $("#ddlPayBy").change(function () {
                var payBy = $(this).val();
                if (payBy == "Bank") {
                    $('#accDiv').css('display', 'block');
                    $('#divForCOA').css('display','none');

                } else {
                    $('#accDiv').css('display', 'none');
                    $('#divForCOA').css('display', 'block');
                }
            });


        </script>
    </form>
    <style>
        select#ddlSupplier {
            padding: 8px;
            width: 100%;
        }

        .tf-back-btn {
            padding: 6px !important;
        }

        button.tf-save-btn {
            padding: 6px !important;
        }

        .tf-note-btn-new {
            padding: 6px !important;
            height: 40px !important;
            width: 40px !important;
            background-color: #575757 !important;
            border-radius: 0px !important;
            border: none;
            box-shadow: 0 8px 6px -5px #cacaca !important;
            display: inline-block;
            float: left;
            margin-right: 4px !important;
        }

            .tf-note-btn-new i {
                color: #fff !important;
            }
    </style>

</body>
</html>
