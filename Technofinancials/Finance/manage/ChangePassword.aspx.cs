﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                divAlertMsg.Visible = false;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {
            string returnMsg = "";
            if (Session["UserPassword"].ToString() == txtOldPassword.Value)
            {
                Session["UserPassword"] = txtNewPassword.Value;
                objDB.UserID = Convert.ToInt32(Session["UserID"]);
                objDB.Password = txtNewPassword.Value;
                returnMsg = objDB.UpdatePassword();
            }
            else
                returnMsg = "Wrong Password";


            if (returnMsg == "Password Updated")
            {
                Common.addlog("Update", "Finance", "User \"" + Session["UserName"] + "\" Password Updated", "Users", objDB.UserID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = returnMsg;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = returnMsg;
            }
        }

    }
}