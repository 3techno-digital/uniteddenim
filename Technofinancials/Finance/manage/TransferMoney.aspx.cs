﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class TransferMoney : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int BalTraID
        {
            get
            {
                if (ViewState["BalTraID"] != null)
                {
                    return (int)ViewState["BalTraID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["BalTraID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {   
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/transfer-money";
         
                    divAlertMsg.Visible = false;
                    clearFields();
                    BindFromAccount();
                    BindToAccount();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["BalanceTranferID"] != null)
                    {
                        BalTraID = Convert.ToInt32(HttpContext.Current.Items["BalanceTranferID"].ToString());
                        getDataByID(BalTraID);
                        CheckAccess();

                    }

                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


      
        private void BindFromAccount()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlFromAccount.Items.Clear();
            ddlFromAccount.DataSource = objDB.GetAllApprovedAccount(ref errorMsg);
            ddlFromAccount.DataTextField = "AccountLongName";
            ddlFromAccount.DataValueField = "AccountID";
            ddlFromAccount.DataBind();
            ddlFromAccount.Items.Insert(0, new ListItem("--- Select Account- --", "0"));

        }

        private void BindToAccount()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlToAccount.Items.Clear();
            ddlToAccount.DataSource = objDB.GetAllApprovedAccount(ref errorMsg);
            ddlToAccount.DataTextField = "AccountLongName";
            ddlToAccount.DataValueField = "AccountID";
            ddlToAccount.DataBind();
            ddlToAccount.Items.Insert(0, new ListItem("--- Select Account- --", "0"));

        }





        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.BalanceTransferID = ID;
            dt = objDB.GetBalanceTransferByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    
                    ddlFromAccount.SelectedValue = dt.Rows[0]["FromAccount"].ToString();
                    ddlToAccount.SelectedValue = dt.Rows[0]["ToAccount"].ToString();
                    txtAmount.Value = dt.Rows[0]["AmountTransfer"].ToString();
                    txtDate.Value = DateTime.Parse(dt.Rows[0]["TransferDate"].ToString()).ToString("dd-MMM-yyyy"); 
                    txtDescription.Value = dt.Rows[0]["Description"].ToString();

                }
            }
            Common.addlog("View", "Finance", "Balance Transfer by ID \"" + (objDB.BalanceTransferID).ToString() + "\" Viewed", "BalanceTransfer", objDB.BalanceTransferID);

        }

        private void clearFields()
        {


            ddlFromAccount.SelectedIndex = -1;
            ddlToAccount.SelectedIndex = -1;
            txtAmount.Value = "0";
            txtDate.Value = "";
            txtDescription.Value = "";

        }

        private void CheckSessions()
        {
            if (Session["userid"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());


                objDB.FromAccount = Convert.ToInt32(ddlFromAccount.SelectedValue);
                objDB.ToAccount = Convert.ToInt32(ddlToAccount.SelectedValue);
                objDB.AmountTransfer = Convert.ToDouble(txtAmount.Value);
                objDB.TransferDate = txtDate.Value;
                objDB.Description = txtDescription.Value;

                if (HttpContext.Current.Items["BalanceTranferID"] != null)
                {
                    objDB.BalanceTransferID = BalTraID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateBalanceTransfer();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddBalanceTransfer();

                    clearFields();
                }





                if (res == "Balance Transfer Added" || res == "Balance Transfer Updated")
                {
                    if (res == "Balance Transfer Added") { Common.addlog("Add", "Finance", " Balance Transfer by ID\"" + (objDB.BalanceTransferID).ToString() + "\" Added", "BalanceTransfer"); }
                    if (res == "Balance Transfer Updated") { Common.addlog("Update", "Finance", "Balance Transfer by ID \"" + (objDB.BalanceTransferID).ToString() + "\" Updated", "BalanceTransfer", objDB.BalanceTransferID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "BalanceTransfer";
                objDB.PrimaryColumnnName = "BalanceTranferID";
                objDB.PrimaryColumnValue = BalTraID.ToString();
                objDB.DocName = "TransferMoney";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "BalanceTransfer", "BalanceTranferID", HttpContext.Current.Items["BalanceTranferID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "Finance", "Balance Tranfer of ID\"" + HttpContext.Current.Items["BalanceTranferID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/transfer-money", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/transfer-money/edit-transfer-money-" + HttpContext.Current.Items["BalanceTranferID"].ToString(), "Balance Tranfer of Amount \"" + txtAmount.Value + "\"", "BalanceTranfer", "TransferMoney", Convert.ToInt32(HttpContext.Current.Items["BalanceTranferID"].ToString()));

                //Common.addlog(res, "Finance", "Balance Transfer of ID \"" + HttpContext.Current.Items["BalanceTranferID"].ToString() + "\" Status Changed", "BalanceTransfer", BalTraID /* ID From Page Top  */);/* Button1_ServerClick  */
                if (res == "Approved Sucessfull" || res == "Reviewed & Approved Sucessfull")
                {

                    objDB.BalanceTransferID= BalTraID;
                    objDB.FromAccount = Convert.ToInt32(ddlFromAccount.SelectedValue);
                    objDB.ToAccount = Convert.ToInt32(ddlToAccount.SelectedValue);
                    objDB.AmountTransfer = Convert.ToDouble(txtAmount.Value);
                    string s=objDB.TransferBalance();

                }
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "BalanceTransfer", "BalanceTranferID", HttpContext.Current.Items["BalanceTranferID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "Balance Transfer of ID \"" + HttpContext.Current.Items["BalanceTranferID"].ToString() + "\" deleted", "BalanceTransfer", BalTraID /* ID From Page Top  */);/* lnkDelete_Click  */



                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

    }
}