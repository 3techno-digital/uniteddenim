﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="COATransferMoney.aspx.cs" Inherits="Technofinancials.Finance.manage.COATransferMoney" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/TransferMoney.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">COA Transfer Money</h3>
                        </div>

                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>


                                <div class="col-sm-4">
                                    <div class="pull-right flex">
                                        <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                        <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                        <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class" "Delete" CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                        <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                        <button class="tf-save-btn" "Reject & Disapproved" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>


                                        <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Date
                                        <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                        <input type="text" class="form-control" placeholder="Date" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" name="companyShortName" id="txtDate" runat="server" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            From Account
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlFromCOA" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <asp:Label CssClass="lbl-COA-Balance"  Text="Balance: 0" ID="lblFromCOABal" runat="server" />

                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlFromCOA_SelectedIndexChanged" ID="ddlFromCOA">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            To Account
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlToCOA" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /><asp:CompareValidator runat="server" ID="cv1" ControlToValidate="ddlToCOA" ControlToCompare="ddlFromCOA" Operator="NotEqual" ErrorMessage="Can't transfer to same Account" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:CompareValidator>
                                            <asp:Label CssClass="lbl-COA-Balance"  Text="Balance: 0" ID="lblCOABal" runat="server" />
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlCOA_SelectedIndexChanged" ID="ddlToCOA">
                                        </asp:DropDownList>
                                    </div>
                                </div>





                            </div>
                            <div class="row">

                                <div class="col-md-4">

                                    <div class="input-group input-group-lg">
                                        <label>
                                            Amount
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtAmount" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                        <input type="number" value="0" class="form-control" id="txtAmount" runat="server" placeholder="0" />
                                    </div>
                                </div>

                            </div>

                               <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h4>Description</h4>
                                        <textarea class="form-control" rows="3" name="Description" id="txtDescription" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix">&nbsp;</div>

                        


                        </ContentTemplate>
                    </asp:UpdatePanel>

                </section>


                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }

            .input-lg, .input-group-lg > .form-control, .input-group-lg > .input-group-addon, .input-group-lg > .input-group-btn > .btn {
                height: 38px !important;
            }
        </style>
    </form>
</body>
</html>
