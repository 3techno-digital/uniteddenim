﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadLoanSchedule.aspx.cs" Inherits="Technofinancials.Finance.Manage.UploadLoanSchedule" %>


<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        h3.AD_btn_inn {
            padding: 3px 24px;
            font-size: 18px;
        }

        #errmsg {
            color: red;
        }

        input.form-control.input-sm {
            display: none;
        }

        input#FileUpload1 {
            background-color: transparent !important;
            color: #000 !important;
        }

        label {
            display: block !important;
        }

        span#RequiredFieldValidator2, span#RequiredFieldValidator3, span#RequiredFieldValidator1 {
            color: red;
        }

        .dataTables_filter {
            margin-bottom: 5px;
            display: none;
        }

        .dt-buttons {
            display: none;
        }

        .amount {
            text-align: center;
            padding-top: 50px;
            color: #003780;
        }

            .amount h5 {
                font-size: 18px;
            }

            .amount h5 {
                font-family: Noto-Regular !important;
            }

            .amount span {
                font-family: Noto-SemiBold !important;
                display: block;
                font-size: 30px;
            }

        i.fa.fa-cloud-download {
            margin-right: 10px;
        }

        .tf-back-btn {
            background-color: #575757;
            padding: 10px 10px 10px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

        input#Button1 {
            padding: 4px 24px;
        }

        .tf-back-btn i {
            color: #fff !important;
        }

        .total {
            font-weight: bold;
            font-size: 20px;
            color: #188ae2;
        }

        h3.AD_btn_inn a {
            color: #fff;
        }

            h3.AD_btn_inn a:hover {
                background: #fff;
                color: #003780;
            }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">

            <div class="wrap">
   
  



               <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Upload Schedule</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                     <button class="AD_btn" id="btnSave" visible="false" runat="server" onserverclick="ButtonSave_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                  <button class="AD_btn"  id="downloadsuccess" runat="server" onserverclick="btnsuccess_ServerClick" validationgroup="btnValidate" type="button">Download</button>
                                 <a class="AD_btn" href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/UploadLoanSchedule"); %>">Back</a>
                                       <a id="template" runat="server" href="/assets/files/LoanPaymentSchedule/TemplateLoanPaymentSchedule.csv">
                                            <h3 class="AD_btn_inn" style="float: right; margin: 0px;"><i class="fa fa-cloud-download" aria-hidden="true"></i>
                                               Template </h3>
                                        </a>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                           <div class="form-group">
                                                <h4>Employee<span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="ddlEmployee" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                </h4>

                                                <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
                                            </div>
                                    </div></div>
                       <br /><br />
                             <div class="col-md-6">
                                  
                                     
                                            <asp:FileUpload
                                                ID="FileUpload1"
                                                runat="server"
                                                BackColor="DarkBlue"
                                                ForeColor="AliceBlue" />

                                            <p>
                                                <asp:Label ID="LblMessage" runat="server" Font-Bold="true"></asp:Label>
                                            </p>
                                            <%--<asp:FileUpload ID="FileUpload1" runat="server"/>    
             <asp:RequiredFieldValidator ID="rfvFileupload" ValidationGroup="validate" runat="server" Display="Dynamic" ErrorMessage="* required" ControlToValidate="FileUpload1"></asp:RequiredFieldValidator>--%>

                                            <asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="FileUpload1"
                                                ErrorMessage="Choose a file!">
                                            </asp:RequiredFieldValidator>
                              

                                    
                                </div>
                                  
                            
                        </div>
                                                </div>
                              <div class="col-lg-4 col-md-6 col-sm-12">  <br /><br /> <div class="col-sm-6">
                                    <contenttemplate>
                                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Upload" class="AD_btn_inn" />


                                    </contenttemplate>
                                </div>  </div>
                      
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">  <div class="form-group" id="div1" runat="server">
                                        <div class="alert tf-alert-info" id="infomsg" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p>
                                                1) Only .csv file is allowed
                                                
                                            </p>
                                              <p>
                                                2) Download the template
                                               
                                            </p>
                                              <p>
                                                3) Avoid using COMMA (,) in amount
                                             
                                            </p>
                                          
                                        </div>
                                    </div></div>
                                
                                <div class="col-sm-12">
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->










                        <!-- /.col -->
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="true">
                                        <Columns>
                                         

                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                            <%-- <table class="table table-bordered gv dataTable no-footer">
                              <thead>
                                  <tr>
                                      <td>S.NO</td>
                                      <td>Submition Date</td>
                                      <td>Total Claim</td>
                                      <td>Payroll Cycle</td>
                                      <td>Status </td>
                                      <td>File</td>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>

                                  </tr>
                              </tbody>
                          </table>--%>
                        </div>
                    </div>
                    <!-- /.row -->
                    <!-- /.container-fluid -->
                </section>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">&nbsp;</div>
                <br />
                <br />
                <br />


            </div>
            <uc:Footer ID="footer1" runat="server" />
            <!-- .wrap -->
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script>
            $(document).ready(function () {

                //called when key is pressed in textbox
                $("#txtSubj").keypress(function (e) {
                    //if the letter is not digit then display error and don't type anything
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        //display error message
                        $("#errmsg").html("Enter number only").show();
                        return false;
                    }
                });
            });
        </script>









        <!-- Modal -->
    </form>
</body>
</html>





