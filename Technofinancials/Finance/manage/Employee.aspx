﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Employee.aspx.cs" Inherits="Technofinancials.Finance.Manage.Employee" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>
		.detail .select2-container {
			width: 196px !important;
		}

		@media only screen and (max-width: 991px) and (min-width: 768px) {
			.detail .select2-container {
				width: 227px !important;
			}

			table#gvQualification .select2-container {
				width: 100px !important;
			}

			.col-lg-12 .select2-container {
				width: 469px !important;
			}

			h1.m-0.text-dark {
				font-size: 20px !important;
			}

			table#gvQualification {
				overflow-x: scroll;
				overflow-y: hidden;
				padding-bottom: 35px;
			}
				/* Track */
				table#gvQualification ::-webkit-scrollbar-track {
					-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
					background-color: #FFF;
				}

				table#gvQualification ::-webkit-scrollbar {
					height: 10px;
					background-color: #ffffff;
				}

				table#gvQualification ::-webkit-scrollbar-thumb {
					background-color: #003780;
					border: 2px solid #003780;
					border-radius: 10px;
				}
		}

		@media only screen and (max-width: 1024px) and (min-width: 992px) {
			.detail .select2-container {
				width: 160px !important;
			}

			.form-group h4 {
				font-size: 13px !important;
			}

			.table_out_data {
				width: 345px;
			}

			label {
				font-size: 12px !important;
			}
		}

		@media only screen and (max-width: 1280px) and (min-width: 800px) {
			.detail .select2-container {
				width: 145px !important;
			}

			.form-group h4 {
				font-size: 13px !important;
			}

			.table_out_data {
				width: 345px;
			}

			label {
				font-size: 12px !important;
			}
		}

		@media only screen and (max-width: 1440px) and (min-width: 900px) {

			.detail .select2-container {
				width: 170px !important;
			}

			.col-lg-12 .select2-container {
				width: 100% !important;
			}
			/*                            .form-group .select2-container {
                                width: 150px !important;
                            }*/
		}

		.select2-hidden-accessible {
			position: relative !important;
		}

		.modal-dialog {
			width: 450px;
			margin: 30px auto;
		}

		.table_out_data {
			padding-left: 10px;
		}

		@media only screen and (max-width: 1400px) and (min-width: 1300px) {
			.table_out_data label {
				font-size: 12px !important;
			}
		}

		@media only screen and (max-width: 1300px) and (min-width: 1281px) {
			.table_out_data label {
				font-size: 12px !important;
			}
		}

		@media only screen and (max-width: 1280px) and (min-width: 1200px) {
			.table_out_data label {
				font-size: 12px !important;
			}
		}

		section.app-content {
			padding-bottom: 53px;
		}

		.modal-footer {
			border-top: 0px solid #e5e5e5;
		}

		.table-bordered > thead > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > tfoot > tr > td {
			font-size: 12px;
		}

		table#gvExperience td:nth-child(6) {
			text-align: center;
		}

		.hiring-create-new-btn {
			margin-top: 17px;
			border: none;
			background: none;
			color: #01769a;
		}

		.table > thead > tr > th {
			width: 500px !important;
		}

		div#experience-modal .input-group {
			width: 100% !important;
		}

		div#qualification-modal .input-group {
			width: 100% !important;
		}

		div#certification-modal .input-group {
			width: 100% !important;
		}

		div#training-modal .input-group {
			width: 100% !important;
		}

		div#skill-modal .input-group {
			width: 100% !important;
		}

		.checkk {
			margin: -8px !important;
		}

		.table_out_data {
			margin: 5px 0px;
		}

		.tf-back-btn {
			background-color: #575757;
			padding: 10px 10px 10px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-back-btn i {
				color: #fff !important;
			}

		.total {
			font-weight: bold;
			font-size: 20px;
			color: #188ae2;
		}

		.content-header.second_heading .container-fluid {
			padding-left: 0px;
		}

		.content-header.second_heading.content-header.second_heading h1 {
			margin-left: 0px !important;
		}

		div#exTab2 ul li a {
			padding-left: 7px !important;
			padding-right: 7px !important;
		}

		.user-img-div img {
			width: 180px !important;
			height: 180px !important;
			border: 1px solid white !important;
			margin: 5px auto;
			padding: 5px;
		}

		div#exTab2 ul li a {
			padding-right: 6px !important;
		}

		.employe-errors {
			margin-top: 15px;
		}

		.form-control {
			font-size: 14px !important;
		}

		.form-group h4, label {
			font-size: 15px;
		}

		.employe-errors li {
			width: 30%;
			display: inline-block;
		}

			.employe-errors li:after {
				color: red;
				content: "*";
				margin-left: 5px;
			}

		#errmsg {
			color: red;
		}

		.tab-content {
			margin-bottom: 15%;
		}

		.checkk {
			position: absolute;
			top: 14px;
			left: 20px;
		}

		.checkk_lab {
			margin-left: 30px;
		}

		input[type="radio"], input[type="checkbox"] {
			margin: 4px 0 0;
			margin-top: 1px 9;
			line-height: normal;
			width: 16px !important;
			height: 25px !important;
			line-height: normal !important;
			margin-left: 0px !important;
		}

		section.app-content {
			padding-bottom: 53px !important;
		}

		.employe-tables h4 {
			font-weight: inherit;
			padding-left: initial;
		}

		.modal-content {
			top: 50px;
			width: 450px;
			position: fixed;
			z-index: 1050;
			left: 20%;
			border-radius: 5%;
			height: 100%;
			box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
		}

		.table-bordered th {
			width: 45px;
		}
	</style>

</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">
			<asp:UpdateProgress ID="updProgress"
				AssociatedUpdatePanelID="UpdatePanel10"
				runat="server">
				<ProgressTemplate>
					<div class="upd_panel">
						<div class="center">
							<img src="/assets/images/Loading.gif" />
						</div>
					</div>
				</ProgressTemplate>
			</asp:UpdateProgress>
			<div class="wrap">
				<asp:UpdatePanel runat="server">
					<ContentTemplate>
						<div class="content-header">
							<div class="container-fluid">
								<div class="row mb-2">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<h1 class="m-0 text-dark">Personnel Management</h1>
									</div>
									<!-- /.col -->
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
										<div style="text-align: right;">
											<button type="button" runat="server" id="btnCalculator" class="AD_btn" data-toggle="modal" data-target="#salary-modal" value="Calculator">Calculator</button>
											<button class="AD_btn" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
											<button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
											<button class="AD_btn" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
											<asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
											<asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
											<button class="AD_btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button><%--Disapprove--%>
											<button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
											<button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Send</button><%--Send--%>
											<button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
											<a class="AD_btn" id="btnBack" runat="server">Back</a>
										</div>
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.container-fluid -->
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
				<section class="app-content">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<asp:UpdatePanel ID="UpdatePanel10" runat="server">
									<ContentTemplate>
										<asp:UpdatePanel ID="btnUpdPnl" runat="server">
											<ContentTemplate>
												<div class="form-group" id="divAlertMsg" runat="server">
													<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
														<span>
															<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
														</span>
														<p id="pAlertMsg" runat="server">
														</p>
													</div>
												</div>
											</ContentTemplate>
										</asp:UpdatePanel>
										<!-- Modal -->
										<div class="modal fade M_set" id="notes-modal" role="dialog">
											<div class="modal-dialog">
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
														<h1 class="m-0 text-dark">Notes</h1>
														<div class="add_new">
															<button type="button" class="AD_btn" data-dismiss="modal">Save</button>
															<button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
														</div>
													</div>
													<div class="modal-body">
														<p class="D_NONE">
															<asp:Literal Visible="false" ID="ltrNotesTable" runat="server"></asp:Literal>
														</p>
														<p>
															<textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
														</p>
													</div>
												</div>
											</div>
										</div>
									</ContentTemplate>
									<Triggers>
										<asp:PostBackTrigger ControlID="btnSave" />
									</Triggers>
								</asp:UpdatePanel>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="row">
								<div class="col-md-12">
									<asp:ValidationSummary runat="server" CssClass="alert alert-danger employe-errors" ID="ValSummary" HeaderText="You must enter a value in the following fields:" EnableClientScript="true" ValidationGroup="btnValidate" />
								</div>
							</div>
						</div>

						<!-- Modal -->
						<div class="modal fade M_set" id="salary-modal" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<h1 class="m-0 text-dark">Salary Calculator</h1>
										<div class="add_new">
											<button type="button" class="AD_btn" value="Copy" data-dismiss="modal" id="btnCopyFields" onclick="copyCalculationsToMainFields();">Copy</button>
											<button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
										</div>
									</div>

									<div class="modal-body">
										<asp:UpdatePanel ID="GrossSalupd" runat="server">
											<ContentTemplate>
												<div class="row">
													<div class="col-sm-12">
														<label>Gross Salary</label>
														<input class="form-control" runat="server" id="txtCalcGrossSalary" type="number" value="0" onkeyup="txtGrossSalary_TextChanged();" />
													</div>
													<div class="col-sm-12">
														<label>
															Basic Salary<br />
															(66.67 % of Gross Salary)</label>
														<input type="number" class="form-control" disabled="disabled" id="txtCalcBasicSalary" value="0" runat="server" />
													</div>

													<div class="col-sm-12">
														<label>
															House Rent Allowance<br />
															(35 % of Basic Salary)</label>
														<input type="number" class="form-control" disabled="disabled" id="txtCalcHouseRent" value="0" runat="server" />
													</div>

													<div class="col-sm-12">
														<label>
															Utilities Allowance<br />
															(5% of Basic Salary)
														</label>
														<input type="number" class="form-control" disabled="disabled" id="txtCalcUtilities" value="0" runat="server" />
													</div>

													<div class="col-sm-12">
														<label>
															Medical Allowance<br />
															(10 % of Basic Salary)
														</label>
														<input type="number" class="form-control" disabled="disabled" id="txtCalcMedical" value="0" runat="server" />
													</div>
												</div>
											</ContentTemplate>
										</asp:UpdatePanel>
									</div>
								</div>

							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-sm-12">
							<div id="exTab2" class="">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#1" data-toggle="tab">Basic Information</a>
									</li>
									<li>
										<a href="#2" data-toggle="tab">Qualification and Experiences</a>
									</li>
									<li>
										<a href="#3" data-toggle="tab">Employment details</a>
									</li>

									<li>
										<a href="#5" data-toggle="tab">Bank Details</a>
									</li>
									<li>
										<a href="#4" data-toggle="tab">Payroll</a>
									</li>

									<li style="display:none;" >
										<a href="#7" data-toggle="tab">Attendance</a>
									</li>
								</ul>

								<div class="tab-content">
									<%-- basic Information --%>

									<div class="tab-pane active" id="1">
										<div class="row">
											<div class="col-lg-4 col-md-6 col-sm-12">
												<div class="row">
													<asp:UpdatePanel ID="UpdatePanel11" runat="server">
														<ContentTemplate>
															<div class="col-md-6 col-sm-6">
																<div class="form-group">
																	<h4>Employee Code 
                                                <span style="color: red !important;">*</span>
																		<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator26" ControlToValidate="txtEmployeeCode" ErrorMessage="Code Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
																</div>
																<input class="form-control" placeholder="Emp No" type="text" id="txtEmployeeCode" runat="server" name="employerNo" />
															</div>
															<div  class="col-md-6 col-sm-6">
																<div class="form-group">
																	<h4>User Code
                                                                      </h4>
																</div>
																<input class="form-control" placeholder="Code" type="number" id="txtWDID" runat="server" name="employerNo" />
															</div>
															
														</ContentTemplate>
													</asp:UpdatePanel>
													
													<asp:UpdatePanel ID="UpdatePanel13" runat="server">
														<ContentTemplate>
															<div class="col-lg-6 col-sm-6 form-group">
																<h4>First Name
                                            <span style="color: red !important;">*</span>
																</h4>
																<input class="form-control" placeholder="First Name" type="text" id="txtFirstName" runat="server" name="firstName" />
															</div>
															<div class="col-lg-6 col-sm-6 form-group">
																<h4>Middle Name</h4>
																<input class="form-control" placeholder="Middle Name" type="text" id="txtMiddleName" runat="server" name="middleName" />
															</div>
															<div class="col-lg-6 col-sm-6 form-group">
																<h4>Last Name
                                                                <span style="color: red !important;">*</span>
																</h4>
																<input class="form-control" placeholder="Last Name" type="text" id="txtLastName" value="" runat="server" name="lastName" />
															</div>
															<div class="col-md-6 col-sm-6 form-group">
																<h4>Father Name</h4>
																<input class="form-control" placeholder="Father Name" type="text" name="FatherName" id="txtFatherName" runat="server" />
															</div>
															<div class="col-md-6 col-sm-6  form-group">
																<h4>Gender
                                        <span style="color: red !important;">*</span>
																	<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator20" ControlToValidate="ddlGender" InitialValue="0" ErrorMessage="Gender Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
																<asp:DropDownList runat="server" Style="width: 100%;" CssClass="form-control select2" ID="ddlGender" data-plugin="select2">
																	<asp:ListItem Value="0">Please Select</asp:ListItem>
																	<asp:ListItem Value="Male">Male</asp:ListItem>
																	<asp:ListItem Value="Female">Female</asp:ListItem>
																	<asp:ListItem Value="Other">Other</asp:ListItem>
																</asp:DropDownList>
															</div>
															<div class="col-md-6 col-sm-6">
																<div class="form-group">
																	<h4>National Identity
                                                <span style="color: red !important;">*</span>
																		<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtCNIC" ErrorMessage="CNIC Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
																</div>
																<input class="form-control cnic-val" placeholder="xxxxx-xxxxxxx-x" maxlength="15" type="text" id="txtCNIC" runat="server" />
															</div>

															<div class="col-md-6 col-sm-6">
														<div class="form-group">
															<h4>Date of Birth
                                             <span style="color: red !important;">*</span>
																<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator16" ControlToValidate="txtDOB" ErrorMessage="Date of Birth Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
															<input type="text" class="form-control datetime-picker" data-date-format="DD-MMM-YYYY" name="dateOfbirth" id="txtDOB" runat="server" />

														</div>
													</div>
															<div class="col-md-6 col-sm-6 form-group">
																<h4>Contact No
                                            <span style="color: red !important;">*</span>
																	<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" ControlToValidate="txtContactNo" ErrorMessage="Contact No Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
																<input class="form-control phone-no-val" placeholder="Contact No" maxlength="14" type="text" name="contactNo" id="txtContactNo" runat="server" />
															</div>
														</ContentTemplate>
													</asp:UpdatePanel>
												</div>
											</div>

											<asp:UpdatePanel ID="UpdatePanel15" runat="server">
												<ContentTemplate>
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="row">

															<div class="col-md-6 col-sm-6 form-group">
																<h4>Email address
                                                <span style="color: red !important;">*</span> <span style="color: red !important;" visible="false" runat="server" id="spnemail">Email Already Exists</span>
																	<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator14" ControlToValidate="txtEmail" ErrorMessage="Email Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
																<asp:TextBox class="form-control" placeholder="Email address" name="emailAddress" AutoPostBack="true" OnTextChanged="txtEmail_ServerChange" ID="txtEmail" runat="server"></asp:TextBox>
																<asp:Label runat="server" ID="lblEmailError" ForeColor="Red" />
															</div>

															<div class="col-md-6 col-sm-6  form-group">
																<h4>Personal Email address</h4>
																<input class="form-control" placeholder="Secondary Email address" type="text" name="SecemailAddress" id="txtSecEmail" runat="server" />
															</div>
															<div class="col-md-6 col-sm-6  form-group">
																<h4>Country</h4><asp:DropDownList Style="width: 100%;" ID="ddlCountries" runat="server" class="form-control select2" data-plugin="select2" OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
															</div>
															<div   class="col-md-6 col-sm-6  form-group">
																<h4>State  </h4><asp:DropDownList Style="width: 100%;" ID="ddlStates" runat="server" class="form-control select2" data-plugin="select2" OnSelectedIndexChanged="ddlStates_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
															</div>
															<div class="col-md-6 col-sm-6  form-group">
																<h4>City  </h4><asp:DropDownList Style="width: 100%;" ID="ddlCities" runat="server" class="form-control select2" data-plugin="select2"></asp:DropDownList>
															</div>
															<div  style="display:none;"  class="col-md-6 col-sm-6  form-group">
																<h4>Zipcode</h4>
																<input class="form-control" placeholder="Zipcode" type="number" name="zipCode" id="txtZIPCode" runat="server" />
															</div>
															<div class="col-md-12 col-sm-6 form-group">
																<h4>Address Line 1  <span style="color: red !important;">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtAddressLine1" ErrorMessage="Address Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
																<input class="form-control" placeholder="Address Line 1" type="text" name="addressLine1" id="txtAddressLine1" runat="server" />
															</div>
															<div class="col-md-12 col-sm-6 form-group">
																<h4>Address Line 2</h4>
																<input class="form-control" placeholder="Address Line 2" type="text" id="txtAddressLine2" runat="server" />
															</div>

														</div>
													</div>
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="row">
														</div>
													</div>
												</ContentTemplate>
											</asp:UpdatePanel>
										</div>

										<asp:UpdatePanel ID="UpdatePanel12" runat="server">
											<ContentTemplate>
												<div class="content-header second_heading">
													<div class="container-fluid">
														<div class="row mb-2">
															<div class="col-sm-6">
																<h1 class="m-0 text-dark">Emergency Contact</h1>
															</div>
															<!-- /.col -->
														</div>
														<!-- /.row -->
													</div>
													<!-- /.container-fluid -->
												</div>
												<div class="row">
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="row">
															<div class="col-md-6 col-sm-6 form-group">
																<h4>Person Name</h4>
																<input class="form-control" placeholder="Person Name" type="text" name="personName" id="txtEmergencyPerson" runat="server" />
															</div>
															<div class="col-md-6 col-sm-6 form-group">
																<h4>Phone number</h4>
																<input class="form-control phone-no-val" placeholder="Phone number" maxlength="14" type="text" name="phoneNumber" id="txtEmergencyPhone" runat="server" />
															</div>
														</div>
													</div>
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="row">
														</div>
													</div>
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="row">
														</div>
													</div>
												</div>
												<div class="clearfix">&nbsp;</div>
												<div class="clearfix">&nbsp;</div>

											</ContentTemplate>
										</asp:UpdatePanel>

									</div>

									<%--Qualifictaion--%>

									<div class="tab-pane employe-tables detail" id="2">
										<asp:UpdatePanel ID="UpdatePanel1" runat="server">
											<ContentTemplate>

												<div class="content-header second_heading">
													<div class="container-fluid">
														<div class="row mb-2">
															<div class="col-sm-6">
																<h1 class="m-0 text-dark">Experience</h1>
															</div>
															<!-- /.col -->
														</div>
														<!-- /.row -->
													</div>
													<!-- /.container-fluid -->
												</div>

												<asp:GridView ID="gvExperience" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvExperience_RowDataBound" OnRowUpdating="gvExperience_RowUpdating" OnRowCancelingEdit="gvExperience_RowCancelingEdit" OnRowEditing="gvExperience_RowEditing">
													<Columns>
														<asp:TemplateField HeaderText="Sr. No" ItemStyle-Width="60px">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Label ID="txtSrNo" runat="server"></asp:Label>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Title">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditTitle" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Organization">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblOrganization" Text='<%# Eval("Organization") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditOrganization" CssClass="form-control" Text='<%# Eval("Organization") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtOrganization" CssClass="form-control" Text='<%# Eval("Organization") %>'></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Start Date">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblStartDate" Text='<%# Eval("StartYear") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditStartDate" data-date-format="DD-MMM-YYYY" CssClass="form-control datetime-picker" Text='<%# Eval("StartYear") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtStartDate" data-date-format="DD-MMM-YYYY" CssClass="form-control datetime-picker" Text='<%# Eval("StartYear") %>'></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="End Date">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblLeavingDate" Text='<%# Eval("EndYear") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditLeavingDate" data-date-format="DD-MMM-YYYY" CssClass="form-control datetime-picker" Text='<%# Eval("EndYear") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtLeavingDate" data-date-format="DD-MMM-YYYY" CssClass="form-control datetime-picker" Text='<%# Eval("EndYear") %>'></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>

														<asp:TemplateField HeaderText="Current Working Here">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblCurrentDate" Text='<%# Eval("EndMonth") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<div class="checkbox checkbox-primary">
																	<asp:CheckBox runat="server" ID="txtEditCurrentDate" AutoPostBack="true" OnCheckedChanged="txtEditCurrentDate_CheckedChanged" Checked='<%# Eval("EndMonth").ToString()=="Yes"? true:false %>' />
																	<label for="txtEditCurrentDate"></label>
																</div>
															</EditItemTemplate>
															<FooterTemplate>
																<div class="checkbox checkbox-primary">
																	<asp:CheckBox runat="server" ID="txtCurrentDate" AutoPostBack="true" OnCheckedChanged="txtCurrentDate_CheckedChanged" />
																	<label for="txtCurrentDate"></label>
																</div>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Remarks">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblRemarks" Text='<%# Eval("Remarks") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditRemarks" CssClass="form-control" Text='<%# Eval("Remarks") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtRemarks" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField ShowHeader="false">
															<EditItemTemplate>
																<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
																&nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
															</EditItemTemplate>
															<ItemTemplate>
																<asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Button ID="btnAddExperiance" runat="server" CssClass="AD_stock form-control" Text="Add" ValidationGroup="btnValidateE" OnClick="btnAddExperience_Click"></asp:Button>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField>
															<ItemTemplate>
																<asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkExperienceRemove_Command"></asp:LinkButton>
															</ItemTemplate>
														</asp:TemplateField>
													</Columns>
												</asp:GridView>

												<div class="content-header second_heading">
													<div class="container-fluid">
														<div class="row mb-2">
															<div class="col-sm-6">
																<h1 class="m-0 text-dark">Qualification</h1>
															</div>
															<!-- /.col -->
														</div>
														<!-- /.row -->
													</div>
													<!-- /.container-fluid -->
												</div>

												<asp:GridView ID="gvQualification" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvQualification_RowDataBound" OnRowUpdating="gvQualification_RowUpdating" OnRowCancelingEdit="gvQualification_RowCancelingEdit" OnRowEditing="gvQualification_RowEditing">
													<Columns>
														<asp:TemplateField HeaderText="Sr. No" ItemStyle-Width="60px">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Label ID="txtSrNo" runat="server"></asp:Label>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="PEC #">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblPecNo" Text='<%# Eval("PecNo") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditPecNo" CssClass="form-control" Text='<%# Eval("PecNo") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtPecNo" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="PEC Expiry">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblPecExp" Text='<%# Eval("PecExp") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditPecExp" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" CssClass="form-control" Text='<%# Eval("PecExp") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtPecExp" data-date-format="DD-MMM-YYYY" CssClass="form-control datetime-picker"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Degree Level">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblDegree" Text='<%# Eval("Degree") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:DropDownList ID="ddlEditDegree" runat="server" CssClass="form-control select2" data-plugin="select2">
																	<asp:ListItem Value="0">-- Select Qualilfication--</asp:ListItem>
																	<asp:ListItem Value="Below Matriculation">Below Matriculation</asp:ListItem>
																	<asp:ListItem Value="Diploma">Diploma</asp:ListItem>
																	<asp:ListItem Value="Matriculation / HSc.">Matriculation / HSc.</asp:ListItem>
																	<asp:ListItem Value="Intermmidiate / FSc.">Intermmidiate / FSc.</asp:ListItem>
																	<asp:ListItem Value="Graduation">Graduation</asp:ListItem>
																	<asp:ListItem Value="Masters">Masters</asp:ListItem>
																	<asp:ListItem Value="PhD">PhD</asp:ListItem>
																</asp:DropDownList>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:DropDownList ID="ddlDegree" runat="server" CssClass="form-control select2" data-plugin="select2">
																	<asp:ListItem Value="0">-- Select Qualilfication--</asp:ListItem>
																	<asp:ListItem Value="Below Matriculation">Below Matriculation</asp:ListItem>
																	<asp:ListItem Value="Diploma">Diploma</asp:ListItem>
																	<asp:ListItem Value="Matriculation / HSc.">Matriculation / HSc.</asp:ListItem>
																	<asp:ListItem Value="Intermmidiate / FSc.">Intermmidiate / FSc.</asp:ListItem>
																	<asp:ListItem Value="Graduation">Graduation</asp:ListItem>
																	<asp:ListItem Value="Masters">Masters</asp:ListItem>
																	<asp:ListItem Value="PhD">PhD</asp:ListItem>
																</asp:DropDownList>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Title">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Month") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditTitle" CssClass="form-control" Text='<%# Eval("Month") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtTitle" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Date">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblLeavingDate" Text='<%# Eval("Year") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditDate" data-date-format="DD-MMM-YYYY" CssClass="form-control datetime-picker" Text='<%# Eval("Year") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtDate" data-date-format="DD-MMM-YYYY" CssClass="form-control datetime-picker" Text='<%# Eval("Year") %>'></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Grade">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblGrade" Text='<%# Eval("Grade") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditGrade" CssClass="form-control" Text='<%# Eval("Grade") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtGrade" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>

														<asp:TemplateField HeaderText="Institution">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblInstitution" Text='<%# Eval("Institution") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditInstitution" CssClass="form-control" Text='<%# Eval("Institution") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtInstitution" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="City">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblCity" Text='<%# Eval("City") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditCity" CssClass="form-control" Text='<%# Eval("City") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtCity" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Remarks">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblRemarks" Text='<%# Eval("Remarks") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditRemarks" CssClass="form-control" Text='<%# Eval("Remarks") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtRemarks" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField ShowHeader="false">
															<EditItemTemplate>
																<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
																&nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
															</EditItemTemplate>
															<ItemTemplate>
																<asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Button ID="btnAddQ" runat="server" CssClass=" AD_stock form-control" ValidationGroup="btnValidateQ" Text="Add" OnClick="btnAddQualification_Click"></asp:Button>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField>
															<ItemTemplate>
																<asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkQualificationRemove_Command"></asp:LinkButton>
															</ItemTemplate>
														</asp:TemplateField>
													</Columns>
												</asp:GridView>

												<div class="content-header second_heading">
													<div class="container-fluid">
														<div class="row mb-2">
															<div class="col-sm-6">
																<h1 class="m-0 text-dark">Certification</h1>
															</div>
															<!-- /.col -->
														</div>
														<!-- /.row -->
													</div>
													<!-- /.container-fluid -->
												</div>

												<asp:GridView ID="gvCertification" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvCertification_RowDataBound" OnRowUpdating="gvCertification_RowUpdating" OnRowCancelingEdit="gvCertification_RowCancelingEdit" OnRowEditing="gvCertification_RowEditing">
													<Columns>
														<asp:TemplateField HeaderText="Sr. No" ItemStyle-Width="60px">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Label ID="txtSrNo" runat="server"></asp:Label>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Title">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditTilte" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtTitle" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Date">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblLeavingDate" Text='<%# Eval("Year") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditDate" data-date-format="DD-MMM-YYYY" CssClass="form-control datetime-picker" Text='<%# Eval("Year") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtDate" data-date-format="DD-MMM-YYYY" CssClass="form-control datetime-picker" Text='<%# Eval("Year") %>'></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Score">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblScore" Text='<%# Eval("Score") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditScore" CssClass="form-control" Text='<%# Eval("Score") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtScore" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Institution">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblInstitution" Text='<%# Eval("Institution") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditInstitution" CssClass="form-control" Text='<%# Eval("Institution") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtInstitution" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="City">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblCity" Text='<%# Eval("Month") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditCity" CssClass="form-control" Text='<%# Eval("Month") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtCity" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Remarks">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblRemarks" Text='<%# Eval("Remarks") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditRemarks" CssClass="form-control" Text='<%# Eval("Remarks") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtRemarks" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField ShowHeader="false">
															<EditItemTemplate>
																<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
																&nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
															</EditItemTemplate>
															<ItemTemplate>
																<asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Button ID="btnAddC" runat="server" CssClass="AD_stock form-control" Text="Add" ValidationGroup="btnValidateC" OnClick="btnAddCertification_Click"></asp:Button>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField>
															<ItemTemplate>
																<asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkCertificationRemove_Command"></asp:LinkButton>
															</ItemTemplate>
														</asp:TemplateField>
													</Columns>
												</asp:GridView>

												<div class="content-header second_heading">
													<div class="container-fluid">
														<div class="row mb-2">
															<div class="col-sm-6">
																<h1 class="m-0 text-dark">Skill Set</h1>
															</div>
															<!-- /.col -->
														</div>
														<!-- /.row -->
													</div>
													<!-- /.container-fluid -->
												</div>

												<asp:GridView ID="gvSkillSet" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvSkillSet_RowDataBound" OnRowUpdating="gvSkill_RowUpdating" OnRowCancelingEdit="gvSkillSet_RowCancelingEdit" OnRowEditing="gvSkillSet_RowEditing">
													<Columns>
														<asp:TemplateField HeaderText="Sr. No">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Label ID="txtSrNo" runat="server"></asp:Label>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Skill">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblSkill" Text='<%# Eval("Skill") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditSkill" CssClass="form-control" Text='<%# Eval("Skill") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtSkill" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Level">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblLevel" Text='<%# Eval("Level") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:DropDownList ID="ddlEditLevel" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single" SelectedValue='<%# Bind("Level") %>'>
																	<asp:ListItem Value="Beginner">Beginner</asp:ListItem>
																	<asp:ListItem Value="Intermediate">Intermediate</asp:ListItem>
																	<asp:ListItem Value="Expert">Expert</asp:ListItem>
																</asp:DropDownList>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:DropDownList ID="ddlLevel" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single">
																	<asp:ListItem Value="Beginner">Beginner</asp:ListItem>
																	<asp:ListItem Value="Intermediate">Intermediate</asp:ListItem>
																	<asp:ListItem Value="Expert">Expert</asp:ListItem>
																</asp:DropDownList>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField ShowHeader="false">
															<EditItemTemplate>
																<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
																&nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
															</EditItemTemplate>
															<ItemTemplate>
																<asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Button ID="btnAddSS" runat="server" CssClass=" AD_stock form-control" ValidationGroup="btnValidateSS" Text="Add" OnClick="btnAddSkillSet_Click"></asp:Button>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField>
															<ItemTemplate>
																<asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkSkillSetRemove_Command"></asp:LinkButton>
															</ItemTemplate>
														</asp:TemplateField>
													</Columns>
												</asp:GridView>

											</ContentTemplate>
										</asp:UpdatePanel>
									</div>

									<%-- Employee Detail --%>

									<div class="tab-pane detail" id="3">

										<div class="row">
											<div class="col-lg-4 col-md-6 col-sm-12">
												<div class="row">
													<asp:UpdatePanel ID="UpdatePanel2" runat="server">
														<ContentTemplate>
															<div class="col-sm-12 col-xs-12 form-group">
																<h4></h4>
															</div>
															<div class="col-md-6 col-sm-6 form-group">
																<div class="form-group">
																	<h4 for="select2-demo-1" class="control-label">Employment Type
                                                    <span style="color: red !important;">*</span>
																		<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator21" ControlToValidate="ddlEmploymentType" InitialValue="0" ErrorMessage="Employment Type Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
																</div>
																<asp:DropDownList ID="ddlEmploymentType" runat="server" class="form-control select2" data-plugin="select2">
																	<asp:ListItem Value="0">--- Please Select ---</asp:ListItem>
																	<asp:ListItem Value="On Probation">Probation</asp:ListItem>
																	<asp:ListItem Value="Regular">Permanent</asp:ListItem>
																	<asp:ListItem Value="Fixed Term">Contractor</asp:ListItem>
																	<asp:ListItem Value="Adhoc">Adhoc</asp:ListItem>
																</asp:DropDownList>
															</div>
														</ContentTemplate>
													</asp:UpdatePanel>
													<asp:UpdatePanel ID="UpdatePanel16" runat="server">
														<ContentTemplate>
															<div class="col-md-6 col-sm-6 form-group D_NONE">
																<h4 class="control-label">Employee COA
                                                    <span style="color: red !important;">*</span>
																	<asp:Label CssClass="lbl-COA-Balance" Text="Balance: 0" ID="lblEmployeeCOABal" runat="server" />
																</h4>
																<asp:DropDownList ClientIDMode="Static" data-plugin="select2" ID="ddlEmployeeCOA" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployeeCOA_SelectedIndexChanged">
																</asp:DropDownList>
															</div>
														</ContentTemplate>
													</asp:UpdatePanel>
													<div class="col-md-6 col-sm-6">
														<div class="form-group">
															<h4>Date of Joining
                                                <span style="color: red !important;">*</span>
																<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator23" ControlToValidate="txtDOJ" ErrorMessage="Date of Joining Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
															<input type="text" data-date-format="DD-MMM-YYYY" class="form-control datetime-picker" name="dateOfJoining" id="txtDOJ" runat="server" />
														</div>
													</div>
													<asp:UpdatePanel ID="UpdatePanel7" runat="server">
														<ContentTemplate>
															<div class="col-md-6 col-sm-6 form-group">
																<h4 for="select2-demo-5" class="control-label">Job Description
																	<span style="color: red !important;">*</span>
																	<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator22" ControlToValidate="ddlJD" InitialValue="0" ErrorMessage="Job Description Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
																<asp:DropDownList ID="ddlJD" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlJD_SelectedIndexChanged">
																</asp:DropDownList>
															</div>
															<div class="col-md-6 col-sm-6 form-group">
																<h4>Designation <span style="color: red !important;">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDesg" InitialValue="0" ErrorMessage="Designation Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
																<asp:DropDownList ID="ddlDesg" runat="server" CssClass="form-control select2" data-plugin="select2">
																</asp:DropDownList>
															</div>

															<div class="col-md-6 col-sm-6 form-group">
																<h4>Cost Center</h4>
																<asp:DropDownList ID="ddlCostCenter" runat="server" CssClass="form-control select2" data-plugin="select2">
																</asp:DropDownList>
															</div>

															<div class="col-md-6 col-sm-6 form-group">
																<h4>Department <span style="color: red !important;">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlDept" InitialValue="0" ErrorMessage="Department Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
																<asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged" data-plugin="select2" ClientIDMode="Static" AutoPostBack="false"></asp:DropDownList>
															</div>

															<!-- END column -->
															<div class="col-sm-12">
																<div class="row">
																	<div class="col-md-6 col-sm-6 form-group">
																		<h4>Division</h4>
																		<input type="text" class="form-control" name="Division" id="txtDivision" placeholder="Division" runat="server" />
																	</div>

																	<div class="col-md-6 col-sm-6 form-group">
																		<h4>Place of posting</h4>
																		<input type="text" class="form-control" name="Place of posting" id="txtPlaceOfPosting" placeholder="Place of posting" runat="server" />
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-6 col-sm-6 form-group">
																		<h4>Team</h4>
																		<input type="text" class="form-control" name="Team" id="txtTeam" placeholder="Team" runat="server" />
																	</div>
																	<div class="col-md-6 col-sm-6 form-group">
																<h4>Overtime Group</h4>
																<asp:DropDownList ID="overtimegroup" runat="server" CssClass="form-control select2" data-plugin="select2">
																</asp:DropDownList>
															</div>


																</div>
																<div class="clearfix">&nbsp;</div>
															</div>
														</ContentTemplate>
													</asp:UpdatePanel>
												</div>
											</div>
											<asp:UpdatePanel ID="UpdatePanel9" runat="server">
												<ContentTemplate>
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="row">
															<div class="col-sm-12 col-xs-12 form-group">
																<h4><strong>Direct Reporting To</strong></h4>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="form-group">
																	<h4>Person</h4>
																</div>
																<asp:DropDownList ID="ddlDirectReportingPerson" runat="server" ClientIDMode="Static" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
															</div>
														</div>
														<div class="row">
															<div class="col-sm-12 col-xs-12 form-group">
																<br />
																<h4><strong>Indirect Reporting To</strong></h4>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="form-group">
																	<h4 style="margin-top: 21px;">Person</h4>
																</div>
																<asp:DropDownList ID="ddlInDirectReportingPerson" runat="server" ClientIDMode="Static" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
															</div>
														</div>
													</div>

												</ContentTemplate>
											</asp:UpdatePanel>
											<div class="col-lg-4 col-md-6 col-sm-12">
												<div class="row">
													<div class="col-md-12 col-sm-6">
														<div class="form-group">
															<div class="uploadProfileBox">
																<img src="/assets/images/noImagePerson.png" id="imgPhoto" runat="server" class="uploadimg" onclick="document.getElementById('updPhoto').click();" />
																<asp:FileUpload ID="updPhoto" runat="server" ClientIDMode="Static" Style="display: none;" accept="image/*"></asp:FileUpload>
																<span><i class="fa fa-pencil" aria-hidden="true" onclick="document.getElementById('updPhoto').click();"></i></span>
															</div>
															<label id="lblerror" class="uploadimgLabel">Kindly Update Picture 35 x 35 pixels and less then 200KB</label>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="content-header second_heading">
											<div class="container-fluid">
												<div class="row mb-2">
													<div class="col-sm-6">
														<h1 class="m-0 text-dark">Document</h1>
													</div>
													<!-- /.col -->

												</div>
												<!-- /.row -->
											</div>
											<!-- /.container-fluid -->
										</div>

										<div class="clearfix">&nbsp;</div>

										<div class="row">
											<div class="col-lg-4 col-md-6 col-sm-12">
												<div class="row">
													<div class="col-md-12 col-sm-6 form-group">
														<h4>File Title </h4>
														<asp:DropDownList ID="ddlDocType" runat="server" data-plugin="select2" CssClass="form-control select2 js-example-basic-single">
															<asp:ListItem Value="None">--- Please Select ---</asp:ListItem>
															<asp:ListItem Value="Resume">Resume</asp:ListItem>
															<asp:ListItem Value="Education Certificate">Education Certificate</asp:ListItem>
															<asp:ListItem Value="Experience Letter">Experience Letter</asp:ListItem>
															<asp:ListItem Value="CNIC">CNIC</asp:ListItem>
															<asp:ListItem Value="Passport">Passport</asp:ListItem>
															<asp:ListItem Value="Domicile">Domicile</asp:ListItem>
															<asp:ListItem Value="Others">Others</asp:ListItem>
														</asp:DropDownList>
													</div>

													<div class="col-md-12 col-sm-12">
														<div class="form-group">
															<h4>Attachments</h4>
														</div>
														<div class="file-upload">
															<div class="image-upload-wrap">
																<asp:FileUpload CssClass="file-upload-input" ID="updAttachments" runat="server" AllowMultiple="true" ClientIDMode="Static" onchange="readUrlMultiple2(this);" accept="image/*"></asp:FileUpload>
																<div class="drag-text">
																	<h3>Drag and drop a file or select add Image (MaxSize: 2mb)</h3>
																</div>
															</div>
															<div class="file-upload-content">
																<img class="file-upload-image" src="#" alt="Supplier image" />
																<div class="image-title-wrap">
																	<button type="button" onclick="removeUploadMultiple2()" class="remove-image">
																		Remove  <span><i class="fa fa-trash" aria-hidden="true"></i></span>
																	</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="row">
													<div class="col-lg-12">
														<div class="card-body table-responsive tbl-cont ">
															<asp:GridView ID="gvFiles" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false">
																<Columns>
																	<asp:TemplateField HeaderText="Sr. No">
																		<ItemTemplate>
																			<asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
																		</ItemTemplate>
																	</asp:TemplateField>

																	<asp:TemplateField HeaderText="Title">
																		<ItemTemplate>
																			<asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
																		</ItemTemplate>
																	</asp:TemplateField>
																	<asp:TemplateField HeaderText="Download">
																		<ItemTemplate>
																			<a runat="server" target="_blank" class="AD_btn two" id="lblDownload" href='<%# Eval("FilePath") %>'>Download</a>
																		</ItemTemplate>
																	</asp:TemplateField>
																</Columns>
															</asp:GridView>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="clearfix">&nbsp;</div>
										<div class="clearfix">&nbsp;</div>

										<asp:UpdatePanel ID="UpdatePanel8" runat="server">
											<ContentTemplate>
												<div class="content-header second_heading D_NONE">
													<div class="container-fluid">
														<div class="row mb-2">
															<div class="col-sm-6">
																<h1 class="m-0 text-dark">Accountabilities and Responsibilities</h1>
															</div>
															<!-- /.col -->

														</div>
														<!-- /.row -->
													</div>
													<!-- /.container-fluid -->
												</div>
												<div class="row D_NONE">
													<div class="col-lg-12">
														<asp:GridView ID="gv" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gv_RowDataBound" OnRowUpdating="gv_RowUpdating" OnRowCancelingEdit="gv_RowCancelingEdit" OnRowEditing="gv_RowEditing">
															<Columns>
																<asp:TemplateField HeaderText="Sr. No">
																	<ItemTemplate>
																		<asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
																	</ItemTemplate>
																	<FooterTemplate>
																		<asp:Label ID="txtSrNo" runat="server"></asp:Label>
																	</FooterTemplate>
																</asp:TemplateField>
																<asp:TemplateField HeaderText="Type <span style='color:red !important;'>*</span>">
																	<ItemTemplate>
																		<asp:Label runat="server" ID="lblKPIType" Text='<%# Eval("KPIType") %>'></asp:Label>
																	</ItemTemplate>
																	<EditItemTemplate>
																		<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator75" ControlToValidate="ddlEditKPIType" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidateuar" ForeColor="Red">*</asp:RequiredFieldValidator>
																		<asp:DropDownList ID="ddlEditKPIType" runat="server" data-plugin="select2" CssClass=" select2 form-control js-example-basic-single" SelectedValue='<%# Bind("KPIType") %>'>
																			<asp:ListItem Value="Accountability">Accountability</asp:ListItem>
																			<asp:ListItem Value="Responsibility">Responsibility</asp:ListItem>
																		</asp:DropDownList>
																	</EditItemTemplate>
																	<FooterTemplate>
																		<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator77" ControlToValidate="ddlKPIType" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidateaar" ForeColor="Red">*</asp:RequiredFieldValidator>
																		<asp:DropDownList ID="ddlKPIType" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single">
																			<asp:ListItem Value="Accountability">Accountability</asp:ListItem>
																			<asp:ListItem Value="Responsibility">Responsibility</asp:ListItem>
																		</asp:DropDownList>
																	</FooterTemplate>
																</asp:TemplateField>
																<asp:TemplateField HeaderText="Description <span style='color:red !important;'>*</span>">
																	<ItemTemplate>
																		<asp:Label runat="server" ID="lblDesc" Text='<%# Eval("KPIDesc") %>'></asp:Label>
																	</ItemTemplate>
																	<EditItemTemplate>
																		<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator76" ControlToValidate="txtEditDesc" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidateuar" ForeColor="Red">*</asp:RequiredFieldValidator>
																		<asp:TextBox runat="server" ID="txtEditDesc" CssClass="form-control" Text='<%# Eval("KPIDesc") %>'></asp:TextBox>
																	</EditItemTemplate>
																	<FooterTemplate>
																		<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator78" ControlToValidate="txtDesc" ErrorMessage="Required" Display="Dynamic" ValidationGroup="btnValidateaar" ForeColor="Red">*</asp:RequiredFieldValidator>
																		<asp:TextBox runat="server" ID="txtDesc" CssClass="form-control"></asp:TextBox>
																	</FooterTemplate>
																</asp:TemplateField>
																<asp:TemplateField ShowHeader="false">
																	<EditItemTemplate>
																		<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" ValidationGroup="btnValidateuar" Text="Update"></asp:LinkButton>
																		&nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
																	</EditItemTemplate>
																	<ItemTemplate>
																		<asp:LinkButton ID="lnkEdit" CssClass="edit-class" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
																	</ItemTemplate>
																	<FooterTemplate>
																		<asp:Button ID="btnAddA" runat="server" CssClass="AD_stock form-control" Text="Add" ValidationGroup="btnValidateaar" OnClick="btnAdd_Click"></asp:Button>
																	</FooterTemplate>
																</asp:TemplateField>
																<asp:TemplateField>
																	<ItemTemplate>
																		<asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemove_Command"></asp:LinkButton>
																	</ItemTemplate>
																</asp:TemplateField>
															</Columns>
														</asp:GridView>
													</div>
												</div>
											</ContentTemplate>
										</asp:UpdatePanel>

										<div class="row D_NONE">
											<div class="col-lg-4 col-md-6 col-sm-12">
												<div class="row">
												</div>
											</div>
											<div class="col-lg-4 col-md-6 col-sm-12">
												<div class="row">
													<h4>File</h4>
													<asp:FileUpload ID="updFiles" runat="server" ClientIDMode="Static" Style="display: none;" accept=".pdf,.doc,.docx,.jpg,.jpeg,.png,.gif,.xlx,.xlxs,.ppt,.pptx,.txt" />
													<button class="AD_btn_inn" data-original-title="Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('updFiles').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>

												</div>
											</div>
											<div class="col-lg-4 col-md-6 col-sm-12">
												<div class="row">
												</div>
											</div>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row D_NONE">
											<div class="col-sm-12">
											</div>
										</div>
									</div>

									<%-- payroll --%>

									<div class="tab-pane" id="4">
										<asp:UpdatePanel ID="UpdatePanel3" runat="server">
											<ContentTemplate>
												<div class="row stock1Pad">
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="row">
															<div class="col-sm-6" style="display: none;">
																<label>
																	PayRoll COA
                                                    <span style="color: red !important;">*</span>
																	<asp:Label CssClass="lbl-COA-Balance" Text="Balance: 0" ID="lblCOABal" runat="server" />
																</label>
																<asp:DropDownList ClientIDMode="Static" data-plugin="select2" ID="ddlCOA" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlCOA_SelectedIndexChanged">
																</asp:DropDownList>
															</div>
															<div class="col-sm-12 D_NONE">
																<label>
																	PayRoll Expense COA
                                                    <span style="color: red !important;">*</span>
																	<asp:Label CssClass="lbl-COA-Balance" Text="Balance: 0" ID="lblExpCOABal" runat="server" />
																</label>
																<asp:DropDownList ClientIDMode="Static" data-plugin="select2" ID="ddlExpCOA" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlExpCOA_SelectedIndexChanged">
																</asp:DropDownList>
															</div>
														</div>
														<div class="row">
															<div class="col-sm-12 col-xs-12 form-group pl-0">
																<h4><strong>Allowances</strong></h4>
															</div>
															<div class="table_out_data">
																<label>
																	Basic Salary
                                                                    <span style="color: red !important;"></span>
																	<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator15" ControlToValidate="txtBasicSalary" ErrorMessage="Basic Salary Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red">*</asp:RequiredFieldValidator></label>
																<input class="form-control" placeholder="Basic Salary" type="number" value="0" id="txtBasicSalary" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
															</div>
															<div class="table_out_data" runat="server" id="divHouseRentAllowance">
																<label>
																	House Rent Allowance</label>
																<input class="form-control" placeholder="House Rent Allowance" type="number" value="0" id="txtHouseRentAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
															</div>
															<div class="table_out_data" runat="server" id="divTax" style="display: none;">
																<label>
																	Tax
                                                   <input class="form-control" placeholder="Tax" type="number" value="0" id="txtTax" runat="server" clientidmode="static" />
															</div>
															<div class="table_out_data" runat="server" id="divMedicalAllowance">
																<label>
																	Medical Allowance</label>
																<input class="form-control" placeholder="Medical Allowance" type="number" value="0" id="txtMedicalAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
															</div>
															<div class="table_out_data" runat="server" id="divUtility">
																<label>
																	Utility Allowance</label>
																<input class="form-control" placeholder="Utility" type="number" id="txtUtility" value="0" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
															</div>
															<div class="table_out_data">
																<label>
																	Maintenance Allowance
																</label>
																<input class="form-control" placeholder="Maintenance Allowance" type="number" value="0" id="txtMaintenanceAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
															</div>
															<div class="table_out_data">
																<label>
																	Food Allowance</label>
																<input class="form-control" placeholder="Food Allowance" type="number" value="0" id="txtFoodAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
															</div>
															<div class="table_out_data">
																<label>
																	Fuel Allowance</label>
																<input class="form-control" placeholder="Fuel Allowance" type="number" value="0" id="txtFuelAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
															</div>

															<div class="table_out_data" runat="server">
																<label>Other Allowances</label>
																<input class="form-control" placeholder="Other / Misc." type="number" id="txtOtherAllowance" readonly="true" value="0" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
															</div>

															<div class="table_out_data" runat="server" id="divGrossSalary">
																<label>Gross Salary  </label>
																<input class="form-control" placeholder="Gross Salary" type="number" value="0" id="txtGrossSalary" runat="server" clientidmode="static" />
															</div>

														</div>
													</div>
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="col-sm-12 col-xs-12 form-group pl-0">
															<h4 style="margin: 19px 0 5px !important;"><strong>Deductions</strong></h4>
														</div>
														<div class="table_out_data" style="display: none;">
															<label>
																E.O.B.I</label>
															<input class="form-control" placeholder="E.O.B.I" type="number" value="0" id="txtEOBIAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
														</div>
														<div class="table_out_data" style="display: none;">
															<label>
																IESSI</label>
															<input class="form-control" placeholder="IESSI" type="number" value="0" id="txtIESSI" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
														</div>

														<div class="table_out_data" style="display: none;">
															<label>
																Other Deductions</label>
															<input class="form-control" placeholder="Other Deduction" type="number" value="0" id="txtOtherDeductions" readonly="true" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
														</div>
														<div class="table_out_data">
															<label>
																Total Deduction</label>
															<input class="form-control" placeholder="Total Deduction" type="number" readonly="true" value="0" id="txtTotalDeduction" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
														</div>
													</div>
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="col-sm-12 col-xs-12 form-group pl-0">
															<h4 style="margin: 19px 0 5px !important;"><strong></strong></h4>
														</div>
														<div class="col-sm-12">
															<div class="table_out_data">
																<div>
																	<input type="checkbox" id="chkIsOverTime" runat="server" name="chkIsOverTime" class="checkk" />
																	<label for="chkIsOverTime" class="checkk_lab">Overtime</label>
																</div>
															</div>
														</div>
													</div>
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="col-sm-12">
															<div class="table_out_data" runat="server" id="divDeductPF">
																<div>
																	<input type="checkbox" id="chkDeductPF" runat="server" name="chkDeductPF" class="checkk" />
																	<label for="chkDeductPF" class="checkk_lab">Deduct Provident Fund</label>
																</div>
															</div>
														</div>
													</div>
													<div class="col-lg-4 col-md-6 col-sm-12" style="display: none">
														<div class="table_out_data">
															<label>
																Over Time Rate
															</label>
															<input class="form-control" placeholder="0" runat="server" type="number" id="txtOverTimeRate" value="0" />
															<p><span style="font-weight: bold;">Please Note:</span> This Rate is calculated based on OTR * No. of Over Time Hours</p>

														</div>
													</div>
												</div>
												<div class="clearfix">&nbsp;</div>
												<div class="row stock1Pad">
													<div class="col-md-6 col-sm-12 col-lg-4" style="padding-right: 0px;">
														<div class="table_out_data" runat="server" id="divNetSalary" style="padding-left: 0px;">
															<label>
																Net Salary</label>
															<input class="form-control" placeholder="Net Salary" readonly="true" type="number" id="txtNetSalary" value="0" runat="server" clientidmode="static" />
														</div>
													</div>
												</div>
												<div class="row stock1Pad">
													<div class="col-sm-6" runat="server" id="divMobileAllowance">
														<label>
															Mobile Allowance</label>
														<input class="form-control" placeholder="Mobile Allowance" type="number" value="0" id="txtMobileAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
													</div>
													<div class="col-sm-6" runat="server" id="divCarAllowance">
														<label>
															Car Allowance</label>
														<input class="form-control" placeholder="Car Allowance" type="number" id="txtCarAllowance" value="0" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
													</div>
													<div class="col-sm-6" runat="server" id="divTravelAllowance">
														<label>
															Travel Allowance</label>
														<input class="form-control" placeholder="Travel Allowance" type="number" value="0" id="txtTravelAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
													</div>
												</div>
												<div class="col-lg-4 col-md-6 col-sm-12">
													<div class="row">
														<div class="col-sm-12" id="divNightFoodAllowance" runat="server" style="display: none;">
															<label>
																Night Food Allowance</label>
															<input class="form-control" placeholder="Night Food Allowance" type="number" value="0" id="txtNightFoodAllowance" runat="server" clientidmode="static" />
														</div>

													</div>
												</div>
												<div class="col-lg-4 col-md-6 col-sm-12">
													<div class="row">
													</div>
												</div>

												<div class="clearfix">&nbsp;</div>
												<div style="display:none;" class="content-header second_heading">
													<div class="container-fluid">
														<div class="row mb-2">
															<div class="col-sm-6">
																<h1 class="m-0 text-dark">Additions</h1>
															</div>
															<!-- /.col -->

														</div>
														<!-- /.row -->
													</div>
													<!-- /.container-fluid -->
												</div>

												<asp:GridView Visible="false" ID="gvAdditions" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvAdditions_RowDataBound" OnRowUpdating="gvAdditions_RowUpdating" OnRowCancelingEdit="gvAdditions_RowCancelingEdit" OnRowEditing="gvAdditions_RowEditing">
													<Columns>
														<asp:TemplateField HeaderText="Sr. No">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Label ID="txtSrNo" runat="server"></asp:Label>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Title">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditTitle" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Type">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblType" Text='<%# Eval("Type") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:DropDownList runat="server" ID="ddlEditType" data-plugin="select2" CssClass="select2 form-control" Text='<%# Eval("Type") %>'>
																	<asp:ListItem>Percentage</asp:ListItem>
																	<asp:ListItem>Amount</asp:ListItem>
																</asp:DropDownList>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:DropDownList runat="server" ID="ddlType" data-plugin="select2" CssClass="select2 form-control">
																	<asp:ListItem>Percentage</asp:ListItem>
																	<asp:ListItem>Amount</asp:ListItem>
																</asp:DropDownList>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Amount">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblAmount" Text='<%# Eval("Amount") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditAmount" CssClass="form-control" Text='<%# Eval("Amount") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtAmount" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField ShowHeader="false" FooterStyle-HorizontalAlign="Center">
															<EditItemTemplate>
																<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
																&nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
															</EditItemTemplate>
															<ItemTemplate>
																<asp:LinkButton ID="lnkEdit" CssClass="edit-class" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Button ID="btnAddAdditions" runat="server" CssClass="AD_stock form-control" Text="Add" OnClick="btnAddAdditions_Click"></asp:Button>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField FooterStyle-HorizontalAlign="Center">
															<ItemTemplate>
																<asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveAdditions_Command"></asp:LinkButton>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Label ID="lblTotal" CssClass="total" runat="server"></asp:Label>
															</FooterTemplate>
														</asp:TemplateField>
													</Columns>
												</asp:GridView>

												<div  class="cleafix">&nbsp;</div>

												<div  style="display:none;" class="content-header second_heading">
													<div class="container-fluid">
														<div class="row mb-2">
															<div class="col-sm-6">
																<h1 class="m-0 text-dark">Deductions</h1>
															</div>
															<!-- /.col -->

														</div>
														<!-- /.row -->
													</div>
													<!-- /.container-fluid -->
												</div>

												<asp:GridView Visible="false" ID="gvDeductions" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvDeductions_RowDataBound" OnRowUpdating="gvDeductions_RowUpdating" OnRowCancelingEdit="gvDeductions_RowCancelingEdit" OnRowEditing="gvDeductions_RowEditing">
													<Columns>
														<asp:TemplateField HeaderText="Sr. No">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Label ID="txtSrNo" runat="server"></asp:Label>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Title">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditTitle" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Type">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblType" Text='<%# Eval("Type") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:DropDownList runat="server" ID="ddlEditType" data-plugin="select2" CssClass="select2 form-control" Text='<%# Eval("Type") %>'>
																	<asp:ListItem>Percentage</asp:ListItem>
																	<asp:ListItem>Amount</asp:ListItem>
																</asp:DropDownList>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:DropDownList runat="server" ID="ddlType" data-plugin="select2" CssClass="select2 form-control">
																	<asp:ListItem>Percentage</asp:ListItem>
																	<asp:ListItem>Amount</asp:ListItem>
																</asp:DropDownList>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="Amount">
															<ItemTemplate>
																<asp:Label runat="server" ID="lblAmount" Text='<%# Eval("Amount") %>'></asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" ID="txtEditAmount" CssClass="form-control" Text='<%# Eval("Amount") %>'></asp:TextBox>
															</EditItemTemplate>
															<FooterTemplate>
																<asp:TextBox runat="server" ID="txtAmount" CssClass="form-control"></asp:TextBox>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField ShowHeader="false" FooterStyle-HorizontalAlign="Center">
															<EditItemTemplate>
																<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
																&nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
															</EditItemTemplate>
															<ItemTemplate>
																<asp:LinkButton ID="lnkEdit" CssClass="edit-class" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Button ID="btnAddAdditions" runat="server" CssClass="AD_stock form-control" Text="Add" OnClick="btnAddDeductions_Click"></asp:Button>
															</FooterTemplate>
														</asp:TemplateField>
														<asp:TemplateField FooterStyle-HorizontalAlign="Center">
															<ItemTemplate>
																<asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveDeductions_Command"></asp:LinkButton>
															</ItemTemplate>
															<FooterTemplate>
																<asp:Label ID="lblTotal" CssClass="total" runat="server"></asp:Label>
															</FooterTemplate>
														</asp:TemplateField>
													</Columns>
												</asp:GridView>
											</ContentTemplate>
										</asp:UpdatePanel>
									</div>

									<%-- bank Detail --%>

									<div class="tab-pane " id="5">
										<asp:UpdatePanel ID="UpdatePanel17" runat="server">
											<ContentTemplate>
												<div class="row">
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<h4>Bank Name</h4>
																	<input type="text" class="form-control" placeholder="Bank Name" name="bankName" id="txtBankName" runat="server" />
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<h4>Branch Name</h4>
																	<input type="text" class="form-control" placeholder="Branch Name" name="branchName" id="txtBranchName" runat="server" />
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<h4>Account No</h4>
																	<input type="number" class="form-control" placeholder="Account No" name="accountNo" id="txtAccountNo" runat="server" />
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<h4>Account Title</h4>
																	<input type="text" class="form-control" placeholder="Account Title" name="accountTitle" id="txtAccountTitle" runat="server" />
																</div>
															</div>

														</div>
													</div>
													<div class="col-lg-8 col-md-6 col-sm-12">
														<div class="row">
														</div>
													</div>
												</div>
											</ContentTemplate>
										</asp:UpdatePanel>
									</div>

									<%-- Attendance --%>

									<div  style="display:none;" class="tab-pane detail " id="7">
										<asp:UpdatePanel ID="UpdatePanel5" runat="server">
											<ContentTemplate>
												<div class="row">
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="row">
															<div class="col-md-6 col-sm-6 form-group">
																<h4>Shift </h4>
																<asp:DropDownList ID="ddlShifts" runat="server" CssClass="form-control select2" data-plugin="select2" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlShifts_SelectedIndexChanged"></asp:DropDownList>
															</div>
															<div class="col-md-6 col-sm-6 form-group">
																<h4>Working Hours</h4>
																<input type="number" disabled="disabled" class="form-control" id="txtHours" runat="server" />
															</div>
															 <div class="col-sm-6 form-group">
												<div class="checkbox checkbox-primary">
                                                    <br />
															<input type="checkbox" id="autoattendance"  runat="server" name="returnedLockerKeys" />
															<label for="autoattendance">Auto Attendance</label>	
											</div>		
								
							</div>	
                                      
															<div class="col-md-12 col-sm-6 form-group">
																<h4>Annual Calendar </h4>
																<asp:DropDownList ID="ddlAnnualCalendar" runat="server" CssClass="form-control select2" data-plugin="select2" ClientIDMode="Static">
																	<asp:ListItem Value="PK"> PK </asp:ListItem>
																	<asp:ListItem Value="US"> US </asp:ListItem>
																	<asp:ListItem Value="Global"> Global </asp:ListItem>
																</asp:DropDownList>
															</div>
															
															<div class="col-md-6 form-group D_NONE">
																<h4>Start Time</h4>
																<input class="form-control" id="txtStartTime" placeholder="Start Date" type="text" data-plugin="timepicker" runat="server" disabled="disabled" />
															</div>
															<div class="col-md-6 form-group D_NONE">
																<h4>End Time</h4>
																<input class="form-control" id="txtEndTime" placeholder="End Date" type="text" data-plugin="timepicker" runat="server" disabled="disabled" />
															</div>
															<div class="col-md-6 form-group D_NONE">
																<h4>Late In Time</h4>
																<input class="form-control" id="txtLateIn" placeholder="Holiday Title" type="text" data-plugin="timepicker" runat="server" disabled="disabled" />
															</div>

															<div class="col-md-6 form-group D_NONE">
																<h4>Half Day Start</h4>
																<input class="form-control" id="txtHalfDay" placeholder="Start Date" type="text" data-plugin="timepicker" runat="server" disabled="disabled" />
															</div>
															<div class="col-md-6 form-group D_NONE">
																<h4>Early Out</h4>
																<input class="form-control" id="txtEarlyOut" placeholder="End Date" type="text" data-plugin="timepicker" runat="server" disabled="disabled" />
															</div>
														</div>
													</div>
													<div class="col-lg-4 col-md-6 col-sm-12">
													</div>
													<div class="col-lg-4 col-md-6 col-sm-12">
														<div class="row">
														</div>
													</div>
												</div>

												<div class="content-header second_heading">
													<div class="container-fluid">
														<div class="row mb-2">
															<div class="col-sm-6">
																<h1 class="m-0 text-dark">Shift Details</h1>
															</div>
															<!-- /.col -->

														</div>
														<!-- /.row -->
													</div>
													<!-- /.container-fluid -->
												</div>

												<div class="row" id="shiftDaysDiv" runat="server">
													<div class="col-md-12">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Day</th>
																	<th>Off Day (OT)</th>
																	<th>Is Special (Customized)</th>
																	<th>Time In Exemption</th>
																	<th>Start Time</th>
																	<th>Late In Time</th>
																	<th>Break Start Time</th>
																	<th>Break End Time</th>
																	<th style="display: none;">Half Day Start</th>
																	<th>Early Out</th>
																	<th>End Time</th>
																	<th>Day<span style="color: red">+1</span> Change</th>
																</tr>
															</thead>

															<tbody>
																<tr>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" checked="checked" id="chkMonday" runat="server" onchange="CheckWeekDayOtSpecial('chkMondayIsSpecial','chkMondayOT')" name="returnedLockerKeys" />
																			<label for="chkMonday">Monday</label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkMondayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkMondayIsSpecial','chkMonday')" />
																			<label for="chkMondayOT"></label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" onchange="CheckWeekDayOtSpecial('chkMondayOT','chkMonday')" id="chkMondayIsSpecial" runat="server" name="IsSpecial" />
																			<label for="chkMondayIsSpecial"></label>
																		</div>
																	</td>
																	<td>
																		<input class="form-control exemption-timein" value="1" id="txtMonTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:00 AM" id="txtMONStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:11 AM" id="txtMONLateInTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:00 PM" id="txtMONBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:30 PM" id="txtMONBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td style="display: none;">
																		<input class="form-control time-picker" value="2:30 PM" id="txtMONHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:29 PM" id="txtMONEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:30 PM" id="txtMONEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<div class="col-sm-2">

																			<div class="checkbox checkbox-primary">
																				<input type="checkbox" id="chkMonNightShift" class="NightShift" runat="server" name="Night Shift" />
																				<label for="chkMonNightShift"></label>
																			</div>
																		</div>

																	</td>

																</tr>
																<tr>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" checked="checked" onchange="CheckWeekDayOtSpecial('chkTuesdayIsSpecial','chkTuesdayOT')" id="chkTuesday" runat="server" name="returnedLockerKeys" />
																			<label for="chkTuesday">Tuesday</label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkTuesdayOT" onchange="CheckWeekDayOtSpecial('chkTuesdayIsSpecial','chkTuesday')" runat="server" name="OT" />
																			<label for="chkTuesdayOT"></label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkTuesdayIsSpecial" runat="server" onchange="CheckWeekDayOtSpecial('chkTuesdayOT','chkTuesday')" name="IsSpecial" />
																			<label for="chkTuesdayIsSpecial"></label>
																		</div>
																	</td>
																	<td>
																		<input class="form-control exemption-timein" value="1" id="txtTueTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:00 AM" id="txtTUEStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:11 AM" id="txtTUELateInTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:00 PM" id="txtTUEBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:30 PM" id="txtTUEBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td style="display: none;">
																		<input class="form-control time-picker" value="2:30 PM" id="txtTUEHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:29 PM" id="txtTUEEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:30 PM" id="txtTUEEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<div class="col-sm-2">

																			<div class="checkbox checkbox-primary">
																				<input type="checkbox" id="chkTUENightShift" class="NightShift" runat="server" name="Night Shift" />
																				<label for="chkTUENightShift"></label>
																			</div>
																		</div>

																	</td>
																</tr>
																<tr>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" checked="checked" id="chkWednesdat" runat="server" onchange="CheckWeekDayOtSpecial('chkWednesdayIsSpecial','chkWednesdayOT')" name="returnedLockerKeys" />
																			<label for="chkWednesdat">Wednesday</label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkWednesdayOT" runat="server" onchange="CheckWeekDayOtSpecial('chkWednesdayIsSpecial','chkWednesdat')" name="OT" />
																			<label for="chkWednesdayOT"></label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkWednesdayIsSpecial" runat="server" name="IsSpecial" onchange="CheckWeekDayOtSpecial('chkWednesdat','chkWednesdayOT')" />
																			<label for="chkWednesdayIsSpecial"></label>
																		</div>
																	</td>
																	<td>
																		<input class="form-control exemption-timein" value="1" id="txtWedTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:00 AM" id="txtWEDStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:11 AM" id="txtWEDLateInTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:00 PM" id="txtWEDBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:30 PM" id="txtWEDBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td style="display: none;">
																		<input class="form-control time-picker" value="2:30 PM" id="txtWEDHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:29 PM" id="txtWEDEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:30 PM" id="txtWEDEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<div class="col-sm-2">

																			<div class="checkbox checkbox-primary">
																				<input type="checkbox" id="chkWEDNightShift" class="NightShift" runat="server" name="Night Shift" />
																				<label for="chkWEDNightShift"></label>
																			</div>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" checked="checked" id="chkThursday" runat="server" onchange="CheckWeekDayOtSpecial('chkThursdayOT','chkThursdayIsSpecial')" name="returnedLockerKeys" />
																			<label for="chkThursday">Thursday</label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkThursdayOT" runat="server" onchange="CheckWeekDayOtSpecial('chkThursdayIsSpecial','chkThursday')" name="OT" />
																			<label for="chkThursdayOT"></label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkThursdayIsSpecial" runat="server" onchange="CheckWeekDayOtSpecial('chkThursdayOT','chkThursday')" name="IsSpecial" />
																			<label for="chkThursdayIsSpecial"></label>
																		</div>
																	</td>
																	<td>
																		<input class="form-control exemption-timein" value="1" id="txtThursTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:00 AM" id="txtTHUStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:11 AM" id="txtTHULateInTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:00 PM" id="txtTHUBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:30 PM" id="txtTHUBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td style="display: none;">
																		<input class="form-control time-picker" value="2:30 PM" id="txtTHUHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:29 PM" id="txtTHUEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:30 PM" id="txtTHUEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<div class="col-sm-2">

																			<div class="checkbox checkbox-primary">
																				<input type="checkbox" id="chkTHUNightShift" class="NightShift" runat="server" name="Night Shift" />
																				<label for="chkTHUNightShift"></label>
																			</div>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" checked="checked" id="chkFriday" runat="server" name="returnedLockerKeys" onchange="CheckWeekDayOtSpecial('chkFridayOT','chkFridayIsSpecial')" />
																			<label for="chkFriday">Friday</label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkFridayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkFridayIsSpecial','chkFriday')" />
																			<label for="chkFridayOT"></label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkFridayIsSpecial" runat="server" name="IsSpecial" onchange="CheckWeekDayOtSpecial('chkFridayOT','chkFriday')" />
																			<label for="chkFridayIsSpecial"></label>
																		</div>
																	</td>
																	<td>
																		<input class="form-control exemption-timein" value="1" id="txtFriTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:00 AM" id="txtFRIStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:11 AM" id="txtFRILateInTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:00 PM" id="txtFRIBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="2:30 PM" id="txtFRIBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td style="display: none;">
																		<input class="form-control time-picker" value="3:00 PM" id="txtFRIHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="6:29 PM" id="txtFRIEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="6:30 PM" id="txtFRIEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<div class="col-sm-2">

																			<div class="checkbox checkbox-primary">
																				<input type="checkbox" id="chkFRINightShift" class="NightShift" runat="server" name="Night Shift" />
																				<label for="chkFRINightShift"></label>
																			</div>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkSaturday" runat="server" name="returnedLockerKeys" onchange="CheckWeekDayOtSpecial('chkSaturdayOT','chkSaturdayIsSpecial')" />
																			<label for="chkSaturday">Saturday</label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkSaturdayOT" onchange="CheckWeekDayOtSpecial('chkSaturdayIsSpecial','chkSaturday')" runat="server" name="OT" />
																			<label for="chkSaturdayOT"></label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkSaturdayIsSpecial" onchange="CheckWeekDayOtSpecial('chkSaturdayOT','chkSaturday')" runat="server" name="IsSpecial" />
																			<label for="chkSaturdayIsSpecial"></label>
																		</div>
																	</td>
																	<td>
																		<input class="form-control exemption-timein" value="1" id="txtSatTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:00 AM" id="txtSATStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:11 AM" id="txtSATLateInTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:00 PM" id="txtSATBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:30 PM" id="txtSATBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td style="display: none;">
																		<input class="form-control time-picker" value="2:30 PM" id="txtSATHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:29 PM" id="txtSATEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:30 PM" id="txtSATEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<div class="col-sm-2">

																			<div class="checkbox checkbox-primary">
																				<input type="checkbox" id="chkSATNightShift" class="NightShift" runat="server" name="Night Shift" />
																				<label for="chkSATNightShift"></label>
																			</div>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkSunday" onchange="CheckWeekDayOtSpecial('chkSundayOT','chkSundayIsSpecial')" runat="server" name="returnedLockerKeys" />
																			<label for="chkSunday">Sunday</label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkSundayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkSundayIsSpecial','chkSunday')" />
																			<label for="chkSundayOT"></label>
																		</div>
																	</td>
																	<td>
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="chkSundayIsSpecial" runat="server" name="IsSpecial" onchange="CheckWeekDayOtSpecial('chkSundayOT','chkSunday')" />
																			<label for="chkSundayIsSpecial"></label>
																		</div>
																	</td>
																	<td>
																		<input class="form-control exemption-timein" value="1" id="txtSunTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:00 AM" id="txtSUNStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="9:11 AM" id="txtSUNLateInTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:00 PM" id="txtSUNBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="1:30 PM" id="txtSUNBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td style="display: none;">
																		<input class="form-control time-picker" value="2:30 PM" id="txtSUNHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:29 PM" id="txtSUNEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<input class="form-control time-picker" value="5:30 PM" id="txtSUNEndTime" placeholder="00:00" type="text" runat="server" /></td>
																	<td>
																		<div class="col-sm-2">

																			<div class="checkbox checkbox-primary">
																				<input type="checkbox" id="chkSUNNightShift" class="NightShift" runat="server" name="Night Shift" />
																				<label for="chkSUNNightShift"></label>
																			</div>
																		</div>
																	</td>
																</tr>
															</tbody>
														</table>

													</div>
												</div>
												<!-- /.row -->

											</ContentTemplate>
										</asp:UpdatePanel>
									</div>

									<%--Tab content close--%>
								</div>
							</div>
						</div>

					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="clearfix">&nbsp;</div>
				</section>
				<!-- #dash-content -->
			</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<asp:UpdatePanel runat="server">
			<ContentTemplate>
				<uc:Scripts ID="script1" runat="server"></uc:Scripts>
			</ContentTemplate>
		</asp:UpdatePanel>
		<!--========== END app main -->

		<script>
			function readUrlMultiple1(input) {
				if (input.files && input.files[0]) {
					var fp = $("#updLogo");
					var lg = fp[0].files.length; // get length

					var fileSize = input.files[0].size;
					if (fileSize > 2000000) {
						removeUpload();
						alert("File size is too big");
					}
					else {

						$('.image-title').html(lg + 'Files Uploaded');
						if (lg > 0) {
							$('.image-upload-wrap').hide();
							$('.file-upload-content').show();
						}
					}


				} else {
					removeUploadMultiple1();
				}
			}

			function removeUploadMultiple1() {
				$('.file-upload-input').replaceWith($('.file-upload-input').clone());
				$('.file-upload-content').hide();
				$('.image-upload-wrap').show();
			}
			$('.image-upload-wrap').bind('dragover', function () {
				$('.image-upload-wrap').addClass('image-dropping');
			});
			$('.image-upload-wrap').bind('dragleave', function () {
				$('.image-upload-wrap').removeClass('image-dropping');
			});

			function readUrlMultiple2(input) {
				if (input.files && input.files[0]) {
					var fp = $("#updAttachments");
					var lg = fp[0].files.length; // get length

					$('.image-title two').html(lg + 'Files Uploaded');
					if (lg > 0) {
						$('.image-upload-wrap two').hide();
						$('.file-upload-content two').show();
					}
				} else {
					removeUploadMultiple2();
				}
			}

			function removeUploadMultiple2() {
				$('.file-upload-input two').replaceWith($('.file-upload-input two').clone());
				$('.file-upload-content two').hide();
				$('.image-upload-wrap two').show();
			}
			$('.image-upload-wrap two').bind('dragover', function () {
				$('.image-upload-wrap two').addClass('image-dropping two');
			});
			$('.image-upload-wrap two').bind('dragleave', function () {
				$('.image-upload-wrap two').removeClass('image-dropping two');
			});

			function readURL(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#imgPhoto').attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				}
			}

			$("#updPhoto").change(function () {
				debugger;
				readURL(this);
			});

			$("#updAttachments").change(function () {
				debugger;
				__doPostBack('attachments', '');
			});

		</script>

		<asp:UpdatePanel runat="server" ID="updScript">
			<ContentTemplate>
				<script>
					function txtGrossSalary_TextChanged() {

						if ($('#txtCalcGrossSalary').val() != "") {
							var grossSalary = parseFloat($('#txtCalcGrossSalary').val());
							var basicSalary = parseFloat((grossSalary * 66.67) / 100);
							var houseAllowance = parseFloat((basicSalary * 35) / 100);
							var Medical = parseFloat((basicSalary * 10) / 100);
							var utilities = parseFloat((grossSalary - basicSalary - houseAllowance - Medical));
							var sumOfAll = basicSalary + houseAllowance + utilities + Medical;

							if (sumOfAll > grossSalary)
								houseAllowance = houseAllowance - 1;

							$('#txtCalcBasicSalary').val(basicSalary.toFixed(2) + '');
							$('#txtCalcHouseRent').val(houseAllowance.toFixed(2) + '');
							$('#txtCalcUtilities').val(utilities.toFixed(2) + '');
							$('#txtCalcMedical').val(Medical.toFixed(2) + '');
						}
						else {
							$('#txtCalcBasicSalary').val('0');
							$('#txtCalcHouseRent').val('0');
							$('#txtCalcUtilities').val('0');
							$('#txtCalcMedical').val('0');

						}
					}

					function copyCalculationsToMainFields() {
						var grossSalary = $("#txtCalcGrossSalary").val();
						var basicSalary = $("#txtCalcBasicSalary").val();
						var houseAllowance = $("#txtCalcHouseRent").val();
						var utilities = $("#txtCalcUtilities").val();
						var MedicalAllowance = $("#txtCalcMedical").val();

						$("#txtGrossSalary").val(grossSalary);
						$("#txtBasicSalary").val(basicSalary);
						$("#txtHouseRentAllowance").val(houseAllowance);
						$("#txtUtility").val(utilities);
						$("#txtMedicalAllowance").val(MedicalAllowance);

						calcSalaries();
					}

					function calcSalaries() {
						$("#txtNetSalary").val('0');
						var medicalAllowance = $("#txtMedicalAllowance").val() != "" ? $("#txtMedicalAllowance").val() : "0";
						var otherAllowance = $("#txtOtherAllowance").val() != "" ? $("#txtOtherAllowance").val() : "0";
						var MaintenanceAllowance = $("#txtMaintenanceAllowance").val() != "" ? $("#txtMaintenanceAllowance").val() : "0";
						var foodAllowance = $("#txtFoodAllowance").val() != "" ? $("#txtFoodAllowance").val() : "0";
						var travelAllowance = $("#txtTravelAllowance").val() != "" ? $("#txtTravelAllowance").val() : "0";
						var carAllowance = $("#txtCarAllowance").val() != "" ? $("#txtCarAllowance").val() : "0";
						var mobileAllowance = $("#txtMobileAllowance").val() != "" ? $("#txtMobileAllowance").val() : "0";
						var fuelAllowance = $("#txtFuelAllowance").val() != "" ? $("#txtFuelAllowance").val() : "0";
						var utility = $("#txtUtility").val() != "" ? $("#txtUtility").val() : "0";
						var houseRentAllowance = $("#txtHouseRentAllowance").val() != "" ? $("#txtHouseRentAllowance").val() : "0";
						var basicSalry = $("#txtBasicSalary").val() != "" ? $("#txtBasicSalary").val() : "0";
						var EOBIAllowance = $("#txtEOBIAllowance").val() != "" ? $("#txtEOBIAllowance").val() : "0";
						var IESSI = $("#txtIESSI").val() != "" ? $("#txtIESSI").val() : "0";
						var OtherDeductions = $("#txtOtherDeductions").val() != "" ? $("#txtOtherDeductions").val() : "0";

						var grossSalary = $("#txtGrossSalary").val() != "" ? $("#txtGrossSalary").val() : "0";

						var netSalary = parseFloat(parseFloat(medicalAllowance) + parseFloat(otherAllowance) + parseFloat(foodAllowance) + parseFloat(travelAllowance) + parseFloat(carAllowance) + parseFloat(mobileAllowance) + parseFloat(fuelAllowance) + parseFloat(utility) + parseFloat(houseRentAllowance) + parseFloat(basicSalry) + parseFloat(MaintenanceAllowance) - parseFloat(EOBIAllowance) - parseFloat(IESSI) - parseFloat(OtherDeductions));
						$("#txtNetSalary").val(netSalary);

						var grossSalary = parseFloat(parseFloat(medicalAllowance) + parseFloat(otherAllowance) + parseFloat(foodAllowance) + parseFloat(travelAllowance) + parseFloat(carAllowance) + parseFloat(mobileAllowance) + parseFloat(fuelAllowance) + parseFloat(utility) + parseFloat(houseRentAllowance) + parseFloat(basicSalry) + parseFloat(MaintenanceAllowance));
						var totDed = parseFloat(parseFloat(EOBIAllowance) + parseFloat(IESSI) + parseFloat(OtherDeductions))
						$("#txtGrossSalary").val(grossSalary);
						$("#txtTotalDeduction").val(totDed);
					}
				</script>

			</ContentTemplate>
		</asp:UpdatePanel>
		<script>
			$(document).ready(function () {
				//called when key is pressed in textbox
				$("#txtWDID").keypress(function (e) {
					//if the letter is not digit then display error and don't type anything
					if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
						//display error message
						$("#errmsg").html("Number only").show();
						return false;
					}
				});
			});
		</script>
	</form>
</body>
</html>
