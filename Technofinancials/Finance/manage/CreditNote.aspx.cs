﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class CreditNote : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        int CDNID
        {
            get
            {
                if (ViewState["CDNID"] != null)
                {
                    return (int)ViewState["CDNID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["CDNID"] = value;
            }
        }
        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            { 
                txtDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");

                ViewState["dtRFQDetails"] = null;
                ViewState["srNo"] = null;
                ViewState["showFirstRow"] = null;
                BindDropDowns();

                dtRFQDetails = createTable();
                BindCOA();

                divAttachments.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;
                divAlertMsg.Visible = false;

                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/credit-note";


                if (HttpContext.Current.Items["CDNID"] != null)
                {
                    CDNID = Convert.ToInt32(HttpContext.Current.Items["CDNID"].ToString());
                    getCDNDetails(CDNID);
                    showFirstRow = true;
                    CheckAccess();
                }

                BindData();
            }
        }
        private void BindCOA()
        {
            //objDB.CategoriesList = "\'CGS/COS\',\'Revenue\',\'Other Rev.\'";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOA.Items.Clear();
            //ddlCOA.DataSource = objDB.GetCOAByCategory(ref errorMsg);
            ddlCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

        }
        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApprove.Visible = false;
            btnReview.Visible = false;
            btnRevApprove.Visible = false;
            lnkReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReview.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;



            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "CDNs";
            objDB.PrimaryColumnnName = "CDNID";
            objDB.PrimaryColumnValue = CDNID.ToString();
            objDB.DocName = "CreditNote";

            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

            if (chkAccessLevel == "Can Edit")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReview.Visible = true;
            }
            if (chkAccessLevel == "Can Edit & Review")
            {
                btnSave.Visible = true;
                btnReview.Visible = true;
                lnkReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve")
            {
                btnSave.Visible = true;
                btnApprove.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve")
            {
                btnSave.Visible = true;
                btnRevApprove.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit")
            {
                btnSave.Visible = true;
            }
        }

        private void getCDNDetails(int CDNID)
        {
            CheckSessions();
            objDB.CDNID = CDNID;
            DataTable dt = new DataTable();
            dt = objDB.GetCDNsByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlInvoice.SelectedValue = dt.Rows[0]["InvoiceID"].ToString();
                    ddlClients.SelectedValue = dt.Rows[0]["ClientID"].ToString();
                    ddlLocation.SelectedValue = dt.Rows[0]["SiteID"].ToString();
                    txtCDNCode.Value = dt.Rows[0]["CDNCode"].ToString();
                    txtDate.Value = dt.Rows[0]["CDNDate"].ToString();
                    txtInvoice.Value = dt.Rows[0]["SalesInvoiceNo"].ToString();
                    txtTotalAmount.Value = dt.Rows[0]["AmountOfInvoice"].ToString();
                    txtNotes.Value = dt.Rows[0]["Notes"].ToString();

                    getCDNItems(CDNID);
                    getCDNAttachments(CDNID);
                }
            }
            Common.addlog("View", "Finance", "CDN \"" + txtCDNCode.Value + "\" Viewed", "CDNs", objDB.CDNID);
        }

        private void getCDNAttachments(int CDNID)
        {
            DataTable dt = new DataTable();
            objDB.CDNID = CDNID;
            dt = objDB.GetCDNAttachmentsByCDNID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    divAttachments.Visible = true;
                    gvAttachments.DataSource = dt;
                    gvAttachments.DataBind();
                }
            }
        }

        private void getCDNItems(int CDNID)
        {
            DataTable dt = new DataTable();
            objDB.CDNID = CDNID;
            dt = objDB.GetCDNItemsByCDNID(ref errorMsg);
            dtRFQDetails = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            BindData();
        }



        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
            return true;
        }



        private void BindDropDowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlInvoice.DataSource = objDB.GetAllApprovedInvoices(ref errorMsg);
            ddlInvoice.DataTextField = "Code";
            ddlInvoice.DataValueField = "InvoiceID";
            ddlInvoice.DataBind();
            ddlInvoice.Items.Insert(0, new ListItem("--- Select Invoice ---", "0"));
            ddlInvoice.SelectedIndex = 0;

            ddlClients.DataSource = objDB.GetAllApprovedClients(ref errorMsg);
            ddlClients.DataTextField = "ClientName";
            ddlClients.DataValueField = "ClientID";
            ddlClients.DataBind();
            ddlClients.Items.Insert(0, new ListItem("--- Select Client ---", "0"));
            ddlClients.SelectedIndex = 0;

            ddlLocation.DataSource = objDB.GetAllSites(ref errorMsg);
            ddlLocation.DataTextField = "SiteName";
            ddlLocation.DataValueField = "SiteID";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("--- Select Location ---", "0"));
            ddlLocation.SelectedIndex = 0;
        }

        protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            //code for generating pr code goes here
        }

        protected DataTable dtRFQDetails
        {
            get
            {
                if (ViewState["dtRFQDetails"] != null)
                {
                    return (DataTable)ViewState["dtRFQDetails"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtRFQDetails"] = value;
            }
        }
        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }
        protected void BindData()
        {
            if (ViewState["dtRFQDetails"] == null)
            {
                dtRFQDetails = createTable();
                ViewState["dtRFQDetails"] = dtRFQDetails;
            }
            else if (dtRFQDetails.Rows.Count == 0)
            {
                dtRFQDetails = createTable();
                showFirstRow = false;
            }
            gv.DataSource = dtRFQDetails;
            gv.DataBind();

            if (showFirstRow)
                gv.Rows[0].Visible = true;
            else
                gv.Rows[0].Visible = false;

            gv.UseAccessibleHeader = true;
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        private DataTable createTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("ItemCode");
            dt.Columns.Add("ItemID");
            dt.Columns.Add("ItemUnit");
            dt.Columns.Add("Quantity");
            dt.Columns.Add("UnitPrice");
            dt.Columns.Add("NetPrice");
            dt.Columns.Add("Remarks");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();


            return dt;

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlItemCode = e.Row.FindControl("ddlItems") as DropDownList;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlItemCode.DataSource = objDB.GetAllItems(ref errorMsg);
                ddlItemCode.DataTextField = "ItemName";
                ddlItemCode.DataValueField = "ItemID";
                ddlItemCode.DataBind();
                ddlItemCode.Items.Insert(0, new ListItem("--- Select Item ---", "0"));


                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = srNo.ToString();

                e.Row.Cells[7].ColumnSpan = 2;
                e.Row.Cells.RemoveAt(8);
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[7].ColumnSpan = 2;
                e.Row.Cells.RemoveAt(8);
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    hdnRowNo.Value = e.Row.RowIndex.ToString();
                }
            }
        }
        protected void calcTotal()
        {
            float total_Amount = 0;
            if (dtRFQDetails != null)
            {
                if (dtRFQDetails.Rows.Count > 0)
                {
                    foreach (DataRow row in dtRFQDetails.Rows)
                    {
                        if (!string.IsNullOrEmpty(row[6].ToString()))
                        {
                            total_Amount += float.Parse(row[6].ToString());
                        }
                    }
                }
            }
            txtTotalAmount.Value = total_Amount.ToString();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CheckSessions();
            bool similar = false;
            foreach (DataRow row in dtRFQDetails.Rows)
            {
                if (row[1].ToString() == ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Text)
                {
                    string itemUnit = ((DropDownList)gv.FooterRow.FindControl("ddlUnit")).SelectedItem.Text;
                    string qty = (Convert.ToInt32(row[4]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtQty")).Text)).ToString();
                    string unitPrice = (Convert.ToInt32(row[5]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtUnitPrice")).Text)).ToString();
                    string totalPrice = (Convert.ToInt32(row[6]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtNetPrice")).Text)).ToString();

                    string remarks = row[7].ToString() + "," + ((TextBox)gv.FooterRow.FindControl("txtRemarks")).Text;

                    row.SetField(3, itemUnit);
                    row.SetField(4, qty);
                    row.SetField(5, unitPrice);
                    row.SetField(6, totalPrice);
                    row.SetField(7, remarks);

                    dtRFQDetails.AcceptChanges();

                    similar = true;
                    break;
                }


            }

            if (!similar)
            {
                DataRow dr = dtRFQDetails.NewRow();
                dr[0] = srNo.ToString();
                dr[1] = ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Text;
                dr[2] = ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Value;
                dr[3] = ((DropDownList)gv.FooterRow.FindControl("ddlUnit")).SelectedItem.Text;
                dr[4] = ((TextBox)gv.FooterRow.FindControl("txtQty")).Text;
                dr[5] = ((TextBox)gv.FooterRow.FindControl("txtUnitPrice")).Text;
                dr[6] = ((TextBox)gv.FooterRow.FindControl("txtNetPrice")).Text;
                dr[7] = ((TextBox)gv.FooterRow.FindControl("txtRemarks")).Text;
                dtRFQDetails.Rows.Add(dr);
                dtRFQDetails.AcceptChanges();

                srNo += 1;
            }
            BindData();


            ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
            calcTotal();

        }

        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void gv_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {

        }

        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            //dtRFQDetails.Rows[index].SetField(3, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditSuppliers")).SelectedItem.Text);
            //dtRFQDetails.Rows[index].SetField(4, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditSuppliers")).SelectedItem.Value);
            dtRFQDetails.Rows[index].SetField(3, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditUnit")).SelectedItem.Text);
            dtRFQDetails.Rows[index].SetField(4, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditQty")).Text);
            dtRFQDetails.Rows[index].SetField(5, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditUnitPrice")).Text);
            dtRFQDetails.Rows[index].SetField(6, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditNetPrice")).Text);
            dtRFQDetails.Rows[index].SetField(7, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditRemarks")).Text);


            dtRFQDetails.AcceptChanges();

            gv.EditIndex = -1;
            BindData();
            calcTotal();
        }

        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindData();
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {

            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
            {
                if (dtRFQDetails.Rows[i][0].ToString() == delSr)
                {
                    dtRFQDetails.Rows[i].Delete();
                    dtRFQDetails.AcceptChanges();
                }
            }
            int count = 1;
            for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
            {
                dtRFQDetails.Rows[i].SetField(0, count);
                dtRFQDetails.AcceptChanges();
                count++;
            }
            srNo = count;
            BindData();
            calcTotal();
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                BindData();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.InvoiceID = Convert.ToInt32(ddlInvoice.SelectedItem.Value);
                objDB.ClientID = Convert.ToInt32(ddlClients.SelectedItem.Value);
                objDB.SiteID = Convert.ToInt32(ddlLocation.SelectedItem.Value);
                objDB.CDNCode = txtCDNCode.Value;
                objDB.CDNDate = txtDate.Value;
                objDB.SalesInvoiceNo = txtInvoice.Value;
                objDB.AmountOfInvoice = float.Parse(txtTotalAmount.Value);
                objDB.Notes = txtNotes.Value;
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.ChartOfAccountID = int.Parse(ddlCOA.SelectedValue);

                if (HttpContext.Current.Items["CDNID"] != null)
                {
                    objDB.CDNID = CDNID;
                    res = objDB.UpdateCDNs();
                }
                else
                {
                    string returnedID = objDB.AddCDNs();
                    CDNID = Convert.ToInt32(returnedID);
                    clearFields();
                    res = "New CDN Added Successfully";
                }

                if (itemAttachments.HasFile || itemAttachments.HasFiles)
                {
                    foreach (HttpPostedFile file in itemAttachments.PostedFiles)
                    {
                        Random rand = new Random((int)DateTime.Now.Ticks);
                        int randnum = 0;
                        randnum = rand.Next(1, 100000);

                        string fn = "";
                        string exten = "";

                        string destDir = Server.MapPath("~/assets/files/CDNs/");
                        randnum = rand.Next(1, 100000);
                        fn = txtCDNCode.Value + "_" + randnum;

                        if (!Directory.Exists(destDir))
                        {
                            Directory.CreateDirectory(destDir);
                        }

                        string fname = Path.GetFileName(file.FileName);
                        exten = Path.GetExtension(file.FileName);
                        file.SaveAs(destDir + fn + exten);

                        objDB.CDNID = Convert.ToInt32(CDNID);
                        objDB.AttachmentPath = "http://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/CDNs/" + fn + exten;
                        objDB.AddCDNAttachments();
                    }
                }

                objDB.CDNID = CDNID;
                objDB.DeleteCDNItemsByCDNID();
                dtRFQDetails = (DataTable)ViewState["dtRFQDetails"] as DataTable;
                if (dtRFQDetails != null)
                {
                    if (dtRFQDetails.Rows.Count > 0)
                    {
                        if (!showFirstRow)
                        {
                            dtRFQDetails.Rows[0].Delete();
                            dtRFQDetails.AcceptChanges();
                        }

                        for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
                        {
                            objDB.CDNID = Convert.ToInt32(CDNID);
                            objDB.ItemID = Convert.ToInt32(dtRFQDetails.Rows[i]["ProductAndServicesID"].ToString());
                            objDB.ItemUnit = dtRFQDetails.Rows[i]["ItemUnit"].ToString();
                            objDB.Qty = Convert.ToInt32(dtRFQDetails.Rows[i]["Quantity"].ToString());
                            objDB.UnitPrice = Convert.ToInt32(dtRFQDetails.Rows[i]["UnitPrice"].ToString());
                            objDB.NetPrice = Convert.ToInt32(dtRFQDetails.Rows[i]["NetPrice"].ToString());
                            objDB.Remarks = dtRFQDetails.Rows[i]["Remarks"].ToString();
                            objDB.ModifiedBy = Session["UserName"].ToString();

                            objDB.AddCDNItems();
                        }
                    }
                }


                if (!showFirstRow)
                {
                    ViewState["dtRFQDetails"] = null;
                    ViewState["srNo"] = null;
                }

                BindData();
                if (res == "New CDN Added Successfully" || res == "CDN Updated Successfully")
                {
                    if (res == "New CDN Added Successfully") { Common.addlog("Add", "Finance", " CDN \"" + objDB.CDNCode + "\" Added", "CDNs"); }
                    if (res == "CDN Updated Successfully") { Common.addlog("Update", "Finance", "CDN\"" + objDB.CDNCode + "\" Updated", "CDNs", objDB.CDNID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtNotes.Value = "";
            //dtRFQDetails = createTable();
            ddlInvoice.SelectedIndex = 0;
            ddlClients.SelectedIndex = 0;
            txtCDNCode.Value = "";
            txtInvoice.Value = "";
            ddlLocation.SelectedIndex = 0;
            txtCDNCode.Value = "";
            txtTotalAmount.Value = "0";
            txtNotes.Value = "";

        }

        protected void ddlRFQ_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CheckSessions();
            //if (ddlRFQ.SelectedIndex != 0)
            //{
            //    txtPOCode.Value = "PO_" + ddlRFQ.SelectedItem.Text.Split('_')[1];
            //    getRFQItemsByRFQID(Convert.ToInt32(ddlRFQ.SelectedItem.Value));
            //    calcTotal();
            //}
            //else
            //{
            //    txtPOCode.Value = "";
            //    dtRFQDetails = createTable();
            //}
            BindData();
        }

        protected void txtNetPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            TextBox txtQty = gv.FooterRow.FindControl("txtQty") as TextBox;
            string qty = txtQty.Text;
            TextBox txtUnitPrice = gv.FooterRow.FindControl("txtUnitPrice") as TextBox;
            string unitPrice = txtUnitPrice.Text;
            TextBox txtRemarks = gv.FooterRow.FindControl("txtRemarks") as TextBox;

            if (!string.IsNullOrEmpty(qty) && !string.IsNullOrEmpty(unitPrice))
            {
                TextBox txtNetPrice = gv.FooterRow.FindControl("txtNetPrice") as TextBox;
                string netPrice = (Convert.ToInt32(qty) * Convert.ToInt32(unitPrice)).ToString();

                txtNetPrice.Text = netPrice;
            }
            TextBox txtSender = (TextBox)sender as TextBox;
            if (txtSender.ID == "txtQty")
                txtUnitPrice.Focus();
            else if (txtSender.ID == "txtUnitPrice")
                txtRemarks.Focus();

        }

        protected void txtEditUnitPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            int rowNum = Convert.ToInt32(hdnRowNo.Value);
            TextBox txtQty = gv.Rows[rowNum].FindControl("txtEditQty") as TextBox;
            string qty = txtQty.Text;
            TextBox txtUnitPrice = gv.Rows[rowNum].FindControl("txtEditUnitPrice") as TextBox;
            string unitPrice = txtUnitPrice.Text;

            if (!string.IsNullOrEmpty(qty) && !string.IsNullOrEmpty(unitPrice))
            {
                TextBox txtNetPrice = gv.Rows[rowNum].FindControl("txtEditNetPrice") as TextBox;
                string netPrice = (Convert.ToInt32(qty) * Convert.ToInt32(unitPrice)).ToString();

                txtNetPrice.Text = netPrice;
            }

            TextBox txtSender = (TextBox)sender as TextBox;
            txtSender.Focus();
        }

        protected void gv_PreRender(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
                gv.UseAccessibleHeader = true;
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void getRFQItemsByRFQID(int RFQID)
        {
            DataTable dt = new DataTable();
            objDB.RFQID = RFQID;
            dt = objDB.GetRFQItemsByRFQID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dtRFQDetails.Rows.Add(new object[] {
                            row[0],
                            row[1],
                            row[2],
                            row[3],
                            row[4],
                            "0",
                            "0",
                            row[5]
                        });

                        dtRFQDetails.AcceptChanges();
                    }

                    BindData();

                }
            }
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "CDNs", "CDNID", HttpContext.Current.Items["CDNID"].ToString(), Session["UserName"].ToString());
            Common.addlogNew(res, "Finance", "Credit Notes of ID\"" + HttpContext.Current.Items["CDNID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/credit-note", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/credit-note/edit-credit-note-" + HttpContext.Current.Items["CDNID"].ToString(), "Credit Notes \"" + txtCDNCode.Value + "\"", "CDNs", "CreditNote", Convert.ToInt32(HttpContext.Current.Items["CDNID"].ToString()));

            //Common.addlog(res, "Finance", "Credit Notes of ID \"" + HttpContext.Current.Items["CDNID"].ToString() + "\" Status Changed", "CDNs", CDNID);

            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;

            CheckAccess();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            string type = btn.CommandArgument;
            string res = Common.addAccessLevels(type, "CDNs", "CDNID", HttpContext.Current.Items["CDNID"].ToString(), Session["UserName"].ToString());
            Common.addlog("Delete", "Finance", "Credit Notes of ID \"" + HttpContext.Current.Items["CDNID"].ToString() + "\" deleted", "CDNs", CDNID);

            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            Response.Redirect(btnBack.HRef);

        }

        protected void ddlInvoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            if (ddlInvoice.SelectedIndex != 0)
            {
                getInvoiceDetails(Convert.ToInt32(ddlInvoice.SelectedItem.Value));
                objDB.InvoiceID = Convert.ToInt32(ddlInvoice.SelectedItem.Value);
                txtCDNCode.Value = objDB.GenereateCDNCodeByInvoiceID();
                calcTotal();
            }
            else
            {
                ddlClients.SelectedIndex = 0;
                ddlLocation.SelectedIndex = 0;
                txtInvoice.Value = "";
                txtTotalAmount.Value = "0";
                txtCDNCode.Value = "";
                showFirstRow = false;
                srNo = 1;
                dtRFQDetails = createTable();
            }
            BindData();
        }

        private void getInvoiceDetails(int InvoiceID)
        {
            DataTable dt = new DataTable();
            objDB.InvoiceID = InvoiceID;
            dt = objDB.GetInvoiceByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    showFirstRow = true;
                    getInvoiceItemsByInvoiceID(Convert.ToInt32(dt.Rows[0]["InvoiceID"].ToString()));
                    ddlClients.SelectedValue = dt.Rows[0]["ClientID"].ToString();
                    ddlLocation.SelectedValue = dt.Rows[0]["SiteID"].ToString();
                    txtTotalAmount.Value = dt.Rows[0]["Amount"].ToString();
                }
            }
        }
        private void getInvoiceItemsByInvoiceID(int InvoiceID)
        {
            DataTable dt = new DataTable();
            objDB.InvoiceID = InvoiceID;
            dt = objDB.GetInvoiceItemsByInvoiceID(ref errorMsg);
            dtRFQDetails = dt;
            BindData();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }
        }
        //private void getPODetails(int POID)
        //{
        //    DataTable dt = new DataTable();
        //    objDB.POID = POID;
        //    dt = objDB.GetInvoicesByID(ref errorMsg);
        //    if (dt != null)
        //    {
        //        if (dt.Rows.Count > 0)
        //        {
        //            ddlClients.SelectedValue = dt.Rows[0]["ClientID"].ToString();
        //            ddlLocation.SelectedValue = dt.Rows[0]["SiteID"].ToString();
        //            txtTotalAmount.Value = dt.Rows[0]["Amount"].ToString();
        //        }
        //    }
        //}


        //private void getPOItemsByPOID(int POID)
        //{
        //    DataTable dt = new DataTable();
        //    objDB.POID = POID;
        //    dt = objDB.GetPOItemsByPOID(ref errorMsg);
        //    dtRFQDetails = dt;
        //    if (dt != null)
        //    {
        //        if (dt.Rows.Count > 0)
        //        {
        //            srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
        //        }
        //    }
        //}

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

    }
}