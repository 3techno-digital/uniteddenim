﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CDMS.aspx.cs" Inherits="Technofinancials.Finance.manage.CDMS" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">
                <section class="app-content">
                    <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                        <ContentTemplate>

                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <img src="/assets/images/CRM/clients.png" class="img-responsive tf-page-heading-img" />
                                    <h3 class="tf-page-heading-text">Clients</h3>
                                </div>

                                	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                                <div class="col-sm-4">
                                    <div class="pull-right flex">
                                        <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>

                                        <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                        <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class"  CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                        <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                        <button class="tf-save-btn" "Reject & Disapprove" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <a class="tf-back-btn" "Add New Lead" id="btnAddLead" runat="server"><i class="fa fa-plus"></i></a>
                                        <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr />
                                </div>
                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnSave" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

                                    <h3 class="tf-page-heading-text">Company Details</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Company Name  <span style="color: red !important;">*</span>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCompanyName" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                        <input type="text" class="form-control" placeholder="Company Name" name="companyName" id="txtCompanyName" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Short Name  <span style="color: red !important;">*</span>
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCompanyShortName" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                        <input type="text" class="form-control" placeholder="Company Short Name" name="companyShortName" id="txtCompanyShortName" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h4 for="comment">Description</h4>
                                        <textarea class="form-control" rows="3" name="companyDescription" id="txtCompanyDescription" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="user-img-div">
                                <img src="/assets/images/3techno-Logo.png" class="img-responsive" id="imgLogo" runat="server" clientidmode="static" />
                                <asp:FileUpload ID="updLogo" runat="server" ClientIDMode="Static" Style="display: none;" accept=".png,.jpg,.jpeg,.gif" />
                                <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('updLogo').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">

                                <h4>Phone Number 1  <span style="color: red !important;">*</span>
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtPhoneNumber1" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                <input class="form-control phone-no-val" placeholder="(XXX) XXX-XXXX" maxlength="14" type="text" name="compnayNtn" id="txtPhoneNumber1" runat="server" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">

                                <h4>Phone Number 2</h4>
                                <input class="form-control phone-no-val" placeholder="(XXX) XXX-XXXX" maxlength="14" type="text" name="companyStrn" id="txtPhoneNumber2" runat="server" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">

                                <h4>Email  <span style="color: red !important;">*</span>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtEmail" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmail" ForeColor="Red" ErrorMessage=" Email Address Not Valid" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="btnValidate"></asp:RegularExpressionValidator></h4>
                                <input type="text" class="form-control" placeholder="Email" name="companySecp" id="txtEmail" runat="server" />
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">

                                <div class="col-sm-4">
                                    <h4>Country  <span style="color: red !important;">*</span>
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCountries" InitialValue="0" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                    <asp:DropDownList ID="ddlCountries" runat="server" class="form-control select2" data-plugin="select2" OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-sm-4">
                                    <h4>State  <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlStates" InitialValue="0" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                    <asp:DropDownList ID="ddlStates" runat="server" class="form-control select2" data-plugin="select2" OnSelectedIndexChanged="ddlStates_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-sm-4">
                                    <h4>City  <span style="color: red !important;">*</span>
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlCities" InitialValue="0" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                    <asp:DropDownList ID="ddlCities" runat="server" class="form-control select2" data-plugin="select2"></asp:DropDownList>
                                </div>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <h4>Address Line 1  <span style="color: red !important;">*</span>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtAddressLine1" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                            <input class="form-control" placeholder="Address Line 1" type="text" name="addressLine1" id="txtAddressLine1" runat="server" />
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <h4>Address Line 2</h4>
                            <input class="form-control" placeholder="Address Line 2" type="text" id="txtAddressLine2" runat="server" />
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <h4>Zip code</h4>
                            <input class="form-control" placeholder="Zip code" type="Number" name="zipCode" id="txtZIPCode" runat="server" />
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <div id="SubComapny" runat="server">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

                                <h3 class="tf-page-heading-text">Contact Person</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <h4>First Name  <span style="color: red !important;">*</span>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtFPFirstName" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                <input class="form-control AphabetOnly" placeholder="First Name" type="text" id="txtFPFirstName" runat="server" name="firstName" />
                            </div>
                            <div class="col-sm-4">
                                <h4>Middle Name </h4>
                                <input class="form-control AphabetOnly" placeholder="Middle Name" type="text" id="txtFPMiddleName" runat="server" name="middleName" />
                            </div>
                            <div class="col-sm-4">
                                <h4>Last Name  <span style="color: red !important;">*</span>
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtFPLastName" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                <input class="form-control AphabetOnly" placeholder="Last Name" type="text" id="txtFPLastName" runat="server" name="lastName" />
                            </div>
                            <div class="col-sm-4">
                                <h4>Phone Number  <span style="color: red !important;">*</span>
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtFPPhoneNumber" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                <input class="form-control phone-no-val" placeholder="(XXX) XXX-XXXX" maxlength="14" type="text" name="email" id="txtFPPhoneNumber" runat="server" />
                            </div>
                            <div class="col-sm-4">
                                <h4>Email address  <span style="color: red !important;">*</span>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtFPEmail" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFPEmail" ForeColor="Red" ErrorMessage=" Email Address Not Valid" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="btnValidate"></asp:RegularExpressionValidator></h4>
                                <input class="form-control" placeholder="Email address" type="email" name="email" id="txtFPEmail" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <div class="input-group input-group-lg">
                                    <label>
                                        CoA  <span style="color: red !important;">*</span>
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="ddlCOA" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                        <asp:Label CssClass="lbl-COA-Balance"  Text="Balance: 0" ID="lblCOABal" runat="server" />
                                    </label>
                                    <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlCOA_SelectedIndexChanged" ID="ddlCOA">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group input-group-lg">
                                    <label>Busniess Name </label>
                                    <span style="color:red !important;">* <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" ControlToValidate="txtBusinessName" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></span> 
                                    <input type="text" class="form-control form-text" placeholder="Business Name" id="txtBusinessName" runat="server">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group input-group-lg">
                                    <label>Bank Name</label>
                                    <input type="text" class="form-control form-text" placeholder="Bank Name" id="txtBankName" runat="server">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Account No </label>
                                <div class="input-group input-group-lg">
                                    <input class="form-control phone-no-val" placeholder="Account No" type="text" id="txtAccountNo" runat="server">
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                               <div class="col-md-12 col-sm-12">
                                <label>NTN, GST & PST/SST/KST/BST </label>
                                <div class="input-group input-group-lg">
                                      <textboxio:Textboxio runat="server" ID="txtNTN" ScriptSrc="/assets/textboxio/textboxio.js" Width="100%" Height="200px" />
                                    <%--<input class="form-control phone-no-val" placeholder="NTN" type="text" id="txtNTN" runat="server">--%>
                                    <%--<textarea class="form-control" rows="3" name="NTN" id="txtNTN" runat="server"></textarea>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="leadsContainer" runat="server">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="clearfix">&nbsp;</div>
                                <h3>Client Leads</h3>
                                <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr. No">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Title">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCol6" Text='<%# Eval("Title") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Added By">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("PreparedBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Busniess Division">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("BDName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="POC">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCol5" Text='<%# Eval("POC") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Value">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("TotalValue") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qualification Status">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("QualificationStatus") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View">
                                            <ItemTemplate>
                                                <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/crm/manage/leads/edit-lead-" + Eval("LeadID") %>" class="edit-classs" >View </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                        </div>

                    </div>

                    <div class="clearfix">&nbsp;</div>
               

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#imgLogo').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#updLogo").change(function () {
                readURL(this);
            });
        </script>

        <style>
            .month-table-show {
                display: none;
                margin-top: 40px;
            }

            .modal-dialog {
                width: 450px;
                margin: 30px auto;
            }

            section.app-content {
                padding-bottom: 53px;
            }

            .modal-footer {
                border-top: 0px solid #e5e5e5;
            }

            .hiring-create-new-btn {
                margin-top: 17px;
                border: none;
                background: none;
                color: #01769a;
            }

            .user-img-div img {
                width: 169px !important;
                display: block;
                margin: 0 auto;
            }

            .user-img-div {
                width: auto;
            }
        </style>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            #btnAddLead {
                padding: 10px 8px 7px 9px !important;
            }
        </style>
    </form>

</body>
</html>


