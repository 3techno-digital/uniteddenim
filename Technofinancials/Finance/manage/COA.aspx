﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="COA.aspx.cs" Inherits="Technofinancials.Finance.manage.COA" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">
                <section class="app-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img src="/assets/images/chart-of-account.png" class="img-responsive tf-page-heading-img" />
                                <h3 class="tf-page-heading-text">Chart of Accounts</h3>
                            </div>

                            <div class="col-md-4">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group" id="divAlertMsg" runat="server">
                                            <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                <span>
                                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                </span>
                                                <p id="pAlertMsg" runat="server">
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                <ContentTemplate>
                                    <div class="col-sm-4">
                                        <div class="pull-right flex">
                                            <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" text="Note"><i class="fa fa-sticky-note-o"></i></button>
                                            <button class="tf-save-btn" text= "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                            <button class="tf-save-btn" text ="Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                            <button class="tf-save-btn" text="Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                            <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" text="Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class"  CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                            <button class="tf-save-btn" text= "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                            <button class="tf-save-btn" text="Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                            <button class="tf-save-btn" text="Reject & Disapproved" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                            <button class="tf-save-btn" etxt="Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                            <a class="tf-back-btn" text="Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr />
                            </div>
                        </div>

                        <div class="clearfix">&nbsp;</div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="container-fluid my-container">
                            <div class="dashboard-left inner-page">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <%--<div class="col-md-6">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Company
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="ddlCompany" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" runat="server" class=" form-control form-text input-group js-example-basic-single select2" ID="ddlCompany" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>--%>

                                            <%--<div class="col-md-6">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Location
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlLocation" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class=" form-control form-text input-group js-example-basic-single select2" ID="ddlLocation"  AutoPostBack="true" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>--%>


                                            <div class="col-md-6">
                                                <div class="input-group input-group-lg">
                                                    <label>
                                                        Account Type
                                            <span style="color: red !important;">*</span>
                                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="ddlAccount" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                    </label>
                                                    <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class=" form-control form-text input-group js-example-basic-single select2" ID="ddlAccount" AutoPostBack="true" OnSelectedIndexChanged="ddlAccount_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-6" style="display: none;">
                                                <div class="input-group input-group-lg">
                                                    <label>
                                                        Tax
                                                    </label>
                                                    <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class=" form-control form-text input-group js-example-basic-single select2" ID="ddlTax"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="input-group input-group-lg">
                                                    <label>
                                                        Account For
                                                    </label>
                                                    <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlFor">
                                                        <asp:ListItem Value="Other">Other</asp:ListItem>
                                                        <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                                        <asp:ListItem Value="Bank">Bank</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group input-group-lg">
                                                    <label>
                                                        Account Title
                                            <span style="color: red !important;">*</span>
                                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtAccountTitle" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                    </label>
                                                    <input type="text" class="form-control" id="txtAccountTitle" runat="server" placeholder="Title" />
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="input-group input-group-lg">
                                                    <label>
                                                        Account Code (XXX)  <span style="color: red !important;">*</span>
                                                        <asp:RangeValidator ID="RangeValtxtAccountCode" runat="server" ControlToValidate="txtAccountCode" ErrorMessage="Code must be Integer & b/w 1-999." ForeColor="Red" MaximumValue="999" MinimumValue="1" Type=" Integer" Display="Dynamic" ValidationGroup="btnValidate"></asp:RangeValidator>
                                                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtAccountCode" ID="REValidatortxtAccountCode" ValidationExpression="^[\s\S]{3,3}$" runat="server" ForeColor="Red" ErrorMessage="Invalid Length!" ValidationGroup="btnValidate"></asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtAccountCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                    </label>
                                                    <asp:TextBox class="form-control" ID="txtAccountCode" Text="" runat="server" OnTextChanged="txtAccountCode_TextChanged" AutoPostBack="true"> </asp:TextBox>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group input-group-lg">
                                                    <label>
                                                        COA Code
                                            <span style="color: red !important;">*</span>
                                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtGLCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                    <input type="text" class="form-control" id="txtGLCode" runat="server" placeholder="COA Code" disabled="disabled" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group input-group-lg">
                                                    <label>
                                                        Opening Balance
                                            <span style="color: red !important;">*</span>
                                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtOpeningBalance" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                    <input type="number" class="form-control" id="txtOpeningBalance" runat="server" placeholder="Opening Balance" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Description </label>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="txtDescription" runat="server" name="Description"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="add-table-div">
                                                    <asp:HiddenField ID="hdnRowNo" runat="server" />
                                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered" OnPreRender="gv_PreRender" ClientIDMode="Static" ShowHeaderWhenEmpty="True" OnRowDataBound="gv_RowDataBound" OnRowUpdated="gv_RowUpdated" OnRowUpdating="gv_RowUpdating" OnRowCancelingEdit="gv_RowCancelingEdit" AutoGenerateColumns="False" ShowFooter="True" OnRowEditing="gv_RowEditing">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Sr. No">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSrNo" runat="server" Text='<%# Eval("SrNo")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Location  <span style='color:red !important;'>*</span>">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("LocationName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="ddlLocation" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                    <asp:DropDownList ID="ddlLocation" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single"></asp:DropDownList>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Balance  <span style='color:red !important;'>*</span>">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBalance" runat="server" Text='<%# Eval("Balance")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtEditBalance" TextMode="Number" CssClass="form-control" runat="server" Text='<%# Eval("Balance")%>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator14" ControlToValidate="txtBalance" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                    <asp:TextBox ID="txtBalance" TextMode="Number" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ShowHeader="False">
                                                                <EditItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Button ID="btnAdd" runat="server" ValidationGroup="btnValidatef" CausesValidation="true" CssClass="form-control" Text="Add" OnClick="btnAdd_Click1"></asp:Button>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemove_Command"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                      
                                        <!-- Modal -->
                                        <div class="modal fade" id="notes-modal" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Notes</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>
                                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                        </p>
                                                        <p>
                                                            <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                        </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                   
                            </div>
                        </div>
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>
            $("#button").click(function () {
                $("form1").valid();
            });
        </script>

        <script>
            $(document).ready(function () {
                var navListItems = $('div.setup-panel div a'),
                    allWells = $('.setup-content'),
                    allNextBtn = $('.nextBtn');

                allWells.hide();

                navListItems.click(function (e) {
                    e.preventDefault();
                    var $target = $($(this).attr('href')),
                        $item = $(this);

                    if (!$item.hasClass('disabled')) {
                        navListItems.removeClass('btn-primary').addClass('btn-default');
                        $item.addClass('btn-primary');
                        allWells.hide();
                        $target.show();
                        $target.find('input:eq(0)').focus();
                    }
                });

                allNextBtn.click(function () {
                    var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                        curInputs = curStep.find("input[type='text'],input[type='url']"),
                        isValid = true;

                    $(".form-group").removeClass("has-error");
                    for (var i = 0; i < curInputs.length; i++) {
                        if (!curInputs[i].validity.valid) {
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                    }

                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                });

                $('div.setup-panel div a.btn-primary').trigger('click');
            });
        </script>
        
    </form>
     <style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }


            .aspNetDisabled {
                width: 100% !important;
                background: #eeeeee;
                height: 46px;
                padding: 10px 16px;
                font-size: 18px;
                line-height: 1.3333333;
                border-radius: 6px;
                border-color: #ccc;
                outline: none;
                box-shadow: none;
            }
        </style>

        <style>
            .stepwizard-step p {
                margin-top: 10px;
            }

            .stepwizard-row {
                display: table-row;
            }

            .stepwizard {
                display: table;
                width: 50%;
                position: relative;
            }

            .stepwizard-step button[disabled] {
                opacity: 1 !important;
                filter: alpha(opacity=100) !important;
            }

            .stepwizard-row:before {
                top: 14px;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 100%;
                height: 1px;
                background-color: #ccc;
                z-order: 0;
            }

            .stepwizard-step {
                display: table-cell;
                text-align: center;
                position: relative;
            }

            .btn-circle {
                width: 30px;
                height: 30px;
                text-align: center;
                padding: 6px 0;
                font-size: 12px;
                line-height: 1.428571429;
                border-radius: 15px;
            }

            button.btn {
                margin-top: 25px;
                margin-bottom: 48px;
                margin-right: 30px;
            }

            th {
                border-right: 1px solid #fff;
                text-align: center;
            }
        </style>
</body>
</html>


