﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.finance.Manage
{
	public partial class OvertimeGroup : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int GroupID
        {
            get
            {
                if (ViewState["GroupID"] != null)
                {
                    return (int)ViewState["GroupID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["GroupID"] = value;
            }
        }


       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                   
                    ViewState["GroupID"] = null;
                    btnDelete.Visible = false;
                    
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/overtime-group";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["GroupID"] != null)
                    {
                        GroupID = Convert.ToInt32(HttpContext.Current.Items["GroupID"].ToString());
                        GetGroupByID(GroupID);
                        
                        btnDelete.Visible = true;
                        btnSave.Visible = true;

                        //CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
               
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "ClearanceItems";
                objDB.PrimaryColumnnName = "ClearanceItemID";
                objDB.PrimaryColumnValue = GroupID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void GetGroupByID(int GroupID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.GroupID = GroupID;
                dt = objDB.GetGroupByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtTitle.Value = dt.Rows[0]["groupname"].ToString();
                        
                        txtNotes.Value = dt.Rows[0]["groupdescription"].ToString();
                        Generalovertime.Value = dt.Rows[0]["Generalovertime"].ToString();
                        Offdayovertime.Value = dt.Rows[0]["Offdayovertime"].ToString();
                        Holidayovertime.Value = dt.Rows[0]["Holidayovertime"].ToString();
                        Eidovertime.Value = dt.Rows[0]["Eidovertime"].ToString();
                        txtTitle.Disabled = true;

                    }
                }
                Common.addlog("View", "Finance", "GroupID \"" + txtTitle.Value + "\" Viewed", "Overtime Group", objDB.GroupID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
               
                objDB.Notes = txtNotes.Value;
                objDB.Groupname         = txtTitle.Value;
                objDB.Generalovertime = Convert.ToDecimal(Generalovertime.Value);
                objDB.Offdayovertime = Convert.ToDecimal(Offdayovertime.Value);
                objDB.Holidayovertime = Convert.ToDecimal(Holidayovertime.Value);
                objDB.Eidovertime = Convert.ToDecimal(Eidovertime.Value);



                if (HttpContext.Current.Items["GroupID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.GroupID = GroupID;
                    res = objDB.UpdateOvertimeGroup();
                    clearFields();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddOvertimeGroup();
                }

                
                if (res == "Group Added Successfully" || res == "Group Updated")
                {
                    if (res == "Group Added Successfully") { Common.addlog("Add", "Finance", "Group Added \"" + objDB.Title + "\" Added", "GroupAdded"); }
                    if (res == "Group Added Updated") { Common.addlog("Update", "Finance", "Group Added \"" + objDB.Title + "\" Updated", "GroupAdded", objDB.GroupID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtTitle.Value =
            txtNotes.Value = "";
            Generalovertime.Value = "0";
            Offdayovertime.Value = "0";
            Holidayovertime.Value = "0";
            Eidovertime.Value = "0";


        }

        protected void btnDelete_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();

                string res = "";

                if (HttpContext.Current.Items["ClearanceItemID"] != null)
                {
                    objDB.ClearanceItemID = Convert.ToInt32(HttpContext.Current.Items["ClearanceItemID"]);
                    res = objDB.DeleteClearanceItemByID();
                    clearFields();
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
	}
}