﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class Payroll : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int PayrollID
        {
            get
            {
                if (ViewState["PayrollID"] != null)
                {
                    return (int)ViewState["PayrollID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PayrollID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/payroll";

                    divAlertMsg.Visible = false;
                    clearFields();
                    BindCOA();
                    ViewState["SalarySrNo"] = null;
                    ViewState["dtEmployeeSalaries"] = null;

                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["PayrollID"] != null)
                    {
                        PayrollID = Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString());
                        getPayrollByID(PayrollID);
                        ddlMonth.Enabled = false;
                        ddlYear.Enabled = false;
                        CheckAccess();
                    }
                    else
                    {
                        //SalarySrNo = 1;
                        //calculateEmployeeSalaries();
                        //CheckAccess();
                    }
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void BindCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOA.Items.Clear();
            ddlCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

        }
        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;


                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "Payroll";
                objDB.PrimaryColumnnName = "PayrollID";
                objDB.PrimaryColumnValue = PayrollID.ToString();
                objDB.DocName = "PayrollManagement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private DataTable dtEmployeeSalaries
        {
            get
            {
                if (ViewState["dtEmployeeSalaries"] != null)
                {
                    return (DataTable)ViewState["dtEmployeeSalaries"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtEmployeeSalaries"] = value;
            }
        }
        protected int SalarySrNo
        {
            get
            {
                if (ViewState["SalarySrNo"] != null)
                {
                    return (int)ViewState["SalarySrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["SalarySrNo"] = value;
            }
        }
        private DataTable createEmployeeSalariesTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("EmployeeCode");
            dt.Columns.Add("EmployeeName");
            dt.Columns.Add("Department");
            dt.Columns.Add("Designation");
            dt.Columns.Add("BasicSalary");
            dt.Columns.Add("Allowances");
            dt.Columns.Add("Deductions");
            dt.Columns.Add("NetSalary");
            dt.Columns.Add("BankDetails");
            dt.Columns.Add("EmployeeID");
            dt.Columns.Add("Taxes");
            dt.Columns.Add("PFEmployee");
            dt.Columns.Add("PFCompany");
            dt.Columns.Add("PFID");
            dt.Columns.Add("GrossAmount");
            dt.AcceptChanges();

            return dt;
        }
        private void calculateEmployeeSalaries()
        {
            try
            {

                dtEmployeeSalaries = createEmployeeSalariesTable();
                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                dt = objDB.GetAllEmployeesByCompanyID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string empCode = dt.Rows[i]["EmployeeCode"].ToString();
                            string empName = dt.Rows[i]["EmployeeName"].ToString();
                            string deptName = dt.Rows[i]["DeptName"].ToString();
                            string desgTitle = dt.Rows[i]["DesgTitle"].ToString();
                            string empID = dt.Rows[i]["EmployeeID"].ToString();

                            float basicSalary = 0;
                            if (dt.Rows[i]["BasicSalary"].ToString() != "")
                                basicSalary = float.Parse(dt.Rows[i]["BasicSalary"].ToString());

                            float baseAllowances = 0;
                            if (dt.Rows[i]["FuelAllowance"].ToString() != "")
                                baseAllowances += float.Parse(dt.Rows[i]["FuelAllowance"].ToString());
                            if (dt.Rows[i]["MobileAllowance"].ToString() != "")
                                baseAllowances += float.Parse(dt.Rows[i]["MobileAllowance"].ToString());
                            if (dt.Rows[i]["CarAllowance"].ToString() != "")
                                baseAllowances += float.Parse(dt.Rows[i]["CarAllowance"].ToString());
                            if (dt.Rows[i]["TravelAllowance"].ToString() != "")
                                baseAllowances += float.Parse(dt.Rows[i]["TravelAllowance"].ToString());
                            if (dt.Rows[i]["FoodAllowance"].ToString() != "")
                                baseAllowances += float.Parse(dt.Rows[i]["FoodAllowance"].ToString());
                            if (dt.Rows[i]["OtherAllowance"].ToString() != "")
                                baseAllowances += float.Parse(dt.Rows[i]["OtherAllowance"].ToString());
                            if (dt.Rows[i]["MedicalAllowance"].ToString() != "")
                                baseAllowances += float.Parse(dt.Rows[i]["MedicalAllowance"].ToString());
                            //if (dt.Rows[i]["HouseAllownace"].ToString() != "")
                            //    baseAllowances += float.Parse(dt.Rows[i]["HouseAllownace"].ToString());
                            //if (dt.Rows[i]["UtitlityAllowance"].ToString() != "")
                            //    baseAllowances += float.Parse(dt.Rows[i]["UtitlityAllowance"].ToString());

                            float grossAmount = 0;
                            if (dt.Rows[i]["GrossAmount"].ToString() != "")
                                grossAmount = float.Parse(dt.Rows[i]["GrossAmount"].ToString());
                            float netSalary = 0;
                            if (dt.Rows[i]["NetSalary"].ToString() != "")
                                netSalary = float.Parse(dt.Rows[i]["NetSalary"].ToString());


                            string bankDetails = dt.Rows[i]["BankDetails"].ToString();
                            float allowances = getEmployeeAdditionsByEmployeeID(Convert.ToInt32(dt.Rows[i]["EmployeeID"].ToString()), basicSalary.ToString(), baseAllowances);
                            float deductions = getEmployeeDeductionsByEmployeeID(Convert.ToInt32(dt.Rows[i]["EmployeeID"].ToString()), basicSalary.ToString());
                            float pfEmployee = 0;
                            float pfCompany = 0;
                            string PFID = "0";

                            DataTable dtPF;
                            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                            dtPF = objDB.GetProvinentFundByCompanyID(ref errorMsg);
                            if (dtPF != null)
                            {
                                if (dtPF.Rows.Count > 0)
                                {
                                    pfEmployee = (basicSalary * float.Parse(dtPF.Rows[0]["EmployeeRatio"].ToString())) / 100.0f;
                                    pfCompany = (basicSalary * float.Parse(dtPF.Rows[0]["CompanyRatio"].ToString())) / 100.0f;
                                    PFID = dtPF.Rows[0]["PFID"].ToString();
                                }
                            }

                            objDB.EmployeeID = int.Parse(empID);
                            objDB.CreatedBy = Session["UserName"].ToString();
                            string loans = objDB.AddEmployeeLaonInstallment();
                            if (loans != "")
                                deductions += float.Parse(loans);

                            //float netSalary = ((basicSalary + allowances) - deductions - pfEmployee);
                            dtEmployeeSalaries.Rows.Add(new object[] {
                            SalarySrNo,
                            empCode,
                            empName,
                            deptName,
                            desgTitle,
                            basicSalary,
                            allowances,
                            deductions,
                            netSalary,
                            bankDetails,
                            empID,
                            "0",
                            pfEmployee,
                            pfCompany,
                            PFID,
                            grossAmount
                        });

                            SalarySrNo += 1;
                        }
                        BindSalariesTable();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private float getEmployeeAdditionsByEmployeeID(int employeeID, string basicSalary, float baseAllowances)
        {
            try
            {

                float total = 0;
                DataTable dt = new DataTable();
                objDB.EmployeeID = employeeID;
                dt = objDB.GetAllEmployeeBudgetByEmployeeID(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        dt = Common.filterTable(dt, "TransictionType", "Additions");
                    }
                }

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    if (dt.Rows[i][4].ToString() != "")
                                    {
                                        if (basicSalary != "")
                                        {
                                            if (dt.Rows[i][3].ToString() == "Percentage")
                                            {
                                                total += ((float.Parse(dt.Rows[i][4].ToString()) / 100) * float.Parse(basicSalary));
                                            }
                                            else
                                            {
                                                total += float.Parse(dt.Rows[i][4].ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return total + baseAllowances;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                return 0f;
            }

        }
        private float getEmployeeDeductionsByEmployeeID(int employeeID, string basicSalary)
        {
            try
            {

                float total = 0;
                DataTable dt = new DataTable();
                objDB.EmployeeID = employeeID;
                dt = objDB.GetAllEmployeeBudgetByEmployeeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        dt = Common.filterTable(dt, "TransictionType", "Deductions");
                    }
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    if (dt.Rows[i][4].ToString() != "")
                                    {
                                        if (basicSalary != "")
                                        {
                                            if (dt.Rows[i][3].ToString() == "Percentage")
                                            {
                                                total += ((float.Parse(dt.Rows[i][4].ToString()) / 100) * float.Parse(basicSalary));
                                            }
                                            else
                                            {
                                                total += float.Parse(dt.Rows[i][4].ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return total;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                return 0f;
            }

        }
        protected void BindSalariesTable()
        {
            try
            {

                if (ViewState["dtEmployeeSalaries"] == null)
                {
                    dtEmployeeSalaries = createEmployeeSalariesTable();
                    ViewState["dtEmployeeSalaries"] = dtEmployeeSalaries;
                }

                gvSalaries.DataSource = dtEmployeeSalaries;
                gvSalaries.DataBind();

                gvSalaries.UseAccessibleHeader = true;
                if (gvSalaries.Rows.Count > 0)
                {
                    gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    float totalBasicSalaries = 0;
                    float totalAllowances = 0;
                    float totalDeductions = 0;
                    float totalPFEmployee = 0;
                    float totalPFCompany = 0;
                    float totalNetSalaries = 0;
                    float totalTaxes = 0;

                    for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                    {
                        totalBasicSalaries += float.Parse(dtEmployeeSalaries.Rows[i]["BasicSalary"].ToString());
                        totalAllowances += float.Parse(dtEmployeeSalaries.Rows[i]["Allowances"].ToString());
                        totalDeductions += float.Parse(dtEmployeeSalaries.Rows[i]["Deductions"].ToString());
                        totalPFEmployee += float.Parse(dtEmployeeSalaries.Rows[i]["PFEmployee"].ToString());
                        totalPFCompany += float.Parse(dtEmployeeSalaries.Rows[i]["PFCompany"].ToString());
                        totalNetSalaries += float.Parse(dtEmployeeSalaries.Rows[i]["NetSalary"].ToString());
                        totalTaxes += float.Parse(dtEmployeeSalaries.Rows[i]["Taxes"].ToString());
                    }



                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalBasicSalaries")).Text = string.Format("{0:f2}", totalBasicSalaries);
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalAllowances")).Text = string.Format("{0:f2}", totalAllowances);
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalGrossSallay")).Text = string.Format("{0:f2}", (totalBasicSalaries + totalAllowances));
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalDeductions")).Text = string.Format("{0:f2}", totalDeductions);
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalProvidentFund")).Text = string.Format("{0:f2}", totalPFEmployee);
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalNetSalaries")).Text = "Net Amount : " + string.Format("{0:f2}", totalNetSalaries);
                    ((Label)gvSalaries.FooterRow.FindControl("txtTotalTaxes")).Text = "Total Taxes : " + string.Format("{0:f2}", totalTaxes);

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvSalaries_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    hdnRowNo.Value = e.Row.RowIndex.ToString();
                }
            }
        }
        protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {

                int index = e.RowIndex;

                dtEmployeeSalaries.Rows[index].SetField(5, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
                dtEmployeeSalaries.Rows[index].SetField(6, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditAllowances")).Text);
                dtEmployeeSalaries.Rows[index].SetField(7, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditDeductions")).Text);
                dtEmployeeSalaries.Rows[index].SetField(8, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditNetSalary")).Text);
                // dtEmployeeSalaries.Rows[index].SetField(9, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditTaxes")).Text);
                dtEmployeeSalaries.Rows[index].SetField(11, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditTaxes")).Text);
                dtEmployeeSalaries.Rows[index].SetField(12, ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditProvidentFund")).Text);




                dtEmployeeSalaries.AcceptChanges();

                gvSalaries.EditIndex = -1;
                BindSalariesTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvSalaries_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSalaries.EditIndex = -1;
            BindSalariesTable();
        }
        protected void gvSalaries_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSalaries.EditIndex = e.NewEditIndex;
            BindSalariesTable();
        }

        private void getPayrollByID(int PayrollID)
        {
            DataTable dt = new DataTable();
            objDB.PayrollID = PayrollID;
            dt = objDB.GetPayrollByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlYear.SelectedValue = dt.Rows[0]["PayrollYear"].ToString();
                    ddlMonth.SelectedValue = dt.Rows[0]["PayrollMonth"].ToString();

                    objDB.DocID = PayrollID;
                    objDB.DocType = "PayRoll";
                    ltrNotesTable.Text = objDB.GetDocNotes();

                    getPayrollDetailsByParollID(Convert.ToInt32(dt.Rows[0]["PayrollID"].ToString()));
                }
            }
            Common.addlog("View", "Finance", "Payroll of id \"" + (objDB.PayrollID).ToString() + "\" Viewed", "Payroll", objDB.PayrollID);

        }
        private void getPayrollDetailsByParollID(int payrollID)
        {
            DataTable dt = new DataTable();
            objDB.PayrollID = PayrollID;
            dt = objDB.GetPayrollDetailsByPayrollID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    SalarySrNo = 1;
                    dtEmployeeSalaries = createEmployeeSalariesTable();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtEmployeeSalaries.Rows.Add(new object[] {
                            SalarySrNo,
                            dt.Rows[i]["EmployeeCode"].ToString(),
                            dt.Rows[i]["EmployeeName"].ToString(),
                            dt.Rows[i]["DeptName"].ToString(),
                            dt.Rows[i]["DesgTitle"].ToString(),
                            dt.Rows[i]["BasicSalary"].ToString(),
                            dt.Rows[i]["Allowances"].ToString(),
                            dt.Rows[i]["Deductions"].ToString(),
                            dt.Rows[i]["NetSalary"].ToString(),
                            dt.Rows[i]["BankDetails"].ToString(),
                            dt.Rows[i]["EmployeeID"].ToString(),
                            dt.Rows[i]["Taxes"].ToString(),
                            dt.Rows[i]["PFEmployee"].ToString(),
                            dt.Rows[i]["PFCompany"].ToString(),
                            dt.Rows[i]["GrossAmount"].ToString()
                        });

                        SalarySrNo += 1;
                    }
                }
            }
            BindSalariesTable();
        }

        private void clearFields()
        {
            ddlMonth.SelectedIndex = 0;
            ddlYear.SelectedIndex = 0;
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.PayrollMonth = ddlMonth.SelectedValue;
                objDB.PayrollYear = ddlYear.SelectedValue;
                objDB.ChartOfAccountID = int.Parse(ddlCOA.SelectedValue);


                if (HttpContext.Current.Items["PayrollID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.PayrollID = PayrollID;

                    objDB.UpdatePayroll();
                    res = "Payroll Data Updated";
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();

                    PayrollID = Convert.ToInt32(objDB.AddPayroll());
                    res = "New Payroll Added";
                }


                objDB.DocType = "Payroll";
                objDB.DocID = PayrollID;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();
                objDB.PayrollID = PayrollID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeletePayrollDetails();

                dtEmployeeSalaries = (DataTable)ViewState["dtEmployeeSalaries"] as DataTable;
                if (dtEmployeeSalaries != null)
                {
                    if (dtEmployeeSalaries.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                        {
                            objDB.PayrollID = Convert.ToInt32(PayrollID);
                            objDB.EmployeeID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["EmployeeID"].ToString());
                            objDB.BasicSalary = float.Parse(dtEmployeeSalaries.Rows[i]["BasicSalary"].ToString());
                            objDB.Allownaces = float.Parse(dtEmployeeSalaries.Rows[i]["Allowances"].ToString());
                            objDB.Deductions = float.Parse(dtEmployeeSalaries.Rows[i]["Deductions"].ToString());
                            objDB.NetAmount = float.Parse(dtEmployeeSalaries.Rows[i]["NetSalary"].ToString());
                            objDB.Taxes = float.Parse(dtEmployeeSalaries.Rows[i]["Taxes"].ToString());
                            objDB.PFEmpoyeeAmount = float.Parse(dtEmployeeSalaries.Rows[i]["PFEmployee"].ToString());
                            objDB.PFCompanyAmount = float.Parse(dtEmployeeSalaries.Rows[i]["PFCompany"].ToString());
                            objDB.GrossAmount = float.Parse(dtEmployeeSalaries.Rows[i]["GrossAmount"].ToString());
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddPayrollDetails();
                        }
                    }
                }


                if (res == "New Payroll Added" || res == "Payroll Data Updated")
                {
                    if (res == "New Payroll Added") { Common.addlog("Add", "Finance", " Payroll of id\"" + (objDB.PayrollID).ToString() + "\" Added", "Payroll"); }
                    if (res == "Payroll Date Updated") { Common.addlog("Update", "Finance", "Payroll of id\"" + (objDB.PayrollID).ToString() + "\" Updated", "Payroll", objDB.PayrollID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Payroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##DATE##", DateTime.Now.ToString("dd-MMM-yyyy"));
                content = content.Replace("##MONTH##", ddlMonth.SelectedItem.Text);
                content = content.Replace("##YEAR##", ddlYear.SelectedItem.Text);

                StringBuilder sbTable = new StringBuilder();
                sbTable.Append("<table style='width:100%;'>");
                sbTable.Append("<thead><tr><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Sr. No</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Employee Code</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Employee</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Department</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Designation</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Bank Details</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Basic Salary</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Allowances</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Deductions</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Net Salary</td></tr></thead>");
                if (dtEmployeeSalaries != null)
                {
                    if (dtEmployeeSalaries.Rows.Count > 0)
                    {
                        float totalBasicSalaries = 0;
                        float totalAllowances = 0;
                        float totalDeductions = 0;
                        float totalNetSalaries = 0;
                        sbTable.Append("<tbody>");
                        for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                        {
                            sbTable.Append("<tr><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["SrNo"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["EmployeeCode"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["EmployeeName"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Department"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Designation"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["BankDetails"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["BasicSalary"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Allowances"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Deductions"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["NetSalary"] + "</td></tr>");
                            totalBasicSalaries += float.Parse(dtEmployeeSalaries.Rows[i][5].ToString());
                            totalAllowances += float.Parse(dtEmployeeSalaries.Rows[i][6].ToString());
                            totalDeductions += float.Parse(dtEmployeeSalaries.Rows[i][7].ToString());
                            totalNetSalaries += float.Parse(dtEmployeeSalaries.Rows[i][8].ToString());
                        }
                        sbTable.Append("</tbody>");
                        sbTable.Append("<tfoot>");
                        sbTable.Append("<tr><td colspan='6' style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Net Calculations:</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalBasicSalaries + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalAllowances + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalDeductions + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left; font-weight:bold;'>" + totalNetSalaries + "</td></tr>");
                        sbTable.Append("</tfoot>");
                    }
                }
                sbTable.Append("</table>");
                content = content.Replace("##TABLE##", sbTable.ToString());

                //IronPdf.HtmlToPdf Renderer = new IronPdf.HtmlToPdf();
                //Renderer.PrintOptions.Header = new HtmlHeaderFooter()
                //{
                //    HtmlFragment = header
                //};
                //Renderer.PrintOptions.Footer = new HtmlHeaderFooter()
                //{
                //    HtmlFragment = footer
                //};
                //Renderer.PrintOptions.MarginTop = 50;
                //Renderer.PrintOptions.MarginLeft = 20;
                //Renderer.PrintOptions.MarginRight = 20;
                //Renderer.PrintOptions.MarginBottom = 20;

                //Renderer.PrintOptions.FitToPaperWidth = true;

                //Renderer.RenderHtmlAsPdf(content).Print();
                Common.generatePDF(header, footer, content, "Payroll Sheet (" + ddlMonth.SelectedItem.Text + "-" + ddlYear.SelectedItem.Text + ")", "A4", "Portrait");

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "Payroll", "PayrollID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "Finance", "Payroll of ID \"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" Status Changed", "Payroll", PayrollID /* ID From Page Top  */);/* Button1_ServerClick  */
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;

                if (btn.ID.ToString() == "btnApprove" || btn.ID.ToString() == "btnRevApprove")
                {
                    if (dtEmployeeSalaries != null)
                    {
                        for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                        {

                            objDB.PFID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PFID"].ToString());
                            objDB.PFCompanyAmount = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PFCompany"].ToString());
                            objDB.PFEmpoyeeAmount = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PFEmployee"].ToString());
                            objDB.PFYear = ddlYear.SelectedValue;
                            objDB.PFMonth = ddlMonth.SelectedValue;
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddPFDeduction();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }




            CheckAccess();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Payroll", "PayrollID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "Payroll of ID \"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" deleted", "Payroll", PayrollID /* ID From Page Top  */);/* lnkDelete_Click  */

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }



        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (ddlMonth.SelectedIndex != 0 && ddlYear.SelectedIndex != 0)
                {
                    SalarySrNo = 1;
                    calculateEmployeeSalaries();
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void txtEditTaxes_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            int rowNum = Convert.ToInt32(hdnRowNo.Value);
            TextBox txtTaxes = gvSalaries.Rows[rowNum].FindControl("txtEditTaxes") as TextBox;
            string taxes = txtTaxes.Text;
            TextBox txtBasicSalary = gvSalaries.Rows[rowNum].FindControl("txtEditBasicSalary") as TextBox;
            string basicSalary = txtBasicSalary.Text;

            if (!string.IsNullOrEmpty(taxes) && !string.IsNullOrEmpty(basicSalary))
            {
                TextBox txtNetSalary = gvSalaries.Rows[rowNum].FindControl("txtEditNetSalary") as TextBox;
                string netPrice = (Convert.ToInt32(basicSalary) - Convert.ToInt32(taxes)).ToString();

                txtNetSalary.Text = netPrice;
            }

            TextBox txtSender = (TextBox)sender as TextBox;
            //txtSender.Focus();
        }

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

    }
}