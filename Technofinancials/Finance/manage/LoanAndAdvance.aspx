﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanAndAdvance.aspx.cs" Inherits="Technofinancials.Finance.Manage.LoanAndAdvance" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
         <asp:HiddenField ID="hdnPFAmount" runat="server" ClientIDMode="Static" />
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
         <%--   <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="UpdatePanel3"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>

            <div class="wrap">




                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <h1 class="m-0 text-dark">Application for Provident Fund Withdrawal</h1>
                                            
                                    </div>      
                                      <div class="col-sm-4">
                                                <div style="text-align: right;">
                                                     <asp:LinkButton class="AD_btn_inn" runat="server" ID="LinkButton1" CommandArgument='Download' OnClick="ButtonDownload_ServerClick">Application<i class="fa fa-cloud-download" aria-hidden="true"></i></asp:LinkButton>  
                                                    <asp:LinkButton class="AD_btn_inn" runat="server" ID="LinkButton2" CommandArgument='Download' OnClick="ButtonAttachments_ServerClick"> Attachments<i class="fa fa-cloud-download" aria-hidden="true"></i></asp:LinkButton>  
                                                   
                                                  <br />   
                                                    </div>
                                            </div>
                                       <br />
                                    <!-- /.col -->
                                    <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                        <ContentTemplate>
                                              
                                
                                            <div class="col-sm-4">
                                                  <br />
                                                <div style="text-align: right;">
                                                    <button class="AD_btn" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                                    <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Review</button>
                                                          <button class="AD_btn" id="btnApprove" runat="server" onserverclick="ButtonDecision_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                                    <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                                    <button class="AD_btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button><%--Disapprove--%>
                                                    <button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Reject</button>
                                                    <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Send</button><%--Send--%>
                                                    <button class="AD_btn" id="modalReject" runat="server"  data-toggle="modal" data-target="#notes-modal" validationgroup="btnValidate" type="button" title="Reject"><%--<i class="fa fa-thumbs-down"></i>--%> Reject</button>
                                                   
                                                    <button class="AD_btn" id="btnSave" runat="server" visible="false" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                  <a class="AD_btn" id="Schedule" href="../company" target="_blank" runat="server">Schedule</a>
                                                        <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                            
                             
                                            <%--        <button  runat="server" class="AD_btn" id="btnDownloadForm"  onserverclick="ButtonDownload_ServerClick" type="button" validationgroup="btnDownloadForm">
                                                         
                                            <h3 class=""  style="margin: -4px;"><i class="fa fa-cloud-download" aria-hidden="true"></i>
                                               Application </h3>
                                        </button>--%>
                                                 
                                                </div>
                                            </div>
                                               
                                            <!-- Modal -->
                                            <div class="modal fade M_set" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="m-0 text-dark">Reason</h1>
                                                <div class="add_new">
                                                    <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                                                    <button type="button" class="AD_btn" runat="server"   id="btnReject"  onserverclick="ButtonDecision_ServerClick" data-dismiss="modal">Save</button>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNote" runat="server" rows="5" placeholder="Reason.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>


                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                          <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                          <div class="col-md-6 form-group">
                                            <h4>Employee Name </h4>
                                            <input class="form-control" id="employeename" placeholder="Loan Title" readonly="true" type="text" runat="server" />
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <h4>Loan Title </h4>
                                            <input class="form-control" id="txtLoanTitle" placeholder="Loan Title" readonly="true" type="text" runat="server" />
                                        </div>
                                      <div class="col-md-6 form-group">
                                            <h4>Grant Date</h4>
                                            <input class="form-control" id="txtGrantDate" data-plugin="datetimepicker" readonly="true" placeholder="Grant Date" type="text" runat="server" data-date-format="DD-MMM-YYYY" />
                                        </div>
                                            <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Effective From  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txteffective" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txteffective" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
                                    </div>
                                </div>

                                     
                                        <div class="col-md-6 form-group">
                                            <h4>PF Withdrawal Type </h4>
                                      <input class="form-control" id="withdrawltype"  readonly="true" runat="server" />
                                     
                                        </div>
                                        <div style="display:none;" class="col-md-6 form-group">
                                            <h4> Currency </h4>
                                   <input class="form-control" id="Currency"  readonly="true" runat="server" />
                                        </div>
                                           <div class="col-md-6 form-group">
                                            <h4>Loan Amount </h4>
                                            <input class="form-control" id="txtLoanAmount" placeholder="Loan Amount" type="number" readonly="true" runat="server" />
                                        </div>
                                           <div class="col-md-6 form-group">
                                            <h4>Remaining Amount  </h4>
                                            <input class="form-control" id="Remamount" placeholder="Loan Amount" type="number" readonly="true" runat="server" />
                                        </div>
                                           <div class="col-md-6 form-group">
                                            <h4>No of Installments</h4>
                                            <input class="form-control" id="txtNoOfInstallments" placeholder="No of Installments" readonly="true" type="number" runat="server" />
                                        </div>
                                         <div class="col-md-6 form-group">
                                            <h4>Remaining Installments</h4>
                                            <input class="form-control" id="remInstallment" placeholder="No of Installments" readonly="true" type="number" runat="server" />
                                        </div>
                                        <div style="display:none;" class="col-md-6 form-group">
                                            <h4>Interest %</h4>
                                            <input class="form-control" readonly="true" id="intrstper"  type="number" runat="server" />
                                        </div>
                                          <div style="display:none;" class="col-md-6 form-group">
                                            <h4>Interest Amount</h4>
                                            <input class="form-control" readonly="true" id="intrst"  type="number" runat="server" />
                                        </div>
                                         <div class="col-sm-6 form-group">
												<div class="checkbox checkbox-primary">
                                                    <br />
															<input type="checkbox" id="allowdeduction" readonly="true" runat="server" name="returnedLockerKeys" />
															<label for="allowdeduction">Allow Deduction</label>	
											</div>		
								
							</div>	
                                       
                                          <div style="display:none;" class="col-md-6 form-group">
                                            <h4>Zakat Amount</h4>
                                            <input class="form-control" readonly="true" id="zakatamount"  type="number" runat="server" />
                                        </div>    <div class="col-sm-6 form-group">
												<div class="checkbox checkbox-primary">
                                                    <br />
															<input type="checkbox" id="zakatdeduct" disabled="disabled" runat="server" name="returnedLockerKeys" />
															<label for="zakatdeduct">Zakat Deduction</label>	
											</div>		
								
							</div>	
                                        <div   class="col-md-12">
                                            <div>
                                                <h4 style="font-weight: 600;">Purpose
                                                </h4>
                                           <textarea class="form-control" id="mainpurpose"  type="text" readonly="" runat="server" />
                                            </div>
                                        </div>
                                          <div   class="col-md-12">
                                            <div>
                                                <h4 style="font-weight: 600;">Sub Purpose
                                                </h4>
                                           <textarea class="form-control" id="subpurpose"  type="text" readonly="" runat="server" />
                                            </div>
                                        </div>
                                   
                                        <div class="col-md-12 form-group">
                                            <h4>Notes</h4>
                                            <textarea class="form-control" id="txtNotes" placeholder="Notes" type="text" runat="server" />
                                        </div>
                                              </ContentTemplate>
                                
                                </asp:UpdatePanel>
                                    </div>
                                </div>
                                  <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                            <span>
                                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                            </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                 <div  class="col-lg-8 col-md-8 col-sm-12" >
                                     <div class="row">
                                         <div class="col-md-8">
                         <asp:UpdatePanel ID="SalaryPanel" runat="server">
                                    <ContentTemplate>
                                                <div>
                                        <div class="">
                                             <div class="form-group">
                                        <h4>Provident Fund Detail</h4>
                                    </div>
                                           
                                            <asp:GridView ID="gvPFDetails" runat="server" CssClass="table table-bordered" ClientIDMode="Static" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
                                            <Columns>
                                              

                                                <asp:TemplateField HeaderText="EmployeePF">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("EmployeePF") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CompanyPF">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("CompanyPF")  %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="WithdrawAmount">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("WithdrawAmount")  %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Amount">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("RemaningAmount")  %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            


                                            </Columns>
                                        </asp:GridView>
                                           
                                            </div>
                                            </div> 
                            
                                        </ContentTemplate>
                                
                                </asp:UpdatePanel>
                                             </div>
                                         </div>
                                      <div class="row"><br /></div>
                                 
  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                          <div class="row">
                               <div class="">
                        
                                       
                                       <div class="col-md-8">
                                          
                                           <div class="form-group">
                                        <h4>Required Documents</h4>
                                    </div>
                                           
                                        
                                               
                                            <asp:GridView ID="RequiredDocs" runat="server" CssClass="table table-bordered" ClientIDMode="Static" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
                                            <Columns>
                                              

                                                <asp:TemplateField HeaderText="Document">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("Attachmentname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                      <asp:TemplateField HeaderText="Attachment">
                                                        <ItemTemplate>
                                                            <a runat="server" target="_blank" class="AD_stock_inn"  visible='<%#(Eval("FilePath")) == "" ? false: true %>'  id="lblDownload" href='<%# Eval("FilePath") %>'> Download <i class="fa fa-download" aria-hidden="true"></i></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                            


                                            </Columns>
                                        </asp:GridView>
                                          
                                       </div>
                                     
                                      
                                       
                                 
                               </div>
                             
                              
                              
                            </div>
                                         <div class="row"><br /></div>

                                              <div class="row">
                               <div class="">
                        
                                       
                                       <div class="col-md-8">
                                          
                                           <div class="form-group">
                                        <h4>Transactions</h4>
                                    </div>
                                           
                                        
                                               
                                            <asp:GridView ID="trans" runat="server" CssClass="table table-bordered" ClientIDMode="Static" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
                                            <Columns>
                                              

                                                <asp:TemplateField HeaderText="Transaction ID">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("transid") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Payroll Month">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("payrollmonth") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Amount Deduct">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("deductamount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 
                                            


                                            </Columns>
                                        </asp:GridView>
                                          
                                       </div>
                                 
                                      
                                       
                                 
                               </div>
                             
                              
                              
                            </div>
                        </ContentTemplate>
        
                                         <%-- <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAddExpDetails" />
                                    <asp:PostBackTrigger ControlID="btnUpdateExpDetails" />
                                </Triggers>--%>

                    </asp:UpdatePanel>
                               
                            </div> 
                              
                            </div>
                        </section>
                  
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
        </style>
        <!-- Modal -->
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>



		</script>
    </form>
    <style>


    </style>
</body>
</html>
