﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayrollUsingUploadedAttendance.aspx.cs" Inherits="Technofinancials.Finance.manage.PayrollUsingUploadedAttendance" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>

    	button#btnView {
    		padding: 3px 24px;
    		margin-top: 3px !important;
    	}
        
        .AD_btnn.two {
            font-size: 14px !important;
            margin: 0 !important;
            padding: 0 !important;
            cursor: pointer;
        }

        .AD_btnn {
            margin: 0 10px;
            font-size: 18px;
            font-weight: 700;
            color: #003780;
            background: none;
            border: 0;
            border-bottom: 2px solid transparent !important;
            border-radius: 0;
            padding: 5px;
        }

            .AD_btnn:hover {
                color: #000;
                border-bottom: 2px solid #000 !important;
            }
        
        .content-header h1 {
            margin-left: 8px !important;
        }

        .form-group {
            display: block;
        }

        .table-bordered > tbody > tr > th {
            border-top-style: none !important;
        }

        .content-header h1 {
            font-size: 20px !important;
        }

        .wrap.p-t-0.tf-navbar.tf-footer-wrapper {
            right: 0 !important;
        }

        g[aria-labelledby="id-66-title"] {
            display: none;
        }

    	@media only screen and (max-width: 991px) and (min-width: 768px) {
    		.container-fuild div {
    			overflow-x: scroll;
    			overflow-y: hidden;
    			padding-bottom: 35px;
    		}

    		.container-fuild div::-webkit-scrollbar-track {
    			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    			background-color: #FFF;
    		}

    		.container-fuild div::-webkit-scrollbar {
    			height: 10px;
    			background-color: #ffffff;
    		}

    		.container-fuild div::-webkit-scrollbar-thumb {
    			background-color: #003780;
    			border: 2px solid #003780;
    			border-radius: 10px;
    		}
    	}

    	@media only screen and (max-width: 1024px) and (min-width: 992px) {
    		.container-fuild div {
    			overflow-x: scroll;
    			overflow-y: hidden;
    			padding-bottom: 35px;
    		}

    		.container-fuild div::-webkit-scrollbar-track {
    			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    			background-color: #FFF;
    		}

    		.container-fuild div::-webkit-scrollbar {
    			height: 10px;
    			background-color: #ffffff;
    		}

    		.container-fuild div::-webkit-scrollbar-thumb {
    			background-color: #003780;
    			border: 2px solid #003780;
    			border-radius: 10px;
    		}
    	}

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }
        }

        .mini-stat.clearfix.present {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #1fb5ac !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            margin-top: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.absent {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #fa8564 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.leave {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #a48ad4 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.holiday {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f4b9b9 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.missing {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f9c851 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }
        .mini-stat.clearfix.short {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #aec785 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .AD_btn {
            font-size: 16px;
            border: 0;
            padding: 0px;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
          
            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">Payrolls </h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div style="text-align: right;">
                                    <button class="AD_btn" id="btnPDFOvertime" runat="server" onserverclick="btnPDFOverTime_ServerClick" style="display: none;" type="button">Overtime Sheet</button>
                                    <button class="AD_btn" id="btnRevApproveByHR" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Paycycle Lock</button>
                                    <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                    <button class="AD_btn" id="btnApproveByHR" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Paycycle Lock</button>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                    <button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Dis Approve</button>
                                    <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                    <button class="AD_btn"  id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                    <button class="AD_btn"  id="btnReleaseActiveEmployees" runat="server" onserverclick="btnReleaseActiveEmployees_ServerClick" validationgroup="btnValidate" type="button">Disburse</button>
                                    <button class="AD_btn"  id="downloadpayroll" runat="server" onserverclick="btnPayroll_ServerClick" validationgroup="btnValidate" type="button">Download Payroll</button>
                                      <button class="AD_btn"  id="noPayroll" runat="server" onserverclick="btnnoPayroll_ServerClick" validationgroup="btnValidate" type="button">No Payroll</button>
                                   
                                    <a class="AD_btn" id="btnFnFSettlement" runat="server">FnF Settlement</a>

                                    <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
                    
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Payroll Date  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPayrollDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <h4>&nbsp;</h4>
                                    <button class="AD_btn_inn" id="btnView" runat="server" onserverclick="txtPayrollDate_ServerChange" type="button">View</button>
                                <label>Total NoPayroll:&nbsp;&nbsp;  </label><label id="noPayrollCount" runat="server" >0</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label>Total Payroll:&nbsp;&nbsp;  </label><label id="PayrollCount" runat="server" >0</label>
                                    </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12"></div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                          <div class="row">
                             <div class="col-md-12">
                           <div class="tab-content">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                       <asp:GridView ID="gvSalaries" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" ShowFooter="true" AutoGenerateColumns="false" OnRowUpdating="gvSalaries_RowUpdating" OnRowCancelingEdit="gvSalaries_RowCancelingEdit" OnRowEditing="gvSalaries_RowEditing">

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("WORKDAYID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="NetSalary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="NetSalary" Text='<%# Eval("NetSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                       
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Salary/Month">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeSalary" Text='<%# Eval("GrossSal","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Present Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="SalaryOfDays" Text='<%# Eval("SalaryOfDays","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Over Time">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTime" Text='<%# Eval("TotalOverTimeAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Increment Of Previous Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="TotalIncrementedAmount" Text='<%# Eval("TotalIncrementedAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Decrement Of Previous Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="TotalDecrementedAmount" Text='<%# Eval("TotalDecrementedAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Commission">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Arrears">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Arrears_" Text='<%# Eval("Arrears_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Spiffs">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Severance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Severance_" Text='<%# Eval("Severance_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reimbursement">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Reimbursement_" Text='<%# Eval("Reimbursement_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="LeaveEncashment">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LeaveEncashment_" Text='<%# Eval("LeaveEncashment_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Previous Days Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="PreviousDaysAmount_" Text='<%# Eval("PreviousDaysAmount_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Expenses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="ExpenseReimb_" Text='<%# Eval("ExpenseReimb_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bonuses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("BonusTotal_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     
                                    <asp:TemplateField HeaderText="Gratuity">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Gratuity_" Text='<%# Eval("Gratuity_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EOBI">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="EOBI_" Text='<%# Eval("EOBI_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    
                                    <asp:TemplateField HeaderText="PF">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="ProvidentFund" Text='<%# Eval("ProvidentFund","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                 
                                    </asp:TemplateField>

                                  

                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="tax" Text='<%# Eval("MonthlyTax","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Additional tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("AdditionalTaxperMonth","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("AdditionalTaxperMonth") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Total Gross Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("TotalGrossAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                     
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Deductions">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("TotalDeductions","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NetSalary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="NetSalary" Text='<%# Eval("NetSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                       
                                    </asp:TemplateField>

                                

                                
                                </Columns>
                            </asp:GridView>


<%--
                            <asp:GridView ID="gvSalaries" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" ShowFooter="true" AutoGenerateColumns="false" OnRowUpdating="gvSalaries_RowUpdating" OnRowCancelingEdit="gvSalaries_RowCancelingEdit" OnRowEditing="gvSalaries_RowEditing">

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeSalary" Text='<%# Eval("EmployeeSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Over Time">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTime" Text='<%# Eval("OverTimeAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Commission">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Spiffs">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Bonuses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Gratuity">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOtherExpenses" Text='<%# Eval("Gratuity","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Other Expenses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOtherExpenses" Text='<%# Eval("OtherExpenses","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Gross Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("GrossSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditBasicSalary" CssClass="form-control" Text='<%# Eval("GrossSalary") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Deductions">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("Deductions","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Absent Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAbsentAmount" Text='<%# Eval("AbsentAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Tax","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("Tax") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Variable Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("AdditionalTax","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("AdditionalTax") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="E.O.B.I">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEOBI" Text='<%# Eval("EOBI","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditEOBI" CssClass="form-control" Text='<%# Eval("EOBI") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Provident Fund">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeePF" Text='<%# Eval("EmployeePF","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditEmployeePF" CssClass="form-control" Text='<%# Eval("EmployeePF") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Net Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("NetSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                     --%>
                                    
                                </div>
                                </div>
                               </div>
                            </div>
                        </div>

                    <div class="row" style="display:none;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                   <asp:GridView ID="gvNormalSal" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
     <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("WORKDAYID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
             <asp:TemplateField HeaderText="NetSalary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="NetSalary" Text='<%# Eval("NetSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                       
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Salary/Month">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeSalary" Text='<%# Eval("GrossSal","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Present Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="SalaryOfDays" Text='<%# Eval("SalaryOfDays","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Over Time">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTime" Text='<%# Eval("TotalOverTimeAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Increment Of Previous Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="TotalIncrementedAmount" Text='<%# Eval("TotalIncrementedAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
           <asp:TemplateField HeaderText="Decrement Of Previous Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="TotalDecrementedAmount" Text='<%# Eval("TotalDecrementedAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Commission">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Arrears">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Arrears_" Text='<%# Eval("Arrears_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Spiffs">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Severance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Severance_" Text='<%# Eval("Severance_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reimbursement">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Reimbursement_" Text='<%# Eval("Reimbursement_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="LeaveEncashment">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LeaveEncashment_" Text='<%# Eval("LeaveEncashment_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Previous Days Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="PreviousDaysAmount_" Text='<%# Eval("PreviousDaysAmount_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Expenses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="ExpenseReimb_" Text='<%# Eval("ExpenseReimb_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bonuses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("BonusTotal_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     
                                    <asp:TemplateField HeaderText="Gratuity">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Gratuity_" Text='<%# Eval("Gratuity_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EOBI">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="EOBI_" Text='<%# Eval("EOBI_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    
                                    <asp:TemplateField HeaderText="PF">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="ProvidentFund" Text='<%# Eval("ProvidentFund","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                 
                                    </asp:TemplateField>

                                  

                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="tax" Text='<%# Eval("MonthlyTax","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Additional tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("AdditionalTaxperMonth","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("AdditionalTaxperMonth") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Total Gross Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("TotalGrossAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                     
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Deductions">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("TotalDeductions","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                

                                

                                
                                </Columns>
                            </asp:GridView>

<%--
                            <asp:GridView ID="gvNormalSal" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("EmployeeSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Over Time Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTimeAmount" Text='<%# Eval("OverTimeAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Commission">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Spiffs">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Bonuses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("TotalBonus","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Other Expenses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOtherExpenses" Text='<%# Eval("OtherExpenses","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Gross Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblGrossSallary" Text='<%# Eval("GrossSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Deductions">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("Deductions","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Absent Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAbsentAmount" Text='<%# Eval("AbsentAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Tax","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="E.O.B.I">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEOBI" Text='<%# Eval("EOBI","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Provident Fund">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeePF" Text='<%# Eval("EmployeePF","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("NetSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        --%>
                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <asp:GridView ID="gvOverTime" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Basic Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("BasicSalary") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Over Time Hours">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTime" Text='<%# Eval("OverTime") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Over Time Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTimeAmount" Text='<%# Eval("OverTimeAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Food Allowance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblFoodAllowence" Text='<%# Eval("FoodAllowence") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Maintenance Allowance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblMaintenance" Text='<%# Eval("Maintenance") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fuel Allowance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblFuel" Text='<%# Eval("Fuel") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAbsentAmount" Text='<%# Eval("OverTimeTotal") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
				</section>

			</div>
			<!-- #dash-content -->
			<div class="clearfix">&nbsp;</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>

	</form>
</body>
</html>
