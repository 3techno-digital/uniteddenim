﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class CDMS : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        static string logoPath = "/assets/images/3techno-Logo.png";
        protected int CID
        {
            get
            {
                if (ViewState["CID"] != null)
                {
                    return (int)ViewState["CID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["CID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                ViewState["CID"] = null;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;
                leadsContainer.Visible = false;
                BindCOA();
                BindCountriesDropDown();
                BindStatesDropDown(166);
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/cdms";

                clearFields();
                divAlertMsg.Visible = false;
                btnAddLead.Visible = false;
                if (HttpContext.Current.Items["CDMSID"] != null)
                {
                    CID = Convert.ToInt32(HttpContext.Current.Items["CDMSID"].ToString());
                    GetClientByID(CID);
                    CheckAccess();

                }
            }
        }
        private void BindCOA()
        {
            //objDB.CategoriesList = "\'Asset\'";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOA.Items.Clear();
            //ddlCOA.DataSource = objDB.GetCOAByCategory(ref errorMsg);
            ddlCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("--- Select COA- --", "0"));
        }
        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "Clients";
                objDB.PrimaryColumnnName = "ClientID";
                objDB.PrimaryColumnValue = CID.ToString();
                objDB.DocName = "CDMS";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void GetClientByID(int ClientID)
        {
            DataTable dt = new DataTable();
            objDB.ClientID = ClientID;
            dt = objDB.GetClientByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtCompanyName.Value = dt.Rows[0]["CompanyName"].ToString();
                    txtCompanyShortName.Value = dt.Rows[0]["CompanyShortName"].ToString();
                    txtCompanyDescription.Value = dt.Rows[0]["CompanyDesc"].ToString();

                    imgLogo.Src = dt.Rows[0]["CompanyLogo"].ToString();
                    imgLogo.Attributes["src"] = dt.Rows[0]["CompanyLogo"].ToString();
                    logoPath = dt.Rows[0]["CompanyLogo"].ToString();

                    txtEmail.Value = dt.Rows[0]["CompanyEmail"].ToString();
                    txtPhoneNumber1.Value = dt.Rows[0]["CompanyPhone1"].ToString();
                    txtPhoneNumber2.Value = dt.Rows[0]["CompanyPhone2"].ToString();

                    txtAddressLine1.Value = dt.Rows[0]["Address1"].ToString();
                    txtAddressLine2.Value = dt.Rows[0]["Address2"].ToString();
                    txtZIPCode.Value = dt.Rows[0]["ZIPCode"].ToString();
                    
                    ddlCOA.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                    txtBusinessName.Value = dt.Rows[0]["Business"].ToString();
                    txtNTN.Content = dt.Rows[0]["NTN"].ToString();
                    txtBankName.Value = dt.Rows[0]["BankName"].ToString();
                    txtAccountNo.Value = dt.Rows[0]["AccountNo"].ToString();
                    


                    ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));
                    int countriesIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));
                    if (countriesIndex == -1)
                        countriesIndex = 0;
                    BindStatesDropDown(Convert.ToInt32(ddlCountries.Items[countriesIndex].Value));

                    ddlStates.SelectedIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["State"].ToString()));
                    int statesIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["State"].ToString()));
                    if (statesIndex == -1)
                        statesIndex = 0;

                    BindCitiesDropDown(Convert.ToInt32(ddlStates.Items[statesIndex].Value));

                    ddlCities.SelectedIndex = ddlCities.Items.IndexOf(ddlCities.Items.FindByText(dt.Rows[0]["City"].ToString()));



                    txtFPFirstName.Value = dt.Rows[0]["FirstName"].ToString();
                    txtFPMiddleName.Value = dt.Rows[0]["MiddleName"].ToString();
                    txtFPLastName.Value = dt.Rows[0]["LastName"].ToString();
                    txtFPPhoneNumber.Value = dt.Rows[0]["PhoneNumber"].ToString();
                    txtFPEmail.Value = dt.Rows[0]["Email"].ToString();

                    if (dt.Rows[0]["DocStatus"].ToString() == "Approved")
                    {
                        btnAddLead.Visible = true;
                        btnAddLead.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/crm/manage/leads/add-lead-by-clientid-" + CID;
                    }
                    else
                    {
                        btnAddLead.Visible = false;
                    }

                    GetLeadsByClientID(ClientID);


                }
            }
            Common.addlog("View", "Finance", "Client \"" + txtFPFirstName.Value + " " + txtFPMiddleName.Value + " " + txtFPLastName.Value+ "\" Viewed", "Clients", objDB.ClientID);

        }



        private void GetLeadsByClientID(int ClientID)
        {
            leadsContainer.Visible = false;
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.ClientID = ClientID;
            dt = objDB.GetAllLeadsByClientID(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    leadsContainer.Visible = true;
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }


        private void clearFields()
        {
            ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText("Pakistan"));
            int countriesIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText("Pakistan"));
            if (countriesIndex == -1)
                countriesIndex = 0;
            BindStatesDropDown(Convert.ToInt32(ddlCountries.Items[countriesIndex].Value));

            ddlStates.SelectedIndex = 0;
            ddlCities.SelectedIndex = 0;

            txtAddressLine1.Value = string.Empty;
            txtAddressLine2.Value = string.Empty;
            txtZIPCode.Value = string.Empty;

            txtCompanyName.Value = string.Empty;
            txtCompanyShortName.Value = string.Empty;
            txtCompanyDescription.Value = string.Empty;
            txtPhoneNumber1.Value = string.Empty;
            txtPhoneNumber2.Value = string.Empty;
            txtEmail.Value = string.Empty;


            txtFPFirstName.Value = string.Empty;
            txtFPLastName.Value = string.Empty;
            txtFPMiddleName.Value = string.Empty;
            txtFPEmail.Value = string.Empty;
            txtFPPhoneNumber.Value = string.Empty;



        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }


        private void BindCountriesDropDown()
        {
            ddlCountries.DataSource = objDB.GetAllCounties(ref errorMsg);
            ddlCountries.DataTextField = "Name";
            ddlCountries.DataValueField = "ID";
            ddlCountries.DataBind();
            ddlCountries.Items.Insert(0, new ListItem("--- Select Country ---", "0"));
            ddlCountries.SelectedValue = "166";
        }

        private void BindStatesDropDown(int CountryID)
        {
            objDB.CountryID = CountryID;
            ddlStates.DataSource = objDB.GetAllStatesByCountryID(ref errorMsg);
            ddlStates.DataTextField = "Name";
            ddlStates.DataValueField = "ID";
            ddlStates.DataBind();
            ddlStates.Items.Insert(0, new ListItem("--- Select State ---", "0"));
        }

        private void BindCitiesDropDown(int StateID)
        {
            objDB.StateID = StateID;
            ddlCities.DataSource = objDB.GetAllCitiesByStateID(ref errorMsg);
            ddlCities.DataTextField = "Name";
            ddlCities.DataValueField = "Name";
            ddlCities.DataBind();
            ddlCities.Items.Insert(0, new ListItem("--- Select City ---", "0"));
        }

        protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            BindCitiesDropDown(Convert.ToInt32(ddlStates.SelectedItem.Value));

        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            BindStatesDropDown(Convert.ToInt32(ddlCountries.SelectedItem.Value));

        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            string res = "";
            if (updLogo.HasFile)
            {
                Random rand = new Random((int)DateTime.Now.Ticks);
                int randnum = 0;

                string fn = "";
                string exten = "";

                string destDir = Server.MapPath("~/assets/files/" + Session["CompanyID"].ToString() + "/Clients_logos/");
                randnum = rand.Next(1, 100000);
                fn = Common.RemoveSpecialCharacter(txtCompanyName.Value).ToLower().Replace(" ", "-") + "_" + randnum;

                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }

                string fname = Path.GetFileName(updLogo.PostedFile.FileName);
                exten = Path.GetExtension(updLogo.PostedFile.FileName);
                updLogo.PostedFile.SaveAs(destDir + fn + exten);

                logoPath = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/" + Session["CompanyID"].ToString() + "/Clients_logos/" + fn + exten;
            }

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            objDB.FirstName = txtFPFirstName.Value;
            objDB.MiddleName = txtFPMiddleName.Value;
            objDB.LastName = txtFPLastName.Value;

            objDB.ContactNo = txtFPPhoneNumber.Value;
            objDB.Email = txtFPEmail.Value;
            objDB.CountryName = ddlCountries.SelectedItem.Text;
            objDB.StateName = ddlStates.SelectedItem.Text;
            objDB.CityName = ddlCities.SelectedItem.Text;
            objDB.AddressLine1 = txtAddressLine1.Value;
            objDB.AddressLine2 = txtAddressLine2.Value;
            objDB.ZIPCode = txtZIPCode.Value;
            objDB.UserID = Convert.ToInt32(Session["UserID"].ToString());

            objDB.BusinessName = txtBusinessName.Value;
            objDB.NTN = txtNTN.Content;
            objDB.AccountNo = txtAccountNo.Value;
            objDB.BankName = txtBankName.Value;
            objDB.CompanyName = txtCompanyName.Value;
            objDB.CompanyShortName = txtCompanyShortName.Value;
            objDB.CompanyDesc = txtCompanyDescription.Value;
            objDB.CompanyEmail = txtEmail.Value;
            objDB.CompanyPhone1 = txtPhoneNumber1.Value;
            objDB.CompanyPhone2 = txtPhoneNumber2.Value;
            objDB.CompanyLogo = logoPath;
            objDB.ChartOfAccountID = int.Parse(ddlCOA.SelectedValue);

            int Docid = 0;
            if (HttpContext.Current.Items["CDMSID"] != null)
            {
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.ClientID = CID;
                objDB.UpdateClient();
                Docid = Convert.ToInt32(HttpContext.Current.Items["CDMSID"].ToString());
                res = "Client Data Updated";
            }
            else
            {
                objDB.CreatedBy = Session["UserName"].ToString();
                Docid = Convert.ToInt32(objDB.AddClient());
                clearFields();
                res = "New Client Added";
            }

            imgLogo.Src = logoPath;

            objDB.DocType = "Clients";
            objDB.DocID = Docid;
            objDB.Notes = txtNotes.Value;
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.AddDocNotes();

            if (res == "New Client Added" || res == "Client Data Updated")
            {
                if (res == "New Client Added") { Common.addlog("Add", "Finance", " Client \"" + objDB.FirstName + " " + objDB.MiddleName + " " + objDB.LastName + "\" Added", "Clients"); }
                if (res == "Client Updated") { Common.addlog("Update", "Finance", "Client\"" + objDB.FirstName + " " + objDB.MiddleName + " " + objDB.LastName + "\" Updated", "Clients", objDB.ClientID); }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }



        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "Clients", "ClientID", HttpContext.Current.Items["CDMSID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "Finance", "CDMs of ID \"" + HttpContext.Current.Items["CDMSID"].ToString() + "\" Status Changed", "Clients", CID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Clients", "ClientID", HttpContext.Current.Items["CDMSID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "CDMs of ID \"" + HttpContext.Current.Items["CDMSID"].ToString() + "\" deleted", "Clients", CID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


    }
}