﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class Shifts : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected static int ShiftID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                try
                { 
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/shift-lists";

                    divAlertMsg.Visible = false;
                    clearFields();
                    if (HttpContext.Current.Items["ShiftID"] != null)
                    {
                        ShiftID = Convert.ToInt32(HttpContext.Current.Items["ShiftID"].ToString());
                        getShiftsByID(ShiftID);
                        CheckAccess();
                    }
                }

                catch (Exception ex)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = ex.Message;
                }

            }
        }
        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "WorkingShifts";
                objDB.PrimaryColumnnName = "ShiftID";
                objDB.PrimaryColumnValue = ShiftID.ToString();
                objDB.DocName = "Shifts";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;
                    btnRejDisApprove.Visible = true;

                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getShiftsByID(int ShiftID)
        {
            try
            {

                DataTable dt, dtday = new DataTable();
                objDB.WorkingShiftID = ShiftID;
                dt = objDB.GetWorkingShiftByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtShiftTitle.Value = dt.Rows[0]["ShiftName"].ToString();
                        //txtStartTime.Value = dt.Rows[0]["StartTime"].ToString();
                        //txtLateIn.Value = dt.Rows[0]["LateInTime"].ToString();
                        //txtHalfDay.Value = dt.Rows[0]["HalfDayStart"].ToString();
                        //txtEarlyOut.Value = dt.Rows[0]["EarlyOut"].ToString();
                        //txtEndTime.Value = dt.Rows[0]["EndTime"].ToString();
                        chkMonday.Checked = getCheckBoxValue(dt.Rows[0]["Monday"].ToString());
                        chkTuesday.Checked = getCheckBoxValue(dt.Rows[0]["Tuesday"].ToString());
                        chkWednesdat.Checked = getCheckBoxValue(dt.Rows[0]["Wednesday"].ToString());
                        chkThursday.Checked = getCheckBoxValue(dt.Rows[0]["Thursday"].ToString());
                        chkFriday.Checked = getCheckBoxValue(dt.Rows[0]["Friday"].ToString());
                        chkSaturday.Checked = getCheckBoxValue(dt.Rows[0]["Saturday"].ToString());
                        chkSunday.Checked = getCheckBoxValue(dt.Rows[0]["Sunday"].ToString());

                        objDB.DocID = ShiftID;
                        objDB.DocType = "Shifts";
                        ltrNotesTable.Text = objDB.GetDocNotes();
                    }
                }
                dtday = objDB.GetWorkingShiftDaysByShiftID(ref errorMsg);
                if (dtday != null && dtday.Rows.Count > 0)
                {
                    for (int i = 0; i < dtday.Rows.Count; i++)
                    {
                        if (dtday.Rows[i]["DayName"].ToString() == "Monday")
                        {
                            txtMONStartTime.Value = dtday.Rows[i]["StartTime"].ToString();
                            txtMONLateInTime.Value = dtday.Rows[i]["LateInTime"].ToString();
                            txtMONBreakEarlyStartTime.Value = dtday.Rows[i]["BreakEarlyStartTime"].ToString();
                            txtMONBreakStartTime.Value = dtday.Rows[i]["BreakStartTime"].ToString();
                            txtMONBreakEndTime.Value = dtday.Rows[i]["BreakEndTime"].ToString();
                            txtMONBreakLateTime.Value = dtday.Rows[i]["BreakLateTime"].ToString();
                            txtMONHalfDayStart.Value = dtday.Rows[i]["HalfDayStart"].ToString();
                            txtMONEarlyOut.Value = dtday.Rows[i]["EarlyOut"].ToString();
                            txtMONEndTime.Value = dtday.Rows[i]["EndTime"].ToString();
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Tuesday")
                        {
                            txtTUEStartTime.Value = dtday.Rows[i]["StartTime"].ToString();
                            txtTUELateInTime.Value = dtday.Rows[i]["LateInTime"].ToString();
                            txtTUEBreakEarlyStartTime.Value = dtday.Rows[i]["BreakEarlyStartTime"].ToString();
                            txtTUEBreakStartTime.Value = dtday.Rows[i]["BreakStartTime"].ToString();
                            txtTUEBreakEndTime.Value = dtday.Rows[i]["BreakEndTime"].ToString();
                            txtTUEBreakLateTime.Value = dtday.Rows[i]["BreakLateTime"].ToString();
                            txtTUEHalfDayStart.Value = dtday.Rows[i]["HalfDayStart"].ToString();
                            txtTUEEarlyOut.Value = dtday.Rows[i]["EarlyOut"].ToString();
                            txtTUEEndTime.Value = dtday.Rows[i]["EndTime"].ToString();
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Wednesday")
                        {
                            txtWEDStartTime.Value = dtday.Rows[i]["StartTime"].ToString();
                            txtWEDLateInTime.Value = dtday.Rows[i]["LateInTime"].ToString();
                            txtWEDBreakEarlyStartTime.Value = dtday.Rows[i]["BreakEarlyStartTime"].ToString();
                            txtWEDBreakStartTime.Value = dtday.Rows[i]["BreakStartTime"].ToString();
                            txtWEDBreakEndTime.Value = dtday.Rows[i]["BreakEndTime"].ToString();
                            txtWEDBreakLateTime.Value = dtday.Rows[i]["BreakLateTime"].ToString();
                            txtWEDHalfDayStart.Value = dtday.Rows[i]["HalfDayStart"].ToString();
                            txtWEDEarlyOut.Value = dtday.Rows[i]["EarlyOut"].ToString();
                            txtWEDEndTime.Value = dtday.Rows[i]["EndTime"].ToString();
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Thursday")
                        {
                            txtTHUStartTime.Value = dtday.Rows[i]["StartTime"].ToString();
                            txtTHULateInTime.Value = dtday.Rows[i]["LateInTime"].ToString();
                            txtTHUBreakEarlyStartTime.Value = dtday.Rows[i]["BreakEarlyStartTime"].ToString();
                            txtTHUBreakStartTime.Value = dtday.Rows[i]["BreakStartTime"].ToString();
                            txtTHUBreakEndTime.Value = dtday.Rows[i]["BreakEndTime"].ToString();
                            txtTHUBreakLateTime.Value = dtday.Rows[i]["BreakLateTime"].ToString();
                            txtTHUHalfDayStart.Value = dtday.Rows[i]["HalfDayStart"].ToString();
                            txtTHUEarlyOut.Value = dtday.Rows[i]["EarlyOut"].ToString();
                            txtTHUEndTime.Value = dtday.Rows[i]["EndTime"].ToString();

                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Friday")
                        {
                            txtFRIStartTime.Value = dtday.Rows[i]["StartTime"].ToString();
                            txtFRILateInTime.Value = dtday.Rows[i]["LateInTime"].ToString();
                            txtFRIBreakEarlyStartTime.Value = dtday.Rows[i]["BreakEarlyStartTime"].ToString();
                            txtFRIBreakStartTime.Value = dtday.Rows[i]["BreakStartTime"].ToString();
                            txtFRIBreakEndTime.Value = dtday.Rows[i]["BreakEndTime"].ToString();
                            txtFRIBreakLateTime.Value = dtday.Rows[i]["BreakLateTime"].ToString();
                            txtFRIHalfDayStart.Value = dtday.Rows[i]["HalfDayStart"].ToString();
                            txtFRIEarlyOut.Value = dtday.Rows[i]["EarlyOut"].ToString();
                            txtFRIEndTime.Value = dtday.Rows[i]["EndTime"].ToString();
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Saturday")
                        {
                            txtSATStartTime.Value = dtday.Rows[i]["StartTime"].ToString();
                            txtSATLateInTime.Value = dtday.Rows[i]["LateInTime"].ToString();
                            txtSATBreakEarlyStartTime.Value = dtday.Rows[i]["BreakEarlyStartTime"].ToString();
                            txtSATBreakStartTime.Value = dtday.Rows[i]["BreakStartTime"].ToString();
                            txtSATBreakEndTime.Value = dtday.Rows[i]["BreakEndTime"].ToString();
                            txtSATBreakLateTime.Value = dtday.Rows[i]["BreakLateTime"].ToString();
                            txtSATHalfDayStart.Value = dtday.Rows[i]["HalfDayStart"].ToString();
                            txtSATEarlyOut.Value = dtday.Rows[i]["EarlyOut"].ToString();
                            txtSATEndTime.Value = dtday.Rows[i]["EndTime"].ToString();
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Sunday")
                        {
                            txtSUNStartTime.Value = dtday.Rows[i]["StartTime"].ToString();
                            txtSUNLateInTime.Value = dtday.Rows[i]["LateInTime"].ToString();
                            txtSUNBreakEarlyStartTime.Value = dtday.Rows[i]["BreakEarlyStartTime"].ToString();
                            txtSUNBreakStartTime.Value = dtday.Rows[i]["BreakStartTime"].ToString();
                            txtSUNBreakEndTime.Value = dtday.Rows[i]["BreakEndTime"].ToString();
                            txtSUNBreakLateTime.Value = dtday.Rows[i]["BreakLateTime"].ToString();
                            txtSUNHalfDayStart.Value = dtday.Rows[i]["HalfDayStart"].ToString();
                            txtSUNEarlyOut.Value = dtday.Rows[i]["EarlyOut"].ToString();
                            txtSUNEndTime.Value = dtday.Rows[i]["EndTime"].ToString();
                        }
                    }

                }


                Common.addlog("View", "HR", "Working Shifts \"" + txtShiftTitle.Value + "\" Viewed", "WorkingShifts", objDB.ShiftID);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                //if (DateTime.Parse(txtStartTime.Value) < DateTime.Parse(txtLateIn.Value) && DateTime.Parse(txtLateIn.Value) < DateTime.Parse(txtHalfDay.Value) && DateTime.Parse(txtHalfDay.Value) < DateTime.Parse(txtEarlyOut.Value) && DateTime.Parse(txtEarlyOut.Value) < DateTime.Parse(txtEndTime.Value))
                //{
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.WorkingShiftName = txtShiftTitle.Value;
                //objDB.StartTime = txtStartTime.Value;
                //objDB.LateInTime = txtLateIn.Value;
                //objDB.HalfDayStart = txtHalfDay.Value;
                //objDB.EarlyOut = txtEarlyOut.Value;
                //objDB.EndTime = txtEndTime.Value;
                objDB.Monday = chkMonday.Checked;
                objDB.Tuesday = chkTuesday.Checked;
                objDB.Wednesday = chkWednesdat.Checked;
                objDB.Thursday = chkThursday.Checked;
                objDB.Friday = chkFriday.Checked;
                objDB.Saturday = chkSaturday.Checked;
                objDB.Sunday = chkSunday.Checked;


                if (HttpContext.Current.Items["ShiftID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.WorkingShiftID = ShiftID;
                    res = objDB.UpdateShift();
                    objDB.DelWorkingShiftDaysByShiftID();
                    AddShiftDays();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    ShiftID = Convert.ToInt32(objDB.AddShift());
                    res = "New Shift Added";
                    AddShiftDays();
                    clearFields();
                }


                objDB.DocType = "Shifts";
                objDB.DocID = ShiftID;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

                if (res == "New Shift Added" || res == "Shift Data Updated")
                {
                    if (res == "New Shift Added") { Common.addlog("Add", "HR", "New Shift \"" + objDB.WorkingShiftName + "\" Added", "WorkingShifts"); }
                    if (res == "Shift Data Updated") { Common.addlog("Update", "HR", "Shift \"" + objDB.WorkingShiftName + "\" Updated", "WorkingShifts", objDB.ShiftID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }

                //}
                //else
                //{
                //    divAlertMsg.Visible = true;
                //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //    pAlertMsg.InnerHtml = "Invalid Entries!!";
                //}


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void AddShiftDays()
        {

            #region Monday
            objDB.StartTime = txtMONStartTime.Value;
            objDB.LateInTime = txtMONLateInTime.Value;
            objDB.BreakEarlyStartTime = txtMONBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtMONBreakStartTime.Value;
            objDB.BreakEndTime = txtMONBreakEndTime.Value;
            objDB.BreakLateTime = txtMONBreakLateTime.Value;
            objDB.HalfDayStart = txtMONHalfDayStart.Value;
            objDB.EarlyOut = txtMONEarlyOut.Value;
            objDB.EndTime = txtMONEndTime.Value;

            objDB.isNightShift = chkMonNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Monday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkMonday.Checked;
            objDB.AddWorkingShiftsDays();
            #endregion

            #region Tuesday

            objDB.StartTime = txtTUEStartTime.Value;
            objDB.LateInTime = txtTUELateInTime.Value;
            objDB.BreakEarlyStartTime = txtTUEBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtTUEBreakStartTime.Value;
            objDB.BreakEndTime = txtTUEBreakEndTime.Value;
            objDB.BreakLateTime = txtTUEBreakLateTime.Value;
            objDB.HalfDayStart = txtTUEHalfDayStart.Value;
            objDB.EarlyOut = txtTUEEarlyOut.Value;
            objDB.EndTime = txtTUEEndTime.Value;

            objDB.isNightShift = chkTUENightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Tuesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkTuesday.Checked;

            objDB.AddWorkingShiftsDays();
            #endregion

            #region Wednesday
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.LateInTime = txtWEDLateInTime.Value;
            objDB.BreakEarlyStartTime = txtWEDBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtWEDBreakStartTime.Value;
            objDB.BreakEndTime = txtWEDBreakEndTime.Value;
            objDB.BreakLateTime = txtWEDBreakLateTime.Value;
            objDB.HalfDayStart = txtWEDHalfDayStart.Value;
            objDB.EarlyOut = txtWEDEarlyOut.Value;
            objDB.EndTime = txtWEDEndTime.Value;

            objDB.isNightShift = chkWEDNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Wednesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkWednesdat.Checked;

            objDB.AddWorkingShiftsDays();
            #endregion

            #region Thursday
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.LateInTime = txtTHULateInTime.Value;
            objDB.BreakEarlyStartTime = txtTHUBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtTHUBreakStartTime.Value;
            objDB.BreakEndTime = txtTHUBreakEndTime.Value;
            objDB.BreakLateTime = txtTHUBreakLateTime.Value;
            objDB.HalfDayStart = txtTHUHalfDayStart.Value;
            objDB.EarlyOut = txtTHUEarlyOut.Value;
            objDB.EndTime = txtTHUEndTime.Value;

            objDB.isNightShift = chkTHUNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Thursday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkThursday.Checked;

            objDB.AddWorkingShiftsDays();
            #endregion

            #region Friday
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.LateInTime = txtFRILateInTime.Value;
            objDB.BreakEarlyStartTime = txtFRIBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtFRIBreakStartTime.Value;
            objDB.BreakEndTime = txtFRIBreakEndTime.Value;
            objDB.BreakLateTime = txtFRIBreakLateTime.Value;
            objDB.HalfDayStart = txtFRIHalfDayStart.Value;
            objDB.EarlyOut = txtFRIEarlyOut.Value;
            objDB.EndTime = txtFRIEndTime.Value;

            objDB.isNightShift = chkFRINightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Friday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkFriday.Checked;

            objDB.AddWorkingShiftsDays();
            #endregion

            #region Saturday
            objDB.StartTime = txtSATStartTime.Value;
            objDB.LateInTime = txtSATLateInTime.Value;
            objDB.BreakEarlyStartTime = txtSATBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtSATBreakStartTime.Value;
            objDB.BreakEndTime = txtSATBreakEndTime.Value;
            objDB.BreakLateTime = txtSATBreakLateTime.Value;
            objDB.HalfDayStart = txtSATHalfDayStart.Value;
            objDB.EarlyOut = txtSATEarlyOut.Value;
            objDB.EndTime = txtSATEndTime.Value;

            objDB.isNightShift = chkSATNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Saturday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSaturday.Checked;

            objDB.AddWorkingShiftsDays();
            #endregion

            #region Sunday
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.LateInTime = txtSUNLateInTime.Value;
            objDB.BreakEarlyStartTime = txtSUNBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtSUNBreakStartTime.Value;
            objDB.BreakEndTime = txtSUNBreakEndTime.Value;
            objDB.BreakLateTime = txtSUNBreakLateTime.Value;
            objDB.HalfDayStart = txtSUNHalfDayStart.Value;
            objDB.EarlyOut = txtSUNEarlyOut.Value;
            objDB.EndTime = txtSUNEndTime.Value;

            objDB.isNightShift = chkSUNNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Sunday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSunday.Checked;

            objDB.AddWorkingShiftsDays();

            #endregion

        }

        private void clearFields()
        {
            txtShiftTitle.Value = "";

            txtMONStartTime.Value = "9:00 AM";
            txtMONLateInTime.Value = "9:11 AM";
            txtMONBreakEarlyStartTime.Value = "1:00 PM";
            txtMONBreakStartTime.Value = "1:00 PM";
            txtMONBreakEndTime.Value = "1:30 PM";
            txtMONBreakLateTime.Value = "1:31 PM";
            txtMONHalfDayStart.Value = "2:30 PM";
            txtMONEarlyOut.Value = "5:29 PM";
            txtMONEndTime.Value = "5:30 PM";

            txtTUEStartTime.Value = "9:00 AM";
            txtTUELateInTime.Value = "9:11 AM";
            txtTUEBreakEarlyStartTime.Value = "1:00 PM";
            txtTUEBreakStartTime.Value = "1:00 PM";
            txtTUEBreakEndTime.Value = "1:30 PM";
            txtTUEBreakLateTime.Value = "1:31 PM";
            txtTUEHalfDayStart.Value = "2:30 PM";
            txtTUEEarlyOut.Value = "5:29 PM";
            txtTUEEndTime.Value = "5:30 PM";

            txtWEDStartTime.Value = "9:00 AM";
            txtWEDLateInTime.Value = "9:11 AM";
            txtWEDBreakEarlyStartTime.Value = "1:00 PM";
            txtWEDBreakStartTime.Value = "1:00 PM";
            txtWEDBreakEndTime.Value = "1:30 PM";
            txtWEDBreakLateTime.Value = "1:31 PM";
            txtWEDHalfDayStart.Value = "2:30 PM";
            txtWEDEarlyOut.Value = "5:29 PM";
            txtWEDEndTime.Value = "5:30 PM";

            txtTHUStartTime.Value = "9:00 AM";
            txtTHULateInTime.Value = "9:11 AM";
            txtTHUBreakEarlyStartTime.Value = "1:00 PM";
            txtTHUBreakStartTime.Value = "1:00 PM";
            txtTHUBreakEndTime.Value = "1:30 PM";
            txtTHUBreakLateTime.Value = "1:31 PM";
            txtTHUHalfDayStart.Value = "2:30 PM";
            txtTHUEarlyOut.Value = "5:29 PM";
            txtTHUEndTime.Value = "5:30 PM";

            txtFRIStartTime.Value = "9:00 AM";
            txtFRILateInTime.Value = "9:11 AM";
            txtFRIBreakEarlyStartTime.Value = "1:00 PM";
            txtFRIBreakStartTime.Value = "1:00 PM";
            txtFRIBreakEndTime.Value = "2:30 PM";
            txtFRIBreakLateTime.Value = "2:31 PM";
            txtFRIHalfDayStart.Value = "3:30 PM";
            txtFRIEarlyOut.Value = "6:29 PM";
            txtFRIEndTime.Value = "6:30 PM";

            txtSATStartTime.Value = "9:00 AM";
            txtSATLateInTime.Value = "9:11 AM";
            txtSATBreakEarlyStartTime.Value = "1:00 PM";
            txtSATBreakStartTime.Value = "1:00 PM";
            txtSATBreakEndTime.Value = "1:30 PM";
            txtSATBreakLateTime.Value = "1:31 PM";
            txtSATHalfDayStart.Value = "2:30 PM";
            txtSATEarlyOut.Value = "5:29 PM";
            txtSATEndTime.Value = "5:30 PM";

            txtSUNStartTime.Value = "9:00 AM";
            txtSUNLateInTime.Value = "9:11 AM";
            txtSUNBreakEarlyStartTime.Value = "1:00 PM";
            txtSUNBreakStartTime.Value = "1:00 PM";
            txtSUNBreakEndTime.Value = "1:30 PM";
            txtSUNBreakLateTime.Value = "1:31 PM";
            txtSUNHalfDayStart.Value = "2:30 PM";
            txtSUNEarlyOut.Value = "5:29 PM";
            txtSUNEndTime.Value = "5:30 PM";

            //txtStartTime.Value = "";
            //txtEndTime.Value = "";
            //txtHalfDay.Value = "";
            //txtLateIn.Value = "";
            //txtEndTime.Value = "";
            //txtEarlyOut.Value = "";

        }

        private bool getCheckBoxValue(string val)
        {
            if (val == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "WorkingShifts", "ShiftID", HttpContext.Current.Items["ShiftID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "Finance", "Working Shifts of ID\"" + HttpContext.Current.Items["ShiftID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/shift-lists", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/shifts/edit-shift-" + HttpContext.Current.Items["ShiftID"].ToString(), "Working Shifts \"" + txtShiftTitle.Value + "\"", "WorkingShifts", "Shifts", Convert.ToInt32(HttpContext.Current.Items["ShiftID"].ToString()));

                //Common.addlog(res, "HR", "Working Shifts of ID\"" + HttpContext.Current.Items["ShiftID"].ToString() + "\" Status Changed", "WorkingShifts", ShiftID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                CheckAccess();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }



        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "WorkingShifts", "ShiftID", HttpContext.Current.Items["ShiftID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "Working Shifts of ID \"" + HttpContext.Current.Items["ShiftID"].ToString() + "\" deleted", "WorkingShifts", ShiftID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

    }

}