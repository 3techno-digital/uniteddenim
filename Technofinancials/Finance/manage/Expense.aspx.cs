﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class Expense : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int ExpenseID
        {
            get
            {
                if (ViewState["ExpenseID"] != null)
                {
                    return (int)ViewState["ExpenseID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["ExpenseID"] = value;
            }
        }

        //DocStatus

        string EXPStatus
        {
            get
            {
                if (ViewState["EXPStatus"] != null)
                {
                    return ViewState["EXPStatus"].ToString();
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["EXPStatus"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;
                divAlertMsg.Visible = false;
                bankContainer.Visible = false;
                jvContaiber.Visible = false;
                btnGenVoucher.Visible = false;


                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/expenses";
                BindCOA();
                BindFromCOA();
                BindToCOA();
                BindClients();
                BindSuppliers();
                BindAccounts();
                chkBillable.Checked = true;

                ViewState["showFirstRow"] = null;
                ViewState["SrNo"] = null;
                ViewState["dtExpenseItems"] = null;
                dtExpenseItems = createExpenseItemTable();
                BindExpenseItemTable();

                //Attachment Start
                ViewState["FilesSrNo"] = null;
                ViewState["dtFiles"] = null;
                FilesSrNo = 1;
                dtFiles = null;
                dtFiles = new DataTable();
                dtFiles = createFiles();
                BindFilesTable();
                //Attachemt End

                clearFields();

                
                if (HttpContext.Current.Items["ExpenseID"] != null)
                {
                    ExpenseID = Convert.ToInt32(HttpContext.Current.Items["ExpenseID"].ToString());
                    getExenseByID(ExpenseID);
                    CheckAccess();
                }
                chkBillable_CheckedChanged(null, null);
            }

            // ATTACHMENT
            Page.ClientScript.GetPostBackEventReference(this, string.Empty);
            string ctrlName = Request.Params.Get("__EVENTTARGET");
            string ctrlArgs = Request.Params.Get("__EVENTARGUMENT");
            if (!String.IsNullOrEmpty(ctrlName) && ctrlName == "LinkUploadFiles")
            {

                if (updFiles.HasFile || updFiles.HasFiles)
                {
                    foreach (HttpPostedFile file in updFiles.PostedFiles)
                    {
                        Random rand = new Random((int)DateTime.Now.Ticks);
                        int randnum = 0;

                        string fn = "";
                        string exten = "";
                        string destDir = Server.MapPath("~/assets/files/AllAtachments/Finance/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");

                        randnum = rand.Next(1, 100000);
                        fn = "Orders-" + Common.RemoveSpecialCharacter("Finance Expense").ToLower().Replace(" ", "-") + "_" + randnum;

                        if (!Directory.Exists(destDir))
                        {
                            Directory.CreateDirectory(destDir);
                        }

                        string fname = Path.GetFileName(file.FileName);
                        exten = Path.GetExtension(file.FileName);
                        file.SaveAs(destDir + fn + exten);

                        DataRow dr = dtFiles.NewRow();
                        dr[0] = FilesSrNo.ToString();
                        dr[1] = fn + exten;
                        dr[2] = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/AllAtachments/Finance/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
                        dtFiles.Rows.Add(dr);
                        dtFiles.AcceptChanges();

                        FilesSrNo += 1;
                        BindFilesTable();
                    }
                }
            }
        }
        private void clearFields()
        {
            CheckSessions();
            DataTable dtVoucher = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            dtVoucher = objDB.GetExpenseVoucherNo(ref errorMsg);
            if (dtVoucher != null)
            {
                if (dtVoucher.Rows.Count>0)
                {
                    txtVoucherNo.Value = dtVoucher.Rows[0][0].ToString();
                }
            }

            txtDate.Value ="";
            txtTitle.Value ="";
            txtAmount.Value ="0";
            ddl_COA.SelectedIndex = -1;
            ddlSupplier.SelectedIndex = -1;
            chkBillable.Checked = false;
            ddlClients.SelectedIndex = -1;
            ddlVoucherType.SelectedIndex = -1;
            ddlAccount.SelectedIndex = -1;
            txtCheque.Value = "";
            ddlFromCOA.SelectedIndex = -1;
            ddlToCOA.SelectedIndex = -1;
            txtDescription.Value = "";
            
        }


        //attachment Start
        private DataTable dtFiles
        {
            get
            {
                if (ViewState["dtFiles"] != null)
                {
                    return (DataTable)ViewState["dtFiles"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtFiles"] = value;
            }
        }
        protected int FilesSrNo
        {
            get
            {
                if (ViewState["FilesSrNo"] != null)
                {
                    return (int)ViewState["FilesSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["FilesSrNo"] = value;
            }
        }
        private DataTable createFiles()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("FilePath");
            dt.AcceptChanges();

            return dt;
        }
        private void GetAllAttachmentsByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.TableName = "Finance";
            objDB.TableID = ID;
            dt = objDB.GetAllAttachmentsByTableIDAndName(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtFiles.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Attachment"]
                        });
                    }
                }
            }

            BindFilesTable();
        }
        protected void BindFilesTable()
        {
            if (ViewState["dtFiles"] == null)
            {
                dtFiles = createFiles();
                ViewState["dtFiles"] = dtFiles;
            }

            gvFiles.DataSource = dtFiles;
            gvFiles.DataBind();

            if (gvFiles.Rows.Count > 0)
            {
                gvFiles.UseAccessibleHeader = true;
                gvFiles.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void lnkRemoveFile_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtFiles.Rows.Count; i++)
            {
                if (dtFiles.Rows[i][0].ToString() == delSr)
                {
                    dtFiles.Rows[i].Delete();
                    dtFiles.AcceptChanges();
                }
            }
            for (int i = 0; i < dtFiles.Rows.Count; i++)
            {
                dtFiles.Rows[i].SetField(0, i + 1);
                dtFiles.AcceptChanges();
            }

            FilesSrNo = dtFiles.Rows.Count + 1;
            BindFilesTable();
        }
        protected void btnUploadPanel2_ServerClick(object sender, EventArgs e)
        {
            if (updFiles.HasFile || updFiles.HasFiles)
            {
                foreach (HttpPostedFile file in updFiles.PostedFiles)
                {
                    Random rand = new Random((int)DateTime.Now.Ticks);
                    int randnum = 0;

                    string fn = "";
                    string exten = "";
                    string destDir = Server.MapPath("~/assets/files/AllAtachments/Finance/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");

                    randnum = rand.Next(1, 100000);
                    fn = "PA-Evidence-" + Common.RemoveSpecialCharacter("Finance Expense").ToLower().Replace(" ", "-") + "_" + randnum;

                    if (!Directory.Exists(destDir))
                    {
                        Directory.CreateDirectory(destDir);
                    }

                    string fname = Path.GetFileName(file.FileName);
                    exten = Path.GetExtension(file.FileName);
                    file.SaveAs(destDir + fn + exten);

                    DataRow dr = dtFiles.NewRow();
                    dr[0] = FilesSrNo.ToString();
                    dr[1] = fn + exten;
                    dr[2] = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/AllAtachments/Finance/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
                    dtFiles.Rows.Add(dr);
                    dtFiles.AcceptChanges();

                    FilesSrNo += 1;
                    BindFilesTable();
                }
            }
        }
        //attachment end



        private void getExenseByID(int ExpenseID)

        {
            CheckSessions();

            DataTable dt = new DataTable();
            objDB.ExpencesID = ExpenseID;
            dt = objDB.GetExpensesByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    EXPStatus = dt.Rows[0]["DocStatus"].ToString();
                    txtDate.Value = DateTime.Parse(dt.Rows[0]["ExpenseDate"].ToString()).ToString("dd-MMM-yyyy");
                    txtTitle.Value = dt.Rows[0]["Title"].ToString();
                    txtAmount.Value = dt.Rows[0]["TotalBalance"].ToString();
                    ddl_COA.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                    ddlSupplier.SelectedValue = dt.Rows[0]["SpentAt"].ToString();
                    if (dt.Rows[0]["isBilledToCustomer"].ToString() == "True")
                    {
                        chkBillable.Checked = true;
                    }
                    else
                    {
                        chkBillable.Checked = false;
                    }
                    ddlClients.SelectedValue = dt.Rows[0]["ClientID"].ToString();
                    txtVoucherNo.Value = dt.Rows[0]["VoucherNo"].ToString();
                    ddlVoucherType.SelectedValue = dt.Rows[0]["NatureOfVoucher"].ToString();
                    ddlAccount.SelectedValue = dt.Rows[0]["AccountNo"].ToString();
                    txtCheque.Value = dt.Rows[0]["ChequeNo"].ToString();
                    ddlFromCOA.SelectedValue = dt.Rows[0]["FromCOA"].ToString();
                    ddlToCOA.SelectedValue = dt.Rows[0]["ToCOA"].ToString();
                    txtDescription.Value = dt.Rows[0]["Description"].ToString();
                    if (dt.Rows[0]["DocStatus"].ToString() == "Approved")
                    {
                        btnGenVoucher.Visible = true;


                    }
                    else
                    {
                        btnGenVoucher.Visible = false;

                    }

                }

            }
            Common.addlog("View", "Finance", "Expense \"" + txtVoucherNo.Value + "\" Viewed", "Expenses", objDB.ExpencesID);

            getExpenseItems(ExpenseID);
            //Attachment
            GetAllAttachmentsByID(ExpenseID);
        }

        private void getExpenseItems(int ExpenseID)
        {
            CheckSessions();

            objDB.ExpencesID = ExpenseID;
            DataTable dt = new DataTable();
            dt = objDB.GetAllExpenseItems(ref errorMsg);


            dtExpenseItems = null;
            dtExpenseItems = new DataTable();
            dtExpenseItems = createExpenseItemTable();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtExpenseItems.Rows[0][0].ToString() == "")
                    {
                        dtExpenseItems.Rows[0].Delete();
                        dtExpenseItems.AcceptChanges();
                        showFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtExpenseItems.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["ItemID"],
                            dt.Rows[i]["ItemCode"],
                            dt.Rows[i]["ItemName"],
                             dt.Rows[i]["COA_ID"],
                            dt.Rows[i]["GL_CODE"],
                            dt.Rows[i]["Price"],
                            dt.Rows[i]["Tax"],
                            dt.Rows[i]["NetAmount"],
                            dt.Rows[i]["ExpenseItem_ID"]
                        });
                    }
                    SrNo = Convert.ToInt32(dtExpenseItems.Rows[dtExpenseItems.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            BindExpenseItemTable();

        }

        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApprove.Visible = false;
            btnReview.Visible = false;
            btnRevApprove.Visible = false;
            lnkReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReview.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;



            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "Expenses";
            objDB.PrimaryColumnnName = "ExpenseID";
            objDB.PrimaryColumnValue = ExpenseID.ToString();
            objDB.DocName = "Expenses";

            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

            if (chkAccessLevel == "Can Edit")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReview.Visible = true;
            }
            if (chkAccessLevel == "Can Edit & Review")
            {
                btnSave.Visible = true;
                btnReview.Visible = true;
                lnkReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve")
            {
                btnSave.Visible = true;
                btnApprove.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve")
            {
                btnSave.Visible = true;
                btnRevApprove.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit")
            {
                btnSave.Visible = true;
            }
        }

        protected void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void BindCOA()
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddl_COA.Items.Clear();
            ddl_COA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddl_COA.DataTextField = "CodeTitle";
            ddl_COA.DataValueField = "COA_ID";
            ddl_COA.DataBind();
            ddl_COA.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
        }



        private void BindFromCOA()
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlFromCOA.Items.Clear();
            ddlFromCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlFromCOA.DataTextField = "CodeTitle";
            ddlFromCOA.DataValueField = "COA_ID";
            ddlFromCOA.DataBind();
            ddlFromCOA.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
        }

        private void BindToCOA()
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlToCOA.Items.Clear();
            ddlToCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlToCOA.DataTextField = "CodeTitle";
            ddlToCOA.DataValueField = "COA_ID";
            ddlToCOA.DataBind();
            ddlToCOA.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
        }
        private void BindClients()
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlClients.Items.Clear();
            ddlClients.DataSource = objDB.GetAllApprovedClients(ref errorMsg);
            ddlClients.DataTextField = "ClientName";
            ddlClients.DataValueField = "ClientID";
            ddlClients.DataBind();
            ddlClients.Items.Insert(0, new ListItem("--- Select Client ---", "0"));
        }

        private void BindSuppliers()
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlSupplier.Items.Clear();
            ddlSupplier.DataSource = objDB.GetAllSupplier(ref errorMsg);
            ddlSupplier.DataTextField = "SupplierName";
            ddlSupplier.DataValueField = "SupplierID";
            ddlSupplier.DataBind();
            ddlSupplier.Items.Insert(0, new ListItem("--- select ---", "0"));
        }

        private void BindAccounts()
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlAccount.Items.Clear();
            ddlAccount.DataSource = objDB.GetAllApprovedAccount(ref errorMsg);
            ddlAccount.DataTextField = "AccountTitle";
            ddlAccount.DataValueField = "AccountID";
            ddlAccount.DataBind();
            ddlAccount.Items.Insert(0, new ListItem("--- Select Account ---", "0"));
        }

        protected void btnSubmit_ServerClick1(object sender, EventArgs e)
        {
            CheckSessions();
            string res = "";

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.Title = txtTitle.Value;
            objDB.ChartOfAccountID = Convert.ToInt32(ddl_COA.SelectedValue);
            objDB.Description = txtDescription.Value;
            objDB.EDMS_ID = Convert.ToInt32(ddlSupplier.SelectedValue);
            objDB.NatureOfVoucher = ddlVoucherType.SelectedValue;
            objDB.ExpencesDate = txtDate.Value;
            objDB.isBillableExpense = chkBillable.Checked;
            objDB.ClientID = Convert.ToInt32(ddlClients.SelectedValue);
            objDB.Taxes = 0;
            objDB.TotalBalance = float.Parse(txtAmount.Value);
            objDB.FromCOA = Convert.ToInt32(ddlFromCOA.SelectedValue);
            objDB.ToCOA = Convert.ToInt32(ddlToCOA.SelectedValue);
            objDB.AccountNo = ddlAccount.SelectedValue;
            objDB.chequeNo = txtCheque.Value;
            objDB.VoucherNo = txtVoucherNo.Value;
            objDB.chequeDate = txtChequeDate.Value;
            objDB.refNo = txtRefNo.Value;
            objDB.paidTo = txtPaidTo.Value;
            if (ExpenseID != 0)
            {
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.ExpencesID = ExpenseID;
                res = objDB.UpdateExpenses();
                if (res == "Expense Updated" && EXPStatus == "Approved")
                {
                    objDB.TableName = "Expenses";
                    objDB.TableID = ExpenseID;
                    objDB.Credit = float.Parse(txtAmount.Value);
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.TransactionDetail = ("Payable to " + ddlSupplier.SelectedItem.Text + " against EXP # " + txtTitle.Value);
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.ChartOfAccountID = Convert.ToInt32(ddl_COA.SelectedValue);
                    objDB.AddTransactionHistoryCredit();
                }
            }
            else
            {
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.ExpencesID = Convert.ToInt32(objDB.ADDExpenses());
                clearFields();
            }

            if (HttpContext.Current.Items["ExpenseID"] == null)
            {
                clearFields();
                ViewState["showFirstRow"] = null;
                ViewState["SrNo"] = null;
                ViewState["dtExpenseItems"] = null;
                dtExpenseItems = createExpenseItemTable();
                BindExpenseItemTable();
                ExpenseID = 0;
                res = "New Expense Added Successfully";
            }

            //attachment start
            objDB.TableID = objDB.ExpencesID;
            objDB.TableName = "Finance";
            objDB.DeletedBy = Session["UserName"].ToString();
            objDB.DelAttachments();

            dtFiles = (DataTable)ViewState["dtFiles"] as DataTable;
            if (dtFiles != null)
            {
                if (dtFiles.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFiles.Rows.Count; i++)
                    {
                        objDB.TableID = objDB.ExpencesID;
                        objDB.TableName = "Finance";
                        objDB.Title = dtFiles.Rows[i]["Title"].ToString();
                        objDB.Attachment = dtFiles.Rows[i]["FilePath"].ToString();
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.AddAttachments();
                    }
                }
            }

            if (dtFiles.Rows.Count == 0)
            {
                ViewState["FilesSrNo"] = null;
                ViewState["dtFiles"] = null;
            }
            BindFilesTable();

            //attachment end

            if (res == "New Expense Added Successfully" || res == "Expense Updated")
            {
                if (res == "New Expense Added Successfully") { Common.addlog("Add", "Finance", " Expense \"" + objDB.Title + "\" Added", "Expenses"); }
                if (res == "Expense Updated") { Common.addlog("Update", "Finance", "Expense\"" + objDB.Title + "\" Updated", "Expenses", objDB.ExpencesID); }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "Expenses", "ExpenseID", HttpContext.Current.Items["ExpenseID"].ToString(), Session["UserName"].ToString());
            Common.addlogNew(res, "Finance", "Expenses of ID\"" + HttpContext.Current.Items["ExpenseID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/expenses", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/expenses/edit-expenses-" + HttpContext.Current.Items["ExpenseID"].ToString(), "Expenses \"" + txtTitle.Value + "\"", "Expenses", "Expenses", Convert.ToInt32(HttpContext.Current.Items["ExpenseID"].ToString()));

            //Common.addlog(res, "Finance", "Expenses of ID \"" + HttpContext.Current.Items["ExpenseID"].ToString() + "\" Status Changed", "Expenses", ExpenseID /* ID From Page Top  */);/* Button1_ServerClick  */
            if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
            {
                EXPStatus = "Approved";
                objDB.TableName = "Expenses";
                objDB.TableID = ExpenseID;
                objDB.Credit = float.Parse(txtAmount.Value);
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.ChartOfAccountID = Convert.ToInt32(ddl_COA.SelectedValue);
                objDB.TransactionDetail = ("Payable to " + ddlSupplier.SelectedItem.Text + " against EXP # " + txtVoucherNo.Value);

                objDB.AddTransactionHistoryCredit();

                dtExpenseItems = (DataTable)ViewState["dtExpenseItems"] as DataTable;
                if (dtExpenseItems != null)
                {
                    if (dtExpenseItems.Rows.Count > 0)
                    {

                        for (int i = 0; i < dtExpenseItems.Rows.Count; i++)
                        {
                            objDB.TableName = "ExpenseItems";
                            objDB.TableID = Convert.ToInt32(dtExpenseItems.Rows[i]["ExpenseItemID"].ToString()); 
                            objDB.ParentID = ExpenseID;
                            objDB.Debit = float.Parse(dtExpenseItems.Rows[i]["NetAmount"].ToString());
                            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.ChartOfAccountID = Convert.ToInt32(dtExpenseItems.Rows[i]["COA_ID"].ToString());
                            objDB.TransactionDetail = ("Payable to " + ddlSupplier.SelectedItem.Text + " against EXP # " + txtVoucherNo.Value);
                            objDB.AddTransactionHistoryDebit();

                        }

                    }
                }

                AddVoucher();
                btnGenVoucher.Visible = true;
            }
                divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;

            CheckAccess();
        }

        protected void AddVoucher()
        {
            CheckSessions();
            string JVID = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.VoucherNo = txtVoucherNo.Value;
            objDB.Against = "Expense";
            objDB.CreatedBy = Session["UserName"].ToString();
            JVID = objDB.AddFinanceVoucher();
            objDB.ExpencesID = ExpenseID;
            objDB.JVID = Convert.ToInt32(JVID);
            objDB.AddJVIDinExpense();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            string type = btn.CommandArgument;
            string res = Common.addAccessLevels(type, "Expenses", "ExpenseID", HttpContext.Current.Items["ExpenseID"].ToString(), Session["UserName"].ToString());
            Common.addlog("Delete", "Finance", "Expenses of ID \"" + HttpContext.Current.Items["ExpenseID"].ToString() + "\" deleted", "Expenses", ExpenseID /* ID From Page Top  */);/* lnkDelete_Click  */



            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            Response.Redirect(btnBack.HRef);

        }

        protected void chkBillable_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBillable.Checked == false)
            {
                ddlClients.SelectedValue = "0";
            }
            ddlClients.Enabled = chkBillable.Checked;

        }

        protected void addExpenseTransaction()
        {
            if (EXPStatus == "Approved")
            {
                objDB.TableName = "Expenses";
                objDB.TableID = ExpenseID;
                objDB.Credit = float.Parse(txtAmount.Value);
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.TransactionDetail = ("Payable to " + ddlSupplier.SelectedItem.Text + " against EXP # " + txtTitle.Value);
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.ChartOfAccountID = Convert.ToInt32(ddl_COA.SelectedValue);
                objDB.AddTransactionHistoryCredit();
            }
        }

        private DataTable dtExpenseItems
        {
            get
            {
                if (ViewState["dtExpenseItems"] != null)
                {
                    return (DataTable)ViewState["dtExpenseItems"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtExpenseItems"] = value;
            }
        }
        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        protected int SrNo
        {
            get
            {
                if (ViewState["SrNo"] != null)
                {
                    return (int)ViewState["SrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["SrNo"] = value;
            }
        }
        private DataTable createExpenseItemTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("ItemID");
            dt.Columns.Add("ItemCode");
            dt.Columns.Add("ItemName");
            dt.Columns.Add("COA_ID");
            dt.Columns.Add("COA_Title");
            dt.Columns.Add("Price");
            dt.Columns.Add("Tax");
            dt.Columns.Add("NetAmount");
            dt.Columns.Add("ExpenseItemID");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindExpenseItemTable()
        {
            if (ViewState["dtExpenseItems"] == null)
            {
                dtExpenseItems = createExpenseItemTable();
                ViewState["dtExpenseItems"] = dtExpenseItems;
            }

            gvAccount.DataSource = dtExpenseItems;
            gvAccount.DataBind();

            if (showFirstRow)
                gvAccount.Rows[0].Visible = true;
            else
                gvAccount.Rows[0].Visible = false;

            gvAccount.UseAccessibleHeader = true;
            gvAccount.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gvAccount_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                DropDownList ddlCOAs = e.Row.FindControl("ddlCOA") as DropDownList;
                ddlCOAs.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
                ddlCOAs.DataTextField = "CodeTitle";
                ddlCOAs.DataValueField = "COA_ID";
                ddlCOAs.DataBind();
                ddlCOAs.Items.Insert(0, new ListItem("--- Select COA ---", "0"));


                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                DropDownList ddlItemCode = e.Row.FindControl("ddlItem") as DropDownList;
                ddlItemCode.DataSource = objDB.GetAllItems(ref errorMsg);
                ddlItemCode.DataTextField = "ItemName";
                ddlItemCode.DataValueField = "ItemID";
                ddlItemCode.DataBind();
                ddlItemCode.Items.Insert(0, new ListItem("--- Select Item ---", "0"));


                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = SrNo.ToString();
            }


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {

                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    DropDownList ddlCOAs = e.Row.FindControl("ddlEditCOA") as DropDownList;
                    ddlCOAs.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
                    ddlCOAs.DataTextField = "CodeTitle";
                    ddlCOAs.DataValueField = "COA_ID";
                    ddlCOAs.DataBind();
                    ddlCOAs.Items.Insert(0, new ListItem("--- Select COA ---", "0"));


                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    DropDownList ddlItemCode = e.Row.FindControl("ddlEditItem") as DropDownList;
                    ddlItemCode.DataSource = objDB.GetAllItems(ref errorMsg);
                    ddlItemCode.DataTextField = "ItemName";
                    ddlItemCode.DataValueField = "ItemID";
                    ddlItemCode.DataBind();
                    ddlItemCode.Items.Insert(0, new ListItem("--- Select Item ---", "0"));

                    hdnRowNo.Value = e.Row.RowIndex.ToString();
                }
            }
        }
        protected void gvAccount_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvAccount.EditIndex = e.NewEditIndex;
            BindExpenseItemTable();
        }
        protected void gvAccount_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvAccount.EditIndex = -1;
            BindExpenseItemTable();
        }
        protected void gvAccount_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            objDB.ExpenseItemID = Convert.ToInt32(((HiddenField)gvAccount.Rows[e.RowIndex].FindControl("hdnItem")).Value);
            objDB.ItemID = Convert.ToInt32(((DropDownList)gvAccount.Rows[e.RowIndex].FindControl("ddlEditItem")).SelectedItem.Value);
            objDB.ItemName = ((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditItemName")).Text;
            objDB.ChartOfAccountID = Convert.ToInt32(((DropDownList)gvAccount.Rows[e.RowIndex].FindControl("ddlEditCOA")).SelectedItem.Value);
            objDB.Price = float.Parse(((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditPrice")).Text);
            objDB.TaxRate = Convert.ToDouble(((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditTax")).Text);
            objDB.NetAmount = float.Parse(((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditNetAmount")).Text);
            objDB.ModifiedBy = Session["UserName"].ToString();
            string s = objDB.UpdateExpenseItem();

            if (EXPStatus == "Approved" && s == "Expense Itemm Updated")
            {
                objDB.TableName = "ExpenseItems";
                objDB.TableID = Convert.ToInt32(((HiddenField)gvAccount.Rows[e.RowIndex].FindControl("hdnItem")).Value);
                objDB.ParentID = ExpenseID;
                objDB.Debit = float.Parse(((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditNetAmount")).Text);
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.ChartOfAccountID = Convert.ToInt32(((DropDownList)gvAccount.Rows[e.RowIndex].FindControl("ddlEditCOA")).SelectedItem.Value);
                objDB.TransactionDetail = ("Payable to " + ddlSupplier.SelectedItem.Text + " against EXP # " + txtVoucherNo.Value);
                objDB.AddTransactionHistoryDebit();
            }


            dtExpenseItems.Rows[index].SetField(1, ((DropDownList)gvAccount.Rows[e.RowIndex].FindControl("ddlEditItem")).SelectedItem.Value);
            dtExpenseItems.Rows[index].SetField(2, ((DropDownList)gvAccount.Rows[e.RowIndex].FindControl("ddlEditItem")).SelectedItem.Text);
            dtExpenseItems.Rows[index].SetField(3, ((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditItemName")).Text);
            dtExpenseItems.Rows[index].SetField(4, ((DropDownList)gvAccount.Rows[e.RowIndex].FindControl("ddlEditCOA")).SelectedItem.Value);
            dtExpenseItems.Rows[index].SetField(5, ((DropDownList)gvAccount.Rows[e.RowIndex].FindControl("ddlEditCOA")).SelectedItem.Text);
            dtExpenseItems.Rows[index].SetField(6, ((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditPrice")).Text);
            dtExpenseItems.Rows[index].SetField(7, ((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditTax")).Text);
            dtExpenseItems.Rows[index].SetField(8, ((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditNetAmount")).Text);
            dtExpenseItems.AcceptChanges();

            gvAccount.EditIndex = -1;
            BindExpenseItemTable();
            CalcTotal();
            addExpenseTransaction();
        }

        protected void Account_lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            string res = Common.addAccessLevels("Delete", "ExpenseItems", "ExpenseItem_ID", delSr, Session["UserName"].ToString());

            if (EXPStatus == "Approved" && res == "Deleted Succesfully")
            {
                objDB.TableID = Convert.ToInt32(delSr);
                objDB.TableName = "ExpenseItems";
                objDB.DeleteTransactionHistoryByTableID();
            }

            getExpenseItems(ExpenseID);
            CalcTotal();
            addExpenseTransaction();
        }

        protected void Account_btnAdd_Click(object sender, EventArgs e)
        {

            if (ExpenseID == 0)
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.Title = txtTitle.Value;
                objDB.ChartOfAccountID = Convert.ToInt32(ddl_COA.SelectedValue);
                objDB.Description = txtDescription.Value;
                objDB.EDMS_ID = Convert.ToInt32(ddlSupplier.SelectedValue);
                objDB.NatureOfVoucher = ddlVoucherType.SelectedValue;
                objDB.ExpencesDate = txtDate.Value;
                objDB.isBillableExpense = chkBillable.Checked;
                objDB.ClientID = Convert.ToInt32(ddlClients.SelectedValue);
                objDB.Taxes = 0;
                objDB.TotalBalance = float.Parse(txtAmount.Value);
                objDB.FromCOA = Convert.ToInt32(ddlFromCOA.SelectedValue);
                objDB.ToCOA = Convert.ToInt32(ddlToCOA.SelectedValue);
                objDB.AccountNo = ddlAccount.SelectedValue;
                objDB.chequeNo = txtCheque.Value;
                objDB.VoucherNo = txtVoucherNo.Value;
                objDB.chequeDate = txtChequeDate.Value;
                objDB.refNo = txtRefNo.Value;
                objDB.paidTo = txtPaidTo.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                string res = objDB.ADDExpenses();
                ExpenseID = Convert.ToInt32(res);
            }

            objDB.ExpencesID = ExpenseID;
            objDB.ItemID = Convert.ToInt32(((DropDownList)gvAccount.FooterRow.FindControl("ddlItem")).SelectedItem.Value);
            objDB.ItemCode = ((DropDownList)gvAccount.FooterRow.FindControl("ddlItem")).SelectedItem.Text;
            objDB.ItemName = ((TextBox)gvAccount.FooterRow.FindControl("txtItemName")).Text;
            objDB.ChartOfAccountID = Convert.ToInt32(((DropDownList)gvAccount.FooterRow.FindControl("ddlCOA")).SelectedItem.Value);
            objDB.Title = ((DropDownList)gvAccount.FooterRow.FindControl("ddlCOA")).SelectedItem.Text;
            objDB.Price = Convert.ToInt32(((TextBox)gvAccount.FooterRow.FindControl("txtPrice")).Text);
            objDB.TaxRate = Convert.ToInt32(((TextBox)gvAccount.FooterRow.FindControl("txtTax")).Text);
            objDB.NetAmount = Convert.ToInt32(((TextBox)gvAccount.FooterRow.FindControl("txtNetAmount")).Text);
            objDB.ExpencesID = ExpenseID;
            objDB.CreatedBy = Session["UserName"].ToString();
            string s = objDB.ADDExpenseItem();
            if (EXPStatus == "Approved")
            {
                objDB.TableName = "ExpenseItems";
                objDB.TableID = Convert.ToInt32(s);
                objDB.ParentID = ExpenseID;
                objDB.Debit = float.Parse(((TextBox)gvAccount.FooterRow.FindControl("txtNetAmount")).Text);
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.ChartOfAccountID = Convert.ToInt32(((DropDownList)gvAccount.FooterRow.FindControl("ddlCOA")).SelectedItem.Value);
                objDB.TransactionDetail = ("Payable to " + ddlSupplier.SelectedItem.Text + " against EXP # " + txtVoucherNo.Value);
                objDB.AddTransactionHistoryDebit();
            }
            getExpenseItems(ExpenseID);
            CalcTotal();
            addExpenseTransaction();

        }



        protected void CalcTotal()
        {
            try
            {
                double Tottax = 0, TotAmount = 0;
                if (dtExpenseItems != null)
                {
                    if (dtExpenseItems.Rows.Count  > 0)
                    {
                        for (int i = 0; i < dtExpenseItems.Rows.Count; i++)
                        {
                            if (dtExpenseItems.Rows[i]["NetAmount"].ToString() != "")
                            {
                                TotAmount += Convert.ToDouble(dtExpenseItems.Rows[i]["NetAmount"].ToString());
                            }
                            if (dtExpenseItems.Rows[i]["Tax"].ToString() != "")
                            {
                                Tottax += Convert.ToDouble(dtExpenseItems.Rows[i]["Tax"].ToString());
                            }
                        }
                    }
                }

                txtAmount.Value = TotAmount.ToString();

            }
            catch (Exception ex)
            {

            }
        }


        protected void ddlVoucherType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlVoucherType.SelectedValue == "Petty Cash")
            {
                bankContainer.Visible = false;
                jvContaiber.Visible = false;
            }
            else if (ddlVoucherType.SelectedValue == "Bank")
            {
                bankContainer.Visible = true;
                jvContaiber.Visible = false;
            }
            else if (ddlVoucherType.SelectedValue == "JV")
            {
                bankContainer.Visible = false;
                jvContaiber.Visible = true;
            }
            else
            {
                bankContainer.Visible = false;
                jvContaiber.Visible = false;
            }

        }

        protected void txtEditPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            int rowNum = Convert.ToInt32(hdnRowNo.Value);
            TextBox txtPrice = gvAccount.Rows[rowNum].FindControl("txtEditPrice") as TextBox;
            string price = txtPrice.Text;
            TextBox txtTax = gvAccount.Rows[rowNum].FindControl("txtEditTax") as TextBox;
            string tax = txtTax.Text;
           
            if (tax == "" || tax == null)
            {
                txtTax.Text = "0";
            }

            if (price == "" || price == null)
            {
                txtPrice.Text = "0";
            }
            if (!string.IsNullOrEmpty(price) && !string.IsNullOrEmpty(tax))
            {
                TextBox txtNetPrice = gvAccount.Rows[rowNum].FindControl("txtEditNetAmount") as TextBox;
                double ItemPice = Convert.ToInt32(price), ItemTax = Convert.ToInt32(tax);
                //ItemTax = ItemPice * (ItemTax / 100);
                string netPrice = (ItemPice * ItemTax).ToString();
                txtNetPrice.Text = netPrice;
            }
        }

        protected void txtPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            TextBox txtPrice = gvAccount.FooterRow.FindControl("txtPrice") as TextBox;
            string price = txtPrice.Text;
            TextBox txtTax = gvAccount.FooterRow.FindControl("txtTax") as TextBox;
            string tax = txtTax.Text;
            if (tax == "" || tax == null)
            {
                txtTax.Text = "0";
            }

            if (price == "" || price == null)
            {
                txtPrice.Text = "0";
            }

            if (!string.IsNullOrEmpty(price) && !string.IsNullOrEmpty(tax))
            {
                TextBox txtNetPrice = gvAccount.FooterRow.FindControl("txtNetAmount") as TextBox;
                double ItemPice = Convert.ToInt32(price), ItemTax = Convert.ToInt32(tax);
                //ItemTax = ItemPice * (ItemTax / 100);
                //string netPrice = (ItemPice - ItemTax).ToString();
                string netPrice = (ItemPice * ItemTax).ToString();
                txtNetPrice.Text = netPrice;
            }
            
        }

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            //DropDownList ddlCom = gv.FooterRow.FindControl("ddlCompanies") as DropDownList;
            objDB.ChartOfAccountID = Convert.ToInt32(((DropDownList)gvAccount.FooterRow.FindControl("ddlCOA")).SelectedItem.Value);
            DataTable dtCOA = objDB.GetCOAByID(ref errorMsg);
            if (dtCOA != null)
            {
                if (dtCOA.Rows.Count>0)
                {
                    TextBox txtTax = gvAccount.FooterRow.FindControl("txtTax") as TextBox;
                    txtTax.Text = dtCOA.Rows[0]["TaxRatePer"].ToString();
                    if (txtTax.Text == "")
                    {
                        txtTax.Text = "0";
                    }
                    CalcTotal();
                }
            }
            
        }

        protected void btnGenVoucher_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/expense-journal-voucher/generate-" + ExpenseID);
            }
            catch (Exception ex)
            {

            }
        }

    }
}