﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayRolls.aspx.cs" Inherits="Technofinancials.Finance.manage.PayRolls" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <%-- <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>

            <div class="wrap">
                
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/Payroll-B.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Payrolls</h3>
                        </div>


                        <div class="col-md-4">

                            <div class="form-group" id="divAlertMsg" runat="server">
                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                    <span>
                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                    </span>
                                    <p id="pAlertMsg" runat="server">
                                    </p>
                                </div>
                            </div>

                        </div>




                        <div class="col-sm-4">
                            <div class="pull-right flex">
                                <button type="button" class="tf-save-btn" value="Print JV" title="Print JV" id="btnGenVoucher" runat="server" onserverclick="btnGenVoucher_ServerClick"><i class="fa fa-file-pdf-o"></i></button>
                                <button type="button" data-toggle="modal" data-target="#paynow-modal" class="tf-save-btn" value="Pay Now" title="Pay Now" id="btnPayNow" runat="server"><i class="fa fa-money"></i></button>
                                <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" title="Note"><i class="fa fa-sticky-note-o"></i></button>
                                <button class="tf-pdf-btn" title="PDF" id="btnPDF" runat="server" onserverclick="btnPDF_ServerClick" type="button"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                                <button class="tf-save-btn" title="Review & Approve" id="btnRevApproveByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                <button class="tf-save-btn" title="Review" id="btnReviewByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                <button class="tf-save-btn" title="Approve" id="btnApproveByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class"   CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                <button class="tf-save-btn" title="Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                <button class="tf-save-btn" title="Submit for Review" id="btnSubForReviewByFinance" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                <button class="tf-save-btn" title="Reject & Disapproved" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                <button class="tf-save-btn" title="Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                <a class="tf-back-btn" title="Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="notes-modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Notes</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                        </p>
                                        <p>
                                            <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">

                                <div class="col-sm-3">
                                    <div class="form-group">

                                        <h4>Payroll Date  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPayrollDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <%--<input class="form-control" id="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" placeholder="Grant Date" type="text" runat="server"   onserverchange="txtPayrollDate_ServerChange"  />--%>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />

                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="input-group input-group-lg">
                                        <label class="marginBottomZeroLabel">
                                            EOBI COA
                                            <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="ddlCOA" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <asp:Label CssClass="lbl-COA-Balance" Text="Balance: 0" ID="lblCOABal" runat="server" />
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlCOA_SelectedIndexChanged" ID="ddlCOA">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group input-group-lg">
                                        <label class="marginBottomZeroLabel">
                                            Tax COA
                                            <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlCOATax" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <asp:Label CssClass="lbl-COA-Balance" Text="Balance: 0" ID="lblCOATaxBal" runat="server" />
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" ID="ddlCOATax">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <h4>&nbsp;</h4>
                                    <button class="tf-view-btn"  id="btnView" runat="server" onserverclick="txtPayrollDate_ServerChange" type="button"><i class="far fa-eye"></i></button>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:HiddenField ID="hdnRowNo" runat="server" />
                            <asp:GridView ID="gvSalaries" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Basic Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("BasicSalary") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Over Time Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTimeAmount" Text='<%# Eval("OverTimeAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Over Time Hours">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTime" Text='<%# Eval("OverTime") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Food Allowance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblFoodAllowence" Text='<%# Eval("FoodAllowence") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maintenance Allowance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblMaintenance" Text='<%# Eval("Maintenance") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fuel Allowance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblFuel" Text='<%# Eval("Fuel") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Absent Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAbsentAmount" Text='<%# Eval("AbsentAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gross Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblGrossSallary" Text='<%# Eval("GrossSalary") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Tax") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gross Salary With Overtime">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblGrossSalaryWithOT" Text='<%# Eval("GrossSalaryWithOT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="E.O.B.I">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEOBI" Text='<%# Eval("EOBI") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Advance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAdvance" Text='<%# Eval("Advance") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Salry">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalSalary" Text='<%# Eval("TotalSalry") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Salry With Overtime">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="TotalSalryWithOT" Text='<%# Eval("TotalSalryWithOT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Signature ">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="TotalSalryWithOT" Text=''></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>


                       <div class="modal fade" id="paynow-modal" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Pay Now</h4>
                                </div>
                                <div class="modal-body">

                                    <asp:UpdatePanel ID="UPPayNow" runat="server">
                                        <ContentTemplate>


                                            <div class="row">
                                                 <div class="col-md-4">
                                                    <label>Remaining Amount</label>
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" value="0" placeholder="0" id="txtRemainingAmount"  disabled="disabled" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Date</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="DD-MM-YYYY" id="txtDatePN" disabled="disabled" runat="server" />
                                                    </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <label>Pay By <span style="color:red !important;">*</span> </label>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlPayBy" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" /></label>
                                                <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" OnSelectedIndexChanged="ddlPayBy_SelectedIndexChanged" AutoPostBack="true" ID="ddlPayBy">
                                                    <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                                    <asp:ListItem Value="Bank">Bank</asp:ListItem>
                                                </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-md-4" id="accDiv" runat="server">
                                                    <label>Account</label>
                                                    <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlAccounts">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>COA <span style="color:red !important;">*</span></label>
                                                     <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="ddlCOAPN" ErrorMessage="*Required" Display="Dynamic" InitialValue="0" ValidationGroup="btnValidatePN" ForeColor="Red" /></label>
                                                <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlCOAPN">
                                                </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="row">
                        <div class="tab-content">
                            <div class="tab-pane active row">
                                <div class="col-sm-12">
                                      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>

                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" >
                                        <Columns>

                                            <%--<asp:TemplateField HeaderText="Payroll">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblPayroll" Text='<%# Eval("PayrollCode") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            
                                            <asp:TemplateField HeaderText="Employee Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("PAR_CAT_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Employee Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Remaining Amount">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblRemainingAmount" Text='<%# Eval("Salary") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Pay Amount">
                                                <ItemTemplate>
                                                    <asp:TextBox type="number" class="form-control"  id="txtPayAmount" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                        </Columns>
                                    </asp:GridView>
                                  </ContentTemplate>
                        </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>


                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="btnPay"  runat="server" onserverclick="btnPay_ServerClick" validationgroup="btnValidatePN" class="btn btn-default" data-dismiss="modal">Pay</button>
                                </div>
                            </div>
                        </div>
                    </div>




                </section>


                <!-- #dash-content -->
                                 </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }
        </style>
    </form>
</body>
</html>



