﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class ProductAndServices : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int ProAndSerID
        {
            get
            {
                if (ViewState["ProAndSerID"] != null)
                {
                    return (int)ViewState["ProAndSerID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["ProAndSerID"] = value;
            }
        }
        protected DataTable dtTaxTable
        {
            get
            {
                if (ViewState["dtTaxTable"] != null)
                {
                    return (DataTable)ViewState["dtTaxTable"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtTaxTable"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["ProAndSerID"] = null;
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/product-and-services";
                    BindCOA();
                    BindTaxesListBox();
                    divAlertMsg.Visible = false;
                    ViewState["dtTaxTable"] = null;
                    if (HttpContext.Current.Items["PASID"] != null)
                    {
                        ProAndSerID = Convert.ToInt32(HttpContext.Current.Items["PASID"].ToString());
                        GetProductAndServicesByID(ProAndSerID);
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void BindCOA()
        {
            //objDB.CategoriesList = "\'CGS/COS\',\'Revenue\',\'Other Rev.\'";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlAssAcc.Items.Clear();
            //ddlAssAcc.DataSource = objDB.GetCOAByCategory(ref errorMsg);
            ddlAssAcc.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlAssAcc.DataTextField = "CodeTitle" ;
            ddlAssAcc.DataValueField = "COA_ID";
            ddlAssAcc.DataBind();
            ddlAssAcc.Items.Insert(0, new ListItem("--- Select Account- --", "0"));
        }
        private void BindTaxesListBox()
        {

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dtTaxTable = objDB.GetAllTaxRates(ref errorMsg);
            lbTaxes.DataSource = dtTaxTable;
            lbTaxes.DataTextField = "TaxRateShortName";
            lbTaxes.DataValueField = "TaxRateID";
            lbTaxes.DataBind();

        }
        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "ProductAndServices";
                objDB.PrimaryColumnnName = "ProductAndServicesID";
                objDB.PrimaryColumnValue = ProAndSerID.ToString();
                objDB.DocName = "ProductServices";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void GetProductAndServiceTaxByID(int ProductAndServicesID)
        {

            DataTable dt = new DataTable();
            objDB.ProductAndServicesID = ProductAndServicesID;
            dt = objDB.GetProductAndServiceTaxByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        foreach (ListItem li in lbTaxes.Items)
                        {
                            if (dt.Rows[i]["TaxRateID"].ToString() == li.Value)
                                li.Selected = true;
                        }
                    }
                }
            }

        }
        private void GetProductAndServicesByID(int ProAndSerID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.ProductAndServicesID = ProAndSerID;
                dt = objDB.GetProductAndServiceByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtName.Value = dt.Rows[0]["Name"].ToString();
                        txtPrice.Value = dt.Rows[0]["Price"].ToString();
                        txtDescription.Value = dt.Rows[0]["Description"].ToString();
                        ddlAssAcc.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                        if (dt.Rows[0]["isBillableExpense"].ToString() == "True")
                        {
                            chkIsBillableExpense.Checked = true;
                        }
                        else
                        {
                            chkIsBillableExpense.Checked = false;
                        }
                        GetProductAndServiceTaxByID(ProAndSerID);
                        //objDB.DocID = ProAndSerID;
                        //objDB.DocType = "GetProductAndServices";
                        //ltrNotesTable.Text = objDB.GetDocNotes();
                    }
                }
                Common.addlog("View", "Finance", "Product And Services \"" + txtName.Value + "\" Viewed", "ProductAndServices", objDB.ProductAndServicesID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Name = txtName.Value;
                objDB.ChartOfAccountID = Convert.ToInt32(ddlAssAcc.SelectedValue);
                objDB.Price = float.Parse(txtPrice.Value);
                objDB.Description = txtDescription.Value;
                if (chkIsBillableExpense.Checked)
                {
                    objDB.isBillableExpense = true;
                }
                else
                {
                    objDB.isBillableExpense = false;
                }

                int Docid = 0;
                if (HttpContext.Current.Items["PASID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.ProductAndServicesID = ProAndSerID;

                    res = objDB.UpdateProductAndServices();
                    objDB.DelProductAndServicesTax();
                    foreach (ListItem li in lbTaxes.Items)
                    {
                        objDB.ProductAndServicesID = ProAndSerID;
                        objDB.TaxRateID = Convert.ToInt32(li.Value);
                        if (li.Selected == true)
                        {
                            objDB.AddProductAndServicesTax();
                        }

                    }
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    Docid = Convert.ToInt32(objDB.AddProductAndServices());
                    res = "Product And Services Added";
                    foreach (ListItem li in lbTaxes.Items)
                    {
                        objDB.ProductAndServicesID = Docid;
                        objDB.TaxRateID = Convert.ToInt32(li.Value);
                        if (li.Selected == true)
                        {
                            objDB.AddProductAndServicesTax();
                        }
                    }
                    clearFields();

                }

                objDB.DocType = "ProductAndServices";
                objDB.DocID = Docid;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

                if (res == "Product And Services Added" || res == "Product And Services Updated")
                {
                    if (res == "Product And Services Added") { Common.addlog("Add", "Finance", " Product And Services \"" + objDB.Name + "\" Added", "ProductAndServices"); }
                    if (res == "Product And Services Updated") { Common.addlog("Update", "Finance", "Product And Services \"" + objDB.Name + "\" Updated", "ProductAndServices", objDB.ProductAndServicesID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtName.Value = "";
            ddlAssAcc.SelectedIndex = -1;
            txtDescription.Value = "";
            txtPrice.Value = "0";
            //       txtNotes.Value = "";

            foreach (ListItem li in lbTaxes.Items)
            {
                li.Selected = false;
            }
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "ProductAndServices", "ProductAndServicesID", HttpContext.Current.Items["PASID"].ToString(), Session["UserName"].ToString());

                Common.addlogNew(res, "Finance", "Product And Services of ID\"" + HttpContext.Current.Items["PASID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/product-and-services", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/product-and-services/edit-product-and-services-" + HttpContext.Current.Items["PASID"].ToString(), "Product And Services \"" + txtName.Value + "\"", "ProductAndServices", "ProductServices", Convert.ToInt32(HttpContext.Current.Items["PASID"].ToString()));


                //Common.addlog(res, "Finance", "Product And Services of ID \"" + HttpContext.Current.Items["PASID"].ToString() + "\" Status Changed", "ProductAndServices", ProAndSerID /* ID From Page Top  */);/* Button1_ServerClick  */
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "ProductAndServices", "ProductAndServicesID", HttpContext.Current.Items["PASID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "Product And Services of ID \"" + HttpContext.Current.Items["PASID"].ToString() + "\" deleted", "ProductAndServices", ProAndSerID /* ID From Page Top  */);/* lnkDelete_Click  */


                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

    }
}