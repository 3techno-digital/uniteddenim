﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
	public partial class PayrollDate : System.Web.UI.Page
	{
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int PayrollDateID
        {
            get
            {
                if (ViewState["PayrollDateID"] != null)
                {
                    return (int)ViewState["PayrollDateID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PayrollDateID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["PayrollDateID"] = null;

                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    divAlertMsg.Visible = false;

                    GetAllPayrollDates();

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                //btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "AddOns";
                objDB.PrimaryColumnnName = "AddOnsID";
                objDB.PrimaryColumnValue = PayrollDateID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    //lnkDelete.Visible = true;
                    //btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    //btnReview.Visible = true;
                    //lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    //btnApprove.Visible = true;
                    //btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    //btnRevApprove.Visible = true;

                    //btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void GetAllPayrollDates()
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.CreatedBy = Session["UserName"].ToString();
                dt = objDB.GetPayrollDates(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtStartDate.Text = dt.Rows[0]["StartDate"].ToString();
                        txtEndtDate.Text = dt.Rows[0]["EndDate"].ToString();
                        PayrollDateID = Convert.ToInt32(dt.Rows[0]["PayrollID"].ToString());
                    }
                }
                //Common.addlog("View", "HR", "Currency \"" + txtTitle.Value + "\" Viewed", "Currency", objDB.PayrollDateID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.PayrollStartDate = Convert.ToInt32(txtStartDate.Text);
                objDB.PayrollEndDate = Convert.ToInt32(txtEndtDate.Text);
                //objDB.PayrollID = ;
                if (PayrollDateID.ToString() != null)
                {
                    objDB.PayrollDateID = PayrollDateID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdatePayrollDates();
                    //res = "AddOn Head Data Updated";
                }
                //else
                //{
                //    objDB.CreatedBy = Session["UserName"].ToString();
                //    res = objDB.AddAddOnsExpense();
                //    clearFields();
                //    //res = "New AddOn Head Added";
                //}



                if (res == "Succesfully Updated")
                {
                    if (res == "New PayrollDate Added") { Common.addlog("Add", "HR", "New PayrollDate \"" + objDB.CurrencyName + "\" Added", "PayrollDate"); }
                    if (res == "PayrollDate Data Updated") { Common.addlog("Update", "HR", "PayrollDate \"" + objDB.CurrencyName + "\" Updated", "PayrollDate", objDB.PayrollDateID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "AddOns", "AddOnsID", ViewState["PayrollDateID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "PayrollDate of ID\"" + ViewState["PayrollDateID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/PayrollDate", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/PayrollDate/edit-PayrollDate-" + ViewState["PayrollDateID"].ToString(), "PayrollDate \"" + PayrollDateID + "\"", "PayrollDate", "Announcement", Convert.ToInt32(ViewState["PayrollDateID"].ToString()));

                //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                //{
                //    objDB.PayrollDateID = PayrollDateID;
                //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //    objDB.AddCurrencyToEmployeeAttendance();
                //}

                //Common.addlog("Delete", "HR", "Currency of ID \"" + objDB.PayrollDateID + "\" deleted", "Currency", objDB.PayrollDateID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "PayrollDate", "PayrollDateID", ViewState["PayrollDateID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "PayrollDate of ID\"" + ViewState["PayrollDateID"].ToString() + "\" Status Changed", "PayrollDate", PayrollDateID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;

                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.PayrollDateID = PayrollDateID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteCurrency();
                Common.addlog("Currency Rejected", "HR", "Payroll Date of ID\"" + ViewState["PayrollDateID"].ToString() + "\" PayrollDate Rejected", "PayrollDate", PayrollDateID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "PayrollDate Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
    }
}