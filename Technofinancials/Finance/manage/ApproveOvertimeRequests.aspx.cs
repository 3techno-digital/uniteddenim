﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class ApproveOvertimeRequests : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int DirectReportEmployeeID
        {
            get
            {
                if (ViewState["DirectReportEmployeeID"] != null)
                {
                    return (int)ViewState["DirectReportEmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DirectReportEmployeeID"] = value;
            }
        }

        protected string DocumentStatus
        {
            get
            {
                if (ViewState["DocumentStatus"] != null)
                {
                    return (string)ViewState["DocumentStatus"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocumentStatus"] = value;
            }
        }

        protected int AttendanceAdjustmentID
        {
            get
            {
                if (ViewState["AttendanceAdjustmentID"] != null)
                {
                    return (int)ViewState["AttendanceAdjustmentID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AttendanceAdjustmentID"] = value;
            }
        }

        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return "Saved as Draft";
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["AttendanceAdjustmentID"] = null;
                    BindEmployeeDropdown();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/Overtime-Approval";

                    ddlEmployee.Enabled = true;
                    txtStartTime.Disabled =
                    txtEndTime.Disabled =
                    txtDate.Disabled = false;

                    divAlertMsg.Visible = false;
                    clearFields();
                    if (HttpContext.Current.Items["AttendanceID"] != null)
                    {
                        int AttendanceID = Convert.ToInt32(HttpContext.Current.Items["AttendanceID"].ToString());
                        DataTable dt = objDB.GetEmployeeOverTimeRecord(AttendanceID, ref errorMsg);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                            txtStartTime.Value = DateTime.Parse(dt.Rows[0]["TimeIn"].ToString()).ToString("hh:mm tt");
                            txtEndTime.Value = DateTime.Parse(dt.Rows[0]["TimeOut"].ToString()).ToString("hh:mm tt");
                            txtDate.Value = DateTime.Parse(dt.Rows[0]["AttendanceDate"].ToString()).ToString("dd-MMM-yyyy");
                            txtRequestremarks.Value = dt.Rows[0]["RequestRemarks"].ToString();
                            txtRequestOTHours.Value = dt.Rows[0]["RequestOTHours"].ToString();
                            txtReviewedremarks.Value = dt.Rows[0]["ReviewedRemarks"].ToString();
                            txtReviewedOTHours.Value = dt.Rows[0]["ReviewedOTHours"].ToString();
                            txtApproverremarks.Value = dt.Rows[0]["ApprovedRemarks"].ToString();
                            txtApprovedOTHours.Value = dt.Rows[0]["ApprovedOTHours"].ToString();


                            ddlEmployee.Enabled = false;
                            txtStartTime.Disabled =
                            txtEndTime.Disabled =
                            txtDate.Disabled = true;

                            btnApprove.Visible = false;
                            if (dt.Rows[0]["Status"].ToString() == "Approved")
                            {
                                
                                txtRequestOTHours.Disabled = true;
                                txtRequestremarks.Disabled = true;
                                txtReviewedOTHours.Disabled =true;
                                txtReviewedremarks.Disabled =true;
                                txtApprovedOTHours.Disabled =true;
                                txtApproverremarks.Disabled = true;
                            }
                            else if(dt.Rows[0]["Status"].ToString() == "Reviewed")
                            {
                                btnApprove.Visible = true;
                                txtRequestOTHours.Disabled = true;
                                txtRequestremarks.Disabled = true;
                                txtReviewedOTHours.Disabled  = true;
                                txtReviewedremarks.Disabled  = true;
                                txtApprovedOTHours.Disabled = false;
                                txtApproverremarks.Disabled = false;
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }


        private void clearFields()
        {
            ddlEmployee.SelectedIndex = -1;
            txtStartTime.Value =
            //txtBreakTime.Value =
            //txtBreakEndTime.Value =
            txtEndTime.Value =
            txtDate.Value = "";
            // txtDescription.Value = "";
        }

        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnApprove_ServerClick(object sender, EventArgs e)
        {
            try
            {
                divAlertMsg.Visible = false;
                if (txtReviewedOTHours.Value != txtApprovedOTHours.Value && txtApproverremarks.Value != "")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Remarks Required";
                    return;
                }
                CheckSessions();
                int EmployeeId, AttendanceId;
                EmployeeId = int.Parse(ddlEmployee.SelectedValue);
                AttendanceId = int.Parse(HttpContext.Current.Items["AttendanceID"].ToString());
                float OTHours = float.Parse (txtReviewedOTHours.Value);
                float.TryParse(txtApprovedOTHours.Value, out OTHours);
                txtApprovedOTHours.Value = OTHours.ToString();
                int CurrentEmployeeId = int.Parse(Session["EmployeeId"].ToString());
                DateTime date = DateTime.Parse(txtDate.Value);
                string errormsg = "";
                DataTable dt = objDB.OverTimeApprove(date, EmployeeId, OTHours, txtApproverremarks.Value, CurrentEmployeeId, ref errormsg);
                if (dt.Rows.Count > 0)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = dt.Rows[0][0].ToString();
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}