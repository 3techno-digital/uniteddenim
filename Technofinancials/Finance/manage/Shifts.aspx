﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Shifts.aspx.cs" Inherits="Technofinancials.Finance.manage.Shifts" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">
                <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>
                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <img src="/assets/images/shift_black.png" class="img-responsive tf-page-heading-img" />
                                    <h3 class="tf-page-heading-text">Shifts</h3>
                                </div>

                                <div class="col-md-4">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>


                                <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                    <ContentTemplate>


                                        <div class="col-sm-4">
                                            <div class="pull-right flex">
                                                <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                                <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                                <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                                <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                                <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class" "Delete" CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                                <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                                <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                                <button class="tf-save-btn" "Reject & Disapprove" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                                <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                                <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                            </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="notes-modal" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Notes</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>
                                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                        </p>
                                                        <p>
                                                            <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                        </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tab-content ">
                                        <div class="tab-pane active row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4>Shift Title <span style="color: red !important;">*</span>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtShiftTitle" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                        <input class="form-control" id="txtShiftTitle" placeholder="Shift Title" type="text" runat="server" />
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <h4>Night Shift</h4>

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkNightShift" runat="server" name="Night Shift" />
                                                            <label for="chkNightShift">*Check this box if change of date occur during shift.</label>
                                                        </div>
                                                    </div>
                                                    <%-- <div class="col-md-4">
                                                        <h4>Start Time <span style="color: red !important;">*</span>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtStartTime" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                        <input class="form-control" id="txtStartTime" placeholder="Start Date" type="text" data-plugin="timepicker" runat="server" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h4>End Time <span style="color: red !important;">*</span>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtEndTime" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                        <input class="form-control" id="txtEndTime" placeholder="End Date" type="text" data-plugin="timepicker" runat="server" />
                                                    </div>--%>
                                                </div>

                                                <%-- <div class="row">
                                                    <div class="col-md-4">
                                                        <h4>Late In Time  <span style="color: red !important;">*</span>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtLateIn" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                        <input class="form-control" id="txtLateIn" placeholder="Holiday Title" type="text" data-plugin="timepicker" runat="server" />
                                                    </div>

                                                    <div class="col-md-4">
                                                        <h4>Half Day Start <span style="color: red !important;">*</span>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtHalfDay" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                        <input class="form-control" id="txtHalfDay" placeholder="Start Date" type="text" data-plugin="timepicker" runat="server" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h4>Early Out  <span style="color: red !important;">*</span>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtEarlyOut" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                        <input class="form-control" id="txtEarlyOut" placeholder="End Date" type="text" data-plugin="timepicker" runat="server" />
                                                    </div>
                                                </div>--%>

                                                <%--      <div class="row">
                                               
                                            <div class="col-sm-2">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" checked="checked" id="chkMonday" runat="server" name="returnedLockerKeys" />
                                                    <label for="chkMonday">Monday</label>
                                                </div>
                                            </div>

                                                 <div class="col-sm-2">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" checked="checked" id="chkTuesday" runat="server" name="returnedLockerKeys" />
                                                    <label for="chkTuesday">Tuesday</label>
                                                </div>
                                            </div>
                                               
                                                 <div class="col-sm-2">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" checked="checked" id="chkWednesdat" runat="server" name="returnedLockerKeys" />
                                                    <label for="chkWednesdat">Wednesday</label>
                                                </div>
                                            </div>

                                                 <div class="col-sm-2">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" checked="checked" id="chkThursday" runat="server" name="returnedLockerKeys" />
                                                    <label for="chkThursday">Thursday</label>
                                                </div>
                                            </div>

                                                 <div class="col-sm-2">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" checked="checked" id="chkFriday" runat="server" name="returnedLockerKeys" />
                                                    <label for="chkFriday">Friday</label>
                                                </div>
                                            </div>

                                                 <div class="col-sm-2">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" checked="checked" id="chkSaturday" runat="server" name="returnedLockerKeys" />
                                                    <label for="chkSaturday">Saturday</label>
                                                </div>
                                            </div>

                                                 <div class="col-sm-2">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" id="chkSunday" runat="server" name="returnedLockerKeys" />
                                                    <label for="chkSunday">Sunday</label>
                                                </div>
                                            </div>


                                            </div>--%>
                                                <div class="clearfix">&nbsp;</div>
                                                <div class="row">
                                                    <div class="col-sm-12">

                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <th>Day</th>
                                                                <th>Start Time</th>
                                                                <th>Late In Time</th>
                                                                <th>Break Early Start Time</th>
                                                                <th>Break Start Time</th>
                                                                <th>Break End Time</th>
                                                                <th>Break Late Time</th>
                                                                <th style="display:none;">Half Day Start</th>
                                                                <th>Early Out</th>
                                                                <th>End Time</th>
                                                                <th>Night Shift</th>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-sm-2">

                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" checked="checked" id="chkMonday" runat="server" name="returnedLockerKeys" />
                                                                                <label for="chkMonday">Monday</label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" value="9:00 AM" id="txtMONStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="9:11 AM" id="txtMONLateInTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtMONBreakEarlyStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtMONBreakStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:30 PM" id="txtMONBreakEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:31 PM" id="txtMONBreakLateTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td style="display:none;">
                                                                        <input class="form-control" value="2:30 PM" id="txtMONHalfDayStart" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:29 PM" id="txtMONEarlyOut" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:30 PM" id="txtMONEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <div class="col-sm-2">

                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" id="chkMonNightShift" class="NightShift" runat="server" name="Night Shift" />
                                                                                <label for="chkMonNightShift"></label>
                                                                            </div>
                                                                        </div>

                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-sm-2">
                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" checked="checked" id="chkTuesday" runat="server" name="returnedLockerKeys" />
                                                                                <label for="chkTuesday">Tuesday</label>
                                                                            </div>
                                                                        </div>
                                                                    </td>

                                                                    <td>
                                                                        <input class="form-control" value="9:00 AM" id="txtTUEStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="9:11 AM" id="txtTUELateInTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtTUEBreakEarlyStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtTUEBreakStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:30 PM" id="txtTUEBreakEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:31 PM" id="txtTUEBreakLateTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td style="display:none;"> 
                                                                        <input class="form-control" value="2:30 PM" id="txtTUEHalfDayStart" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:29 PM" id="txtTUEEarlyOut" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:30 PM" id="txtTUEEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <div class="col-sm-2">

                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" id="chkTUENightShift" class="NightShift" runat="server" name="Night Shift" />
                                                                                <label for="chkTUENightShift"></label>
                                                                            </div>
                                                                        </div>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-sm-2">
                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" checked="checked" id="chkWednesdat" runat="server" name="returnedLockerKeys" />
                                                                                <label for="chkWednesdat">Wednesday</label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" value="9:00 AM" id="txtWEDStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="9:11 AM" id="txtWEDLateInTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtWEDBreakEarlyStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtWEDBreakStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:30 PM" id="txtWEDBreakEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:31 PM" id="txtWEDBreakLateTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td style="display:none;">
                                                                        <input class="form-control" value="2:30 PM" id="txtWEDHalfDayStart" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:29 PM" id="txtWEDEarlyOut" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:30 PM" id="txtWEDEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <div class="col-sm-2">

                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" id="chkWEDNightShift" class="NightShift" runat="server" name="Night Shift" />
                                                                                <label for="chkWEDNightShift"></label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-sm-2">
                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" checked="checked" id="chkThursday" runat="server" name="returnedLockerKeys" />
                                                                                <label for="chkThursday">Thursday</label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" value="9:00 AM" id="txtTHUStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="9:11 AM" id="txtTHULateInTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtTHUBreakEarlyStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtTHUBreakStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:30 PM" id="txtTHUBreakEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:31 PM" id="txtTHUBreakLateTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td style="display:none;">
                                                                        <input class="form-control" value="2:30 PM" id="txtTHUHalfDayStart" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:29 PM" id="txtTHUEarlyOut" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:30 PM" id="txtTHUEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <div class="col-sm-2">

                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" id="chkTHUNightShift" class="NightShift" runat="server" name="Night Shift" />
                                                                                <label for="chkTHUNightShift"></label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-sm-2">
                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" checked="checked" id="chkFriday" runat="server" name="returnedLockerKeys" />
                                                                                <label for="chkFriday">Friday</label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" value="9:00 AM" id="txtFRIStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="9:11 AM" id="txtFRILateInTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtFRIBreakEarlyStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtFRIBreakStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="2:30 PM" id="txtFRIBreakEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="2:31 PM" id="txtFRIBreakLateTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td style="display:none;">
                                                                        <input class="form-control" value="3:00 PM" id="txtFRIHalfDayStart" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="6:29 PM" id="txtFRIEarlyOut" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="6:30 PM" id="txtFRIEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <div class="col-sm-2">

                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" id="chkFRINightShift" class="NightShift" runat="server" name="Night Shift" />
                                                                                <label for="chkFRINightShift"></label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-sm-2">
                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" checked="checked" id="chkSaturday" runat="server" name="returnedLockerKeys" />
                                                                                <label for="chkSaturday">Saturday</label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" value="9:00 AM" id="txtSATStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="9:11 AM" id="txtSATLateInTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtSATBreakEarlyStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtSATBreakStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:30 PM" id="txtSATBreakEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:31 PM" id="txtSATBreakLateTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td style="display:none;">
                                                                        <input class="form-control" value="2:30 PM" id="txtSATHalfDayStart" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:29 PM" id="txtSATEarlyOut" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:30 PM" id="txtSATEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <div class="col-sm-2">

                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" id="chkSATNightShift" class="NightShift" runat="server" name="Night Shift" />
                                                                                <label for="chkSATNightShift"></label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-sm-2">
                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" id="chkSunday" runat="server" name="returnedLockerKeys" />
                                                                                <label for="chkSunday">Sunday</label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" value="9:00 AM" id="txtSUNStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="9:11 AM" id="txtSUNLateInTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtSUNBreakEarlyStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:00 PM" id="txtSUNBreakStartTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:30 PM" id="txtSUNBreakEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="1:31 PM" id="txtSUNBreakLateTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td style="display:none;">
                                                                        <input class="form-control" value="2:30 PM" id="txtSUNHalfDayStart" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:29 PM" id="txtSUNEarlyOut" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <input class="form-control" value="5:30 PM" id="txtSUNEndTime" placeholder="00:00" type="text" data-plugin="timepicker" runat="server" /></td>
                                                                    <td>
                                                                        <div class="col-sm-2">

                                                                            <div class="checkbox checkbox-primary">
                                                                                <input type="checkbox" id="chkSUNNightShift" class="NightShift" runat="server" name="Night Shift" />
                                                                                <label for="chkSUNNightShift"></label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                                <div class="clearfix">&nbsp;</div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
        </style>
        <script>
            $('#chkNightShift').change(function () {
                if ($(this).is(':checked')) {
                    $('.NightShift').prop('checked', true);
                }
                else {
                    $('.NightShift').prop('checked', false);
                }

            }
            );


        </script>
        <!-- Modal -->
    </form>
</body>
</html>
