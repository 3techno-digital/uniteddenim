﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttendanceAdjustment.aspx.cs" Inherits="Technofinancials.Finance.Manage.AttendanceAdjustment" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="StyleSheets1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <uc:Header ID="header2" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar1" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">
              <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <h1 class="m-0 text-dark">Attendance Adjustment</h1>
                        </div>
                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div style="text-align: right;">
                                        <%--<button type="button" class="AD_btn" data-toggle="modal" data-target="#notes-modal" value="Add Note">Note</button>--%>
                                        <button class="AD_btn" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                        <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                        <button class="AD_btn" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                        <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn" CommandArgument='Reject' data-toggle="modal" data-target="#notes-modal" value="Add Note">Reject</asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        <button class="AD_btn"  id="btnDisapprove" runat="server" onserverclick="btnDisapprove_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                        <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                        <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                        <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade M_set" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="m-0 text-dark">Notes</h1>
                                                <div class="add_new">
                                                    <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                                                    <button type="button" class="AD_btn" runat="server" onserverclick="btnReject_ServerClick" data-dismiss="modal">Save</button>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtAdjustmentNote" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <h4>Employee  <span style="color: red !important;">*</span></h4>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator31" ControlToValidate="ddlEmployee" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                    <asp:DropDownList ID="ddlEmployee" runat="server" data-plugin="select2" CssClass="form-control select2"></asp:DropDownList>
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Date  <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input id="txtDate" data-date-format="DD-MMM-YYYY" class="form-control datetime-picker" placeholder="Date" type="text" runat="server" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Start Time <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtStartTime" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input  class="form-control" data-date-format="hh:mm A" data-plugin="datetimepicker" id="txtStartTime"  type="text"  runat="server" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>End Time 
                                       <%-- <lable><input type="checkbox" runat="server" name="isNextDay" id="isNextDay" /> On Next Day</lable>
                                        <span style="color: red !important;">*</span>--%>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator42" ControlToValidate="txtEndTime" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input  class="form-control" data-date-format="hh:mm A" data-plugin="datetimepicker" id="txtEndTime" type="text"  runat="server" />
                                </div>
                                <div class="col-md-12 form-group">
                                    <h4>Reason</h4>
                                    <textarea class="form-control" id="txtDescription" placeholder="Reason" type="text" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>



<%--                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr />
                                </div>
                            </div>--%>


                        

                                                <div class="clearfix">&nbsp;</div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer2" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="Scripts1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
            .tf-alert-danger p {
                display: inline-block;
                margin-top: 0px;
                position: absolute;
                margin-left: 7px;
                font-size: 13px;
                color: #fff;
            }
            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }

        </style>
        <!-- Modal -->
    </form>
</body>
</html>
