﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class COATransferMoney : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COABalTraID
        {
            get
            {
                if (ViewState["COABalTraID"] != null)
                {
                    return (int)ViewState["COABalTraID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COABalTraID"] = value;
            }
        }

        string TMStatus
        {
            get
            {
                if (ViewState["TMStatus"] != null)
                {
                    return ViewState["TMStatus"].ToString();
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["TMStatus"] = value;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/coa-transfer-money";

                    divAlertMsg.Visible = false;
                    clearFields();
                    BindFromCOA();
                    BindToCOA();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["COABalanceTransferID"] != null)
                    {
                        COABalTraID = Convert.ToInt32(HttpContext.Current.Items["COABalanceTransferID"].ToString());
                        getDataByID(COABalTraID);
                        CheckAccess();

                    }

                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }



        private void BindFromCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlFromCOA.Items.Clear();
            ddlFromCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlFromCOA.DataTextField = "CodeTitle";
            ddlFromCOA.DataValueField = "COA_ID";
            ddlFromCOA.DataBind();
            ddlFromCOA.Items.Insert(0, new ListItem("--- Select From COA ---", "0"));
        }

        private void BindToCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlToCOA.Items.Clear();
            ddlToCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlToCOA.DataTextField = "CodeTitle";
            ddlToCOA.DataValueField = "COA_ID";
            ddlToCOA.DataBind();
            ddlToCOA.Items.Insert(0, new ListItem("--- Select To COA ---", "0"));

        }





        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.COABalanceTransferID = ID;
            dt = objDB.GetCOABalanceTransferByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    TMStatus = dt.Rows[0]["DocStatus"].ToString();
                    ddlFromCOA.SelectedValue = dt.Rows[0]["FromCOA"].ToString();
                    ddlToCOA.SelectedValue = dt.Rows[0]["ToCOA"].ToString();
                    txtAmount.Value = dt.Rows[0]["AmountTransfer"].ToString();
                    txtDate.Value = DateTime.Parse(dt.Rows[0]["TransferDate"].ToString()).ToString("dd-MMM-yyyy");
                    txtDescription.Value = dt.Rows[0]["Description"].ToString();

                }
            }
            Common.addlog("View", "Finance", "Balance Transfer by ID \"" + (objDB.COABalanceTransferID).ToString() + "\" Viewed", "COABalanceTransfer", objDB.COABalanceTransferID);

        }

        private void clearFields()
        {
            ddlFromCOA.SelectedIndex = -1;
            ddlToCOA.SelectedIndex = -1;
            txtAmount.Value = "0";
            txtDate.Value = "";
            txtDescription.Value = "";

        }

        private void CheckSessions()
        {
            if (Session["userid"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.FromCOA = Convert.ToInt32(ddlFromCOA.SelectedValue);
                objDB.ToCOA = Convert.ToInt32(ddlToCOA.SelectedValue);
                objDB.AmountTransfer = Convert.ToDouble(txtAmount.Value);
                objDB.TransferDate = txtDate.Value;
                objDB.Description = txtDescription.Value;

                if (HttpContext.Current.Items["COABalanceTransferID"] != null)
                {
                    objDB.COABalanceTransferID = COABalTraID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOABalanceTransfer();

                    if (TMStatus == "Approved")
                    {
                        objDB.TableName = "COABalanceTransfer";
                        objDB.TableID = COABalTraID;
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.TransactionDetail = ("Payable to " + ddlFromCOA.SelectedItem.Text + " against Balance Transfe");

                        objDB.ChartOfAccountID = Convert.ToInt32(ddlFromCOA.SelectedValue);
                        objDB.Credit = float.Parse(txtAmount.Value);
                        objDB.AddTransactionHistoryCredit();

                        objDB.TableName = "COABalanceTransfer2";
                        objDB.ChartOfAccountID = Convert.ToInt32(ddlToCOA.SelectedValue);
                        objDB.Debit = float.Parse(txtAmount.Value);
                        objDB.AddTransactionHistoryDebit();
                    }
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOABalanceTransfer();
                    clearFields();
                }


                if (res == "Balance Transfer Added" || res == "Balance Transfer Updated")
                {
                    if (res == "Balance Transfer Added") { Common.addlog("Add", "Finance", " Balance Transfer by ID\"" + (objDB.COABalanceTransferID).ToString() + "\" Added", "COABalanceTransfer"); }
                    if (res == "Balance Transfer Updated") { Common.addlog("Update", "Finance", "Balance Transfer by ID \"" + (objDB.COABalanceTransferID).ToString() + "\" Updated", "COABalanceTransfer", objDB.COABalanceTransferID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "COABalanceTransfer";
                objDB.PrimaryColumnnName = "COABalanceTransferID";
                objDB.PrimaryColumnValue = COABalTraID.ToString();
                objDB.DocName = "TransferMoney";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "COABalanceTransfer", "COABalanceTransferID", HttpContext.Current.Items["COABalanceTransferID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "Finance", "Balance Transfer of ID\"" + HttpContext.Current.Items["COABalanceTransferID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/coa-transfer-money", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/coa-transfer-money/edit-coa-transfer-money-" + HttpContext.Current.Items["COABalanceTransferID"].ToString(), "Balance Transfer From COA \"" + ddlFromCOA.SelectedItem.Text + " To COA " + ddlFromCOA.SelectedItem.Text + "\"", "COABalanceTransfer", "TransferMoney", Convert.ToInt32(HttpContext.Current.Items["COABalanceTransferID"].ToString()));

                //Common.addlog(res, "Finance", "Balance Transfer of ID \"" + HttpContext.Current.Items["COABalanceTransferID"].ToString() + "\" Status Changed", "COABalanceTransfer", COABalTraID /* ID From Page Top  */);/* Button1_ServerClick  */
                if (res == "Approved Sucessfull" || res == "Reviewed & Approved Sucessfull")
                {
                    TMStatus = "Approved";
                    objDB.TableName = "COABalanceTransfer";
                    objDB.TableID = COABalTraID;
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.TransactionDetail = ("Payable to " + ddlFromCOA.SelectedItem.Text + " against Balance Transfe");

                    objDB.ChartOfAccountID = Convert.ToInt32(ddlFromCOA.SelectedValue);
                    objDB.Credit = float.Parse(txtAmount.Value);
                    objDB.AddTransactionHistoryCredit();

                    objDB.TableName = "COABalanceTransfer2";
                    objDB.ChartOfAccountID = Convert.ToInt32(ddlToCOA.SelectedValue);
                    objDB.Debit = float.Parse(txtAmount.Value);
                    objDB.AddTransactionHistoryDebit();

                }
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "COABalanceTransfer", "COABalanceTransferID", HttpContext.Current.Items["COABalanceTransferID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "Balance Transfer of ID \"" + HttpContext.Current.Items["COABalanceTransferID"].ToString() + "\" deleted", "COABalanceTransfer", COABalTraID /* ID From Page Top  */);/* lnkDelete_Click  */



                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlToCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlFromCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblFromCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlFromCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblFromCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }

}