﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.manage.coa
{
    public partial class Location : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COAID
        {
            get
            {
                if (ViewState["COAID"] != null)
                {
                    return (int)ViewState["COAID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COAID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/chart-of-account-step-01";

                   
                    ViewState["showLocationFirstRow"] = null;
                    ViewState["LocationSrNo"] = null;
                    ViewState["dtLocationCode"] = null;
                    dtLocationCode = createLocationCodeTable();
                    BindLocationCodeTable();

                   

                    divAlertMsg.Visible = false;
                    clearFields();
                    GetData();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["COAID"] != null)
                    {
                        COAID = Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString());
                       getDataByID(COAID);
                        CheckAccess();
                    }
                    else
                    {

                    }

                 

                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

     
        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "FAM_COA";
                objDB.PrimaryColumnnName = "COA_ID";
                objDB.PrimaryColumnValue = COAID.ToString();
                objDB.DocName = "ConfigureCOA";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void GetData()
        {
            CheckSessions();
            
            // Location Code Start
            //objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            DataTable dt = new DataTable();
            dt = objDB.GetAllLocations(ref errorMsg);
            //if (dt == null)
            //{
            //    objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            //    dt = objDB.GetAllLocationsByParentCompanyID(ref errorMsg);
            //    dt.Columns.Add("Code");
            //    dt.AcceptChanges();
            //    if (dt != null)
            //    {
            //        int loc_code = 1;
            //        int CurComID = 0;
            //        for (int i = 0; i < dt.Rows.Count; i++)
            //        {
            //            if (int.Parse(dt.Rows[i]["CompanyID"].ToString()) != CurComID)
            //            {
            //                CurComID = int.Parse(dt.Rows[i]["CompanyID"].ToString());
            //                loc_code = 1;
            //            }
            //            if (loc_code < 10)
            //            {
            //                dt.Rows[i]["Code"] = "0" + loc_code.ToString();
            //            }
            //            else
            //            {
            //                dt.Rows[i]["Code"] = loc_code.ToString();
            //            }
            //            dt.AcceptChanges();
            //            loc_code++;
            //        }
            //    }

            //}

            dtLocationCode = null;
            dtLocationCode = new DataTable();
            dtLocationCode = createLocationCodeTable();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtLocationCode.Rows[0][0].ToString() == "")
                    {
                        dtLocationCode.Rows[0].Delete();
                        dtLocationCode.AcceptChanges();
                        showLocationFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtLocationCode.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["LOCATION_ID"],
                            dt.Rows[i]["NAME"],
                            dt.Rows[i]["LocationCode"],
                            dt.Rows[i]["CompanyID"],
                            dt.Rows[i]["CompanyName"]
                        });
                    }
                    LocationSrNo = Convert.ToInt32(dtLocationCode.Rows[dtLocationCode.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            BindLocationCodeTable();


            // Company Code End


        }

        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.ChartOfAccountID = COAID;
            dt = objDB.GetCOAByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                   


                  
                }
            }

        }

        private void clearFields()
        {
            txtNotes.Value = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.Notes = txtNotes.Value;

                if (HttpContext.Current.Items["COAID"] != null)
                {
                    // update coding
                    objDB.ChartOfAccountID = COAID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOA();
                }
                else
                {
                    // add coding
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOA();
                    clearFields();
                }

                if (res == "New COA Added" || res == "COA Updated")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }
        

        private DataTable dtLocationCode
        {
            get
            {
                if (ViewState["dtLocationCode"] != null)
                {
                    return (DataTable)ViewState["dtLocationCode"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtLocationCode"] = value;
            }
        }
        bool showLocationFirstRow
        {
            get
            {
                if (ViewState["showLocationFirstRow"] != null)
                {
                    return (bool)ViewState["showLocationFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showLocationFirstRow"] = value;
            }
        }
        protected int LocationSrNo
        {
            get
            {
                if (ViewState["LocationSrNo"] != null)
                {
                    return (int)ViewState["LocationSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["LocationSrNo"] = value;
            }
        }
        private DataTable createLocationCodeTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("LocationSrNo");
            dt.Columns.Add("LocationID");
            dt.Columns.Add("LocationName");
            dt.Columns.Add("LocationCode");
            dt.Columns.Add("CompanyID");
            dt.Columns.Add("CompanyName");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindLocationCodeTable()
        {
            if (ViewState["dtLocationCode"] == null)
            {
                dtLocationCode = createLocationCodeTable();
                ViewState["dtLocationCode"] = dtLocationCode;
            }

            gvLocation.DataSource = dtLocationCode;
            gvLocation.DataBind();

            if (showLocationFirstRow)
                gvLocation.Rows[0].Visible = true;
            else
                gvLocation.Rows[0].Visible = false;

            gvLocation.UseAccessibleHeader = true;
            gvLocation.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gvLocation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                //objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"]);
                //DropDownList ddlCom = e.Row.FindControl("ddlCompanies") as DropDownList;
                //ddlCom.DataSource = objDB.GetCompaniesByParentID(ref errorMsg);
                //ddlCom.DataTextField = "CompanyName";
                //ddlCom.DataValueField = "CompanyID";
                //ddlCom.DataBind();
                //ddlCom.Items.Insert(0, new ListItem("--- Select Company ---", "0"));


                //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //DropDownList ddlLoc = e.Row.FindControl("ddlLocation") as DropDownList;
                //ddlLoc.DataSource = objDB.GetAllLocations(ref errorMsg);
                //ddlLoc.DataTextField = "NAME";
                //ddlLoc.DataValueField = "LOCATION_ID";
                //ddlLoc.DataBind();
                //ddlLoc.Items.Insert(0, new ListItem("--- Select Location ---", "0"));
                //ddlLoc.Enabled = false;

                //Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                //txtSrNo.Text = LocationSrNo.ToString();
            }


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    //DropDownList ddlLocEdit = e.Row.FindControl("ddlEditLocation") as DropDownList;
                    //ddlLocEdit.DataSource = objDB.GetAllLocations(ref errorMsg);
                    //ddlLocEdit.DataTextField = "NAME";
                    //ddlLocEdit.DataValueField = "LOCATION_ID";
                    //ddlLocEdit.DataBind();
                    //ddlLocEdit.Items.Insert(0, new ListItem("--- Select Location ---", "0"));


                }
            }
        }
        protected void gvLocation_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvLocation.EditIndex = e.NewEditIndex;
            BindLocationCodeTable();
        }
        protected void gvLocation_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvLocation.EditIndex = -1;
            BindLocationCodeTable();
        }
        protected void gvLocation_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            /// dtLocationCode.Rows[index].SetField(1, ((DropDownList)gvLocation.Rows[e.RowIndex].FindControl("ddlEditLocation")).SelectedItem.Text);
            dtLocationCode.Rows[index].SetField(3, ((TextBox)gvLocation.Rows[e.RowIndex].FindControl("txtEditDesc")).Text);
            dtLocationCode.AcceptChanges();

            gvLocation.EditIndex = -1;
            BindLocationCodeTable();
        }
        protected void Location_lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtLocationCode.Rows.Count; i++)
            {
                if (dtLocationCode.Rows[i][0].ToString() == delSr)
                {
                    dtLocationCode.Rows[i].Delete();
                    dtLocationCode.AcceptChanges();
                }
            }
            for (int i = 0; i < dtLocationCode.Rows.Count; i++)
            {
                dtLocationCode.Rows[i].SetField(0, i + 1);
                dtLocationCode.AcceptChanges();
            }
            if (dtLocationCode.Rows.Count < 1)
            {
                DataRow dr = dtLocationCode.NewRow();
                dtLocationCode.Rows.Add(dr);
                dtLocationCode.AcceptChanges();
                showLocationFirstRow = false;
            }
            if (showLocationFirstRow)
                LocationSrNo = dtLocationCode.Rows.Count + 1;
            else
                LocationSrNo = 1;

            BindLocationCodeTable();
        }
        protected void Location_btnAdd_Click(object sender, EventArgs e)
        {
            if (dtLocationCode.Rows[0][0].ToString() == "")
            {
                dtLocationCode.Rows[0].Delete();
                dtLocationCode.AcceptChanges();
                showLocationFirstRow = true;
            }

            DataRow dr = dtLocationCode.NewRow();
            dr[0] = LocationSrNo.ToString();
            dr[1] = ((DropDownList)gvLocation.FooterRow.FindControl("ddlLocation")).SelectedItem.Value;
            dr[2] = ((DropDownList)gvLocation.FooterRow.FindControl("ddlLocation")).SelectedItem.Text;
            dr[3] = ((TextBox)gvLocation.FooterRow.FindControl("txtDesc")).Text;
            dr[4] = ((DropDownList)gvLocation.FooterRow.FindControl("ddlCompanies")).SelectedItem.Value;
            dr[5] = ((DropDownList)gvLocation.FooterRow.FindControl("ddlCompanies")).SelectedItem.Text;
            dtLocationCode.Rows.Add(dr);
            dtLocationCode.AcceptChanges();
            LocationSrNo += 1;
            BindLocationCodeTable();
            ((Label)gvLocation.FooterRow.FindControl("txtSrNo")).Text = LocationSrNo.ToString();
        }


        
        protected void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            //DropDownList ddlCom = gv.FooterRow.FindControl("ddlCompanies") as DropDownList;
            objDB.CompanyID = Convert.ToInt32(((DropDownList)gvLocation.FooterRow.FindControl("ddlCompanies")).SelectedItem.Value);
            DropDownList ddlLoc = gvLocation.FooterRow.FindControl("ddlLocation") as DropDownList;
            ddlLoc.Enabled = true;
            ddlLoc.DataSource = objDB.GetAllLocations(ref errorMsg);
            ddlLoc.DataTextField = "NAME";
            ddlLoc.DataValueField = "LOCATION_ID";
            ddlLoc.DataBind();
            ddlLoc.Items.Insert(0, new ListItem("--- Select Location ---", "0"));

        }

        protected void btnsaveNext_ServerClick(object sender, EventArgs e)
        {
            //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            //objDB.DeletedBy = Session["UserName"].ToString();
            //objDB.DelCOA_Location();

            //dtLocationCode = (DataTable)ViewState["dtLocationCode"] as DataTable;
            //if (dtLocationCode != null)
            //{
            //    if (dtLocationCode.Rows.Count > 0)
            //    {
            //        if (!showLocationFirstRow)
            //        {
            //            dtLocationCode.Rows[0].Delete();
            //            dtLocationCode.AcceptChanges();
            //        }

            //        for (int i = 0; i < dtLocationCode.Rows.Count; i++)
            //        {
            //            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            //            objDB.LocationID = Convert.ToInt32(dtLocationCode.Rows[i]["LocationID"].ToString());
            //            objDB.Code = dtLocationCode.Rows[i]["LocationCode"].ToString();
            //            objDB.CreatedBy = Session["UserName"].ToString();
            //            objDB.AddCOA_Location();
            //        }
            //    }
            //}

            Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/chart-of-account-step-03");
        }
    }
}