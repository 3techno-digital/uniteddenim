﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.manage.coa
{
    public partial class Category : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COAID
        {
            get
            {
                if (ViewState["COAID"] != null)
                {
                    return (int)ViewState["COAID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COAID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/chart-of-account-step-02";

                    ViewState["showFirstRow"] = null;
                    ViewState["srNo"] = null;
                    ViewState["dtCategoryCode"] = null;
                    dtCategoryCode = createCategoryCodesTable();
                    BindCategoryCodesTable();



                    divAlertMsg.Visible = false;
                    clearFields();
                    GetData();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["COAID"] != null)
                    {
                        COAID = Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString());
                        getDataByID(COAID);
                        CheckAccess();
                    }
                    else
                    {

                    }


                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }



        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "FAM_COA";
                objDB.PrimaryColumnnName = "COA_ID";
                objDB.PrimaryColumnValue = COAID.ToString();
                objDB.DocName = "ConfigureCOA";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void GetData()
        {
            CheckSessions();

            // Category Code Start
            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            DataTable dt = new DataTable();
            dt = objDB.GetAllCOA_Category(ref errorMsg);
            if (dt == null)
            {
                dt = new DataTable();
                dt.Columns.Add("Name");
                dt.Columns.Add("Code");
                dt.AcceptChanges();

                dt.Rows.Add(new object[] {"Asset", "1"});
                dt.AcceptChanges();

                dt.Rows.Add(new object[] {"Liabilities", "2"});
                dt.AcceptChanges();

                dt.Rows.Add(new object[] {"Equity", "3"});
                dt.AcceptChanges();

                dt.Rows.Add(new object[] {"Revenue", "4"});
                dt.AcceptChanges();

                dt.Rows.Add(new object[] {"CGS/COS", "5"});
                dt.AcceptChanges();

                dt.Rows.Add(new object[] { "Expenses", "6"});
                dt.AcceptChanges();

                dt.Rows.Add(new object[] {"Other Rev.", "7"});
                dt.AcceptChanges();

                dt.Rows.Add(new object[] {"Other Exp.", "8"});
                dt.AcceptChanges();
          
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                            objDB.Name = dt.Rows[i]["Name"].ToString();
                            objDB.Code = dt.Rows[i]["Code"].ToString();
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddCOA_Category();
                        }
                    }
                }
            }

            dtCategoryCode = null;
            dtCategoryCode = new DataTable();
            dtCategoryCode = createCategoryCodesTable();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtCategoryCode.Rows[0][0].ToString() == "")
                    {
                        dtCategoryCode.Rows[0].Delete();
                        dtCategoryCode.AcceptChanges();
                        showFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtCategoryCode.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Name"],
                            dt.Rows[i]["Code"]
                        });
                    }
                    srNo = Convert.ToInt32(dtCategoryCode.Rows[dtCategoryCode.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            BindCategoryCodesTable();

          
            // Company Code End


        }

        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.ChartOfAccountID = COAID;
            dt = objDB.GetCOAByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                }
            }

        }

        private void clearFields()
        {

            txtNotes.Value = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());


                objDB.Notes = txtNotes.Value;

                if (HttpContext.Current.Items["COAID"] != null)
                {
                    // update coding
                    objDB.ChartOfAccountID = COAID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOA();
                }
                else
                {
                    // add coding
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOA();
                    clearFields();
                }

                if (res == "New COA Added" || res == "COA Updated")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }





        private DataTable dtCategoryCode
        {
            get
            {
                if (ViewState["dtCategoryCode"] != null)
                {
                    return (DataTable)ViewState["dtCategoryCode"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtCategoryCode"] = value;
            }
        }
        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }
        private DataTable createCategoryCodesTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("CategoryName");
            dt.Columns.Add("Code");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindCategoryCodesTable()
        {
            if (ViewState["dtCategoryCode"] == null)
            {
                dtCategoryCode = createCategoryCodesTable();
                ViewState["dtCategoryCode"] = dtCategoryCode;
            }

            gv.DataSource = dtCategoryCode;
            gv.DataBind();

            if (showFirstRow)
                gv.Rows[0].Visible = true;
            else
                gv.Rows[0].Visible = false;

            gv.UseAccessibleHeader = true;
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                
                //Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                //txtSrNo.Text = srNo.ToString();
            }


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                   
                }
            }
        }
        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindCategoryCodesTable();
        }
        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindCategoryCodesTable();
        }
        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            dtCategoryCode.Rows[index].SetField(1, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditCategory")).Text);
            dtCategoryCode.Rows[index].SetField(2, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditDesc")).Text);
            dtCategoryCode.AcceptChanges();

            gv.EditIndex = -1;
            BindCategoryCodesTable();
        }
        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtCategoryCode.Rows.Count; i++)
            {
                if (dtCategoryCode.Rows[i][0].ToString() == delSr)
                {
                    dtCategoryCode.Rows[i].Delete();
                    dtCategoryCode.AcceptChanges();
                }
            }
            for (int i = 0; i < dtCategoryCode.Rows.Count; i++)
            {
                dtCategoryCode.Rows[i].SetField(0, i + 1);
                dtCategoryCode.AcceptChanges();
            }
            if (dtCategoryCode.Rows.Count < 1)
            {
                DataRow dr = dtCategoryCode.NewRow();
                dtCategoryCode.Rows.Add(dr);
                dtCategoryCode.AcceptChanges();
                showFirstRow = false;
            }
            if (showFirstRow)
                srNo = dtCategoryCode.Rows.Count + 1;
            else
                srNo = 1;

            BindCategoryCodesTable();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (dtCategoryCode.Rows[0][0].ToString() == "")
            {
                dtCategoryCode.Rows[0].Delete();
                dtCategoryCode.AcceptChanges();
                showFirstRow = true;
            }

            DataRow dr = dtCategoryCode.NewRow();
            dr[0] = srNo.ToString();
            dr[1] = ((TextBox)gv.FooterRow.FindControl("txtCategory")).Text;
            dr[2] = ((TextBox)gv.FooterRow.FindControl("txtDesc")).Text;
            dtCategoryCode.Rows.Add(dr);
            dtCategoryCode.AcceptChanges();
            srNo += 1;
            BindCategoryCodesTable();
            ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
        }


        protected void btnsaveNext_ServerClick(object sender, EventArgs e)
        {
            //objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            //objDB.DeletedBy = Session["UserName"].ToString();
            //objDB.DelCOA_Category();

            //dtCategoryCode = (DataTable)ViewState["dtCategoryCode"] as DataTable;
            //if (dtCategoryCode != null)
            //{
            //    if (dtCategoryCode.Rows.Count > 0)
            //    {
            //        if (!showFirstRow)
            //        {
            //            dtCategoryCode.Rows[0].Delete();
            //            dtCategoryCode.AcceptChanges();
            //        }

            //        for (int i = 0; i < dtCategoryCode.Rows.Count; i++)
            //        {
            //            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            //            objDB.Name = dtCategoryCode.Rows[i]["CategoryName"].ToString();
            //            objDB.Code = dtCategoryCode.Rows[i]["Code"].ToString();
            //            objDB.CreatedBy = Session["UserName"].ToString();
            //            objDB.AddCOA_Category();
            //        }
            //    }
            //}

            Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/chart-of-account-step-04");
        }
    }
}