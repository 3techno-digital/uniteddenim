﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.manage.coa
{
    public partial class coa_old : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COAID
        {
            get
            {
                if (ViewState["COAID"] != null)
                {
                    return (int)ViewState["COAID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COAID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    BindSupplier();
                    BindCompanies();
                    BindLocations();
                    checkCompany.Checked = true;
                    CheckLocation.Checked = true;
                    CheckCategory.Checked = true;
                    CheckSubCategory.Checked = true;
                    CheckAccountType.Checked = true;
                    CheckSubAccount.Checked = true;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/coa";



                    divAlertMsg.Visible = false;
                    clearFields();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["COAID"] != null)
                    {
                        COAID = Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString());
                        DocStatus = getDataByID(COAID);
                        CheckAccess();
                    }
                    else
                    {

                    }

                    isShowContainer.Visible = chkCostCenter.Checked;

                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void BindSupplier()
        {
            ddlSupplier.Items.Clear();
            ddlSupplier.DataSource = objDB.GetAllSupplier(ref errorMsg);
            ddlSupplier.DataTextField = "SupplierName";
            ddlSupplier.DataValueField = "SupplierID";
            ddlSupplier.DataBind();
            ddlSupplier.Items.Insert(0, new ListItem("--- Select Supplier- --", "0"));
        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "FAM_COA";
                objDB.PrimaryColumnnName = "COA_ID";
                objDB.PrimaryColumnValue = COAID.ToString();
                objDB.DocName = "ConfigureCOA";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private string getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.ChartOfAccountID = COAID;
            dt = objDB.GetCOAByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtAccDep.Value = dt.Rows[0]["ACCUMULATED_DEP_CODE"].ToString();
                    txtAmor.Value = dt.Rows[0]["AMORTIZATION_CODE"].ToString();
                    txtBank.Value = dt.Rows[0]["BANK"].ToString();
                    txtDepCode.Value = dt.Rows[0]["DEPRICIATION_CODE"].ToString();
                    txtDescription.Value = dt.Rows[0]["DESCRIPTIONS"].ToString();
                    txtGainLoss.Value = dt.Rows[0]["GAIN_LOSS"].ToString();
                    txtGLCode.Value = dt.Rows[0]["GL_CODE"].ToString();
                    ddlSupplier.SelectedValue = dt.Rows[0]["SUPPLIER"].ToString();
                    //  txtAssCode.Value = dt.Rows[0]["ASSET_CODE"].ToString();
                    chkCostCenter.Checked = (dt.Rows[0]["COST_CENTER"].ToString() == "1") ? true : false;
                    // txtNotes.Value = dt.Rows[0]["Notes"].ToString();
                    if (dt.Rows[0]["CompanyName"].ToString() == "0")
                    {
                        checkCompany.Checked = false;
                        ddlCompany.Enabled = false;
                        txtCompanyCode.Enabled = false;

                    }
                    else
                        ddlCompany.SelectedValue = dt.Rows[0]["CompanyName"].ToString();
                    txtCompanyCode.Text = dt.Rows[0]["CompanyCode"].ToString();
                    if (dt.Rows[0]["GeoLocation"].ToString() == "0")
                    {
                        CheckLocation.Checked = false;
                        ddlLocation.Enabled = false;
                        txtLocationCode.Enabled = false;

                    }
                    ddlLocation.SelectedValue = dt.Rows[0]["GeoLocation"].ToString();
                    txtLocationCode.Text = dt.Rows[0]["GeoLocationCode"].ToString();


                    if (dt.Rows[0]["Category"].ToString() == "----")
                    {
                        CheckCategory.Checked = false;
                        txtCategory.Disabled = true;
                        txtCategoryCode.Enabled = false;

                    }
                    txtCategory.Value = dt.Rows[0]["Category"].ToString();
                    txtCategoryCode.Text = dt.Rows[0]["CategoryCode"].ToString();
                    if (dt.Rows[0]["SubCategory"].ToString() == "----")
                    {
                        CheckSubCategory.Checked = false;
                        txtSubCategory.Disabled = true;
                        txtSubCategoryCode.Enabled = false;

                    }
                    txtSubCategory.Value = dt.Rows[0]["SubCategory"].ToString();
                    txtSubCategoryCode.Text = dt.Rows[0]["SubCategoryCode"].ToString();
                    if (dt.Rows[0]["Account"].ToString() == "----")
                    {
                        CheckAccountType.Checked = false;
                        txtAccountType.Disabled = true;
                        txtAccountTypeCode.Enabled = false;

                    }
                    txtAccountType.Value = dt.Rows[0]["Account"].ToString();
                    txtAccountTypeCode.Text = dt.Rows[0]["AccountCode"].ToString();
                    if (dt.Rows[0]["SubAccount"].ToString() == "----")
                    {
                        CheckSubAccount.Checked = false;
                        txtSubAccount.Disabled = true;
                        txtSubAccountCode.Enabled = false;

                    }
                    txtSubAccount.Value = dt.Rows[0]["SubAccount"].ToString();
                    txtSubAccountCode.Text = dt.Rows[0]["SubAccountCode"].ToString();



                    return dt.Rows[0]["DocStatus"].ToString();
                }
            }

            return null;
        }

        private void clearFields()
        {
            txtAccDep.Value = "";
            txtAmor.Value = "";
            txtBank.Value = "";
            txtDepCode.Value = "";
            txtDescription.Value = "";
            txtGainLoss.Value = "";
            txtGLCode.Value = "";
            ddlSupplier.SelectedValue = "0";
            txtNotes.Value = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.GLCode = txtGLCode.Value;
                objDB.Description = txtDescription.Value;
                objDB.CostCenter = ((chkCostCenter.Checked) ? 1 : 0);
                objDB.AssetCode = "";
                objDB.DepreciationCode = txtDepCode.Value;
                objDB.AccumulatedDepreciationCode = txtAccDep.Value;
                objDB.AmortizationCode = txtAmor.Value;
                objDB.GainLoss = txtGainLoss.Value;
                objDB.Bank = txtBank.Value;


                objDB.CompanyID2 = Convert.ToInt32(ddlCompany.SelectedValue);
                objDB.CompanyName = ddlCompany.Text;
                objDB.CompanyCode = txtCompanyCode.Text;
                objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
                objDB.GeoLocation = ddlLocation.Text;
                objDB.GeoLocationCode = txtLocationCode.Text;
                objDB.Category = txtCategory.Value;
                objDB.CategoryCode = txtCategoryCode.Text;
                objDB.SubCategoryName = txtSubCategory.Value;
                objDB.SubCategoryCode = txtSubCategoryCode.Text;
                objDB.Account = txtAccountType.Value;
                objDB.AccountCode = txtAccountTypeCode.Text;
                objDB.SubAccount = txtSubAccount.Value;
                objDB.SubAccountCode = txtSubAccountCode.Text;




                objDB.Notes = txtNotes.Value;
                objDB.SupplierID = int.Parse(ddlSupplier.SelectedValue);

                if (HttpContext.Current.Items["COAID"] != null)
                {
                    // update coding
                    objDB.ChartOfAccountID = COAID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOA();
                }
                else
                {
                    // add coding
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOA();
                    clearFields();
                }

                if (res == "New COA Added" || res == "COA Updated")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }



        protected void Unnamed_CheckedChanged(object sender, EventArgs e)
        {
            isShowContainer.Visible = chkCostCenter.Checked;
        }

        protected void checkCompany_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCompany.Checked)
            {
                ddlCompany.Enabled = true;
                txtCompanyCode.Enabled = true;

            }
            else
            {
                ddlCompany.SelectedIndex = 0;
                txtCompanyCode.Text = "##";
                ddlCompany.Enabled = false;
                txtCompanyCode.Enabled = false;
                txtCompanyCode_TextChanged(null, null);

            }
        }

        protected void CheckLocation_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckLocation.Checked)
            {
                ddlLocation.Enabled = true;
                txtLocationCode.Enabled = true;
            }
            else
            {
                ddlLocation.SelectedIndex = 0;
                txtLocationCode.Text = "##";
                ddlLocation.Enabled = false;
                txtLocationCode.Enabled = false;
                txtCompanyCode_TextChanged(null, null);

            }
        }

        protected void CheckCategory_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckCategory.Checked)
            {
                txtCategory.Disabled = false;
                txtCategoryCode.Enabled = true;

            }
            else
            {
                txtCategory.Value = "----";
                txtCategoryCode.Text = "##";
                txtCategory.Disabled = true;
                txtCategoryCode.Enabled = false;
                txtCompanyCode_TextChanged(null, null);

            }
        }

        protected void CheckSubCategory_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckSubCategory.Checked)
            {
                txtSubCategory.Disabled = false;
                txtSubCategoryCode.Enabled = true;

            }
            else
            {
                txtSubCategory.Value = "----";
                txtSubCategoryCode.Text = "##";
                txtSubCategory.Disabled = true;
                txtSubCategoryCode.Enabled = false;
                txtCompanyCode_TextChanged(null, null);

            }
        }

        protected void CheckAccountType_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckAccountType.Checked)
            {
                txtAccountType.Disabled = false;
                txtAccountTypeCode.Enabled = true;

            }
            else
            {
                txtAccountType.Value = "----";
                txtAccountTypeCode.Text = "##";
                txtAccountType.Disabled = true;
                txtAccountTypeCode.Enabled = false;
                txtCompanyCode_TextChanged(null, null);

            }
        }

        protected void CheckSubAccount_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckSubAccount.Checked)
            {
                txtSubAccount.Disabled = false;
                txtSubAccountCode.Enabled = true;

            }
            else
            {

                txtSubAccount.Disabled = true;
                txtSubAccountCode.Enabled = false;
                txtSubAccount.Value = "----";
                txtSubAccountCode.Text = "##";
                txtCompanyCode_TextChanged(null, null);
            }
        }

        private void BindCompanies()
        {
            ddlCompany.Items.Clear();
            objDB.ParentCompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCompany.DataSource = objDB.GetCompaniesByParentID(ref errorMsg);
            ddlCompany.DataTextField = "CompanyName";
            ddlCompany.DataValueField = "CompanyID";
            ddlCompany.DataBind();
            ddlCompany.Items.Insert(0, new ListItem("-- Select --", "0"));
        }
        private void BindLocations()
        {
            ddlLocation.Items.Clear();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlLocation.DataSource = objDB.GetAllApprovedLocations(ref errorMsg);
            ddlLocation.DataTextField = "NAME";
            ddlLocation.DataValueField = "LOCATION_ID";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("-- Select --", "0"));
        }

        protected void txtCompanyCode_TextChanged(object sender, EventArgs e)
        {
            string com = txtCompanyCode.Text;
            string loc = txtLocationCode.Text;
            string cat = txtCategoryCode.Text;
            string scat = txtSubCategoryCode.Text;
            string acc = txtAccountTypeCode.Text;
            string sacc = txtSubAccountCode.Text;
            if (!checkCompany.Checked)
            { com = ""; }
            if (!CheckLocation.Checked)
            { loc = ""; }
            if (!CheckCategory.Checked)
            { cat = ""; }
            if (!CheckSubCategory.Checked)
            { scat = ""; }
            if (!CheckAccountType.Checked)
            { acc = ""; }
            if (!CheckSubAccount.Checked)
            { sacc = ""; }
            txtGLCode.Value = com + loc + cat + scat + acc + sacc;
        }


    }
}