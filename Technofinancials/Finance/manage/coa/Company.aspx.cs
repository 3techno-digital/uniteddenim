﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage.coa
{
    public partial class Company : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COAID
        {
            get
            {
                if (ViewState["COAID"] != null)
                {
                    return (int)ViewState["COAID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COAID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {
                    CheckSessions();


                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/dashboard";

                    ViewState["showFirstRow"] = null;
                    ViewState["srNo"] = null;
                    ViewState["dtCompanyCode"] = null;
                    dtCompanyCode = createCompanyCodesTable();
                    BindCompanyCodesTable();

                  

                    divAlertMsg.Visible = false;
                    clearFields();
                    GetData();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["COAID"] != null)
                    {
                        COAID = Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString());
                        getDataByID(COAID);
                        CheckAccess();
                    }
                    else
                    {

                    }


                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

     

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "FAM_COA";
                objDB.PrimaryColumnnName = "COA_ID";
                objDB.PrimaryColumnValue = COAID.ToString();
                objDB.DocName = "ConfigureCOA";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void GetData()
        {
            CheckSessions();

            // Company Code Start
            DataTable dt = new DataTable();

            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            dt = objDB.GetCompaniesByParentID(ref errorMsg);
            
            //if (dt == null)
            //{
            //    objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            //    dt = objDB.GetCompaniesByParentID(ref errorMsg);
            //    dt.Columns.Add("Code");
            //    dt.AcceptChanges();
            //    if (dt != null)
            //    {
            //        for (int i = 0; i < dt.Rows.Count; i++)
            //        {
            //            if (i < 9)
            //            {
            //                dt.Rows[i]["Code"] = "0" + (i + 1).ToString();
            //            }
            //            else
            //            {
            //                dt.Rows[i]["Code"] = (i + 1).ToString();
            //            }
            //            dt.AcceptChanges();
            //        }
            //    }

            //}

            dtCompanyCode = null;
            dtCompanyCode = new DataTable();
            dtCompanyCode = createCompanyCodesTable();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtCompanyCode.Rows[0][0].ToString() == "")
                    {
                        dtCompanyCode.Rows[0].Delete();
                        dtCompanyCode.AcceptChanges();
                        showFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtCompanyCode.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["CompanyID"],
                            dt.Rows[i]["CompanyName"],
                            dt.Rows[i]["CompanyCode"]
                        });
                    }
                    srNo = Convert.ToInt32(dtCompanyCode.Rows[dtCompanyCode.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            BindCompanyCodesTable();


            // Company Code End


        }

        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.ChartOfAccountID = COAID;
            dt = objDB.GetCOAByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                   
                }
            }

        }

        private void clearFields()
        {
           
            txtNotes.Value = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                




                objDB.Notes = txtNotes.Value;

                if (HttpContext.Current.Items["COAID"] != null)
                {
                    // update coding
                    objDB.ChartOfAccountID = COAID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOA();
                }
                else
                {
                    // add coding
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOA();
                    clearFields();
                }

                if (res == "New COA Added" || res == "COA Updated")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }



       

        private DataTable dtCompanyCode
        {
            get
            {
                if (ViewState["dtCompanyCode"] != null)
                {
                    return (DataTable)ViewState["dtCompanyCode"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtCompanyCode"] = value;
            }
        }
        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }
        private DataTable createCompanyCodesTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("CompanyID");
            dt.Columns.Add("CompanyName");
            dt.Columns.Add("Code");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindCompanyCodesTable()
        {
            if (ViewState["dtCompanyCode"] == null)
            {
                dtCompanyCode = createCompanyCodesTable();
                ViewState["dtCompanyCode"] = dtCompanyCode;
            }

            gv.DataSource = dtCompanyCode;
            gv.DataBind();

            if (showFirstRow)
                gv.Rows[0].Visible = true;
            else
                gv.Rows[0].Visible = false;

            gv.UseAccessibleHeader = true;
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                //objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"]);
                //DropDownList ddlCom = e.Row.FindControl("ddlCompanies") as DropDownList;
                //ddlCom.DataSource = objDB.GetCompaniesByParentID(ref errorMsg);
                //ddlCom.DataTextField = "CompanyName";
                //ddlCom.DataValueField = "CompanyID";
                //ddlCom.DataBind();
                //ddlCom.Items.Insert(0, new ListItem("--- Select Company ---", "0"));


                //Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                //txtSrNo.Text = srNo.ToString();
            }


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    //objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"]);
                    //DropDownList ddlComEdit = e.Row.FindControl("ddlEditCompanies") as DropDownList;
                    //ddlComEdit.DataSource = objDB.GetCompaniesByParentID(ref errorMsg);
                    //ddlComEdit.DataTextField = "CompanyName";
                    //ddlComEdit.DataValueField = "CompanyID";
                    //ddlComEdit.DataBind();
                    //ddlComEdit.Items.Insert(0, new ListItem("--- Select Company ---", "0"));


                }
            }
        }
        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindCompanyCodesTable();
        }
        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindCompanyCodesTable();
        }
        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            //dtCompanyCode.Rows[index].SetField(1, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditCompanies")).SelectedItem.Text);
            dtCompanyCode.Rows[index].SetField(3, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditDesc")).Text);
            dtCompanyCode.AcceptChanges();

            gv.EditIndex = -1;
            BindCompanyCodesTable();
        }
        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtCompanyCode.Rows.Count; i++)
            {
                if (dtCompanyCode.Rows[i][0].ToString() == delSr)
                {
                    dtCompanyCode.Rows[i].Delete();
                    dtCompanyCode.AcceptChanges();
                }
            }
            for (int i = 0; i < dtCompanyCode.Rows.Count; i++)
            {
                dtCompanyCode.Rows[i].SetField(0, i + 1);
                dtCompanyCode.AcceptChanges();
            }
            if (dtCompanyCode.Rows.Count < 1)
            {
                DataRow dr = dtCompanyCode.NewRow();
                dtCompanyCode.Rows.Add(dr);
                dtCompanyCode.AcceptChanges();
                showFirstRow = false;
            }
            if (showFirstRow)
                srNo = dtCompanyCode.Rows.Count + 1;
            else
                srNo = 1;

            BindCompanyCodesTable();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (dtCompanyCode.Rows[0][0].ToString() == "")
            {
                dtCompanyCode.Rows[0].Delete();
                dtCompanyCode.AcceptChanges();
                showFirstRow = true;
            }

            DataRow dr = dtCompanyCode.NewRow();
            dr[0] = srNo.ToString();
            dr[1] = ((DropDownList)gv.FooterRow.FindControl("ddlCompanies")).SelectedItem.Value;
            dr[2] = ((DropDownList)gv.FooterRow.FindControl("ddlCompanies")).SelectedItem.Text;
            dr[3] = ((TextBox)gv.FooterRow.FindControl("txtDesc")).Text;
            dtCompanyCode.Rows.Add(dr);
            dtCompanyCode.AcceptChanges();
            srNo += 1;
            BindCompanyCodesTable();
            ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
        }


        protected void btnsaveNext_ServerClick(object sender, EventArgs e)
        {
            //objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            //objDB.DeletedBy = Session["UserName"].ToString();
            //objDB.DelCOA_Company();

            //dtCompanyCode = (DataTable)ViewState["dtCompanyCode"] as DataTable;
            //if (dtCompanyCode != null)
            //    {
            //        if (dtCompanyCode.Rows.Count > 0)
            //        {
            //            if (!showFirstRow)
            //            {
            //                dtCompanyCode.Rows[0].Delete();
            //                dtCompanyCode.AcceptChanges();
            //            }

            //            for (int i = 0; i < dtCompanyCode.Rows.Count; i++)
            //            {
            //                objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            //                objDB.CompanyID = Convert.ToInt32(dtCompanyCode.Rows[i]["CompanyID"].ToString()); 
            //                objDB.Code = dtCompanyCode.Rows[i]["Code"].ToString();
            //                objDB.CreatedBy = Session["UserName"].ToString();
            //                objDB.AddCOA_Company();
            //            }
            //        }
            //    }

            Response.Redirect("/"+ Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/chart-of-account-step-02");
        }
    }
}