﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="coa-old.aspx.cs" Inherits="Technofinancials.Finance.manage.coa.coa_old" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/chart-of-account.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Chart of Accounts</h3>
                        </div>


                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>


                                <div class="col-sm-4">
                                    <div class="pull-right flex">
                                        <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                        <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                        <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class"  CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                        <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                        <button class="tf-save-btn" "Reject & Disapproved" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>


                                        <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                        <div class="clearfix">&nbsp;</div>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                <asp:CheckBox ID="checkCompany" runat="server" AutoPostBack="true" OnCheckedChanged="checkCompany_CheckedChanged" />
                                                Company
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="ddlCompany" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control form-text input-group js-example-basic-single" ID="ddlCompany"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Code
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtCompanyCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <asp:TextBox class="form-control" ID="txtCompanyCode" Text="00" runat="server" OnTextChanged="txtCompanyCode_TextChanged" AutoPostBack="true"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>

                                                <asp:CheckBox ID="CheckLocation" runat="server" AutoPostBack="true" OnCheckedChanged="CheckLocation_CheckedChanged" />
                                                Location
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlLocation" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class="select2 form-control form-text input-group js-example-basic-single" ID="ddlLocation"></asp:DropDownList>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Code
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtLocationCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <asp:TextBox class="form-control" ID="txtLocationCode" Text="00" runat="server" OnTextChanged="txtCompanyCode_TextChanged" AutoPostBack="true"> </asp:TextBox>

                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>

                                                <asp:CheckBox ID="CheckCategory" runat="server" AutoPostBack="true" OnCheckedChanged="CheckCategory_CheckedChanged" />
                                                Category
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtCategory" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <input type="text" class="form-control" id="txtCategory" runat="server" placeholder="Category" />
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Code
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtCategoryCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <asp:TextBox class="form-control" ID="txtCategoryCode" Text="00" runat="server" OnTextChanged="txtCompanyCode_TextChanged" AutoPostBack="true"> </asp:TextBox>

                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>

                                                <asp:CheckBox ID="CheckSubCategory" runat="server" AutoPostBack="true" OnCheckedChanged="CheckSubCategory_CheckedChanged" />
                                                Sub Category
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtSubCategory" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <input type="text" class="form-control" id="txtSubCategory" runat="server" placeholder="Sub Category" />
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Code
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtSubCategoryCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <asp:TextBox class="form-control" ID="txtSubCategoryCode" Text="00" runat="server" OnTextChanged="txtCompanyCode_TextChanged" AutoPostBack="true"> </asp:TextBox>

                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>

                                                <asp:CheckBox ID="CheckAccountType" runat="server" AutoPostBack="true" OnCheckedChanged="CheckAccountType_CheckedChanged" />
                                                Account Type
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtAccountType" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <input type="text" class="form-control" id="txtAccountType" runat="server" placeholder="Account Type" />
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Code
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="txtAccountTypeCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <asp:TextBox class="form-control" ID="txtAccountTypeCode" Text="00" runat="server" OnTextChanged="txtCompanyCode_TextChanged" AutoPostBack="true"> </asp:TextBox>

                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>

                                                <asp:CheckBox ID="CheckSubAccount" runat="server" AutoPostBack="true" OnCheckedChanged="CheckSubAccount_CheckedChanged" />
                                                Sub Account
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtSubAccount" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <input type="text" class="form-control" id="txtSubAccount" runat="server" placeholder="Sub Account" />
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Code
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" ControlToValidate="txtSubAccountCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            </label>
                                            <asp:TextBox class="form-control" ID="txtSubAccountCode" Text="00" runat="server" OnTextChanged="txtCompanyCode_TextChanged" AutoPostBack="true"> </asp:TextBox>

                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                GL Code
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtGLCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <input type="text" class="form-control" id="txtGLCode" runat="server" placeholder="GL Code" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <label>Description </label>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" id="txtDescription" runat="server" name="Description"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Cost Center</label>
                                        <div class="form-group checkbox-div">
                                            <asp:CheckBox Text="" ID="chkCostCenter" OnCheckedChanged="Unnamed_CheckedChanged" AutoPostBack="true" runat="server" />
                                        </div>
                                    </div>

                                </div>
                                <div class="row" id="isShowContainer" runat="server">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <%--<div class="col-md-4">
                            <label>Asset Code</label>
                                   <input type="text" class="form-control"  id="txtAssCode"  runat="server" placeholder="Asset Code" />
                           
                            </div>--%>

                                            <div class="col-md-4">
                                                <label>Accumulated Depreciation Account</label>
                                                <input type="text" class="form-control" id="txtAccDep" runat="server" placeholder="Accumulated Depreciation Account" />

                                            </div>
                                            <div class="col-md-4">
                                                <label>Depreciation Expense Account</label>
                                                <input type="text" class="form-control" id="txtDepCode" runat="server" placeholder="Depreciation Expense Account" />

                                            </div>
                                            <div class="col-md-4">
                                                <label>Supplier</label>
                                                <div class="input-group input-group-lg">
                                                    <asp:DropDownList ClientIDMode="Static" runat="server" data-plugin="select2" class=" select2 form-control form-text input-group js-example-basic-single" ID="ddlSupplier">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Amortization Code</label>
                                                <input type="text" class="form-control" id="txtAmor" runat="server" placeholder="Amortization Code" />

                                            </div>
                                            <div class="col-md-4">
                                                <label>Gain/loss Account</label>
                                                <input type="text" class="form-control" id="txtGainLoss" runat="server" placeholder="Gain/loss Account" />

                                            </div>
                                            <div class="col-md-4">
                                                <label>Bank</label>
                                                <input type="text" class="form-control" id="txtBank" runat="server" placeholder="Bank" />

                                            </div>
                                        </div>


                                        <div class="row">
                                        </div>


                                    </div>
                                </div>
                                <div class="clearfix">&nbsp;</div>




                            </ContentTemplate>
                        </asp:UpdatePanel>
                </section>


                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }


            .aspNetDisabled {
                width: 100% !important;
                background: #eeeeee;
                height: 46px;
                padding: 10px 16px;
                font-size: 18px;
                line-height: 1.3333333;
                border-radius: 6px;
                border-color: #ccc;
                outline: none;
                box-shadow: none;
            }
        </style>

        <style>
            .stepwizard-step p {
                margin-top: 10px;
            }

            .stepwizard-row {
                display: table-row;
            }

            .stepwizard {
                display: table;
                width: 50%;
                position: relative;
            }

            .stepwizard-step button[disabled] {
                opacity: 1 !important;
                filter: alpha(opacity=100) !important;
            }

            .stepwizard-row:before {
                top: 14px;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 100%;
                height: 1px;
                background-color: #ccc;
                z-order: 0;
            }

            .stepwizard-step {
                display: table-cell;
                text-align: center;
                position: relative;
            }

            .btn-circle {
                width: 30px;
                height: 30px;
                text-align: center;
                padding: 6px 0;
                font-size: 12px;
                line-height: 1.428571429;
                border-radius: 15px;
            }

            button.btn {
                margin-top: 25px;
                margin-bottom: 48px;
                margin-right: 30px;
            }

            th {
                border-right: 1px solid #fff;
                text-align: center;
            }
        </style>
        <script>
            $("#button").click(function () {
                $("form1").valid();
            });
        </script>

        <script>
            $(document).ready(function () {
                var navListItems = $('div.setup-panel div a'),
                    allWells = $('.setup-content'),
                    allNextBtn = $('.nextBtn');

                allWells.hide();

                navListItems.click(function (e) {
                    e.preventDefault();
                    var $target = $($(this).attr('href')),
                        $item = $(this);

                    if (!$item.hasClass('disabled')) {
                        navListItems.removeClass('btn-primary').addClass('btn-default');
                        $item.addClass('btn-primary');
                        allWells.hide();
                        $target.show();
                        $target.find('input:eq(0)').focus();
                    }
                });

                allNextBtn.click(function () {
                    var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                        curInputs = curStep.find("input[type='text'],input[type='url']"),
                        isValid = true;

                    $(".form-group").removeClass("has-error");
                    for (var i = 0; i < curInputs.length; i++) {
                        if (!curInputs[i].validity.valid) {
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                    }

                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                });

                $('div.setup-panel div a.btn-primary').trigger('click');
            });
        </script>
    </form>
</body>
</html>


