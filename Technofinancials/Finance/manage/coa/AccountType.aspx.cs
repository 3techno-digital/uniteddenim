﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.manage.coa
{
    public partial class AccountType : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COAID
        {
            get
            {
                if (ViewState["COAID"] != null)
                {
                    return (int)ViewState["COAID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COAID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/chart-of-account-step-04";

                    ViewState["showLocationFirstRow"] = null;
                    ViewState["LocationSrNo"] = null;
                    ViewState["dtLocationCode"] = null;
                    dtAccountCode = createAccountCodeTable();
                    BindAccountCodeTable();



                    divAlertMsg.Visible = false;
                    clearFields();
                    GetData();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["COAID"] != null)
                    {
                        COAID = Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString());
                        getDataByID(COAID);
                        CheckAccess();
                    }
                    else
                    {

                    }



                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "FAM_COA";
                objDB.PrimaryColumnnName = "COA_ID";
                objDB.PrimaryColumnValue = COAID.ToString();
                objDB.DocName = "ConfigureCOA";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void GetData()
        {
            CheckSessions();

            // Location Code Start
            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            DataTable dt = new DataTable();
            dt = objDB.GetAllCOA_Account(ref errorMsg);


            dtAccountCode = null;
            dtAccountCode = new DataTable();
            dtAccountCode = createAccountCodeTable();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtAccountCode.Rows[0][0].ToString() == "")
                    {
                        dtAccountCode.Rows[0].Delete();
                        dtAccountCode.AcceptChanges();
                        showAccountFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtAccountCode.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Name"],
                            dt.Rows[i]["Code"],
                             dt.Rows[i]["SubCategoryID"],
                            dt.Rows[i]["SubCategoryName"],
                            dt.Rows[i]["AccountID"]
                        });
                    }
                    AccountSrNo = Convert.ToInt32(dtAccountCode.Rows[dtAccountCode.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            BindAccountCodeTable();


            // Company Code End


        }

        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.ChartOfAccountID = COAID;
            dt = objDB.GetCOAByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {




                }
            }

        }

        private void clearFields()
        {
            txtNotes.Value = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.Notes = txtNotes.Value;

                if (HttpContext.Current.Items["COAID"] != null)
                {
                    // update coding
                    objDB.ChartOfAccountID = COAID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOA();
                }
                else
                {
                    // add coding
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOA();
                    clearFields();
                }

                if (res == "New COA Added" || res == "COA Updated")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private DataTable dtAccountCode
        {
            get
            {
                if (ViewState["dtAccountCode"] != null)
                {
                    return (DataTable)ViewState["dtAccountCode"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtAccountCode"] = value;
            }
        }
        bool showAccountFirstRow
        {
            get
            {
                if (ViewState["showAccountFirstRow"] != null)
                {
                    return (bool)ViewState["showAccountFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showAccountFirstRow"] = value;
            }
        }
        protected int AccountSrNo
        {
            get
            {
                if (ViewState["AccountSrNo"] != null)
                {
                    return (int)ViewState["AccountSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["AccountSrNo"] = value;
            }
        }
        private DataTable createAccountCodeTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AccountSrNo");
            dt.Columns.Add("AccountName");
            dt.Columns.Add("AccountCode");           
            dt.Columns.Add("SubCategoryID");
            dt.Columns.Add("SubCategoryName");
            dt.Columns.Add("AccountID");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindAccountCodeTable()
        {
            if (ViewState["dtAccountCode"] == null)
            {
                dtAccountCode = createAccountCodeTable();
                ViewState["dtAccountCode"] = dtAccountCode;
            }

            gvAccount.DataSource = dtAccountCode;
            gvAccount.DataBind();

            if (showAccountFirstRow)
                gvAccount.Rows[0].Visible = true;
            else
                gvAccount.Rows[0].Visible = false;

            gvAccount.UseAccessibleHeader = true;
            gvAccount.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gvAccount_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"]);
                DropDownList ddlCom = e.Row.FindControl("ddlSubCategory") as DropDownList;
                ddlCom.DataSource = objDB.GetAllCOA_SubCategory(ref errorMsg);
                ddlCom.DataTextField = "Name";
                ddlCom.DataValueField = "SubCategoryID";
                ddlCom.DataBind();
                ddlCom.Items.Insert(0, new ListItem("--- Select SubCategory ---", "0"));

                //DropDownList ddlCom2 = e.Row.FindControl("ddlSubCategory") as DropDownList;
                //ddlCom2.DataSource = null;
                //ddlCom2.DataBind();
                //ddlCom2.Items.Insert(0, new ListItem("--- Select SubCategory ---", "0"));
                //ddlCom2.Enabled = false;

                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = AccountSrNo.ToString();
            }


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    //DropDownList ddlLocEdit = e.Row.FindControl("ddlEditAccount") as DropDownList;
                    //ddlLocEdit.DataSource = objDB.GetAllAccounts(ref errorMsg);
                    //ddlLocEdit.DataTextField = "NAME";
                    //ddlLocEdit.DataValueField = "Account_ID";
                    //ddlLocEdit.DataBind();
                    //ddlLocEdit.Items.Insert(0, new ListItem("--- Select Account ---", "0"));


                }
            }
        }
        protected void gvAccount_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvAccount.EditIndex = e.NewEditIndex;
            BindAccountCodeTable();
        }
        protected void gvAccount_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvAccount.EditIndex = -1;
            BindAccountCodeTable();
        }
        protected void gvAccount_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            objDB.COA_AccountID = Convert.ToInt32(((HiddenField)gvAccount.Rows[e.RowIndex].FindControl("hdnAccountID")).Value);
            objDB.Name = ((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditAccount")).Text;
            objDB.Code = ((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditDesc")).Text;
            objDB.ModifiedBy = Session["UserName"].ToString();
            string s = objDB.UpdateCOA_Account();


            // dtAccountCode.Rows[index].SetField(1, ((DropDownList)gvAccount.Rows[e.RowIndex].FindControl("ddlEditAccount")).SelectedItem.Text);
            dtAccountCode.Rows[index].SetField(1, ((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditAccount")).Text);
            dtAccountCode.Rows[index].SetField(2, ((TextBox)gvAccount.Rows[e.RowIndex].FindControl("txtEditDesc")).Text);
            dtAccountCode.AcceptChanges();

            gvAccount.EditIndex = -1;
            BindAccountCodeTable();
        }
        protected void Account_lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtAccountCode.Rows.Count; i++)
            {
                if (dtAccountCode.Rows[i][0].ToString() == delSr)
                {
                    string res = Common.addAccessLevels("Delete", "COA_Account", "AccountID", dtAccountCode.Rows[i]["AccountID"].ToString(), Session["UserName"].ToString());
                    GetData();

                    //dtAccountCode.Rows[i].Delete();
                    //dtAccountCode.AcceptChanges();
                }
            }
            //for (int i = 0; i < dtAccountCode.Rows.Count; i++)
            //{
            //    dtAccountCode.Rows[i].SetField(0, i + 1);
            //    dtAccountCode.AcceptChanges();
            //}
            //if (dtAccountCode.Rows.Count < 1)
            //{
            //    DataRow dr = dtAccountCode.NewRow();
            //    dtAccountCode.Rows.Add(dr);
            //    dtAccountCode.AcceptChanges();
            //    showAccountFirstRow = false;
            //}
            //if (showAccountFirstRow)
            //    AccountSrNo = dtAccountCode.Rows.Count + 1;
            //else
            //    AccountSrNo = 1;

            //BindAccountCodeTable();
        }
        protected void Account_btnAdd_Click(object sender, EventArgs e)
        {

            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            objDB.COA_SubCategoryID = Convert.ToInt32(((DropDownList)gvAccount.FooterRow.FindControl("ddlSubCategory")).SelectedItem.Value);
            objDB.Name = ((TextBox)gvAccount.FooterRow.FindControl("txtAccount")).Text;
            objDB.Code = ((TextBox)gvAccount.FooterRow.FindControl("txtDesc")).Text;
            objDB.CreatedBy = Session["UserName"].ToString();
            string s = objDB.AddCOA_Account();

            GetData();

            //if (dtAccountCode.Rows[0][0].ToString() == "")
            //{
            //    dtAccountCode.Rows[0].Delete();
            //    dtAccountCode.AcceptChanges();
            //    showAccountFirstRow = true;
            //}

            //DataRow dr = dtAccountCode.NewRow();
            //dr[0] = AccountSrNo.ToString();
            //dr[1] = ((TextBox)gvAccount.FooterRow.FindControl("txtAccount")).Text;
            //dr[2] = ((TextBox)gvAccount.FooterRow.FindControl("txtDesc")).Text;
            //dr[3] = ((DropDownList)gvAccount.FooterRow.FindControl("ddlSubCategory")).SelectedItem.Value;
            //dr[4] = ((DropDownList)gvAccount.FooterRow.FindControl("ddlSubCategory")).SelectedItem.Text;
            //dtAccountCode.Rows.Add(dr);
            //dtAccountCode.AcceptChanges();
            //AccountSrNo += 1;
            //BindAccountCodeTable();
            //((Label)gvAccount.FooterRow.FindControl("txtSrNo")).Text = AccountSrNo.ToString();
        }





        protected void btnsaveNext_ServerClick(object sender, EventArgs e)
        {
            //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            //objDB.DeletedBy = Session["UserName"].ToString();
            //objDB.DelCOA_Account();

            //dtAccountCode = (DataTable)ViewState["dtAccountCode"] as DataTable;
            //if (dtAccountCode != null)
            //{
            //    if (dtAccountCode.Rows.Count > 0)
            //    {
            //        if (!showAccountFirstRow)
            //        {
            //            dtAccountCode.Rows[0].Delete();
            //            dtAccountCode.AcceptChanges();
            //        }

            //        for (int i = 0; i < dtAccountCode.Rows.Count; i++)
            //        {
            //            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            //            objDB.COA_SubCategoryID = Convert.ToInt32(dtAccountCode.Rows[i]["SubCategoryID"].ToString());
            //            objDB.Name = dtAccountCode.Rows[i]["AccountName"].ToString();
            //            objDB.Code = dtAccountCode.Rows[i]["AccountCode"].ToString();
            //            objDB.CreatedBy = Session["UserName"].ToString();
            //            objDB.AddCOA_Account();
            //        }
            //    }
            //}

            Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/coa");
        }

        protected void ddlSubCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            objDB.COA_SubCategoryID = Convert.ToInt32(((DropDownList)gvAccount.FooterRow.FindControl("ddlSubCategory")).SelectedItem.Value);
            TextBox txtCode = gvAccount.FooterRow.FindControl("txtDesc") as TextBox;
            DataTable dtCode = objDB.Gen_COA_AccountType(ref errorMsg);
            if (dtCode != null)
            {
                if (dtCode.Rows.Count > 0)
                {
                    txtCode.Text = dtCode.Rows[0]["NewCode"].ToString();
                }
            }
        }

        //protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    CheckSessions();

        //    objDB.COA_CategoryID = Convert.ToInt32(((DropDownList)gvAccount.FooterRow.FindControl("ddlCategory")).SelectedItem.Value);
        //    DropDownList ddlSubCat = gvAccount.FooterRow.FindControl("ddlSubCategory") as DropDownList;

        //    ddlSubCat.DataSource = objDB.GetAllLocations(ref errorMsg);
        //    ddlSubCat.DataTextField = "NAME";
        //    ddlSubCat.DataValueField = "LOCATION_ID";
        //    ddlSubCat.DataBind();
        //    if (ddlSubCat.DataSource != null)
        //    {
        //        ddlSubCat.Enabled = true;
        //    }
        //    else
        //    {
        //        ddlSubCat.Enabled = false;
        //    }
        //    ddlSubCat.Items.Insert(0, new ListItem("--- Select SubCategory ---", "0"));
        //}
    }
}