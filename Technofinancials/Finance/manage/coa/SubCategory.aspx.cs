﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.manage.coa
{
    public partial class SubCategory : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COAID
        {
            get
            {
                if (ViewState["COAID"] != null)
                {
                    return (int)ViewState["COAID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COAID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/chart-of-account-step-03";
                    ViewState["showLocationFirstRow"] = null;
                    ViewState["LocationSrNo"] = null;
                    ViewState["dtLocationCode"] = null;
                    dtSubCategoryCode = createSubCategoryCodeTable();
                    BindSubCategoryCodeTable();
                    divAlertMsg.Visible = false;
                    clearFields();
                    GetData();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["COAID"] != null)
                    {
                        COAID = Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString());
                        getDataByID(COAID);
                        CheckAccess();
                    }
                    else
                    {

                    }

                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "FAM_COA";
                objDB.PrimaryColumnnName = "COA_ID";
                objDB.PrimaryColumnValue = COAID.ToString();
                objDB.DocName = "ConfigureCOA";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void GetData()
        {
            CheckSessions();

            // Location Code Start
            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            DataTable dt = new DataTable();
            dt = objDB.GetAllCOA_SubCategory(ref errorMsg);
           

            dtSubCategoryCode = null;
            dtSubCategoryCode = new DataTable();
            dtSubCategoryCode = createSubCategoryCodeTable();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtSubCategoryCode.Rows[0][0].ToString() == "")
                    {
                        dtSubCategoryCode.Rows[0].Delete();
                        dtSubCategoryCode.AcceptChanges();
                        showSubCategoryFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtSubCategoryCode.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Name"],
                            dt.Rows[i]["Code"],
                            dt.Rows[i]["CategoryID"],
                            dt.Rows[i]["CategoryName"],
                            dt.Rows[i]["SubCategoryID"]
                        });
                    }
                    SubCategorySrNo = Convert.ToInt32(dtSubCategoryCode.Rows[dtSubCategoryCode.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            BindSubCategoryCodeTable();


            // Company Code End


        }

        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.ChartOfAccountID = COAID;
            dt = objDB.GetCOAByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {




                }
            }

        }

        private void clearFields()
        {
            txtNotes.Value = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.Notes = txtNotes.Value;

                if (HttpContext.Current.Items["COAID"] != null)
                {
                    // update coding
                    objDB.ChartOfAccountID = COAID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOA();
                }
                else
                {
                    // add coding
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOA();
                    clearFields();
                }

                if (res == "New COA Added" || res == "COA Updated")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private DataTable dtSubCategoryCode
        {
            get
            {
                if (ViewState["dtSubCategoryCode"] != null)
                {
                    return (DataTable)ViewState["dtSubCategoryCode"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtSubCategoryCode"] = value;
            }
        }
        bool showSubCategoryFirstRow
        {
            get
            {
                if (ViewState["showSubCategoryFirstRow"] != null)
                {
                    return (bool)ViewState["showSubCategoryFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showSubCategoryFirstRow"] = value;
            }
        }
        protected int SubCategorySrNo
        {
            get
            {
                if (ViewState["SubCategorySrNo"] != null)
                {
                    return (int)ViewState["SubCategorySrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["SubCategorySrNo"] = value;
            }
        }
        private DataTable createSubCategoryCodeTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SubCategorySrNo");
            dt.Columns.Add("SubCategoryName");
            dt.Columns.Add("SubCategoryCode");
            dt.Columns.Add("CategoryID");
            dt.Columns.Add("CategoryName");
            dt.Columns.Add("SubCategoryID");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindSubCategoryCodeTable()
        {
            if (ViewState["dtSubCategoryCode"] == null)
            {
                dtSubCategoryCode = createSubCategoryCodeTable();
                ViewState["dtSubCategoryCode"] = dtSubCategoryCode;
            }

            gvSubCategory.DataSource = dtSubCategoryCode;
            gvSubCategory.DataBind();

            if (showSubCategoryFirstRow)
                gvSubCategory.Rows[0].Visible = true;
            else
                gvSubCategory.Rows[0].Visible = false;

            gvSubCategory.UseAccessibleHeader = true;
            gvSubCategory.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gvSubCategory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"]);
                DropDownList ddlCom = e.Row.FindControl("ddlCategory") as DropDownList;
                ddlCom.DataSource = objDB.GetAllCOA_Category(ref errorMsg);
                ddlCom.DataTextField = "Name";
                ddlCom.DataValueField = "CategoryID";
                ddlCom.DataBind();
                ddlCom.Items.Insert(0, new ListItem("--- Select Category ---", "0"));
                

                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = SubCategorySrNo.ToString();
            }


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    //DropDownList ddlLocEdit = e.Row.FindControl("ddlEditSubCategory") as DropDownList;
                    //ddlLocEdit.DataSource = objDB.GetAllSubCategorys(ref errorMsg);
                    //ddlLocEdit.DataTextField = "NAME";
                    //ddlLocEdit.DataValueField = "SubCategory_ID";
                    //ddlLocEdit.DataBind();
                    //ddlLocEdit.Items.Insert(0, new ListItem("--- Select SubCategory ---", "0"));


                }
            }
        }
        protected void gvSubCategory_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSubCategory.EditIndex = e.NewEditIndex;
            BindSubCategoryCodeTable();
        }
        protected void gvSubCategory_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSubCategory.EditIndex = -1;
            BindSubCategoryCodeTable();
        }
        protected void gvSubCategory_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;
            string SubCatID = ((HiddenField)gvSubCategory.Rows[e.RowIndex].FindControl("hdnSubCatID")).Value;
            objDB.COA_SubCategoryID = Convert.ToInt32(SubCatID);
            objDB.Name = ((TextBox)gvSubCategory.Rows[e.RowIndex].FindControl("txtEditSubCategory")).Text;
            objDB.Code = ((TextBox)gvSubCategory.Rows[e.RowIndex].FindControl("txtEditDesc")).Text;
            objDB.ModifiedBy = Session["UserName"].ToString();
            objDB.UpdateCOA_SubCategory();

            // dtSubCategoryCode.Rows[index].SetField(1, ((DropDownList)gvSubCategory.Rows[e.RowIndex].FindControl("ddlEditSubCategory")).SelectedItem.Text);
            dtSubCategoryCode.Rows[index].SetField(1, ((TextBox)gvSubCategory.Rows[e.RowIndex].FindControl("txtEditSubCategory")).Text);
            dtSubCategoryCode.Rows[index].SetField(2, ((TextBox)gvSubCategory.Rows[e.RowIndex].FindControl("txtEditDesc")).Text);
            dtSubCategoryCode.AcceptChanges();

            gvSubCategory.EditIndex = -1;
            BindSubCategoryCodeTable();
        }
        protected void SubCategory_lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtSubCategoryCode.Rows.Count; i++)
            {
                if (dtSubCategoryCode.Rows[i][0].ToString() == delSr)
                {
                    string res = Common.addAccessLevels("Delete", "COA_SubCategory", "SubCategoryID", dtSubCategoryCode.Rows[i]["SubCategoryID"].ToString(), Session["UserName"].ToString());
                    GetData();
                    //dtSubCategoryCode.Rows[i].Delete();
                    //dtSubCategoryCode.AcceptChanges();
                }
            }
            //for (int i = 0; i < dtSubCategoryCode.Rows.Count; i++)
            //{
            //    dtSubCategoryCode.Rows[i].SetField(0, i + 1);
            //    dtSubCategoryCode.AcceptChanges();
            //}
            //if (dtSubCategoryCode.Rows.Count < 1)
            //{
            //    DataRow dr = dtSubCategoryCode.NewRow();
            //    dtSubCategoryCode.Rows.Add(dr);
            //    dtSubCategoryCode.AcceptChanges();
            //    showSubCategoryFirstRow = false;
            //}
            //if (showSubCategoryFirstRow)
            //    SubCategorySrNo = dtSubCategoryCode.Rows.Count + 1;
            //else
            //    SubCategorySrNo = 1;

            //BindSubCategoryCodeTable();
        }
        protected void SubCategory_btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
               
                    if (dtSubCategoryCode.Rows[0][0].ToString() == "")
                    {
                        dtSubCategoryCode.Rows[0].Delete();
                        dtSubCategoryCode.AcceptChanges();
                        showSubCategoryFirstRow = true;
                    }

                    objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
                    objDB.CategoryID = Convert.ToInt32(((DropDownList)gvSubCategory.FooterRow.FindControl("ddlCategory")).SelectedItem.Value);
                    objDB.Name = ((TextBox)gvSubCategory.FooterRow.FindControl("txtSubCategory")).Text;
                    objDB.Code = ((TextBox)gvSubCategory.FooterRow.FindControl("txtDesc")).Text;
                    objDB.CreatedBy = Session["UserName"].ToString();
                    string res = objDB.AddCOA_SubCategory();
                    if (res == "New Sub Category Added Successfully")
                    {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                    GetData();
                }
                    else
                    {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;

                }

               
                
               
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
                 

            

            //DataRow dr = dtSubCategoryCode.NewRow();
            //dr[0] = SubCategorySrNo.ToString();
            //dr[1] = ((TextBox)gvSubCategory.FooterRow.FindControl("txtSubCategory")).Text;
            //dr[2] = ((TextBox)gvSubCategory.FooterRow.FindControl("txtDesc")).Text;
            //dr[3] = ((DropDownList)gvSubCategory.FooterRow.FindControl("ddlCategory")).SelectedItem.Value;
            //dr[4] = ((DropDownList)gvSubCategory.FooterRow.FindControl("ddlCategory")).SelectedItem.Text;
            //dtSubCategoryCode.Rows.Add(dr);
            //dtSubCategoryCode.AcceptChanges();
            //SubCategorySrNo += 1;
            //BindSubCategoryCodeTable();
            //((Label)gvSubCategory.FooterRow.FindControl("txtSrNo")).Text = SubCategorySrNo.ToString();

            
        }



     

        protected void btnsaveNext_ServerClick(object sender, EventArgs e)
        {
            //objDB.ParentCompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            //objDB.DeletedBy = Session["UserName"].ToString();
            //objDB.DelCOA_SubCategory();

            //dtSubCategoryCode = (DataTable)ViewState["dtSubCategoryCode"] as DataTable;
            //if (dtSubCategoryCode != null)
            //{
            //    if (dtSubCategoryCode.Rows.Count > 0)
            //    {
            //        if (!showSubCategoryFirstRow)
            //        {
            //            dtSubCategoryCode.Rows[0].Delete();
            //            dtSubCategoryCode.AcceptChanges();
            //        }

            //        for (int i = 0; i < dtSubCategoryCode.Rows.Count; i++)
            //        {
            //            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            //            objDB.CategoryID = Convert.ToInt32(dtSubCategoryCode.Rows[i]["CategoryID"].ToString());
            //            objDB.Name = dtSubCategoryCode.Rows[i]["SubCategoryName"].ToString();
            //            objDB.Code = dtSubCategoryCode.Rows[i]["SubCategoryCode"].ToString();
            //            objDB.CreatedBy = Session["UserName"].ToString();
            //            objDB.AddCOA_SubCategory();
            //        }
            //    }
            //}

            Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/chart-of-account-step-05");
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            objDB.COA_CategoryID = Convert.ToInt32(((DropDownList)gvSubCategory.FooterRow.FindControl("ddlCategory")).SelectedItem.Value);
            TextBox txtCode = gvSubCategory.FooterRow.FindControl("txtDesc") as TextBox;
            DataTable dtCode  = objDB.Gen_COA_SubCatCode(ref errorMsg);
            if (dtCode != null)
            {
                if (dtCode.Rows.Count>0)
                {
                    txtCode.Text = dtCode.Rows[0]["NewCode"].ToString();
                }
            }
            
        }
    }
}