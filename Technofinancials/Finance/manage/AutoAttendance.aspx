﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoAttendance.aspx.cs" Inherits="Technofinancials.Finance.manage.AutoAttendance" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>		
		button.multiselect.dropdown-toggle.custom-select.text-center span.multiselect-selected-text {
			white-space: nowrap;
		}

		.open > .dropdown-menu {
			display: block;
			width: 300px !important
		}

			.open > .dropdown-menu ::-webkit-scrollbar {
				width: 10px;
			}

		.dropdown-menu {
			box-shadow: none !important;
		}

		.multiselect-container.dropdown-menu {
			overflow: hidden;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center {
			width: 200px;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center {
			background-color: #fff;
			border: 1px solid #aaa;
			border-radius: 5px;
			padding-left: 8px;
			padding-right: 8px;
			padding: 3px 37px;
		}

		.open > .dropdown-menu {
			padding: 15px;
			max-height: 250px !important;
			border: 1px solid #aaa;
			border-bottom-left-radius: 5px;
			border-bottom-right-radius: 5px;
		}

		multiselect-filter {
			margin-bottom: 15px;
		}

		.multiselect-container.dropdown-menu {
			overflow: overlay !important;
		}

		.multiselect-container .multiselect-all .form-check, .multiselect-container .multiselect-group .form-check, .multiselect-container .multiselect-option .form-check {
			padding: 0px;
		}

		@media only screen and (max-width: 1280px) and (min-width: 800px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 25px !important;
			}
		}

		@media only screen and (max-width: 1440px) and (min-width: 900px) {

			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 0px !important;
				width: 170px;
			}
		}

		@media only screen and (max-width: 1024px) and (min-width: 993px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 15px !important;
				width: 130px;
			}
		}

		@media only screen and (max-width: 992px) and (min-width: 768px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 50px;
				width: 80px;
			}
		}

		@media only screen and (max-width: 1599px) and (min-width: 1201px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 37px;
			}
		}

		.multiselect-container.dropdown-menu {
			overflow: hidden auto !important;
		}

		.open > .dropdown-menu {
			max-height: 300px !important;
		}

	


	</style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->

		<main id="app-main" class="app-main">
			<asp:UpdateProgress ID="updProgress"
				AssociatedUpdatePanelID="btnUpdPnl"
				runat="server">
				<ProgressTemplate>
					<div class="upd_panel">
						<div class="center">
							<img src="/assets/images/Loading.gif" />
						</div>


					</div>
				</ProgressTemplate>
			</asp:UpdateProgress>

			<div class="wrap">
				   <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>
                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <h1 class="m-0 text-dark">Auto Attendance</h1>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div style="text-align: right;">
                                           <button class="AD_btn" id="btnSave" runat="server"  onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                           

                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>
                        <!-- Modal -->
                        <div class="modal fade M_set" id="notes-modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="m-0 text-dark">Notes</h1>
                                        <div class="add_new">
                                            <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                            <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                        </p>
                                        <p>
                                            <textarea id="Textarea1" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>
                </asp:UpdatePanel>

				<section class="app-content">
					
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6" style="display: none">
									<div class="form-group">
										<h4>Employee Name</h4>
										<asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2" data-plugin="select2">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Department</h4>
										<asp:DropDownList ID="ddldept" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddldept_SelectedIndexChanged" class="form-control select2" data-plugin="select2">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee Name<span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="ddlEmployee" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <br />
                                        <asp:ListBox SelectionMode="Multiple" ID="lstEmployees" runat="server"></asp:ListBox>
                                    </div>
                                </div>
								 <div class="col-sm-6 form-group">
												<div class="checkbox checkbox-primary">
                                                    <br />
															<input type="checkbox" id="autoattendance"  runat="server" name="returnedLockerKeys" />
															<label for="autoattendance">Auto</label>	
											</div>		
								
							</div>	
                                       
								
							</div>
						</div>
					
						<div class="col-lg-4 col-md-6 col-sm-12">
							<asp:UpdatePanel ID="UpdatePanel4" runat="server">
								<ContentTemplate>
									<div class="form-group" id="divAlertMsg" runat="server">
										<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
											<span>
												<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
											</span>
											<p id="pAlertMsg" runat="server">
											</p>
										</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>
					</div>
					
					 <div class="row">

                         <div class="col-md-12">
                            <div class="tab-content">
                                <div class="tab-pane active row">
                                     <div class="fixed-table-wrap">

                                        <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv dataTable no-footer" ClientIDMode="Static" AutoGenerateColumns="false">
                                            <Columns>



                                                <asp:TemplateField HeaderText="Sr.No.">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                               <%-- <asp:TemplateField HeaderText="">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chckchanged" ID="checkAll" ToolTip="Click To Select All"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" Enabled='<%# Eval("Status").ToString()=="Submitted"||Eval("Status").ToString()=="Saved as Draft"? true : false %>' ID="check"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="IT-3 ID">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="IT3ID" Text='<%# Eval("WDID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="LblName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Submition Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="SubmissionDate" Text='<%# Eval("DesgTitle") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Total Claim">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="TotalClaim" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            


</Columns>
                            </asp:GridView>
                       
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <br />
                                        <br />
                                        <br />
                                   </div>

                    </div>
                                  </div>
                                </div>
                            </div>
                    
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">

								<asp:UpdatePanel ID="btnUpdPnl" runat="server">
									<ContentTemplate>
									</ContentTemplate>
								</asp:UpdatePanel>
							</div>
						</div>

						<div class="col-md-6 col-lg-4 col-sm-12">
						</div>

					</div>

					<div class="clearfix">&nbsp;</div>
					<div class="clearfix">&nbsp;</div>


					<%-- Print --%>

					<div style="display: none">

						<div id="imgReport">

							<div style="text-align: center">
								<img src="../assets/images/a4print.png" style="max-width: 100%" />
							</div>

						</div>
					</div>
   
					<asp:UpdatePanel ID="UpdatePanel1" runat="server">
						<ContentTemplate>
							<div runat="server" id="divPayrSlip" visible="false">
								<asp:Literal ID="ltrShowPaySlip" runat="server"></asp:Literal>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</section>

				<!-- #dash-content -->
			</div>
					<div class="clearfix">&nbsp;</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>

		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>
		    <script src="/business/scripts/ViewDesignations.js"></script>

		<script src="/assets/js/printThis.js"></script>
		<script>
			function PrintThis() {
				$('#divPrint').printThis({
					importCSS: false,
					importStyle: false,
					header: "",
					footer: "",
					loadCSS: ""
				});
			}

			function GetPaySlipData() {
				$("#dvPayslip").text($("#ReportPayrolls").text());
			}
		</script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
		<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

		<script type="text/javascript">
			function Print() {
				var HTML_Width = $("#ReportPayroll").width();
				var HTML_Height = $("#ReportPayroll").height();
				var top_left_margin = 15;
				var PDF_Width = HTML_Width + (top_left_margin * 2);
				var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
				var canvas_image_width = HTML_Width;
				var canvas_image_height = HTML_Height;

				var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

				html2canvas($("#ReportPayroll")[0]).then(function (canvas) {
					var imgData = canvas.toDataURL("image/jpeg", 1.0);
					var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
					// var pdf = new jsPDF("p", "pt", "a4");
					pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
					for (var i = 1; i <= totalPDFPages; i++) {
						pdf.addPage(PDF_Width, PDF_Height);
						pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height * i) + (top_left_margin * 4), canvas_image_width, canvas_image_height);
					}
					pdf.save("PaySlip.pdf");
					$(".html-content").hide();
				});
			}

			$(function () {
				$('[id*=lstEmployees]').multiselect({
					includeSelectAllOption: true,
					buttonContainer: '<div class="btn-group" />',
					includeSelectAllIfMoreThan: 0,
					enableFiltering: true,
					filterPlaceholder: 'Search',
					filterBehavior: 'text',
					includeFilterClearBtn: true,
					enableCaseInsensitiveFiltering: true,
					numberDisplayed: 1,
					maxHeight: true,
					maxHeight: 350,
					templates: {
						button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span></button>',
						Container: '<div class="multiselect-container dropdown-menu"></div>',
						filter: '<div class="multiselect-filter"><div class="input-group input-group-sm p-1"><div class="input-group-prepend"></div><input class="form-control multiselect-search" type="text" /></div></div>',

						filterClearBtn: '<div class="input-group-append"><button class="multiselect-clear-filter input-group-text" type="button"><i class="fas fa-times"></i></button></div>',

						option: '<button class="multiselect-option dropdown-item"></button>',

						divider: '<div class="dropdown-divider"></div>',

						optionGroup: '<button class="multiselect-group dropdown-item"></button>',

						resetButton: '<div class="multiselect-reset text-center p-2"><button class="btn btn-sm btn-block btn-outline-secondary"></button></div>'
					}
				});
			});

		</script>

		<style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }

            @media only screen and (max-width: 1280px) and (min-width: 800px) {
                            .tf-alert-danger p {
                display: inline-block;
                margin-top: 0px;
                position: absolute;
                margin-left: 7px;
                font-size: 13px;
                color: #fff;
            }
            }
        </style>
	</form>
</body>
</html>

