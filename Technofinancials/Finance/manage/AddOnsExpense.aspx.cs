﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class AddOnsExpense : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int AddOnsExpenseID
        {
            get
            {
                if (ViewState["AddOnsExpenseID"] != null)
                {
                    return (int)ViewState["AddOnsExpenseID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AddOnsExpenseID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                { 
                    ViewState["AddOnsExpenseID"] = null;
                    BindCurrencyDropdown();
                    BindEmployeeDropdown();
                    BindAddOnsHeadDropdown();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/AddOnsExpense";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["AddOnsExpenseID"] != null)
                    {
                        AddOnsExpenseID = Convert.ToInt32(HttpContext.Current.Items["AddOnsExpenseID"].ToString());
                        getAddOnsExpenseByID(AddOnsExpenseID);
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "AddOns";
                objDB.PrimaryColumnnName = "AddOnsID";
                objDB.PrimaryColumnValue = AddOnsExpenseID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getAddOnsExpenseByID(int AddOnsExpenseID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.AddOnsExpenseID = AddOnsExpenseID;
                dt = objDB.GetAddOnsExpenseByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtAmount.Text = dt.Rows[0]["Amount"].ToString();
                        txtNotes.Value = dt.Rows[0]["Note"].ToString();
                        txtCurrencyAmount.Text = Convert.ToDouble(dt.Rows[0]["NetAmount"]).ToString("N2");
                        txtRate.Text = dt.Rows[0]["CurrencyRate"].ToString();
                        ddlCurrency.SelectedValue = dt.Rows[0]["CurrencyID"].ToString();
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        ddlAddOn.SelectedValue = dt.Rows[0]["AddOnsHeadID"].ToString();
                    }
                }
                //Common.addlog("View", "HR", "Currency \"" + txtTitle.Value + "\" Viewed", "Currency", objDB.AddOnsExpenseID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Amount = txtAmount.Text;
                objDB.Notes = txtNotes.Value;
                objDB.CurrencyRate = Convert.ToDouble(txtRate.Text);
                objDB.AmountAfterCurrency = Convert.ToDouble(txtCurrencyAmount.Text);
                objDB.CurrencyID = Convert.ToInt32(ddlCurrency.SelectedValue);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.AddOnsID = Convert.ToInt32(ddlAddOn.SelectedValue);
                objDB.Status = "Submited Via Finance Module";
                
                if (HttpContext.Current.Items["AddOnsExpenseID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.AddOnsExpenseID = AddOnsExpenseID;
                    res = objDB.UpdateAddOnsExpense();
                    //res = "Add On Head Data Updated";
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddAddOnsExpense();
                    clearFields();
                    //res = "New Add On Head Added";
                }



                if (res == "New AddOns Added Successfully" || res == "AddOns  Data Updated")
                {
                    if (res == "New AddOns Added Successfully") { Common.addlog("Add", "HR", "New AddOns \"" + objDB.CurrencyName + "\" Added", "AddOns"); }
                    if (res == "AddOns  Data Updated") { Common.addlog("Update", "HR", "AddOns \"" + objDB.CurrencyName + "\" Updated", "AddOns", objDB.AddOnsExpenseID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtAmount.Text = "0";
            txtRate.Text = "1";
            txtCurrencyAmount.Text = "0";
            ddlCurrency.SelectedValue = "0";
            ddlEmployee.SelectedValue = "0";
            ddlAddOn.SelectedValue = "0";
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "AddOns", "AddOnsID", HttpContext.Current.Items["AddOnsExpenseID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "Finance", "AddOn of ID\"" + HttpContext.Current.Items["AddOnsExpenseID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/AddOnsexpense", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/AddOnsexpense/edit-AddOnsexpense-" + HttpContext.Current.Items["AddOnsExpenseID"].ToString(), "AddOn \"" + AddOnsExpenseID + "\"", "AddOnsHead", "Announcement", Convert.ToInt32(HttpContext.Current.Items["AddOnsExpenseID"].ToString()));

                //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                //{
                //    objDB.AddOnsExpenseID = AddOnsExpenseID;
                //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //    objDB.AddCurrencyToEmployeeAttendance();
                //}

                //Common.addlog("Delete", "HR", "Currency of ID \"" + objDB.AddOnsExpenseID + "\" deleted", "Currency", objDB.AddOnsExpenseID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "AddOns", "AddOnsExpenseID", HttpContext.Current.Items["AddOnsExpenseID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "AddOns of ID\"" + HttpContext.Current.Items["AddOnsExpenseID"].ToString() + "\" Status Changed", "AddOns", AddOnsExpenseID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.AddOnsExpenseID = AddOnsExpenseID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteAddOnsExpenseByID();
                Common.addlog("AddOns Rejected", "HR", "AddOns of ID\"" + HttpContext.Current.Items["AddOnsExpenseID"].ToString() + "\" AddOns Rejected", "AddOns", AddOnsExpenseID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "AddOns Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        private void BindCurrencyDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlCurrency.DataSource = objDB.GetAllCurrencyByCompanyID(ref errorMsg);
                ddlCurrency.DataTextField = "CurrencyName";
                ddlCurrency.DataValueField = "CurrencyID";
                ddlCurrency.DataBind();
                ddlCurrency.Items.Insert(0, new ListItem("--- Select Currency ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        private void BindAddOnsHeadDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlAddOn.DataSource = objDB.GetAllAddOnsByCompanyID(ref errorMsg);
                ddlAddOn.DataTextField = "Tittle";
                ddlAddOn.DataValueField = "AddOnsID";
                ddlAddOn.DataBind();
                ddlAddOn.Items.Insert(0, new ListItem("--- Select Add Ons Head ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            string errorMsg = "";
            if (ddl.SelectedValue != "0")
            {
                DataTable dt = new DataTable();
                objDB.CurrencyID = Convert.ToInt32(ddl.SelectedValue);
                dt = objDB.getCurrencyByID(ref errorMsg);
                if (dt != null && dt.Rows.Count > 0)
                {
                    txtRate.Text = dt.Rows[0]["Rate"].ToString();
                    txtCurrencyAmount.Text = (Convert.ToDouble(txtAmount.Text) * Convert.ToDouble(txtRate.Text)).ToString("N2");
                }
            }
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            txtCurrencyAmount.Text = (Convert.ToDouble(txtAmount.Text) * Convert.ToDouble(txtRate.Text)).ToString("N2");
        }
    }
}