﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MiniPayrollNew.aspx.cs" Inherits="Technofinancials.Finance.manage.MiniPayrollNew" %>

      
<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <%-- <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>

            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">Mini Payroll</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div style="text-align: right;">

                                <button type="button" class="AD_btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" style="display:none;" >Note</button>
                                <button class="AD_btn"  id="btnPDFOvertime" runat="server" onserverclick="btnPDFOverTime_ServerClick" style="display:none;" type="button">Overtime Sheet</button>
                                <button class="AD_btn" style="display:none;" id="btnPDF" runat="server" onserverclick="btnPDFNormalSal_ServerClick" type="button">Salary Sheet</button>
                                <button class="AD_btn"  id="btnRevApproveByHR" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                <button class="AD_btn"  id="btnApproveByHR" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class"  CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
                                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                <button class="AD_btn"  id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Dis Approve</button>
                                <button class="AD_btn"  id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                <button class="AD_btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Reject</button>
                                <button class="AD_btn"  id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                    <button class="AD_btn"  id="viewPayroll" runat="server" style="display:none;" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">View Payroll</button>
                                    <button class="AD_btn"  id="btnReleaseActiveEmployees" runat="server" onserverclick="btnReleaseActiveEmployees_ServerClick" validationgroup="btnValidate" type="button">Disburse</button>
                                  
                                    <button class="AD_btn"  id="downloadpayroll" runat="server" onserverclick="btnPayroll_ServerClick" validationgroup="btnValidate" type="button">Download Mini Payroll</button>
                                      <button class="AD_btn"  id="noPayroll" runat="server" onserverclick="btnnoPayroll_ServerClick" validationgroup="btnValidate" type="button">No Payroll</button>
                                   
                                    <a class="AD_btn"  id="btnBack" runat="server">Back</a>
                           
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
                      <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Payroll Date  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPayrollDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
                                    </div>
                                </div>

                                <div class="col-md-6">
                                   <br />
                                    <button class="AD_btn_inn" id="btnView" runat="server" onserverclick="txtPayrollDate_ServerChange" type="button">View</button>
                                
                                </div>
                                <label>Total NoPayroll:&nbsp;&nbsp;  </label><label id="noPayrollCount" runat="server" >0</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label>Total MiniPayroll:&nbsp;&nbsp;  </label><label id="PayrollCount" runat="server" >0</label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12"></div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                         <div class="col-md-12">
                           
                           <div class="tab-content">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                       <asp:GridView ID="gvSalaries" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" ShowFooter="true" AutoGenerateColumns="false" OnRowUpdating="gvSalaries_RowUpdating" OnRowCancelingEdit="gvSalaries_RowCancelingEdit" OnRowEditing="gvSalaries_RowEditing">

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("WORKDAYID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="NetSalary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="NetSalary" Text='<%# Eval("NetSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                       
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Salary/Month">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeSalary" Text='<%# Eval("GrossSal","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Present Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="SalaryOfDays" Text='<%# Eval("SalaryOfDays","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Over Time">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTime" Text='<%# Eval("TotalOverTimeAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Increment Of Previous Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="TotalIncrementedAmount" Text='<%# Eval("TotalIncrementedAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Decrement Of Previous Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="TotalDecrementedAmount" Text='<%# Eval("TotalDecrementedAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Commission">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Arrears">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Arrears_" Text='<%# Eval("Arrears_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Spiffs">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Severance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Severance_" Text='<%# Eval("Severance_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reimbursement">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Reimbursement_" Text='<%# Eval("Reimbursement_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="LeaveEncashment">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LeaveEncashment_" Text='<%# Eval("LeaveEncashment_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Previous Days Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="PreviousDaysAmount_" Text='<%# Eval("PreviousDaysAmount_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Expenses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="ExpenseReimb_" Text='<%# Eval("ExpenseReimb_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bonuses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("BonusTotal_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     
                                    <asp:TemplateField HeaderText="Gratuity">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Gratuity_" Text='<%# Eval("Gratuity_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EOBI">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="EOBI_" Text='<%# Eval("EOBI_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    
                                    <asp:TemplateField HeaderText="PF">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="ProvidentFund" Text='<%# Eval("ProvidentFund","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                 
                                    </asp:TemplateField>

                                  

                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="tax" Text='<%# Eval("MonthlyTax","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Additional tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("AdditionalTaxperMonth","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("AdditionalTaxperMonth") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Total Gross Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("TotalGrossAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                     
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Deductions">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("TotalDeductions","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NetSalary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="NetSalary" Text='<%# Eval("NetSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                       
                                    </asp:TemplateField>

                                

                                
                                </Columns>
                            </asp:GridView>


                              
                                </div>
                                </div>
                               </div>
                            

                        </div>
                    </div>



                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               <asp:GridView ID="gvNormalSal" runat="server" Visible="false" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
     <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("WORKDAYID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
             <asp:TemplateField HeaderText="NetSalary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="NetSalary" Text='<%# Eval("NetSalary","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                       
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Salary/Month">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeSalary" Text='<%# Eval("GrossSal","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Present Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="SalaryOfDays" Text='<%# Eval("SalaryOfDays","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Over Time">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTime" Text='<%# Eval("TotalOverTimeAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Increment Of Previous Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="TotalIncrementedAmount" Text='<%# Eval("TotalIncrementedAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
           <asp:TemplateField HeaderText="Decrement Of Previous Days">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="TotalDecrementedAmount" Text='<%# Eval("TotalDecrementedAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Commission">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCommission" Text='<%# Eval("Commission_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Arrears">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Arrears_" Text='<%# Eval("Arrears_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Spiffs">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSpiffs" Text='<%# Eval("Spiffs_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Severance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Severance_" Text='<%# Eval("Severance_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reimbursement">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Reimbursement_" Text='<%# Eval("Reimbursement_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="LeaveEncashment">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="LeaveEncashment_" Text='<%# Eval("LeaveEncashment_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Previous Days Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="PreviousDaysAmount_" Text='<%# Eval("PreviousDaysAmount_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Expenses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="ExpenseReimb_" Text='<%# Eval("ExpenseReimb_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bonuses">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalBonus" Text='<%# Eval("BonusTotal_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     
                                    <asp:TemplateField HeaderText="Gratuity">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Gratuity_" Text='<%# Eval("Gratuity_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EOBI">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="EOBI_" Text='<%# Eval("EOBI_","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    
                                    <asp:TemplateField HeaderText="PF">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="ProvidentFund" Text='<%# Eval("ProvidentFund","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                 
                                    </asp:TemplateField>

                                  

                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="tax" Text='<%# Eval("MonthlyTax","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Additional tax">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("AdditionalTaxperMonth","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("AdditionalTaxperMonth") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Total Gross Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("TotalGrossAmount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                     
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Deductions">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmpDeductions" Text='<%# Eval("TotalDeductions","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                

                                

                                
                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>


                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <asp:GridView ID="gvOverTime" runat="server"  Visible="false" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Basic Salary">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("BasicSalary") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Over Time Hours">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTime" Text='<%# Eval("OverTime") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Over Time Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOverTimeAmount" Text='<%# Eval("OverTimeAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Food Allowance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblFoodAllowence" Text='<%# Eval("FoodAllowence") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Maintenance Allowance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblMaintenance" Text='<%# Eval("Maintenance") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fuel Allowance">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblFuel" Text='<%# Eval("Fuel") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAbsentAmount" Text='<%# Eval("OverTimeTotal") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Signature">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSignature" Text=''></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    

                </section>

                
                <!-- #dash-content -->
            </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>  
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }
            
                .row.tables div {
/*                    overflow-x: scroll;*/
                    overflow-y: hidden;
                    padding-bottom: 35px;
                }


                .row.tables div::-webkit-scrollbar-track {
                    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                    background-color: #FFF;
                }

                .row.tables div::-webkit-scrollbar {
                    height: 10px;
                    background-color: #ffffff;
                }

                .row.tables  div::-webkit-scrollbar-thumb {
                    background-color: #003780;
                    border: 2px solid #003780;
                    border-radius: 10px;
                }
        </style>
    </form>
</body>
</html>

