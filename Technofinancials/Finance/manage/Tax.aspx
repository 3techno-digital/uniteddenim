﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tax.aspx.cs" Inherits="Technofinancials.Finance.manage.Tax" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <section class="app-content">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img src="/assets/images/Admin_tax_rates.png" class="img-responsive tf-page-heading-img" />
                                <h3 class="tf-page-heading-text">Tax Rate</h3>
                            </div>

                            	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                            <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                <ContentTemplate>


                                    <div class="col-sm-4">
                                        <div class="pull-right flex">
                                            <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                            <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                            <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                            <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class" "Delete" CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                            <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                            <button class="tf-save-btn" "Reject & Disapprove" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                            <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>

                                            <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSubmit_ServerClick1" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                            <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                        </div>

                                    </div>

                                </ContentTemplate>

                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr />
                            </div>
                        </div>
                        <div class="row">


                            <div class="dashboard-left inner-page">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group-lg add-tax-rate">
                                            <label>Tax Rate Name    <span style="color:red !important;">*</span></label>
                                            <input type="text" class="form-control form-text" placeholder="Tax Rate Name" id="txtName" runat="server" />
                                          <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtName" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">

                                        <div class="input-group-lg add-tax-rate">
                                            <label>Acronym <span style="color:red !important;">*</span></label>
                                            <input type="text" class="form-control form-text" placeholder="Ancronym" id="txtShortCode" runat="server" />
                                             <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtShortCode" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">

                                        <div class="input-group-lg add-tax-rate">
                                            <label>Tax Percentage <span style="color:red !important;">*</span> </label>
                                            <input type="number" class="form-control form-text" placeholder="Tax Rate Percentage" id="txtPer" runat="server" />
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtPer" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />

                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                     <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Tax Type
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="ddlTaxType" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlTaxType">
                                                <asp:ListItem Value ="0" > --- Select --- </asp:ListItem>
                                                <asp:ListItem Value ="Invoice" >Invoice</asp:ListItem>
                                                <asp:ListItem Value ="Bill" >Bill</asp:ListItem>
                                                <asp:ListItem Value ="Both" >Both</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Select Account
                                                <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlAssAcc" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlAssAcc">
                                           
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Recoverable</label>
                                        <div class="form-group checkbox-div">
                                            <input type="checkbox" id="chkIsRecoverable" runat="server" />




                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Description</h4>

                                        <textarea class="form-control" rows="3" name="Description" id="txtDescription" runat="server"></textarea>
                                    </div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                       
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
        </style>
    </form>
</body>
</html>
