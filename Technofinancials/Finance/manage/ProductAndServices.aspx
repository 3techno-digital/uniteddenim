﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductAndServices.aspx.cs" Inherits="Technofinancials.Finance.manage.ProductAndServices" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="upd1"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="wrap">
                <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>
                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <img src="/assets/images/ProductServices.png" class="img-responsive tf-page-heading-img" />
                                    <h3 class="tf-page-heading-text">Product And Services</h3>
                                </div>

                                	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                <div class="col-sm-4">
                                    <div class="pull-right flex">
                                        <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                        <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                        <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class" "Delete" CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                        <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                        <button class="tf-save-btn" "Reject & Disapprove" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr />
                                </div>
                            </div>
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12">



                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Name  <span style="color:red !important;">*</span> 
                                                     <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtName" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input class="form-control" id="txtName" placeholder="Name" type="text" runat="server" />
                                            </div>
                                            <div class="col-md-4">
                                                <h4>Unit Price  <span style="color:red !important;">*</span> 
                                                     <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtPrice" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input class="form-control" id="txtPrice" placeholder="Price" type="number" runat="server" />
                                            </div>
                                            <div class="col-md-4">
                                                <label>Taxes Applied</label>
                                                <div class="form-group">
                                                    <asp:ListBox ID="lbTaxes" runat="server" CssClass="select2" ClientIDMode="Static" SelectionMode="Multiple" data-plugin="select2"></asp:ListBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="input-group input-group-lg">
                                                    <label>Select Account  
                                                        <span style="color:red !important;">*</span> 
                                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlAssAcc" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                    
                                                    <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlAssAcc">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Reimbursement of Billable Expense</label>
                                                <div class="form-group checkbox-div">
                                                    <input type="checkbox" id="chkIsBillableExpense" runat="server" />




                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4>Description
                                                    <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtDescription" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>

                                                <textarea class="form-control" rows="3" name="Description" id="txtDescription" runat="server"></textarea>
                                            </div>
                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                
                                    </div>
                                </div>
                            </div>
                        </section>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }

            label {
                font-size: 16px;
                margin-top: 11px !important;
            }

            .form-control {
                border-color: #a9a9a9 !important;
            }
        </style>
        <!-- Modal -->
    </form>
</body>
</html>
