﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="COASetup.aspx.cs" Inherits="Technofinancials.Finance.manage.COASetup" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
           <%-- <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>

            <div class="wrap">

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/Payroll-B.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Payroll COA</h3>
                        </div>

                        	<div class="col-md-4">
                                               
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                              
                                        </div>

                        


                                <div class="col-sm-4">
                                    <div class="pull-right flex">
                                         
                                        <button class="tf-save-btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>

                               

                           
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label class="marginBottomZeroLabel">   
                                           EOBI COA
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" InitialValue ="0" ControlToValidate="ddlCOA" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <asp:Label CssClass="lbl-COA-Balance"  Text="Balance: 0" ID="lblCOABal" runat="server" />
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true"  ID="ddlCOA">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label class="marginBottomZeroLabel">   
                                           Tax COA
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" InitialValue ="0" ControlToValidate="ddlCOATax" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <asp:Label CssClass="lbl-COA-Balance"  Text="Balance: 0" ID="lblCOATaxBal" runat="server" />
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true"  ID="ddlCOATax">
                                        </asp:DropDownList>
                                    </div>
                                </div> 
                                
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label class="marginBottomZeroLabel">   
                                           PF COA
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" InitialValue ="0" ControlToValidate="ddlCOAPF" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <asp:Label CssClass="lbl-COA-Balance"  Text="Balance: 0" ID="lblCOAPFBal" runat="server" />
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" AutoPostBack="true"  ID="ddlCOAPF">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                          
                                       
                                 
                                    </div>
                    
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
               
                </section>

                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }
        </style>
    </form>
</body>
</html>
