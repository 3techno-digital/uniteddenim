﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Holidays.aspx.cs" Inherits="Technofinancials.Finance.Manage.Holidays" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>


<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="upd1"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">Holidays</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div style="text-align: right;">
                                    <button type="button" class="AD_btn" data-toggle="modal" data-target="#notes-modal" value="Add Note">Note </button>
                                    <button class="AD_btn" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                    <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                    <button class="AD_btn" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                    <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                    <button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                    <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                    <button class="AD_btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                    <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                    <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>
                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>
                                                Holiday Title  <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtHolidayTitle" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <input class="form-control" id="txtHolidayTitle" placeholder="Holiday Title" type="text" runat="server" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>
                                                Annual Calendar </label>
                                            <asp:DropDownList Enabled="false" ID="ddlAnnualCalendar" runat="server" CssClass="form-control select2" data-plugin="select2" ClientIDMode="Static">
                                                <asp:ListItem Value="PK"> PK </asp:ListItem>
                                                <asp:ListItem Value="US"> US </asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                         <div class="col-md-6">
                                            <label>
                                                Type  </label>
                                            <asp:DropDownList ID="ddlHolidayType" runat="server" CssClass="form-control select2" data-plugin="select2" ClientIDMode="Static">
                                                <asp:ListItem Value="Govt."> Govt. </asp:ListItem>
                                                <asp:ListItem Value="Gazette"> Gazette </asp:ListItem>
                                                <asp:ListItem Value="EID"> Eid</asp:ListItem>
                                                <asp:ListItem Value="Others"> Others </asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-6">
                                            <label>
                                                Start Date  <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtStartDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <input data-date-format="DD-MMM-YYYY" class="form-control datetime-picker" id="txtStartDate" placeholder="Start Date" type="text" runat="server" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>
                                                End Date  <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtEndDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                            <input data-date-format="DD-MMM-YYYY" class="form-control datetime-picker" id="txtEndDate" placeholder="End Date" type="text" runat="server" />
                                        </div>
                                        <div class="col-md-6">
                                            <div class="table_out_data" runat="server" id="divDeductPF">
                                                <div>
                                                    <input type="checkbox" id="chkSpecialHoliday" runat="server" name="chkSpecialHoliday" class="checkk" />
                                                    <label for="chkSpecialHoliday" class="checkk_lab">Working Holiday</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                            <span>
                                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                            </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade M_set" id="notes-modal" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="m-0 text-dark">Notes</h1>
                                                        <div class="add_new">
                                                            <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                                            <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                                        </div>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>
                                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                        </p>
                                                        <p>
                                                            <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                        </p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            </div>
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12">



                                        <div class="row">
                                        </div>
                                        <div class="row">
                                        </div>
                                        <div class="clearfix">&nbsp;</div>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
             .table_out_data input{
                 float:none;
                 width:10% !important;
                 margin:0px !important;
             }
            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
            .table_out_data label {
    display: inline-block;
    margin-top: -4px;
}
        </style>
        <!-- Modal -->
    </form>
</body>


</html>
