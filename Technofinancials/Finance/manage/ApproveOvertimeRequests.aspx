﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApproveOvertimeRequests.aspx.cs" Inherits="Technofinancials.Finance.manage.ApproveOvertimeRequests" %>
                            
<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="StyleSheets1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <uc:Header ID="header2" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar1" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">
              <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <h1 class="m-0 text-dark">Overtime Request</h1>
                        </div>
                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div style="text-align: right;">
                                        <%--<button type="button" class="AD_btn" >Note</button>--%>
                                        <button class="AD_btn" id="btnApprove" runat="server" onserverclick="btnApprove_ServerClick" validationgroup="btnValidate" type="button">Approve</button>                                      
                                        <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                    </div>
                                </div>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnApprove" EventName="serverclick" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <h4>Employee  <span style="color: red !important;">*</span></h4>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator31" ControlToValidate="ddlEmployee" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                    <asp:DropDownList ID="ddlEmployee" runat="server" Enabled="false" data-plugin="select2" CssClass="form-control select2"></asp:DropDownList>
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Date  <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input id="txtDate" data-date-format="DD-MMM-YYYY" disabled="disabled" class="form-control datetime-picker" placeholder="Date" type="text" runat="server" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Start Time <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtStartTime" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input  class="form-control" data-plugin="datetimepicker" disabled="disabled" data-date-format="hh:mm A" id="txtStartTime"  type="text"  runat="server" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>End Time 
                                        <lable><%--<input type="checkbox" runat="server" name="isNextDay" id="isNextDay" /> On Next Day</lable>
                                        <span style="color: red !important;">*</span>--%>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator42" ControlToValidate="txtEndTime" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input  class="form-control" data-plugin="datetimepicker" disabled="disabled" data-date-format="hh:mm A" id="txtEndTime" type="text"  runat="server" />
                                </div>
                                <div class="col-md-3 form-group">
                                    <h4>Request OverTime Hours</h4>
                                    <input class="form-control"  id="txtRequestOTHours" placeholder="eg: 2"  type="number" runat="server" />
                                </div>
                                <div class="col-md-3 form-group">
                                    <h4>Review OverTime Hours</h4>
                                    <input class="form-control" id="txtReviewedOTHours" placeholder="eg: 2"  type="number" runat="server" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Approve OverTime Hours</h4>
                                    <input class="form-control" id="txtApprovedOTHours" placeholder="eg: 2"  type="number" runat="server" />
                                </div>
                                <div class="col-md-12 form-group">
                                    <h4>Requested Remarks</h4>
                                    <textarea class="form-control" id="txtRequestremarks" placeholder="Remarks" type="text" runat="server" />
                                </div>
                                <div class="col-md-12 form-group">
                                    <h4>Rviewer Remarks</h4>
                                    <textarea class="form-control"  id="txtReviewedremarks" placeholder="Remarks" type="text" runat="server" />
                                </div>
                                <div class="col-md-12 form-group">
                                    <h4>Approver Remarks</h4>
                                    <textarea class="form-control" id="txtApproverremarks" placeholder="Remarks" type="text" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>



<%--                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr />
                                </div>
                            </div>--%>


                        

                                                <div class="clearfix">&nbsp;</div>
                        </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer2" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="Scripts1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
            @media only screen and (max-width: 1280px) and (min-width: 800px) {
                section.app-content .col-lg-4.col-md-6.col-sm-12 {
                    width: 40%;
                }
            }
        </style>
        <!-- Modal -->
    </form>
</body>
</html>
