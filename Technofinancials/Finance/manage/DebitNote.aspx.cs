﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.manage
{
    public partial class DebitNote : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        int DBNID
        {
            get
            {
                if (ViewState["DBNID"] != null)
                {
                    return (int)ViewState["DBNID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DBNID"] = value;
            }
        }
        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                txtDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");

                ViewState["dtRFQDetails"] = null;
                ViewState["srNo"] = null;
                ViewState["showFirstRow"] = null;
                BindDropDowns();

                dtRFQDetails = createTable();
                BindCOA();

                divAttachments.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;
                divAlertMsg.Visible = false;

                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/debit-note";


                if (HttpContext.Current.Items["DBNID"] != null)
                {
                    DBNID = Convert.ToInt32(HttpContext.Current.Items["DBNID"].ToString());
                    getDBNDetails(DBNID);
                    showFirstRow = true;
                    CheckAccess();
                }

                BindData();
            }
        }
        private void BindCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlCOA.Items.Clear();
            ddlCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

        }
        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApprove.Visible = false;
            btnReview.Visible = false;
            btnRevApprove.Visible = false;
            lnkReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReview.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;



            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "DBNs";
            objDB.PrimaryColumnnName = "DBNID";
            objDB.PrimaryColumnValue = DBNID.ToString();
            objDB.DocName = "DebitNote";

            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

            if (chkAccessLevel == "Can Edit")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReview.Visible = true;
            }
            if (chkAccessLevel == "Can Edit & Review")
            {
                btnSave.Visible = true;
                btnReview.Visible = true;
                lnkReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve")
            {
                btnSave.Visible = true;
                btnApprove.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve")
            {
                btnSave.Visible = true;
                btnRevApprove.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit")
            {
                btnSave.Visible = true;
            }
        }

        private void getDBNDetails(int dBNID)
        {
            CheckSessions();
            objDB.DBNID = DBNID;
            DataTable dt = new DataTable();
            dt = objDB.GetDBNsByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlMRN.SelectedValue = dt.Rows[0]["MRNID"].ToString();
                    ddlPOs.SelectedValue = dt.Rows[0]["POID"].ToString();
                    ddlSupplier.SelectedValue = dt.Rows[0]["SupplierID"].ToString();
                    txtDBNCode.Value = dt.Rows[0]["DBNCode"].ToString();
                    txtDate.Value = dt.Rows[0]["DBNDate"].ToString();
                    txtInvoice.Value = dt.Rows[0]["SalesInvoiceNo"].ToString();
                    txtTotalAmount.Value = dt.Rows[0]["AmountOfInvoice"].ToString();
                    txtNotes.Value = dt.Rows[0]["Notes"].ToString();

                    getDBNItems(DBNID);
                    getDBNAttachments(DBNID);
                }
            }
            Common.addlog("View", "Finance", "DBN \"" + txtDBNCode.Value + "\" Viewed", "CDNs", objDB.DBNID);

        }

        private void getDBNAttachments(int dBNID)
        {
            DataTable dt = new DataTable();
            objDB.DBNID = DBNID;
            dt = objDB.GetDBNAttachmentsByDBNID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    divAttachments.Visible = true;
                    gvAttachments.DataSource = dt;
                    gvAttachments.DataBind();
                }
            }
        }

        private void getDBNItems(int dBNID)
        {
            DataTable dt = new DataTable();
            objDB.DBNID = DBNID;
            dt = objDB.GetDBNItemsByDBNID(ref errorMsg);
            dtRFQDetails = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            BindData();
        }



        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
            return true;
        }



        private void BindDropDowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlMRN.DataSource = objDB.GetActiveMRNs(ref errorMsg);
            ddlMRN.DataTextField = "MRNCode";
            ddlMRN.DataValueField = "MRNID";
            ddlMRN.DataBind();
            ddlMRN.Items.Insert(0, new ListItem("--- Select MRN ---", "0"));
            ddlMRN.SelectedIndex = 0;

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlSupplier.DataSource = objDB.GetActiveSupplier(ref errorMsg);
            ddlSupplier.DataTextField = "SupplierName";
            ddlSupplier.DataValueField = "SupplierID";
            ddlSupplier.DataBind();
            ddlSupplier.Items.Insert(0, new ListItem("--- Select Supplier ---", "0"));
            ddlSupplier.SelectedIndex = 0;

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlPOs.DataSource = objDB.GetActivePOs(ref errorMsg);
            ddlPOs.DataTextField = "POCode";
            ddlPOs.DataValueField = "POID";
            ddlPOs.DataBind();
            ddlPOs.Items.Insert(0, new ListItem("--- Select PO ---", "0"));
            ddlPOs.SelectedIndex = 0;
        }

        protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            //code for generating pr code goes here
        }

        protected DataTable dtRFQDetails
        {
            get
            {
                if (ViewState["dtRFQDetails"] != null)
                {
                    return (DataTable)ViewState["dtRFQDetails"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtRFQDetails"] = value;
            }
        }
        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }
        protected void BindData()
        {
            if (ViewState["dtRFQDetails"] == null)
            {
                dtRFQDetails = createTable();
                ViewState["dtRFQDetails"] = dtRFQDetails;
            }
            else if (dtRFQDetails.Rows.Count == 0)
            {
                dtRFQDetails = createTable();
                showFirstRow = false;
            }
            gv.DataSource = dtRFQDetails;
            gv.DataBind();

            if (showFirstRow)
                gv.Rows[0].Visible = true;
            else
                gv.Rows[0].Visible = false;

            gv.UseAccessibleHeader = true;
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        private DataTable createTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("ItemCode");
            dt.Columns.Add("ItemID");
            dt.Columns.Add("ItemUnit");
            dt.Columns.Add("Quantity");
            dt.Columns.Add("UnitPrice");
            dt.Columns.Add("NetPrice");
            dt.Columns.Add("Remarks");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();


            return dt;

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlItemCode = e.Row.FindControl("ddlItems") as DropDownList;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlItemCode.DataSource = objDB.GetAllItems(ref errorMsg);
                ddlItemCode.DataTextField = "ItemName";
                ddlItemCode.DataValueField = "ItemID";
                ddlItemCode.DataBind();
                ddlItemCode.Items.Insert(0, new ListItem("--- Select Item ---", "0"));


                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = srNo.ToString();

                e.Row.Cells[7].ColumnSpan = 2;
                e.Row.Cells.RemoveAt(8);
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[7].ColumnSpan = 2;
                e.Row.Cells.RemoveAt(8);
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    hdnRowNo.Value = e.Row.RowIndex.ToString();
                }
            }
        }
        protected void calcTotal()
        {
            float total_Amount = 0;
            if (dtRFQDetails != null)
            {
                if (dtRFQDetails.Rows.Count > 0)
                {
                    foreach (DataRow row in dtRFQDetails.Rows)
                    {
                        if (!string.IsNullOrEmpty(row[6].ToString()))
                        {
                            total_Amount += float.Parse(row[6].ToString());
                        }
                    }
                }
            }
            txtTotalAmount.Value = total_Amount.ToString();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CheckSessions();
            bool similar = false;
            foreach (DataRow row in dtRFQDetails.Rows)
            {
                if (row[1].ToString() == ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Text)
                {
                    string itemUnit = ((DropDownList)gv.FooterRow.FindControl("ddlUnit")).SelectedItem.Text;
                    string qty = (Convert.ToInt32(row[4]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtQty")).Text)).ToString();
                    string unitPrice = (Convert.ToInt32(row[5]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtUnitPrice")).Text)).ToString();
                    string totalPrice = (Convert.ToInt32(row[6]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtNetPrice")).Text)).ToString();

                    string remarks = row[7].ToString() + "," + ((TextBox)gv.FooterRow.FindControl("txtRemarks")).Text;

                    row.SetField(3, itemUnit);
                    row.SetField(4, qty);
                    row.SetField(5, unitPrice);
                    row.SetField(6, totalPrice);
                    row.SetField(7, remarks);

                    dtRFQDetails.AcceptChanges();

                    similar = true;
                    break;
                }


            }

            if (!similar)
            {
                DataRow dr = dtRFQDetails.NewRow();
                dr[0] = srNo.ToString();
                dr[1] = ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Text;
                dr[2] = ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Value;
                dr[3] = ((DropDownList)gv.FooterRow.FindControl("ddlUnit")).SelectedItem.Text;
                dr[4] = ((TextBox)gv.FooterRow.FindControl("txtQty")).Text;
                dr[5] = ((TextBox)gv.FooterRow.FindControl("txtUnitPrice")).Text;
                dr[6] = ((TextBox)gv.FooterRow.FindControl("txtNetPrice")).Text;
                dr[7] = ((TextBox)gv.FooterRow.FindControl("txtRemarks")).Text;
                dtRFQDetails.Rows.Add(dr);
                dtRFQDetails.AcceptChanges();

                srNo += 1;
            }
            BindData();


            ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
            calcTotal();

        }

        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void gv_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {

        }

        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            //dtRFQDetails.Rows[index].SetField(3, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditSuppliers")).SelectedItem.Text);
            //dtRFQDetails.Rows[index].SetField(4, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditSuppliers")).SelectedItem.Value);
            dtRFQDetails.Rows[index].SetField(3, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditUnit")).SelectedItem.Text);
            dtRFQDetails.Rows[index].SetField(4, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditQty")).Text);
            dtRFQDetails.Rows[index].SetField(5, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditUnitPrice")).Text);
            dtRFQDetails.Rows[index].SetField(6, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditNetPrice")).Text);
            dtRFQDetails.Rows[index].SetField(7, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditRemarks")).Text);


            dtRFQDetails.AcceptChanges();

            gv.EditIndex = -1;
            BindData();
            calcTotal();
        }

        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindData();
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {

            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
            {
                if (dtRFQDetails.Rows[i][0].ToString() == delSr)
                {
                    dtRFQDetails.Rows[i].Delete();
                    dtRFQDetails.AcceptChanges();
                }
            }
            int count = 1;
            for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
            {
                dtRFQDetails.Rows[i].SetField(0, count);
                dtRFQDetails.AcceptChanges();
                count++;
            }
            srNo = count;
            BindData();
            calcTotal();
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                BindData();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.MRNID = Convert.ToInt32(ddlMRN.SelectedItem.Value);
                objDB.POID = Convert.ToInt32(ddlPOs.SelectedItem.Value);
                objDB.SupplierID = Convert.ToInt32(ddlSupplier.SelectedItem.Value);
                objDB.DBNCode = txtDBNCode.Value;
                objDB.DBNDate = txtDate.Value;
                objDB.SalesInvoiceNo = txtInvoice.Value;
                objDB.AmountOfInvoice = float.Parse(txtTotalAmount.Value);
                objDB.Notes = txtNotes.Value;
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.ChartOfAccountID = int.Parse(ddlCOA.SelectedValue);

                if (HttpContext.Current.Items["DBNID"] != null)
                {
                    objDB.DBNID = DBNID;
                    res = objDB.UpdateDBNs();
                }
                else
                {
                    string returnedID = objDB.AddDBNs();
                    DBNID = Convert.ToInt32(returnedID);
                    clearFields();
                    res = "New DBN Added Successfully";
                }

                if (itemAttachments.HasFile || itemAttachments.HasFiles)
                {
                    foreach (HttpPostedFile file in itemAttachments.PostedFiles)
                    {
                        Random rand = new Random((int)DateTime.Now.Ticks);
                        int randnum = 0;
                        randnum = rand.Next(1, 100000);

                        string fn = "";
                        string exten = "";

                        string destDir = Server.MapPath("~/assets/files/DBNs/");
                        randnum = rand.Next(1, 100000);
                        fn = txtDBNCode.Value + "_" + randnum;

                        if (!Directory.Exists(destDir))
                        {
                            Directory.CreateDirectory(destDir);
                        }

                        string fname = Path.GetFileName(file.FileName);
                        exten = Path.GetExtension(file.FileName);
                        file.SaveAs(destDir + fn + exten);

                        objDB.DBNID = Convert.ToInt32(DBNID);
                        objDB.AttachmentPath = "http://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/DBNs/" + fn + exten;
                        objDB.AddDBNAttachments();
                    }
                }

                objDB.DBNID = DBNID;
                objDB.DeleteDBNItemsByDBNID();
                dtRFQDetails = (DataTable)ViewState["dtRFQDetails"] as DataTable;
                if (dtRFQDetails != null)
                {
                    if (dtRFQDetails.Rows.Count > 0)
                    {
                        if (!showFirstRow)
                        {
                            dtRFQDetails.Rows[0].Delete();
                            dtRFQDetails.AcceptChanges();
                        }

                        for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
                        {
                            objDB.DBNID = Convert.ToInt32(DBNID);
                            objDB.ItemID = Convert.ToInt32(dtRFQDetails.Rows[i]["ItemID"].ToString());
                            objDB.ItemUnit = dtRFQDetails.Rows[i]["ItemUnit"].ToString();
                            objDB.Qty = Convert.ToInt32(dtRFQDetails.Rows[i]["Quantity"].ToString());
                            objDB.UnitPrice = Convert.ToInt32(dtRFQDetails.Rows[i]["UnitPrice"].ToString());
                            objDB.NetPrice = Convert.ToInt32(dtRFQDetails.Rows[i]["NetPrice"].ToString());
                            objDB.Remarks = dtRFQDetails.Rows[i]["Remarks"].ToString();
                            objDB.ModifiedBy = Session["UserName"].ToString();

                            objDB.AddDBNItems();
                        }
                    }
                }


                if (!showFirstRow)
                {
                    ViewState["dtRFQDetails"] = null;
                    ViewState["srNo"] = null;
                }

                BindData();
                if (res == "New DBN Added Successfully" || res == "DBN Updated Successfully")
                {
                    if (res == "New DBN Added Successfully") { Common.addlog("Add", "Finance", " DBN \"" + objDB.DBNCode + "\" Added", "DBNs"); }
                    if (res == "DBN Updated Successfully") { Common.addlog("Update", "Finance", "DBN\"" + objDB.DBNCode + "\" Updated", "DBNs", objDB.DBNID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtNotes.Value = "";
            //dtRFQDetails = createTable();
            ddlMRN.SelectedIndex = 0;
            ddlPOs.SelectedIndex = 0;
            txtDBNCode.Value = "";
            txtInvoice.Value = "";
            ddlSupplier.SelectedIndex = 0;
            txtDBNCode.Value = "";
            txtTotalAmount.Value = "0";
            txtNotes.Value = "";

        }

        protected void ddlRFQ_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CheckSessions();
            //if (ddlRFQ.SelectedIndex != 0)
            //{
            //    txtPOCode.Value = "PO_" + ddlRFQ.SelectedItem.Text.Split('_')[1];
            //    getRFQItemsByRFQID(Convert.ToInt32(ddlRFQ.SelectedItem.Value));
            //    calcTotal();
            //}
            //else
            //{
            //    txtPOCode.Value = "";
            //    dtRFQDetails = createTable();
            //}
            BindData();
        }

        protected void txtNetPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            TextBox txtQty = gv.FooterRow.FindControl("txtQty") as TextBox;
            string qty = txtQty.Text;
            TextBox txtUnitPrice = gv.FooterRow.FindControl("txtUnitPrice") as TextBox;
            string unitPrice = txtUnitPrice.Text;
            TextBox txtRemarks = gv.FooterRow.FindControl("txtRemarks") as TextBox;

            if (!string.IsNullOrEmpty(qty) && !string.IsNullOrEmpty(unitPrice))
            {
                TextBox txtNetPrice = gv.FooterRow.FindControl("txtNetPrice") as TextBox;
                string netPrice = (Convert.ToInt32(qty) * Convert.ToInt32(unitPrice)).ToString();

                txtNetPrice.Text = netPrice;
            }
            TextBox txtSender = (TextBox)sender as TextBox;
            if (txtSender.ID == "txtQty")
                txtUnitPrice.Focus();
            else if (txtSender.ID == "txtUnitPrice")
                txtRemarks.Focus();

        }

        protected void txtEditUnitPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            int rowNum = Convert.ToInt32(hdnRowNo.Value);
            TextBox txtQty = gv.Rows[rowNum].FindControl("txtEditQty") as TextBox;
            string qty = txtQty.Text;
            TextBox txtUnitPrice = gv.Rows[rowNum].FindControl("txtEditUnitPrice") as TextBox;
            string unitPrice = txtUnitPrice.Text;

            if (!string.IsNullOrEmpty(qty) && !string.IsNullOrEmpty(unitPrice))
            {
                TextBox txtNetPrice = gv.Rows[rowNum].FindControl("txtEditNetPrice") as TextBox;
                string netPrice = (Convert.ToInt32(qty) * Convert.ToInt32(unitPrice)).ToString();

                txtNetPrice.Text = netPrice;
            }

            TextBox txtSender = (TextBox)sender as TextBox;
            txtSender.Focus();
        }

        protected void gv_PreRender(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
                gv.UseAccessibleHeader = true;
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void getRFQItemsByRFQID(int RFQID)
        {
            DataTable dt = new DataTable();
            objDB.RFQID = RFQID;
            dt = objDB.GetRFQItemsByRFQID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dtRFQDetails.Rows.Add(new object[] {
                            row[0],
                            row[1],
                            row[2],
                            row[3],
                            row[4],
                            "0",
                            "0",
                            row[5]
                        });

                        dtRFQDetails.AcceptChanges();
                    }

                    BindData();

                }
            }
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "DBNs", "DBNID", HttpContext.Current.Items["DBNID"].ToString(), Session["UserName"].ToString());
            Common.addlogNew(res, "Finance", "Debit Note of ID\"" + HttpContext.Current.Items["DBNID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/debit-note", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/debit-note/edit-debit-note-" + HttpContext.Current.Items["DBNID"].ToString(), "Debit Note \"" + txtDBNCode.Value + "\"", "DBNs", "DebitNote", Convert.ToInt32(HttpContext.Current.Items["DBNID"].ToString()));

            //Common.addlog(res, "Finance", "Debit Note of ID \"" + HttpContext.Current.Items["DBNID"].ToString() + "\" Status Changed", "DBNs", DBNID);

            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;

            CheckAccess();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            string type = btn.CommandArgument;
            string res = Common.addAccessLevels(type, "DBNs", "DBNID", HttpContext.Current.Items["DBNID"].ToString(), Session["UserName"].ToString());
            Common.addlog("Delete", "Finance", "Debit Note of ID \"" + HttpContext.Current.Items["DBNID"].ToString() + "\" deleted", "DBNs", DBNID);

            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            Response.Redirect(btnBack.HRef);

        }

        protected void ddlMRN_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            if (ddlMRN.SelectedIndex != 0)
            {
                getMRNDetails(Convert.ToInt32(ddlMRN.SelectedItem.Value));
                objDB.MRNID = Convert.ToInt32(ddlMRN.SelectedItem.Value);
                txtDBNCode.Value = objDB.GenereateDBNCodeByMRNID();
                calcTotal();
            }
            else
            {
                ddlPOs.SelectedIndex = 0;
                ddlSupplier.SelectedIndex = 0;
                txtInvoice.Value = "";
                txtTotalAmount.Value = "0";
                txtDBNCode.Value = "";
                showFirstRow = false;
                srNo = 1;
                dtRFQDetails = createTable();
            }
            BindData();
        }

        private void getMRNDetails(int MRNID)
        {
            DataTable dt = new DataTable();
            objDB.MRNID = MRNID;
            dt = objDB.GetMRNsByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    showFirstRow = true;
                    getPOItemsByPOID(Convert.ToInt32(dt.Rows[0]["POID"].ToString()));
                    getPODetails(Convert.ToInt32(dt.Rows[0]["POID"].ToString()));
                }
            }
        }

        private void getPODetails(int POID)
        {
            DataTable dt = new DataTable();
            objDB.POID = POID;
            dt = objDB.GetPOsByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlPOs.SelectedValue = dt.Rows[0]["POID"].ToString();
                    ddlSupplier.SelectedValue = dt.Rows[0]["SupplierID"].ToString();
                    txtTotalAmount.Value = dt.Rows[0]["POAmount"].ToString();
                }
            }
        }


        private void getPOItemsByPOID(int POID)
        {
            DataTable dt = new DataTable();
            objDB.POID = POID;
            dt = objDB.GetPOItemsByPOID(ref errorMsg);
            dtRFQDetails = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }
        }

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

    }
}