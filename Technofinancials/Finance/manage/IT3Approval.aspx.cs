﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Finance.Manage
{
    public partial class IT3Approval : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int DirectReportEmployeeID
        {
            get
            {
                if (ViewState["DirectReportEmployeeID"] != null)
                {
                    return (int)ViewState["DirectReportEmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DirectReportEmployeeID"] = value;
            }
        }
       
        protected int EmployeeID
        {
            get
            {
                if (ViewState["EmployeeID"] != null)
                {
                    return (int)ViewState["EmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["EmployeeID"] = value;
            }
        }
        protected int IT3_ID
        {
            get
            {
                if (ViewState["IT3_ID"] != null)
                {
                    return (int)ViewState["IT3_ID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["IT3_ID"] = value;
            }
        }
        protected string DocumentStatus
        {
            get
            {
                if (ViewState["DocumentStatus"] != null)
                {
                    return (string)ViewState["DocumentStatus"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocumentStatus"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {
                    ViewState["PersonName"] = "";
                    hdnPFAmount.Value = "0";
                    CheckSessions();
                    ViewState["loanID"] = null;
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                  
                    btnRejDisApprove.Visible = false;
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/IT3Approval";

                    BindDropDown();
                    GetPFDeatils();
                    lnkDelete.Visible = false;
                  



                    divAlertMsg.Visible = false;
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                    DataTable dtEmp = objDB.GetEmployeeByID(ref errorMsg);

                    if (dtEmp != null)
                    {
                        if (dtEmp.Rows.Count > 0 && dtEmp.Rows[0]["DirectReportingTo"].ToString() != "")
                        {
                            DirectReportEmployeeID = Convert.ToInt32(dtEmp.Rows[0]["DirectReportingTo"].ToString());
                        }
                    }


                    if (HttpContext.Current.Items["IT3_ID"] != null)
                    {
                        IT3_ID = Convert.ToInt32(HttpContext.Current.Items["IT3_ID"].ToString());
                        CheckAccess();
                        

                        getIT3byID(IT3_ID);
                        checkDocStatus();
                       

                    }
                    else
                    {
                        DocumentStatus = "Saved as Draft";
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void GetPFDeatils()
        {
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString()); 
            DataTable dt = objDB.GetProvinentFundByEmployeeID(ref errorMsg);
            hdnPFAmount.Value = dt.Rows[0]["RemaningAmount"].ToString();
            //gvPFDetails.DataSource = dt;
            //gvPFDetails.DataBind();
        }
        private void BindDropDown()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
               
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getIT3byID(int IT3_ID)
        {
            try
            {
                DataTable dt = new DataTable();
              
                objDB.IT3_ID = IT3_ID;
                dt = objDB.GetIT3ByID(ref errorMsg);

              

                if (dt != null)
                {
                    employeename.Value = dt.Rows[0]["Created_by"].ToString();
                    WDID.Value = dt.Rows[0]["wdid"].ToString();
                    subdate.Value= dt.Rows[0]["Created_Date2"].ToString();
                    txteffective.Text = dt.Rows[0]["Payroll"].ToString();
                    amountclaim.Value= dt.Rows[0]["Total_Claim"].ToString();
                    amountapp.Value = dt.Rows[0]["Approved_Claim"].ToString();
                    allowcredit.Checked = dt.Rows[0]["isallow"].ToString() == "True" ? true : false;
                    downloadIT3.HRef= dt.Rows[0]["File_Path"].ToString();
                    DocumentStatus = dt.Rows[0]["status"].ToString();
                    EmployeeID = Convert.ToInt32(dt.Rows[0]["Employee_ID"].ToString());

                }
                Common.addlog("View", "Finance", "IT3 \"" + IT3_ID + "\" Viewed", "IT3", IT3_ID);


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                divAlertMsg.Visible = false;
                string res = "";


                objDB.PayrollDate = txteffective.Text;
                objDB.isActive = allowcredit.Checked == true ? true : false;
                objDB.ModifiedBy = Session["Username"].ToString();
                objDB.IT3_ID = IT3_ID;
                if (DocumentStatus == "Submitted")
                {
                    objDB.PayrollDate = txteffective.Text;
                    objDB.ApproveAmount = Convert.ToDouble(amountapp.Value);
                    objDB.Status = "Approved";

                }
                else
                {
                    objDB.Status = "Modified";

                }

                res = objDB.Update_IT3_Submission();
				if (res == "Approved")
				{
                    Common.addlogNew(res, "IT3 Form Approved",
             "IT3 Form Approved",
             "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "employee-self-service/view/IT3",
             "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/view/IT3", "IT3 Form Approved", "IT3_Submission", "ESS", EmployeeID);

                }
               
                checkDocStatus();
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Saved Successfully";
                
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                divAlertMsg.Visible = false;
                string res = "";



                objDB.isActive = false;
                objDB.ModifiedBy = Session["Username"].ToString();
                objDB.IT3_ID = IT3_ID;
                objDB.ApproveAmount = amountapp.Value =="" ? 0 : Convert.ToDouble(amountapp.Value);
                objDB.Status = "Rejected";
                objDB.Remarks = txtNote.Value;
           



                res = objDB.Update_IT3_Submission();
                if (res == "Rejected")
                {
                    Common.addlogNew(res, "IT3 Form Rejected",
             "IT3 Form Rejected",
             "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "employee-self-service/view/IT3",
             "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/view/IT3", "IT3 Form Rejected", "IT3_Submission", "ESS", EmployeeID);

                }
                checkDocStatus();
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Rejected";

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {

           // //txtLoanTitle.Value = "";
           // txtLoanAmount.Value = "";
           //// txtGrantDate.Value = "";
           // txtNoOfInstallments.Value = "";
           // txtNotes.Value = "";
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            //try
            //{
            //    CheckSessions();
            //    System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            //    string res = Common.addAccessLevels(btn.ID.ToString(), "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
            //    Common.addlogNew(res, "ESS", "Loan of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/view/loans", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/loans/edit-loan-" + HttpContext.Current.Items["LoanID"].ToString(), "Loan \"" + txtLoanTitle.Value + "\"", "Loans", "loans", Convert.ToInt32(HttpContext.Current.Items["LoanID"].ToString()));

            //    //Common.addlog(res, "ESS", "Loan of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "Loans", loanID);

            //    divAlertMsg.Visible = true;
            //    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            //    pAlertMsg.InnerHtml = res;
            //    btnSubForReview.Visible = false;
            //    btnSave.Visible = false;
            //    btnNotes.Visible = false;
            //    lnkDelete.Visible = false;
            //}
            //catch (Exception ex)
            //{
            //    divAlertMsg.Visible = true;
            //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
            //    pAlertMsg.InnerHtml = ex.Message;
            //}
            try
            {
                CheckSessions();
                string res = "";
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;

                if (btn.ID.ToString() == "btnSubForReview")
                {
                    if (Session["EmployeeID"] != null)
                    {

                        if (DirectReportEmployeeID == Convert.ToInt32(Session["EmployeeID"].ToString()))
                        {
                            objDB.Status = "Submited By Manager";
                          //  objDB.LoanID = loanID;
                            res = objDB.UpdateLoanStatus();
                        }
                        else
                        {
                            objDB.Status = "Submited By Employee";
                           // objDB.LoanID = loanID;
                            res = objDB.UpdateLoanStatus();
                        }
                    }
                    //res = Common.addAccessLevels(btn.ID.ToString(), "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
                    //Common.addlogNew(res, "HR", "OtherExpense of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/other-expense", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/other-expense/edit-other-expense-" + LoanID, "OtherExpense of ID \"" + LoanID + "\"", "Loans", "Loans", LoanID);
                }


                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
            checkDocStatus();

        }
        private void checkDocStatus()
        {
            CheckSessions();
            
            if (DocumentStatus == "Approved")
            {
            btnApprove.Visible = true;
            modalReject.Visible = false;
            txteffective.Enabled = false;
            amountapp.Disabled = true;
            


            }
            else if (DocumentStatus == "Rejected")
            {
                btnApprove.Visible = false;
                modalReject.Visible = false;
                txteffective.Enabled = false;
                amountapp.Disabled = true;



            }
            else
		    {
            
            btnApprove.Visible = true;
            modalReject.Visible = true;

            }
            
           
        }
        private void CheckAccess()
        {
            try
            {

               
                btnReview.Visible = false;
                lnkDelete.Visible = false;
             
                btnApprove.Visible = false;
                btnReview.Visible = false;
             
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
              
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "IT3_Submission";
                objDB.PrimaryColumnnName = "IT3_ID";
                objDB.PrimaryColumnValue = HttpContext.Current.Items["IT3_ID"].ToString();
                objDB.DocName = "IT3";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                  
                    lnkDelete.Visible = true;
              
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                   
                    btnReview.Visible = true;
                    //lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    
                    //btnApprove.Visible = true;
                    //btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                   
                    //btnRevApprove.Visible = true;

                    //btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    //btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "Finance", "IT3_ID of ID \"" + HttpContext.Current.Items["LoanID"].ToString() + "\" deleted", "Loans", IT3_ID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

		
    }
}