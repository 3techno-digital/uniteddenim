﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Technofinancials.Finance.manage
{
    public partial class Invoice : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        string POStatus
        {
            get
            {
                if (ViewState["POStatus"] != null)
                {
                    return ViewState["POStatus"].ToString();
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["POStatus"] = value;
            }
        }
        int InvoiceID
        {
            get
            {
                if (ViewState["InvoiceID"] != null)
                {
                    return (int)ViewState["InvoiceID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["InvoiceID"] = value;
            }
        }

        int TraID
        {
            get
            {
                if (ViewState["TraID"] != null)
                {
                    return (int)ViewState["TraID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["TraID"] = value;
            }
        }


        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                hdnTransactionID.Value = "0";
                // Check Discount and WOT Chart of Accounts

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Title = "Discount";
                string res = objDB.GetCOAQuickSetupBybYtITLE();
                if (res != "0" && res != "")
                {
                    objDB.Title = "WOT";
                    res = objDB.GetCOAQuickSetupBybYtITLE();
                    if (res == "0" || res == "")
                    {
                        Session["From"] = "Invoice";
                        Session["COATitle"] = "WOT";
                        if (HttpContext.Current.Items["InvoiceID"] != null)
                        {
                            InvoiceID = Convert.ToInt32(HttpContext.Current.Items["InvoiceID"].ToString());
                            Session["FromURL"] = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/invoice/edit-invoice-" + InvoiceID;
                        }
                        else
                        {
                            Session["FromURL"] = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/invoice/add-invoice";
                        }
                        Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/COAQuickSetup/add-COAQuickSetup");
                    }
                    else
                    {
                        objDB.Title = "STWH";
                        res = objDB.GetCOAQuickSetupBybYtITLE();
                        if (res == "0" || res == "")
                        {
                            Session["From"] = "Invoice";
                            Session["COATitle"] = "STWH";
                            if (HttpContext.Current.Items["InvoiceID"] != null)
                            {
                                InvoiceID = Convert.ToInt32(HttpContext.Current.Items["InvoiceID"].ToString());
                                Session["FromURL"] = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/invoice/edit-invoice-" + InvoiceID;
                            }
                            else
                            {
                                Session["FromURL"] = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/invoice/add-invoice";
                            }
                            Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/COAQuickSetup/add-COAQuickSetup");
                        }
                    }
                }
                else
                {
                    Session["COATitle"] = "Discount";
                    Session["From"] = "Invoice";
                    if (HttpContext.Current.Items["InvoiceID"] != null)
                    {
                        InvoiceID = Convert.ToInt32(HttpContext.Current.Items["InvoiceID"].ToString());
                        Session["FromURL"] = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/invoice/edit-invoice-" + InvoiceID;
                    }
                    else
                    {
                        Session["FromURL"] = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/invoice/add-invoice";
                    }
                    Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/COAQuickSetup/add-COAQuickSetup");
                }

                //End Check Discount and WOT Chart of Accounts


                txtDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");
                BindDropDowns();
                //accDiv.Visible = false;

                btnPayNow.Visible = false;

                // divPayNow.Visible = false;
                divPay.Visible = false;
                ViewState["dtTaxTable"] = null;


                ViewState["srNo"] = null;
                ViewState["dtRFQDetails"] = null;
                ViewState["showFirstRow"] = null;
                dtRFQDetails = createTable();

                divRecurrig.Visible = false;
                BindCOA();
                BindCOAPN("Cash");
                divAttachments.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;
                divAlertMsg.Visible = false;
                btnGenVoucher.Visible = false;
                //btnPayVoucher.Visible = false;

                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/invoice";

                BindTaxesListBox();
                // txtRFQCode.Value = objDB.GenerateRFQCode();
                showFirstRow = false;
                DataTable dtCode = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                dtCode = objDB.GetInvoiceCode(ref errorMsg);
                if (dtCode != null)
                {
                    if (dtCode.Rows.Count > 0)
                    {
                        txtCode.Value = dtCode.Rows[0][0].ToString();
                    }
                }
                if (HttpContext.Current.Items["InvoiceID"] != null)
                {
                    InvoiceID = Convert.ToInt32(HttpContext.Current.Items["InvoiceID"].ToString());
                    getInvoiceDetails(InvoiceID);
                    showFirstRow = true;
                    getAllVoucher();
                    CheckAccess();
                }

                if (HttpContext.Current.Items["ItemID"] != null)
                {
                    RFQDIV.Visible = false;
                    int itemID = Convert.ToInt32(HttpContext.Current.Items["ItemID"].ToString());
                    getItemDetails(itemID);
                    showFirstRow = true;

                }

                BindData();

            }
        }
        private void BindCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            ddlCOA.Items.Clear();
            ddlCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("--- Select COA---", "0"));

        }
        private void BindCOAPN(string COAFor)
        {
            objDB.COAFor = COAFor;
            objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            ddlCOAPN.Items.Clear();
            ddlCOAPN.DataSource = objDB.GetAllApprovedCOAsFor(ref errorMsg);
            ddlCOAPN.DataTextField = "CodeTitle";
            ddlCOAPN.DataValueField = "COA_ID";
            ddlCOAPN.DataBind();
            ddlCOAPN.Items.Insert(0, new ListItem("--- Select COA- --", "0"));

        }
        private void getItemDetails(int itemID)
        {
            if (CheckSessions())
            {
                DataTable dt = new DataTable();
                objDB.ItemID = itemID;
                dt = objDB.GetProductAndServiceByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        //ddlSupplier.SelectedValue = dt.Rows[0]["PreferredSupplierID"].ToString();
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                        // txtCode.Value = objDB.GenereatePOCodeForQuickPurchase();


                        if (dtRFQDetails.Rows[0][0].ToString() == "")
                        {
                            dtRFQDetails.Rows[0].Delete();
                            dtRFQDetails.AcceptChanges();
                        }

                        DataRow dr = dtRFQDetails.NewRow();
                        dr[0] = "1";
                        dr[1] = dt.Rows[0]["ItemCode"].ToString();
                        dr[2] = dt.Rows[0]["ItemID"].ToString();
                        dr[3] = dt.Rows[0]["MeasurmentUnit"].ToString();
                        dr[4] = "0";
                        dr[5] = dt.Rows[0]["UnitPrice"].ToString();
                        dr[6] = "0";
                        dr[7] = "";
                        srNo = 2;

                        dtRFQDetails.Rows.Add(dr);
                        dtRFQDetails.AcceptChanges();

                    }
                }
            }
        }

        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApprove.Visible = false;
            btnReview.Visible = false;
            btnRevApprove.Visible = false;
            lnkReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReview.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;



            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "Invoices";
            objDB.PrimaryColumnnName = "InvoiceID";
            objDB.PrimaryColumnValue = InvoiceID.ToString();
            objDB.DocName = "Invoice";

            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

            if (chkAccessLevel == "Can Edit")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReview.Visible = true;
            }
            if (chkAccessLevel == "Can Edit & Review")
            {
                btnSave.Visible = true;
                btnReview.Visible = true;
                lnkReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve")
            {
                btnSave.Visible = true;
                btnApprove.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve")
            {
                btnSave.Visible = true;
                btnRevApprove.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit")
            {
                btnSave.Visible = true;
            }
        }

        protected DataTable dtTaxTable
        {
            get
            {
                if (ViewState["dtTaxTable"] != null)
                {
                    return (DataTable)ViewState["dtTaxTable"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtTaxTable"] = value;
            }
        }

        private void BindTaxesListBox()
        {
            if (CheckSessions())
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                dtTaxTable = objDB.GetAllTaxRates(ref errorMsg);
                lbTaxes.DataSource = dtTaxTable;
                lbTaxes.DataTextField = "TaxRateShortName";
                lbTaxes.DataValueField = "TaxRateID";
                lbTaxes.DataBind();
            }
        }



        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
            return true;
        }

        private void getInvoiceDetails(int InvoiceID)
        {
            DataTable dt = new DataTable();
            objDB.InvoiceID = InvoiceID;
            dt = objDB.GetInvoiceByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    POStatus = dt.Rows[0]["DocStatus"].ToString();

                    ddlClient.SelectedValue = dt.Rows[0]["ClientID"].ToString();
                    ddlLocation.SelectedValue = dt.Rows[0]["SiteID"].ToString();



                    ddlQuotation.SelectedValue = (dt.Rows[0]["QouteID"].ToString() != null && dt.Rows[0]["QouteID"].ToString() != "") ? dt.Rows[0]["QouteID"].ToString() : "0";

                    //ddlCOA.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                    txtTitle.Value = dt.Rows[0]["Title"].ToString();
                    txtDiscount.Text = dt.Rows[0]["Discount"].ToString();
                    txtDescription.Value = dt.Rows[0]["Description"].ToString();

                    txtCode.Value = dt.Rows[0]["Code"].ToString();
                    txtDate.Value = dt.Rows[0]["Date"].ToString();
                    txtNetAmount.Value = dt.Rows[0]["Amount"].ToString();
                    txtNotes.Value = dt.Rows[0]["Notes"].ToString();
                    txtRemainingAmount.Value = dt.Rows[0]["RemainingAmount"].ToString();
                    if (dt.Rows[0]["isRecurring"].ToString() == "True")
                    {
                        chkIsRecurring.Checked = true;
                        divRecurrig.Visible = true;
                        txtNoOfInvoices.Value = dt.Rows[0]["NoOfInvoices"].ToString();
                        txtInvoiceDuration.Value = dt.Rows[0]["InvoiceDuration"].ToString();
                        txtDueInvoice.Value = dt.Rows[0]["DueDuration"].ToString();
                        if (dt.Rows[0]["isAuto"].ToString() == "True")
                        {
                            chkIsAuto.Checked = true;
                        }
                    }


                    if (dt.Rows[0]["DocStatus"].ToString() == "Approved" && dt.Rows[0]["isPaid"].ToString() != "True")
                    {
                        btnPayNow.Visible = true;
                        txtDatePN.Value = DateTime.Now.ToString("dd-MMM-yyyy");



                    }
                    else
                    {
                        btnPayNow.Visible = false;

                    }

                    if (dt.Rows[0]["DocStatus"].ToString() == "Approved")
                    {
                        btnGenVoucher.Visible = true;
                        //btnPayVoucher.Visible = true;


                    }
                    else
                    {
                        btnGenVoucher.Visible = false;
                        //btnPayVoucher.Visible = false;

                    }
                    getInvoiceItemsByInvoiceID(InvoiceID);
                    getInvoiceAttachmentsByInvoiceID(InvoiceID);
                    GetInvoiceTaxesByInvoiceID(InvoiceID);
                }
            }
            Common.addlog("View", "Finance", "Invoice \"" + txtCode.Value + "\" Viewed", "Invoices", objDB.InvoiceID);

        }

        private void GetInvoiceTaxesByInvoiceID(int InvoiceID)
        {
            if (CheckSessions())
            {
                DataTable dt = new DataTable();
                objDB.InvoiceID = InvoiceID;
                dt = objDB.GetInvoiceTaxesByInvoiceID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            foreach (ListItem li in lbTaxes.Items)
                            {
                                if (dt.Rows[i]["TaxRateID"].ToString() == li.Value)
                                    li.Selected = true;
                            }
                        }
                    }
                }
            }
        }

        private void getInvoiceAttachmentsByInvoiceID(int InvoiceID)
        {
            DataTable dt = new DataTable();
            objDB.InvoiceID = InvoiceID;
            dt = objDB.GetInvoiceAttachmentsByInvoiceID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    divAttachments.Visible = true;
                    gvAttachments.DataSource = dt;
                    gvAttachments.DataBind();
                }
            }
        }

        private void getInvoiceItemsByInvoiceID(int InvoiceID)
        {
            DataTable dt = new DataTable();
            objDB.InvoiceID = InvoiceID;
            dt = objDB.GetInvoiceItemsByInvoiceID(ref errorMsg);
            dtRFQDetails = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }
        }

        private void BindDropDowns()
        {


            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlLocation.DataSource = objDB.GetAllApprovedLocations(ref errorMsg);
            ddlLocation.DataTextField = "SiteName";
            ddlLocation.DataValueField = "SiteID";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("--- Select Location ---", "0"));
            ddlLocation.SelectedIndex = 0;

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlClient.DataSource = objDB.GetAllApprovedClients(ref errorMsg);
            ddlClient.DataTextField = "ClientName";
            ddlClient.DataValueField = "ClientID";
            ddlClient.DataBind();
            ddlClient.Items.Insert(0, new ListItem("--- Select Client ---", "0"));
            ddlClient.SelectedIndex = 0;


            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlQuotation.DataSource = objDB.GetAllApprovedQuotations(ref errorMsg);
            ddlQuotation.DataTextField = "Code";
            ddlQuotation.DataValueField = "QuoteID";
            ddlQuotation.DataBind();
            ddlQuotation.Items.Insert(0, new ListItem("--- Select---", "0"));
            ddlQuotation.SelectedIndex = 0;

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlAccounts.DataSource = objDB.GetAllApprovedAccount(ref errorMsg);
            ddlAccounts.DataTextField = "AccountTitle";
            ddlAccounts.DataValueField = "AccountID";
            ddlAccounts.DataBind();
            ddlAccounts.Items.Insert(0, new ListItem("--- Select---", "0"));
            ddlAccounts.SelectedIndex = 0;
            //ddlPRs.DataSource = objDB.GetActivePurchaseRequisite(ref errorMsg);
            //ddlPRs.DataTextField = "PRCode";
            //ddlPRs.DataValueField = "PRID";
            //ddlPRs.DataBind();
            //ddlPRs.Items.Insert(0, new ListItem("--- Select PR ---", "0"));
            //ddlPRs.Items.Insert(1, new ListItem("--- None ---", "0"));
            //ddlPRs.SelectedIndex = 0;
        }

        protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            //code for generating pr code goes here
        }

        protected DataTable dtRFQDetails
        {
            get
            {
                if (ViewState["dtRFQDetails"] != null)
                {
                    return (DataTable)ViewState["dtRFQDetails"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtRFQDetails"] = value;
            }
        }
        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }

        protected void BindData()
        {
            if (ViewState["dtRFQDetails"] == null)
            {
                dtRFQDetails = createTable();
                ViewState["dtRFQDetails"] = dtRFQDetails;
            }
            else if (dtRFQDetails.Rows.Count == 0)
            {
                dtRFQDetails = createTable();
                showFirstRow = false;
            }
            gv.DataSource = dtRFQDetails;
            gv.DataBind();

            if (showFirstRow)
                gv.Rows[0].Visible = true;
            else
                gv.Rows[0].Visible = false;

            gv.UseAccessibleHeader = true;
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;

            calcTotal();
        }

        private DataTable createTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("ItemCode");
            dt.Columns.Add("ProductAndServicesID");
            dt.Columns.Add("ItemUnit");
            dt.Columns.Add("Quantity");
            dt.Columns.Add("UnitPrice");
            dt.Columns.Add("NetPrice");
            dt.Columns.Add("Remarks");
            dt.Columns.Add("COA_ID");

            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();


            return dt;

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                DropDownList ddlItemCode = e.Row.FindControl("ddlItems") as DropDownList;
                ddlItemCode.DataSource = objDB.GetAllApprovedProductAndService(ref errorMsg);
                ddlItemCode.DataTextField = "Name";
                ddlItemCode.DataValueField = "ProductAndServicesID";
                ddlItemCode.DataBind();
                ddlItemCode.Items.Insert(0, new ListItem("--- Select---", "0"));

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                DropDownList ddlExpenseCOA = e.Row.FindControl("ddlExpCOA") as DropDownList;
                ddlExpenseCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
                ddlExpenseCOA.DataTextField = "CodeTitle";
                ddlExpenseCOA.DataValueField = "COA_ID";
                ddlExpenseCOA.DataBind();
                ddlExpenseCOA.Items.Insert(0, new ListItem("--- Select COA ---", "0"));
                //DropDownList ddlSuppliers = e.Row.FindControl("ddlSuppliers") as DropDownList;
                //ddlSuppliers.DataSource = objDB.GetActiveSupplier(ref errorMsg);
                //ddlSuppliers.DataTextField = "SupplierName";
                //ddlSuppliers.DataValueField = "SupplierID";
                //ddlSuppliers.DataBind();
                //ddlSuppliers.Items.Insert(0, new ListItem("--- None ---", "0"));


                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = srNo.ToString();

                e.Row.Cells[7].ColumnSpan = 2;
                e.Row.Cells.RemoveAt(8);
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //gv.UseAccessibleHeader = true;
                //e.Row.TableSection = TableRowSection.TableHeader;
                e.Row.Cells[7].ColumnSpan = 2;
                e.Row.Cells.RemoveAt(8);
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                    DropDownList ddlExpenseCOA = e.Row.FindControl("ddlEditExpCOA") as DropDownList;
                    ddlExpenseCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
                    ddlExpenseCOA.DataTextField = "CodeTitle";
                    ddlExpenseCOA.DataValueField = "COA_ID";
                    ddlExpenseCOA.DataBind();
                    ddlExpenseCOA.Items.Insert(0, new ListItem("--- Select COA ---", "0"));



                    hdnRowNo.Value = e.Row.RowIndex.ToString();
                    int rowIndex = e.Row.RowIndex;

                    ddlExpenseCOA.Items.IndexOf(ddlExpenseCOA.Items.FindByText(dtRFQDetails.Rows[rowIndex][6].ToString()));
                }
            }

            //if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
            //{
            //    if (!string.IsNullOrEmpty(e.Row.Cells[6].Text.ToString()))
            //    {
            //        total_Amount += float.Parse(e.Row.Cells[6].Text.ToString());
            //    }
            //}

            //txtTotalAmount.Value = total_Amount.ToString();
        }
        protected void calcTotal()
        {
            float total_Amount = 0;
            if (dtRFQDetails != null)
            {
                if (dtRFQDetails.Rows.Count > 0)
                {
                    foreach (DataRow row in dtRFQDetails.Rows)
                    {
                        if (!string.IsNullOrEmpty(row[6].ToString()))
                        {
                            total_Amount += float.Parse(row[6].ToString());
                        }
                    }
                    float tempAmount = total_Amount;
                    txtTotalAmount.Value = total_Amount.ToString();
                    for (int i = 0; i < lbTaxes.Items.Count; i++)
                    {

                        if (lbTaxes.Items[i].Selected == true)
                        {
                            float per = float.Parse(dtTaxTable.Rows[i]["TaxRatePer"].ToString());
                            total_Amount = (total_Amount + ((tempAmount * per) / 100.0f));
                        }
                    }
                    txtTotalTax.Value = (total_Amount - tempAmount).ToString();
                }
            }
            float discount = 0;
            if (float.TryParse(txtDiscount.Text, out discount))
            {
                txtNetAmount.Value = (total_Amount - discount).ToString();
            }
            else
            {
                txtNetAmount.Value = total_Amount.ToString();

            }
            //txtNetAmount.Value = total_Amount.ToString();
            ((Label)gv.FooterRow.FindControl("lblTotal")).Text = "Total : " + string.Format("{0:f2}", total_Amount);
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CheckSessions();
            bool similar = false;
            foreach (DataRow row in dtRFQDetails.Rows)
            {
                if (row[1].ToString() == ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Text && row[7].ToString() == ((DropDownList)gv.FooterRow.FindControl("ddlExpCOA")).SelectedItem.Text)
                {
                    //string supplier = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Text;
                    //string supplierID = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Value;
                    string itemUnit = ((DropDownList)gv.FooterRow.FindControl("ddlUnit")).SelectedItem.Text;
                    string qty = (Convert.ToInt32(row[4]) + Convert.ToInt32(((TextBox)gv.FooterRow.FindControl("txtQty")).Text)).ToString();
                    string unitPrice = (Convert.ToInt32(row[5]) + float.Parse(((TextBox)gv.FooterRow.FindControl("txtUnitPrice")).Text, NumberStyles.Currency)).ToString();
                    string totalPrice = (Convert.ToInt32(row[6]) + float.Parse(((TextBox)gv.FooterRow.FindControl("txtNetPrice")).Text, NumberStyles.Currency)).ToString();

                    //    string remarks = row[7].ToString() + "," + ((TextBox)gv.FooterRow.FindControl("txtRemarks")).Text;

                    //row.SetField(3, supplier);
                    //row.SetField(4, supplierID);
                    row.SetField(3, itemUnit);
                    row.SetField(4, qty);
                    row.SetField(5, unitPrice);
                    row.SetField(6, totalPrice);
                    //   row.SetField(7, remarks);

                    dtRFQDetails.AcceptChanges();

                    similar = true;
                    break;
                }


            }

            if (!similar)
            {
                DataRow dr = dtRFQDetails.NewRow();
                dr[0] = srNo.ToString();
                dr[1] = ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Text;
                dr[2] = ((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Value;
                //dr[3] = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Text;
                //dr[4] = ((DropDownList)gv.FooterRow.FindControl("ddlSuppliers")).SelectedItem.Value;
                dr[3] = ((DropDownList)gv.FooterRow.FindControl("ddlUnit")).SelectedItem.Text;
                dr[4] = ((TextBox)gv.FooterRow.FindControl("txtQty")).Text;
                dr[5] = ((TextBox)gv.FooterRow.FindControl("txtUnitPrice")).Text;
                dr[6] = ((TextBox)gv.FooterRow.FindControl("txtNetPrice")).Text;
                if (((DropDownList)gv.FooterRow.FindControl("ddlExpCOA")).SelectedItem.Value == "0")
                {
                    objDB.ProductAndServicesID = Convert.ToInt32(((DropDownList)gv.FooterRow.FindControl("ddlItems")).SelectedItem.Value);

                    DataTable dt = new DataTable();
                    dt = objDB.GetProductAndServiceCOAByID(ref errorMsg);

                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            dr[7] = dt.Rows[0]["COAName"];
                            dr[8] = dt.Rows[0]["COA_ID"];
                        }
                    }

                }
                else
                {
                    dr[7] = ((DropDownList)gv.FooterRow.FindControl("ddlExpCOA")).SelectedItem.Text;
                    dr[8] = ((DropDownList)gv.FooterRow.FindControl("ddlExpCOA")).SelectedItem.Value;
                }
                dtRFQDetails.Rows.Add(dr);
                dtRFQDetails.AcceptChanges();

                srNo += 1;
            }
            BindData();


            ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
            calcTotal();

        }

        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void gv_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {

        }

        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            //dtRFQDetails.Rows[index].SetField(3, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditSuppliers")).SelectedItem.Text);
            //dtRFQDetails.Rows[index].SetField(4, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditSuppliers")).SelectedItem.Value);
            dtRFQDetails.Rows[index].SetField(3, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditUnit")).SelectedItem.Text);
            dtRFQDetails.Rows[index].SetField(4, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditQty")).Text);
            dtRFQDetails.Rows[index].SetField(5, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditUnitPrice")).Text);
            dtRFQDetails.Rows[index].SetField(6, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditNetPrice")).Text);

            if (((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditExpCOA")).SelectedItem.Value == "0")
            {
                objDB.ProductAndServicesID = Convert.ToInt32(dtRFQDetails.Rows[index]["ProductAndServicesID"].ToString());
                DataTable dt = new DataTable();
                dt = objDB.GetProductAndServiceCOAByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        dtRFQDetails.Rows[index].SetField(7, dt.Rows[0]["COAName"]);
                        dtRFQDetails.Rows[index].SetField(8, dt.Rows[0]["COA_ID"]);
                    }
                }
            }
            else
            {
                dtRFQDetails.Rows[index].SetField(7, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditExpCOA")).SelectedItem.Text);
                dtRFQDetails.Rows[index].SetField(8, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditExpCOA")).SelectedItem.Value);
            }
            //dtRFQDetails.Rows[index].SetField(7, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditExpCOA")).SelectedItem.Text);
            //dtRFQDetails.Rows[index].SetField(8, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditExpCOA")).SelectedItem.Value);
            dtRFQDetails.AcceptChanges();

            gv.EditIndex = -1;
            BindData();
            calcTotal();
        }

        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindData();
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {

            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
            {
                if (dtRFQDetails.Rows[i][0].ToString() == delSr)
                {
                    dtRFQDetails.Rows[i].Delete();
                    dtRFQDetails.AcceptChanges();
                }
            }
            for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
            {
                dtRFQDetails.Rows[i].SetField(0, i);
                dtRFQDetails.AcceptChanges();
            }
            srNo = dtRFQDetails.Rows.Count;
            BindData();
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                BindData();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Code = txtCode.Value;
                objDB.ClientID = Convert.ToInt32(ddlClient.SelectedItem.Value);
                objDB.SiteID = Convert.ToInt32(ddlLocation.SelectedItem.Value);
                objDB.QuoteID = Convert.ToInt32(ddlQuotation.SelectedItem.Value);
                objDB.Date = txtDate.Value;
                objDB.Description = txtDescription.Value;
                objDB.Title = txtTitle.Value;

                objDB.TotalAmount = float.Parse(txtNetAmount.Value, NumberStyles.Currency);
                objDB.Notes = txtNotes.Value;
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.ChartOfAccountID = int.Parse(objDB.GetClientCOAByClientID());
                objDB.isRecurring = chkIsRecurring.Checked;
                objDB.NoOfInvoices = int.Parse(txtNoOfInvoices.Value);
                objDB.InvoiceDuration = int.Parse(txtInvoiceDuration.Value);
                objDB.DueDuration = int.Parse(txtDueInvoice.Value);
                objDB.isAuto = chkIsAuto.Checked;
                objDB.Discount = float.Parse(txtDiscount.Text == "" ? "0" : txtDiscount.Text, NumberStyles.Currency);

                if (HttpContext.Current.Items["InvoiceID"] != null)
                {
                    objDB.InvoiceID = InvoiceID;
                    objDB.Status = "Active"; //this would be changed
                    res = objDB.UpdateInvoice();
                    if (res == "Invoice Updated Successfully" && POStatus == "Approved")
                    {
                        objDB.TableName = "Invoices";
                        objDB.TableID = InvoiceID;
                        objDB.Debit = float.Parse(txtNetAmount.Value, NumberStyles.Currency);
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.TransactionDetail = ("Payable to " + ddlClient.SelectedItem.Text + " against Invoice # " + txtCode.Value);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.ModifiedBy = Session["UserName"].ToString();
                        objDB.ClientID = Convert.ToInt32(ddlClient.SelectedValue);
                        objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetClientCOAByClientID());

                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                        objDB.Type = "JV";
                        objDB.Against = "INV";
                        objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
                        objDB.Instrument = txtCode.Value;


                        DataTable dt = new DataTable();
                        dt = objDB.AddDirectPayments(ref errorMsg);
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                objDB.DeleteTransactionHistoryByTableID();
                                objDB.SerialNo = dt.Rows[0]["SerialNo"].ToString();
                                objDB.VoucherNo = dt.Rows[0]["VoucherNo"].ToString();
                                objDB.DirectPaymentID = Convert.ToInt32(dt.Rows[0]["DirectPaymentID"].ToString());
                                objDB.AddTransactionHistoryDebit();

                                objDB.ParentID = InvoiceID;
                                objDB.TableName = "InvoiceDiscount";
                                objDB.DeleteTransactionHistoryByParentID();

                                objDB.TableName = "InvoiceDiscount";
                                objDB.TableID = 0;
                                objDB.ParentID = InvoiceID;
                                objDB.Credit = objDB.Discount;
                                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                                objDB.CreatedBy = Session["UserName"].ToString();
                                objDB.Title = "Discount";
                                objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetCOAQuickSetupBybYtITLE());
                                objDB.TransactionDetail = ("Discount Given to " + ddlClient.SelectedItem.Text + " against " + txtCode.Value);
                                objDB.AddTransactionHistoryCredit();

                            }
                            else
                            {
                                objDB.SerialNo = "0";
                            }
                        }

                    }
                    //getRFQAttachmentsByRFQID(RFQID);
                    //showAttachments = true;

                    foreach (ListItem li in lbTaxes.Items)
                    {
                        objDB.TaxRateID = Convert.ToInt32(li.Value);
                        if (li.Selected == true)
                        {
                            objDB.AddInvoiceTax();
                        }
                        else
                        {
                            objDB.DelInvoiceTaxes();
                        }
                    }
                }
                else
                {
                    string returnedID = objDB.AddInvoice();
                    InvoiceID = Convert.ToInt32(returnedID);

                    res = "New Invoice Added Successfully";

                    foreach (ListItem li in lbTaxes.Items)
                    {
                        objDB.InvoiceID = InvoiceID;
                        objDB.TaxRateID = Convert.ToInt32(li.Value);
                        if (li.Selected == true)
                        {
                            objDB.AddInvoiceTax();
                        }
                    }



                    clearFields();


                }

                if (itemAttachments.HasFile || itemAttachments.HasFiles)
                {
                    foreach (HttpPostedFile file in itemAttachments.PostedFiles)
                    {
                        Random rand = new Random((int)DateTime.Now.Ticks);
                        int randnum = 0;
                        randnum = rand.Next(1, 100000);

                        string fn = "";
                        string exten = "";

                        string destDir = Server.MapPath("~/assets/files/Invoices/");
                        randnum = rand.Next(1, 100000);
                        fn = txtCode.Value + "_" + randnum;

                        if (!Directory.Exists(destDir))
                        {
                            Directory.CreateDirectory(destDir);
                        }

                        string fname = Path.GetFileName(file.FileName);
                        exten = Path.GetExtension(file.FileName);
                        file.SaveAs(destDir + fn + exten);

                        objDB.InvoiceID = Convert.ToInt32(InvoiceID);
                        objDB.AttachmentPath = "http://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/Invoices/" + fn + exten;
                        objDB.AddInvoiceAttachments();
                    }
                }

                objDB.InvoiceID = InvoiceID;
                objDB.DeleteInvoiceItemsByInvoiceID();
                objDB.ParentID = InvoiceID;
                objDB.TableName = "InvoiceItems";
                objDB.DeleteTransactionHistoryByParentID();
                dtRFQDetails = (DataTable)ViewState["dtRFQDetails"] as DataTable;
                if (dtRFQDetails != null)
                {
                    if (dtRFQDetails.Rows.Count > 0)
                    {
                        if (dtRFQDetails.Rows[0][0].ToString() == "")
                        {
                            dtRFQDetails.Rows[0].Delete();
                            dtRFQDetails.AcceptChanges();
                        }

                        for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
                        {
                            if (res == "Invoice Updated Successfully" && POStatus == "Approved" && objDB.SerialNo != "0")
                            {
                                objDB.TableName = "InvoiceItems";
                                objDB.TableID = 0;
                                objDB.ParentID = InvoiceID;
                                objDB.Credit = float.Parse(dtRFQDetails.Rows[i]["NetPrice"].ToString(), NumberStyles.Currency);
                                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                                objDB.CreatedBy = Session["UserName"].ToString();
                                //objDB.ProductAndServicesID = Convert.ToInt32(dtRFQDetails.Rows[i]["ProductAndServicesID"].ToString());
                                //objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetProductAndServicesCOAByProductAndServicesID());
                                objDB.ChartOfAccountID = Convert.ToInt32(dtRFQDetails.Rows[i]["COA_ID"].ToString());
                                objDB.TransactionDetail = ("Receivable from " + ddlClient.SelectedItem.Text + " against INV # " + txtCode.Value);
                                objDB.AddTransactionHistoryCredit();
                            }
                            objDB.InvoiceID = Convert.ToInt32(InvoiceID);
                            objDB.ItemID = Convert.ToInt32(dtRFQDetails.Rows[i]["ProductAndServicesID"].ToString());
                            objDB.ItemUnit = dtRFQDetails.Rows[i]["ItemUnit"].ToString();
                            objDB.Qty = Convert.ToInt32(dtRFQDetails.Rows[i]["Quantity"].ToString());
                            objDB.UnitPrice = float.Parse(dtRFQDetails.Rows[i]["UnitPrice"].ToString(), NumberStyles.Currency);
                            objDB.NetPrice = float.Parse(dtRFQDetails.Rows[i]["NetPrice"].ToString(), NumberStyles.Currency);
                            objDB.Remarks = dtRFQDetails.Rows[i]["Remarks"].ToString();
                            objDB.ChartOfAccountID = Convert.ToInt32(dtRFQDetails.Rows[i]["COA_ID"].ToString());
                            objDB.ModifiedBy = Session["UserName"].ToString();

                            objDB.AddInvoiceItems();
                        }
                    }
                }


                if (!showFirstRow)
                {
                    ViewState["dtRFQDetails"] = null;
                    srNo = 1;
                }

                BindData();

                objDB.ParentID = InvoiceID;
                objDB.TableName = "InvoiceTaxes";
                objDB.DeleteTransactionHistoryByParentID();
                for (int i = 0; i < lbTaxes.Items.Count; i++)
                {
                    if (lbTaxes.Items[i].Selected == true && res == "Invoice Updated Successfully" && POStatus == "Approved" && objDB.SerialNo != "0")
                    {
                        objDB.TableName = "InvoiceTaxes";
                        objDB.TableID = 0;
                        objDB.ParentID = InvoiceID;
                        float per = float.Parse(dtTaxTable.Rows[i]["TaxRatePer"].ToString());
                        objDB.Credit = ((float.Parse(txtTotalAmount.Value, NumberStyles.Currency) * per) / 100.0f);
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.ChartOfAccountID = Convert.ToInt32(dtTaxTable.Rows[i]["COA_ID"].ToString());
                        objDB.TransactionDetail = (dtTaxTable.Rows[i]["TaxRateName"].ToString() + " @ " + dtTaxTable.Rows[i]["TaxRatePer"].ToString() + "% on " + txtTotalAmount.Value + "/- from " + ddlClient.SelectedItem.Text + " against INV # " + txtCode.Value);
                        objDB.AddTransactionHistoryCredit();
                    }
                }

                if (res == "Invoice Updated Successfully" && POStatus == "Approved" && objDB.SerialNo != "0")
                {
                    objDB.UpdateAllDirectPaymentAmounts();
                    getAllVoucher();
                    PrintVoucher(objDB.DirectPaymentID);
                }

                if (res == "New Invoice Added Successfully" || res == "Invoice Updated Successfully")
                {
                    if (res == "New Invoice Added Successfully") { Common.addlog("Add", "Finance", " Invoice \"" + objDB.Code + "\" Added", "Invoices"); }
                    if (res == "Invoice Updated Successfully") { Common.addlog("Update", "Finance", "Invoice\"" + objDB.Code + "\" Updated", "Invoices", objDB.InvoiceID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        private void clearFields()
        {
            txtNotes.Value = "";
            //dtRFQDetails = createTable();
            ddlClient.SelectedIndex = 0;
            ddlCOA.SelectedIndex = 0;
            ddlLocation.SelectedIndex = 0;
            ddlQuotation.SelectedIndex = 0;
            DataTable dtCode = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            dtCode = objDB.GetInvoiceCode(ref errorMsg);
            if (dtCode != null)
            {
                if (dtCode.Rows.Count > 0)
                {
                    txtCode.Value = dtCode.Rows[0][0].ToString();
                }
            }
            txtTotalAmount.Value = "0";
            txtNetAmount.Value = "0";
            txtTotalTax.Value = "0";
            txtNotes.Value = "";
            txtDescription.Value = "";
            txtTitle.Value = "";

            foreach (ListItem li in lbTaxes.Items)
            {
                li.Selected = false;
            }

        }

        protected void ddlRFQ_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            if (ddlLocation.SelectedIndex != 0)
            {
                objDB.RFQID = Convert.ToInt32(ddlLocation.SelectedItem.Value);
                //       txtCode.Value = objDB.GenereatePOCodeByRFQID();
                getRFQItemsByRFQID(Convert.ToInt32(ddlLocation.SelectedItem.Value));
                calcTotal();
            }
            else
            {
                // txtCode.Value = "";
                dtRFQDetails = createTable();
            }
            BindData();
        }

        protected void txtNetPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            TextBox txtQty = gv.FooterRow.FindControl("txtQty") as TextBox;
            string qty = txtQty.Text;
            TextBox txtUnitPrice = gv.FooterRow.FindControl("txtUnitPrice") as TextBox;
            string unitPrice = txtUnitPrice.Text;
            //DropDownList txtRemarks = gv.FooterRow.FindControl("ddlExpCOA") as DropDownList;

            if (!string.IsNullOrEmpty(qty) && !string.IsNullOrEmpty(unitPrice))
            {
                //unitPrice = unitPrice.Replace(",", "");
                //qty = qty.Replace(",", "");
                TextBox txtNetPrice = gv.FooterRow.FindControl("txtNetPrice") as TextBox;
                string netPrice = (Convert.ToInt32(qty) * double.Parse(unitPrice, NumberStyles.Currency)).ToString();

                txtNetPrice.Text = netPrice;
            }
            //TextBox txtSender = (TextBox)sender as TextBox;
            //if (txtSender.ID == "txtQty")
            //    txtUnitPrice.Focus();
            //else if (txtSender.ID == "txtUnitPrice")
            //    txtRemarks.Focus();
        }

        protected void txtEditUnitPrice_TextChanged(object sender, EventArgs e)
        {
            CheckSessions();
            int rowNum = Convert.ToInt32(hdnRowNo.Value);
            TextBox txtQty = gv.Rows[rowNum].FindControl("txtEditQty") as TextBox;
            string qty = txtQty.Text;
            TextBox txtUnitPrice = gv.Rows[rowNum].FindControl("txtEditUnitPrice") as TextBox;
            string unitPrice = txtUnitPrice.Text;

            if (!string.IsNullOrEmpty(qty) && !string.IsNullOrEmpty(unitPrice))
            {
                TextBox txtNetPrice = gv.Rows[rowNum].FindControl("txtEditNetPrice") as TextBox;
                string netPrice = (Convert.ToInt32(qty) * float.Parse(unitPrice, NumberStyles.Currency)).ToString();

                txtNetPrice.Text = netPrice;
            }

            //TextBox txtSender = (TextBox)sender as TextBox;
            //txtSender.Focus();
        }

        protected void gv_PreRender(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
                gv.UseAccessibleHeader = true;
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void getRFQItemsByRFQID(int RFQID)
        {
            DataTable dt = new DataTable();
            objDB.RFQID = RFQID;
            dt = objDB.GetRFQItemsByRFQID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dtRFQDetails.Rows.Add(new object[] {
                            row[0],
                            row[1],
                            row[2],
                            row[3],
                            row[4],
                            "0",
                            "0",
                            row[5]
                        });

                        dtRFQDetails.AcceptChanges();
                    }
                    BindData();

                }
            }
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "Invoices", "InvoiceID", HttpContext.Current.Items["InvoiceID"].ToString(), Session["UserName"].ToString());
            Common.addlogNew(res, "Finance", "Invoices of ID\"" + HttpContext.Current.Items["InvoiceID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/invoice", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/invoice/edit-invoice-" + HttpContext.Current.Items["InvoiceID"].ToString(), "Invoice \"" + txtTitle.Value + "\"", "Invoices", "Invoice", Convert.ToInt32(HttpContext.Current.Items["InvoiceID"].ToString()));

            //Common.addlog(res, "Finance", "Invoices of ID \"" + HttpContext.Current.Items["InvoiceID"].ToString() + "\" Status Changed", "Invoices", InvoiceID /* ID From Page Top  */);/* Button1_ServerClick  */
            if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
            {
                objDB.TableName = "Invoices";
                objDB.TableID = InvoiceID;
                objDB.Debit = float.Parse(txtNetAmount.Value, NumberStyles.Currency);
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.ClientID = Convert.ToInt32(ddlClient.SelectedValue);
                objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetClientCOAByClientID());
                objDB.TransactionDetail = ("Receivable From " + ddlClient.SelectedItem.Text + " against " + txtCode.Value);

                objDB.Type = "JV";
                objDB.Against = "INV";
                objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
                objDB.Instrument = txtCode.Value;
                objDB.GeneratedDate = DateTime.Now.ToString("dd-MMM-yyyy");
                DataTable dt = new DataTable();
                dt = objDB.AddDirectPayments(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        objDB.SerialNo = dt.Rows[0]["SerialNo"].ToString();
                        objDB.VoucherNo = dt.Rows[0]["VoucherNo"].ToString();
                        objDB.DirectPaymentID = Convert.ToInt32(dt.Rows[0]["DirectPaymentID"].ToString());
                        objDB.AddTransactionHistoryDebit();


                        objDB.TableName = "InvoiceDiscount";
                        objDB.ParentID = InvoiceID;
                        objDB.DeleteTransactionHistoryByParentID();

                        objDB.TableName = "InvoiceDiscount";
                        objDB.TableID = InvoiceID;
                        objDB.Credit = float.Parse(txtDiscount.Text == "" ? "0" : txtDiscount.Text, NumberStyles.Currency);
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.Title = "Discount";
                        objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetCOAQuickSetupBybYtITLE());
                        objDB.TransactionDetail = ("Discount given to " + ddlClient.SelectedItem.Text + " against INV " + txtCode.Value);

                        objDB.AddTransactionHistoryCredit();

                        objDB.TableName = "InvoiceItems";
                        objDB.ParentID = InvoiceID;
                        objDB.DeleteTransactionHistoryByParentID();

                        dtRFQDetails = (DataTable)ViewState["dtRFQDetails"] as DataTable;
                        if (dtRFQDetails != null)
                        {
                            if (dtRFQDetails.Rows.Count > 0)
                            {

                                for (int i = 0; i < dtRFQDetails.Rows.Count; i++)
                                {
                                    objDB.TableName = "InvoiceItems";
                                    objDB.TableID = 0;
                                    objDB.ParentID = InvoiceID;
                                    objDB.Credit = float.Parse(dtRFQDetails.Rows[i]["NetPrice"].ToString(), NumberStyles.Currency);
                                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                                    objDB.CreatedBy = Session["UserName"].ToString();
                                    //objDB.ProductAndServicesID = Convert.ToInt32(dtRFQDetails.Rows[i]["ProductAndServicesID"].ToString());
                                    objDB.ChartOfAccountID = Convert.ToInt32(dtRFQDetails.Rows[i]["COA_ID"].ToString());
                                    objDB.TransactionDetail = ("Receivable From " + ddlClient.SelectedItem.Text + " against " + txtCode.Value);

                                    objDB.AddTransactionHistoryCredit();

                                }

                            }
                        }

                        objDB.ParentID = InvoiceID;
                        objDB.TableName = "InvoiceTaxes";
                        objDB.DeleteTransactionHistoryByParentID();
                        for (int i = 0; i < lbTaxes.Items.Count; i++)
                        {
                            if (lbTaxes.Items[i].Selected == true)
                            {
                                objDB.TableName = "InvoiceTaxes";
                                objDB.TableID = 0;
                                objDB.ParentID = InvoiceID;
                                float per = float.Parse(dtTaxTable.Rows[i]["TaxRatePer"].ToString(), NumberStyles.Currency);
                                objDB.Credit = ((float.Parse(txtTotalAmount.Value, NumberStyles.Currency) * per) / 100.0f);
                                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                                objDB.CreatedBy = Session["UserName"].ToString();
                                objDB.ChartOfAccountID = Convert.ToInt32(dtTaxTable.Rows[i]["COA_ID"].ToString());
                                objDB.TransactionDetail = (dtTaxTable.Rows[i]["TaxRateName"].ToString() + " @ " + dtTaxTable.Rows[i]["TaxRatePer"].ToString() + "% on " + txtTotalAmount.Value + "/- recived from " + ddlClient.SelectedItem.Text + " against " + txtCode.Value);
                                objDB.AddTransactionHistoryCredit();
                            }
                        }

                        objDB.UpdateAllDirectPaymentAmounts();
                        getAllVoucher();
                        PrintVoucher(objDB.DirectPaymentID);


                    }

                }




                //AddVoucher();
                btnGenVoucher.Visible = true;
                //btnPayVoucher.Visible = true;
                btnPayNow.Visible = true;
                txtDatePN.Value = DateTime.Now.ToString("dd-MMM-yyyy");

                sendEmails(InvoiceID);

            }
            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;

            CheckAccess();
        }
        protected void AddVoucher()
        {
            CheckSessions();
            string JVID = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.VoucherNo = txtCode.Value;
            objDB.Against = "Invoice";
            objDB.CreatedBy = Session["UserName"].ToString();
            JVID = objDB.AddFinanceVoucher();
            objDB.InvoiceID = InvoiceID;
            objDB.JVID = Convert.ToInt32(JVID);
            objDB.AddJVIDinInvoice();
        }

        protected void addBCVoucher(int ID)
        {
            try
            {
                //int BCVoucherID = 0;
                //CheckSessions();
                //objDB.VouchrType = ddlPayBy.SelectedValue;
                //objDB.VoucherNo = txtCode.Value;
                //objDB.Against = "Invoice";
                //objDB.TableName = "Invoices";
                //objDB.TableID = ID;
                //objDB.VoucherAmount = Convert.ToDouble(txtAmountPN.Value);
                //objDB.DebitCOA = int.Parse(ddlCOAPN.SelectedValue);
                //objDB.CreditCOA = int.Parse(ddlCOA.SelectedValue);
                //objDB.TransactionDetail = ("Received From " + ddlClient.SelectedItem.Text + " against INV # " + txtCode.Value);
                //objDB.CreatedBy = Session["UserName"].ToString();
                //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                //string vid = objDB.AddBankCashVoucher();
                ////VouchrType, VoucherNo, Against, TableName, TableID, VoucherAmount,CreditCOA,DebitCOA,TransactionDetail, CreatedBy, CompanyID 

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            string type = btn.CommandArgument;
            string res = Common.addAccessLevels(type, "Invoices", "InvoiceID", HttpContext.Current.Items["InvoiceID"].ToString(), Session["UserName"].ToString());
            Common.addlog("Delete", "Finance", "Invoices of ID \"" + HttpContext.Current.Items["InvoiceID"].ToString() + "\" deleted", "Invoices", InvoiceID /* ID From Page Top  */);/* lnkDelete_Click  */

            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            Response.Redirect(btnBack.HRef);

        }

        protected void lbTaxes_SelectedIndexChanged(object sender, EventArgs e)
        {
            calcTotal();
        }

        protected void ddlQuotation_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        private void getQuotationDetails(int QuoteID)
        {
            DataTable dt = new DataTable();
            objDB.QuoteID = QuoteID;
            dt = objDB.GetQuotationByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlClient.SelectedValue = dt.Rows[0]["ClientID"].ToString();
                    ddlLocation.SelectedValue = dt.Rows[0]["SiteID"].ToString();
                    ddlCOA.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                    //txtCode.Value = dt.Rows[0]["Code"].ToString();
                    txtDate.Value = dt.Rows[0]["Date"].ToString();
                    txtNetAmount.Value = dt.Rows[0]["Amount"].ToString();
                    txtNotes.Value = dt.Rows[0]["Notes"].ToString();

                    getQuotationItemsByQuoteID(QuoteID);
                    getQuotationAttachmentsByQuoteID(QuoteID);
                    GetQuotationTaxesByQuoteID(QuoteID);
                }
            }
        }

        private void GetQuotationTaxesByQuoteID(int QuoteID)
        {
            if (CheckSessions())
            {
                DataTable dt = new DataTable();
                objDB.QuoteID = QuoteID;
                dt = objDB.GetQuotationTaxesByQuoteID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            foreach (ListItem li in lbTaxes.Items)
                            {
                                if (dt.Rows[i]["TaxRateID"].ToString() == li.Value)
                                    li.Selected = true;
                            }
                        }
                    }
                }
            }
        }

        private void getQuotationAttachmentsByQuoteID(int QuoteID)
        {
            DataTable dt = new DataTable();
            objDB.QuoteID = QuoteID;
            dt = objDB.GetQuotationAttachmentsByQuoteID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    divAttachments.Visible = true;
                    gvAttachments.DataSource = dt;
                    gvAttachments.DataBind();
                }
            }
        }

        private void getQuotationItemsByQuoteID(int QuoteID)
        {
            DataTable dt = new DataTable();
            objDB.QuoteID = QuoteID;
            dt = objDB.GetQuotationItemsByQuoteIDForInvoice(ref errorMsg);

            dtRFQDetails = null;
            dtRFQDetails = new DataTable();
            dtRFQDetails = createTable();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtRFQDetails.Rows[0][0].ToString() == "")
                    {
                        dtRFQDetails.Rows[0].Delete();
                        dtRFQDetails.AcceptChanges();
                        showFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtRFQDetails.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["ItemCode"],
                            dt.Rows[i]["ProductAndServicesID"],
                            dt.Rows[i]["ItemUnit"],
                             dt.Rows[i]["Quantity"],
                            dt.Rows[i]["UnitPrice"],
                            dt.Rows[i]["NetPrice"],
                            dt.Rows[i]["COAName"],
                            dt.Rows[i]["COA_ID"]
                        });
                    }
                    srNo = Convert.ToInt32(dtRFQDetails.Rows[dtRFQDetails.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            BindData();
        }

        protected void chkIsRecurring_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsRecurring.Checked)
            {
                divRecurrig.Visible = true;
            }
            else
            {
                divRecurrig.Visible = false;

            }
        }



        protected void btnPayNow_ServerClick1(object sender, EventArgs e)
        {
            //if (!divPay.Visible)
            //{
            //    divPay.Visible = true;
            //    txtDatePN.Value = DateTime.Now.ToString("dd-MMM-yyyy");
            //    BindCOAPN();
            //}
            //else
            //{
            //    divPay.Visible = false;
            //    ddlCOAPN.SelectedIndex = 0;
            //    txtAmountPN.Value = "0";
            //}
        }

        protected void btnPay_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.PayDate = txtDatePN.Value;
                objDB.GeneratedDate = txtDatePN.Value;

                objDB.PayAmount = float.Parse(txtAmountPN.Value);
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.AccountID = int.Parse(ddlAccounts.SelectedValue);

                if (ddlPayBy.SelectedValue == "Bank")
                {
                    string id = objDB.GetAccountsCOAIDByID();
                    objDB.PayCOAID = Convert.ToInt32(id == "" ? "0" : id);
                }
                else
                {
                    objDB.PayCOAID = int.Parse(ddlCOAPN.SelectedValue);
                }
                objDB.InvoiceID = InvoiceID;
                objDB.PayType = ddlPayBy.SelectedValue;
                objDB.WOT = float.Parse(txtWOT.Value == "" ? "0" : txtWOT.Value);
                objDB.STWH = float.Parse(txtSTWH.Value == "" ? "0" : txtSTWH.Value);

                if ((objDB.PayAmount + objDB.WOT + objDB.STWH) > double.Parse(txtRemainingAmount.Value))
                {
                    res = "Amount must be less than or equal to remaining amont.";
                }
                else if (objDB.PayType == "Bank" && ddlAccounts.SelectedValue == "0")
                {
                    res = "Please slect bank account.";
                }
                else
                {
                    res = objDB.AddInvoicePay();
                }


                if (res == "Paid")
                {
                    res = "Received";
                    objDB.TableName = "InvoicePay";
                    objDB.TableID = InvoiceID;
                    objDB.ParentID = InvoiceID;
                    objDB.Credit = float.Parse(txtAmountPN.Value) + objDB.WOT + objDB.STWH;
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.ClientID = Convert.ToInt32(ddlClient.SelectedValue);
                    objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetClientCOAByClientID());


                    if (txtTransactionDetail.Value == "")
                    {
                        objDB.TransactionDetail = ("Received From " + ddlClient.SelectedItem.Text + " against " + txtCode.Value);

                    }
                    else
                    {
                        objDB.TransactionDetail = txtTransactionDetail.Value;
                    }

                    if (ddlPayBy.SelectedValue == "Cash")
                    {
                        objDB.Type = "CRV";
                    }
                    else
                    {
                        objDB.Type = "BRV";
                    }
                    objDB.Against = "INV";
                    objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);

                    if (txtInstrument.Value == "")
                    {
                        objDB.Instrument = txtCode.Value;
                    }
                    else
                    {
                        objDB.Instrument = txtInstrument.Value;
                    }

                    DataTable dt = new DataTable();
                    dt = objDB.AddDirectPayments(ref errorMsg);
                    objDB.TableID = 0;
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            objDB.SerialNo = dt.Rows[0]["SerialNo"].ToString();
                            objDB.VoucherNo = dt.Rows[0]["VoucherNo"].ToString();
                            objDB.DirectPaymentID = Convert.ToInt32(dt.Rows[0]["DirectPaymentID"].ToString());
                            objDB.TableID = 0;
                            objDB.AddTransactionHistoryCredit();
                        }
                        else
                        {
                            objDB.SerialNo = "0";
                        }
                    }

                    objDB.TableName = "InvoicePay";
                    objDB.TableID = 0;
                    objDB.ParentID = InvoiceID;
                    objDB.Debit = float.Parse(txtAmountPN.Value);
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.CreatedBy = Session["UserName"].ToString();
                    if (ddlPayBy.SelectedValue == "Bank")
                    {
                        string id = objDB.GetAccountsCOAIDByID();
                        objDB.ChartOfAccountID = Convert.ToInt32(id == "" ? "0" : id);
                    }
                    else
                    {
                        objDB.ChartOfAccountID = int.Parse(ddlCOAPN.SelectedValue);
                    }
                    //objDB.TransactionDetail = ("Received From " + ddlClient.SelectedItem.Text + " against INV # " + txtCode.Value);
                    objDB.AddTransactionHistoryDebit();

                    if (objDB.WOT != 0)
                    {
                        objDB.TableName = "InvoicePay";
                        objDB.TableID = 0;
                        objDB.ParentID = InvoiceID;
                        objDB.Debit = objDB.WOT;
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.Title = "WOT";
                        objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetCOAQuickSetupBybYtITLE());
                        //objDB.TransactionDetail = ("Received From " + ddlClient.SelectedItem.Text + " against INV # " + txtCode.Value);
                        objDB.AddTransactionHistoryDebit();
                    }

                    if (objDB.STWH != 0)
                    {
                        objDB.TableName = "InvoicePay";
                        objDB.TableID = 0;
                        objDB.ParentID = InvoiceID;
                        objDB.Debit = objDB.STWH;
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.Title = "STWH";
                        objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetCOAQuickSetupBybYtITLE());
                        //objDB.TransactionDetail = ("Received From " + ddlClient.SelectedItem.Text + " against INV # " + txtCode.Value);
                        objDB.AddTransactionHistoryDebit();

                    }
                    objDB.UpdateAllDirectPaymentAmounts();
                    getAllVoucher();
                    PrintVoucher(objDB.DirectPaymentID);
                    Common.addlog("Update", "Finance", "Invoice of ID \"" + (objDB.InvoiceID).ToString() + "\" Paid", "Expenses", objDB.InvoiceID);
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                    //addBCVoucher(InvoiceID);
                    getInvoiceDetails(InvoiceID);
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnGenVoucher_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/invoice-journal-voucher/generate-" + InvoiceID);
            }
            catch (Exception ex)
            {

            }
        }

        protected void ddlQuotation_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (ddlQuotation.SelectedIndex != 0)
            {
                getQuotationDetails(Convert.ToInt32(ddlQuotation.SelectedItem.Value));
                showFirstRow = true;
                BindData();
            }
            else
            {
                dtRFQDetails = null;
                ViewState["srNO"] = null;
                ViewState["showFirstRow"] = null;
                dtRFQDetails = createTable();
                BindData();

                clearFields();
            }
        }

        protected void btnPayVoucher_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Session["TableName"] = "InvoicePay";
                Session["BackLink"] = ("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/invoice/edit-invoice-" + InvoiceID);

                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/payment-voucher/generate-" + InvoiceID);
            }
            catch (Exception ex)
            {

            }
        }


        private void sendEmails(int InvoiceID)
        {
            //DataTable dt = new DataTable();
            //string Header = "";
            //string content = "";
            //string Footer = "";
            //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            //objDB.DocType = "Invoice";
            //dt = objDB.GetDocumentDesign(ref errorMsg);
            //if (dt == null)
            //{
            //    objDB.CompanyID = 2;
            //    objDB.DocType = "Invoice";
            //    dt = objDB.GetDocumentDesign(ref errorMsg);
            //}


            //if (dt != null)
            //{
            //    if (dt.Rows.Count > 0)
            //    {
            //        Header = dt.Rows[0]["DocHeader"].ToString();
            //        content = dt.Rows[0]["DocContent"].ToString();
            //        Footer = dt.Rows[0]["DocFooter"].ToString();
            //    }
            //}

            //objDB.InvoiceID = InvoiceID;
            //double totAmount = 0;
            //DataTable INdt = objDB.GetInvoiceByIDForReport(ref errorMsg);
            //if (INdt != null)
            //{
            //    content = content.Replace("##TITLE##", INdt.Rows[0]["Title"].ToString());
            //    content = content.Replace("##CLIENT_NAME##", INdt.Rows[0]["ClientName"].ToString());
            //    content = content.Replace("##CLIENT_ADDRESSLINE1##", INdt.Rows[0]["ClientAddress1"].ToString());
            //    content = content.Replace("##CLIENT_ADDRESSLINE2##", INdt.Rows[0]["ClientAddress2"].ToString());
            //    content = content.Replace("##CLIENT_CITY##", INdt.Rows[0]["ClientCity"].ToString());
            //    content = content.Replace("##CLIENT_COUNTRY##", INdt.Rows[0]["ClientCountry"].ToString());
            //    content = content.Replace("##SITE_NAME##", INdt.Rows[0]["SiteName"].ToString());
            //    content = content.Replace("##INVOICE_NO##", INdt.Rows[0]["Code"].ToString());
            //    content = content.Replace("##INVOICE_DATE##", INdt.Rows[0]["ApprovedDate"].ToString());
            //    content = content.Replace("##QUOTE_NO##", INdt.Rows[0]["QuoteNo"].ToString());
            //    content = content.Replace("##INVOICE_DESCRIPTION##", INdt.Rows[0]["Description"].ToString());
            //    content = content.Replace("##NTN_N0##", INdt.Rows[0]["NTN"].ToString());
            //    content = content.Replace("##NTN_NO##", INdt.Rows[0]["ClientNTN"].ToString());
            //    string table = "";
            //    table += "<table class='item-table' style = 'width:100%;'>";
            //    table += "<tr><th style='border: 1px solid black;  padding:5px;'>Sr #</th><th style='border: 1px solid black;  padding:5px;'>Item Code</th><th style='border: 1px solid black;  padding:5px;'>UOM</th><th style='border: 1px solid black;  padding:5px;'>Quantity</th><th style='border: 1px solid black;  padding:5px;'>Unit Price</th><th style='border: 1px solid black;  padding:5px;'>Net Price</th></tr>";
            //    if (INdt.Rows.Count > 0)
            //    {
            //        DataTable dtINItems = new DataTable();

            //        dtINItems = objDB.GetInvoiceItemsByInvoiceID(ref errorMsg);
            //        if (dtINItems != null)
            //        {
            //            if (dtINItems.Rows.Count > 0)
            //            {

            //                for (int i = 0; i < dtINItems.Rows.Count; i++)
            //                {

            //                    table += "<tr><td>" + (i + 1) + "</td><td>" + dtINItems.Rows[i]["ItemCode"] + "</td><td>" + dtINItems.Rows[i]["ItemUnit"] + "</td><td>" + dtINItems.Rows[i]["Quantity"] + "</td><td>" + dtINItems.Rows[i]["UnitPrice"] + "</td><td>" + dtINItems.Rows[i]["NetPrice"] + "</td></tr>";
            //                    totAmount += Convert.ToDouble(dtINItems.Rows[i]["NetPrice"].ToString());
            //                }

            //            }

            //        }
            //    }




            //    DataTable dtPOItems = new DataTable();
            //    objDB.TableID = InvoiceID;
            //    objDB.Table1 = "InvoiceTaxes";
            //    objDB.Table2 = "Table3";
            //    objDB.Table3 = "Table3";
            //    dtPOItems = objDB.GetCreditForPayVoucher(ref errorMsg);
            //    table += "<tr><td colspan = '5' style = 'text-align:right;border: 1px solid black;  padding:5px;'>" + "Total Amount" + "</td><td style = 'border: 1px solid black;  padding:5px;'>" + totAmount + "</td></tr>";

            //    if (dtPOItems != null)
            //    {
            //        if (dtPOItems.Rows.Count > 0)
            //        {

            //            for (int i = 0; i < dtPOItems.Rows.Count; i++)
            //            {

            //                table += "<tr><td colspan = '5' style = 'text-align:right; border: 1px solid black;  padding:5px;'>" + dtPOItems.Rows[i]["TransactionDetail"] + "</td><td style = 'border: 1px solid black;  padding:5px;'>" + dtPOItems.Rows[i]["Credit"] + "</td></tr>";
            //            }

            //        }
            //        else
            //        {

            //            table += "<tr><td colspan = '5' style = 'text-align:right; border: 1px solid black;  padding:5px;'>" + "Tax" + "</td><td style = 'border: 1px solid black;  padding:5px;'>" + "0" + "</td></tr>";

            //        }
            //    }

            //    table += "<tr><td colspan = '5' style = 'text-align:right; border: 1px solid black;  padding:5px;'>" + "Net Total" + "</td><td style = 'border: 1px solid black;  padding:5px;'>" + INdt.Rows[0]["Amount"] + "</td></tr>";


            //    table += "</table>";
            //    content = content.Replace("##ITEM_LIST##", table);

            //}

            //string file = "";
            //string html = "";
            //file = System.Web.HttpContext.Current.Server.MapPath("/assets/emails/Invoice.html");
            //StreamReader sr = new StreamReader(file);
            //FileInfo fi = new FileInfo(file);

            //if (System.IO.File.Exists(file))
            //{
            //    html += sr.ReadToEnd();
            //    sr.Close();
            //}

            //html = html.Replace("##CONTENT##", content);

            //DataTable dtAttachments = new DataTable();
            //objDB.InvoiceID = InvoiceID;
            //dtAttachments = objDB.GetInvoiceAttachmentsByInvoiceID(ref errorMsg);
            //if (dtAttachments != null)
            //{
            //    if (dtAttachments.Rows.Count > 0)
            //    {
            //        html = html.Replace("##ATTACHMENTSTITLE##", "Please Find Attachments");
            //        string attachments = "";
            //        for (int j = 0; j < dtAttachments.Rows.Count; j++)
            //        {
            //            attachments += "<a href=" + dtAttachments.Rows[j]["AttachmentPath"] + ">File - " + (j + 1) + "</a><br />";
            //        }
            //        html = html.Replace("##ATTACHMENTS##", attachments);
            //    }
            //    else
            //    {
            //        html = html.Replace("##ATTACHMENTSTITLE##", "");
            //        html = html.Replace("##ATTACHMENTS##", "");
            //    }
            //}
            //else
            //{
            //    html = html.Replace("##ATTACHMENTSTITLE##", "");
            //    html = html.Replace("##ATTACHMENTS##", "");
            //}


            //MailMessage mail = new MailMessage();
            //mail.Subject = "Invovice By " + Session["CompanyName"].ToString();
            //mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), Session["CompanyName"].ToString());

            //DataTable dtClient = new DataTable();
            //objDB.ClientID = Convert.ToInt32(ddlClient.SelectedValue);
            //dtClient = objDB.GetClientByID(ref errorMsg);
            //if (dtClient != null)
            //{
            //    if (dtClient.Rows.Count > 0)
            //    {
            //        mail.To.Add(dtClient.Rows[0]["CompanyEmail"].ToString());
            //        mail.CC.Add(dtClient.Rows[0]["Email"].ToString());
            //    }
            //}

            //mail.Body = html;
            //mail.IsBodyHtml = true;

            //SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["NoReplySMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NoReplyPort"].ToString()));
            //smtp.EnableSsl = true;
            //NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["NoReplyPassword"].ToString());
            //smtp.Credentials = netCre;
            //ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            //smtp.Send(mail);

        }

        protected void txtDiscount_TextChanged(object sender, EventArgs e)
        {
            calcTotal();
        }


        protected void getAllVoucher()
        {
            objDB.Against = "INV";
            objDB.TableID = InvoiceID;
            DataTable dtVoc = objDB.GetDirectPaymentsByAgainstAndID(ref errorMsg);
            if (dtVoc != null && dtVoc.Rows.Count > 0)
            {
                gvVoucherModel.DataSource = dtVoc;
                gvVoucherModel.DataBind();
            }

        }

        protected void PrintVoucher(int ID)
        {
            hdnTransactionID.Value = ID.ToString();
            TraID = ID;
        }

        protected void GenVoucherPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (TraID != 0)
                {
                    Common.PrintVoucher(TraID);
                }
            }

            catch (Exception)
            {
            }
            finally
            {
                TraID = 0;
            }


        }



    }
}
