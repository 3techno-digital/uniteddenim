﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.Finance.manage
{
    public partial class DirectPayment : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        int DirectPaymentID
        {
            get
            {
                if (ViewState["DirectPaymentID"] != null)
                {
                    return (int)ViewState["DirectPaymentID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DirectPaymentID"] = value;
            }
        }
        int POID
        {
            get
            {
                if (ViewState["POID"] != null)
                {
                    return (int)ViewState["POID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["POID"] = value;
            }
        }

        double TotalDebit
        {
            get
            {
                if (ViewState["TotalDebit"] != null)
                {
                    return (double)ViewState["TotalDebit"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["TotalDebit"] = value;
            }
        }

        double TotalCredit
        {
            get
            {
                if (ViewState["TotalCredit"] != null)
                {
                    return (double)ViewState["TotalCredit"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["TotalCredit"] = value;
            }
        }

        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        string DosSatus
        {
            get
            {
                if (ViewState["DosSatus"] != null)
                {
                    return ViewState["DosSatus"].ToString();
                }
                else
                {
                    return "Saved as Draft";
                }
            }

            set
            {
                ViewState["DosSatus"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                txtDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");

                BindDropDowns();
                ViewState["srNo"] = null;
                ViewState["dtPaymentDetails"] = null;
                ViewState["showFirstRow"] = null;
                dtPaymentDetails = createTable();
                btnGenVoucher.Visible = false;
                btnApproveByFinance.Visible = false;
                btnReviewByFinance.Visible = false;
                btnRevApproveByFinance.Visible = false;
                btnReject.Visible = false;
                lnkDelete.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;
                divAlertMsg.Visible = false;
                //btnGenVoucher.Visible = false;
                //btnPayVoucher.Visible = false;
                GRNDIV.Visible = false;
                PODIV.Visible = false;
                INVDIV.Visible = false;
                ddlGRN.SelectedValue = "0";
                ddlPO.SelectedValue = "0";
                ddlInvoice.SelectedValue = "0";

                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/direct-payment";

                showFirstRow = false;

                ddlType_SelectedIndexChanged(null, null);

                if (HttpContext.Current.Items["DirectPaymentID"] != null)
                {

                    DirectPaymentID = Convert.ToInt32(HttpContext.Current.Items["DirectPaymentID"].ToString());
                    btnGenVoucher.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reports/grn-journal-voucher/generate-" + DirectPaymentID;

                    getDirectPaymentDetails(DirectPaymentID);
                    //showFirstRow = true;
                    CheckAccess();
                }

                BindData();

            }
        }

        private void BindDropDowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlLocation.DataSource = objDB.GetAllApprovedLocations(ref errorMsg);
            ddlLocation.DataTextField = "SiteName";
            ddlLocation.DataValueField = "SiteID";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("--- Select Location ---", "0"));
            ddlLocation.SelectedIndex = 0;

            ddlPO.Items.Clear();
            ddlPO.DataSource = objDB.GetAllPOsForDirectPayment(ref errorMsg);
            ddlPO.DataTextField = "poCode";
            ddlPO.DataValueField = "POID";
            ddlPO.DataBind();
            ddlPO.Items.Insert(0, new ListItem("--- Select PO ---", "0"));
            ddlPO.SelectedIndex = 0;

            ddlGRN.Items.Clear();
            ddlGRN.DataSource = objDB.GetAllGRNsApprovedByFinance(ref errorMsg);
            ddlGRN.DataTextField = "GRNCode";
            ddlGRN.DataValueField = "GRNID";
            ddlGRN.DataBind();
            ddlGRN.Items.Insert(0, new ListItem("--- Select GRN ---", "0"));
            ddlGRN.SelectedIndex = 0;

            ddlInvoice.Items.Clear();
            ddlInvoice.DataSource = objDB.GetAllInvoicesForDirectPayment(ref errorMsg);
            ddlInvoice.DataTextField = "Code";
            ddlInvoice.DataValueField = "InvoiceID";
            ddlInvoice.DataBind();
            ddlInvoice.Items.Insert(0, new ListItem("--- Select Invoice ---", "0"));
            ddlInvoice.SelectedIndex = 0;


        }

        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApproveByFinance.Visible = false;
            btnReviewByFinance.Visible = false;
            btnRevApproveByFinance.Visible = false;
            btnReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReviewByFinance.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;
            btnGenVoucher.Visible = false;

            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "DirectPayments";
            objDB.PrimaryColumnnName = "DirectPaymentID";
            objDB.PrimaryColumnValue = DirectPaymentID.ToString();
            objDB.DocName = "Purchase Order";

            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
            if (chkAccessLevel == "Can Edit")
            {
                btnSubForReviewByFinance.Visible = true;
                btnSave.Visible = true;
                //lnkDelete.Visible = true;
                //btnSubForReviewByFinance.Visible = true;
            }
            //if (chkAccessLevel == "Can Edit")
            //{
            //    btnSave.Visible = true;
            //    lnkDelete.Visible = true;
            //    btnSubForReviewByFinance.Visible = true;
            //}
            if (chkAccessLevel == "Can Edit & Review")
            {
                btnSave.Visible = true;
                btnReviewByFinance.Visible = true;
                btnReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve")
            {
                btnSave.Visible = true;
                btnApproveByFinance.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve")
            {
                btnSave.Visible = true;
                btnRevApproveByFinance.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit")
            {
                //btnSave.Visible = true;
                btnGenVoucher.Visible = true;
            }
        }


        protected bool CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Finance", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
            return true;
        }

        private void getDirectPaymentDetails(int DirectPaymentID)
        {
            DataTable dt = new DataTable();
            objDB.DirectPaymentID = DirectPaymentID;
            dt = objDB.GetDirectPaymentsByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlType.SelectedValue = dt.Rows[0]["Type"].ToString();
                    if (dt.Rows[0]["GeneratedDate"].ToString() != "")
                    {
                        txtDate.Value = Convert.ToDateTime(dt.Rows[0]["GeneratedDate"].ToString()).ToString("dd-MMM-yyyy");
                    }
                    txtSerialNo.Value = dt.Rows[0]["SerialNo"].ToString();
                    txtVoucherNO.Value = dt.Rows[0]["VoucherNo"].ToString();
                    txtRefNO.Value = dt.Rows[0]["RefNo"].ToString();
                    TotalDebit = float.Parse(dt.Rows[0]["TotalDebit"].ToString());
                    TotalCredit = float.Parse(dt.Rows[0]["TotalCredit"].ToString());

                    DosSatus = dt.Rows[0]["DocStatus"].ToString();
                    txtDescription.Value = dt.Rows[0]["Description"].ToString();
                    txtTotalCredit.Value = dt.Rows[0]["TotalCredit"].ToString();
                    txtTotalDebit.Value = dt.Rows[0]["TotalDebit"].ToString();
                    ddlAgaints.SelectedValue = dt.Rows[0]["Against"].ToString();
                    ddlLocation.SelectedValue = dt.Rows[0]["LocationID"].ToString();

                    if (ddlAgaints.SelectedValue == "INV")
                    {
                        INVDIV.Visible = true;
                        ddlInvoice.SelectedValue = dt.Rows[0]["TableID"].ToString();
                        ddlInvoice.Enabled = false;
                        ddlAgaints.Enabled = false;

                    }
                    else if (ddlAgaints.SelectedValue == "GRN")
                    {
                        GRNDIV.Visible = true;
                        ddlGRN.SelectedValue = dt.Rows[0]["TableID"].ToString();
                        ddlGRN.Enabled = false;
                        ddlAgaints.Enabled = false;

                    }
                    else if (ddlAgaints.SelectedValue == "PO")
                    {
                        PODIV.Visible = true;
                        ddlPO.SelectedValue = dt.Rows[0]["TableID"].ToString();
                        ddlPO.Enabled = false;
                        ddlAgaints.Enabled =     false;
                    }

                    if (dt.Rows[0]["DocStatus"].ToString() == "Approved By Finance")
                    {
                        //btnGenVoucher.Visible = true;
                        //btnPayVoucher.Visible = true;
                    }
                    else
                    {
                        //btnGenVoucher.Visible = false;
                        //btnPayVoucher.Visible = false;

                    }
                    getDirectPaymentDetailByDirectPaymentID(DirectPaymentID);
                }
            }
            Common.addlog("View", "Finance", "DirectPayment \"" + txtVoucherNO.Value + "\" Viewed", "DirectPayments", objDB.DirectPaymentID);

        }



        private void getDirectPaymentDetailByDirectPaymentID(int DirectPaymentID)
        {
            DataTable dt = new DataTable();
            objDB.DirectPaymentID = DirectPaymentID;
            dt = objDB.GetDirectPaymentDetailByDirectPaymentID(ref errorMsg);
            //dtPaymentDetails = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dtPaymentDetails.NewRow();
                        dr[0] = (i + 1).ToString();
                        dr[1] = dt.Rows[i]["CodeTitle"].ToString();
                        dr[2] = dt.Rows[i]["COA_ID"].ToString();
                        dr[3] = dt.Rows[i]["Instrument"].ToString();
                        dr[4] = dt.Rows[i]["TransactionDetail"].ToString();
                        dr[5] = dt.Rows[i]["Debit"].ToString();
                        dr[6] = dt.Rows[i]["Credit"].ToString();
                        dtPaymentDetails.Rows.Add(dr);
                    }
                    dtPaymentDetails.AcceptChanges();
                    srNo = Convert.ToInt32(dtPaymentDetails.Rows[dtPaymentDetails.Rows.Count - 1][0].ToString()) + 1;
                }
            }
        }

        protected DataTable dtPaymentDetails
        {
            get
            {
                if (ViewState["dtPaymentDetails"] != null)
                {
                    return (DataTable)ViewState["dtPaymentDetails"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtPaymentDetails"] = value;
            }
        }

        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }

        protected void BindData()
        {
            if (ViewState["dtPaymentDetails"] == null)
            {
                dtPaymentDetails = createTable();
                ViewState["dtPaymentDetails"] = dtPaymentDetails;
            }
            else if (dtPaymentDetails.Rows.Count == 0)
            {
                dtPaymentDetails = createTable();
                showFirstRow = false;
            }
            gv.DataSource = dtPaymentDetails;
            gv.DataBind();

            if (showFirstRow)
                gv.Rows[0].Visible = true;
            else
                gv.Rows[0].Visible = false;

            gv.UseAccessibleHeader = true;
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;

        }

        private DataTable createTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("COACode");
            dt.Columns.Add("COA_ID");
            dt.Columns.Add("Instrument");
            dt.Columns.Add("TransactionDetail");
            dt.Columns.Add("Debit");
            dt.Columns.Add("Credit");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();
            return dt;
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Footer)
            {

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                DropDownList ddlExpenseCOA = e.Row.FindControl("ddlCOA") as DropDownList;
                ddlExpenseCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
                ddlExpenseCOA.DataTextField = "CodeTitle";
                ddlExpenseCOA.DataValueField = "COA_ID";
                ddlExpenseCOA.DataBind();
                ddlExpenseCOA.Items.Insert(0, new ListItem("--- Select COA ---", "0"));

                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = srNo.ToString();

            }
            if (e.Row.RowType == DataControlRowType.Header)
            {

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    hdnRowNo.Value = e.Row.RowIndex.ToString();
                    int rowIndex = e.Row.RowIndex;
                }
            }

        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CheckSessions();
            bool similar = false;
            double Debit = double.Parse(((TextBox)gv.FooterRow.FindControl("txtDebit")).Text == "" ? "0" : ((TextBox)gv.FooterRow.FindControl("txtDebit")).Text);
           double Credit = double.Parse(((TextBox)gv.FooterRow.FindControl("txtCredit")).Text == "" ? "0" : ((TextBox)gv.FooterRow.FindControl("txtCredit")).Text);

            if ((Debit == 0 && Credit == 0) || (Debit != 0 && Credit != 0))
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "Invalid Entry!";
                return;
            }
            
            foreach (DataRow row in dtPaymentDetails.Rows)
            {
                if (row[2].ToString() == ((DropDownList)gv.FooterRow.FindControl("ddlCOA")).SelectedItem.Value && ((double.Parse(row[5].ToString()) == 0 && Debit == 0) || (double.Parse(row[6].ToString()) == 0 && Credit == 0)) )
                {
                    TotalDebit += Debit;
                    TotalCredit += Credit;
                    txtTotalCredit.Value = TotalCredit.ToString();
                    txtTotalDebit.Value = TotalDebit.ToString();

                    Debit = double.Parse(row[5].ToString()) + Debit;
                    Credit = double.Parse(row[6].ToString()) + Credit;

                    if (((ddlAgaints.SelectedValue == "INV" && Credit != 0) || (Convert.ToInt32(row[2].ToString()) == AgainstCOA && Debit != 0)) || ((ddlAgaints.SelectedValue == "GRN" && Debit != 0) || (Convert.ToInt32(row[2].ToString()) == AgainstCOA && Credit != 0)) || ((ddlAgaints.SelectedValue == "PO" && Debit != 0) || (Convert.ToInt32(row[2].ToString()) == AgainstCOA && Credit != 0)))
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "Invalid Entry!";
                        return;
                    }

                    //if (!CheckIsValid())
                    //{
                    //    divAlertMsg.Visible = true;
                    //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    //    pAlertMsg.InnerHtml = "Invalid Entries! please check accounts entries";
                    //    return;
                    //}


                    row.SetField(3, ((TextBox)gv.FooterRow.FindControl("txtInstrument")).Text);
                    row.SetField(4, ((TextBox)gv.FooterRow.FindControl("txtTransactionDetail")).Text);
                    row.SetField(5, Debit.ToString());
                    row.SetField(6, Credit.ToString());
                    dtPaymentDetails.AcceptChanges();

                    

                    similar = true;
                    break;
                }
                
            }
            
            if (!similar)
            {
                //    string Credit = ((TextBox)gv.FooterRow.FindControl("txtCredit")).Text == "" ? "0" : ((TextBox)gv.FooterRow.FindControl("txtCredit")).Text;
                //    string Debit = ((TextBox)gv.FooterRow.FindControl("txtDebit")).Text == "" ? "0" : ((TextBox)gv.FooterRow.FindControl("txtDebit")).Text;

                if (((ddlAgaints.SelectedValue == "INV" && Credit != 0) || (Convert.ToInt32(((DropDownList)gv.FooterRow.FindControl("ddlCOA")).SelectedItem.Value) == AgainstCOA && Debit != 0)) || ((ddlAgaints.SelectedValue == "GRN" && Debit != 0) || (Convert.ToInt32(((DropDownList)gv.FooterRow.FindControl("ddlCOA")).SelectedItem.Value) == AgainstCOA && Credit != 0)) || ((ddlAgaints.SelectedValue == "PO" && Debit != 0) || (Convert.ToInt32(((DropDownList)gv.FooterRow.FindControl("ddlCOA")).SelectedItem.Value) == AgainstCOA && Credit != 0)))
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Invalid Entry!";
                    return;
                }
                DataRow dr = dtPaymentDetails.NewRow();
                dr[0] = srNo.ToString();
                dr[1] = ((DropDownList)gv.FooterRow.FindControl("ddlCOA")).SelectedItem.Text;
                dr[2] = ((DropDownList)gv.FooterRow.FindControl("ddlCOA")).SelectedItem.Value;
                dr[3] = ((TextBox)gv.FooterRow.FindControl("txtInstrument")).Text;
                dr[4] = ((TextBox)gv.FooterRow.FindControl("txtTransactionDetail")).Text;
                dr[5] = Debit.ToString();
                dr[6] = Credit.ToString();



                dtPaymentDetails.Rows.Add(dr);
                dtPaymentDetails.AcceptChanges();

                //if (!CheckIsValid())
                //{
                //    divAlertMsg.Visible = true;
                //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //    pAlertMsg.InnerHtml = "Invalid Entries! please check accounts entries";
                //    return;
                //}
                TotalDebit += Debit;
                TotalCredit += Credit;

                txtTotalCredit.Value = TotalCredit.ToString();
                txtTotalDebit.Value = TotalDebit.ToString();

                srNo += 1;
            }
            BindData();
            ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
        }

        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void gv_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {

        }

        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;
            bool notSimilar = true;
            double Credit = double.Parse(((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditCredit")).Text == "" ? "0" : ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditCredit")).Text);
            double Debit = double.Parse(((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditDebit")).Text == "" ? "0" : ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditDebit")).Text);

            //string Credit = ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditCredit")).Text == "" ? "0" : ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditCredit")).Text;
            //string Debit = ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditDebit")).Text == "" ? "0" : ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditDebit")).Text;

            int AgainstCOAID = Convert.ToInt32(dtPaymentDetails.Rows[index]["COA_ID"].ToString());

            if ((Debit == 0 && Credit == 0) || (Debit != 0 && Credit != 0))
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "Invalid Entry!";
                return;
            }
            
            if ((ddlAgaints.SelectedValue == "INV" && Credit != 0 && AgainstCOAID != AgainstCOA) || (ddlAgaints.SelectedValue == "GRN" && Debit != 0 && AgainstCOAID != AgainstCOA) || (ddlAgaints.SelectedValue == "PO" && Debit != 0 && AgainstCOAID != AgainstCOA))
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "Invalid Entry!";
                return;
            }

            int i = 0;
            foreach (DataRow row in dtPaymentDetails.Rows)
            {
                if (row[2].ToString() == AgainstCOAID.ToString() && i != index  && ((double.Parse(row[5].ToString()) == 0 && Debit == 0) || (double.Parse(row[6].ToString()) == 0 && Credit == 0)))
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Invalid Entry!";

                    notSimilar = false;
                    break;
                }
                i++;
            }

            if (notSimilar)
            {
                TotalDebit += (Debit - double.Parse(dtPaymentDetails.Rows[index]["Debit"].ToString()));
                TotalCredit += (Credit - double.Parse(dtPaymentDetails.Rows[index]["Credit"].ToString()));

                dtPaymentDetails.Rows[index].SetField(3, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditInstrument")).Text);
                dtPaymentDetails.Rows[index].SetField(4, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditTransactionDetail")).Text);
                dtPaymentDetails.Rows[index].SetField(5, Debit.ToString());
                dtPaymentDetails.Rows[index].SetField(6, Credit.ToString());
                dtPaymentDetails.AcceptChanges();
            }


            //if (!CheckIsValid())
            //{
            //    divAlertMsg.Visible = true;
            //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
            //    pAlertMsg.InnerHtml = "Invalid Entries! please check accounts entries";
            //    return;
            //}                 s

            txtTotalCredit.Value = TotalCredit.ToString();
            txtTotalDebit.Value = TotalDebit.ToString();

            gv.EditIndex = -1;
            BindData();
        }

        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindData();
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtPaymentDetails.Rows.Count; i++)
            {
                if (dtPaymentDetails.Rows[i][0].ToString() == delSr)
                {
                    dtPaymentDetails.Rows[i].Delete();
                    dtPaymentDetails.AcceptChanges();
                }
            }
            for (int i = 0; i < dtPaymentDetails.Rows.Count; i++)
            {
                dtPaymentDetails.Rows[i].SetField(0, i);
                dtPaymentDetails.AcceptChanges();
            }
            srNo = dtPaymentDetails.Rows.Count;
            BindData();
        }

        protected void gv_PreRender(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
                gv.UseAccessibleHeader = true;
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                BindData();
                string res = "";
                string tempMsg = CheckTransactionTable();
                if (tempMsg != "")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = tempMsg;
                    return;
                }
                if (!IsBalanced())
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = " Total Credit & Total Debit are not balanaced";
                    return;
                }

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.GeneratedDate = txtDate.Value;
                objDB.VoucherNo = txtVoucherNO.Value;
                objDB.SerialNo = txtSerialNo.Value;
                objDB.RefNO = txtRefNO.Value;
                objDB.TotalDebit = float.Parse(txtTotalDebit.Value);
                objDB.TotalCredit = float.Parse(txtTotalCredit.Value);
                objDB.Description = txtDescription.Value;
                objDB.Type = ddlType.SelectedValue;
                objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
                objDB.Against = ddlAgaints.SelectedValue;
                if (ddlAgaints.SelectedValue == "INV")
                {
                    objDB.TableName = "InvoicePay";
                    objDB.ParentID = Convert.ToInt32(ddlInvoice.SelectedValue);
                    objDB.TableID = Convert.ToInt32(ddlInvoice.SelectedValue);
                    if (objDB.ParentID == 0)
                    {
                        spnReqInvoice.InnerHtml = "*Required";
                        return;
                    }
                }
                else if (ddlAgaints.SelectedValue == "GRN")
                {
                    objDB.TableName = "GRNPay";
                    objDB.ParentID = Convert.ToInt32(ddlGRN.SelectedValue);
                    objDB.TableID = Convert.ToInt32(ddlGRN.SelectedValue);
                    if (objDB.ParentID == 0)
                    {
                        spnReqGRN.InnerHtml = "*Required";
                        return;
                    }
                }
                else if (ddlAgaints.SelectedValue == "PO")
                {
                    objDB.TableName = "POPay";
                    objDB.ParentID = Convert.ToInt32(ddlPO.SelectedValue);
                    objDB.TableID = Convert.ToInt32(ddlPO.SelectedValue);
                    if (objDB.ParentID == 0)
                    {
                        spnReqPO.InnerHtml = "*Required";
                        return;
                    }
                }
                else
                {
                    objDB.TableName = "DirectPayments";
                    objDB.ParentID = 0;
                }
                objDB.LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
                objDB.ModifiedBy = Session["UserName"].ToString();

                objDB.isApproved = true;
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string btnType = btn.ID.ToString();

                objDB.DocStatus = DosSatus;

                if (btnType == "btnSubForReviewByFinance")
                {
                    objDB.DocStatus = "Data Submitted for Review";
                }
                else if (btnType == "btnReviewByFinance")
                {
                    objDB.DocStatus = "Reviewed";
                }
                else if (btnType == "btnApproveByFinance" || btnType == "btnRevApproveByFinance")
                {
                    objDB.DocStatus = "Approved";
                    
                }
                else if (btnType == "btnRejDisApprove" || btnType == "btnDisapprove")
                {
                    objDB.DocStatus = "Disapproved";
                }
                else if (btnType == "btnReject")
                {
                    objDB.DocStatus = "Rejected";
                }

                if (HttpContext.Current.Items["DirectPaymentID"] != null)
                {
                    objDB.DirectPaymentID = DirectPaymentID;
                    res = objDB.UpdateDirectPayments();
                    if (objDB.DocStatus == "Approved")
                    {
                        res = "Paid";
                        PrintVoucher(DirectPaymentID);
                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = objDB.AddDirectPayments(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            objDB.SerialNo = dt.Rows[0]["SerialNo"].ToString();
                            objDB.VoucherNo = dt.Rows[0]["VoucherNo"].ToString();
                            objDB.DirectPaymentID = Convert.ToInt32(dt.Rows[0]["DirectPaymentID"].ToString());
                            DirectPaymentID = objDB.DirectPaymentID;
                            res = "New DirectPayment Added Successfully";
                        }
                        else
                        {
                            objDB.SerialNo = "0";
                        }
                        //DirectPaymentID = Convert.ToInt32(returnedID);
                    }
                }
                objDB.DirectPaymentID = DirectPaymentID;
                dtPaymentDetails = (DataTable)ViewState["dtPaymentDetails"] as DataTable;
                if (res == "DirectPayment Updated Successfully" || res == "New DirectPayment Added Successfully")
                {
                    if (objDB.ParentID == 0)
                    {
                        objDB.TableID = DirectPaymentID;
                    }
                    else
                    {
                        objDB.TableID = 0;
                    }

                    objDB.AccountID = 0;
                    objDB.PayType = ddlType.SelectedValue;
                    objDB.WOT = 0;
                    objDB.STWH = 0;
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.PayDate = txtDate.Value;
                    objDB.PayAmount = float.Parse(txtTotalDebit.Value);
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.PayCOAID = 0;

                    if (ddlAgaints.SelectedValue == "INV")
                    {
                        if (HttpContext.Current.Items["DirectPaymentID"] != null && objDB.DocStatus == "Approved")
                        {
                            objDB.InvoiceID = objDB.ParentID;
                            res = objDB.AddInvoicePay();
                        }

                        objDB.DeleteTransactionHistoryByParentID();
                    }
                    else if (ddlAgaints.SelectedValue == "GRN")
                    {
                        if (HttpContext.Current.Items["DirectPaymentID"] != null && objDB.DocStatus == "Approved")
                        {
                            objDB.GRNID = objDB.ParentID;
                            DataTable dt = new DataTable();
                            dt = objDB.GetGRNsByIDFinance(ref errorMsg);
                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    POID = Convert.ToInt32(dt.Rows[0]["POID"].ToString());
                                    objDB.POID = POID;
                                }
                            }
                            res = objDB.AddPOPayAgainstGRN();
                        }

                        objDB.DeleteTransactionHistoryByParentID();
                    }
                    else if (ddlAgaints.SelectedValue == "PO")
                    {
                        if (HttpContext.Current.Items["DirectPaymentID"] != null && objDB.DocStatus == "Approved")
                        {
                            objDB.POID = Convert.ToInt32(ddlPO.SelectedValue);
                            res = objDB.AddPOPay();
                        }
                        objDB.DeleteTransactionHistoryByParentID();
                    }
                    else
                    {
                        objDB.DeleteTransactionHistoryByTableID();
                    }

                    
                    if (res == "Paid" || res == "DirectPayment Updated Successfully" || res == "New DirectPayment Added Successfully")
                    {
                        if (objDB.SerialNo != "0")
                        {
                            for (int i = 0; i < dtPaymentDetails.Rows.Count; i++)
                            {
                                if (dtPaymentDetails.Rows[i][0].ToString() == "")
                                {
                                    dtPaymentDetails.Rows[i].Delete();
                                    dtPaymentDetails.AcceptChanges();
                                    //continue;
                                }
                                if (dtPaymentDetails.Rows[i]["COA_ID"].ToString() != null && dtPaymentDetails.Rows[i]["COA_ID"].ToString() != "")
                                {
                                    objDB.ChartOfAccountID = Convert.ToInt32(dtPaymentDetails.Rows[i]["COA_ID"].ToString());
                                }
                                else
                                {
                                    objDB.ChartOfAccountID = 0;
                                }
                                objDB.TransactionDetail = dtPaymentDetails.Rows[i]["TransactionDetail"].ToString();
                                objDB.Instrument = dtPaymentDetails.Rows[i]["Instrument"].ToString();
                                if (dtPaymentDetails.Rows[i]["Debit"].ToString() != "" && float.Parse(dtPaymentDetails.Rows[i]["Debit"].ToString()) != 0)
                                {
                                    objDB.Debit = float.Parse(dtPaymentDetails.Rows[i]["Debit"].ToString());
                                    objDB.AddTransactionHistoryDebit();
                                }
                                else
                                {
                                    objDB.Credit = float.Parse(dtPaymentDetails.Rows[i]["Credit"].ToString());
                                    objDB.AddTransactionHistoryCredit();
                                }
                            }
                        }
                    }

                }



                if (res == "Paid" || res == "DirectPayment Updated Successfully" || res == "New DirectPayment Added Successfully")
                {
                    if (!showFirstRow)
                    {
                        ViewState["dtPaymentDetails"] = null;
                        dtPaymentDetails = createTable();
                        srNo = 1;
                    }


                    if (res == "New DirectPayment Added Successfully") { Common.addlog("Add", "Finance", " DirectPayment \"" + objDB.VoucherNo + "\" Added", "DirectPayments"); clearFields(); }
                    if (res == "DirectPayment Updated Successfully")
                    {
                        getDirectPaymentDetails(DirectPaymentID);
                        Common.addlog("Update", "Finance", "DirectPayment \"" + objDB.VoucherNo + "\" Updated", "DirectPayments", objDB.DirectPaymentID);
                        CheckAccess();
                    }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    if (res == "Paid" && objDB.DocStatus == "Approved")
                    {
                        res = "Approved";
                        getDirectPaymentDetails(DirectPaymentID);
                        CheckAccess();
                        
                    }
                    pAlertMsg.InnerHtml = res;
                    
                    BindData();

                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }



        public bool CheckIsValid()
        {
            bool isValid = true;
            TotalDebit = 0;
            TotalCredit = 0;

            if (dtPaymentDetails != null)
            {
                if (dtPaymentDetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPaymentDetails.Rows.Count; i++)
                    {
                        if (dtPaymentDetails.Rows[i][1].ToString() == "")
                        {
                            continue;
                        }
                        if ((float.Parse(dtPaymentDetails.Rows[i]["Debit"].ToString()) == 0 && float.Parse(dtPaymentDetails.Rows[i]["Credit"].ToString()) == 0) || (float.Parse(dtPaymentDetails.Rows[i]["Debit"].ToString()) != 0 && float.Parse(dtPaymentDetails.Rows[i]["Credit"].ToString()) != 0))
                        {
                            isValid = false;
                            TotalCredit = 0;
                            TotalDebit = 0;
                            break;
                        }
                        else
                        {
                            TotalDebit += float.Parse(dtPaymentDetails.Rows[i]["Debit"].ToString());
                            TotalCredit += float.Parse(dtPaymentDetails.Rows[i]["Credit"].ToString());
                        }
                    }
                }
            }
            return isValid;
        }

        public string CheckTransactionTable()
        {
            //bool isValid = true;
            string Msg = "";

            if (dtPaymentDetails != null)
            {
                if ((dtPaymentDetails.Rows.Count > 0 && dtPaymentDetails.Rows[0][1].ToString() != "") || (dtPaymentDetails.Rows.Count > 1 && dtPaymentDetails.Rows[0][1].ToString() == ""))
                {
                    for (int i = 0; i < dtPaymentDetails.Rows.Count; i++)
                    {
                        if (dtPaymentDetails.Rows[i][1].ToString() == "")
                        {
                            continue;
                        }
                        if ((float.Parse(dtPaymentDetails.Rows[i]["Debit"].ToString()) == 0 && float.Parse(dtPaymentDetails.Rows[i]["Credit"].ToString()) == 0) || (float.Parse(dtPaymentDetails.Rows[i]["Debit"].ToString()) != 0 && float.Parse(dtPaymentDetails.Rows[i]["Credit"].ToString()) != 0))
                        {
                            //isValid = false;
                            TotalCredit = 0;
                            TotalDebit = 0;
                            Msg = "Invalid Entries! Please check table entries.";
                            break;
                        }
                        else
                        {
                            TotalDebit += float.Parse(dtPaymentDetails.Rows[i]["Debit"].ToString());
                            TotalCredit += float.Parse(dtPaymentDetails.Rows[i]["Credit"].ToString());
                        }
                    }
                }
                else
                {
                    Msg = "Voucher record can not be empty!";
                    //isValid = false;
                }

            }
            return Msg;
        }

        public bool IsBalanced()
        {
            bool isBalanced = true;
            if ((TotalDebit == 0 && TotalCredit == 0) || TotalDebit != TotalCredit)
            {
                isBalanced = false;
            }
            return isBalanced;
        }

        private void clearFields()
        {
            txtVoucherNO.Value = "";
            txtSerialNo.Value = "";
            txtRefNO.Value = "";
            txtDate.Value = "";
            ddlAgaints.SelectedValue = "Direct";
            ddlAgaints_SelectedIndexChanged(null, null);
            //TotalDebit = 0;
            //TotalCredit = 0;
        }

        //protected void Button1_ServerClick(object sender, EventArgs e)
        //{

        //    if (!CheckIsValid())
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = "Invalid Entries! please check accounts entries";
        //        return;
        //    }
        //    if (!IsBalanced())
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = " Total Credit & Total Debit are not balanaced";
        //        return;
        //    }
        //    System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;

        //    if (btn.ID.ToString() == "btnSubForReview" || btn.ID.ToString() == "btnSubForReviewByFinance")
        //    {
        //        btnSubmit_ServerClick(null, null);
        //    }


        //    string res = Common.addAccessLevels(btn.ID.ToString(), "DirectPayments", "DirectPaymentID", HttpContext.Current.Items["DirectPaymentID"].ToString(), Session["UserName"].ToString());
        //    Common.addlogNew(res, "Finance", "DirectPayments of ID\"" + HttpContext.Current.Items["DirectPaymentID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/view/direct-payment", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/finance/manage/direct-payment/edit-direct-payment-" + HttpContext.Current.Items["DirectPaymentID"].ToString(), "DirectPayments \"" + txtVoucherNO.Value + "\"", "DirectPayments", "DirectPayments", Convert.ToInt32(HttpContext.Current.Items["DirectPaymentID"].ToString()));

        //    //Common.addlog(res, "Finance", "DirectPayments of ID \"" + HttpContext.Current.Items["DirectPaymentID"].ToString() + "\" Status Changed", "DirectPayments", DirectPaymentID /* ID From Page Top  */);/* Button1_ServerClick  */

        //    if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
        //    {
        //        //btnGenVoucher.Visible = true;
        //        //btnPayVoucher.Visible = true;
        //        //btnPayNow.Visible = true;
        //        //AddVoucher();

        //    }
        //    divAlertMsg.Visible = true;
        //    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
        //    pAlertMsg.InnerHtml = res;

        //    CheckAccess();
        //}

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            string type = btn.CommandArgument;
            string res = Common.addAccessLevels(type, "DirectPayments", "DirectPaymentID", HttpContext.Current.Items["DirectPaymentID"].ToString(), Session["UserName"].ToString());

            Common.addlog("Delete", "Finance", "DirectPayments of ID \"" + HttpContext.Current.Items["DirectPaymentID"].ToString() + "\" deleted", "DirectPayments", DirectPaymentID /* ID From Page Top  */);/* lnkDelete_Click  */

            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            Response.Redirect(btnBack.HRef);

        }



        protected void AddVoucher()
        {
            CheckSessions();

            string JVID = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.VoucherNo = txtVoucherNO.Value;
            objDB.Against = "Purchase Order";
            objDB.CreatedBy = Session["UserName"].ToString();
            JVID = objDB.AddFinanceVoucher();
            objDB.DirectPaymentID = DirectPaymentID;
            objDB.JVID = Convert.ToInt32(JVID);
            objDB.AddJVIDinPO();
        }

        protected void addBCVoucher(int ID)
        {
            try
            {
                int BCVoucherID = 0;
                CheckSessions();
                objDB.VouchrType = ddlType.SelectedValue;
                objDB.VoucherNo = txtVoucherNO.Value;
                objDB.Against = "Purchase Order";
                objDB.TableName = "DirectPayments";
                objDB.TableID = ID;
                objDB.VoucherAmount = TotalDebit;
                //objDB.CreditCOA = int.Parse(ddlCOAPN.SelectedValue);
                //objDB.DebitCOA = int.Parse(ddlCOA.SelectedValue);
                objDB.TransactionDetail = "Paid amount against DirectPayment# " + txtVoucherNO.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                BCVoucherID = Convert.ToInt32(objDB.AddBankCashVoucher());
                //VouchrType, VoucherNo, Against, TableName, TableID, VoucherAmount,CreditCOA,DebitCOA,TransactionDetail, CreatedBy, CompanyID 

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            objDB.Type = ddlType.SelectedValue;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            DataTable dt = new DataTable();
            dt = objDB.GenerateSerialAndVoucherNo(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtSerialNo.Value = dt.Rows[0]["SerialNo"].ToString();
                    txtVoucherNO.Value = dt.Rows[0]["VoucherNo"].ToString();
                }
            }
        }

        protected void ddlAgaints_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlAgaints.SelectedValue == "GRN")
            {
                GRNDIV.Visible = true;
                PODIV.Visible = false;
                INVDIV.Visible = false;
                ddlGRN.SelectedValue = "0";
                ddlPO.SelectedValue = "0";
                ddlInvoice.SelectedValue = "0";

            }
            else if (ddlAgaints.SelectedValue == "PO")
            {
                GRNDIV.Visible = false;
                PODIV.Visible = true;
                INVDIV.Visible = false;
                ddlGRN.SelectedValue = "0";
                ddlPO.SelectedValue = "0";
                ddlInvoice.SelectedValue = "0";

            }
            else if (ddlAgaints.SelectedValue == "INV")
            {
                GRNDIV.Visible = false;
                PODIV.Visible = false;
                INVDIV.Visible = true;
                ddlGRN.SelectedValue = "0";
                ddlPO.SelectedValue = "0";
                ddlInvoice.SelectedValue = "0";

            }
            else
            {
                GRNDIV.Visible = false;
                PODIV.Visible = false;
                INVDIV.Visible = false;
                ddlGRN.SelectedValue = "0";
                ddlPO.SelectedValue = "0";
                ddlInvoice.SelectedValue = "0";
            }
            ViewState["srNo"] = null;
            ViewState["dtPaymentDetails"] = null;
            ViewState["showFirstRow"] = null;
            dtPaymentDetails = createTable();
            BindData();
        }

        protected int AgainstCOA
        {
            get
            {
                if (ViewState["AgainstCOA"] != null)
                {
                    return (int)ViewState["AgainstCOA"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AgainstCOA"] = value;
            }
        }

        protected void ddlGRN_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["srNo"] = null;
            ViewState["dtPaymentDetails"] = null;
            ViewState["showFirstRow"] = null;
            dtPaymentDetails = createTable();
            BindData();

            DataTable dt = new DataTable();
            objDB.GRNID = Convert.ToInt32(ddlGRN.SelectedValue);
            dt = objDB.GetGRNsByIDFinance(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlLocation.SelectedValue = dt.Rows[0]["LocationID"].ToString();
                    txtRefNO.Value = dt.Rows[0]["GRNCode"].ToString();
                    objDB.SupplierID = Convert.ToInt32(dt.Rows[0]["SupplierID"].ToString());
                    POID = Convert.ToInt32(dt.Rows[0]["POID"].ToString());
                    objDB.ChartOfAccountID = Convert.ToInt32(objDB.usp_TF_GetSupplierCOAIDByID());
                    AgainstCOA = objDB.ChartOfAccountID;

                    DataRow dr = dtPaymentDetails.NewRow();
                    dr[0] = srNo.ToString();
                    dr[1] = dt.Rows[0]["CodeTitle"].ToString();
                    dr[2] = dt.Rows[0]["COAID"].ToString();
                    dr[3] = "";
                    dr[4] = "";
                    dr[5] = dt.Rows[0]["RemainingAmount"].ToString();
                    dr[6] = "0";
                    dtPaymentDetails.Rows.Add(dr);

                    dtPaymentDetails.AcceptChanges();
                    srNo = Convert.ToInt32(dtPaymentDetails.Rows[dtPaymentDetails.Rows.Count - 1][0].ToString()) + 1;
                    txtTotalDebit.Value = dt.Rows[0]["RemainingAmount"].ToString();
                    BindData();

                }
            }
        }

        protected void ddlPO_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["srNo"] = null;
            ViewState["dtPaymentDetails"] = null;
            ViewState["showFirstRow"] = null;
            dtPaymentDetails = createTable();
            BindData();
            DataTable dt = new DataTable();
            objDB.POID = Convert.ToInt32(ddlPO.SelectedValue);
            POID = objDB.POID;
            dt = objDB.GetPOsByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlLocation.SelectedValue = dt.Rows[0]["LocationID"].ToString();
                    txtRefNO.Value = dt.Rows[0]["POCode"].ToString();
                    objDB.SupplierID = Convert.ToInt32(dt.Rows[0]["SupplierID"].ToString());
                    objDB.ChartOfAccountID = Convert.ToInt32(objDB.usp_TF_GetSupplierCOAIDByID());
                    AgainstCOA = objDB.ChartOfAccountID;

                    DataRow dr = dtPaymentDetails.NewRow();
                    dr[0] = srNo.ToString();
                    dr[1] = dt.Rows[0]["CodeTitle"].ToString();
                    dr[2] = dt.Rows[0]["COAID"].ToString();
                    dr[3] = "";
                    dr[4] = "";
                    dr[5] = dt.Rows[0]["RemainingAmount"].ToString();
                    dr[6] = "0";
                    dtPaymentDetails.Rows.Add(dr);

                    dtPaymentDetails.AcceptChanges();
                    srNo = Convert.ToInt32(dtPaymentDetails.Rows[dtPaymentDetails.Rows.Count - 1][0].ToString()) + 1;
                    txtTotalDebit.Value = dt.Rows[0]["RemainingAmount"].ToString();
                    BindData();

                }
            }
        }

        protected void ddlInvoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["srNo"] = null;
            ViewState["dtPaymentDetails"] = null;
            ViewState["showFirstRow"] = null;
            dtPaymentDetails = createTable();
            BindData();
            DataTable dt = new DataTable();
            objDB.InvoiceID = Convert.ToInt32(ddlInvoice.SelectedValue);
            dt = objDB.GetInvoiceByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlLocation.SelectedValue = dt.Rows[0]["SiteID"].ToString();
                    txtRefNO.Value = dt.Rows[0]["Code"].ToString();
                    objDB.ClientID = Convert.ToInt32(dt.Rows[0]["ClientID"].ToString());
                    objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetClientCOAByClientID());
                    AgainstCOA = objDB.ChartOfAccountID;

                    DataRow dr = dtPaymentDetails.NewRow();
                    dr[0] = srNo.ToString();
                    dr[1] = dt.Rows[0]["CodeTitle"].ToString();
                    dr[2] = dt.Rows[0]["COAID"].ToString();
                    dr[3] = "";
                    dr[4] = "";
                    dr[5] = "0";
                    dr[6] = dt.Rows[0]["RemainingAmount"].ToString();
                    dtPaymentDetails.Rows.Add(dr);

                    dtPaymentDetails.AcceptChanges();
                    srNo = Convert.ToInt32(dtPaymentDetails.Rows[dtPaymentDetails.Rows.Count - 1][0].ToString()) + 1;
                    txtTotalCredit.Value = dt.Rows[0]["RemainingAmount"].ToString();

                    BindData();


                }
            }
        }

        int TraID
        {
            get
            {
                if (ViewState["TraID"] != null)
                {
                    return (int)ViewState["TraID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["TraID"] = value;
            }
        }
        protected void PrintVoucher(int ID)
        {
            hdnTransactionID.Value = ID.ToString();
            TraID = ID;
        }
        protected void GenVoucherPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (TraID != 0)
                {
                    Common.PrintVoucher(TraID);
                }
            }

            catch (Exception ex)
            {

            }
            finally
            {
                TraID = 0;
            }


        }


        //protected void btnPay_ServerClick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string res = "";
        //        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //        objDB.PayDate = txtDate.Value;
        //        objDB.PayAmount = TotalAmount;
        //        objDB.ModifiedBy = Session["UserName"].ToString();
        //        objDB.PayCOAID = int.Parse(ddlCOAPN.SelectedValue);
        //        objDB.DirectPaymentID = DirectPaymentID;
        //        objDB.AccountID = int.Parse(ddlAccounts.SelectedValue);
        //        objDB.PayType = ddlPayBy.SelectedValue;
        //        objDB.WOT = float.Parse(txtWOT.Value == "" ? "0" : txtWOT.Value);
        //        objDB.STWH = float.Parse(txtSTWH.Value == "" ? "0" : txtSTWH.Value);
        //        res = objDB.AddPOPay();

        //        if (res == "Paid")
        //        {
        //            objDB.TableName = "DirectPaymentPay";
        //            objDB.TableID = 0;
        //            objDB.ParentID = DirectPaymentID;
        //            objDB.Debit = float.Parse(txtAmountPN.Value) + objDB.WOT + objDB.STWH;
        //            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //            objDB.CreatedBy = Session["UserName"].ToString();
        //            objDB.SupplierID = Convert.ToInt32(ddlSupplier.SelectedValue);
        //            objDB.ChartOfAccountID = Convert.ToInt32(objDB.usp_TF_GetSupplierCOAIDByID());
        //            objDB.TransactionDetail = "Paid against DirectPayment# " + txtDirectPaymentCode.Value;
        //            objDB.AddTransactionHistoryDebit();



        //            objDB.TableName = "DirectPaymentPay";
        //            objDB.TableID = 0;
        //            objDB.ParentID = DirectPaymentID;
        //            objDB.Credit = float.Parse(txtAmountPN.Value);
        //            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //            objDB.CreatedBy = Session["UserName"].ToString();
        //            objDB.ChartOfAccountID = Convert.ToInt32(ddlCOAPN.SelectedValue);
        //            objDB.TransactionDetail = "Paid against DirectPayment# " + txtDirectPaymentCode.Value;
        //            objDB.AddTransactionHistoryCredit();


        //            objDB.TableName = "DirectPaymentPay";
        //            objDB.TableID = 0;
        //            objDB.ParentID = DirectPaymentID;
        //            objDB.Credit = objDB.WOT;
        //            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //            objDB.CreatedBy = Session["UserName"].ToString();
        //            objDB.Title = "WOT";
        //            objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetCOAQuickSetupBybYtITLE());
        //            objDB.TransactionDetail = "Paid against DirectPayment# " + txtDirectPaymentCode.Value;
        //            objDB.AddTransactionHistoryCredit();


        //            objDB.TableName = "DirectPaymentPay";
        //            objDB.TableID = 0;
        //            objDB.ParentID = DirectPaymentID;
        //            objDB.Credit = objDB.STWH;
        //            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //            objDB.CreatedBy = Session["UserName"].ToString();
        //            objDB.Title = "STWH";
        //            objDB.ChartOfAccountID = Convert.ToInt32(objDB.GetCOAQuickSetupBybYtITLE());
        //            objDB.TransactionDetail = "Paid against DirectPayment# " + txtDirectPaymentCode.Value;
        //            objDB.AddTransactionHistoryCredit();


        //            divAlertMsg.Visible = true;
        //            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
        //            pAlertMsg.InnerHtml = res;
        //            addBCVoucher(DirectPaymentID);
        //            getDirectPaymentDetails(DirectPaymentID);

        //        }
        //        else
        //        {
        //            divAlertMsg.Visible = true;
        //            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //            pAlertMsg.InnerHtml = res;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }
        //}

        //protected void btnGenVoucher_ServerClick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ResDirectPaymentnse.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reDirectPaymentrts/journal-voucher/generate-" + DirectPaymentID);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        //protected void btnPayVoucher_ServerClick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Session["TableName"] = "DirectPaymentPay";
        //        Session["BackLink"] = ("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/manage/purchase-orders/edit-purchase-order-" + DirectPaymentID);

        //        ResDirectPaymentnse.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/reDirectPaymentrts/payment-voucher/generate-" + DirectPaymentID);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

    }
}