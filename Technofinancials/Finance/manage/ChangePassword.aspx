﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Technofinancials.Finance.manage.ChangePassword" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <%
                if (Session["UserID"] == null)
                    Response.Redirect("/login");
            %>
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>



            <input type="hidden" id="hdnCompanyID" value="<% Response.Write(Session["CompanyID"]); %>" />
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">
                <asp:UpdatePanel ID="UpdPnl" runat="server">
                    <ContentTemplate>
                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <img src="/assets/images/hr-02.png" class="img-responsive tf-page-heading-img" />
                                    <h3 class="tf-page-heading-text">Update Password</h3>
                                </div>

                                	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                                <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                    <ContentTemplate>

                                        <div class="col-sm-4">
                                            <div class="pull-right flex">
                                                <%--<button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" class="tf-note-btn-new " value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>--%>
                                                <button class="tf-save-btn" "Save" runat="server" onserverclick="Unnamed_ServerClick" type="button" validationgroup="btnValidate"><i class="far fa-save"></i></button>
                                            </div>

                                        </div>



                                        <!-- Modal -->
                                        <div class="modal fade" id="notes-modal" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Notes</h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <p>
                                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                        </p>

                                                        <p>
                                                            <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                        </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr />
                                </div>
                            </div>
                            <div class="row ">
                                <div class="tab-content ">
                                    <div class="tab-pane active row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <h4>Current Password
                                                        <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtOldPassword" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                    <input class="form-control" id="txtOldPassword" placeholder="Current Password" type="password" runat="server" />
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <h4>New Password
                                                        <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtNewPassword" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                   <asp:RegularExpressionValidator ID="RegularExpressionValidatorStreet" runat="server" ErrorMessage="Password must contain atleaset 1 Special Charachter, 1 Number, 1 Capital Letter, 1 Small Letter and must be of 8 Charachters" ValidationGroup="btnValidate" Display="Dynamic" ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%^&*+]).{8,}$" ControlToValidate="txtNewPassword"></asp:RegularExpressionValidator></h4>
                                                    <input class="form-control" id="txtNewPassword" placeholder="New Password" type="password" runat="server" />
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <h4>Re-type New Password
                                                        <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtConfirmPassword" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /><asp:CompareValidator runat="server" ErrorMessage="Password Not Match" ControlToValidate="txtConfirmPassword" ControlToCompare="txtNewPassword" ValidationGroup="btnValidate" Display="Dynamic" ForeColor="Red" /></h4>
                                                    <input class="form-control" id="txtConfirmPassword" placeholder="New Password" type="password" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear-fix">&nbsp;</div>
                                     
                                    </div>
                                </div>
                            </div>
                        </section>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/UpdateUserPassword.js"></script>

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }
        </style>
    </form>
</body>
</html>
