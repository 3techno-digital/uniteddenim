﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Expense.aspx.cs" Inherits="Technofinancials.Finance.manage.Expense" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/Expenses.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Expenses</h3>
                        </div>

                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>
                                <div class="col-sm-4">
                                    <div class="pull-right flex">

                                        <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                        
                                            <button type="button" class="tf-save-btn" value="Print JV" "Print JV" id="btnGenVoucher" runat="server" onserverclick="btnGenVoucher_ServerClick"><i class="fa fa-file-pdf-o"></i></button>
                                        <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnSubmitGroup" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnSubmitGroup" type="button"><i class="fas fa-check-square"></i></button>
                                        <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnSubmitGroup" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class" "Delete" CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                        <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnSubmitGroup" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnSubmitGroup" type="button"><i class="fas fa-paper-plane"></i></button>
                                        <button class="tf-save-btn" "Reject & Disapproved" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnSubmitGroup" type="button"><i class="fas fa-thumbs-down"></i></button>


                                        <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSubmit_ServerClick1" validationgroup="btnSubmitGroup" type="button"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Voucher No
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" ControlToValidate="txtVoucherNo" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnSubmitGroup" ForeColor="Red" /></label>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator14" ControlToValidate="txtVoucherNo" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                        <input type="text" class="form-control" id="txtVoucherNo" disabled="disabled" runat="server" placeholder="Name" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Date
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnSubmitGroup" ForeColor="Red" /></label>
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                        <input type="text" class="form-control" placeholder="Date" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" name="companyShortName" id="txtDate" runat="server" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Title
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtTitle" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnSubmitGroup" ForeColor="Red" /></label>
                                       <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtTitle" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                        <input type="text" class="form-control" id="txtTitle" runat="server" placeholder="Name" />
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Select Account
                                                <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="ddl_COA" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnSubmitGroup" ForeColor="Red" /></label>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddl_COA" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                        
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control select2" ID="ddl_COA">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Spent At </label>
                                        
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control select2" ID="ddlSupplier">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Paid to
                                        </label>
                                        <input type="text" class="form-control" id="txtPaidTo" runat="server" placeholder="Name" />
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            <asp:CheckBox ID="chkBillable" runat="server" AutoPostBack="true" OnCheckedChanged="chkBillable_CheckedChanged" />
                                            Is Billable Expense 
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control select2" ID="ddlClients">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Voucher Type
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="ddlVoucherType" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnSubmitGroup" ForeColor="Red" />
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="ddlVoucherType" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                        </label>
                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control select2" ID="ddlVoucherType" OnSelectedIndexChanged="ddlVoucherType_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="Petty Cash">Petty Cash</asp:ListItem>
                                            <asp:ListItem Value="Bank">Bank</asp:ListItem>
                                            <asp:ListItem Value="JV">JV</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div id="bankContainer" runat="server">

                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Account 
                                        <asp:DropDownList runat="server" class="form-control select2" data-plugin="select2" ID="ddlAccount">
                                        </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Cheque Number 
                                        <input type="text" class="form-control" id="txtCheque" runat="server" placeholder="Cheque Number" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Reference Number  
                                        <input type="text" class="form-control" id="txtRefNo" runat="server" placeholder="Reference Number" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                Cheque Date
                                        <input type="text" class="form-control" placeholder="Date" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" name="Date" id="txtChequeDate" runat="server" />
                                        </div>
                                    </div>

                                </div>
                                <div id="jvContaiber" runat="server">

                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                From COA 
                                         <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control select2" ID="ddlFromCOA">
                                         </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="input-group input-group-lg">
                                            <label>
                                                To COA 
                                         <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control select2" ID="ddlToCOA">
                                         </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="input-group input-group-lg">
                                        <label>
                                            Net Ammount
                                            <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtAmount" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnSubmitGroup" ForeColor="Red" /></label>
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtAmount" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                        <input type="text" class="form-control" id="txtAmount" runat="server" value="0" placeholder="Net Amount" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h4>Description</h4>
                                        <textarea class="form-control" rows="3" name="Description" id="txtDescription" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-12">
                                    <h3>Items</h3>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:HiddenField ID="hdnRowNo" runat="server" />
                                            <asp:GridView ID="gvAccount" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvAccount_RowDataBound" OnRowUpdating="gvAccount_RowUpdating" OnRowCancelingEdit="gvAccount_RowCancelingEdit" OnRowEditing="gvAccount_RowEditing">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr. No">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                            <asp:HiddenField runat="server" ID="hdnItem" Value='<%# Eval("ExpenseItemID") %>' />
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <%--                          <asp:TemplateField HeaderText="Category">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblCategory" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                           
                                                            <FooterTemplate>
                                                                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control js-example-basic-single" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>--%>

                                                    <asp:TemplateField HeaderText="Item Code">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblItemCode" Text='<%# Eval("ItemCode") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlEditItem" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single">
                                                            </asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlItem" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single">
                                                            </asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Item Name  <span style='color:red !important;'>*</span>">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblItemName" Text='<%# Eval("ItemName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox runat="server" ID="txtEditItemName" CssClass="form-control" Text='<%# Eval("ItemName") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:RequiredFieldValidator runat="server" ID="reqValItemName" ControlToValidate="txtItemName" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                            <asp:TextBox runat="server" ID="txtItemName" CssClass="form-control"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Price <span style='color:red !important;'>*</span> ">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblPrice" Text='<%# Eval("Price") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox runat="server" ID="txtEditPrice" CssClass="form-control" Text='<%# Eval("Price") %>' AutoPostBack="true" OnTextChanged="txtEditPrice_TextChanged"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:RequiredFieldValidator runat="server" ID="reqValPrice" ControlToValidate="txtPrice" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />

                                                            <asp:TextBox runat="server" ID="txtPrice" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtPrice_TextChanged"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Quantity  <span style='color:red !important;'>*</span> ">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Tax") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox runat="server" ID="txtEditTax" CssClass="form-control" Text='<%# Eval("Tax") %>' AutoPostBack="true" OnTextChanged="txtEditPrice_TextChanged"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                           <asp:RequiredFieldValidator runat="server" ID="reqValTax" ControlToValidate="txtTax" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                            <asp:TextBox runat="server" ID="txtTax" CssClass="form-control" AutoPostBack="true" Text="0" OnTextChanged="txtPrice_TextChanged"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Net Amount">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblNetAmount" Text='<%# Eval("NetAmount") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox runat="server" Enabled="false" ID="txtEditNetAmount" CssClass="form-control" Text='<%# Eval("NetAmount") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox runat="server" Enabled="false" ID="txtNetAmount" CssClass="form-control"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="COA <span style='color:red !important;'>*</span>">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblCOA" Text='<%# Eval("COA_Title") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlEditCOA" runat="server"  CssClass="select2 form-control js-example-basic-single select2" data-plugin="select2">
                                                            </asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                             <asp:RequiredFieldValidator runat="server" ID="reqValCOA" InitialValue="0" ControlToValidate="ddlCOA" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                            <asp:DropDownList ID="ddlCOA" runat="server" CssClass="select2 form-control js-example-basic-single select2" data-plugin="select2">
                                                            </asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="false">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Button ID="Account_btnAdd" runat="server" CssClass="form-control" Text="Add" ValidationGroup="btnValidate" CausesValidation="true" OnClick="Account_btnAdd_Click"></asp:Button>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="Account_lnkRemove" runat="server" CommandArgument='<%# Eval("ExpenseItemID")%>' Text="Delete" OnCommand="Account_lnkRemove_Command"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>


                                        </ContentTemplate>
                                    </asp:UpdatePanel>


                                </div>
                            </div>

                            <%--Attachments--%>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="clearfix">&nbsp;</div>
                                    <h3>Attachments(Evidence)&nbsp;
                                            <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('updFiles').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                    </h3>
                                    <asp:FileUpload ID="updFiles" runat="server" ClientIDMode="Static" AllowMultiple="true" Style="display: none;" accept=".pdf,.doc,.docx" />
                                    <div class="clearfix">&nbsp;</div>
                                </div>


                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:GridView ID="gvFiles" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr. No">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Attachment">
                                                <ItemTemplate>
                                                    <a runat="server" id="lblTitle" href='<%# Eval("FilePath") %>' target="_blank">Attachment <%#Container.DataItemIndex+1 %></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveFile_Command"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>



                            </div>


                            <div class="clearfix">&nbsp;</div>
                
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .aspNetDisabled {
                width: 100% !important;
                background: #eeeeee;
                height: 38px;
                padding: 8px 16px;
                font-size: 15px;
                line-height: 1.3333333;
                border-radius: 6px;
                border-color: #ccc;
                outline: none;
                box-shadow: none;
            }

            .form-control {
                border-radius: 6px !important;
                height: 38px !important;
            }
        </style>
        <script>
        $("#updFiles").change(function () {
            __doPostBack('LinkUploadFiles', '');
        });
    </script>
    </form>
</body>
</html>

