﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SideBar.ascx.cs" Inherits="Technofinancials.Administrator.usercontrols.SideBar" %>
<aside id="menubar" class="menubar tf-sidebar">
    <div class="menubar-scroll">
        <div class="menubar-scroll-inner tf-sidebar-menu-items">
            <ul class="app-menu">
                <li class="tf-first-menu">
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/company-profile"); %>">
                        <img src="/assets/images/Companies.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Company Profile</span>
                    </a>
                </li>
                 <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/locations"); %>">
                        <img src="/assets/images/Child_Companies_white.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Locations</span>
                    </a>
                </li>
                <li class="has-submenu">
                    <a href="javascript:void(0)" class="submenu-toggle">
                        <img src="/assets/images/OrganizationalChart.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Organization Structure</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                    </a>
                    <ul class="submenu">
                        <li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/cost-center"); %>"><span class="menu-text">Cost Center</span></a></li>
                        <li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/departments"); %>"><span class="menu-text">Departments</span></a></li>
                        <li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/designations"); %>"><span class="menu-text">Designations</span></a></li>
                      <%--  <li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/job-descriptions"); %>"><span class="menu-text">Job Description</span></a></li>--%>
      <%--                  <li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/divisions"); %>"><span class="menu-text">Divisions</span></a></li>
                        <li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/teams"); %>"><span class="menu-text">Teams</span></a></li>--%>
                   <%--     <li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/workday-sheet"); %>"><span class="menu-text">Workday Integration Sheets   </span></a></li>--%>
                    </ul>
                </li>
                <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/users"); %>">
                        <img src="/assets/images/users.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Users</span>
                    </a>
                </li>
                <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/log"); %>">
                        <img src="/assets/images/users.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Audit Logs</span>
                    </a>
                </li>
            </ul>
            <!-- .app-menu -->
        </div>
        <!-- .menubar-scroll-inner -->
    </div>
    <div class="tf-vno">tf-v2.9 <%Response.Write(DateTime.Now.ToString("MM-dd-yyyy")); %></div>
    <!-- .menubar-scroll -->
</aside>

<!--========== END app aside -->
