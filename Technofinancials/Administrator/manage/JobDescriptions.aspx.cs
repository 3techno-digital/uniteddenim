﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.manage
{
	public partial class JobDescriptions : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/job-descriptions";

                BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
                clearFields();
                divAlertMsg.Visible = false;

                if (HttpContext.Current.Items["TitleID"] != null)
                {
                    getJobDesByID(Convert.ToInt32(HttpContext.Current.Items["TitleID"].ToString()));
                }
            }
        }

        private void clearFields()
        {
            txtName.Value = "";
            ddldesg.SelectedIndex = 0;
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void BindDepartmentDropDown(int companyID)
        {
            objDB.CompanyID = companyID;
            ddldesg.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
            ddldesg.DataTextField = "DesgTitle";
            ddldesg.DataValueField = "DesgID";
            ddldesg.DataBind();
            ddldesg.Items.Insert(0, new ListItem("--- None ---", "0"));
        }

        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
        }

        private void getJobDesByID(int JDID)
        {
            DataTable dt = new DataTable();
            objDB.JDID = JDID;
            dt = objDB.GetJobDescreptionByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
                    ddldesg.SelectedValue = dt.Rows[0]["DesgID"].ToString();
                    txtName.Value = dt.Rows[0]["Title"].ToString();
                    comment.Value = dt.Rows[0]["TitleDesc"].ToString();
                    code.Value = dt.Rows[0]["TitleCode"].ToString();
                }
            }
        }

        protected void btnAddDesg_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();

            objDB.DesgID = Convert.ToInt32(ddldesg.SelectedItem.Value);

            objDB.Name = txtName.Value;
            objDB.Description = comment.Value;
            objDB.Code = code.Value;
            string res = "";
            
            if (HttpContext.Current.Items["TitleID"] != null)
            {
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.JDID = Convert.ToInt32(HttpContext.Current.Items["TitleID"].ToString());

                res = objDB.UpdateJobDescByID();
            }
            else
            {
                objDB.CreatedBy = Session["UserName"].ToString();

                res = objDB.AddJobDescreption();
                clearFields();
            }

            if (res == "New Job Descreption Added" || res == "Job Descreption Updated")
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }
    }
}