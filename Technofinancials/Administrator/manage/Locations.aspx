﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Locations.aspx.cs" Inherits="Technofinancials.Administrator.manage.Locations" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary" onkeydown = "return (event.keyCode!=13)">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>



            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <h1 class="m-0 text-dark">Locations</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                    <ContentTemplate>
                                        <div style="text-align: right;">
<%--                                            <button type="button" class="AD_btn" data-toggle="modal" data-target="#notes-modal" value="Add Note">Note</button>--%>
                                            <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate"  type="button">Save</button>
                                            <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                        </div>
                                     
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- Modal -->
                <div class="modal fade M_set" id="notes-modal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="m-0 text-dark">Notes</h1>
                                <div class="add_new">
                                    <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                    <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                </div>
                            </div>
                            <div class="modal-body">
                                <p>
                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                </p>
                                <p>
                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>

                <section class="app-content">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="  ">
                                                        <label>Code <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtCode"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                        
                                                        <input type="text" class="form-control form-text" placeholder="Code" id="txtCode" runat="server">
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div class="col-md-6" style="display:none;">
                                            <div class="  ">
                                                <label>
                                                    CoA Code
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtLocationCode" ErrorMessage=" *"   Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                <input type="text" class="form-control" id="txtLocationCode" runat="server" placeholder="Location Code" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="  ">
                                                <label>
                                                    Name
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtName" ErrorMessage=" *"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                <input type="text" class="form-control" id="txtName" runat="server" placeholder="Name" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="  ">
                                                <label>
                                                    Prefix
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtPrefix" ErrorMessage=" *"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                <input type="text" class="form-control" id="txtPrefix" runat="server" placeholder="Prefix" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12" style="display:none;">
                                            <div class="  ">
                                                <label>Project Manger  <span style="color: red !important;">*</span></label>
                                                <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPM"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />--%>
                                                <input type="text" class="form-control form-text" placeholder="Project Manager" id="txtPM" runat="server">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="  ">
                                                <label>Email</label>
                                                <input type="email" class="form-control form-text" placeholder="Email" id="txtEmail" runat="server">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="  ">
                                                <label>Phone</label>
                                                <input class="form-control phone-no-val" placeholder="" maxlength="14" type="number" id="txtPhone" runat="server">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 form-group">
                                            <div class="checkbox checkbox-primary">
                                                <asp:CheckBox ID="chkCW" runat="server" />
                                                <label class="checkbox" for="chkCW">Mark Location as Central Warehouse</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                            <ContentTemplate>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="  ">
                                                        <label>Select Country <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ErrorMessage=" *" ControlToValidate="ddlCountries" InitialValue="0"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                        
                                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlCountries" AutoPostBack="true" OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged">
                                                            <asp:ListItem>--- Select Country ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="  ">
                                                        <label>Select State <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ErrorMessage=" *" ControlToValidate="ddlStates" InitialValue="0"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /> </label>
                                                        
                                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlStates" AutoPostBack="true" OnSelectedIndexChanged="ddlStates_SelectedIndexChanged">
                                                            <asp:ListItem>--- Select State ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="  ">
                                                        <label>Select City <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlCities" ErrorMessage=" *" InitialValue="0"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                        <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlCities" AutoPostBack="true" OnSelectedIndexChanged="ddlCities_SelectedIndexChanged">
                                                            <asp:ListItem>--- Select City ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div class="col-md-6 col-sm-6">
                                            <label>Zip / Postal Code</label>
                                            <div class="  ">
                                                <input type="Number" class="form-control form-text" placeholder="ZIP Code / Postal Code" id="txtZipCode" runat="server"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="  ">
                                                <label>Address Line 1 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtAddressLine1" ErrorMessage=" *"   Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                <input type="text" class="form-control form-text" placeholder="Address Line 1" id="txtAddressLine1" runat="server">

                                                
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <label>Address Line 2</label>
                                            <div class="  ">
                                                <input type="text" class="form-control form-text" placeholder="Address Line 2" id="txtAddressLine2" runat="server">
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-6">
                                            <div class="form-group">
                                                <h4>Description</h4>
                                                <textarea class="form-control" rows="3" name="Description" id="txtDescription" runat="server"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                            <span>
                                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                            </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </section>


                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
             input[type="radio"], input[type="checkbox"]{
                 display:none !important;
             }
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }
            .checkbox label{
                margin-top:46px;
            }
        </style>
    </form>
</body>
</html>

