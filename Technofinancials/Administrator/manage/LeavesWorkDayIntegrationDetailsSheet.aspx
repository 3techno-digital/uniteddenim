﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeavesWorkDayIntegrationDetailsSheet.aspx.cs" Inherits="Technofinancials.Administrator.manage.LeavesWorkDayIntegrationDetailsSheet" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>
		.table > thead:first-child > tr:first-child > th {
			font-size: 11px;
		}

		table#gv {
			width: 3000px;
		}

		div#gv_wrapper {
			margin-bottom: 20%;
		}

		div div#gv_wrapper::-webkit-scrollbar-thumb {
			background-color: #003780;
			border: 2px solid #003780;
			border-radius: 10px;
		}

		div div#gv_wrapper::-webkit-scrollbar {
			height: 10px;
			background-color: #ffffff;
		}

		div div#gv_wrapper::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
			background-color: #FFF;
		}

		@media only screen and (max-width: 992px) and (min-width: 768px) {
			div div#gv_wrapper {
				overflow-x: scroll;
				overflow-y: hidden;
				padding-bottom: 5rem;
			}
		}

		@media only screen and (max-width: 1024px) and (min-width: 993px) {
			div div#gv_wrapper {
				overflow-x: scroll;
				overflow-y: hidden;
				padding-bottom: 5rem;
			}
		}

		@media only screen and (max-width: 1280px) and (min-width: 800px) {
			div div#gv_wrapper {
				overflow-x: scroll;
				overflow-y: hidden;
				padding-bottom: 5rem;
			}
		}
	</style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">

			<div class="wrap">
				<asp:UpdatePanel runat="server">
					<ContentTemplate>
						<div class="content-header">
							<div class="container-fluid">
								<div class="row mb-2">
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
										<h1 class="m-0 text-dark">WorkDay Sheet Details</h1>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
										<div style="text-align: right;">
											<a class="AD_btn" id="btnBack" runat="server">Back</a>
										</div>
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.container-fluid -->
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
				<section class="app-content">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<asp:UpdatePanel ID="UpdatePanel4" runat="server">
								<ContentTemplate>
									<div class="form-group" id="divAlertMsg" runat="server">
										<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
											<span>
												<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
											</span>
											<p id="pAlertMsg" runat="server">
											</p>
										</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>
					</div>
					<div class="clear-fix">&nbsp;</div>
					<div class="row ">
						<div class="col-sm-12">
							<div class="tab-content ">
								<div class="tab-pane active row">
									<div class="col-sm-12 adminpanel-padding-bottom">
										<div class="fixed-table-wrap">
											<asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" ShowHeaderWhenEmpty="true" EmptyDataText="Records Not Found">
											</asp:GridView>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
			<div class="clear-fix">&nbsp;</div>
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>
	</form>
</body>
</html>