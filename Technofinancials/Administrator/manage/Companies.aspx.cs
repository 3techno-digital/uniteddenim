﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace Technofinancials.Administrator.manage
{
    public partial class Companies : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
       
        protected string logoPath
        {
            get
            {
                if (ViewState["logoPath"] != null)
                {
                    return (string)ViewState["logoPath"];
                }
                else
                {
                    return "/assets/images/3techno-Logo.png";
                }
            }
            set
            {
                ViewState["logoPath"] = value;
            }
        }




        protected int ParentCompanyID
        {
            get
            {
                if (ViewState["ParentCompanyID"] != null)
                {
                    return (int)ViewState["ParentCompanyID"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["ParentCompanyID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                BindCountriesDropDown();
                BindStatesDropDown(166);
                clearFields();
                divAlertMsg.Visible = false;
                DataTable ComCode = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                ComCode = objDB.GetNewCompanyCode(ref errorMsg);
                if (ComCode != null)
                {
                    if (ComCode.Rows.Count > 0)
                    {
                        txtCompnayCode.Value = ComCode.Rows[0][0].ToString();
                    }
                }
                if (HttpContext.Current.Items["CompanyID"] != null)
                {
                    SubComapny.Visible = false;
                    getCompanyByID(Convert.ToInt32(HttpContext.Current.Items["CompanyID"].ToString()));
                }
            }
        }

        private void getCompanyByID(int companyID)
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = companyID;
            dt = objDB.GetCompanyID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));
                    int countriesIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));
                    if (countriesIndex == -1)
                        countriesIndex = 0;
                    BindStatesDropDown(Convert.ToInt32(ddlCountries.Items[countriesIndex].Value));

                    ddlStates.SelectedIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["State"].ToString()));
                    int statesIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["State"].ToString()));
                    if (statesIndex == -1)
                        statesIndex = 0;

                    BindCitiesDropDown(Convert.ToInt32(ddlStates.Items[statesIndex].Value));

                    ddlCities.SelectedIndex = ddlCities.Items.IndexOf(ddlCities.Items.FindByText(dt.Rows[0]["City"].ToString()));

                    imgLogo.Src = dt.Rows[0]["CompanyLogo"].ToString();
                    imgLogo.Attributes["src"] = dt.Rows[0]["CompanyLogo"].ToString();
                    logoPath = dt.Rows[0]["CompanyLogo"].ToString();
                        

                    txtAddressLine1.Value = dt.Rows[0]["AddressLine1"].ToString();
                    txtAddressLine2.Value = dt.Rows[0]["AddressLine2"].ToString();
                    txtZIPCode.Value = dt.Rows[0]["ZIPCode"].ToString();

                    txtCompanyName.Value = dt.Rows[0]["CompanyName"].ToString();
                    txtCompanyShortName.Value = dt.Rows[0]["CompanyShortName"].ToString();
                    txtCompanyDescription.Value = dt.Rows[0]["CompanyDescription"].ToString();

                    txtCompnayNtn.Value = dt.Rows[0]["CompanyNTN"].ToString();
                    txtCompanyStrn.Value = dt.Rows[0]["CompanySTRN"].ToString();
                    txtCompanySecp.Value = dt.Rows[0]["CompanySECP"].ToString();
                    txtCompanyStn.Value = dt.Rows[0]["CompanySTN"].ToString();
                    txtCompanyArn.Value = dt.Rows[0]["CompnayARN"].ToString();
                    txtCompnayCode.Value = dt.Rows[0]["CompanyCode"].ToString();
                    Session["CEOName"] = dt.Rows[0]["CEOName"].ToString();
                    if (dt.Rows[0]["ParentCompanyID"].ToString() != "")
                        ParentCompanyID = Convert.ToInt32(dt.Rows[0]["ParentCompanyID"].ToString());

                }
            }
            Common.addlog("View", "Admin", "Location \"" + txtCompanyName.Value + "\" Viewed", "Companies", objDB.CompanyID);

        }

        private void clearFields()
        {
            ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText("Pakistan"));
            int countriesIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText("Pakistan"));
            if (countriesIndex == -1)
                countriesIndex = 0;
            BindStatesDropDown(Convert.ToInt32(ddlCountries.Items[countriesIndex].Value));

            ddlStates.SelectedIndex = 0;
            ddlCities.SelectedIndex = 0;

            txtAddressLine1.Value = string.Empty;
            txtAddressLine2.Value = string.Empty;
            txtZIPCode.Value = string.Empty;

            txtCompanyName.Value = string.Empty;
            txtCompanyShortName.Value = string.Empty;
            txtCompanyDescription.Value = string.Empty;

            txtCompnayNtn.Value = string.Empty;
            txtCompanyStrn.Value = string.Empty;
            txtCompanySecp.Value = string.Empty;
            txtCompanyStn.Value = string.Empty;
            txtCompanyArn.Value = string.Empty;
            Session["CEOName"] = null;

            txtCEOFirstName.Value = string.Empty;
            txtCEOLastName.Value = string.Empty;
            txtCEOMiddleName.Value = string.Empty;
            emailCEOAddress.Value = string.Empty;

            txtHRFirstName.Value = string.Empty;
            txtHRLastName.Value = string.Empty;
            txtHRMiddleName.Value = string.Empty;
            emailHRAddress.Value = string.Empty;

            DataTable ComCode = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ComCode = objDB.GetNewCompanyCode(ref errorMsg);
            if (ComCode != null)
            {
                if (ComCode.Rows.Count > 0)
                {
                    txtCompnayCode.Value = ComCode.Rows[0][0].ToString();
                }
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }


        private void BindCountriesDropDown()
        {
            ddlCountries.DataSource = objDB.GetAllCounties(ref errorMsg);
            ddlCountries.DataTextField = "Name";
            ddlCountries.DataValueField = "ID";
            ddlCountries.DataBind();
            ddlCountries.Items.Insert(0, new ListItem("--- Select Country ---", "0"));
            ddlCountries.SelectedValue = "166";
        }

        private void BindStatesDropDown(int CountryID)
        {
            objDB.CountryID = CountryID;
            ddlStates.DataSource = objDB.GetAllStatesByCountryID(ref errorMsg);
            ddlStates.DataTextField = "Name";
            ddlStates.DataValueField = "ID";
            ddlStates.DataBind();
            ddlStates.Items.Insert(0, new ListItem("--- Select State ---", "0"));
        }

        private void BindCitiesDropDown(int StateID)
        {
            objDB.StateID = StateID;
            ddlCities.DataSource = objDB.GetAllCitiesByStateID(ref errorMsg);
            ddlCities.DataTextField = "Name";
            ddlCities.DataValueField = "Name";
            ddlCities.DataBind();
            ddlCities.Items.Insert(0, new ListItem("--- Select City ---", "0"));
        }

        protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            BindCitiesDropDown(Convert.ToInt32(ddlStates.SelectedValue));

        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            BindStatesDropDown(Convert.ToInt32(ddlCountries.SelectedItem.Value));

        }

        protected void button_ServerClick(object sender, EventArgs e)
        {
            int SubCompID, DepID, GraID, DesgID, JobDesID, EmployeeID;
            string res = "";
            if (updLogo != null)
            {
                Random rand = new Random((int)DateTime.Now.Ticks);
                int randnum = 0;

                string fn = "";
                string exten = "";

                string destDir = Server.MapPath("~/assets/files/companies_logos/");
                randnum = rand.Next(1, 100000);
                fn = Common.RemoveSpecialCharacter(txtCompanyName.Value).ToLower().Replace(" ", "-") + "_" + randnum;

                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }

                string fname = Path.GetFileName(updLogo.PostedFile.FileName);
                if (fname != "")
                {
                    exten = Path.GetExtension(updLogo.PostedFile.FileName);
                    updLogo.PostedFile.SaveAs(destDir + fn + exten);
                    logoPath = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/companies_logos/" + fn + exten;
                }


            }

            objDB.CompanyName = txtCompanyName.Value;
            objDB.CompanyShortName = txtCompanyShortName.Value;
            objDB.ParentCompanyID = Convert.ToInt32(Session["CompnayID"]);
            objDB.CompanyDesc = txtCompanyDescription.Value;
            objDB.CompanyLogo = logoPath;
            objDB.CountryName = ddlCountries.SelectedItem.Text;
            objDB.StateName = ddlStates.SelectedItem.Text;
            objDB.CityName = ddlCities.SelectedItem.Text;
            objDB.AddressLine1 = txtAddressLine1.Value;
            objDB.AddressLine2 = txtAddressLine2.Value;
            objDB.ZIPCode = txtZIPCode.Value;
            objDB.CompanyNTN = txtCompnayNtn.Value;
            objDB.CompanySTRN = txtCompanyStrn.Value;
            objDB.CompanySECP = txtCompanySecp.Value;
            objDB.CompanySTN = txtCompanyStn.Value;
            objDB.CompanyARN = txtCompanyArn.Value;
            objDB.CompanyCode = txtCompnayCode.Value;
            Session["CompanyLogo"] = logoPath;
            if (HttpContext.Current.Items["CompanyID"] != null)
            {
                objDB.CEOName = Session["CEOName"].ToString();
                objDB.ParentCompanyID = ParentCompanyID;
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.CompanyID = int.Parse(HttpContext.Current.Items["CompanyID"].ToString());
                res = objDB.UpdateCompany();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "changeLogo('" + logoPath + "')", true);
            }
            else
            {

                objDB.CEOName = txtCEOFirstName.Value + " " + txtCEOMiddleName.Value + " " + txtCEOLastName.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.ParentCompanyID = int.Parse(Session["CompanyID"].ToString());
                SubCompID = int.Parse(objDB.AddCompany());

                objDB.CompanyID = SubCompID;
                objDB.WorkingShiftName = "Default";
                objDB.Monday = true;
                objDB.Tuesday = true;
                objDB.Wednesday = true;
                objDB.Thursday = true;
                objDB.Friday = true;
                objDB.Saturday = false;
                objDB.Sunday = false;

                objDB.Hours = "8";

                objDB.CreatedBy = Session["UserName"].ToString();
                int ShiftID = Convert.ToInt32(objDB.AddShift());


                objDB.TableName = "WorkingShifts";
                objDB.PrimaryColumnnName = "ShiftID";
                objDB.PrimaryColumnValue = ShiftID.ToString();
                objDB.UpdateColumnName = "ReviewedBy";
                objDB.UpdateColumnValue = Session["UserName"].ToString();
                objDB.DateColumnName = "ReviewedDate";
                objDB.DocStatus = "Reviewed";

                objDB.UpdateDocStatus();

                objDB.TableName = "WorkingShifts";
                objDB.PrimaryColumnnName = "ShiftID";
                objDB.PrimaryColumnValue = ShiftID.ToString();
                objDB.UpdateColumnName = "ApprovedBy";
                objDB.UpdateColumnValue = Session["UserName"].ToString();
                objDB.DateColumnName = "ApprovedDate";
                objDB.DocStatus = "Approved";

                objDB.UpdateDocStatus();

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = SubCompID;
                objDB.DeptName = "Directors";
                DepID = int.Parse(objDB.AddDepartment());

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = SubCompID;
                objDB.GradeName = "CEO";
                GraID = int.Parse(objDB.AddGrade());


                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.DesgTitle = "CEO";
                objDB.DeptID = DepID;
                DesgID = int.Parse(objDB.AddDesignation());

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.NodeTitle = "CEO";
                objDB.GradeID = GraID;
                objDB.DesgID = DesgID;
                objDB.ProbabtionTime = 0;
                objDB.BasicSalary = 0;
                JobDesID = int.Parse(objDB.AddNode());

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.NodeID = JobDesID;
                objDB.GradeID = GraID;
                objDB.EmployeeFName = txtCEOFirstName.Value;
                objDB.EmployeeMName = txtCEOMiddleName.Value;
                objDB.EmployeeLName = txtCEOLastName.Value;
                objDB.Email = emailCEOAddress.Value;
                objDB.EmployeeCode = objDB.GenerateEmployeeCode();
                objDB.ShiftID = ShiftID;
                EmployeeID = int.Parse(objDB.AddEmployeesByCode());

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.EmployeeID = EmployeeID;
                objDB.Email = emailCEOAddress.Value;
                objDB.AccessLevel = "Directors";

                objDB.TableName = "Employees";
                objDB.PrimaryColumnnName = "EmployeeID";
                objDB.PrimaryColumnValue = EmployeeID.ToString();
                objDB.UpdateColumnName = "ReviewedBy";
                objDB.UpdateColumnValue = Session["UserName"].ToString();
                objDB.DateColumnName = "ReviewedDate";
                objDB.DocStatus = "Reviewed";

                objDB.UpdateDocStatus();

                objDB.TableName = "Employees";
                objDB.PrimaryColumnnName = "EmployeeID";
                objDB.PrimaryColumnValue = EmployeeID.ToString();
                objDB.UpdateColumnName = "ApprovedBy";
                objDB.UpdateColumnValue = Session["UserName"].ToString();
                objDB.DateColumnName = "ApprovedDate";
                objDB.DocStatus = "Approved";

                objDB.UpdateDocStatus();


                int userID = int.Parse(objDB.AddUser());

                sendEmail(txtHRFirstName.Value + " " + txtHRLastName.Value, objDB.Email, objDB.AccessLevel);

                objDB.TableName = "Users";
                objDB.PrimaryColumnnName = "UserID";
                objDB.PrimaryColumnValue = userID.ToString();
                objDB.UpdateColumnName = "ReviewedBy";
                objDB.UpdateColumnValue = Session["UserName"].ToString();
                objDB.DateColumnName = "ReviewedDate";
                objDB.DocStatus = "Reviewed";

                objDB.UpdateDocStatus();

                objDB.TableName = "Users";
                objDB.PrimaryColumnnName = "UserID";
                objDB.PrimaryColumnValue = userID.ToString();
                objDB.UpdateColumnName = "ApprovedBy";
                objDB.UpdateColumnValue = Session["UserName"].ToString();
                objDB.DateColumnName = "ApprovedDate";
                objDB.DocStatus = "Approved";

                objDB.UpdateDocStatus();

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = SubCompID;
                objDB.DeptName = "Human Resource";
                DepID = int.Parse(objDB.AddDepartment());

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = SubCompID;
                objDB.GradeName = "Human Resource";
                GraID = int.Parse(objDB.AddGrade());


                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.DesgTitle = "Human Resource";
                objDB.DeptID = DepID;
                DesgID = int.Parse(objDB.AddDesignation());

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.NodeTitle = "Human Resource";
                objDB.GradeID = GraID;
                objDB.DesgID = DesgID;
                objDB.ProbabtionTime = 0;
                objDB.BasicSalary = 0;
                JobDesID = int.Parse(objDB.AddNode());

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.NodeID = JobDesID;
                objDB.GradeID = GraID;
                objDB.EmployeeFName = txtHRFirstName.Value;
                objDB.EmployeeMName = txtHRMiddleName.Value;
                objDB.EmployeeLName = txtHRLastName.Value;
                objDB.Email = emailHRAddress.Value;
                objDB.ShiftID = ShiftID;
                objDB.EmployeeCode = objDB.GenerateEmployeeCode();
                EmployeeID = int.Parse(objDB.AddEmployeesByCode());

                objDB.TableName = "Employees";
                objDB.PrimaryColumnnName = "EmployeeID";
                objDB.PrimaryColumnValue = EmployeeID.ToString();
                objDB.UpdateColumnName = "ReviewedBy";
                objDB.UpdateColumnValue = Session["UserName"].ToString();
                objDB.DateColumnName = "ReviewedDate";
                objDB.DocStatus = "Reviewed";

                objDB.UpdateDocStatus();

                objDB.TableName = "Employees";
                objDB.PrimaryColumnnName = "EmployeeID";
                objDB.PrimaryColumnValue = EmployeeID.ToString();
                objDB.UpdateColumnName = "ApprovedBy";
                objDB.UpdateColumnValue = Session["UserName"].ToString();
                objDB.DateColumnName = "ApprovedDate";
                objDB.DocStatus = "Approved";

                objDB.UpdateDocStatus();


                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.EmployeeID = EmployeeID;
                objDB.Email = emailHRAddress.Value;
                objDB.AccessLevel = "Human Resource";

                userID = int.Parse(objDB.AddUser());

                sendEmail(txtHRFirstName.Value + " " + txtHRLastName.Value, objDB.Email, objDB.AccessLevel);

                objDB.TableName = "Users";
                objDB.PrimaryColumnnName = "UserID";
                objDB.PrimaryColumnValue = userID.ToString();
                objDB.UpdateColumnName = "ReviewedBy";
                objDB.UpdateColumnValue = Session["UserName"].ToString();
                objDB.DateColumnName = "ReviewedDate";
                objDB.DocStatus = "Reviewed";

                objDB.UpdateDocStatus();

                objDB.TableName = "Users";
                objDB.PrimaryColumnnName = "UserID";
                objDB.PrimaryColumnValue = userID.ToString();
                objDB.UpdateColumnName = "ApprovedBy";
                objDB.UpdateColumnValue = Session["UserName"].ToString();
                objDB.DateColumnName = "ApprovedDate";
                objDB.DocStatus = "Approved";

                objDB.UpdateDocStatus();


                objDB.UserID = userID;
                objDB.DocName = "Shifts";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "Holidays";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();



                objDB.UserID = userID;
                objDB.DocName = "Loans";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "PromotionLetter";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "PayrollSheet";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "WarningLetter";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "OfferLetter";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();



                objDB.UserID = userID;
                objDB.DocName = "MailingList";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "Surveys";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "PerformanceAppraisals";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();

                objDB.UserID = userID;
                objDB.DocName = "Succession";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "Tests";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "Trainings";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "candidates";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "newVacancie";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();

                objDB.UserID = userID;
                objDB.DocName = "InterviewsAndTests";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();

                objDB.UserID = userID;
                objDB.DocName = "EmployeeLists";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();

                objDB.UserID = userID;
                objDB.DocName = "AttendanceSheet";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();

                objDB.UserID = userID;
                objDB.DocName = "PayRoll";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();


                objDB.UserID = userID;
                objDB.DocName = "ProvinentFund";
                objDB.isNormUser = false;
                objDB.isReviewer = true;
                objDB.isApprover = true;
                objDB.AddUserAccess();



                res = "New Company Added";
                objDB.ShiftID = ShiftID;
                objDB.usp_TF_AddWorkingShiftDaysClient();
                clearFields();
            }

            imgLogo.Src = logoPath;

            if (res == "New Company Added" || res == "Company Data Updated")
            {
                if (res == "New Company Added")
                {
                    //Common.addlog("Add", "Admin", "New Company \"" + objDB.CompanyName + "\" Added", "Companies"); 
                    Common.addlogNew("Add", "Admin", "New Company  \"" + objDB.CompanyName + "\" Added", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/company-profile", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/companies/edit-company-" + objDB.CompanyID, "New Company \"" + objDB.CompanyName + "\" Added", "Companies", "Directors", Convert.ToInt32(objDB.CompanyID));

                }
                if (res == "Company Data Updated")
                {
                    //Common.addlog("Update", "Admin", "Company \"" + objDB.CompanyName + "\" Updated", "Companies", objDB.CompanyID);
                    Common.addlogNew("Update", "Admin", "Company \"" + objDB.CompanyName + "\" Updated", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/company-profile", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/companies/edit-company-" + objDB.CompanyID, "Company \"" + objDB.CompanyName + "\" Updated", "Companies", "Directors", Convert.ToInt32(objDB.CompanyID));

                }
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }

        private void sendEmail(string Name, string UserName, string AccessLevel)
        {
            string returnMsg = "";
            try
            {
                DBQueries objDB = new DBQueries();
                string errorMsg = "";
                string file = "";
                string html = "";

                file = System.Web.HttpContext.Current.Server.MapPath("/assets/emails/NewUser.html");
                StreamReader sr = new StreamReader(file);
                FileInfo fi = new FileInfo(file);

                if (System.IO.File.Exists(file))
                {
                    html += sr.ReadToEnd();
                    sr.Close();
                }

                html = html.Replace("##CREATED_BY##", Session["UserName"].ToString());
                html = html.Replace("##NAME##", Name);
                html = html.Replace("##USER_NAME##", UserName);
                html = html.Replace("##ROLE##", AccessLevel);

                MailMessage mail = new MailMessage();
                mail.Subject = "User Account Credentials for Technofinancials";
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), "Techno Financials");
                mail.To.Add(UserName);
                mail.Body = html;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["NoReplySMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NoReplyPort"].ToString()));
                smtp.EnableSsl = true;
                NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["NoReplyPassword"].ToString());
                smtp.Credentials = netCre;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                returnMsg = ex.Message;
            }

        }



    }
}