﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;

namespace Technofinancials.Administrator.manage
{
    public partial class CostCenter : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/cost-center";

                divAlertMsg.Visible = false;

                if (HttpContext.Current.Items["CostCenterID"] != null)
                {
                    getCostCenterByID(Convert.ToInt32(HttpContext.Current.Items["CostCenterID"].ToString()));
                }
            }
        }

        private void getCostCenterByID(int CostCenterID)
        {
            DataTable dt = new DataTable();
            objDB.CostCenterID = CostCenterID;
            dt = objDB.GetCostCenterByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    TxtCode.Value = dt.Rows[0]["CostCenterID"].ToString();
                    txtName.Value = dt.Rows[0]["CostCenterName"].ToString();
                    comment.Value = dt.Rows[0]["CostCenterDesc"].ToString();
                }
            }
            Common.addlog("View", "Admin", "CostCenter \"" + txtName.Value + "\" Viewed", "CostCenters", objDB.CostCenterID);


        }

        private void clearFields()
        {
            txtName.Value = "";
            comment.Value = "";
            TxtCode.Value = "";
        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.CostCenterName = txtName.Value;
            objDB.CostCenterDesc = comment.Value;
			if (TxtCode.Value != "")
			{
                objDB.CostCenterID = Convert.ToInt32(TxtCode.Value);

            }
            else
			{
                objDB.CostCenterID = 0;

            }

            string res = "";
            if (HttpContext.Current.Items["CostCenterID"] != null)
            {
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.CostCenterID = Convert.ToInt32(HttpContext.Current.Items["CostCenterID"].ToString());
                res = objDB.UpdateCostCenter();
            }
            else
            {
                objDB.CreatedBy = Session["UserName"].ToString();
                res = objDB.AddCostCenter();

                int DepID = 0;

                if (int.TryParse(res, out DepID))
                {
                    objDB.CostCenterID = DepID;
                    res = "New Cost Center Added";
                    clearFields();
                }
                
            }
            if (res == "New Cost Center Added" || res == "Cost Center Updated")
            {
                if (res == "New Cost Center Added")
                {
                    Common.addlogNew("Add", "Directors", "CostCenter \"" + objDB.CostCenterName + "\" Added", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/cost-center", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/cost-center/edit-cost-center-" + objDB.CostCenterID, "New CostCenter \"" + objDB.CostCenterName + "\" Added", "CostCenter", "Directors", Convert.ToInt32(objDB.CostCenterID));
                }
                if (res == "Cost Center Updated")
                {
                    Common.addlogNew("Update", "Directors", "CostCenter \"" + objDB.CostCenterName + "\" Updated", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/cost-center", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/cost-center/edit-cost-center-" + objDB.CostCenterID, "CostCenter \"" + objDB.CostCenterName + "\" Updated", "CostCenter", "Directors", Convert.ToInt32(objDB.CostCenterID));

                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }
    }
}