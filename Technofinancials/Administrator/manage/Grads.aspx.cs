﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.manage
{
    public partial class Grads : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/grades";

                showFirstRow = false;
                srNo = 1;

                //BindCompaniesDropDown();
                clearFields();
                divAlertMsg.Visible = false;
                dtGeneralKPIS = createKPIsTable();
                BindKPIsTable();


                dtGradeLeaves = null;
                GLsrNo = 1;
                dtGradeLeaves = createGreadLeavesTable();
                showGLFirstRow = false;
                TotalNoOfLeaves = 0;
                BindGreadLeavesTable();



                if (HttpContext.Current.Items["GradeID"] != null)
                {
                    getGradeByID(Convert.ToInt32(HttpContext.Current.Items["GradeID"].ToString()));
                }
            }

        }

        private void getGradeByID(int gardeID)
        {
            DataTable dt = new DataTable();
            objDB.GradeID = gardeID;
            dt = objDB.GetGradeByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    //ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(dt.Rows[0]["CompanyID"].ToString()));
                    txtName.Value = dt.Rows[0]["GradeName"].ToString();
                    getGeneralKPISByGradeID(Convert.ToInt32(dt.Rows[0]["GradeID"].ToString()));
                    BindKPIsTable();
                    getGradeLeavesByGradeID(Convert.ToInt32(dt.Rows[0]["GradeID"].ToString()));
                    BindGreadLeavesTable();
                }
            }
            Common.addlog("View", "Admin", "Grade \"" + txtName.Value + "\" Viewed", "Grades", objDB.GradeID);

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void clearFields()
        {
            txtName.Value = "";
            //ddlCompany.SelectedIndex = 0;
        }


        public bool CheckEntries()
        {
            bool IsValid = true;
            dtGeneralKPIS = (DataTable)ViewState["dtGeneralKPIS"] as DataTable;
            dtGradeLeaves = (DataTable)ViewState["dtGradeLeaves"] as DataTable;

            if (dtGeneralKPIS != null)
            {
                if (dtGeneralKPIS.Rows.Count > 0)
                {
                    if (dtGeneralKPIS.Rows[0][1].ToString() == "")
                    {
                        dtGeneralKPIS.Rows[0].Delete();
                        dtGeneralKPIS.AcceptChanges();
                    }
                    if (dtGeneralKPIS.Rows.Count <= 0)
                    {
                        IsValid = false;
                    }
                }
                else
                {
                    IsValid = false;
                }
            }
            else
            {
                IsValid = false;
            }

            if (dtGradeLeaves != null)
            {
                if (dtGradeLeaves.Rows.Count > 0)
                {
                    if (dtGradeLeaves.Rows[0][1].ToString() == "")
                    {
                        dtGradeLeaves.Rows[0].Delete();
                        dtGradeLeaves.AcceptChanges();
                    }
                    if (dtGradeLeaves.Rows.Count <= 0)
                    {
                        IsValid = false;
                    }
                }
                else
                {
                    IsValid = false;
                }
            }
            else
            {
                IsValid = false;
            }

            return IsValid;
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            int gradeID = 0;
            bool isNew = false;
            string res = "";

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.GradeName = txtName.Value;
            if (CheckEntries())
            {

                dtGeneralKPIS = (DataTable)ViewState["dtGeneralKPIS"] as DataTable;
                dtGradeLeaves = (DataTable)ViewState["dtGradeLeaves"] as DataTable;
                //if (dtGradeLeaves.Rows[0]["SrNo"].ToString() == "" && dtGeneralKPIS.Rows[0]["SrNo"].ToString() == "")
                //{

                //}

                if (HttpContext.Current.Items["GradeID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    gradeID = Convert.ToInt32(HttpContext.Current.Items["GradeID"].ToString());
                    objDB.GradeID = gradeID;
                    res = objDB.UpdateGrade();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    //gradeID = Convert.ToInt32(objDB.AddGrade());
                    res = objDB.AddGrade();
                    isNew = true;
                }
                if (int.TryParse(res, out gradeID))
                {
                    res = "New Grade Added";
                    objDB.GradeID = gradeID;
                    objDB.DeletedBy = Session["UserName"].ToString();
                    objDB.DeleteGradeKPIByGradeID();

                    if (dtGeneralKPIS != null)
                    {
                        if (dtGeneralKPIS.Rows.Count > 0)
                        {
                            if (dtGeneralKPIS.Rows[0]["SrNo"].ToString() == "")
                            {
                                dtGeneralKPIS.Rows[0].Delete();
                                dtGeneralKPIS.AcceptChanges();
                            }

                            for (int i = 0; i < dtGeneralKPIS.Rows.Count; i++)
                            {
                                objDB.GradeID = Convert.ToInt32(gradeID);
                                objDB.KPIDesc = dtGeneralKPIS.Rows[i]["KPIDesc"].ToString();
                                objDB.KPIType = dtGeneralKPIS.Rows[i]["KPIType"].ToString();
                                objDB.CreatedBy = Session["UserName"].ToString();
                                objDB.AddGradeKPI();

                            }
                        }
                    }


                    objDB.GradeID = gradeID;
                    objDB.DeletedBy = Session["UserName"].ToString();
                    objDB.DeleteLeavesByGradeID();

                    if (dtGradeLeaves != null)
                    {
                        if (dtGradeLeaves.Rows.Count > 0)
                        {
                            if (dtGradeLeaves.Rows[0]["SrNo"].ToString() == "")
                            {
                                dtGradeLeaves.Rows[0].Delete();
                                dtGradeLeaves.AcceptChanges();
                            }

                            for (int i = 0; i < dtGradeLeaves.Rows.Count; i++)
                            {
                                objDB.GradeID = Convert.ToInt32(gradeID);
                                objDB.LeaveTitle = dtGradeLeaves.Rows[i]["LeaveTitle"].ToString();
                                objDB.NoOfLeaves = int.Parse(dtGradeLeaves.Rows[i]["NoOfLeaves"].ToString());
                                objDB.CreatedBy = Session["UserName"].ToString();
                                objDB.AddGradeLeaves();
                            }
                        }
                    }
                    if (isNew)
                    {
                        srNo = 1;
                        ViewState["dtGeneralKPIS"] = null;
                        BindKPIsTable();
                        ViewState["dtGradeLeaves"] = null;
                        GLsrNo = 1;
                        BindGreadLeavesTable();
                    }

                }
            }
            else
            {
                res = "Please add data in both tables";
            }

            if (res == "New Grade Added" || res == "Grade Updated")
            {
                if (res == "New Grade Added")
                {
                    //Common.addlog("Add", "Admin", "New Grade \"" + objDB.GradeName + "\" Added", "Grades");
                    Common.addlogNew("Add", "Admin", "New Grade \"" + objDB.GradeName + "\" Updated", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/grades", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/grades/edit-grade-" + objDB.GradeID, "New Grade \"" + objDB.GradeName + "\" Added", "Grades", "Directors", Convert.ToInt32(objDB.GradeID));

                }
                if (res == "Grade Updated")
                {
                    //Common.addlog("Update", "Admin", "Grade \"" + objDB.GradeName + "\" Updated", "Grades", objDB.GradeID);
                    Common.addlogNew("Update", "Admin", "Grade \"" + objDB.GradeName + "\" Updated", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/grades", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/grades/edit-grade-" + objDB.GradeID, "Grade \"" + objDB.GradeName + "\" Updated", "Grades", "Directors", Convert.ToInt32(objDB.GradeID));

                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }

        private DataTable dtGeneralKPIS
        {
            get
            {
                if (ViewState["dtGeneralKPIS"] != null)
                {
                    return (DataTable)ViewState["dtGeneralKPIS"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtGeneralKPIS"] = value;
            }
        }
        private bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }
        private DataTable createKPIsTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("KPIType");
            dt.Columns.Add("KPIDesc");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindKPIsTable()
        {
            if (ViewState["dtGeneralKPIS"] == null)
            {
                dtGeneralKPIS = createKPIsTable();
                ViewState["dtGeneralKPIS"] = dtGeneralKPIS;
            }

            gv.DataSource = dtGeneralKPIS;
            gv.DataBind();


            if (showFirstRow)
                gv.Rows[0].Visible = true;
            else
                gv.Rows[0].Visible = false;

            gv.UseAccessibleHeader = true;
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = srNo.ToString();
            }
        }
        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindKPIsTable();
        }
        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindKPIsTable();
        }
        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            dtGeneralKPIS.Rows[index].SetField(1, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditKPIType")).SelectedItem.Text);
            dtGeneralKPIS.Rows[index].SetField(2, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditDesc")).Text);
            dtGeneralKPIS.AcceptChanges();
            ViewState["dtGeneralKPIS"] = dtGeneralKPIS;
            gv.EditIndex = -1;
            BindKPIsTable();
        }
        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            srNo = 0;
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtGeneralKPIS.Rows.Count; i++)
            {
                if (dtGeneralKPIS.Rows[i][0].ToString() == delSr)
                {
                    dtGeneralKPIS.Rows[i].Delete();
                    dtGeneralKPIS.AcceptChanges();
                }
            }
            for (int i = 0; i < dtGeneralKPIS.Rows.Count; i++)
            {
                dtGeneralKPIS.Rows[i].SetField(0, i + 1);
                dtGeneralKPIS.AcceptChanges();
                srNo = srNo + 1;
            }
            if (dtGeneralKPIS.Rows.Count < 1)
            {
                DataRow dr = dtGeneralKPIS.NewRow();
                dtGeneralKPIS.Rows.Add(dr);
                dtGeneralKPIS.AcceptChanges();
                showFirstRow = false;
            }
            if (showFirstRow)
                srNo = dtGeneralKPIS.Rows.Count + 1;
            else
                //srNo = 1;
            ViewState["dtGeneralKPIS"] = dtGeneralKPIS;
            BindKPIsTable();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            DataRow dr = dtGeneralKPIS.NewRow();
            dr["SrNo"] = srNo.ToString();
            dr["KPIType"] = ((DropDownList)gv.FooterRow.FindControl("ddlKPIType")).SelectedItem.Value;
            dr["KPIDesc"] = ((TextBox)gv.FooterRow.FindControl("txtDesc")).Text;
            dtGeneralKPIS.Rows.Add(dr);
            dtGeneralKPIS.AcceptChanges();
            ViewState["dtGeneralKPIS"] = dtGeneralKPIS;

            srNo += 1;
            BindKPIsTable();
            ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
        }
        private void getGeneralKPISByGradeID(int gradeID)
        {
            DataTable dt = new DataTable();
            objDB.GradeID = gradeID;
            dt = objDB.GetGeneralKPIsByGradeID(ref errorMsg);
            dtGeneralKPIS = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    showFirstRow = true;
                    srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }
        }


        private DataTable dtGradeLeaves
        {
            get
            {
                if (ViewState["dtGradeLeaves"] != null)
                {
                    return (DataTable)ViewState["dtGradeLeaves"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtGradeLeaves"] = value;
            }
        }
        private bool showGLFirstRow
        {
            get
            {
                if (ViewState["showGLFirstRow"] != null)
                {
                    return (bool)ViewState["showGLFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showGLFirstRow"] = value;
            }
        }
        protected int GLsrNo
        {
            get
            {
                if (ViewState["GLsrNo"] != null)
                {
                    return (int)ViewState["GLsrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["GLsrNo"] = value;
            }
        }

        protected int TotalNoOfLeaves
        {
            get
            {
                if (ViewState["TotalNoOfLeaves"] != null)
                {
                    return (int)ViewState["TotalNoOfLeaves"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["TotalNoOfLeaves"] = value;
            }
        }

        private DataTable createGreadLeavesTable()
        {   
            DataTable dt = new DataTable();
            //dt.Columns.Add("SrNo");
            dt.Columns.Add("LeaveTitle");
            dt.Columns.Add("NoOfLeaves");
            dt.AcceptChanges();

            DataColumn workColumn = dt.Columns.Add("SrNo", typeof(Int32));
            workColumn.AutoIncrement = true;
            //dt.Columns.Add(workColumn);
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindGreadLeavesTable()
        {
            if (ViewState["dtGradeLeaves"] == null)
            {
                dtGradeLeaves = createGreadLeavesTable();
                ViewState["dtGradeLeaves"] = dtGradeLeaves;
            }

            gvAttendance.DataSource = dtGradeLeaves;
            gvAttendance.DataBind();
            TotalNoOfLeaves = 0;
            for (int i = 0; i < dtGradeLeaves.Rows.Count; i++)
            {
                if (dtGradeLeaves.Rows[i]["NoOfLeaves"].ToString() != "")
                {
                    TotalNoOfLeaves += Convert.ToInt32(dtGradeLeaves.Rows[i]["NoOfLeaves"].ToString());
                }

            };

            if (showGLFirstRow)
                gvAttendance.Rows[0].Visible = true;
            else
                gvAttendance.Rows[0].Visible = false;

            gvAttendance.UseAccessibleHeader = true;
            gvAttendance.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gvAttendance_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = GLsrNo.ToString();
            }
        }
        protected void gvAttendance_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvAttendance.EditIndex = e.NewEditIndex;
            BindGreadLeavesTable();
        }
        protected void gvAttendance_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvAttendance.EditIndex = -1;
            BindGreadLeavesTable();
        }
        protected void gvAttendance_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            if (dtGradeLeaves.Rows[index]["NoOfLeaves"].ToString() != "")
            {
                TotalNoOfLeaves = TotalNoOfLeaves - int.Parse(dtGradeLeaves.Rows[index]["NoOfLeaves"].ToString());
            }

            TotalNoOfLeaves = TotalNoOfLeaves + Convert.ToInt32(((TextBox)gvAttendance.Rows[e.RowIndex].FindControl("txtEditNoOfLeaves")).Text.ToString());
            if (TotalNoOfLeaves <= 365)
            {

                dtGradeLeaves.Rows[index].SetField(1, ((TextBox)gvAttendance.Rows[e.RowIndex].FindControl("txtEditLeaveTitle")).Text);
                dtGradeLeaves.Rows[index].SetField(2, ((TextBox)gvAttendance.Rows[e.RowIndex].FindControl("txtEditNoOfLeaves")).Text);
                dtGradeLeaves.AcceptChanges();

                gvAttendance.EditIndex = -1;
                BindGreadLeavesTable();
            }
            else
            {
                TotalNoOfLeaves = TotalNoOfLeaves - Convert.ToInt32(((TextBox)gvAttendance.Rows[e.RowIndex].FindControl("txtEditNoOfLeaves")).Text.ToString());
                TotalNoOfLeaves = TotalNoOfLeaves + Convert.ToInt32(dtGradeLeaves.Rows[index]["NoOfLeaves"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "Leaves can not be greater then 365";

            }
        }
        protected void GLlnkRemove_Command(object sender, CommandEventArgs e)
        {
            GLsrNo = 0;
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtGradeLeaves.Rows.Count; i++)
            {
                if (dtGradeLeaves.Rows[i][0].ToString() == delSr)
                {
                    if (dtGradeLeaves.Rows[i]["NoOfLeaves"].ToString() != "")
                    {
                        TotalNoOfLeaves = TotalNoOfLeaves - Convert.ToInt32(dtGradeLeaves.Rows[i]["NoOfLeaves"].ToString());

                    }

                    dtGradeLeaves.Rows[i].Delete();
                    dtGradeLeaves.AcceptChanges();
                }
            }
            for (int i = 0; i < dtGradeLeaves.Rows.Count; i++)
            {
                dtGradeLeaves.Rows[i].SetField(0, i + 1);
                dtGradeLeaves.AcceptChanges();
                GLsrNo = GLsrNo + 1;
            }
            if (dtGradeLeaves.Rows.Count < 1)
            {
                DataRow dr = dtGradeLeaves.NewRow();
                dtGradeLeaves.Rows.Add(dr);
                dtGradeLeaves.AcceptChanges();
                showGLFirstRow = false;
            }
            if (showGLFirstRow)
                GLsrNo = dtGradeLeaves.Rows.Count + 1;
            else
                //GLsrNo = 1;

            BindGreadLeavesTable();
        }
        protected void GLbtnAdd_Click(object sender, EventArgs e)
        {
            if (((TextBox)gvAttendance.FooterRow.FindControl("txtNoOfLeaves")).Text.ToString() != "")
            {
                TotalNoOfLeaves = TotalNoOfLeaves + Convert.ToInt32(((TextBox)gvAttendance.FooterRow.FindControl("txtNoOfLeaves")).Text.ToString());
            }

            if (TotalNoOfLeaves <= 365)
            {
                DataRow dr = dtGradeLeaves.NewRow();
                dr["SrNo"] = GLsrNo.ToString();
                dr["LeaveTitle"] = ((TextBox)gvAttendance.FooterRow.FindControl("txtLeaveTitle")).Text;
                dr["NoOfLeaves"] = ((TextBox)gvAttendance.FooterRow.FindControl("txtNoOfLeaves")).Text;
                dtGradeLeaves.Rows.Add(dr);
                dtGradeLeaves.AcceptChanges();
                GLsrNo += 1;
                BindGreadLeavesTable();
                ((Label)gvAttendance.FooterRow.FindControl("txtSrNo")).Text = GLsrNo.ToString();
            }
            else
            {
                TotalNoOfLeaves = TotalNoOfLeaves - Convert.ToInt32(((TextBox)gvAttendance.FooterRow.FindControl("txtNoOfLeaves")).Text.ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "Leaves can not be greater then 365";
            }

        }
        private void getGradeLeavesByGradeID(int gradeID)
        {
            DataTable dt = new DataTable();
            objDB.GradeID = gradeID;
            dt = objDB.GetAllLeavesByGradeID(ref errorMsg);
            dtGradeLeaves = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    showGLFirstRow = true;
                    GLsrNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }
        }


    }
}