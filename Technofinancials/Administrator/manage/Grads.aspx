﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Grads.aspx.cs" Inherits="Technofinancials.Administrator.manage.Grads" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        @media only screen and (max-width: 991px) and (min-width: 768px) {
            table#gvAttendance tr th:nth-child(1) {
                width: 9% !important;
            }

            table#gvAttendance tr th:nth-child(2) {
                width: 23% !important;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            table#gvAttendance tr th:nth-child(1) {
                width: 9% !important;
            }

            table#gvAttendance tr th:nth-child(2) {
                width: 24% !important;
            }
        }

        @media only screen and (max-width: 1280px) and (min-width: 800px) {
            table#gvAttendance tr th:nth-child(1) {
                width: 9% !important;
            }

            table#gvAttendance tr th:nth-child(2) {
                width: 26% !important;
            }
        }
                @media only screen and (max-width: 1440px) and (min-width: 800px) {
            table#gvAttendance tr th:nth-child(1) {
                width: 10% !important;
            }

            table#gvAttendance tr th:nth-child(2) {
                width: 25% !important;
            }
        }
                                @media only screen and (max-width: 1600px) and (min-width: 1441px) {
        div#UpdatePanel2 table#gvAttendance tr th:nth-child(1) {
            width: 11% !important;
        }

        div#UpdatePanel2 table#gvAttendance tr th:nth-child(2) {
            width: 28% !important;
        }
        }



        .tf-back-btn {
            background-color: #575757;
            padding: 10px 10px 10px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

        .select2-container {
            width: 100% !important;
        }

        .alert {
            margin-top: 0px !important;
        }

        .table-bordered > thead > tr > th {
            border-top: none !important;
        }

        .table {
            margin-top: -1rem;
        }

        .tf-back-btn i {
            color: #fff !important;
        }

        .content-header.second_heading .container-fluid {
            padding-left: 0px;
        }

        .content-header.second_heading h1 {
            margin-left: 0px !important;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">
                <%--     <asp:UpdatePanel ID="UpdPnl" runat="server">
                    <ContentTemplate>--%>

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Grades</h1>
                            </div>
                            <!-- /.col -->
                            <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                <ContentTemplate>
                                    <div class="col-sm-4">
                                        <div style="text-align: right;">
                                            <button class="AD_btn" runat="server" id="btnSave" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                            <a id="btnBack" runat="server" class="AD_btn">Back</a>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>





                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <h4>Name <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                            <input class="form-control" id="txtName" placeholder="Grade Name" type="text" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>


                    <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h1 class="m-0 text-dark">Accountabilities and Responsibilities</h1>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <section class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="col-lg-12 no-pad">
                                            <asp:GridView ID="gv" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gv_RowDataBound" OnRowUpdating="gv_RowUpdating" OnRowCancelingEdit="gv_RowCancelingEdit" OnRowEditing="gv_RowEditing">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr. No">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Type  <span style='color:red !important;'>*</span>">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblKPIType" Text='<%# Eval("KPIType") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlEditKPIType" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidateARU" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlEditKPIType" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single" SelectedValue='<%# Bind("KPIType") %>'>
                                                                <asp:ListItem Value="Accountability">Accountability</asp:ListItem>
                                                                <asp:ListItem Value="Responsibility">Responsibility</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlKPIType" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidateARA" ForeColor="Red"></asp:RequiredFieldValidator>

                                                            <asp:DropDownList ID="ddlKPIType" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single">
                                                                <asp:ListItem Value="Accountability">Accountability</asp:ListItem>
                                                                <asp:ListItem Value="Responsibility">Responsibility</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description  <span style='color:red !important;'>*</span>">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblDesc" Text='<%# Eval("KPIDesc") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEditDesc" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidateARU" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            <asp:TextBox runat="server" ID="txtEditDesc" CssClass="form-control" Text='<%# Eval("KPIDesc") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDesc" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidateARA" ForeColor="Red"></asp:RequiredFieldValidator>

                                                            <asp:TextBox runat="server" ID="txtDesc" CssClass="form-control"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="false">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" ValidationGroup="btnValidateARU" CommandName="Update" Text="Update"></asp:LinkButton>
                                                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit" Csssclass="edit-class"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Button ID="btnAdd" runat="server" CssClass=" AD_stock form-control" Text="Add" ValidationGroup="btnValidateARA" OnClick="btnAdd_Click"></asp:Button>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemove_Command"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <!-- /.container-fluid -->
                    </section>





                    <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h1 class="m-0 text-dark">Leaves</h1>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <section class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <div class="col-lg-12 no-pad">
                                            <asp:GridView ID="gvAttendance" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvAttendance_RowDataBound" OnRowUpdating="gvAttendance_RowUpdating" OnRowCancelingEdit="gvAttendance_RowCancelingEdit" OnRowEditing="gvAttendance_RowEditing">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr. No">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Title   <span style='color:red !important;'>*</span>">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblLeaveTitle" Text='<%# Eval("LeaveTitle") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtEditLeaveTitle" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidateLU" ForeColor="Red"></asp:RequiredFieldValidator>

                                                            <asp:TextBox runat="server" ID="txtEditLeaveTitle" CssClass="form-control" Text='<%# Eval("LeaveTitle") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtLeaveTitle" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidateLA" ForeColor="Red"></asp:RequiredFieldValidator>

                                                            <asp:TextBox runat="server" ID="txtLeaveTitle" CssClass="form-control AphabetOnly"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="No Of Leaves  <span style='color:red !important;'>*</span> ">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblNoOfLeaves" Text='<%# Eval("NoOfLeaves") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtEditNoOfLeaves" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateLU" ForeColor="Red"></asp:RequiredFieldValidator>

                                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditNoOfLeaves" CssClass="form-control" Text='<%# Eval("NoOfLeaves") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtNoOfLeaves" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateLA" ForeColor="Red"></asp:RequiredFieldValidator>

                                                            <asp:TextBox runat="server" TextMode="Number" Text="0" ID="txtNoOfLeaves" CssClass="form-control"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ShowHeader="false">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" ValidationGroup="btnValidateLU" Text="Update"></asp:LinkButton>
                                                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" CssClass="edit-class" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Button ID="btnAdd" runat="server" CssClass="AD_stock form-control" Text="Add" ValidationGroup="btnValidateLA" OnClick="GLbtnAdd_Click"></asp:Button>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%# Eval("SrNo")%>' CssClass="delete-class" Text="Delete" OnCommand="GLlnkRemove_Command"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <!-- /.container-fluid -->
                    </section>
                </section>
                <%-- </ContentTemplate>
                </asp:UpdatePanel>--%>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <!-- Modal -->

    </form>
</body>
</html>
