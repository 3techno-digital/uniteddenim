﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.manage
{
    public partial class Users : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            { 
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/users";
                BindEmployeesDropDown(Convert.ToInt32(Session["CompanyID"]));

                tblHR.Visible = false;
                tblProcrument.Visible = false;
                tblCRM.Visible = false;
                clearFields();
                divAlertMsg.Visible = false;
                tblOM.Visible = false;
                tblCustom.Visible = false;
                tblFAM.Visible = false;
                tblFinance.Visible = false;
                tblCRMGeneral.Visible = false;
                if (HttpContext.Current.Items["UserID"] != null)
                {
                    //UserID = Convert.ToInt32(HttpContext.Current.Items["UserID"].ToString());
                    getUserByID(Convert.ToInt32(HttpContext.Current.Items["UserID"].ToString()));
                }
            }
        }

        private void getUserByID(int UserID)
        {
            DataTable dt = new DataTable();
            objDB.UserID = UserID;
            dt = objDB.GetUserDetailsByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    //ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(dt.Rows[0]["CompanyID"].ToString()));

                    //if (ddlCompany.SelectedIndex == -1)
                    //    ddlCompany.SelectedIndex = 0;
                    BindEmployeesDropDown(Convert.ToInt32(Session["CompanyID"]));
                    ddlEmployees.SelectedIndex = ddlEmployees.Items.IndexOf(ddlEmployees.Items.FindByValue(dt.Rows[0]["EmployeeID"].ToString()));
                    txtName.Value = dt.Rows[0]["Email"].ToString();
                    ddlAccessLevels.SelectedIndex = ddlAccessLevels.Items.IndexOf(ddlAccessLevels.Items.FindByValue(dt.Rows[0]["UserAccess"].ToString()));

                    ddlAccessLevels_SelectedIndexChanged(null, null);
                    //if (dt.Rows[0]["UserAccess"].ToString() == "Directors")
                    //{
                    //    tblAdministrators.Visible = true;
                    //}

                    GetUserAccess(dt.Rows[0]["UserID"].ToString());
                }
            }
            Common.addlog("View", "Admin", "User \"" + txtName.Value + "\" Viewed", "Users", objDB.UserID);



        }

        private void GetUserAccess(string UserID)
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.UserID = Convert.ToInt32(UserID);
            dt = objDB.GetUserAccessByUserID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        if (dt.Rows[i]["DocName"].ToString() == "OM All Pages")
                        {
                            OMAllPages_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            OMAllPages_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            OMAllPages_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }


                        if (dt.Rows[i]["DocName"].ToString() == "candidates")
                        {
                            candidates_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            candidates_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            candidates_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }


                        if (dt.Rows[i]["DocName"].ToString() == "newVacancie")
                        {
                            newVacancie_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            newVacancie_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            newVacancie_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "PolicyProcedure")
                        {
                            PolicyProcedures_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            PolicyProcedures_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            PolicyProcedures_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "InterviewsAndTests")
                        {
                            InterviewsAndTests_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            InterviewsAndTests_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            InterviewsAndTests_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }


                        if (dt.Rows[i]["DocName"].ToString() == "EmployeeLists")
                        {
                            EmployeeLists_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            EmployeeLists_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            EmployeeLists_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }


                        if (dt.Rows[i]["DocName"].ToString() == "AttendanceSheet")
                        {
                            AttendanceSheet_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            AttendanceSheet_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            AttendanceSheet_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "Announcement")
                        {
                            Announcement_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Announcement_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Announcement_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "OtherExpenses")
                        {
                            OtherExpenses_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            OtherExpenses_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            OtherExpenses_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }


                        if (dt.Rows[i]["DocName"].ToString() == "Device")
                        {
                            Device_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Device_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Device_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "DeviceUsers")
                        {
                            DeviceUsers_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            DeviceUsers_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            DeviceUsers_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "PayRoll")
                        {
                            PayRoll_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            PayRoll_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            PayRoll_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "ProvinentFund")
                        {
                            ProvinentFund_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            ProvinentFund_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            ProvinentFund_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "Trainings")
                        {
                            Trainings_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Trainings_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Trainings_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "Tests")
                        {
                            Tests_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Tests_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Tests_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "Succession")
                        {
                            Succession_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Succession_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Succession_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "PerformanceAppraisals")
                        {
                            PerformanceAppraisals_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            PerformanceAppraisals_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            PerformanceAppraisals_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }


                        if (dt.Rows[i]["DocName"].ToString() == "Surveys")
                        {
                            Surveys_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Surveys_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Surveys_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "MailingList")
                        {
                            MailingList_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            MailingList_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            MailingList_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        if (dt.Rows[i]["DocName"].ToString() == "OfferLetter")
                        {
                            OfferLetter_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            OfferLetter_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            OfferLetter_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "WarningLetter")
                        {
                            WarningLetter_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            WarningLetter_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            WarningLetter_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }


                        if (dt.Rows[i]["DocName"].ToString() == "PayrollSheet")
                        {
                            PayrollSheet_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            PayrollSheet_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            PayrollSheet_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "PromotionLetter")
                        {
                            PromotionLetter_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            PromotionLetter_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            PromotionLetter_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }


                        if (dt.Rows[i]["DocName"].ToString() == "Loans")
                        {
                            Loans_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Loans_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Loans_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "Holidays")
                        {
                            Holidays_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Holidays_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Holidays_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "Shifts")
                        {
                            Shifts_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Shifts_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Shifts_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["DocName"].ToString() == "Purchase Requisite")
                        {
                            pr_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            pr_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            pr_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Asset Transfer Report")
                        {
                            atr_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            atr_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            atr_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Delivery Note")
                        {
                            dn_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            dn_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            dn_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Tax Rate")
                        {
                            tax_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            tax_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            tax_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Project")
                        {
                            project_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            project_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            project_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Categories")
                        {
                            cat_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            cat_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            cat_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Items")
                        {
                            item_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            item_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            item_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Request For Quotes")
                        {
                            rfq_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            rfq_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            rfq_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Quote Comparision Report")
                        {
                            qcr_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            qcr_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            qcr_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Purchase Order")
                        {
                            po_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            po_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            po_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Good Receive Note")
                        {
                            grn_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            grn_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            grn_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Material Return Note")
                        {
                            mrn_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            mrn_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            mrn_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Debit Note")
                        {
                            dbn_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            dbn_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            dbn_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "VDMS")
                        {
                            vdms_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            vdms_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            vdms_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Purchase Plan")
                        {
                            pp_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            pp_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            pp_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "BusinessDivision")
                        {
                            BusinessDivision_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            BusinessDivision_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            BusinessDivision_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "ProductAndServices")
                        {
                            ProductAndServices_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            ProductAndServices_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            ProductAndServices_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Clients")
                        {
                            Clients_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Clients_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Clients_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Leads")
                        {
                            Leads_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Leads_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Leads_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "AccountDepreciation")
                        {
                            AccountDepreciation_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            AccountDepreciation_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            AccountDepreciation_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "Asset")
                        {
                            Asset_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Asset_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Asset_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "AssetDisposal")
                        {
                            AssetDisposal_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            AssetDisposal_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            AssetDisposal_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "AssetRevalution")
                        {
                            AssetRevalution_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            AssetRevalution_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            AssetRevalution_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "AssetTransfer")
                        {
                            AssetTransfer_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            AssetTransfer_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            AssetTransfer_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "AssetType")
                        {
                            AssetType_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            AssetType_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            AssetType_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "ChartOfAccount")
                        {
                            ChartOfAccount_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            ChartOfAccount_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            ChartOfAccount_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "CWIP")
                        {
                            CWIP_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            CWIP_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            CWIP_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "Location")
                        {
                            Location_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Location_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Location_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "ParentCategory")
                        {
                            ParentCategory_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            ParentCategory_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            ParentCategory_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "PoliciesAndProcedures")
                        {
                            PoliciesAndProcedures_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            PoliciesAndProcedures_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            PoliciesAndProcedures_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "TaxCategory")
                        {
                            TaxCategory_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            TaxCategory_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            TaxCategory_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "TaxDepreciation")
                        {
                            TaxDepreciation_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            TaxDepreciation_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            TaxDepreciation_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }



                        else if (dt.Rows[i]["DocName"].ToString() == "Invoice")
                        {
                            Invoice_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Invoice_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Invoice_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Quotation")
                        {
                            Quotation_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Quotation_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Quotation_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "CreditNote")
                        {
                            CreditNote_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            CreditNote_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            CreditNote_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "StatementSharing")
                        {
                            StatementSharing_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            StatementSharing_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            StatementSharing_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "ProductServices")
                        {
                            ProductServices_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            ProductServices_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            ProductServices_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "CDMS")
                        {
                            CDMS_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            CDMS_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            CDMS_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "PurchaseOrder")
                        {
                            PurchaseOrder_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            PurchaseOrder_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            PurchaseOrder_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "DebitNote")
                        {
                            DebitNote_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            DebitNote_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            DebitNote_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Expenses")
                        {
                            Expenses_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Expenses_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Expenses_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "PayrollManagement")
                        {
                            PayrollManagement_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            PayrollManagement_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            PayrollManagement_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "Accounts")
                        {
                            Accounts_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Accounts_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Accounts_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "TransferMoney")
                        {
                            TransferMoney_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            TransferMoney_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            TransferMoney_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        else if (dt.Rows[i]["DocName"].ToString() == "BankReconciliation")
                        {
                            BankReconciliation_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            BankReconciliation_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            BankReconciliation_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "PettyCashManagement")
                        {
                            PettyCashManagement_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            PettyCashManagement_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            PettyCashManagement_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }


                        else if (dt.Rows[i]["DocName"].ToString() == "ConfigureCOA")
                        {
                            ConfigureCOA_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            ConfigureCOA_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            ConfigureCOA_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "OpeningBalance")
                        {
                            OpeningBalance_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            OpeningBalance_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            OpeningBalance_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "MultiCurrency")
                        {
                            MultiCurrency_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            MultiCurrency_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            MultiCurrency_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        else if (dt.Rows[i]["DocName"].ToString() == "Tax")
                        {
                            Tax2_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Tax2_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Tax2_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }



                    }
                }
            }

            objDB.UserID = Convert.ToInt32(UserID);
            dt = objDB.GetCustomAccessByUserID(ref errorMsg);   // change
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        if (dt.Rows[i]["UserAccess"].ToString() == "Directors")
                        {
                            Administrator_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["UserAccess"].ToString() == "OM")
                        {
                            OM_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            OM_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            OM_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["UserAccess"].ToString() == "IT")
                        {
                            IT_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            IT_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            IT_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }
                        if (dt.Rows[i]["UserAccess"].ToString() == "Fixed Asset")
                        {
                            FixedAsset_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            FixedAsset_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            FixedAsset_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["UserAccess"].ToString() == "Procrument")
                        {
                            ProcrumentManagement_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            ProcrumentManagement_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            ProcrumentManagement_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["UserAccess"].ToString() == "Human Resource")
                        {
                            PeopleManagement_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            PeopleManagement_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            PeopleManagement_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["UserAccess"].ToString() == "CRM")
                        {
                            CRM_isPreparer.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            CRM_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            CRM_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["UserAccess"].ToString() == "Finance")
                        {
                            Finance_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Finance_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Finance_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                        if (dt.Rows[i]["UserAccess"].ToString() == "Production")
                        {
                            Production_isNormUser.Checked = string.Equals(dt.Rows[i]["isPreparer"].ToString(), "True") ? true : false;
                            Production_isReviewer.Checked = string.Equals(dt.Rows[i]["isReviewer"].ToString(), "True") ? true : false;
                            Production_isApprover.Checked = string.Equals(dt.Rows[i]["isApprover"].ToString(), "True") ? true : false;
                        }

                    }

                }
            }

        }

        private void clearFields()
        {
            txtName.Value = "";
            //ddlCompany.SelectedIndex = 0;
            ddlEmployees.SelectedIndex = 0;
        }

        //private void BindCompaniesDropDown()
        //{
        //    objDB.ParentCompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
        //    ddlCompany.DataSource = objDB.GetCompaniesByParentID(ref errorMsg);
        //    ddlCompany.DataTextField = "CompanyName";
        //    ddlCompany.DataValueField = "CompanyID";
        //    ddlCompany.DataBind();
        //    ddlCompany.Items.Insert(0, new ListItem("--- Select Company ---", "0"));
        //}

        private void BindEmployeesDropDown(int CompanyID)
        {
            ddlEmployees.Items.Clear();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlEmployees.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
            ddlEmployees.DataTextField = "EmployeeName";
            ddlEmployees.DataValueField = "EmployeeID";
            ddlEmployees.DataBind();
            ddlEmployees.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }


        private void sendEmail(string Email)
        {
            try
            {
                string html = "";
                html = "Dear User,<br/>Your account is created on <b>Technofinancials</b>. Yours credentials are given bellow. <br /><br /><b>User Name: </b>" + Email + "<br /><b>Password :</b> 1234<br /> <br /><b>Note: </b> Kindly Login on https://www.technofinancials.com And Update your Password <br /> ";

                MailMessage mail = new MailMessage();
                mail.Subject = "New Account Created";
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString(), "Techno Financials");
                mail.To.Add(Email);
                mail.Body = html;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SenderSMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SenderPort"].ToString()));
                smtp.EnableSsl = true;
                NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["SenderPassword"].ToString());
                smtp.Credentials = netCre;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            int UserID = 0;
            objDB.EmployeeID = Convert.ToInt32(ddlEmployees.SelectedItem.Value);
            objDB.Email = txtName.Value;
            objDB.AccessLevel = ddlAccessLevels.SelectedItem.Value;

            string res = "";
            if (HttpContext.Current.Items["UserID"] != null)
            {
                UserID = Convert.ToInt32(HttpContext.Current.Items["UserID"]);
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.UserID = UserID;
                res = objDB.UpdateUser();
            }
            else
            {
                sendEmails();
                objDB.CreatedBy = Session["UserName"].ToString();
                UserID = Convert.ToInt32(objDB.AddUser());
                res = "New User Added";
                sendEmail(txtName.Value);
                clearFields();
            }

            objDB.UserID = UserID;
            objDB.DelCustomAccessByUserID();   // change

            objDB.UserID = UserID;
            objDB.DocName = "OM";
            objDB.isNormUser = OM_isNormUser.Checked ? true : false;
            objDB.isReviewer = OM_isReviewer.Checked ? true : false;
            objDB.isApprover = OM_isApprover.Checked ? true : false;
            objDB.AddCustomAccess();

            objDB.UserID = UserID;
            objDB.DocName = "Directors";
            objDB.isNormUser = false;
            objDB.isReviewer = false;
            objDB.isApprover = Administrator_isApprover.Checked ? true : false;
            objDB.AddCustomAccess();    //change

            objDB.UserID = UserID;
            objDB.DocName = "CRM";
            objDB.isNormUser = CRM_isPreparer.Checked ? true : false;
            objDB.isReviewer = CRM_isReviewer.Checked ? true : false;
            objDB.isApprover = CRM_isApprover.Checked ? true : false;
            objDB.AddCustomAccess();


            objDB.UserID = UserID;
            objDB.DocName = "IT";
            objDB.isNormUser = IT_isPreparer.Checked ? true : false;
            objDB.isReviewer = IT_isReviewer.Checked ? true : false;
            objDB.isApprover = IT_isApprover.Checked ? true : false;
            objDB.AddCustomAccess();

            objDB.UserID = UserID;
            objDB.DocName = "Fixed Asset";
            objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
            objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
            objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
            objDB.AddCustomAccess();

            objDB.UserID = UserID;
            objDB.DocName = "Procrument";
            objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
            objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
            objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
            objDB.AddCustomAccess();

            objDB.UserID = UserID;
            objDB.DocName = "Human Resource";
            objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
            objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
            objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
            objDB.AddCustomAccess();  //change

            objDB.UserID = UserID;
            objDB.DocName = "Finance";
            objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
            objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
            objDB.isApprover = Finance_isApprover.Checked ? true : false;
            objDB.AddCustomAccess();  //change

            objDB.UserID = UserID;
            objDB.DocName = "CRM General";
            objDB.isNormUser = crmGeneral_isNormUser.Checked ? true : false;
            objDB.isReviewer = crmGeneral_isReviewer.Checked ? true : false;
            objDB.isApprover = crmGeneral_isApprover.Checked ? true : false;
            objDB.AddCustomAccess();

            objDB.UserID = UserID;
            objDB.DocName = "Production";
            objDB.isNormUser = Production_isNormUser.Checked ? true : false;
            objDB.isReviewer = Production_isReviewer.Checked ? true : false;
            objDB.isApprover = Production_isApprover.Checked ? true : false;
            objDB.AddCustomAccess();  //change

            /////////////////////////////////////

            objDB.UserID = UserID;
            objDB.DelUserAccessByUserID();

            if (ddlAccessLevels.SelectedIndex == 1)
            {

                objDB.UserID = UserID;
                objDB.DocName = "CRM General";
                objDB.isNormUser = crmGeneral_isNormUser.Checked ? true : false;
                objDB.isReviewer = crmGeneral_isReviewer.Checked ? true : false;
                objDB.isApprover = crmGeneral_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Production";
                objDB.isNormUser = Production_isNormUser.Checked ? true : false;
                objDB.isReviewer = Production_isReviewer.Checked ? true : false;
                objDB.isApprover = Production_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "OM All Pages";
                objDB.isNormUser = OM_isNormUser.Checked ? true : false;
                objDB.isReviewer = OM_isReviewer.Checked ? true : false;
                objDB.isApprover = OM_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Tax";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "MultiCurrency";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "OpeningBalance";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "ConfigureCOA";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "PettyCashManagement";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "BankReconciliation";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "TransferMoney";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Accounts";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "PayrollManagement";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Expenses";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "DebitNote";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "CDMS";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "PurchaseOrder";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "ProductServices";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "StatementSharing";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "CreditNote";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Quotation";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Invoice";
                objDB.isNormUser = Finance_isNormUser.Checked ? true : false;
                objDB.isReviewer = Finance_isReviewer.Checked ? true : false;
                objDB.isApprover = Finance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();




                objDB.UserID = UserID;
                objDB.DocName = "BusinessDivision";
                objDB.isNormUser = CRM_isPreparer.Checked ? true : false;
                objDB.isReviewer = CRM_isReviewer.Checked ? true : false;
                objDB.isApprover = CRM_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "ProductAndServices";
                objDB.isNormUser = CRM_isPreparer.Checked ? true : false;
                objDB.isReviewer = CRM_isReviewer.Checked ? true : false;
                objDB.isApprover = CRM_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Clients";
                objDB.isNormUser = CRM_isPreparer.Checked ? true : false;
                objDB.isReviewer = CRM_isReviewer.Checked ? true : false;
                objDB.isApprover = CRM_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Leads";
                objDB.isNormUser = CRM_isPreparer.Checked ? true : false;
                objDB.isReviewer = CRM_isReviewer.Checked ? true : false;
                objDB.isApprover = CRM_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "Purchase Plan";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Shifts";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();




                objDB.UserID = UserID;
                objDB.DocName = "Holidays";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "Loans";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "PromotionLetter";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();




                objDB.UserID = UserID;
                objDB.DocName = "PayrollSheet";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "WarningLetter";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "OfferLetter";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "MailingList";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Surveys";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "PerformanceAppraisals";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Succession";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "Tests";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();




                objDB.UserID = UserID;
                objDB.DocName = "Trainings";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "candidates";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "newVacancie";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "PolicyProcedure";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "InterviewsAndTests";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "EmployeeLists";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "AttendanceSheet";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Announcement";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "OtherExpenses";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Device";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "DeviceUsers";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "PayRoll";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "ProvinentFund";
                objDB.isNormUser = PeopleManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = PeopleManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PeopleManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                //objDB.UserID = UserID;
                //objDB.DocName = "Job Descriptions";
                //objDB.isNormUser = jds_isPreparer.Checked ? true : false;
                //objDB.isReviewer = jds_isReviewer.Checked ? true : false;
                //objDB.isApprover = jds_isApprover.Checked ? true : false;
                //objDB.AddUserAccess();

                //objDB.UserID = UserID;
                //objDB.DocName = "Department";
                //objDB.isNormUser = dept_isPreparer.Checked ? true : false;
                //objDB.isReviewer = dept_isReviewer.Checked ? true : false;
                //objDB.isApprover = dept_isApprover.Checked ? true : false;
                //objDB.AddUserAccess();

                //objDB.UserID = UserID;
                //objDB.DocName = "Designation";
                //objDB.isNormUser = desg_isPreparer.Checked ? true : false;
                //objDB.isReviewer = desg_isReviewer.Checked ? true : false;
                //objDB.isApprover = desg_isApprover.Checked ? true : false;
                //objDB.AddUserAccess();

                //objDB.UserID = UserID;
                //objDB.DocName = "Grades";
                //objDB.isNormUser = grade_isPreparer.Checked ? true : false;
                //objDB.isReviewer = grade_isReviewer.Checked ? true : false;
                //objDB.isApprover = grade_isApprover.Checked ? true : false;
                //objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Purchase Requisite";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Asset Transfer Report";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Delivery Note";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Tax Rate";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Project";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Categories";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Items";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Request For Quotes";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Quote Comparision Report";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Purchase Order";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Good Receive Note";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Material Return Note";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Debit Note";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "VDMS";
                objDB.isNormUser = ProcrumentManagement_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProcrumentManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = ProcrumentManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();
                objDB.UserID = UserID;
                objDB.DocName = "AccountDepreciation";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Asset";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "AssetDisposal";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "AssetRevalution";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "AssetTransfer";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "AssetType";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "ChartOfAccount";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "CWIP";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Location";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "ParentCategory";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "PoliciesAndProcedures";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "TaxCategory";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "TaxDepreciation";
                objDB.isNormUser = FixedAsset_isPreparer.Checked ? true : false;
                objDB.isReviewer = FixedAsset_isReviewer.Checked ? true : false;
                objDB.isApprover = FixedAsset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();
            }
            else
            {
                objDB.UserID = UserID;
                objDB.DocName = "CRM General";
                objDB.isNormUser = crmGeneralAllPages_isNormUser.Checked ? true : false;
                objDB.isReviewer = crmGeneralAllPages_isReviewer.Checked ? true : false;
                objDB.isApprover = crmGeneralAllPages_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "OM All Pages";
                objDB.isNormUser = OMAllPages_isNormUser.Checked ? true : false;
                objDB.isReviewer = OMAllPages_isReviewer.Checked ? true : false;
                objDB.isApprover = OMAllPages_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Tax";
                objDB.isNormUser = Tax2_isNormUser.Checked ? true : false;
                objDB.isReviewer = Tax2_isReviewer.Checked ? true : false;
                objDB.isApprover = Tax2_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "MultiCurrency";
                objDB.isNormUser = MultiCurrency_isNormUser.Checked ? true : false;
                objDB.isReviewer = MultiCurrency_isReviewer.Checked ? true : false;
                objDB.isApprover = MultiCurrency_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "OpeningBalance";
                objDB.isNormUser = OpeningBalance_isNormUser.Checked ? true : false;
                objDB.isReviewer = OpeningBalance_isReviewer.Checked ? true : false;
                objDB.isApprover = OpeningBalance_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "ConfigureCOA";
                objDB.isNormUser = ConfigureCOA_isNormUser.Checked ? true : false;
                objDB.isReviewer = ConfigureCOA_isReviewer.Checked ? true : false;
                objDB.isApprover = ConfigureCOA_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "PettyCashManagement";
                objDB.isNormUser = PettyCashManagement_isNormUser.Checked ? true : false;
                objDB.isReviewer = PettyCashManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PettyCashManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "BankReconciliation";
                objDB.isNormUser = BankReconciliation_isNormUser.Checked ? true : false;
                objDB.isReviewer = BankReconciliation_isReviewer.Checked ? true : false;
                objDB.isApprover = BankReconciliation_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "TransferMoney";
                objDB.isNormUser = TransferMoney_isNormUser.Checked ? true : false;
                objDB.isReviewer = TransferMoney_isReviewer.Checked ? true : false;
                objDB.isApprover = TransferMoney_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Accounts";
                objDB.isNormUser = Accounts_isNormUser.Checked ? true : false;
                objDB.isReviewer = Accounts_isReviewer.Checked ? true : false;
                objDB.isApprover = Accounts_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "PayrollManagement";
                objDB.isNormUser = PayrollManagement_isNormUser.Checked ? true : false;
                objDB.isReviewer = PayrollManagement_isReviewer.Checked ? true : false;
                objDB.isApprover = PayrollManagement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Expenses";
                objDB.isNormUser = Expenses_isNormUser.Checked ? true : false;
                objDB.isReviewer = Expenses_isReviewer.Checked ? true : false;
                objDB.isApprover = Expenses_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "DebitNote";
                objDB.isNormUser = DebitNote_isNormUser.Checked ? true : false;
                objDB.isReviewer = DebitNote_isReviewer.Checked ? true : false;
                objDB.isApprover = DebitNote_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "CDMS";
                objDB.isNormUser = CDMS_isNormUser.Checked ? true : false;
                objDB.isReviewer = CDMS_isReviewer.Checked ? true : false;
                objDB.isApprover = CDMS_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "PurchaseOrder";
                objDB.isNormUser = PurchaseOrder_isNormUser.Checked ? true : false;
                objDB.isReviewer = PurchaseOrder_isReviewer.Checked ? true : false;
                objDB.isApprover = PurchaseOrder_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "ProductServices";
                objDB.isNormUser = ProductServices_isNormUser.Checked ? true : false;
                objDB.isReviewer = ProductServices_isReviewer.Checked ? true : false;
                objDB.isApprover = ProductServices_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "StatementSharing";
                objDB.isNormUser = StatementSharing_isNormUser.Checked ? true : false;
                objDB.isReviewer = StatementSharing_isReviewer.Checked ? true : false;
                objDB.isApprover = StatementSharing_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "CreditNote";
                objDB.isNormUser = CreditNote_isNormUser.Checked ? true : false;
                objDB.isReviewer = CreditNote_isReviewer.Checked ? true : false;
                objDB.isApprover = CreditNote_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Quotation";
                objDB.isNormUser = Quotation_isNormUser.Checked ? true : false;
                objDB.isReviewer = Quotation_isReviewer.Checked ? true : false;
                objDB.isApprover = Quotation_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Invoice";
                objDB.isNormUser = Invoice_isNormUser.Checked ? true : false;
                objDB.isReviewer = Invoice_isReviewer.Checked ? true : false;
                objDB.isApprover = Invoice_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "BusinessDivision";
                objDB.isNormUser = BusinessDivision_isNormUser.Checked ? true : false;
                objDB.isReviewer = BusinessDivision_isReviewer.Checked ? true : false;
                objDB.isApprover = BusinessDivision_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "ProductAndServices";
                objDB.isNormUser = ProductAndServices_isNormUser.Checked ? true : false;
                objDB.isReviewer = ProductAndServices_isReviewer.Checked ? true : false;
                objDB.isApprover = ProductAndServices_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Clients";
                objDB.isNormUser = Clients_isNormUser.Checked ? true : false;
                objDB.isReviewer = Clients_isReviewer.Checked ? true : false;
                objDB.isApprover = Clients_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Leads";
                objDB.isNormUser = Leads_isNormUser.Checked ? true : false;
                objDB.isReviewer = Leads_isReviewer.Checked ? true : false;
                objDB.isApprover = Leads_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "Purchase Plan";
                objDB.isNormUser = pp_isNormUser.Checked ? true : false;
                objDB.isReviewer = pp_isReviewer.Checked ? true : false;
                objDB.isApprover = pp_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Shifts";
                objDB.isNormUser = Shifts_isPreparer.Checked ? true : false;
                objDB.isReviewer = Shifts_isReviewer.Checked ? true : false;
                objDB.isApprover = Shifts_isApprover.Checked ? true : false;
                objDB.AddUserAccess();




                objDB.UserID = UserID;
                objDB.DocName = "Holidays";
                objDB.isNormUser = Holidays_isPreparer.Checked ? true : false;
                objDB.isReviewer = Holidays_isReviewer.Checked ? true : false;
                objDB.isApprover = Holidays_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "Loans";
                objDB.isNormUser = Loans_isPreparer.Checked ? true : false;
                objDB.isReviewer = Loans_isReviewer.Checked ? true : false;
                objDB.isApprover = Loans_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "PromotionLetter";
                objDB.isNormUser = PromotionLetter_isPreparer.Checked ? true : false;
                objDB.isReviewer = PromotionLetter_isReviewer.Checked ? true : false;
                objDB.isApprover = PromotionLetter_isApprover.Checked ? true : false;
                objDB.AddUserAccess();




                objDB.UserID = UserID;
                objDB.DocName = "PayrollSheet";
                objDB.isNormUser = PayrollSheet_isPreparer.Checked ? true : false;
                objDB.isReviewer = PayrollSheet_isReviewer.Checked ? true : false;
                objDB.isApprover = PayrollSheet_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "WarningLetter";
                objDB.isNormUser = WarningLetter_isPreparer.Checked ? true : false;
                objDB.isReviewer = WarningLetter_isReviewer.Checked ? true : false;
                objDB.isApprover = WarningLetter_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "OfferLetter";
                objDB.isNormUser = OfferLetter_isPreparer.Checked ? true : false;
                objDB.isReviewer = OfferLetter_isReviewer.Checked ? true : false;
                objDB.isApprover = OfferLetter_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "MailingList";
                objDB.isNormUser = MailingList_isPreparer.Checked ? true : false;
                objDB.isReviewer = MailingList_isReviewer.Checked ? true : false;
                objDB.isApprover = MailingList_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Surveys";
                objDB.isNormUser = Surveys_isPreparer.Checked ? true : false;
                objDB.isReviewer = Surveys_isReviewer.Checked ? true : false;
                objDB.isApprover = Surveys_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "PerformanceAppraisals";
                objDB.isNormUser = PerformanceAppraisals_isPreparer.Checked ? true : false;
                objDB.isReviewer = PerformanceAppraisals_isReviewer.Checked ? true : false;
                objDB.isApprover = PerformanceAppraisals_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Succession";
                objDB.isNormUser = Succession_isPreparer.Checked ? true : false;
                objDB.isReviewer = Succession_isReviewer.Checked ? true : false;
                objDB.isApprover = Succession_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "Tests";
                objDB.isNormUser = Tests_isPreparer.Checked ? true : false;
                objDB.isReviewer = Tests_isReviewer.Checked ? true : false;
                objDB.isApprover = Tests_isApprover.Checked ? true : false;
                objDB.AddUserAccess();




                objDB.UserID = UserID;
                objDB.DocName = "Trainings";
                objDB.isNormUser = Trainings_isPreparer.Checked ? true : false;
                objDB.isReviewer = Trainings_isReviewer.Checked ? true : false;
                objDB.isApprover = Trainings_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "candidates";
                objDB.isNormUser = candidates_isPreparer.Checked ? true : false;
                objDB.isReviewer = candidates_isReviewer.Checked ? true : false;
                objDB.isApprover = candidates_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "newVacancie";
                objDB.isNormUser = newVacancie_isPreparer.Checked ? true : false;
                objDB.isReviewer = newVacancie_isReviewer.Checked ? true : false;
                objDB.isApprover = newVacancie_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "PolicyProcedure";
                objDB.isNormUser = PolicyProcedures_isPreparer.Checked ? true : false;
                objDB.isReviewer = PolicyProcedures_isReviewer.Checked ? true : false;
                objDB.isApprover = PolicyProcedures_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "InterviewsAndTests";
                objDB.isNormUser = InterviewsAndTests_isPreparer.Checked ? true : false;
                objDB.isReviewer = InterviewsAndTests_isReviewer.Checked ? true : false;
                objDB.isApprover = InterviewsAndTests_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                objDB.UserID = UserID;
                objDB.DocName = "EmployeeLists";
                objDB.isNormUser = EmployeeLists_isPreparer.Checked ? true : false;
                objDB.isReviewer = EmployeeLists_isReviewer.Checked ? true : false;
                objDB.isApprover = EmployeeLists_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "AttendanceSheet";
                objDB.isNormUser = AttendanceSheet_isPreparer.Checked ? true : false;
                objDB.isReviewer = AttendanceSheet_isReviewer.Checked ? true : false;
                objDB.isApprover = AttendanceSheet_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Device";
                objDB.isNormUser = Device_isPreparer.Checked ? true : false;
                objDB.isReviewer = Device_isReviewer.Checked ? true : false;
                objDB.isApprover = Device_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "DeviceUsers";
                objDB.isNormUser = DeviceUsers_isPreparer.Checked ? true : false;
                objDB.isReviewer = DeviceUsers_isReviewer.Checked ? true : false;
                objDB.isApprover = DeviceUsers_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "Announcement";
                objDB.isNormUser = Announcement_isPreparer.Checked ? true : false;
                objDB.isReviewer = Announcement_isReviewer.Checked ? true : false;
                objDB.isApprover = Announcement_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "OtherExpenses";
                objDB.isNormUser = OtherExpenses_isPreparer.Checked ? true : false;
                objDB.isReviewer = OtherExpenses_isReviewer.Checked ? true : false;
                objDB.isApprover = OtherExpenses_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "PayRoll";
                objDB.isNormUser = PayRoll_isPreparer.Checked ? true : false;
                objDB.isReviewer = PayRoll_isReviewer.Checked ? true : false;
                objDB.isApprover = PayRoll_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "ProvinentFund";
                objDB.isNormUser = ProvinentFund_isPreparer.Checked ? true : false;
                objDB.isReviewer = ProvinentFund_isReviewer.Checked ? true : false;
                objDB.isApprover = ProvinentFund_isApprover.Checked ? true : false;
                objDB.AddUserAccess();



                //objDB.UserID = UserID;
                //objDB.DocName = "Job Descriptions";
                //objDB.isNormUser = jds_isPreparer.Checked ? true : false;
                //objDB.isReviewer = jds_isReviewer.Checked ? true : false;
                //objDB.isApprover = jds_isApprover.Checked ? true : false;
                //objDB.AddUserAccess();

                //objDB.UserID = UserID;
                //objDB.DocName = "Department";
                //objDB.isNormUser = dept_isPreparer.Checked ? true : false;
                //objDB.isReviewer = dept_isReviewer.Checked ? true : false;
                //objDB.isApprover = dept_isApprover.Checked ? true : false;
                //objDB.AddUserAccess();

                //objDB.UserID = UserID;
                //objDB.DocName = "Designation";
                //objDB.isNormUser = desg_isPreparer.Checked ? true : false;
                //objDB.isReviewer = desg_isReviewer.Checked ? true : false;
                //objDB.isApprover = desg_isApprover.Checked ? true : false;
                //objDB.AddUserAccess();

                //objDB.UserID = UserID;
                //objDB.DocName = "Grades";
                //objDB.isNormUser = grade_isPreparer.Checked ? true : false;
                //objDB.isReviewer = grade_isReviewer.Checked ? true : false;
                //objDB.isApprover = grade_isApprover.Checked ? true : false;
                //objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Purchase Requisite";
                objDB.isNormUser = pr_isNormUser.Checked ? true : false;
                objDB.isReviewer = pr_isReviewer.Checked ? true : false;
                objDB.isApprover = pr_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Asset Transfer Report";
                objDB.isNormUser = atr_isNormUser.Checked ? true : false;
                objDB.isReviewer = atr_isReviewer.Checked ? true : false;
                objDB.isApprover = atr_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Delivery Note";
                objDB.isNormUser = dn_isNormUser.Checked ? true : false;
                objDB.isReviewer = dn_isReviewer.Checked ? true : false;
                objDB.isApprover = dn_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Tax Rate";
                objDB.isNormUser = tax_isNormUser.Checked ? true : false;
                objDB.isReviewer = tax_isReviewer.Checked ? true : false;
                objDB.isApprover = tax_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Project";
                objDB.isNormUser = project_isNormUser.Checked ? true : false;
                objDB.isReviewer = project_isReviewer.Checked ? true : false;
                objDB.isApprover = project_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Categories";
                objDB.isNormUser = cat_isNormUser.Checked ? true : false;
                objDB.isReviewer = cat_isReviewer.Checked ? true : false;
                objDB.isApprover = cat_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Items";
                objDB.isNormUser = item_isNormUser.Checked ? true : false;
                objDB.isReviewer = item_isReviewer.Checked ? true : false;
                objDB.isApprover = item_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Request For Quotes";
                objDB.isNormUser = rfq_isNormUser.Checked ? true : false;
                objDB.isReviewer = rfq_isReviewer.Checked ? true : false;
                objDB.isApprover = rfq_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Quote Comparision Report";
                objDB.isNormUser = qcr_isNormUser.Checked ? true : false;
                objDB.isReviewer = qcr_isReviewer.Checked ? true : false;
                objDB.isApprover = qcr_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Purchase Order";
                objDB.isNormUser = po_isNormUser.Checked ? true : false;
                objDB.isReviewer = po_isReviewer.Checked ? true : false;
                objDB.isApprover = po_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Good Receive Note";
                objDB.isNormUser = grn_isNormUser.Checked ? true : false;
                objDB.isReviewer = grn_isReviewer.Checked ? true : false;
                objDB.isApprover = grn_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Material Return Note";
                objDB.isNormUser = mrn_isNormUser.Checked ? true : false;
                objDB.isReviewer = mrn_isReviewer.Checked ? true : false;
                objDB.isApprover = mrn_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Debit Note";
                objDB.isNormUser = dbn_isNormUser.Checked ? true : false;
                objDB.isReviewer = dbn_isReviewer.Checked ? true : false;
                objDB.isApprover = dbn_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "VDMS";
                objDB.isNormUser = vdms_isNormUser.Checked ? true : false;
                objDB.isReviewer = vdms_isReviewer.Checked ? true : false;
                objDB.isApprover = vdms_isApprover.Checked ? true : false;
                objDB.AddUserAccess();


                objDB.UserID = UserID;
                objDB.DocName = "AccountDepreciation";
                objDB.isNormUser = AccountDepreciation_isNormUser.Checked ? true : false;
                objDB.isReviewer = AccountDepreciation_isReviewer.Checked ? true : false;
                objDB.isApprover = AccountDepreciation_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Asset";
                objDB.isNormUser = Asset_isNormUser.Checked ? true : false;
                objDB.isReviewer = Asset_isReviewer.Checked ? true : false;
                objDB.isApprover = Asset_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "AssetDisposal";
                objDB.isNormUser = AssetDisposal_isNormUser.Checked ? true : false;
                objDB.isReviewer = AssetDisposal_isReviewer.Checked ? true : false;
                objDB.isApprover = AssetDisposal_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "AssetRevalution";
                objDB.isNormUser = AssetRevalution_isNormUser.Checked ? true : false;
                objDB.isReviewer = AssetRevalution_isReviewer.Checked ? true : false;
                objDB.isApprover = AssetRevalution_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "AssetTransfer";
                objDB.isNormUser = AssetTransfer_isNormUser.Checked ? true : false;
                objDB.isReviewer = AssetTransfer_isReviewer.Checked ? true : false;
                objDB.isApprover = AssetTransfer_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "AssetType";
                objDB.isNormUser = AssetType_isNormUser.Checked ? true : false;
                objDB.isReviewer = AssetType_isReviewer.Checked ? true : false;
                objDB.isApprover = AssetType_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "ChartOfAccount";
                objDB.isNormUser = ChartOfAccount_isNormUser.Checked ? true : false;
                objDB.isReviewer = ChartOfAccount_isReviewer.Checked ? true : false;
                objDB.isApprover = ChartOfAccount_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "CWIP";
                objDB.isNormUser = CWIP_isNormUser.Checked ? true : false;
                objDB.isReviewer = CWIP_isReviewer.Checked ? true : false;
                objDB.isApprover = CWIP_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "Location";
                objDB.isNormUser = Location_isNormUser.Checked ? true : false;
                objDB.isReviewer = Location_isReviewer.Checked ? true : false;
                objDB.isApprover = Location_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "ParentCategory";
                objDB.isNormUser = ParentCategory_isNormUser.Checked ? true : false;
                objDB.isReviewer = ParentCategory_isReviewer.Checked ? true : false;
                objDB.isApprover = ParentCategory_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "PoliciesAndProcedures";
                objDB.isNormUser = PoliciesAndProcedures_isNormUser.Checked ? true : false;
                objDB.isReviewer = PoliciesAndProcedures_isReviewer.Checked ? true : false;
                objDB.isApprover = PoliciesAndProcedures_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "TaxCategory";
                objDB.isNormUser = TaxCategory_isNormUser.Checked ? true : false;
                objDB.isReviewer = TaxCategory_isReviewer.Checked ? true : false;
                objDB.isApprover = TaxCategory_isApprover.Checked ? true : false;
                objDB.AddUserAccess();

                objDB.UserID = UserID;
                objDB.DocName = "TaxDepreciation";
                objDB.isNormUser = TaxDepreciation_isNormUser.Checked ? true : false;
                objDB.isReviewer = TaxDepreciation_isReviewer.Checked ? true : false;
                objDB.isApprover = TaxDepreciation_isApprover.Checked ? true : false;
                objDB.AddUserAccess();
            }

            if (res == "New User Added" || res == "User Data Updated")
            {
                if (res == "New User Added")
                {
                    //Common.addlog("Add", "Admin", "New User \"" + objDB.Email + "\" Added", "Users");
                    Common.addlogNew("Add", "Admin", "New User \"" + objDB.Email + "\" Added", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/Admin/view/Users", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/Users/edit-Users-" + objDB.UserID, "New User \"" + objDB.Email + "\" Added", "Users", "Directors", Convert.ToInt32(objDB.UserID));

                }
                if (res == "User Data Updated") { 
                    //Common.addlog("Update", "Admin", "User \"" + objDB.Email + "\" Updated", "Users", objDB.UserID);
                    Common.addlogNew("Update", "Admin", "User \"" + objDB.Email + "\" Updated", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/Users", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/Users/edit-Users-" + objDB.UserID, "User \"" + objDB.Email + "\" Updated", "Users", "Directors", Convert.ToInt32(objDB.UserID));

                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }

        //protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    CheckSessions();
        //    BindEmployeesDropDown(Convert.ToInt32(ddlCompany.SelectedItem.Value));
        //}

        protected void ddlAccessLevels_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            tblCustom.Visible = false;
            tblHR.Visible = false;
            tblProcrument.Visible = false;
            tblCRM.Visible = false;
            tblFAM.Visible = false;
            tblOM.Visible = false;
            tblCRMGeneral.Visible = false;
            divAccessLevels.Visible = true;
            tblFinance.Visible = false;
            if (ddlAccessLevels.SelectedIndex != 0)
            {
                if (ddlAccessLevels.SelectedIndex == 1)
                {
                    divAccessLevels.Visible = true;
                    tblCustom.Visible = true;
                }
                else if (ddlAccessLevels.SelectedIndex == 3)
                {
                    divAccessLevels.Visible = true;
                    tblHR.Visible = true;
                }
                //else if (ddlAccessLevels.SelectedIndex == 4)
                //{
                //    divAccessLevels.Visible = true;
                //    tblProcrument.Visible = true;
                //}

                //else if (ddlAccessLevels.SelectedIndex == 5)
                //{
                //    divAccessLevels.Visible = true;
                //    tblCRM.Visible = true;
                //}
                //else if (ddlAccessLevels.SelectedIndex == 6)
                //{
                //    divAccessLevels.Visible = true;
                //    tblFAM.Visible = true;
                //}

                //else if (ddlAccessLevels.SelectedIndex == 7)
                //{
                //    divAccessLevels.Visible = true;
                //    tblFinance.Visible = true;

                //}

                //else if (ddlAccessLevels.SelectedIndex == 8)
                //{
                //    divAccessLevels.Visible = true;
                //    tblOM.Visible = true;

                //}
                //else if (ddlAccessLevels.SelectedIndex == 9)
                //{
                //    divAccessLevels.Visible = true;
                //    tblCRMGeneral.Visible = true;

                //}
            }
        }


        private void sendEmails()
        {

            string file = "";
            string html = "";
            file = System.Web.HttpContext.Current.Server.MapPath("/assets/emails/NewUser.html");
            StreamReader sr = new StreamReader(file);
            FileInfo fi = new FileInfo(file);

            if (System.IO.File.Exists(file))
            {
                html += sr.ReadToEnd();
                sr.Close();
            }

            html = html.Replace("##NAME##", ddlEmployees.SelectedItem.Text);
            html = html.Replace("##CREATED_BY##", Session["UserName"].ToString());
            html = html.Replace("##USER_NAME##", txtName.Value);
            html = html.Replace("##ROLE##", ddlAccessLevels.SelectedValue);






            MailMessage mail = new MailMessage();
            mail.Subject = "New Usser created on technofinancials.com";
            mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), Session["CompanyName"].ToString());
            mail.To.Add(txtName.Value);
            mail.Body = html;
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["NoReplySMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NoReplyPort"].ToString()));
            smtp.EnableSsl = true;
            NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["NoReplyPassword"].ToString());
            smtp.Credentials = netCre;
            ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            smtp.Send(mail);

        }



    }
}