﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.manage
{
    public partial class Designation : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();

            if (!Page.IsPostBack)
            {
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/designations";

                BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
                clearFields();
                divAlertMsg.Visible = false;

                if (HttpContext.Current.Items["DesgID"] != null)
                {
                    getDesgByID(Convert.ToInt32(HttpContext.Current.Items["DesgID"].ToString()));
                }
            }
        }

        private void clearFields()
        {
            comment.Value = txtName.Value = "";
            ddlDept.SelectedIndex = 0;
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void BindDepartmentDropDown(int companyID)
        {
            objDB.CompanyID = companyID;
            ddlDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDept.DataTextField = "DeptName";
            ddlDept.DataValueField = "DeptID";
            ddlDept.DataBind();
            ddlDept.Items.Insert(0, new ListItem("--- SELECT ---", "0"));
        }

        private void getDesgByID(int desgID)
        {
            DataTable dt = new DataTable();
            objDB.DesgID = desgID;
            dt = objDB.GetDesignationsByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
                    ddlDept.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                    txtName.Value = dt.Rows[0]["desgtitle"].ToString();
                    comment.Value = dt.Rows[0]["Description"].ToString();
                }
            }

            Common.addlog("View", "Admin", "Location \"" + txtName.Value + "\" Viewed", "Designations", objDB.DesgID);
        }

        protected void btnAddDesg_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DeptID = Convert.ToInt32(ddlDept.SelectedItem.Value);


            objDB.DesgTitle = txtName.Value;
            objDB.Description = comment.Value;

            string res = "";
            if (HttpContext.Current.Items["DesgID"] != null)
            {
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.DesgID = Convert.ToInt32(HttpContext.Current.Items["DesgID"].ToString());
               
            }
            else
            {
                objDB.CreatedBy = Session["UserName"].ToString();

                res = objDB.AddNewDesignation();
                int DesID = 0;

                if (int.TryParse(res, out DesID))
                {
                    
                    objDB.DesgID = DesID;
                    objDB.DeptID = Convert.ToInt32(ddlDept.SelectedItem.Value);
                    objDB.AddJobDescreption();

                    res = "New Designation Added";
                    clearFields();
                }
               
            }

            if (res == "New Designation Added" || res == "Designation Updated")
            {
                if (res == "New Designation Added")
                    Common.addlogNew("Add", "Admin", "New Designation \"" + objDB.DesgTitle + "\" Updated", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/designations", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/designations/edit-designation-" + objDB.DesgID, "New Designation \"" + objDB.DesgTitle + "\" Added", "Designations", "Directors", Convert.ToInt32(objDB.DesgID));
                else if (res == "Designation Updated")
                    Common.addlogNew("Update", "Admin", "Designation \"" + objDB.DesgTitle + "\" Updated", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/designations", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/designations/edit-designation-" + objDB.DesgID, "Department \"" + objDB.DesgTitle + "\" Updated", "Designations", "Directors", Convert.ToInt32(objDB.DesgID));

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }
    }
}