﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Technofinancials.Administrator.manage.Users" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">

            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">
                <asp:UpdatePanel ID="UpdPnl" runat="server">
                    <ContentTemplate>
                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <h1 class="m-0 text-dark">Users</h1>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                        <div style="text-align: right;">
                                        <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                        <a id="btnBack" runat="server" class="AD_btn" >Back</a>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>



                        <section class="app-content">
                           
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <h4>Employee <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlEmployees" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                            <asp:DropDownList ID="ddlEmployees" runat="server" CssClass="form-control select2" data-plugin="select2" ></asp:DropDownList>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <h4>Email <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic" runat="server" ControlToValidate="txtName" ForeColor="Red" ErrorMessage=" Email Address Not Valid" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="btnValidate"></asp:RegularExpressionValidator>--%>

                                            <input class="form-control" id="txtName" placeholder="Email" type="text" runat="server" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <h4>Access Levels <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAccessLevels" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                            <asp:DropDownList ID="ddlAccessLevels" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlAccessLevels_SelectedIndexChanged">
                                                <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                                <asp:ListItem Value="Custom">Module Wise</asp:ListItem>
                                                <asp:ListItem Value="Directors">Administrator</asp:ListItem>
                                                <asp:ListItem Value="Human Resource">People Management</asp:ListItem>
                                               <%-- <asp:ListItem Value="Procrument">Procurement Management</asp:ListItem>
                                                <asp:ListItem Value="CRM">Customer Relationship Management</asp:ListItem>
                                                <asp:ListItem Value="Fixed Asset">Fixed Asset</asp:ListItem>
                                                <asp:ListItem Value="Finance">Finance</asp:ListItem>
                                                <asp:ListItem Value="Operation Management">Operation Management</asp:ListItem>
                                                <asp:ListItem Value="CRM-General">CRM-General</asp:ListItem>--%>
                                                <asp:ListItem Value="Normal User">ESS ONLY</asp:ListItem>
                                                   <asp:ListItem Value="Limited">Limited Access</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">

                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                            <div class="content-header second_heading">
                                <div class="container-fluid">
                                    <div class="row mb-2">
                                        <div class="col-sm-6">
                                            <h1 class="m-0 text-dark" id="divAccessLevels" runat="server">Access Levels
                                            </h1>
                                        </div>
                                        <!-- /.col -->

                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.container-fluid -->
                            </div>
                            <section class="content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-12 no-pad">

                                            <table class="table table-bordered" id="tblCustom" runat="server">
                                                <thead>
                                                    <tr>
                                                        <th>Access
                                                        </th>
                                                        <th>Preparer
                                                        </th>
                                                        <th>Reviewer
                                                        </th>
                                                        <th>Approver
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Administrator
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <input type="checkbox" class="check_Administrator" id="Administrator_isApprover" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>People Management
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="radio_PeopleManagement" id="PeopleManagement_isPreparer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_PeopleManagement" id="PeopleManagement_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_PeopleManagement" id="PeopleManagement_isApprover" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr style="display:none">
                                                        <td>Procrument Management
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="radio_ProcrumentManagement" id="ProcrumentManagement_isPreparer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_ProcrumentManagement" id="ProcrumentManagement_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_ProcrumentManagement" id="ProcrumentManagement_isApprover" runat="server" />
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td>IT
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_IT" id="IT_isPreparer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_IT" id="IT_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_IT" id="IT_isApprover" runat="server" />
                                                        </td>
                                                    </tr>
                                                     <tr style="display:none">
                                                        <td>Fixed Asset
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="radio_FixedAsset" id="FixedAsset_isPreparer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_FixedAsset" id="FixedAsset_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_FixedAsset" id="FixedAsset_isApprover" runat="server" />
                                                        </td>
                                                    </tr>
                                                  <tr style="display:none">
                                                        <td>CRM
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="radio_CRM" id="CRM_isPreparer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_CRM" id="CRM_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_CRM" id="CRM_isApprover" runat="server" />
                                                        </td>
                                                    </tr>

                                                    <tr >
                                                        <td>Finance
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="radio_Finance" id="Finance_isNormUser" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_Finance" id="Finance_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_Finance" id="Finance_isApprover" runat="server" />
                                                        </td>
                                                    </tr>

                                                    <tr style="display:none">
                                                        <td>Operation Management
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="radio_OM" id="OM_isNormUser" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_OM" id="OM_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_OM" id="OM_isApprover" runat="server" />
                                                        </td>
                                                    </tr>

                                                    <tr style="display:none">
                                                        <td>CRM General
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="radio_crmGeneral" id="crmGeneral_isNormUser" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_crmGeneral" id="crmGeneral_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_crmGeneral" id="crmGeneral_isApprover" runat="server" />
                                                        </td>
                                                    </tr>
                                                   <tr style="display:none">
                                                        <td>Production
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="radio_crmGeneral" id="Production_isNormUser" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_crmGeneral" id="Production_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_crmGeneral" id="Production_isApprover" runat="server" />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>


                                            <table class="table table-bordered" id="tblOM" runat="server">
                                                <thead>
                                                    <tr>
                                                        <th>Access
                                                        </th>
                                                        <th>Preparer
                                                        </th>
                                                        <th>Reviewer
                                                        </th>
                                                        <th>Approver
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>All Pages
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="radio_Finance" id="OMAllPages_isNormUser" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_Finance" id="OMAllPages_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_Finance" id="OMAllPages_isApprover" runat="server" />
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>



                                            <table class="table table-bordered" id="tblCRMGeneral" runat="server">
                                                <thead>
                                                    <tr>
                                                        <th>Access
                                                        </th>
                                                        <th>Preparer
                                                        </th>
                                                        <th>Reviewer
                                                        </th>
                                                        <th>Approver
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>All Pages
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="radio_crmGeneral" id="crmGeneralAllPages_isNormUser" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_crmGeneral" id="crmGeneralAllPages_isReviewer" runat="server" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check_crmGeneral" id="crmGeneralAllPages_isApprover" runat="server" />
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>

                                            <table class="table table-bordered" id="tblHR" runat="server">
                                                <thead>
                                                    <tr>
                                                        <th>Access / Document
                                                        </th>
                                                        <th>Preparer
                                                        </th>
                                                        <th>Reviewer
                                                        </th>
                                                        <th>Approver
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tr>
                                                    <td>Policy Procedures
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_PolicyProcedures" id="PolicyProcedures_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PolicyProcedures" id="PolicyProcedures_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PolicyProcedures" id="PolicyProcedures_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>New Vacancy
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_newVacancie" id="newVacancie_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_newVacancie" id="newVacancie_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_newVacancie" id="newVacancie_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Candidates
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_candidates" id="candidates_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_candidates" id="candidates_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_candidates" id="candidates_isApprover" runat="server" />
                                                    </td>
                                                </tr>




                                                <tr>
                                                    <td>Interviews / Tests
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_InterviewsAndTests" id="InterviewsAndTests_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_InterviewsAndTests" id="InterviewsAndTests_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_InterviewsAndTests" id="InterviewsAndTests_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Employee Lists
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_EmployeeLists" id="EmployeeLists_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_EmployeeLists" id="EmployeeLists_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_EmployeeLists" id="EmployeeLists_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Attendance Sheet
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_AttendanceSheet" id="AttendanceSheet_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AttendanceSheet" id="AttendanceSheet_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AttendanceSheet" id="AttendanceSheet_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>PayRoll
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_PayRoll" id="PayRoll_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PayRoll" id="PayRoll_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PayRoll" id="PayRoll_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Provinent Fund
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_ProvinentFund" id="ProvinentFund_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ProvinentFund" id="ProvinentFund_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ProvinentFund" id="ProvinentFund_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Trainings
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Trainings" id="Trainings_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Trainings" id="Trainings_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Trainings" id="Trainings_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Tests
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Tests" id="Tests_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tests" id="Tests_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tests" id="Tests_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Succession
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Succession" id="Succession_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Succession" id="Succession_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Succession" id="Succession_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Performance Appraisals
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_PerformanceAppraisals" id="PerformanceAppraisals_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PerformanceAppraisals" id="PerformanceAppraisals_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PerformanceAppraisals" id="PerformanceAppraisals_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Surveys
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Surveys" id="Surveys_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Surveys" id="Surveys_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Surveys" id="Surveys_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Mailing List
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_MailingList" id="MailingList_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_MailingList" id="MailingList_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_MailingList" id="MailingList_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Offer Letter
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_OfferLetter" id="OfferLetter_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_OfferLetter" id="OfferLetter_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_OfferLetter" id="OfferLetter_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Warning Letter
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_WarningLetter" id="WarningLetter_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_WarningLetter" id="WarningLetter_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_WarningLetter" id="WarningLetter_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Payroll Sheet
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_PayrollSheet" id="PayrollSheet_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PayrollSheet" id="PayrollSheet_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PayrollSheet" id="PayrollSheet_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Promotion Letter
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_PromotionLetter" id="PromotionLetter_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PromotionLetter" id="PromotionLetter_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PromotionLetter" id="PromotionLetter_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Loans
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Loans" id="Loans_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Loans" id="Loans_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Loans" id="Loans_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Holidays
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Holidays" id="Holidays_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Holidays" id="Holidays_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Holidays" id="Holidays_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Shifts
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Shifts" id="Shifts_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Shifts" id="Shifts_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Shifts" id="Shifts_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                            </table>

                                            <table class="table table-bordered" id="tblProcrument" runat="server">
                                                <thead>
                                                    <tr>
                                                        <th>Access / Document
                                                        </th>
                                                        <th>Preparer
                                                        </th>
                                                        <th>Reviewer
                                                        </th>
                                                        <th>Approver
                                                        </th>
                                                    </tr>
                                                </thead>

                                                <tr>
                                                    <td>Projects
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_project" id="project_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_project" id="project_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_project" id="project_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Tax Rate
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_tax" id="tax_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_tax" id="tax_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_tax" id="tax_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Categories
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_cat" id="cat_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_cat" id="cat_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_cat" id="cat_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Items
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_item" id="item_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_item" id="item_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_item" id="item_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>VDMS
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_vdms" id="vdms_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_vdms" id="vdms_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_vdms" id="vdms_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Purchase Requisite
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_pr" id="pr_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_pr" id="pr_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_pr" id="pr_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Asset Transfer Report
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_atr" id="atr_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_atr" id="atr_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_atr" id="atr_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Delivery Notes
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_dn" id="dn_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_dn" id="dn_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_dn" id="dn_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Request for Quotes
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_rfq" id="rfq_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_rfq" id="rfq_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_rfq" id="rfq_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Quote Comparision Report
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_qcr" id="qcr_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_qcr" id="qcr_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_qcr" id="qcr_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Purchase Orders
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_po" id="po_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_po" id="po_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_po" id="po_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Goods Receive Note
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_grn" id="grn_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_grn" id="grn_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_grn" id="grn_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Material Return Note
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_mrn" id="mrn_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_mrn" id="mrn_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_mrn" id="mrn_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Debit Note
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_dbn" id="dbn_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_dbn" id="dbn_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_dbn" id="dbn_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Purchase Plan
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_pp" id="pp_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_pp" id="pp_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_pp" id="pp_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>

                                            <table class="table table-bordered" id="tblCRM" runat="server">
                                                <thead>
                                                    <tr>
                                                        <th>Access / Document
                                                        </th>
                                                        <th>Preparer
                                                        </th>
                                                        <th>Reviewer
                                                        </th>
                                                        <th>Approver
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tr>
                                                    <td>Leads
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Leads" id="Leads_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Leads" id="Leads_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Leads" id="Leads_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Clients
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Clients" id="Clients_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Clients" id="Clients_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Clients" id="Clients_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Business Division
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_BusinessDivision" id="BusinessDivision_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_BusinessDivision" id="BusinessDivision_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_BusinessDivision" id="BusinessDivision_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Product And Services
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_ProductAndServices" id="ProductAndServices_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ProductAndServices" id="ProductAndServices_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ProductAndServices" id="ProductAndServices_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                            </table>


                                            <table class="table table-bordered" id="tblFAM" runat="server">
                                                <thead>
                                                    <tr>
                                                        <th>Access / Document
                                                        </th>
                                                        <th>Preparer
                                                        </th>
                                                        <th>Reviewer
                                                        </th>
                                                        <th>Approver
                                                        </th>
                                                    </tr>
                                                </thead>



                                                <tr>
                                                    <td>Account Depreciation
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_AccountDepreciation" id="AccountDepreciation_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AccountDepreciation" id="AccountDepreciation_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AccountDepreciation" id="AccountDepreciation_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Asset
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Asset" id="Asset_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Asset" id="Asset_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Asset" id="Asset_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Asset Disposal
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_AssetDisposal" id="AssetDisposal_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AssetDisposal" id="AssetDisposal_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AssetDisposal" id="AssetDisposal_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Asset Revalution
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_AssetRevalution" id="AssetRevalution_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AssetRevalution" id="AssetRevalution_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AssetRevalution" id="AssetRevalution_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Asset Transfer
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_AssetTransfer" id="AssetTransfer_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AssetTransfer" id="AssetTransfer_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AssetTransfer" id="AssetTransfer_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Sub Category
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_AssetType" id="AssetType_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AssetType" id="AssetType_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_AssetType" id="AssetType_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Chart Of Account
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_ChartOfAccount" id="ChartOfAccount_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ChartOfAccount" id="ChartOfAccount_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ChartOfAccount" id="ChartOfAccount_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>CWIP
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_CWIP" id="CWIP_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_CWIP" id="CWIP_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_CWIP" id="CWIP_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Location
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Location" id="Location_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Location" id="Location_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Location" id="Location_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Parent Category
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_ParentCategory" id="ParentCategory_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ParentCategory" id="ParentCategory_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ParentCategory" id="ParentCategory_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Policies And Procedures
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_PoliciesAndProcedures" id="PoliciesAndProcedures_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PoliciesAndProcedures" id="PoliciesAndProcedures_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PoliciesAndProcedures" id="PoliciesAndProcedures_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tax Category
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_TaxCategory" id="TaxCategory_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_TaxCategory" id="TaxCategory_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_TaxCategory" id="TaxCategory_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tax Depreciation
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_TaxDepreciation" id="TaxDepreciation_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_TaxDepreciation" id="TaxDepreciation_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_TaxDepreciation" id="TaxDepreciation_isApprover" runat="server" />
                                                    </td>
                                                </tr>


                                            </table>


                                            <table class="table table-bordered" id="tblFinance" runat="server">
                                                <thead>
                                                    <tr>
                                                        <th>Access / Document
                                                        </th>
                                                        <th>Preparer
                                                        </th>
                                                        <th>Reviewer
                                                        </th>
                                                        <th>Approver
                                                        </th>
                                                    </tr>
                                                </thead>





                                                <tr>
                                                    <td>Invoice
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Invoice" id="Invoice_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Invoice" id="Invoice_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Invoice" id="Invoice_isApprover" runat="server" />
                                                    </td>
                                                </tr>




                                                <tr>
                                                    <td>Quotation
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Quotation" id="Quotation_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Quotation" id="Quotation_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Quotation" id="Quotation_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>CreditNote
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_CreditNote" id="CreditNote_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_CreditNote" id="CreditNote_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_CreditNote" id="CreditNote_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>StatementSharing
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_StatementSharing" id="StatementSharing_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_StatementSharing" id="StatementSharing_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_StatementSharing" id="StatementSharing_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>ProductServices
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_ProductServices" id="ProductServices_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ProductServices" id="ProductServices_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ProductServices" id="ProductServices_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>CDMS
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_CDMS" id="CDMS_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_CDMS" id="CDMS_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_CDMS" id="CDMS_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>PurchaseOrder
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_PurchaseOrder" id="PurchaseOrder_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PurchaseOrder" id="PurchaseOrder_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PurchaseOrder" id="PurchaseOrder_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>DebitNote
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_DebitNote" id="DebitNote_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_DebitNote" id="DebitNote_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_DebitNote" id="DebitNote_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Expenses
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Expenses" id="Expenses_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Expenses" id="Expenses_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Expenses" id="Expenses_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>PayrollManagement
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_PayrollManagement" id="PayrollManagement_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PayrollManagement" id="PayrollManagement_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PayrollManagement" id="PayrollManagement_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Accounts
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Accounts" id="Accounts_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Accounts" id="Accounts_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Accounts" id="Accounts_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>TransferMoney
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_TransferMoney" id="TransferMoney_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_TransferMoney" id="TransferMoney_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_TransferMoney" id="TransferMoney_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>BankReconciliation
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_BankReconciliation" id="BankReconciliation_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_BankReconciliation" id="BankReconciliation_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_BankReconciliation" id="BankReconciliation_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>PettyCashManagement
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_PettyCashManagement" id="PettyCashManagement_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PettyCashManagement" id="PettyCashManagement_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_PettyCashManagement" id="PettyCashManagement_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>ConfigureCOA
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_ConfigureCOA" id="ConfigureCOA_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ConfigureCOA" id="ConfigureCOA_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_ConfigureCOA" id="ConfigureCOA_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>OpeningBalance
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_OpeningBalance" id="OpeningBalance_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_OpeningBalance" id="OpeningBalance_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_OpeningBalance" id="OpeningBalance_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>MultiCurrency
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_MultiCurrency" id="MultiCurrency_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_MultiCurrency" id="MultiCurrency_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_MultiCurrency" id="MultiCurrency_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tax
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Tax" id="Tax2_isNormUser" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tax" id="Tax2_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tax" id="Tax2_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Device
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Tax" id="Device_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tax" id="Device_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tax" id="Device_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>DeviceUsers
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Tax" id="DeviceUsers_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tax" id="DeviceUsers_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tax" id="DeviceUsers_isApprover" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Announcement
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Tax" id="Announcement_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tax" id="Announcement_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tax" id="Announcement_isApprover" runat="server" />
                                                    </td>
                                                </tr> 
                                                
                                                <tr>
                                                    <td>OtherExpenses
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="radio_Tax" id="OtherExpenses_isPreparer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tax" id="OtherExpenses_isReviewer" runat="server" />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="check_Tax" id="OtherExpenses_isApprover" runat="server" />
                                                    </td>
                                                </tr>

                                            </table>
                                                                                            <div class="clearfix">&nbsp;</div>
                                                <div class="clearfix">&nbsp;</div>
                                                <div class="clearfix">&nbsp;</div>
                                                <br />
                                                <br />
                                                <br />
                                        </div>
                                    </div>

                                </div>
                                <!-- /.container-fluid -->


                            </section>

                        </section>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
                        @media only screen and (max-width: 992px) and (min-width: 768px) {
                div#gv_wrapper{
    overflow-x: scroll;
                    overflow-y: hidden;
                padding-bottom: 35px;
}
        /* Track */
                    div#gv_wrapper::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            div#gv_wrapper::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            div#gv_wrapper::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }
            }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }
            .table > tbody > tr:nth-child(1) th{
    border-top:none !important;
}
                .tf-back-btn i {
                    color: #fff !important;
                }
                .content-header.second_heading{
                    margin-bottom:0px !important;
                }
        </style>
        <script>

            //Tax Start
            $('.radio_crmgeneral').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_crmgeneral').prop('checked', false);
                }
            });

            $('.check_crmgeneral').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_crmgeneral').prop('checked', false);
                }
            });

            //Tax End




            //Tax Start
            $('.radio_Tax').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Tax').prop('checked', false);
                }
            });

            $('.check_Tax').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Tax').prop('checked', false);
                }
            });

            //Tax End

            //MultiCurrency Start
            $('.radio_MultiCurrency').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_MultiCurrency').prop('checked', false);
                }
            });

            $('.check_MultiCurrency').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_MultiCurrency').prop('checked', false);
                }
            });

            //MultiCurrency End


            //OpeningBalance Start
            $('.radio_OpeningBalance').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_OpeningBalance').prop('checked', false);
                }
            });

            $('.check_OpeningBalance').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_OpeningBalance').prop('checked', false);
                }
            });

            //OpeningBalance End


            //ConfigureCOA Start
            $('.radio_ConfigureCOA').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_ConfigureCOA').prop('checked', false);
                }
            });

            $('.check_ConfigureCOA').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_ConfigureCOA').prop('checked', false);
                }
            });

            //ConfigureCOA End

            //PettyCashManagement Start
            $('.radio_PettyCashManagement').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PettyCashManagement').prop('checked', false);
                }
            });

            $('.check_PettyCashManagement').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_PettyCashManagement').prop('checked', false);
                }
            });

            //PettyCashManagement End

            //BankReconciliation Start
            $('.radio_BankReconciliation').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_BankReconciliation').prop('checked', false);
                }
            });

            $('.check_BankReconciliation').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_BankReconciliation').prop('checked', false);
                }
            });

            //BankReconciliation End


            //TransferMoney Start
            $('.radio_TransferMoney').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_TransferMoney').prop('checked', false);
                }
            });

            $('.check_TransferMoney').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_TransferMoney').prop('checked', false);
                }
            });

            //TransferMoney End


            //Accounts Start
            $('.radio_Accounts').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Accounts').prop('checked', false);
                }
            });

            $('.check_Accounts').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Accounts').prop('checked', false);
                }
            });

            //Accounts End


            //PayrollManagement Start
            $('.radio_PayrollManagement').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PayrollManagement').prop('checked', false);
                }
            });

            $('.check_PayrollManagement').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_PayrollManagement').prop('checked', false);
                }
            });

            //PayrollManagement End



            //Expenses Start
            $('.radio_Expenses').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Expenses').prop('checked', false);
                }
            });

            $('.check_Expenses').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Expenses').prop('checked', false);
                }
            });

            //Expenses End


            //DebitNote Start
            $('.radio_DebitNote').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_DebitNote').prop('checked', false);
                }
            });

            $('.check_DebitNote').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_DebitNote').prop('checked', false);
                }
            });

            //DebitNote End


            //PurchaseOrder Start
            $('.radio_PurchaseOrder').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PurchaseOrder').prop('checked', false);
                }
            });

            $('.check_PurchaseOrder').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_PurchaseOrder').prop('checked', false);
                }
            });

            //PurchaseOrder End



            //CDMS Start
            $('.radio_CDMS').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_CDMS').prop('checked', false);
                }
            });

            $('.check_CDMS').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_CDMS').prop('checked', false);
                }
            });

            //CDMS End

            //ProductServices Start
            $('.radio_ProductServices').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_ProductServices').prop('checked', false);
                }
            });

            $('.check_ProductServices').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_ProductServices').prop('checked', false);
                }
            });

            //ProductServices End


            //StatementSharing Start
            $('.radio_StatementSharing').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_StatementSharing').prop('checked', false);
                }
            });

            $('.check_StatementSharing').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_StatementSharing').prop('checked', false);
                }
            });

            //StatementSharing End


            //CreditNote Start
            $('.radio_CreditNote').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_CreditNote').prop('checked', false);
                }
            });

            $('.check_CreditNote').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_CreditNote').prop('checked', false);
                }
            });

            //CreditNote End


            //Quotation Start
            $('.radio_Quotation').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Quotation').prop('checked', false);
                }
            });

            $('.check_Quotation').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Quotation').prop('checked', false);
                }
            });

            //Quotation End


            //Invoice Start
            $('.radio_Invoice').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Invoice').prop('checked', false);
                }
            });

            $('.check_Invoice').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Invoice').prop('checked', false);
                }
            });

            //Invoice End


            //AccountDepreciation Start
            $('.radio_finance').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_finance').prop('checked', false);
                }
            });

            $('.check_finance').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_finance').prop('checked', false);
                }
            });

            //AccountDepreciation End

            //AccountDepreciation Start
            $('.radio_AccountDepreciation').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_AccountDepreciation').prop('checked', false);
                }
            });

            $('.check_AccountDepreciation').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_AccountDepreciation').prop('checked', false);
                }
            });

            //AccountDepreciation End

            //Asset Start
            $('.radio_Asset').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Asset').prop('checked', false);
                }
            });

            $('.check_Asset').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Asset').prop('checked', false);
                }
            });

            //Asset End

            //AssetDisposal Start
            $('.radio_AssetDisposal').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_AssetDisposal').prop('checked', false);
                }
            });

            $('.check_AssetDisposal').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_AssetDisposal').prop('checked', false);
                }
            });

            //AssetDisposal End

            //AssetRevalution Start
            $('.radio_AssetRevalution').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_AssetRevalution').prop('checked', false);
                }
            });

            $('.check_AssetRevalution').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_AssetRevalution').prop('checked', false);
                }
            });

            //AssetRevalution End

            //AssetTransfer Start
            $('.radio_AssetTransfer').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_AssetTransfer').prop('checked', false);
                }
            });

            $('.check_AssetTransfer').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_AssetTransfer').prop('checked', false);
                }
            });

            //AssetTransfer End

            //SubCategory Start
            $('.radio_AssetType').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_AssetType').prop('checked', false);
                }
            });

            $('.check_AssetType').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_AssetType').prop('checked', false);
                }
            });

            //SubCategory End

            //ChartOfAccount Start
            $('.radio_ChartOfAccount').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_ChartOfAccount').prop('checked', false);
                }
            });

            $('.check_ChartOfAccount').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_ChartOfAccount').prop('checked', false);
                }
            });

            //ChartOfAccount End

            //CWIP Start
            $('.radio_CWIP').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_CWIP').prop('checked', false);
                }
            });

            $('.check_CWIP').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_CWIP').prop('checked', false);
                }
            });

            //CWIP End

            //Location Start
            $('.radio_Location').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Location').prop('checked', false);
                }
            });

            $('.check_Location').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Location').prop('checked', false);
                }
            });

            //Location End

            //ParentCategory Start
            $('.radio_ParentCategory').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_ParentCategory').prop('checked', false);
                }
            });

            $('.check_ParentCategory').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_ParentCategory').prop('checked', false);
                }
            });

            //ParentCategory End

            //PoliciesAndProcedures Start
            $('.radio_PoliciesAndProcedures').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PoliciesAndProcedures').prop('checked', false);
                }
            });

            $('.check_PoliciesAndProcedures').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_PoliciesAndProcedures').prop('checked', false);
                }
            });

            //PoliciesAndProcedures End

            //TaxCategory Start
            $('.radio_TaxCategory').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_TaxCategory').prop('checked', false);
                }
            });

            $('.check_TaxCategory').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_TaxCategory').prop('checked', false);
                }
            });

            //TaxCategory End

            //TaxDepreciation Start
            $('.radio_TaxDepreciation').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_TaxDepreciation').prop('checked', false);
                }
            });

            $('.check_TaxDepreciation').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_TaxDepreciation').prop('checked', false);
                }
            });

            //TaxDepreciation End


            //Finance Start
            $('.radio_Finance').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Finance').prop('checked', false);
                }
            });

            $('.check_Finance').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Finance').prop('checked', false);
                }
            });

            //Finance End

            //CRM Start
            $('.radio_CRM').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_CRM').prop('checked', false);
                }
            });

            $('.check_CRM').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_CRM').prop('checked', false);
                }
            });

            //CRM End

            //FixedAsset Start
            $('.radio_FixedAsset').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_FixedAsset').prop('checked', false);
                }
            });

            $('.check_FixedAsset').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_FixedAsset').prop('checked', false);
                }
            });

            //FixedAsset End

            //ProcrumentManagement Start
            $('.radio_ProcrumentManagement').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_ProcrumentManagement').prop('checked', false);
                }
            });

            $('.check_ProcrumentManagement').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_ProcrumentManagement').prop('checked', false);
                }
            });

            //ProcrumentManagement End

            //PeopleManagement Start
            $('.radio_PeopleManagement').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PeopleManagement').prop('checked', false);
                }
            });

            $('.check_PeopleManagement').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_PeopleManagement').prop('checked', false);
                }
            });

            //PeopleManagement End



            //Clients Start
            $('.radio_Clients').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Clients').prop('checked', false);
                }
            });

            $('.check_Clients').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Clients').prop('checked', false);
                }
            });

            //Clients End



            //Leads Start
            $('.radio_Leads').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Leads').prop('checked', false);
                }
            });

            $('.check_Leads').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Leads').prop('checked', false);
                }
            });

            //Leads End



            //Business Division Start
            $('.radio_BusinessDivision').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_BusinessDivision').prop('checked', false);
                }
            });

            $('.check_BusinessDivision').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_BusinessDivision').prop('checked', false);
                }
            });

            //Business Division End

            //ProductAndServices Start
            $('.radio_ProductAndServices').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_ProductAndServices').prop('checked', false);
                }
            });

            $('.check_ProductAndServices').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_ProductAndServices').prop('checked', false);
                }
            });

            //ProductAndServices End





            //Purchase pLan Start
            $('.radio_PP').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_pp').prop('checked', false);
                }
            });

            $('.check_PP').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_pp').prop('checked', false);
                }
            });

            // Purchase pLan End


            // Shifts Script Start
            $('.radio_Shifts').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Shifts').prop('checked', false);
                }
            });

            $('.check_Shifts').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Shifts').prop('checked', false);
                }
            });

            // Shifts Script End



            // Holidays Script Start
            $('.radio_Holidays').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Holidays').prop('checked', false);
                }
            });

            $('.check_Holidays').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Holidays').prop('checked', false);
                }
            });

            // Holidays Script End



            // Loans Script Start
            $('.radio_Loans').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Loans').prop('checked', false);
                }
            });

            $('.check_Loans').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Loans').prop('checked', false);
                }
            });

            // Loans Script End


            // PromotionLetter Script Start
            $('.radio_PromotionLetter').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PromotionLetter').prop('checked', false);
                }
            });

            $('.check_PromotionLetter').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_PromotionLetter').prop('checked', false);
                }
            });

            // PromotionLetter Script End




            // PayrollSheet Script Start
            $('.radio_PayrollSheet').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PayrollSheet').prop('checked', false);
                }
            });

            $('.check_PayrollSheet').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_PayrollSheet').prop('checked', false);
                }
            });

            // PayrollSheet Script End




            // WarningLetter Script Start
            $('.radio_WarningLetter').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_WarningLetter').prop('checked', false);
                }
            });

            $('.check_WarningLetter').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_WarningLetter').prop('checked', false);
                }
            });

            // WarningLetter Script End


            // OfferLetter Script Start
            $('.radio_OfferLetter').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_OfferLetter').prop('checked', false);
                }
            });

            $('.check_OfferLetter').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_OfferLetter').prop('checked', false);
                }
            });

            // OfferLetter Script End




            // MailingList Script Start
            $('.radio_MailingList').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_MailingList').prop('checked', false);
                }
            });

            $('.check_MailingList').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_MailingList').prop('checked', false);
                }
            });

            // MailingList Script End


            // Surveys Script Start
            $('.radio_Surveys').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Surveys').prop('checked', false);
                }
            });

            $('.check_Surveys').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Surveys').prop('checked', false);
                }
            });

            // Surveys Script End



            // PerformanceAppraisals Script Start
            $('.radio_PerformanceAppraisals').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PerformanceAppraisals').prop('checked', false);
                }
            });

            $('.check_PerformanceAppraisals').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_PerformanceAppraisals').prop('checked', false);
                }
            });

            // PerformanceAppraisals Script End



            // Succession Script Start
            $('.radio_Succession').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Succession').prop('checked', false);
                }
            });

            $('.check_Succession').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Succession').prop('checked', false);
                }
            });

            // Succession Script End


            // Tests Script Start
            $('.radio_Tests').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Tests').prop('checked', false);
                }
            });

            $('.check_Tests').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Tests').prop('checked', false);
                }
            });

            // Tests Script End



            // Trainings Script Start
            $('.radio_Trainings').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_Trainings').prop('checked', false);
                }
            });

            $('.check_Trainings').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_Trainings').prop('checked', false);
                }
            });

            // Trainings Script End


            //candidate script start
            $('.radio_candidates').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_candidates').prop('checked', false);
                }
            });

            $('.check_candidates').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_candidates').prop('checked', false);
                }
            });
            //candidate scripr end


            // newVacancie Script Start
            $('.radio_newVacancie').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_newVacancie').prop('checked', false);
                }
            });

            $('.check_newVacancie').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_newVacancie').prop('checked', false);
                }
            });

            // newVacancie Script End 

            // PolicyProcedures Script Start
            $('.radio_PolicyProcedures').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PolicyProcedures').prop('checked', false);
                }
            });

            $('.check_PolicyProcedures').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_PolicyProcedures').prop('checked', false);
                }
            });

            // PolicyProcedures Script End



            // InterviewsAndTests Script Start
            $('.radio_InterviewsAndTests').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_InterviewsAndTests').prop('checked', false);
                }
            });

            $('.check_InterviewsAndTests').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_InterviewsAndTests').prop('checked', false);
                }
            });

            // InterviewsAndTests Script End



            // EmployeeLists Script Start
            $('.radio_EmployeeLists').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_EmployeeLists').prop('checked', false);
                }
            });

            $('.check_EmployeeLists').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_EmployeeLists').prop('checked', false);
                }
            });

            // EmployeeLists Script End



            // AttendanceSheet Script Start
            $('.radio_AttendanceSheet').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_AttendanceSheet').prop('checked', false);
                }
            });

            $('.check_AttendanceSheet').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_AttendanceSheet').prop('checked', false);
                }
            });

            // AttendanceSheet Script End



            // PayRoll Script Start
            $('.radio_PayRoll').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PayRoll').prop('checked', false);
                }
            });

            $('.check_PayRoll').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_PayRoll').prop('checked', false);
                }
            });

            // PayRoll Script End




            // ProvinentFund Script Start
            $('.radio_ProvinentFund').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_ProvinentFund').prop('checked', false);
                }
            });

            $('.check_ProvinentFund').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_ProvinentFund').prop('checked', false);
                }
            });

            // ProvinentFund Script End

            $('.radio_pr').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_pr').prop('checked', false);
                }
            });

            $('.check_pr').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_pr').prop('checked', false);
                }
            });

            $('.radio_atr').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_atr').prop('checked', false);
                }
            });

            $('.check_atr').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_atr').prop('checked', false);
                }
            });

            $('.radio_dn').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_dn').prop('checked', false);
                }
            });

            $('.check_dn').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_dn').prop('checked', false);
                }
            });


            $('.radio_tax').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_tax').prop('checked', false);
                }
            });

            $('.check_tax').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_tax').prop('checked', false);
                }
            });

            $('.radio_project').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_project').prop('checked', false);
                }
            });

            $('.check_project').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_project').prop('checked', false);
                }
            });

            $('.radio_cat').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_cat').prop('checked', false);
                }
            });

            $('.check_cat').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_cat').prop('checked', false);
                }
            });

            $('.radio_item').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_item').prop('checked', false);
                }
            });

            $('.check_item').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_item').prop('checked', false);
                }
            });

            $('.radio_qcr').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_qcr').prop('checked', false);
                }
            });

            $('.check_qcr').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_qcr').prop('checked', false);
                }
            });

            $('.radio_rfq').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_rfq').prop('checked', false);
                }
            });

            $('.check_rfq').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_rfq').prop('checked', false);
                }
            });

            $('.radio_po').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_po').prop('checked', false);
                }
            });

            $('.check_po').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_po').prop('checked', false);
                }
            });


            $('.radio_grn').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_grn').prop('checked', false);
                }
            });

            $('.check_grn').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_grn').prop('checked', false);
                }
            });


            $('.radio_mrn').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_mrn').prop('checked', false);
                }
            });

            $('.check_mrn').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_mrn').prop('checked', false);
                }
            });


            $('.radio_dbn').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_dbn').prop('checked', false);
                }
            });

            $('.check_dbn').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_dbn').prop('checked', false);
                }
            });


            $('.radio_vdms').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_vdms').prop('checked', false);
                }
            });

            $('.check_vdms').change(function () {
                if ($(this).is(':checked')) {
                    $('.radio_vdms').prop('checked', false);
                }
            });




        </script>
        <!-- Modal -->
    </form>
</body>
</html>
