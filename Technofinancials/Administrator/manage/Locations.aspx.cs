﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.manage
{
    public partial class Locations : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int LocID
        {
            get
            {
                if (ViewState["LocID"] != null)
                {
                    return (int)ViewState["LocID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["LocID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    txtCode.Disabled = true;
                    txtLocationCode.Disabled = true;
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/locations";

                    divAlertMsg.Visible = false;
                    clearFields();

                    BindCountriesDropDown();
                    //BindStatesDropDown(166);

                    DataTable LocCode = new DataTable();
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                    LocCode = objDB.GetNewLocationCode(ref errorMsg);
                    if (LocCode != null)
                    {
                        if (LocCode.Rows.Count > 0)
                        {
                            txtLocationCode.Value = LocCode.Rows[0][0].ToString();
                        }
                    }

                    if (HttpContext.Current.Items["LocationID"] != null)
                    {
                        LocID = Convert.ToInt32(HttpContext.Current.Items["LocationID"].ToString());
                        getDataByID(LocID);

                    }

                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.LocationID = ID;
            dt = objDB.GetLocationByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    txtName.Value = dt.Rows[0]["NAME"].ToString();
                    txtPrefix.Value = dt.Rows[0]["PREFIX"].ToString();
                    txtDescription.Value = dt.Rows[0]["DESCRIPTION"].ToString();
                    txtLocationCode.Value = dt.Rows[0]["LocationCode"].ToString();

                    ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["CountryName"].ToString()));
                    int countriesIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["CountryName"].ToString()));
                    BindStatesDropDown(Convert.ToInt32(ddlCountries.Items[countriesIndex].Value));

                    ddlStates.SelectedIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["StateName"].ToString()));
                    int statesIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["StateName"].ToString()));
                    BindCitiesDropDown(Convert.ToInt32(ddlStates.Items[statesIndex].Value));

                    ddlCities.SelectedIndex = ddlCities.Items.IndexOf(ddlCities.Items.FindByText(dt.Rows[0]["CityName"].ToString()));

                    txtAddressLine1.Value = dt.Rows[0]["AddressLine1"].ToString();
                    txtAddressLine2.Value = dt.Rows[0]["AddressLine2"].ToString();
                    txtZipCode.Value = dt.Rows[0]["ZIPCode"].ToString();
                    txtPM.Value = dt.Rows[0]["ProjectManagerName"].ToString();
                    txtCode.Value = dt.Rows[0]["SiteCode"].ToString();
                    txtEmail.Value = dt.Rows[0]["SiteEmail"].ToString();
                    txtPhone.Value = dt.Rows[0]["SitePhone"].ToString();
                    chkCW.Checked = Common.getCheckBoxValue(dt.Rows[0]["isCenteralWarehouse"].ToString());

                }
            }
            Common.addlog("View", "Admin", "Location \"" + txtName.Value + "\" Viewed", "FAM_LOCATION", objDB.LocationID);


        }
        private void clearFields()
        {
            txtDescription.Value = "";
            txtName.Value = "";
            txtPrefix.Value = "";
            txtEmail.Value = "";
            txtPhone.Value = "";
            txtAddressLine1.Value = "";
            txtAddressLine2.Value = "";
            txtZipCode.Value = "";
            txtCode.Value = "";
            txtPM.Value = "";
            ddlStates.SelectedIndex = 0;
            ddlCities.SelectedIndex = 0;
            chkCW.Checked = false;
            DataTable LocCode = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            LocCode = objDB.GetNewLocationCode(ref errorMsg);
            if (LocCode != null)
            {
                if (LocCode.Rows.Count > 0)
                {
                    txtLocationCode.Value = LocCode.Rows[0][0].ToString();
                }
            }
        }
        private bool CheckSessions()
        {
            if (Session["userid"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

            return true;
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.Description = txtDescription.Value;
                objDB.Name = txtName.Value;
                objDB.Prefix = txtPrefix.Value;
                objDB.Code = txtLocationCode.Value;

                objDB.CountryName = ddlCountries.SelectedItem.Text;
                objDB.StateName = ddlStates.SelectedItem.Text;
                objDB.CityName = ddlCities.SelectedItem.Text;
                objDB.AddressLine1 = txtAddressLine1.Value;
                objDB.AddressLine2 = txtAddressLine2.Value;
                objDB.ZIPCode = txtZipCode.Value;
                objDB.SiteCode = txtCode.Value;
                objDB.SiteName = txtName.Value;
                objDB.SitePhone = txtPhone.Value;
                objDB.SiteEmail = txtEmail.Value;
                objDB.ProjectManagerName = txtPM.Value;
                objDB.isCenteralWarehouse = chkCW.Checked;

                if (HttpContext.Current.Items["LocationID"] != null)
                {
                    // update coding
                    objDB.LocationID = LocID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateLocation();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddLocation();

                    clearFields();
                    // add coding
                }

                if (res == "New Location Added" || res == "Location Updated")
                {

                    if (res == "New Location Added")
                    {
                        //Common.addlog("Add", "Admin", "New Location \"" + objDB.Name + "\" Added", "FAM_LOCATION");
                        Common.addlogNew("Add", "Admin", "New Location \"" + objDB.Name + "\" Added", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/locations", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/locations/edit-location-" + objDB.LocationID, "New Location \"" + objDB.Name + "\" Added", "FAM_LOCATION", "Directors", Convert.ToInt32(objDB.LocationID));

                    }
                    if (res == "Location Updated")
                    {
                        //Common.addlog("Update", "Admin", "Location \"" + objDB.Name + "\" Updated", "FAM_LOCATION", objDB.LocationID);
                        Common.addlogNew("Update", "Admin", "Location \"" + objDB.Name + "\" Updated", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/locations", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/locations/edit-location-" + objDB.LocationID, "Location \"" + objDB.Name + "\" Updated", "FAM_LOCATION", "Directors", Convert.ToInt32(objDB.LocationID));

                    }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {


                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void BindCountriesDropDown()
        {
            ddlCountries.DataSource = objDB.GetAllCounties(ref errorMsg);
            ddlCountries.DataTextField = "Name";
            ddlCountries.DataValueField = "ID";
            ddlCountries.DataBind();
            ddlCountries.Items.Insert(0, new ListItem("--- Select Country ---", "0"));
            //ddlCountries.SelectedValue = "166";
        }
        private void BindStatesDropDown(int CountryID)
        {
            objDB.CountryID = CountryID;
            ddlStates.DataSource = objDB.GetAllStatesByCountryID(ref errorMsg);
            ddlStates.DataTextField = "Name";
            ddlStates.DataValueField = "ID";
            ddlStates.DataBind();
            ddlStates.Items.Insert(0, new ListItem("--- Select State ---", "0"));
        }
        private void BindCitiesDropDown(int StateID)
        {
            objDB.StateID = StateID;
            ddlCities.DataSource = objDB.GetAllCitiesByStateID(ref errorMsg);
            ddlCities.DataTextField = "Name";
            ddlCities.DataValueField = "Name";
            ddlCities.DataBind();
            ddlCities.Items.Insert(0, new ListItem("--- Select City ---", "0"));
        }
        protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CheckSessions())
            {
                BindCitiesDropDown(Convert.ToInt32(ddlStates.SelectedValue));
            }
        }
        protected void ddlCities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CheckSessions())
            {
                string shortName = "";
                shortName += Session["CompanyShortName"].ToString() + "_";
                shortName += ddlCities.SelectedItem.Text.Substring(0, 3).ToUpper() + "_";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.CityName = ddlCities.SelectedValue;
                shortName += objDB.GetProjectCodeForNewItem();
                txtCode.Value = shortName;
            }
        }
        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CheckSessions())
            {
                BindStatesDropDown(Convert.ToInt32(ddlCountries.SelectedValue));
            }
        }
    }
}