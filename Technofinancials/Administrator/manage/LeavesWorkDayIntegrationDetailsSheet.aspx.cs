﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.manage
{
    public partial class LeavesWorkDayIntegrationDetailsSheet : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/workday-sheet";

                if (HttpContext.Current.Items["MasterID"] != null)
                {
                    objDB.MasterId = Convert.ToInt32(HttpContext.Current.Items["MasterID"].ToString());
                   
                    DataTable dtt = new DataTable();
                    dtt = objDB.GetAllWorkDaySheetDetails(ref errorMsg);
                    gv.DataSource = dtt;
                    gv.DataBind();

                
                }
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }
    }
}