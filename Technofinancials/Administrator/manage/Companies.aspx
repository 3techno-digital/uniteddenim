﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Companies.aspx.cs" Inherits="Technofinancials.Administrator.manage.Companies" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
<form id="form1" runat="server">
    <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>

    <uc:Header ID="header1" runat="server"></uc:Header>


    <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
    <!-- APP MAIN ==========-->

    <main id="app-main" class="app-main">
        <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
            <ProgressTemplate>
                <div class="upd_panel">
                    <div class="center">
                        <img src="/assets/images/Loading.gif" />
                    </div>


                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <div class="wrap">

            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <h1 class="m-0 text-dark">Company Details</h1>
                        </div>
                      

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div style="text-align: right;">

                                <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                    <ContentTemplate>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <button class="AD_btn" id="Button1" runat="server" onserverclick="button_ServerClick" validationgroup="btnValidate">Save</button>
                                <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/company-profile"); %>" class="AD_btn">Back</a>
                                <%--<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/child-companies"); %>" class="AD_btn">Back</a>--%>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>


            <section class="app-content">
                <%--      <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <img src="/assets/images/companies-2.png" class="img-responsive tf-page-heading-img" />
                        <h3 class="tf-page-heading-text">Company Details test</h3>
                    </div>
                    <div class="col-sm-4" style="margin-top: 10px;">
                        <div class="pull-right">
                            <button class="AD_btn" "Save" id="btnSave" runat="server" onserverclick="button_ServerClick" validationgroup="btnValidate">Save</button>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/child-companies"); %>" class="tf-back-btn" "Back"><i class="fas fa-arrow-left"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <hr />
                    </div>
                </div>--%>

                <div class="row">

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h4>Company Name <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCompanyName"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator></h4>
                                    <input type="text" class="form-control" placeholder="Company Name" name="companyName" id="txtCompanyName" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h4>Short Name <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCompanyShortName"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator></h4>
                                    <input type="text" class="form-control" placeholder="Company Short Name" name="companyShortName" id="txtCompanyShortName" runat="server" />
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="form-group">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtCompnayCode"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <h4>CompanyCode  <span style="color: red !important;">*</span></h4>
                                    <input type="text" disabled="disabled" class="form-control" placeholder="Company Code" name="compnayCode" id="txtCompnayCode" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h4>NTN </h4>
                                    <input type="text" class="form-control" placeholder="Company NTN" name="compnayNtn" id="txtCompnayNtn" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h4>STRN  </h4>
                                    <input type="text" class="form-control" placeholder="Company STRN" name="companyStrn" id="txtCompanyStrn" runat="server" />
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h4>SECP  </h4>
                                    <input type="text" class="form-control" placeholder="Company SECP" name="companySecp" id="txtCompanySecp" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h4>ARN  </h4>
                                    <input type="text" class="form-control" placeholder="Company ARN" name="companyArn" id="txtCompanyArn" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h4>STN </h4>
                                    <input type="text" class="form-control" placeholder="Company STN" name="companyStn" id="txtCompanyStn" runat="server" />
                                </div>
                            </div>

                            <asp:UpdatePanel ID="upd1" runat="server">
                                <ContentTemplate>
                                    <div class="col-sm-6 form-group">
                                        <h4>Country <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCountries" InitialValue="0"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                        <asp:DropDownList ID="ddlCountries" runat="server" class="form-control select2" data-plugin="select2" OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <h4>State <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlStates" InitialValue="0"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                        <asp:DropDownList ID="ddlStates" runat="server" class="form-control select2" data-plugin="select2" OnSelectedIndexChanged="ddlStates_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <h4>City <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlCities" InitialValue="0"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                        <asp:DropDownList ID="ddlCities" runat="server" class="form-control select2" data-plugin="select2"></asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="col-sm-6 form-group">
                                <h4>Zip code</h4>
                                <input class="form-control" placeholder="Zip code" type="Number" name="zipCode" id="txtZIPCode" runat="server" />
                            </div>

                            


                        </div>
                    </div>

                    <%--four col off--%>

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <h4>Address Line 1 <span style="color: red !important;">*</span>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtAddressLine1"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                <input class="form-control" placeholder="Address Line 1" type="text" name="addressLine1" id="txtAddressLine1" runat="server" />
                            </div>
                            <div class="col-sm-12 form-group">
                                <h4>Address Line 2</h4>
                                <input class="form-control" placeholder="Address Line 2" type="text" id="txtAddressLine2" runat="server" />
                            </div>


                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h4>Description</h4>
                                    <textarea class="form-control" rows="5" name="companyDescription" id="txtCompanyDescription" runat="server"></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4></h4>
                                </div>
                                <div class="file-upload">
                                    <div class="image-upload-wrap">
                                        <%--<asp:FileUpload ID="updLogo" onchange="readURL(this);" runat="server" ClientIDMode="Static" accept=".png,.jpg,.jpeg,.gif" />--%>
                                        <input class="file-upload-input" runat="server" type="file" name="uploadFile" id="updLogo"  onchange="readURL(this);" accept="image/*" />
                                        <div class="drag-text">
                                            <h3>Drag and drop a file or select add Image (MaxSize: 2mb)</h3>
                                        </div>
                                    </div>
                                    <div class="file-upload-content">
                                        <img class="file-upload-image" src="/assets/images/3techno-Logo.png" alt="Company Image" runat="server" id="imgLogo" />
                                        <div class="image-title-wrap">
                                            <button type="button" onclick="removeUpload()" class="remove-image">
                                                Remove  <span><i class="fa fa-trash" aria-hidden="true"></i></span>

                                            </button>
                                        </div>
                                    </div>
                                </div>
                           
                                
                            </div>
                        </div>
                    </div>

                    <%--four col off--%>

                    <div class="clearfix"></div>

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="row">
                              <div class="col-md-12">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        </div>
                    </div>

                    <%--four col off--%>
                </div>
                <%--main row--%>



                <%--                    <div class="row">
                    <div class="col-sm-9">
                        <div class="row">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="user-img-div">
                            <img src="/assets/images/3techno-Logo.png" class="img-responsive" id="imgLogo" runat="server" clientidmode="static" />
                            <asp:FileUpload ID="updLogo" runat="server" ClientIDMode="Static" Style="display: none;" accept=".png,.jpg,.jpeg,.gif" />
                            <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('updLogo').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>--%>




                <div id="SubComapny" runat="server">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <section class="content">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h3 class="text-dark text_saprate">CEO</h3>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h4>First Name <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtCEOFirstName"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                                <input class="form-control" placeholder="First Name" type="text" id="txtCEOFirstName" runat="server" name="firstName" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h4>Middle Name</h4>
                                                <input class="form-control" placeholder="Middle Name" type="text" id="txtCEOMiddleName" runat="server" name="middleName" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h4>Last Name <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtCEOLastName"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                                <input class="form-control" placeholder="Last Name" type="text" id="txtCEOLastName" runat="server" name="lastName" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h4>Email address <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="emailCEOAddress"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic" runat="server" ControlToValidate="emailCEOAddress" ForeColor="Red" ErrorMessage=" Email Address Not Valid" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="btnValidate"></asp:RegularExpressionValidator>--%></h4>

                                                <input class="form-control" placeholder="Email address" type="email" name="email" id="emailCEOAddress" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                  
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <section class="content">
                                <div class="row">
                                     <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h3 class="text_saprate text-dark">Director HR</h3>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h4>First Name <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtHRFirstName"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                                <input class="form-control" placeholder="First Name" type="text" id="txtHRFirstName" runat="server" name="firstName" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h4>Middle Name</h4>
                                                <input class="form-control" placeholder="Middle Name" type="text" id="txtHRMiddleName" runat="server" name="middleName" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h4>Last Name <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtHRLastName"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                                <input class="form-control" placeholder="Last Name" type="text" id="txtHRLastName" runat="server" name="lastName" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h4>Email address <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="emailHRAddress"  Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" runat="server" ControlToValidate="emailHRAddress" ForeColor="Red" ErrorMessage=" Email Address Not Valid" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="btnValidate"></asp:RegularExpressionValidator>--%></h4>
                                                <input class="form-control" placeholder="Email address" type="email" name="email" id="emailHRAddress" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                                <div class="clearfix">&nbsp;</div>
                                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">&nbsp;</div>
            </section>
            <!-- #dash-content -->
        </div>
        <!-- .wrap -->
        <uc:Footer ID="footer1" runat="server"></uc:Footer>
    </main>

    <!--========== END app main -->
    <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imgLogo').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#updLogo").change(function () {
            readURL(this);
        });
    </script>
    <script>


        $(document).ready(function () {
            $('.image-upload-wrap').hide();
            $('.file-upload-content').show();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var ext = input.files[0].name.split('.').pop().toLowerCase();
                if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
                    removeUpload();
                    alert('invalid extension!');
                }
                else {
                    var reader = new FileReader();
                    var myimg = '';
                    reader.onload = function (e) {
                        $('.image-upload-wrap').hide();
                        $('.file-upload-image').attr('src', e.target.result);
                        $('.file-upload-content').show();
                        $('.image-title').html(input.files[0].name);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            else {
                removeUpload();
            }
        }
        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }
        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
            $('.image-upload-wrap').removeClass('image-dropping');
        });
    </script>

    <style>
        .month-table-show {
            display: none;
            margin-top: 40px;
        }

        .modal-dialog {
            width: 450px;
            margin: 30px auto;
        }

        section.app-content {
            padding-bottom: 53px;
        }

        .modal-footer {
            border-top: 0px solid #e5e5e5;
        }

        .hiring-create-new-btn {
            margin-top: 17px;
            border: none;
            background: none;
            color: #01769a;
        }

        .user-img-div img {
            width: 169px !important;
            display: block;
            margin: 0 auto;
        }

        .user-img-div {
            width: auto;
        }
    </style>
    <style>
        .tf-back-btn {
            background-color: #575757;
            padding: 10px 10px 10px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-back-btn i {
                color: #fff !important;
            }
    </style>
    <script>
        function changeLogo(path) {
            $('#imgCompanyLogo').prop('src', path)
        }

    </script>

</form>

</body>
</html>

