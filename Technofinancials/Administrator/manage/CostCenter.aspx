﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CostCenter.aspx.cs" Inherits="Technofinancials.Administrator.manage.CostCenter" %>


                    <%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="upd1" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">
                <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>
                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <h1 class="m-0 text-dark">Cost Center</h1>
                                    </div>
                                    <!-- /.col -->


                                    <!-- /.col -->
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div style="text-align: right;">
                                            <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                            <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>



                        <section class="app-content">
                            <div class="row">
                                <div class="tab-content ">
                                    <div class="tab-pane active">
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="row">
                                                <%-- <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <h4>Company</h4>
                                            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged"></asp:DropDownList>
                                        </div>--%>
                                                 <div class="col-sm-12">
                                                    <h4>Code</h4>
                                                    <input class="form-control" id="TxtCode" placeholder="Code" readonly="true" type="text" runat="server" />
                                                </div>
                                                <div class="col-sm-12">
                                                    <h4>Name <span style="color: red !important;">*</span>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                                    <input class="form-control" id="txtName" placeholder="Name" type="text" runat="server" />
                                                </div>
                                                                                    
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <h4>Description</h4>
                                                        <textarea class="form-control" rows="5" id="comment" runat="server"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="row">
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                        <ContentTemplate>
                                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                                    <span>
                                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                                    </span>
                                                                    <p id="pAlertMsg" runat="server">
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                                                                                      <div class="clearfix">&nbsp;</div>
          <div class="clearfix">&nbsp;</div>
          <div class="clearfix">&nbsp;</div>
          <div class="clearfix">&nbsp;</div>
                    <br />
                    <br />
                    <br />
                        </section>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
        </style>
        <!-- Modal -->
    </form>
</body>
</html>
