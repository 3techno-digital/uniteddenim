﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.manage
{
    public partial class Department : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/view/departments";


                BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
                clearFields();
                divAlertMsg.Visible = false;

                if (HttpContext.Current.Items["DepartmentID"] != null)
                {
                    getDepartmentByID(Convert.ToInt32(HttpContext.Current.Items["DepartmentID"].ToString()));
                }
            }
        }

        private void getDepartmentByID(int departmentID)
        {
            DataTable dt = new DataTable();
            objDB.DeptID = departmentID;
            dt = objDB.GetDepartmentByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    //ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(dt.Rows[0]["CompanyID"].ToString()));

                    //if (ddlCompany.SelectedIndex == -1)
                    //    ddlCompany.SelectedIndex = 0;

                    BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
                    ddlDept.SelectedIndex = ddlDept.Items.IndexOf(ddlDept.Items.FindByValue(dt.Rows[0]["DeptParentID"].ToString()));
                    txtName.Value = dt.Rows[0]["DeptName"].ToString();
                    comment.Value = dt.Rows[0]["DeptDesc"].ToString();
                }
            }
            Common.addlog("View", "Admin", "Department \"" + txtName.Value + "\" Viewed", "Departments", objDB.DeptID);


        }

        private void clearFields()
        {
            txtName.Value = "";
            //ddlCompany.SelectedIndex = 0;
            ddlDept.SelectedIndex = 0;
            comment.Value = "";
        }

        private void BindDepartmentDropDown(int companyID)
        {
            objDB.CompanyID = companyID;
            ddlDept.DataSource = objDB.GetAllCostCentersByCompanyID(ref errorMsg);
            ddlDept.DataTextField = "CostCenterName";
            ddlDept.DataValueField = "CostCenterID";
            ddlDept.DataBind();
            ddlDept.Items.Insert(0, new ListItem("--- None ---", "0"));
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DeptParentID = Convert.ToInt32(ddlDept.SelectedItem.Value);
            objDB.DeptName = txtName.Value;
            objDB.DeptDesc = comment.Value;

            string res = "";
            if (HttpContext.Current.Items["DepartmentID"] != null)
            {
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.DeptID = Convert.ToInt32(HttpContext.Current.Items["DepartmentID"].ToString());
                //res = objDB.UpdateDepartment();
            }
            else
            {
                objDB.CreatedBy = Session["UserName"].ToString();
                res = objDB.AddDepartment();

                int DepID = 0;

                if (int.TryParse(res, out DepID))
                {
                    objDB.DeptID = DepID;
                    res = "New Department Added";
                    clearFields();
                }
            }
            if (res == "New Department Added" || res == "Department Updated")
            {
                if (res == "New Department Added")
                {
                    //Common.addlog("Add", "Admin", "New Department \"" + objDB.DeptName + "\" Added", "Departments"); 
                    Common.addlogNew("Add", "Directors", "Department \"" + objDB.DeptName + "\" Added", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/departments", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/departments/edit-department-" + objDB.DeptID, "New Department \"" + objDB.DeptName + "\" Added", "Departments", "Directors", Convert.ToInt32(objDB.DeptID));
                }
                if (res == "Department Updated")
                {
                    //Common.addlog("Update", "Admin", "Department \"" + objDB.DeptName + "\" Updated", "Departments", objDB.DeptID); 
                    Common.addlogNew("Update", "Directors", "Department \"" + objDB.DeptName + "\" Updated", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/departments", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/manage/departments/edit-department-" + objDB.DeptID, "Department \"" + objDB.DeptName + "\" Updated", "Departments", "Directors", Convert.ToInt32(objDB.DeptID));

                }



                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }
    }
}