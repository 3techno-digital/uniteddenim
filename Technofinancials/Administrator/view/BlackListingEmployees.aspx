﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BlackListingEmployees.aspx.cs" Inherits="Technofinancials.Administrator.view.BlackListingEmployees" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">

                

                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <h1 class="m-0 text-dark">BlackListed Employees</h1>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                        <div style="text-align: right;">
                                        
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>




                <section class="app-content">
                    
                    <div class="row ">
                        <div class="tab-content ">
                            <div class="tab-pane active">
                                <div class="col-sm-12">
                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Department">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Desgnation">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("DesgTitle") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Grade">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol5" Text='<%# Eval("GradeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="is Black Listed">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol6" Text='<%# Eval("isBlackListed") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Black Listed By">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol7" Text='<%# Eval("BlackListedBy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="BlackListed Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol8" Text='<%# Eval("BlackListedDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="BlackListing Reason">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol9" Text='<%# Eval("BlackListReason") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <%--  <asp:TemplateField HeaderText="UnBlackList By">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol10" Text='<%# Eval("UnBlackListBy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="UnBlackListed Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol11" Text='<%# Eval("UnBlackListDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="UnBlackList Reason">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol12" Text='<%# Eval("UnBlackListReason") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                               <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <a href="<%# Eval("BlackListAttachment") %>" "Download">Attachment</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                               <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <a onclick="showModal('<%# Eval("EmployeeID") %>')" data-toggle="modal" data-target="#myModal" style="cursor:pointer;">Revoke</a>
                                                     <%--<asp:LinkButton ID="lnkModal" runat="server" CssClass="delete-class" "Toggle Black Listing" CommandArgument='<%# Eval("EmployeeID") %>' OnClick="lnkModal_Click">Toggle Black Listing</asp:LinkButton>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <div class="modal fade M_set" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="m-0 text-dark">Reason</h1>
                        <div class="add_new">
                            <button type="button" class="AD_btn" id="btnToggleBlackListing" runat="server" onserverclick="btnToggleBlackListing_ServerClick">Save</button>
                            <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <p>
                            <asp:HiddenField ID="hdnEmployeeID" runat="server" ClientIDMode="Static"/>
                            <textarea id="txtReason" runat="server" rows="5" placeholder="Reason.." class="form-control"></textarea>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }
        </style>
        <script>
            function showModal(id) {
                $('#hdnEmployeeID').val(id);
                //$('#myModal').show();
            }
        </script>
    </form>
</body>
</html>
