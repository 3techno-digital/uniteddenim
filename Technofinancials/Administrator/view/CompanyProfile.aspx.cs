﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.view
{
    public partial class CompanyProfile : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected static int companyID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            { 
                Session["UserAccess"] = Session["OldUserAccess"];
                if (Session["CompanyID"] != null)
                {
                    companyID = Convert.ToInt32(Session["CompanyID"].ToString());
                    getCompanyByID(companyID);
                }
            }
        }

        private void getCompanyByID(int companyID)
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = companyID;
            dt = objDB.GetCompanyID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    imgLogo.Src = dt.Rows[0]["CompanyLogo"].ToString();
                    imgLogo.Attributes["src"] = dt.Rows[0]["CompanyLogo"].ToString();

                    txtAddress.Text = dt.Rows[0]["AddressLine1"].ToString() + " " + dt.Rows[0]["AddressLine2"].ToString() + ", " + dt.Rows[0]["City"].ToString() + ", " + dt.Rows[0]["State"].ToString() + ", " + dt.Rows[0]["Country"].ToString() + " - " + dt.Rows[0]["ZIPCode"].ToString();
                    txtCompanyName.Text = dt.Rows[0]["CompanyName"].ToString();
                    txtCompanyShortName.Text = dt.Rows[0]["CompanyShortName"].ToString();
                    txtCompanyDescription.Text = dt.Rows[0]["CompanyDescription"].ToString();

                    txtCompnayNtn.Text = dt.Rows[0]["CompanyNTN"].ToString();
                    txtCompanyStrn.Text = dt.Rows[0]["CompanySTRN"].ToString();
                    txtCompanySecp.Text = dt.Rows[0]["CompanySECP"].ToString();
                    txtCompanyStn.Text = dt.Rows[0]["CompanySTN"].ToString();
                    txtCompanyArn.Text = dt.Rows[0]["CompnayARN"].ToString();

                    txtCEO.Text = dt.Rows[0]["CEOName"].ToString();
                }
            }
            Common.addlog("View", "Admin", "Company \"" + txtCompanyName.Text + "\" Viewed", "Companies", objDB.CompanyID);

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }
    }
}