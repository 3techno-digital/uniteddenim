﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logs.aspx.cs" Inherits="Technofinancials.Administrator.view.Logs" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Audit Logs</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                 
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>


                <section class="app-content">
                       <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>From Date
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtFromDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>To Date
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtToDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                   
                                                                <div class="col-md-12">
                                    <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
                                </div>
                            </div>
                        </div>
                    <div class="row ">
                        <div class="tab-content ">
                            <div class="tab-pane active">
                                <div class="col-sm-12 adminpanel-padding-bottom">
                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                             <asp:TemplateField HeaderText="S No.">
                                                <ItemTemplate>
                                                      <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Date/Time">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("ActionDateTime") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--    <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("Actions") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Activity">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("Actions") +" - "+ Eval("Description") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Module">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("Module") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="User Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("UserName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="IP Address">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("LoginIP") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol4" Text='<%#  Eval("LoginCityName")  + ", " + Eval("LoginState") +", "+ Eval("LoginCountryName") +"." %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Lat, Lang">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol4" Text='<%#  Eval("LoginLatitude")  + ", " + Eval("LoginLongitude") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--                                              <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <a href= "<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/directors/manage/job-descriptions/edit-job-description-" + Eval("NodeID") %>" class="edit-class" > Edit</a>
                                                     <asp:LinkButton ID="lnkDelete" runat="server" CssClass="delete-class"  CommandArgument='<%# Eval("NodeID") %>'  OnClick="lnkDelete_Click"> Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                       
                                    </asp:GridView>
                                    <%-- <table id="tb" class="gv table table-responsive table-hover">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Employee Code</th>
                                                <th>Department</th>
                                                <th>Designation</th>
                                                <th>Edit / Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
                                    @media only screen and (max-width: 992px) and (min-width: 768px) {
                div#gv_wrapper{
    overflow-x: scroll;
                    overflow-y: hidden;
                padding-bottom: 35px;
}
        /* Track */
                    div#gv_wrapper::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            div#gv_wrapper::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            div#gv_wrapper::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }
            }
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }
            div#gv_wrapper {
    margin-bottom: 15%;
}
                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }
            th.sorting_asc{
                width:0px;
            }
                .tf-disapproved-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }
            @media only screen and (max-width: 991px) and (min-width: 768px) {
                div#gv_wrapper table#gv::-webkit-scrollbar-thumb {
                    background-color: #003780;
                    border: 2px solid #003780;
                    border-radius: 10px;
                }

                div#gv_wrapper table#gv ::-webkit-scrollbar {
                    height: 10px;
                    background-color: #fff;
                }

                div#gv_wrapper table#gv {
                    width: 1024px;
                }

                div#gv_wrapper table#gv {
                    overflow-x: scroll;
                    overflow-y: hidden;
                    padding-bottom: 35px;
                }
            }
            /*gridview*/
.table table  tbody  tr  td a ,
.table table  tbody  tr  td  span {
position: relative;
float: left;
padding: 6px 12px;
margin-left: -1px;
line-height: 1.42857143;
color: #337ab7;
text-decoration: none;
background-color: #fff;
border: 1px solid #ddd;

}

.table table > tbody > tr > td > span {
z-index: 3;
color: #fff;
cursor: default;
background-color: #337ab7;
border-color: #337ab7;
}

.table table > tbody > tr > td:first-child > a,
.table table > tbody > tr > td:first-child > span {
margin-left: 0;
border-top-left-radius: 4px;
border-bottom-left-radius: 4px;
}

.table table > tbody > tr > td:last-child > a,
.table table > tbody > tr > td:last-child > span {
border-top-right-radius: 4px;
border-bottom-right-radius: 4px;
}

.table table > tbody > tr > td > a:hover,
.table   table > tbody > tr > td > span:hover,
.table table > tbody > tr > td > a:focus,
.table table > tbody > tr > td > span:focus {
z-index: 2;
color: #23527c;
background-color: #eee;
border-color: #ddd;
}
            table {
        width: 30%;
            }
        </style>
    </form>
</body>
</html>
