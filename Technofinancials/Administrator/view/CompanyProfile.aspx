﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyProfile.aspx.cs" Inherits="Technofinancials.Administrator.view.CompanyProfile" %>
<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <asp:UpdatePanel ID="UpdPnl" runat="server">
            <ContentTemplate>
                <main id="app-main" class="app-main">
                    <div class="wrap">

                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <h1 class="m-0 text-dark">Company Profile</h1>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                        <div style="text-align: right;">
                                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/directors/manage/companies/edit-company-" + Session["CompanyID"]); %>" class="AD_btn">Edit</a>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>

                  <%--      <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h3 class="widget-title">Company Profile</h3>
                            </div>
                            <div class="col-sm-4">
                                <div style="text-align: right; margin-top: 20px;">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/directors/manage/companies/edit-company-" + Session["CompanyID"]); %>" class="AD_btn" >Edit</a>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr />
                            </div>
                        </div>--%>




                        <section class="app-content">
                           
                            <div class="row">
                                <div class="col-lg-6 col-lg-offset-3">
                                      <a href="/" ><img src="/assets/images/3techno-Logo.png" class="img-responsive company-logo-img" id="imgLogo" runat="server" style="margin:0 auto;" clientidmode="static" /> </a> 
                                </div>
                                 <div class="col-lg-6 col-lg-offset-3" style="text-align:center;">
                                     <asp:Label ID="txtCompanyName" runat="server" style="font-weight:bold;font-size:20px;" ></asp:Label>
                                </div>
                                 <div class="col-lg-6 col-lg-offset-3" style="text-align:center;">
                                     <asp:Label ID="txtCompanyShortName" runat="server" style="font-weight:bold;font-size:18px;" ></asp:Label>
                                </div>
                                 <div class="col-lg-6 col-lg-offset-3" style="text-align:center;">
                                     <asp:Label ID="txtCompanyDescription" runat="server"></asp:Label>
                                </div>
                                 <div class="col-lg-6 col-lg-offset-3" style="text-align:center;">
                                     <asp:Label ID="txtCEO" runat="server"></asp:Label>
                                </div>
                                 <div class="col-lg-6 col-lg-offset-3" style="text-align:center;">
                                    <asp:Label ID="txtAddress" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <table class="table table-responsive table-striped" style="    background:#fff /*#e8e8e8*/;">
                                        <tr>
                                            <td>
                                                NTN:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCompnayNtn" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                          <tr>
                                            <td>
                                                STRN:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCompanyStrn" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                          <tr>
                                            <td>
                                                SECP:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCompanySecp" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                          <tr>
                                            <td>
                                                ARN:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCompanyArn" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                          <tr>
                                            <td>
                                                STN:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCompanyStn" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>  
                                <br />
                                <br />
                        </section>
                        <!-- #dash-content -->
                    </div>
                    <!-- .wrap -->
                    <uc:Footer ID="footer1" runat="server"></uc:Footer>
                </main>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#imgLogo').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#updLogo").change(function () {
                readURL(this);
            });
        </script>

        <style>
            .month-table-show {
                display: none;
                margin-top: 40px;
            }

            .modal-dialog {
                width: 450px;
                margin: 30px auto;
            }

            section.app-content {
                padding-bottom: 53px;
            }

            .modal-footer {
                border-top: 0px solid #e5e5e5;
            }

            .hiring-create-new-btn {
                margin-top: 17px;
                border: none;
                background: none;
                color: #01769a;
            }

            .user-img-div img {
                width: 169px !important;
                display: block;
                margin: 0 auto;
            }

            .user-img-div {
                width: auto;
            }
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

                            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }
               @media only screen and (max-width: 767px){
                .app-content {
                    margin: 25px 25px 150px 40px;
                }
                .tf-footer-wrapper {
                    width: 100%;
                }
                .app-footer .footer-menu li:last-child {
    float: none; 
}
                .tf-color-white.tf-font-times-new-roman.tf-mt-5{
                    text-align:center;
                }
                .content-header h1 {
                    font-size: 21px !important;
                }
                .tf-footer {
                    padding: 3px 0px 3px 0;
                }
               }
        </style>
    </form>

</body>
</html>