﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobDescriptions.aspx.cs" Inherits="Technofinancials.Administrator.view.JobDescriptions" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>
		.content-header.second_heading {
			margin: 15px -13px 0 !important;
		}
	</style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">
			<div class="wrap">
				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
								<h1 class="m-0 text-dark">Job Description</h1>
							</div>
							<!-- /.col -->
							<div class="col-sm-4">
								<div style="text-align: right;">
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/manage/job-descriptions/add-new-job-description"); %>" class="AD_btn">Add</a>
								</div>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.container-fluid -->
				</div>

				<section class="app-content">

					<div class="row ">
						<div class="tab-content ">
							<div class="tab-pane active">
								<div class="col-sm-12 adminpanel-padding-bottom">
									<asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
										<Columns>
											<asp:TemplateField HeaderText="Sr.No.">
												<ItemTemplate>
													<%#Container.DataItemIndex+1 %>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Title">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblIndex" Text='<%# Eval("title") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<%--<asp:TemplateField HeaderText="Description">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblDes" Text='<%# Eval("TitleDesc") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>--%>
										<%--	<asp:TemplateField HeaderText="Designation">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblDeg" Text='<%# Eval("DesgName") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>--%>
											<asp:TemplateField HeaderText="View">
												<ItemTemplate>
													<a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/directors/manage/job-descriptions/edit-job-description-" + Eval("TitleID") %>" class="edit-class">Edit</a>
													<asp:LinkButton ID="LinkButton" runat="server" CssClass="delete-class" OnClick="lnkDelete_Click" CommandArgument='<%# Eval("TitleID") %>'>Delete</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</asp:GridView>

								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- #dash-content -->
			</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>
	</form>
</body>
</html>
