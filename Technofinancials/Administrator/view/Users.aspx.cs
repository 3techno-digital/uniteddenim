﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.view
{
    public partial class Users : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            { 
                GetData();
            }
        }

        private void GetData()
        {
            CheckSessions();

            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetAllUsersByCompanyID(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "Admin", "All Users Viewed", "Users");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            int ID = Convert.ToInt32(btn.CommandArgument);
            
            
            if (ID != Convert.ToInt32(Session["UserID"].ToString()))
            {
                objDB.UserID = ID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteUser();
                Common.addlog("Delete", "Admin", "User of ID \"" + objDB.UserID + "\" deleted", "Users", objDB.UserID);
                //Session["UserID"] = null;
                //Session["EmployeeID"] = null;
            }
            GetData();
        }

        protected void lnkReset_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            int ID = Convert.ToInt32(btn.CommandArgument);
            objDB.UserID = ID;
            objDB.ModifiedBy = Session["UserName"].ToString();
            string Email=objDB.ResetUserPassword();


            try
            {
                string html = "";
                html = "Your New Password is 1234 kindly Login And Update your Password <br /> ";
                MailMessage mail = new MailMessage();
                mail.Subject = "User Account Credentials for Technofinancials";
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString(), "Techno Financials");
                mail.To.Add(Email);
                mail.Body = html;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SenderSMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SenderPort"].ToString()));
                smtp.EnableSsl = true;
                NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["SenderPassword"].ToString());
                smtp.Credentials = netCre;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                smtp.Send(mail);
            }
            catch (Exception ex)
            {

            }


            Common.addlog("Reset", "Admin", "User of ID \"" + objDB.UserID + "\" Password Reset", "Users", objDB.UserID);

            GetData();
        }
    }
}