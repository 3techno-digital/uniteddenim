﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.view
{
    public partial class OrganizationChart : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                hdnCompanyName.Value = Session["CompanyName"].ToString();

                string str = "<script type=\"text/javascript\">";
                str += "$(document).ready(function () {";
                str += "var peopleElement = document.getElementById(\"people\");";
                str += "var orgChart = new getOrgChart(peopleElement, {";
                str += "primaryFields: [\"title\"],";
                str += "secondParentIdField: \"secondParenId\",";
                str += "dataSource: [";
                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                dt = objDB.GetAllDesignationByCompanyIDForChart(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == dt.Rows.Count - 1)
                            {
                                str += "{ id: " + dt.Rows[i]["DesgID"] + ", parentId: " + ((dt.Rows[i]["DirectReportingTo"].ToString() == "") ? "null" : dt.Rows[i]["DirectReportingTo"]) + ", title: \"" + dt.Rows[i]["DesgTitle"] + "\", secondParenId:" + ((dt.Rows[i]["InDirectReportingTo"].ToString() == "") ? "null" : dt.Rows[i]["InDirectReportingTo"]) + "}";
                            }
                            else
                            {
                                str += "{ id: " + dt.Rows[i]["DesgID"] + ", parentId: " + ((dt.Rows[i]["DirectReportingTo"].ToString() == "") ? "null" : dt.Rows[i]["DirectReportingTo"]) + ", title: \"" + dt.Rows[i]["DesgTitle"] + "\", secondParenId:" + ((dt.Rows[i]["InDirectReportingTo"].ToString() == "") ? "null" : dt.Rows[i]["InDirectReportingTo"]) + "},";
                            }
                        }

                    }
                }


                str += "],";
                str += "enableExportToImage: true";
                str += " });";
                str += "});";
                str += "</script>";
                str += "";

                ltrOrgScript.Text = str;


                //GetData();
            }
        }

        private void GetData()
        {
            CheckSessions();

            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetAllDesignationByCompanyIDForChart(ref errorMsg);
           // dt = objDB.GetAllEmployeesByCompanyIDForChart(ref errorMsg);
            if (dt!= null)
            {
                if (dt.Rows.Count > 0)
                {
                    string strTable = "";
                    strTable += "<table id='orgData'>";
                    strTable += "<thead><tr><th>id</th><th>parent id</th><th>title</th><th>secondParenId</th></tr></thead>";
                    strTable += "<tbody>";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strTable += "<tr><td>" + dt.Rows[i]["DesgID"] + "</td><td>" + dt.Rows[i][7] + "</td><td>" + dt.Rows[i]["DesgTitle"] + "</td><td>" + dt.Rows[i][8] + "</td></tr>";
                        // strTable += "<tr><td>" + dt.Rows[i]["EmployeeID"] + "</td><td>" + dt.Rows[i]["DirectReportingPerson"] + "</td><td>" + dt.Rows[i]["EmployeeName"] + " - "+ dt.Rows[i]["DesgTitle"] + "</td><td>" + dt.Rows[i]["InDirectReportingPerson"] + "</td></tr>";
                    }
                    strTable += "</tbody></table>";
                    divTable.InnerHtml = strTable;
                }
            }
            Common.addlog("ViewAll", "Admin", "Organization Chart Viewed", "OrgChart");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }
    }
}