﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Teams.aspx.cs" Inherits="Technofinancials.Administrator.view.Teams" %>



<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
<form id="form1" runat="server">
<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
<uc:Header ID="header1" runat="server"></uc:Header>
<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
    <div class="wrap">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <h1 class="m-0 text-dark">Team</h1>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4">
                        <div style="text-align: right; display:none;">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/manage/job-descriptions/add-new-job-description"); %>" class="AD_btn">Add</a>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

        <section class="app-content">
            <%-- <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="row">

                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="row">

                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="row">

                    </div>
                </div>
            </div>--%>
            <div class="row ">
                <div class="tab-content ">
                    <div class="tab-pane active">
                        <div class="col-sm-12 adminpanel-padding-bottom">
                            <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                <Columns>

                                    <asp:TemplateField HeaderText="Title">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("NodeTitle") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("GradeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("DeptName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("DesgTitle") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/directors/manage/job-descriptions/edit-job-description-" + Eval("NodeID") %>" class="edit-class" >Edit</a>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CssClass="delete-class"  CommandArgument='<%# Eval("NodeID") %>' OnClick="lnkDelete_Click"> Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <%-- <table id="tb" class="gv table table-responsive table-hover">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Employee Code</th>
                                        <th>Department</th>
                                        <th>Designation</th>
                                        <th>Edit / Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>--%>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- #dash-content -->
    </div>
    <!-- .wrap -->
    <uc:Footer ID="footer1" runat="server"></uc:Footer>
</main>
<!--========== END app main -->
<uc:Scripts ID="script1" runat="server"></uc:Scripts>
    <style>
            .content-header.second_heading {
                margin: 15px -13px 0 !important;
            }
        </style>
<style>
    div#gv_wrapper {
    margin-bottom: 15%;
}
    .tf-note-btn {
        background-color: #575757;
        padding: 10px 8px 7px 10px;
        border-radius: 100px;
        border: none !important;
        color: #fff;
    }

        .tf-note-btn i {
            color: #fff !important;
        }

    .tf-disapproved-btn {
        background-color: #575757;
        padding: 10px 8px 7px 10px;
        border-radius: 100px;
        border: none !important;
        color: #fff;
    }

        .tf-disapproved-btn i {
            color: #fff !important;
        }

    .tf-add-btn {
        background-color: #575757;
        padding: 12px 10px 8px 10px !important;
        border-radius: 100px;
        border: none !important;
        color: #fff;
    }

        .tf-add-btn i {
            color: #fff !important;
        }
</style>
</form>
</body>
</html>


