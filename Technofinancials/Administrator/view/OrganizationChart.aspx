﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrganizationChart.aspx.cs" Inherits="Technofinancials.Administrator.view.OrganizationChart" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/getorgchart.css">
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">

                <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <h1 class="m-0 text-dark">Organizational Chart</h1>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                        <div style="text-align: right;">
                                         
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>




                <section class="app-content">
                    

                    <div class="row ">
                        <div class="tab-content ">
                            <div class="tab-pane active">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div id="people"></div>
                                            <div id="divTable" runat="server" style="display: none;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
          <asp:HiddenField ID="hdnCompanyName" runat="server" />
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            [data-btn-action="add"] {
                display: none !important;
            }

            [data-btn-action="edit"] {
                display: none !important;
            }

            [data-btn-action="del"] {
                display: none !important;
            }

            [data-btn-action="details"] {
                display: none !important;
            }
        </style>
        <script src="/assets/js/getorgchart.js"></script>

        <asp:Literal id="ltrOrgScript" runat="server" />
       <%-- <script type="text/javascript">
            $(document).ready(function () {
                var peopleElement = document.getElementById("people");
                var orgChart = new getOrgChart(peopleElement, {
                    primaryFields: ["title"],
                    secondParentIdField: "secondParenId",
                    //enablePrint: true,
                    dataSource: document.getElementById("orgData"),
                    enableExportToImage: true
                });

                //document.getElementById("btnExportToImage").addEventListener("click", function () {
                //    orgChart.exportToImage();
                //});
            });
        </script>--%>
    </form>
</body>
</html>

