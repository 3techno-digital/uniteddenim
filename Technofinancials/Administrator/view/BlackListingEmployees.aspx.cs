﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.view
{
    public partial class BlackListingEmployees : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            CheckSessions();

            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetAllEmployeesByCompanyIDForBlackListing(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "Admin", "All BlackListed Employees Viewed", "Employees");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }

        protected void lnkModal_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            hdnEmployeeID.Value = btn.CommandArgument.ToString();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "none","<script>$('#mymodal').modal('show');</script>", false);
        }

        protected void btnToggleBlackListing_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            objDB.EmployeeID = Convert.ToInt32(hdnEmployeeID.Value);
            objDB.BlackListedBy = Session["UserName"].ToString();
            objDB.BlackListedReason = txtReason.Value;
            objDB.ToggleEmployeeBlackListing();
            Common.addlogNew("Toggle", "Admin", "Employee of ID \"" + objDB.EmployeeID + "\" BlackList Status Toggled", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/black-listing-employees", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/directors/view/black-listing-employees", "Employee of ID \"" + objDB.EmployeeID + "\" BlackList Status Toggled", "Employees", "Admin", Convert.ToInt32(objDB.EmployeeID));
            //Common.addlog("Toggle", "Admin", "Employee of ID \"" + objDB.EmployeeID + "\" BlackList Status Toggled", "Employees", objDB.EmployeeID);

            GetData();
        }
    }
}