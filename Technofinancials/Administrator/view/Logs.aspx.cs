﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.Administrator.view
{
    public partial class Logs : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                //GetData(10,0);
                //GetData();
            }
        }
        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            try
            {

                GetData();
            }
            catch (Exception ex)
            {
                
            }
        }
        private void GetData()
        {
            CheckSessions();

            DataTable ds = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DateTime fromdate, todate;
            fromdate = DateTime.Parse(txtFromDate.Text);
            todate = DateTime.Parse(txtToDate.Text);
            todate = todate.AddDays(1);

            objDB.FromDate = fromdate.ToString();
            objDB.ToDate = todate.ToString();
            ds = objDB.GetAllLog(ref errorMsg);
          

            gv.DataSource = ds;
            gv.DataBind();
            if (ds != null)
            {
                if (ds.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        //private void GetData(int numberOfRecords, int from)
        //{
        //    CheckSessions();

        //    DataSet ds = new DataSet();
        //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //    ds = objDB.GetAllLog(ref errorMsg, numberOfRecords, from);
        //    var dt = ds.Tables[0];
        //    _gv.VirtualItemCount = Convert.ToInt32(ds.Tables[1].Rows[0]["column1"]);

        //    _gv.DataSource = dt;
        //    _gv.DataBind();
        //    if (dt != null)
        //    {
        //        if (dt.Rows.Count > 0)
        //        {
        //            _gv.UseAccessibleHeader = true;
        //            _gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        //        }
        //    }
        //}

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Directors", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }

        //protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    _gv.PageIndex = e.NewPageIndex;
        //    GetData(10, e.NewPageIndex);
        //}

        //protected void lnkDelete_Click(object sender, EventArgs e)
        //{
        //    CheckSessions();
        //    LinkButton btn = (LinkButton)sender as LinkButton;
        //    int ID = Convert.ToInt32(btn.CommandArgument);
        //    objDB.NodeID = ID;
        //    objDB.DeletedBy = Session["UserName"].ToString();
        //    objDB.DeleteNode();
        //    GetData();
        //}
    }
}