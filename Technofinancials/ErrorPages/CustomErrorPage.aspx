﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomErrorPage.aspx.cs" Inherits="Technofinancials.ErrorPages.CustomErrorPage" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />
    <style>
        body{
            background-image:none!important;
            background-color:#fff!important;
        }
        .error-page img{
            display:block;
            margin:0 auto;
            }
        .error-p{
            color:#000;
            text-align:center;
            width:450px;
            display:block;
            margin:0 auto;
            font-size:17px;
            font-weight:bold;
        }
.error-btn {
    border-radius: 100px;
    text-align: center;
    display: block;
    margin: 15px auto 0;
    margin-top: 15px;
    color: #fff;
    background-color: #021eb6;
    width:200px;
}
.error-btn:hover{
    color: #fff;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScrptMangr" runat="server"></asp:ScriptManager>
        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 error-page">
                        <div>
                            <img src="/assets/images/error.gif"  class="img-responsive"/>
                            <p class="error-p">We are unable to find your requested information. Please contact Techno Financial's Champion for support</p>
                            <a class="btn error-btn" href="/login">GO TO HOMEPAGE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    
</body>
</html>
