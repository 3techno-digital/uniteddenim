﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Seperation.aspx.cs" Inherits="Technofinancials.ESS.Seperation" %>


<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">

                  <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Seperation</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                     <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="row">
                                             <div class="col-sm-12">
                                                <h4 for="select2-demo-2" class="control-label">Type Of Seperation</h4>
                                                <asp:DropDownList ID="ddlTypeOfSeperation" runat="server" CssClass="form-control js-example-basic-single">
                                                    <asp:ListItem Value="0">--- Please Select ---</asp:ListItem>
                                                    <asp:ListItem Value="Terminations">Terminations</asp:ListItem>
                                                    <asp:ListItem Value="Lay off">Lay off</asp:ListItem>
                                                    <asp:ListItem Value="Resignation">Resignation</asp:ListItem>
                                                </asp:DropDownList>
                                                <%--<input type="text" class="form-control" placeholder="Appraisee's name" />--%>
                                            </div>
                                                    <div class="col-sm-12">
                                                <h4 for="select2-demo-3" class="control-label">Reason For Leaving </h4>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" name="reasonForLeaving" id="txtReasonForLeaving" runat="server"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <h4 for="select2-demo-3" class="control-label">Comments Regarding Salary </h4>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="txtCommentsRegardingSalary" runat="server" name="commentsRegardingSalary"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <h4 for="select2-demo-3" class="control-label">Things Like About Company</h4>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="txtThingsLikeAboutCompany" runat="server" name="thingsLikeAboutCompany"></textarea>
                                                </div>
                                            </div>

                                                  <div class="col-sm-12">
                                                <h4 for="select2-demo-4" class="control-label">Notice Period End Date</h4>
                                                <div class="form-group">
                                                    <input type="text" id="txtNoticePeriodEndDate" runat="server" class="form-control" data-plugin="datetimepicker" name="noticePeriodEndDate" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <h4>Email Or Call Forwarding Period</h4>
                                                    <input type="text" id="txtEmailForwardingPeriod" runat="server" class="form-control" data-plugin="datetimepicker" name="emailOrCallForwadingPeriod" />
                                                </div>
                                            </div>



                                </div>
                            </div>

                            

                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="row">




                                </div>
                            </div>
                        </div>
                 
                    <!-- /.container-fluid -->
           

                                    <div class="content-header second_heading">
                                        <div class="container-fluid">
                                            <div class="row mb-2">
                                                <div class="col-sm-6">
                                                    <h1 class="m-0 text-dark">IT Seperation</h1>
                                                </div>
                                                <!-- /.col -->

                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <!-- /.container-fluid -->
                                    </div>

                                        <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <h4>Email ID Status</h4>
                                    <asp:DropDownList ID="ddlEmailStatus" runat="server" CssClass="form-control js-example-basic-single">
                                        <asp:ListItem Value="0">--- Please Select ---</asp:ListItem>
                                        <asp:ListItem Value="Transfered">Transfered</asp:ListItem>
                                        <asp:ListItem Value="Revoked">Revoked</asp:ListItem>
                                        <asp:ListItem Value="Deleted">Deleted</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Domain Access</h4>
                                    <asp:DropDownList ID="ddlDomainAccess" runat="server" CssClass="form-control js-example-basic-single">
                                        <asp:ListItem Value="0">--- Please Select ---</asp:ListItem>
                                        <asp:ListItem Value="Transfered">Transfered</asp:ListItem>
                                        <asp:ListItem Value="Revoked">Revoked</asp:ListItem>
                                        <asp:ListItem Value="Deleted">Deleted</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-12">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="chkAccessStatus" runat="server" name="isAccessStatusRevoked" />
                                        <label for="checkbox-demo-4">is Access Status Revoked</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="chkCompanyInfoSotred" runat="server" name="companyInfoStoredPersonally" />
                                        <label for="custome-checkbox2">Company Info Stored Personally</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h4 for="select2-demo-3" class="control-label">IT Comments</h4>
                                        <textarea class="form-control" rows="5" id="txtITComments" runat="server" name="itComments"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                    </div>


                                        
                                    

                       <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark">Admin Seperation</h1>
                                </div>
                                <!-- /.col -->

                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>

                                       <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="chkLockerKeys" runat="server" name="returnedLockerKeys" />
                                        <label for="custome-checkbox7">Returned Locker Keys</label>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="chkReturnedCar" runat="server" name="returnedCar" />
                                        <label for="custome-checkbox9">Returned Car</label>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="chkStationary" runat="server" name="returnedStationary" />
                                        <label for="custome-checkbox11">Returned Stationary</label>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="chkMobile" runat="server" name="returnedMobilePhone" />
                                        <label for="custome-checkbox8">Returned Mobile Phone</label>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="chkInternetDevice" runat="server" name="returnedInternetDevice" />
                                        <label for="custome-checkbox10">Returned Internet Device</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <h4 for="select2-demo-3" class="control-label">Notes</h4>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txtAdminNotes" runat="server" name="notes"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                    </div>
                                        <%-- <div class="row">
                                            <div class="col-sm-12">
                                                <button class="tf-send-btn" "" id="admin-sepration" data-original-"Send"><i class="fas fa-paper-plane"></i></button>
                                            </div>
                                        </div>--%>

                                  <div class="content-header second_heading">
                                    <div class="container-fluid">
                                        <div class="row mb-2">
                                            <div class="col-sm-6">
                                                <h1 class="m-0 text-dark">HR Seperation</h1>
                                            </div>
                                            <!-- /.col -->

                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <!-- /.container-fluid -->
                                </div>

                           <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <h4>Notice Period Dedudctions</h4>
                                    <input type="number" class="form-control" placeholder="Notice Period Dedudctions" name="noticePeriodDeductions" id="txtNoticePeriodDeductions" runat="server" />
                                </div>

                                <div class="col-md-6 form-group">
                                    <h4>Attendance Details</h4>
                                    <input type="text" class="form-control" placeholder="Attendance Details" name="attendanceDetails" id="txtAttendanceDetails" runat="server" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>UnAwailed Leave Balance</h4>
                                    <input type="number" class="form-control" placeholder="UnAwailed Leave Balance" name="unawailedLeaveBalance" id="txtUnawailedLeaveBalance" runat="server" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Medical Refurbishments</h4>
                                    <input type="number" class="form-control" placeholder="Medical Refurbishments" name="medicalRefurbishments" id="txtMedicalRefurbishments" runat="server" />
                                </div>
                                <div class="col-md-12 form-group">
                                    <h4 for="select2-demo-3" class="control-label">Notes</h4>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="1" cols="10" id="txtHRNotes" runat="server" name="notesmedical"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                    </div>
                                   
                              
                      <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark">Finance Seperation</h1>
                                </div>
                                <!-- /.col -->

                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>

                                      
                     <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <h4>Loans and advances</h4>
                                    <input type="number" class="form-control" placeholder="Loans and advances" name="loanAndAdvances" id="txtLoansAndAdvances" runat="server" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Company Loan</h4>
                                    <input type="number" class="form-control" placeholder="Company Loan" name="companyLoan" id="txtCompanyLoans" runat="server" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Any other advance</h4>
                                    <input type="number" class="form-control" placeholder="UnAwailed Leave Balance" name="anyOtherAdvance" id="txtAnyOtherAdvance" runat="server" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Total Dues</h4>
                                    <input type="number" class="form-control" placeholder="Total Dues" name="totalDues" id="txtTotalDues" runat="server" />
                                </div>
                                <div class="col-md-12 form-group">
                                    <h4 for="select2-demo-3" class="control-label">Notes</h4>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txtFinanceNotes" runat="server" name="financenotes"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">

                            </div>
                        </div>

                         <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="row">
                                           <div class="col-md-12" >
                            <div class="form-group" id="divAlertMsg" runat="server">
                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                    <span>
                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                    </span>
                                    <p id="pAlertMsg" runat="server">
                                    </p>
                                </div>
                            </div>
                        </div>

                                </div>
                            </div>

                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
</body>
</html>
