﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class Shits : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected static int ShiftID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    CheckSessions();
                    shiftDiv.Disabled = true;
                    shiftDaysDiv.Visible = false;
                    //btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/shifts";
                    divAlertMsg.Visible = false;
                    BindShiftsDropDown(Convert.ToInt32(Session["CompanyID"].ToString()));
                    getShiftsByEmployeeID(Convert.ToInt32(Session["EmployeeID"].ToString()));
                }
                catch (Exception ex)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = ex.Message;
                }

            }
        }
        private void BindShiftsDropDown(int companyID)
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = companyID;
                ddlShifts.DataSource = objDB.GetWorkingShiftByCompanyID(ref errorMsg);
                ddlShifts.DataTextField = "ShiftName";
                ddlShifts.DataValueField = "ShiftID";
                ddlShifts.DataBind();
                ddlShifts.Items.Insert(0, new ListItem("--- None ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getShiftsByEmployeeID(int EmpID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.EmployeeID = EmpID;
                dt = objDB.GetWorkingShiftByEmployeeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ddlShifts.SelectedValue = dt.Rows[0]["ShiftID"].ToString();
                        shiftDaysDiv.Visible = true;
                        getShiftsByShiftID(Convert.ToInt32(dt.Rows[0]["ShiftID"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getShiftsByShiftID(int ShiftID)
        {
            try
            {
                shiftDaysDiv.Visible = true;
                DataTable dt, dtday = new DataTable();
                objDB.WorkingShiftID = ShiftID;
                dt = objDB.GetWorkingShiftByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        //txtShiftTitle.Value = dt.Rows[0]["ShiftName"].ToString();
                        txtHours.Value = dt.Rows[0]["Hours"].ToString();
                        chkMonday.Checked = dt.Rows[0]["Monday"].ToString() == "True";
                        chkTuesday.Checked = dt.Rows[0]["Tuesday"].ToString() == "True";
                        chkWednesdat.Checked = dt.Rows[0]["Wednesday"].ToString() == "True";
                        chkThursday.Checked = dt.Rows[0]["Thursday"].ToString() == "True";
                        chkFriday.Checked = dt.Rows[0]["Friday"].ToString() == "True";
                        chkSaturday.Checked = dt.Rows[0]["Saturday"].ToString() == "True";
                        chkSunday.Checked = dt.Rows[0]["Sunday"].ToString() == "True";

                        objDB.DocID = ShiftID;
                        objDB.DocType = "Shifts";
                    }
                }
                dtday = objDB.GetWorkingShiftDaysByShiftID(ref errorMsg);
                if (dtday != null && dtday.Rows.Count > 0)
                {
                    allowHolidayWorking.Checked = dtday.Rows[0]["IsSpecial"].ToString() == "True" ? true : false;
                    for (int i = 0; i < dtday.Rows.Count; i++)
                    {
                        if (dtday.Rows[i]["DayName"].ToString() == "Monday")
                        {
                            chkMonNightShift.Checked = dtday.Rows[i]["isNightShift"].ToString() == "True";
                            txtMONStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtMONLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtMONBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtMONBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtMONHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtMONEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtMONEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkMondayIsSpecial.Checked = dtday.Rows[i]["IsFlexible"].ToString() == "True";
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Tuesday")
                        {
                            chkTUENightShift.Checked = dtday.Rows[i]["isNightShift"].ToString() == "True";
                            txtTUEStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtTUELateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtTUEBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtTUEBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtTUEHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtTUEEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtTUEEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkTuesdayIsSpecial.Checked = dtday.Rows[i]["IsFlexible"].ToString() == "True";
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Wednesday")
                        {
                            chkWEDNightShift.Checked = dtday.Rows[i]["isNightShift"].ToString() == "True";
                            txtWEDStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtWEDLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtWEDBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtWEDBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtWEDHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtWEDEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtWEDEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkWednesdayIsSpecial.Checked = dtday.Rows[i]["IsFlexible"].ToString() == "True";
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Thursday")
                        {
                            chkTHUNightShift.Checked = dtday.Rows[i]["isNightShift"].ToString() == "True";
                            txtTHUStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtTHULateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtTHUBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtTHUBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtTHUHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtTHUEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtTHUEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkThursdayIsSpecial.Checked = dtday.Rows[i]["IsFlexible"].ToString() == "True";
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Friday")
                        {
                            chkFRINightShift.Checked = dtday.Rows[i]["isNightShift"].ToString() == "True";
                            txtFRIStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtFRILateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtFRIBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtFRIBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtFRIHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtFRIEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtFRIEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkFridayIsSpecial.Checked = dtday.Rows[i]["IsFlexible"].ToString() == "True";
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Saturday")
                        {
                            chkSATNightShift.Checked = dtday.Rows[i]["isNightShift"].ToString() == "True";
                            txtSATStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtSATLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtSATBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtSATBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtSATHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtSATEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtSATEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkSaturdayIsSpecial.Checked = dtday.Rows[i]["IsFlexible"].ToString() == "True";
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Sunday")
                        {
                            chkSUNNightShift.Checked = dtday.Rows[i]["isNightShift"].ToString() == "True";
                            txtSUNStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtSUNLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtSUNBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtSUNBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtSUNHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtSUNEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtSUNEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkSundayIsSpecial.Checked = dtday.Rows[i]["IsFlexible"].ToString() == "True";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

    }

}