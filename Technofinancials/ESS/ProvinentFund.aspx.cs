﻿using System;
using System.Data;
using System.Web.UI;

namespace Technofinancials.ESS
{
	public partial class ProvinentFund : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int employeeID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();

                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/dashboard";
                divAlertMsg.Visible = false;

                if (Session["EmployeeID"] != null)
                {
                    employeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                    getEmployeeByID(employeeID);
                }
            }
        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        private void getEmployeeByID(int EmpID)
        {
            objDB.EmployeeID = EmpID;
            DataTable dt = objDB.GetProvinentFundByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtRemaningAmount.Value = dt.Rows[0]["RemaningAmount"].ToString();
                    txtTotalAmount.Value = dt.Rows[0]["TotalAmount"].ToString();
                    txtWithdrawAmount.Value = dt.Rows[0]["WithdrawAmount"].ToString();
                    txtEmployeePF.Value = dt.Rows[0]["EmployeePF"].ToString();
                    txtCompanyPF.Value = dt.Rows[0]["CompanyPF"].ToString();
                }
            }

            Common.addlog("ViewAll", "ESS", "Provinent Fund Viewed", "");
        }
    }
}