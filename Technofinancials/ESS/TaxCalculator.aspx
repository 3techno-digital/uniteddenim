﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaxCalculator.aspx.cs" Inherits="Technofinancials.ESS.TaxCalculator" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
            <style>
            .AD_btn_inn:hover {
                background: #fff;
                color: #003780;
                transition: all .2s linear;
                border: 1px solid #003780;
            }
            input#btnCalculate {
                margin-top: 20px;
                padding: 3px 14px !important;
            }
            @media only screen and (max-width: 1280px) and (min-width: 800px) {
                .table_out_data {
                    width: 330px;
                }
            }
        </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <%--       <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />--%>

            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h3 class="tf-page-heading-text">Basic Tax Calculator</h3>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                    <button type="button" runat="server" id="btnCalculator" class="AD_btn" data-toggle="modal" data-target="#salary-modal" value="Calculator">Calculator</button>
                                    <%--<button class="AD_btn" "Generate Report" id="btnReport" runat="server" onserverclick="btnReport_ServerClick" type="button">PDF</button>--%>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
                    <div class="row stock1Pad">
                        <div class="col-lg-4 col-md-6 col-sm-12 pl-0">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label>Year<span style="color: red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlYear" ErrorMessage="" InitialValue="0" Display="Dynamic" ValidationGroup="btnCalculate" ForeColor="Red" /></label>
                                    <select class="form-control select2 " runat="server" data-plugin="select2" id="ddlYear">
                                    
                                           <option value="2020-21">2019-20</option>
                                        <option value="2020-21">2020-21</option>
                                         <option value="2020-21">2021-22</option>
                                   
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label>Monthly Salary <span style="color: red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtSalary" ErrorMessage="" Display="Dynamic" ValidationGroup="btnCalculate" ForeColor="Red" /></label>
                                    <input class="form-control single-line text-box single-line" runat="server" id="txtSalary" name="txtSalary" type="number" />
                                </div>
                             
                            </div>
                           
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 pl-0">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label>Yearly Bonus <span style="color: red !important;">*</span><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtBonus" ErrorMessage="" Display="Dynamic" ValidationGroup="btnCalculate" ForeColor="Red" /></label>
                                    <input class="form-control single-line text-box single-line" runat="server" id="txtBonus" name="txtBonus" type="number" />
                                </div>
                                 <div class="col-md-6 col-sm-6">
                                   <label> </label>
                                  
                                    <input type="button" onserverclick="btnCalculate_ServerClick" runat="server" class="form-control single-line text-box single-line AD_btn_inn" validationgroup="btnCalculate" id="Button1" name="name" value="Check now" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />

                    <div class="row stock1Pad">
                        <div class="col-lg-4 col-md-12 pl-0">

                            <div class="table_out_data">
                                <label>Monthly Salary <span class="field-validation-valid text-danger" data-valmsg-for="GrossAmount" data-valmsg-replace="true"></span></label>
                                <input class="form-control text-box single-line valid" runat="server" disabled="disabled" id="txtMonthlySalary" name="GrossAmount" type="text" value="" aria-invalid="false" />
                            </div>
                            <div class="table_out_data">
                                <label>Monthly Taxable Salary <span class="field-validation-valid text-danger" data-valmsg-for="GrossAmount" data-valmsg-replace="true"></span></label>
                                <input class="form-control text-box single-line valid" runat="server" disabled="disabled" id="txtMonthlyTaxableSalary" name="GrossAmount" type="text" value="" aria-invalid="false" />
                            </div>
                            <div class="table_out_data">
                                <label>Medical Allowance<span class="field-validation-valid text-danger" data-valmsg-for="Tax" data-valmsg-replace="true"></span></label>
                                <input class="form-control text-box single-line" runat="server" disabled="disabled" id="txtMonthlyExemptedTax" name="Tax" type="text" value="" />
                            </div>
                            <div class="table_out_data">
                                <label>Monthly Tax <span class="field-validation-valid text-danger" data-valmsg-for="Tax" data-valmsg-replace="true"></span></label>
                                <input class="form-control text-box single-line" runat="server" disabled="disabled" id="txtMonthlyTax" name="Tax" type="text" value="" />
                            </div>

                            <div class="table_out_data">
                                <label>Salary After Tax <span class="field-validation-valid text-danger" data-valmsg-for="Discount" data-valmsg-replace="true"></span></label>
                                <input class="form-control text-box single-line" runat="server" disabled="disabled" data-val="true" data-val-number="The field Discount must be a number." data-val-required="The Discount field is required." id="txtSalaryAfterTax" max="100" min="0" type="text" value="0" />
                            </div>
                            <div class="table_out_data">
                                <label>Yearly Salary <span class="field-validation-valid text-danger" data-valmsg-for="ShippingCharges" data-valmsg-replace="true"></span></label>
                                <input class="form-control text-box single-line" runat="server" disabled="disabled" id="txtYearlySalary" name="ShippingCharges" type="text" value="" />
                            </div>
                            <div class="table_out_data">
                                <label>Yearly Taxable Salary <span class="field-validation-valid text-danger" data-valmsg-for="ShippingCharges" data-valmsg-replace="true"></span></label>
                                <input class="form-control text-box single-line" runat="server" disabled="disabled" id="txtTaxableYearlySalary" name="ShippingCharges" type="text" value="" />
                            </div>
                            <div class="table_out_data">
                                <label>Yearly Tax <span class="field-validation-valid text-danger" data-valmsg-for="NetAmount" data-valmsg-replace="true"></span></label>
                                <input class="form-control text-box single-line" runat="server" disabled="disabled" id="txtYearlyTax" name="NetAmount" type="text" value="" />
                            </div>
                            <div class="table_out_data">
                                <label>Yearly Salary <span class="field-validation-valid text-danger" data-valmsg-for="NetAmount" data-valmsg-replace="true"></span></label>
                                <input class="form-control text-box single-line" runat="server" disabled="disabled" id="txtyearlySalaryAfter" name="NetAmount" type="text" value="" />
                            </div>
                        </div>
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
             <!-- Modal -->
                        <div class="modal fade M_set" id="salary-modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="m-0 text-dark">Salary Calculator</h1>
                                        <div class="add_new">
                                       <%--     <button type="button" class="AD_btn" value="Copy" data-dismiss="modal" id="btnCopyFields" onclick="copyCalculationsToMainFields();">Copy</button>--%>
                                            <%--<button type="button" class="AD_btn" runat="server" value="Copy" data-dismiss="modal" id="btnCopyFields" onserverclick="Unnamed_ServerClick">Copy</button>--%>
                                            <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                        </div>
                                    </div>

                                    <div class="modal-body">
                                        <asp:UpdatePanel ID="GrossSalupd" runat="server">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label>Gross Salary</label>
                                                        <input class="form-control" runat="server" id="txtCalcGrossSalary" type="number" value="0" onkeyup="txtGrossSalary_TextChanged();" />
                                                        <%--<asp:TextBox runat="server" CssClass="form-control" ID="txtCalcGrossSalary" Text="0" TextMode="Number" AutoPostBack="true" OnTextChanged="txtGrossSalary_TextChanged" />--%>
                                                        <%--<input type="number" class="form-control" id="txtCalcGrossSalary" runat="server" value="0"/>--%>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <label>
                                                            Basic Salary<br />
                                                            (66.67% of Gross Salary)</label>
                                                        <input type="number" class="form-control" disabled="disabled" id="txtCalcBasicSalary" value="0" runat="server" />
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <label>
                                                            House Rent Allowance<br />
                                                            (23.33% of Gross Salary)</label>
                                                        <input type="number" class="form-control" disabled="disabled" id="txtCalcHouseRent" value="0" runat="server" />
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <label>
                                                            Utilities Allowance<br />
                                                            (3.33% of Gross Salary)
                                                        </label>
                                                        <input type="number" class="form-control" disabled="disabled" id="txtCalcUtilities" value="0" runat="server" />
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <label>
                                                            Medical Allowance<br />
                                                            (6.67% of Gross Salary)
                                                        </label>
                                                        <input type="number" class="form-control" disabled="disabled" id="txtCalcMedical" value="0" runat="server" />
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>

                            </div>
                        </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
<script>     
    function txtGrossSalary_TextChanged() {

        if ($('#txtCalcGrossSalary').val() != "") {
            var grossSalary = parseFloat($('#txtCalcGrossSalary').val());
            // var basicSalary = parseFloat((grossSalary * 66.67) / 100);
            //  var houseAllowance = parseFloat((grossSalary * 23.33) / 100);
            // var utilities = parseFloat((grossSalary * 3.33) / 100);
            // var Medical = parseFloat((grossSalary * 6.67) / 100);

            var basicSalary = parseFloat((grossSalary * 66.67) / 100);
            var houseAllowance = parseFloat((basicSalary * 35) / 100);

            var Medical = parseFloat((basicSalary * 10) / 100);
            var utilities = parseFloat((grossSalary - basicSalary - houseAllowance - Medical));

            var sumOfAll = basicSalary + houseAllowance + utilities + Medical;

            if (sumOfAll > grossSalary)
                houseAllowance = houseAllowance - 1;

            //alert('' + grossSalary);
            $('#txtCalcBasicSalary').val(basicSalary.toFixed(2) + '');
            $('#txtCalcHouseRent').val(houseAllowance.toFixed(2) + '');
            $('#txtCalcUtilities').val(utilities.toFixed(2) + '');
            $('#txtCalcMedical').val(Medical.toFixed(2) + '');
        }
        else {
            $('#txtCalcBasicSalary').val('0');
            $('#txtCalcHouseRent').val('0');
            $('#txtCalcUtilities').val('0');
            $('#txtCalcMedical').val('0');

        }
    }
</script>


    </form>
</body>
</html>
