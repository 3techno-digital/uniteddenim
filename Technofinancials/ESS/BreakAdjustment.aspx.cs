﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class BreakAdjustment : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                btnApprove.Visible = false;
                btnDisapprove.Visible = false;
                divAlertMsg.Visible = false;
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                DataTable dtEmployee = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
                ddlEmployee.DataSource = dtEmployee;
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                
                if (dtEmployee.Rows.Count > 1)
                {
                    ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));
                }

                ddlAdjustmentStatus.DataSource = objDB.GetBreakAdjustmentStatus(ref errorMsg);
                ddlAdjustmentStatus.DataTextField = "AdjustmentStatus";
                ddlAdjustmentStatus.DataValueField = "AdjustmentStatus";
                ddlAdjustmentStatus.DataBind();
                ddlAdjustmentStatus.Items.Insert(0, new ListItem("ALL", "0"));
                ddlAdjustmentStatus.Text = "Data Submitted for Review";
                DateTime now = DateTime.Now;
                txtFromDate.Text = new DateTime(now.Year, now.Month, 1).ToString("dd-MMM-yyyy");
                txtToDate.Text = now.ToString("dd-MMM-yyyy");

                GetData("Data Submitted for Review");
            }
        }

        private DataTable FilterData(DataTable dt)
        {
            if (dt == null)
            {
                return dt;
            }
            DataTable dtFilter = new DataTable();

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.DocName = "Loans";

            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");

                    if (dtTemp != null)
                        dtFilter.Merge(dtTemp);

                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }
                }
            }

            return dtFilter;
        }

        private void GetData(string BreakAdjustmentStatus = "0")
        {
            CheckSessions();
            DataTable dt = new DataTable();

            objDB.EmployeeID = Convert.ToInt16(ddlEmployee.SelectedValue);
            objDB.FromDate = txtFromDate.Text;
            objDB.ToDate = txtToDate.Text;

            DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
            DateTime.TryParse(txtFromDate.Text, out Fdate);
            DateTime.TryParse(txtToDate.Text, out Tdate);

            if (Tdate < Fdate)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "From Date shoul be less than To Date";
                gv.DataSource = "";
                gv.DataBind();
                return;
            }

            if (Fdate > DateTime.Now)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "From Date shoul be Equal or less than Current Date";
                gv.DataSource = "";
                gv.DataBind();
                return;
            }
            
            if ((Tdate - Fdate).Days > 31 || (Fdate.Day <= Tdate.Day && (Tdate.Month - Fdate.Month) > 0))
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "date range should be of 1 month";
                gv.DataSource = "";
                gv.DataBind();
                return;
            }
            divAlertMsg.Visible = false;
            dt = objDB.GetAllBreakAdjustmentByEmployeeID(ddlEmployee.SelectedValue == Session["EmployeeID"].ToString() ? "0" : Session["EmployeeID"].ToString(), ref errorMsg, BreakAdjustmentStatus);
            gv.DataSource = new DataTable();
            gv.DataBind();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.DataSource = dt;
                    gv.DataBind();
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                    divAlertMsg.Visible = false;
                }
            }
            else
            {
                divAlertMsg.Visible = false;
            }

            Common.addlog("ViewAll", "ESS", "All employee-Adjustments Viewed", "BreakAdjustment");
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void chckchanged(object sender, EventArgs e)

        {
            CheckBox chckheader = (CheckBox)gv.HeaderRow.FindControl("checkAll");

            foreach (GridViewRow row in gv.Rows)
            {
                if (((Label)row.FindControl("lblDocStatus")).Text == "Data Submitted for Review" || ((Label)row.FindControl("lblDocStatus")).Text == "Saved as Draft")
                {
                    ((CheckBox)row.FindControl("check")).Checked = chckheader.Checked;
                }
            }
        }
        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                btnApprove.Visible = false;
                btnDisapprove.Visible = false;
                GetData(ddlAdjustmentStatus.SelectedValue);
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnApprove_ServerClick(object sender, EventArgs e)
        {
            btnApprove.Visible = false;
            btnDisapprove.Visible = false;
            CheckSessions();
            string alertMsg = "";
            int recordsCount = 0;
            foreach (GridViewRow gvr in gv.Rows)
            {
                if (((CheckBox)gvr.FindControl("check")).Checked)
                {
                    string employeeID = ((Label)gvr.FindControl("lblEmployeeID")).Text;
                    int breakAdjustmentId = Convert.ToInt32(((Label)gvr.FindControl("lblBreakAdjustmentID")).Text);
                    string employeeName = ((Label)gvr.FindControl("lblName")).Text;
                    string attendanceDate = ((Label)gvr.FindControl("lblAttendanceDate")).Text;
                    string docStatus = ((Label)gvr.FindControl("lblDocStatus")).Text;
                    string breakTimeIn = ((Label)gvr.FindControl("lblBreakTimeIn")).Text;
                    string breakTimeOut = ((Label)gvr.FindControl("lblBreakTimeOut")).Text;
                    string resultant = ((Label)gvr.FindControl("lblResultant")).Text;

                    if (employeeID != Session["EmployeeID"].ToString())
                    {
                        string requestStatus = ApproveBreakAdjustmentRequest(breakAdjustmentId, sender, employeeID);
                        //msg += $"{employeeName} request status for date {attendanceDate} is {requestStatus}";
                        recordsCount++;
                    }
                }
            }

            GetData(ddlAdjustmentStatus.SelectedValue);

            alertMsg = recordsCount > 0 ? "" + recordsCount + " Records Updated Successfully" : "No Records Updated ";

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + alertMsg + "')", true);

            //divAlertMsg.Visible = true;
            //divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            //pAlertMsg.InnerHtml = approveList;
        }

        private string ApproveBreakAdjustmentRequest(int breakAdjustmentId, object sender, string employeeID)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "BreakAdjustment", "BreakAdjustmentID", breakAdjustmentId.ToString(), Session["UserName"].ToString());
            Common.addlogNew(res, "ESS", "BreakAdjustment of ID\"" + breakAdjustmentId.ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/attendance-adjustment", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/attendance-adjustment/edit-attendance-adjustment-" + breakAdjustmentId.ToString(), "BreakAdjustment for Employee \"" + employeeID + "\"", "BreakAdjustment", "Loans", breakAdjustmentId);

            if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
            {
                objDB.BreakAdjustmentID = breakAdjustmentId;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.ApproveBreakAdjustment();

            }
            else if (res == "Rejected & Disapproved Sucessfull" || res == "Disapproved" || res == "Rejected" || res == "Disapproved")
            {
                objDB.BreakAdjustmentID = breakAdjustmentId;
                objDB.EnableBreakAdjustment();
                objDB.BreakAdjustmentID = breakAdjustmentId;
                objDB.BreakAdjustmentRemarks = txtAdjustmentNote.Value;
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.RejectBreakAdjustment();
            }

            return res;
        }
    }
}

