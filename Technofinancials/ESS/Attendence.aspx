﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Attendence.aspx.cs" Inherits="Technofinancials.ESS.Attendence" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">

                 <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Attendance</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>


                <section class="app-content">
                  
                    <div class="row">
                        <div class="col-sm-4">
                                <div class="form-group">
                                    <h4>Year</h4>
                                         <asp:DropDownList ID="ddlYear" runat="server" class="form-control select2" data-plugin="select2"  AutoPostBack="true" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                                          <asp:ListItem Value="0">--- Select Year ---</asp:ListItem>
                                               <asp:ListItem Value="2018">2018</asp:ListItem>
                                                                <asp:ListItem Value="2017">2017</asp:ListItem>
                                                                <asp:ListItem Value="2016">2016</asp:ListItem>
                                                                <asp:ListItem Value="2015">2015</asp:ListItem>
                                                                <asp:ListItem Value="2014">2014</asp:ListItem>
                                                                <asp:ListItem Value="2013">2013</asp:ListItem>
                                                                <asp:ListItem Value="2012">2012</asp:ListItem>
                                                                <asp:ListItem Value="2011">2011</asp:ListItem>
                                                                <asp:ListItem Value="2010">2010</asp:ListItem>
                                                </asp:DropDownList>
                                </div>
                            </div>
                        <div class="col-sm-4">
                                <div class="form-group">
                                    <h4>Month</h4>
                                         <asp:DropDownList ID="ddlMonth" runat="server" class="form-control select2" data-plugin="select2"  AutoPostBack="true" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                                          <asp:ListItem Value="0">--- Select Month ---</asp:ListItem>
                                             <asp:ListItem Value="1">January</asp:ListItem>
                                                                <asp:ListItem Value="2">Ferbruary</asp:ListItem>
                                                                <asp:ListItem Value="3">March</asp:ListItem>
                                                                <asp:ListItem Value="4">April</asp:ListItem>
                                                                <asp:ListItem Value="5">May</asp:ListItem>
                                                                <asp:ListItem Value="6">June</asp:ListItem>
                                                                <asp:ListItem Value="7">July</asp:ListItem>
                                                                <asp:ListItem Value="8">August</asp:ListItem>
                                                                <asp:ListItem Value="9">September</asp:ListItem>    
                                                                <asp:ListItem Value="10">October</asp:ListItem>       
                                                                <asp:ListItem Value="11">November</asp:ListItem>       
                                                                <asp:ListItem Value="12">December</asp:ListItem>       
                                             
                                         </asp:DropDownList>
                                </div>
                            </div>
                  
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row ">
                        
                       
                                <div class="col-sm-12">

                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static"  ShowHeaderWhenEmpty="true" OnRowDataBound="gv_RowDataBound">
                                        
                                    </asp:GridView>
                                </div>
                          
                       
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757 !important;
                border-radius: 0px !important;
                box-shadow: 0 8px 6px -5px #cacaca !important;
                height: 38px !important;
                width: 37px !important;
                display: inline-block !important;
                padding: 5px !important;
                position: relative !important;
                text-align: center !important;
            }
        </style>
    </form>
</body>
</html>
