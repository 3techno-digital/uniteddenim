﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class Seperation : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int employeeID = 0;
        protected string empPhoto = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
               
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/dashboard";

                divAlertMsg.Visible = false;
               


                if (Session["EmployeeID"] != null)
                {
                    employeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                    
                    getEmployeeSeperationByEmployweeID(employeeID);
                    disableFields();

                }
            }
        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        private void getEmployeeSeperationByEmployweeID(int EmpID)
        {


            objDB.EmployeeID = EmpID;
            DataTable dt = objDB.GetEmployeeSeperationDetails(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlTypeOfSeperation.SelectedValue = dt.Rows[0]["TypeOfSeperation"].ToString();
                    txtReasonForLeaving.Value = dt.Rows[0]["ReasoForLeaving"].ToString();
                    txtCommentsRegardingSalary.Value = dt.Rows[0]["CommentsRegardingSalary"].ToString();
                    txtThingsLikeAboutCompany.Value = dt.Rows[0]["ThingsLikeAboutCompany"].ToString();
                    txtNoticePeriodEndDate.Value = dt.Rows[0]["NoticePeriodEndDate"].ToString();
                    txtEmailForwardingPeriod.Value = dt.Rows[0]["EmailOrCallForwardingPeriod"].ToString();
                    ddlEmailStatus.SelectedValue = dt.Rows[0]["EmailIDStatus"].ToString();
                    ddlDomainAccess.SelectedValue = dt.Rows[0]["DomainAccess"].ToString();
                    chkAccessStatus.Checked = getCheckBoxValue(dt.Rows[0]["IsAccessStatusRevoked"].ToString());
                    chkCompanyInfoSotred.Checked = getCheckBoxValue(dt.Rows[0]["CompanyInfoStoredPersonally"].ToString());
                    txtITComments.Value = dt.Rows[0]["ITComments"].ToString();
                    chkLockerKeys.Checked = getCheckBoxValue(dt.Rows[0]["ReturnedLockerKey"].ToString());
                    chkReturnedCar.Checked = getCheckBoxValue(dt.Rows[0]["ReturnedCar"].ToString());
                    chkStationary.Checked = getCheckBoxValue(dt.Rows[0]["ReturnedStationary"].ToString());
                    chkReturnedCar.Checked = getCheckBoxValue(dt.Rows[0]["ReturnedMobilePhone"].ToString());
                    chkInternetDevice.Checked = getCheckBoxValue(dt.Rows[0]["ReturnedInternetDevice"].ToString());
                    chkMobile.Checked = getCheckBoxValue(dt.Rows[0]["ReturnedMobilePhone"].ToString());
                    txtAdminNotes.Value = dt.Rows[0]["AdminNotes"].ToString();
                    txtNoticePeriodDeductions.Value = dt.Rows[0]["NoticePeriodDedudctions"].ToString();
                    txtAttendanceDetails.Value = dt.Rows[0]["AttendanceDetails"].ToString();
                    txtUnawailedLeaveBalance.Value = dt.Rows[0]["UnAwailedLeaveBalance"].ToString();
                    txtMedicalRefurbishments.Value = dt.Rows[0]["AnyOtherAdvances"].ToString();
                    txtHRNotes.Value = dt.Rows[0]["HRNotes"].ToString();
                    txtLoansAndAdvances.Value = dt.Rows[0]["LoansAndAdvances"].ToString();
                    txtCompanyLoans.Value = dt.Rows[0]["CompanyLoan"].ToString();
                    txtAnyOtherAdvance.Value = dt.Rows[0]["ReasoForLeaving"].ToString();
                    txtTotalDues.Value = dt.Rows[0]["TotalDues"].ToString();
                    txtFinanceNotes.Value = dt.Rows[0]["FinanceNotes"].ToString();

                   
                }
            }

            Common.addlog("ViewAll", "ESS", "Seperation Viewed", "");

        }

        private void disableFields() {


            ddlTypeOfSeperation.Enabled = false;
            //txtReasonForLeaving.Disabled = true;
            //txtCommentsRegardingSalary.Disabled = true;
            //txtThingsLikeAboutCompany.Disabled = true;
            txtNoticePeriodEndDate.Disabled = true;
            txtEmailForwardingPeriod.Disabled = true;
            ddlEmailStatus.Enabled = false;
            ddlDomainAccess.Enabled = false;
            chkAccessStatus.Disabled = true;
            chkCompanyInfoSotred.Disabled = true;
            txtITComments.Disabled = true;
            chkLockerKeys.Disabled = true;
            chkReturnedCar.Disabled = true;
            chkStationary.Disabled = true;
            chkReturnedCar.Disabled = true;
            chkInternetDevice.Disabled = true;
            chkMobile.Disabled = true;
            txtAdminNotes.Disabled = true;
            txtNoticePeriodDeductions.Disabled = true;
            txtAttendanceDetails.Disabled = true;
            txtUnawailedLeaveBalance.Disabled = true;
            txtMedicalRefurbishments.Disabled = true;
            txtHRNotes.Disabled = true;
            txtLoansAndAdvances.Disabled = true;
            txtCompanyLoans.Disabled = true;
            txtAnyOtherAdvance.Disabled = true;
            txtTotalDues.Disabled = true;
            txtFinanceNotes.Disabled = true;


        }

        private bool getCheckBoxValue(string val)
        {
            if (val == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}