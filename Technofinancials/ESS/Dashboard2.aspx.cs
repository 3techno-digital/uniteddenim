﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class Dashboard2 : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterPostBackControl(this.btnTimeIN);
                //scriptManager.RegisterPostBackControl(this.btnTimeOut);
                //scriptManager.RegisterPostBackControl(this.btnSubmit);

                //divAlertTheme.Visible = false;
                CheckSessions();
                Session["UserAccess"] = "Normal User";

                string CurrDate = DateTime.Now.ToString();
                checkStatus();

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                //objDB.date = DateTime.Now.ToString("dd-MMM-yyyy");
                //btnTimeOut.Visible = false;
                //DataTable dt = objDB.GetEmployeeAttendanceByDate(ref errorMsg);
                //if (dt != null)
                //{
                //    if (dt.Rows.Count > 0)
                //    {
                //        if(dt.Rows[0]["TimeOut"].ToString() == "" && dt.Rows[0]["isOnLeave"].ToString() == "True")
                //            btnTimeOut.Visible = true;

                //        btnTimeIN.Visible = false;
                //    }
                //}

                //int shiftID = 0;
                //if (Session["ShiftID"] != null)
                //{
                //    if (Session["ShiftID"].ToString() != "")
                //    {
                //        shiftID = Convert.ToInt32(Session["ShiftID"]);
                //    }
                //}
                //else
                //{
                //    btnTimeIN.Visible = false;
                //    btnTimeOut.Visible = false;
                //}
                //getWorkingDaysByShiftID(Convert.ToInt32(shiftID));
                //GetAnnouncementsByCompanyID();
                //getHolidaysByCompanyID();
                //getShiftsByID();
                //generateProvinentFundChart();
                //attendanceGauage();

                BindBirthdayRepeater();
                DataTable dt = new DataTable();
                dt = objDB.GetAttendanceSummaryKTByEmployeeID(ref errorMsg);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lblPresent.Text = dt.Rows[0]["Present"].ToString();
                    lblAbsent.Text = dt.Rows[0]["Absent"].ToString();
                    lblEarlyOut.Text = dt.Rows[0]["Early Out"].ToString();
                    lblLate.Text = dt.Rows[0]["Late In"].ToString();
                    lblLeave.Text = dt.Rows[0]["Leave"].ToString();
                }
                Common.addlog("ViewAll", "ESS", "Dashboard Viewed", "");

            }
        }

        private void GetAnnouncementsByCompanyID()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetTop5AnnouncementByCompanyID(ref errorMsg);
            gvAnnouncement.DataSource = dt;
            gvAnnouncement.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvAnnouncement.UseAccessibleHeader = true;
                    gvAnnouncement.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All Announcement Viewed", "Announcement");
        }

        #region old
        private void getShiftsByID()
        {
            DataTable dt = new DataTable();
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            dt = objDB.GetWorkingShiftByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    //tdShiftName.InnerHtml = dt.Rows[0]["ShiftName"].ToString();
                    //tdStartTime.InnerHtml = DateTime.Parse(dt.Rows[0]["StartTime"].ToString()).ToString("hh:mm tt");
                    //tdLateIn.InnerHtml = DateTime.Parse(dt.Rows[0]["LateInTime"].ToString()).ToString("hh:mm tt");
                    //tdHalfDay.InnerHtml = DateTime.Parse(dt.Rows[0]["HalfDayStart"].ToString()).ToString("hh:mm tt");
                    //tdEarlyOut.InnerHtml = DateTime.Parse(dt.Rows[0]["EarlyOut"].ToString()).ToString("hh:mm tt");
                    //tdEndTime.InnerHtml = DateTime.Parse(dt.Rows[0]["EndTime"].ToString()).ToString("hh:mm tt");

                    string workingDays = "";
                    if (dt.Rows[0]["Monday"].ToString() == "True")
                        workingDays += "Monday, ";
                    if (dt.Rows[0]["Tuesday"].ToString() == "True")
                        workingDays += "Tuesday, ";
                    if (dt.Rows[0]["Wednesday"].ToString() == "True")
                        workingDays += "Wednesday, ";
                    if (dt.Rows[0]["Thursday"].ToString() == "True")
                        workingDays += "Thursday, ";
                    if (dt.Rows[0]["Friday"].ToString() == "True")
                        workingDays += "Friday, ";
                    if (dt.Rows[0]["Saturday"].ToString() == "True")
                        workingDays += "Saturday, ";
                    if (dt.Rows[0]["Sunday"].ToString() == "True")
                        workingDays += "Sunday, ";

                    workingDays = workingDays.Remove(workingDays.Length - 1);
                    workingDays = workingDays.Remove(workingDays.Length - 1);

                    //tdWorkingDays.InnerHtml = workingDays;
                    DataTable dtWS = new DataTable();

                    objDB.WorkingShiftID = Convert.ToInt32(dt.Rows[0]["ShiftID"].ToString());
                    dtWS = objDB.GetWorkingShiftDaysByShiftIDForESS(ref errorMsg);
                    gv.DataSource = dtWS;
                    gv.DataBind();
                    if (dtWS != null)
                    {
                        if (dtWS.Rows.Count > 0)
                        {
                            gv.UseAccessibleHeader = true;
                            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                        }
                    }

                }
            }



        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected bool isTimeIn
        {
            get
            {
                if (ViewState["isTimeIn"] != null)
                {
                    return (bool)ViewState["isTimeIn"];
                }
                else
                {
                    return true;
                }
            }

            set
            {
                ViewState["isTimeIn"] = value;
            }
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            DateTime CurTime = DateTime.Now;
            DateTime lateIn, StartTime, halfDay, EndTime, OverTime, BeforeTime;
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            DataTable Empdt = objDB.GetWorkingShiftByEmployeeID(ref errorMsg);
            if (Empdt != null)
            {
                if (Empdt.Rows.Count > 0)
                {
                    lateIn = DateTime.Parse(Empdt.Rows[0]["LateInTime"].ToString());
                    StartTime = DateTime.Parse(Empdt.Rows[0]["StartTime"].ToString());
                    halfDay = DateTime.Parse(Empdt.Rows[0]["HalfDayStart"].ToString());
                    objDB.ShiftID = int.Parse(Empdt.Rows[0]["ShiftID"].ToString());

                    if (isTimeIn)
                    {
                        objDB.LateReason = txtReason.Value;
                        if (CurTime >= halfDay)
                        {
                            objDB.TimeIn = CurTime.ToString("dd-MMM-yyyy HH:mm tt");
                            objDB.Resultant = "Half Day";
                        }
                        else if (CurTime >= lateIn)
                        {
                            objDB.TimeIn = CurTime.ToString("dd-MMM-yyyy HH:mm tt");
                            objDB.Resultant = "Late In";
                        }
                        objDB.AddEmployeeAttendance();
                        Response.Redirect(Request.Url.AbsolutePath, false);
                        btnTimeIN.Visible = false;
                        btnTimeOut.Visible = true;
                    }
                    else
                    {
                        EndTime = DateTime.Parse(Empdt.Rows[0]["EndTime"].ToString());
                        OverTime = DateTime.Parse(Empdt.Rows[0]["EndTime"].ToString());
                        BeforeTime = DateTime.Parse(Empdt.Rows[0]["HalfDayStart"].ToString());
                        OverTime.AddMinutes(15);
                        objDB.ShiftID = int.Parse(Empdt.Rows[0]["ShiftID"].ToString());
                        objDB.TimeOUt = CurTime.ToString("dd-MMM-yyyy HH:mm tt");

                        if (CurTime >= OverTime)
                        {
                            objDB.Resultant = "Over Time";
                        }

                        else
                        {
                            objDB.Resultant = "Early out";
                        }
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        objDB.LateReason = txtReason.Value;
                        objDB.TimeIn = DateTime.Now.ToString("dd-MMM-yyyy");
                        objDB.AddEmployeeAttendance();
                        Response.Redirect(Request.Url.AbsolutePath, false);

                    }
                }
            }
        }

        private static bool chkMonday = true;
        private static bool chkTuesday = true;
        private static bool chkWednesday = true;
        private static bool chkThursday = true;
        private static bool chkFriday = true;
        private static bool chkSaturday = true;
        private static bool chkSunday = true;

        //private static DataTable dtHolidays;

        protected DataTable dtHolidays
        {
            get
            {
                if (ViewState["dtHolidays"] != null)
                {
                    return (DataTable)ViewState["dtHolidays"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtHolidays"] = value;
            }
        }

        private void getHolidaysByCompanyID()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dtHolidays = objDB.GetAllHolidaysByCompanyID(ref errorMsg);
        }

        private void getWorkingDaysByShiftID(int shiftID)
        {
            DataTable dtShifts = new DataTable();
            objDB.WorkingShiftID = shiftID;
            dtShifts = objDB.GetWorkingShiftByID(ref errorMsg);

            if (dtShifts != null)
            {
                if (dtShifts.Rows.Count > 0)
                {
                    chkMonday = getCheckBoxValue(dtShifts.Rows[0]["Monday"].ToString());
                    chkTuesday = getCheckBoxValue(dtShifts.Rows[0]["Tuesday"].ToString());
                    chkWednesday = getCheckBoxValue(dtShifts.Rows[0]["Wednesday"].ToString());
                    chkThursday = getCheckBoxValue(dtShifts.Rows[0]["Thursday"].ToString());
                    chkFriday = getCheckBoxValue(dtShifts.Rows[0]["Friday"].ToString());
                    chkSaturday = getCheckBoxValue(dtShifts.Rows[0]["Saturday"].ToString());
                    chkSunday = getCheckBoxValue(dtShifts.Rows[0]["Sunday"].ToString());
                }
            }
        }

        private bool getCheckBoxValue(string val)
        {
            if (val == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        protected void clndrAttendance_DayRender(object sender, DayRenderEventArgs e)
        {
            e.Day.IsSelectable = false;

            //if (e.Day.IsOtherMonth == true)
            //    e.Cell.Attributes["style"] = "display: none !important;";

            bool isHoliday = false;
            bool isNonWorkingDay = false;

            string dayName = e.Day.Date.DayOfWeek.ToString();
            if (dayName == "Monday" && chkMonday == false)
            {
                e.Cell.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                isNonWorkingDay = true;
            }
            else if (dayName == "Tuesday" && chkTuesday == false)
            {
                e.Cell.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                isNonWorkingDay = true;
            }
            else if (dayName == "Wednesday" && chkWednesday == false)
            {
                e.Cell.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                isNonWorkingDay = true;
            }
            else if (dayName == "Thursday" && chkThursday == false)
            {
                e.Cell.Attributes.Add("class", "CalP");
                //e.Cell. = "";
                isNonWorkingDay = true;
            }
            else if (dayName == "Friday" && chkFriday == false)
            {
                e.Cell.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                isNonWorkingDay = true;
            }
            else if (dayName == "Saturday" && chkSaturday == false)
            {
                e.Cell.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                isNonWorkingDay = true;
            }
            else if (dayName == "Sunday" && chkSunday == false)
            {
                e.Cell.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                isNonWorkingDay = true;
            }


            if (dtHolidays != null)
            {
                if (dtHolidays.Rows.Count > 0)
                {
                    for (int i = 0; i < dtHolidays.Rows.Count; i++)
                    {
                        DateTime currDate = e.Day.Date;
                        DateTime holidayStartDate = DateTime.Parse(dtHolidays.Rows[i]["StartDate"].ToString());
                        DateTime holidayEndDate = DateTime.Parse(dtHolidays.Rows[i]["EndDate"].ToString());

                        if (currDate >= holidayStartDate && currDate <= holidayEndDate)
                        {
                            e.Cell.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                            isHoliday = true;
                        }
                    }
                }
            }


            if (e.Day.Date <= DateTime.Now)
            {
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                objDB.date = e.Day.Date.ToString("dd-MMM-yyyy");
                string chk = objDB.CheckEmployeeAttendanceByDate();

                if (chk == "P")
                {
                    e.Cell.BackColor = System.Drawing.Color.FromArgb(1, 32, 113, 39);
                    e.Cell.ForeColor = System.Drawing.Color.White;
                }
                else if (chk == "A" && !isHoliday && !isNonWorkingDay)
                {
                    e.Cell.BackColor = System.Drawing.Color.FromArgb(1, 229, 58, 44);
                    e.Cell.ForeColor = System.Drawing.Color.White;
                }
                else if (chk == "L" && !isHoliday && !isNonWorkingDay)
                {
                    e.Cell.BackColor = System.Drawing.Color.FromArgb(1, 7, 172, 22);
                    e.Cell.ForeColor = System.Drawing.Color.White;
                }
            }
        }

        protected void generateProvinentFundChart()
        {
            //string sb = "";

            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            DataTable dt = objDB.GetLoanSummaryByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    string script = "<div data-plugin=\"chart\" data-options=\"{";
                    script += "tooltip:";
                    script += "{";
                    script += "trigger: 'item',";
                    script += "formatter: '{a} <br/>{b}: {c} ({d}%)'";
                    script += "},";
                    script += "legend:";
                    script += "{";
                    script += "orient: 'vertical',";
                    script += "x: 'left',";
                    script += "data:[";
                    script += "'Total Amount', 'Repaid Amount', 'Remaining Amount'";

                    script += "]";
                    script += "},";
                    script += "series: [";
                    script += "{";
                    script += "name: 'Loan Calculations',";
                    script += "type: 'pie',";
                    script += "radius: ['50%', '70%'],";
                    script += "avoidLabelOverlap: false,";
                    script += "label:";
                    script += "{";
                    script += "normal:";
                    script += "{";
                    script += "show: false,";
                    script += "position: 'center'";
                    script += "},";
                    script += "emphasis:";
                    script += "{";
                    script += "show: true,";
                    script += "textStyle:";
                    script += "{";
                    script += "fontSize: '30',";
                    script += "fontWeight: 'bold'";
                    script += "}";
                    script += "}";
                    script += "},";
                    script += "labelLine:";
                    script += "{";
                    script += "normal:";
                    script += "{";
                    script += "show: false";
                    script += "}";
                    script += "},";
                    script += "data:[";
                    script += "{ value: " + dt.Rows[0]["TotalAmount"].ToString() + ", name: 'Total Amount'},";
                    script += "{ value: " + dt.Rows[0]["AmountRepaid"].ToString() + ", name: 'Repaid Amount'},";
                    script += "{ value: " + dt.Rows[0]["RemaningAmount"].ToString() + ", name: 'Remaining Amount'}";

                    script += "]";
                    script += "}";
                    script += "]";
                    script += "}";
                    script += "\" style = \"height: 300px; \"></div>";

                    ltrPFChart.Text = script.ToString();
                }
            }
            //sb += @"<div data-plugin='chart' data-options='{";
            //sb += @"    tooltip : {";
            //sb += @"trigger: 'item',";
            //sb += @"formatter: '{a} <br/>{b} : {c} ({d}%)'";
            //sb += @"},";
            //sb += @"legend: {";
            //sb += @"x : 'center',";
            //sb += @"y : 'top',";
            //sb += @"data:['rose1','rose2','rose3','rose4','rose5','rose6', 'rose7']";
            //sb += @"},";
            //sb += @"calculable : true,";
            //sb += @"series : [";
            //sb += @"{";
            //sb += @"name:'Rose',";
            //sb += @"type:'pie',";
            //sb += @"radius : [20, 110],";
            //sb += @"center : ['50%', 200],";
            //sb += @"roseType : 'radius',";
            //sb += @"label: {";
            //sb += @"normal: {";
            //sb += @"show: false";
            //sb += @"},";
            //sb += @"emphasis: {";
            //sb += @"show: true";
            //sb += @"}";
            //sb += @"},";
            //sb += @"lableLine: {";
            //sb += @"normal: {";
            //sb += @"show: false";
            //sb += @"},";
            //sb += @"emphasis: {";
            //sb += @"show: true";
            //sb += @"}";
            //sb += @"},";
            //sb += @"data:[";
            //sb += @"{ value:10, name:'rose1'},";
            //sb += @"{ value:5, name:'rose2'},";
            //sb += @"{ value:15, name:'rose3'},";
            //sb += @"{ value:25, name:'rose4'},";
            //sb += @"{ value:20, name:'rose5'},";
            //sb += @"{ value:35, name:'rose6'},";
            //sb += @"{ value:30, name:'rose7'},";
            //sb += @"]";
            //sb += @"}";
            //sb += @"]";
            //sb += @"}' style='height: 300px;'>";

            //sb = @"<div data-plugin=chart; data-options={tooltip:{trigger:&#39;item&#39;,formatter: &#39;{a} <br/>{b} : {c} ({d}%)&#39;},legend: {x : &#39;center&#39;,y : &#39;top&#39;,data:[&#39;rose1&#39;,&#39;rose2&#39;,&#39;rose3&#39;,&#39;rose4&#39;,&#39;rose5&#39;,&#39;rose6&#39;, &#39;rose7&#39;]},calculable : true,series : [{name:&#39;Rose&#39;,type:&#39;pie&#39;,radius : [20, 110],center : [&#39;50%&#39;, 200],roseType : &#39;radius&#39;,label: {normal: {show: false},emphasis: {show: true}},lableLine: {normal: {show: false},emphasis: {show: true}},data:[{value:10, name:&#39;rose1&#39;},{value:5, name:&#39;rose2&#39;},{value:15, name:&#39;rose3&#39;},{value:25, name:&#39;rose4&#39;},{value:20, name:&#39;rose5&#39;},{value:35, name:&#39;rose6&#39;},{value:30, name:&#39;rose7&#39;},]}]};&#39; style=&#39;height: 300px;&#39;>";



            //ltrPFChart.Text = sb.ToString();
        }

        private void attendanceGauage()
        {
            string str = "";

            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            DataTable dt = objDB.GetAllLeavesByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    float total = 0;
                    float consumed = 0;
                    int remaining = 0;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        total += Convert.ToInt32(dt.Rows[i]["NoOfLeaves"].ToString());
                        consumed += Convert.ToInt32(dt.Rows[i]["LeavesConsumed"].ToString());
                    }

                    remaining = (int)Math.Ceiling((consumed / total) * 100.0);

                    str = " <div data-plugin='chart' data-options='{toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: \"Leaves\",type: \"gauge\",data: [{value: " + remaining.ToString() + ", name: \"Completed\"}]}]}' style='height: 300px;'> </div>";

                    ltrLeaves.Text = str;
                }
            }
        }

        //protected void btnTimeIN_ServerClick(object sender, EventArgs e)
        //{
        //    isTimeIn = true;
        //    DateTime CurTime = DateTime.Now;
        //    DateTime lateIn, StartTime, halfDay;
        //    objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
        //    DataTable Empdt = objDB.GetWorkingShiftByEmployeeID(ref errorMsg);
        //    if (Empdt != null)
        //    {
        //        if (Empdt.Rows.Count > 0)
        //        {
        //            lateIn = DateTime.Parse(Empdt.Rows[0]["LateInTime"].ToString());
        //            StartTime = DateTime.Parse(Empdt.Rows[0]["StartTime"].ToString());
        //            halfDay = DateTime.Parse(Empdt.Rows[0]["HalfDayStart"].ToString());
        //            objDB.ShiftID = int.Parse(Empdt.Rows[0]["ShiftID"].ToString());
        //            if (CurTime >= halfDay)
        //            {
        //                objDB.TimeIn = CurTime.ToString("dd-MMM-yyyy HH:mm tt");
        //                objDB.Resultant = "Half Day";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        //            }
        //            else if (CurTime >= lateIn)
        //            {
        //                objDB.TimeIn = CurTime.ToString("dd-MMM-yyyy HH:mm tt");
        //                objDB.Resultant = "Late In";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        //            }
        //            else
        //            {
        //                objDB.TimeIn = CurTime.ToString("dd-MMM-yyyy HH:mm tt");
        //                objDB.AddEmployeeAttendance();
        //                btnTimeIN.Visible = false;
        //                btnTimeOut.Visible = true;
        //            }

        //        }
        //        else
        //        {
        //            //divAlertMsg.Visible = true;
        //            //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //            //pAlertMsg.InnerHtml = "No Shift Assigned to you";
        //        }
        //    }
        //    else
        //    {
        //        //divAlertMsg.Visible = true;
        //        //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        //pAlertMsg.InnerHtml = "No Shift Assigned to you";

        //    }
        //}



        //protected void btnTimeOut_ServerClick(object sender, EventArgs e)
        //{
        //    isTimeIn = false;
        //    DateTime CurTime = DateTime.Now;
        //    DateTime EndTime, OverTime, BeforeTime;
        //    objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
        //    DataTable Empdt = objDB.GetWorkingShiftByEmployeeID(ref errorMsg);
        //    if (Empdt != null)
        //    {
        //        if (Empdt.Rows.Count > 0)
        //        {
        //            EndTime = DateTime.Parse(Empdt.Rows[0]["EndTime"].ToString());
        //            OverTime = DateTime.Parse(Empdt.Rows[0]["EndTime"].ToString());
        //            BeforeTime = DateTime.Parse(Empdt.Rows[0]["HalfDayStart"].ToString());
        //            OverTime.AddMinutes(15);

        //            objDB.ShiftID = int.Parse(Empdt.Rows[0]["ShiftID"].ToString());

        //            if (CurTime >= OverTime)
        //            {
        //                objDB.TimeOUt = CurTime.ToString("dd-MMM-yyyy HH:mm tt");
        //                objDB.Resultant = "Over Time";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        //            }
        //            else if (CurTime >= EndTime)
        //            {
        //                objDB.TimeOUt = CurTime.ToString("dd-MMM-yyyy HH:mm tt");
        //                objDB.TimeIn = DateTime.Now.ToString("dd-MMM-yyyy");
        //                objDB.AddEmployeeAttendance();
        //                Response.Redirect(Request.Url.AbsolutePath, false);
        //            }
        //            else
        //            {
        //                objDB.TimeOUt = CurTime.ToString("dd-MMM-yyyy HH:mm tt");
        //                objDB.Resultant = "Early out";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        //            }

        //        }
        //    }


        //}
        #endregion
        private void checkStatus()
        {
            btnTimeOut.Visible = false;
            btnTimeIN.Visible = false;
            btnBreakTimeOut.Visible = false;
            btnBreakTimeIN.Visible = false;
            objDB.date = DateTime.Now.ToString();
            //objDB.date = "12/16/2019 01:20:00 PM";


            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());

            DataTable dtStatus = objDB.CheckAttendanceButtonStatusWithTimeNew(ref errorMsg);

            string btnStatus = "None";

            if (dtStatus != null && dtStatus.Rows.Count > 0)
            {
                btnStatus = dtStatus.Rows[0]["BtnStatus"].ToString();
            }

            if (btnStatus == "OnLeave")
            {
                timer_is_on.Value = "0";
            }
            else if (btnStatus == "OnHoliday")
            {
                btnTimeIN.Visible = true;
                timer_is_on.Value = "0";
            }
            else if (btnStatus == "Off")
            {
                btnTimeIN.Visible = true;
                timer_is_on.Value = "0";
            }
            else if (btnStatus == "None")
            {
                timer_is_on.Value = "0";
            }
            else if (btnStatus == "TimeOut")
            {
                btnTimeOut.Visible = true;
                hdSec.Value = dtStatus.Rows[0]["TimeOut"].ToString();
                timer_is_on.Value = "1";
            }
            else if (btnStatus == "BreakEndTime")
            {
                btnBreakTimeIN.Visible = true;
                hdSec.Value = dtStatus.Rows[0]["BreakEndTime"].ToString();
                timer_is_on.Value = "1";
            }
            else if (btnStatus == "BreakTime")
            {
                btnBreakTimeOut.Visible = true;
                hdSec.Value = dtStatus.Rows[0]["BreakTime"].ToString();
                timer_is_on.Value = "1";
            }
            else if (btnStatus == "TimeIn")
            {
                btnTimeIN.Visible = true;
                timer_is_on.Value = "0";

            }
            else
            {
                timer_is_on.Value = "0";
            }

            // string btnStatus = objDB.CheckAttendanceButtonStatus();
            //if (btnStatus == "OnLeave")
            //{
            //    timer_is_on.Value = "0";
            //}
            //else if (btnStatus == "None")
            //{
            //    timer_is_on.Value = "0";
            //}
            //else if (btnStatus == "TimeOut")
            //{
            //    btnTimeOut.Visible = true;
            //    timer_is_on.Value = "1";
            //}
            //else if (btnStatus == "BreakEndTime")
            //{
            //    btnBreakTimeIN.Visible = true;
            //    timer_is_on.Value = "0";
            //}
            //else if (btnStatus == "BreakTime")
            //{
            //    btnBreakTimeOut.Visible = true;
            //    timer_is_on.Value = "1";
            //}
            //else if (btnStatus == "TimeIn")
            //{
            //    btnTimeIN.Visible = true;
            //    timer_is_on.Value = "0";
            //}
            //else
            //{
            //    timer_is_on.Value = "0";
            //}

            //GetAnnouncementsByCompanyID();
            //getHolidaysByCompanyID();
            //getShiftsByID();
            //generateProvinentFundChart();
            //attendanceGauage();


        }
        protected void btnTimeIN_ServerClick(object sender, EventArgs e)
        {

            objDB.date = DateTime.Now.ToString();
            // objDB.date = "12/16/2019 01:20:00 PM";
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            objDB.AttendanceMarkTimeINNew();
            checkStatus();
        }

        protected void btnBreakTimeOut_ServerClick(object sender, EventArgs e)
        {
            objDB.date = DateTime.Now.ToString();
            // objDB.date = "12/16/2019 1:01:00 PM";

            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            string ss = objDB.AttendanceMarkBreakOut();
            checkStatus();

        }

        protected void btnBreakTimeIN_ServerClick(object sender, EventArgs e)
        {
            objDB.date = DateTime.Now.ToString();
            // objDB.date = "12/16/2019 2:01:00 PM";
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            objDB.AttendanceMarkBreakIn();
            checkStatus();
        }
        protected void btnTimeOut_ServerClick(object sender, EventArgs e)
        {
            objDB.date = DateTime.Now.ToString();
            //       objDB.date = "12/16/2019 10:01:00 PM";
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            string ss = objDB.AttendanceMarkTimeOutNew();
            checkStatus();
        }
        private void BindBirthdayRepeater()
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetBirthdayOfNext7DaysByCompanyID(ref errorMsg);
            if (dt != null && dt.Rows.Count > 0)
            {
                repBirthday.DataSource = dt;
                repBirthday.DataBind();
            }

        }
    }
}