﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class BasicInfo : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int employeeID = 0;
        protected string empPhoto = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                BindCountriesDropDown();
                BindStatesDropDown(166);
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/dashboard";
                bindJDs();
                bindGrades();
                BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
                BindEmployeesDropDown();
                divAlertMsg.Visible = false;
                txtEmployeeCode.Disabled = true;


                if (Session["EmployeeID"] != null)
                {
                    employeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                    //getCandidateByID(candidateID);
                    getEmployeeByID(employeeID);
                    getEmployeeDetailByID(employeeID);
                    disableFields();
                }
            }
        }


        private void disableFields()
        {
            ddlEmploymentType.Enabled = false;
            txtDOJ.Disabled = true;
            ddlGrades.Enabled = false;
            ddlDept.Enabled = false;
            ddlDesg.Enabled = false;
            ddlJD.Enabled = false;
            ddlDirectReportToDept.Enabled = false;
            ddlInDirectReportToDept.Enabled = false;
            ddlDirectreportTo.Enabled = false;
            ddlInDirectreportTo.Enabled = false;
            ddlDirectReportingPerson.Enabled = false;
            ddlInDirectReportingPerson.Enabled = false;
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void bindJDs()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlJD.DataSource = objDB.GetAllNodesByCompanyID(ref errorMsg);
            ddlJD.DataTextField = "DesgTitle";
            ddlJD.DataValueField = "NodeID";
            ddlJD.DataBind();
            ddlJD.Items.Insert(0, new ListItem("--- Select ---", "0"));
        }
        protected void bindGrades()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlGrades.DataSource = objDB.GetAllGradesByCompanyID(ref errorMsg);
            ddlGrades.DataTextField = "GradeName";
            ddlGrades.DataValueField = "GradeID";
            ddlGrades.DataBind();
            ddlGrades.Items.Insert(0, new ListItem("--- Select ---", "0"));
        }

        protected void BindEmployeesDropDown()
        {
            try
            {
                objDB.CompanyID = int.Parse(Session["CompanyID"].ToString());
                ddlDirectReportingPerson.DataSource = objDB.GetAllEMPLeavesByCompanyID(ref errorMsg);
                ddlDirectReportingPerson.DataTextField = "EmployeeName";
                ddlDirectReportingPerson.DataValueField = "EmployeeID";
                ddlDirectReportingPerson.DataBind();
                ddlDirectReportingPerson.Items.Insert(0, new ListItem("--- Select ---", "0"));


                objDB.CompanyID = int.Parse(Session["CompanyID"].ToString());
                ddlInDirectReportingPerson.DataSource = objDB.GetAllEMPLeavesByCompanyID(ref errorMsg);
                ddlInDirectReportingPerson.DataTextField = "EmployeeName";
                ddlInDirectReportingPerson.DataValueField = "EmployeeID";
                ddlInDirectReportingPerson.DataBind();
                ddlInDirectReportingPerson.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }
        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDesg.Items.Clear();
            if (ddlDept.SelectedIndex != 0)
            {
                bindDesignationsDropDown(Convert.ToInt32(ddlDept.SelectedItem.Value));
            }
        }
        protected void bindDesignationsDropDown(int DeptID)
        {
            objDB.DeptID = DeptID;
            ddlDesg.DataSource = objDB.GetAllDesignationByDepartmentID(ref errorMsg);
            ddlDesg.DataTextField = "DesgTitle";
            ddlDesg.DataValueField = "DesgID";
            ddlDesg.DataBind();
            ddlDesg.Items.Insert(0, new ListItem("--- Select ---", "0"));
        }
        protected void ddlDirectReportToDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDirectreportTo.Items.Clear();
            if (ddlDirectReportToDept.SelectedIndex != 0)
            {
                bindDirectReportingDropDown(Convert.ToInt32(ddlDirectReportToDept.SelectedItem.Value));
            }
        }
        protected void bindDirectReportingDropDown(int DeptID)
        {
            objDB.DeptID = DeptID;
            ddlDirectreportTo.DataSource = objDB.GetAllDesignationByDepartmentID(ref errorMsg);
            ddlDirectreportTo.DataTextField = "DesgTitle";
            ddlDirectreportTo.DataValueField = "DesgID";
            ddlDirectreportTo.DataBind();
            ddlDirectreportTo.Items.Insert(0, new ListItem("--- None ---", "0"));
        }
        protected void ddlInDirectReportToDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlInDirectreportTo.Items.Clear();
            if (ddlInDirectReportToDept.SelectedIndex != 0)
            {
                bindInDirectReportingDropDown(Convert.ToInt32(ddlInDirectReportToDept.SelectedItem.Value));
            }
        }
        protected void bindInDirectReportingDropDown(int DeptID)
        {
            objDB.DeptID = DeptID;
            ddlInDirectreportTo.DataSource = objDB.GetAllDesignationByDepartmentID(ref errorMsg);
            ddlInDirectreportTo.DataTextField = "DesgTitle";
            ddlInDirectreportTo.DataValueField = "DesgID";
            ddlInDirectreportTo.DataBind();
            ddlInDirectreportTo.Items.Insert(0, new ListItem("--- None ---", "0"));
        }

        private void BindDepartmentDropDown(int companyID)
        {
            objDB.CompanyID = companyID;
            ddlDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDept.DataTextField = "DeptName";
            ddlDept.DataValueField = "DeptID";
            ddlDept.DataBind();
            ddlDept.Items.Insert(0, new ListItem("--- None ---", "0"));

            objDB.CompanyID = companyID;
            ddlDirectReportToDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDirectReportToDept.DataTextField = "DeptName";
            ddlDirectReportToDept.DataValueField = "DeptID";
            ddlDirectReportToDept.DataBind();
            ddlDirectReportToDept.Items.Insert(0, new ListItem("--- None ---", "0"));

            objDB.CompanyID = companyID;
            ddlInDirectReportToDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlInDirectReportToDept.DataTextField = "DeptName";
            ddlInDirectReportToDept.DataValueField = "DeptID";
            ddlInDirectReportToDept.DataBind();
            ddlInDirectReportToDept.Items.Insert(0, new ListItem("--- None ---", "0"));
        }

        private void BindCountriesDropDown()
        {
            ddlCountries.DataSource = objDB.GetAllCounties(ref errorMsg);
            ddlCountries.DataTextField = "Name";
            ddlCountries.DataValueField = "ID";
            ddlCountries.DataBind();
            ddlCountries.Items.Insert(0, new ListItem("--- Select Country ---", "0"));
            ddlCountries.SelectedValue = "166";
        }
        private void BindStatesDropDown(int CountryID)
        {
            objDB.CountryID = CountryID;
            ddlStates.DataSource = objDB.GetAllStatesByCountryID(ref errorMsg);
            ddlStates.DataTextField = "Name";
            ddlStates.DataValueField = "ID";
            ddlStates.DataBind();
            ddlStates.Items.Insert(0, new ListItem("--- Select State ---", "0"));
        }
        private void BindCitiesDropDown(int StateID)
        {
            objDB.StateID = StateID;
            ddlCities.DataSource = objDB.GetAllCitiesByStateID(ref errorMsg);
            ddlCities.DataTextField = "Name";
            ddlCities.DataValueField = "Name";
            ddlCities.DataBind();
            ddlCities.Items.Insert(0, new ListItem("--- Select City ---", "0"));
        }
        protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            BindCitiesDropDown(Convert.ToInt32(ddlStates.SelectedItem.Value));

        }
        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            BindStatesDropDown(Convert.ToInt32(ddlCountries.SelectedItem.Value));

        }
        protected void ddlGrades_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGeneralKPIS = null;
            dtGeneralKPIS = createKPIsTable();
            if (ddlGrades.SelectedIndex != 0)
            {
                DataTable dt = new DataTable();
                objDB.GradeID = Convert.ToInt32(ddlGrades.SelectedItem.Value);
                dt = objDB.GetGeneralKPIsByGradeID(ref errorMsg);
                dtGeneralKPIS = dt;
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {

                        srNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                    }
                }
            }
            //BindKPIsTable();
        }
        private DataTable dtGeneralKPIS = new DataTable();

        protected int srNo = 1;
        private DataTable createKPIsTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("KPIType");
            dt.Columns.Add("KPIDesc");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        //protected void BindKPIsTable()
        //{
        //    if (dtGeneralKPIS == null)
        //    {
        //        dtGeneralKPIS = createKPIsTable();
        //    }

        //    gv.DataSource = dtGeneralKPIS;
        //    gv.DataBind();


        //    gv.UseAccessibleHeader = true;
        //    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        //}

        private void getGeneralKPISByEmployeeID(int SurveyID)
        {
            dtGeneralKPIS = null;
            dtGeneralKPIS = new DataTable();
            dtGeneralKPIS = createKPIsTable();

            DataTable dt = new DataTable();
            objDB.EmployeeID = employeeID;
            //  dt = objDB.GetAllSurveyInvitations(ref errorMsg);
            dt = objDB.GetAllEmployeeTableByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtGeneralKPIS.Rows[0][0].ToString() == "")
                    {
                        dtGeneralKPIS.Rows[0].Delete();
                        dtGeneralKPIS.AcceptChanges();

                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtGeneralKPIS.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Title"]
                        });
                    }
                    srNo = Convert.ToInt32(dtGeneralKPIS.Rows[dtGeneralKPIS.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            //BindKPIsTable();
        }
        private void getEmployeeDetailByID(int EmpID)
        {
            objDB.EmployeeID = EmpID;
            DataTable dt = objDB.GetEmployeeByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {


                    ddlEmploymentType.SelectedValue = dt.Rows[0]["EmploymentType"].ToString();
                    txtDOJ.Value = dt.Rows[0]["DateOfJoining"].ToString();
                    ddlGrades.SelectedValue = dt.Rows[0]["GradeID"].ToString();
                    ddlDept.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                    ddlDept_SelectedIndexChanged(null, null);
                    ddlDesg.SelectedValue = dt.Rows[0]["DesgID"].ToString();
                    ddlJD.SelectedValue = dt.Rows[0]["NodeID"].ToString();

                    ddlDirectReportToDept.SelectedIndex = ddlDirectReportToDept.Items.IndexOf(ddlDirectReportToDept.Items.FindByValue(dt.Rows[0]["DirectReportingToDeptID"].ToString()));
                    ddlDirectReportToDept_SelectedIndexChanged(null, null);
                    ddlInDirectReportToDept.SelectedIndex = ddlInDirectReportToDept.Items.IndexOf(ddlInDirectReportToDept.Items.FindByValue(dt.Rows[0]["InDirectReportingToDeptID"].ToString()));
                    ddlInDirectReportToDept_SelectedIndexChanged(null, null);

                    ddlDirectreportTo.SelectedIndex = ddlDirectreportTo.Items.IndexOf(ddlDirectreportTo.Items.FindByValue(dt.Rows[0]["DirectReportingTo"].ToString()));
                    ddlInDirectreportTo.SelectedIndex = ddlInDirectreportTo.Items.IndexOf(ddlInDirectreportTo.Items.FindByValue(dt.Rows[0]["InDirectReportingTo"].ToString()));

                    ddlDirectReportingPerson.SelectedValue = dt.Rows[0]["DirectReportingPerson"].ToString();
                    ddlInDirectReportingPerson.SelectedValue = dt.Rows[0]["InDirectReportingPerson"].ToString();

                    getGeneralKPISByEmployeeID(EmpID);


                }
            }
            Common.addlog("ViewAll", "ESS", "Employee Detail Viewed", "");

        }
        private void getEmployeeByID(int EmpID)
        {
            objDB.EmployeeID = EmpID;
            DataTable dt = objDB.GetEmployeeByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    txtEmployeeCode.Value = dt.Rows[0]["EmployeeCode"].ToString();
                    txtCNIC.Value = dt.Rows[0]["CNIC"].ToString();
                    txtFirstName.Value = dt.Rows[0]["EmployeeFirstName"].ToString();
                    txtMiddleName.Value = dt.Rows[0]["EmployeeMiddleName"].ToString();
                    txtLastName.Value = dt.Rows[0]["EmployeeLastName"].ToString();
                    ddlGender.SelectedValue = dt.Rows[0]["Gender"].ToString();
                    ddlGender.SelectedValue = dt.Rows[0]["Gender"].ToString();
                    txtContactNo.Value = dt.Rows[0]["ContactNo"].ToString();
                    txtEmail.Value = dt.Rows[0]["Email"].ToString();
                    WorkdayID.Value = dt.Rows[0]["WDID"].ToString();
                    empPhoto = dt.Rows[0]["EmployeePhoto"].ToString();
                    imgPhoto.Src = empPhoto;
                    Email1.Value = dt.Rows[0]["SecEmail"].ToString();
                    txtCountry.Value = dt.Rows[0]["Country"].ToString();
                    txtState.Value = dt.Rows[0]["State"].ToString();
                    txtCity.Value = dt.Rows[0]["City"].ToString();


                    //ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));
                    //int countriesIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));
                    //if (countriesIndex == -1)
                    //    countriesIndex = 0;
                    //BindStatesDropDown(Convert.ToInt32(ddlCountries.Items[countriesIndex].Value));

                    //ddlStates.SelectedIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["State"].ToString()));
                    //int statesIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["State"].ToString()));
                    //if (statesIndex == -1)
                    //    statesIndex = 0;

                    //BindCitiesDropDown(Convert.ToInt32(ddlStates.Items[statesIndex].Value));

                    //ddlCities.SelectedIndex = ddlCities.Items.IndexOf(ddlCities.Items.FindByText(dt.Rows[0]["City"].ToString()));
                    txtAddressLine1.Value = dt.Rows[0]["AddressLine1"].ToString();
                    txtAddressLine2.Value = dt.Rows[0]["AddressLine2"].ToString();
                    txtZIPCode.Value = dt.Rows[0]["ZIPCode"].ToString();

                    //txtEmergencyPerson.Value = dt.Rows[0]["EmergencyContactPerson"].ToString();
                    //txtEmergencyPhone.Value = dt.Rows[0]["EmergencyContactNo"].ToString();

                
                 
                }
            }
            Common.addlog("ViewAll", "ESS", "Basic Info Viewed", "");

        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            string res = "";
            //if (updPhoto.HasFile)
            //{
            //    Random rand = new Random((int)DateTime.Now.Ticks);
            //    int randnum = 0;

            //    string fn = "";
            //    string exten = "";

            //    string destDir = Server.MapPath("~/assets/files/employees/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
            //    randnum = rand.Next(1, 100000);
            //    fn = Common.RemoveSpecialCharacter(txtCNIC.Value).ToLower().Replace(" ", "-") + "_" + randnum;

            //    if (!Directory.Exists(destDir))
            //    {
            //        Directory.CreateDirectory(destDir);
            //    }

            //    string fname = Path.GetFileName(updPhoto.PostedFile.FileName);
            //    exten = Path.GetExtension(updPhoto.PostedFile.FileName);
            //    updPhoto.PostedFile.SaveAs(destDir + fn + exten);

            //    empPhoto = "http://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/employees/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
            //}

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.EmployeeCode = txtEmployeeCode.Value;
        
            objDB.EmployeeFName = txtFirstName.Value;
            objDB.EmployeeMName = txtMiddleName.Value;
            objDB.EmployeeLName = txtLastName.Value;
            objDB.EmployeePhoto = empPhoto;
            objDB.CNIC = txtCNIC.Value;
            objDB.Gender = ddlGender.SelectedValue;
            objDB.ContactNo = txtContactNo.Value;
            objDB.Email = txtEmail.Value;
            objDB.CountryName = ddlCountries.SelectedItem.Text;
            objDB.StateName = ddlStates.SelectedItem.Text;
            objDB.CityName = ddlCities.SelectedItem.Text;
            objDB.AddressLine1 = txtAddressLine1.Value;
            objDB.AddressLine2 = txtAddressLine2.Value;
            objDB.ZIPCode = txtZIPCode.Value;
           
           // objDB.EmergencyContactNo = txtEmergencyPhone.Value;
           // objDB.EmergencyContactPerson = txtEmergencyPerson.Value;
          

         
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.EmployeeID = employeeID;
            res = objDB.UpdateEmployeeBasicInformation();
           
            
           
            if (res == "Basic Information Updated")
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }
        


    }
}