﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentSchedule.aspx.cs" Inherits="Technofinancials.ESS.view.PaymentSchedule" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

   <title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
 <style>
 
        .B_class {
            padding: 20px;
            border: 1px solid #000;
            background: #fff;
        }
        .B_class h1,
        .B_class h2,
        .B_class h3,
        .B_class h4,
        .B_class p,
        .B_class td {
            margin: 0;
            font-family: lato;
            color: #000;
        }
        .B_class table {
            border: 1px solid;
            margin-top: -25px;
        }
        .B_class table td,
        .B_class table th {
            padding: 3.4px;
        }
        .B_class .slip-table thead tr th {
            background-color: #ffffff;
            border-bottom: 1px solid #000000 !important;
            border-top: 1px solid #000000 !important;
            color: #000;
            padding: 0 10px;
        }
        .slip-table-bordered thead tr th,
        .slip-table-bordered thead tr td,
        .slip-table-bordered tbody tr th,
        .slip-table-bordered tbody tr td {
            border-top: 1px solid transparent !important;
            border: 0;
            vertical-align: middle;
            padding: 0.1rem;
            font-size: 14px;
        }
        .slip-table thead tr th,
        .slip-table thead tr td,
        .slip-table tbody tr th,
        .slip-table tbody tr td,
        .slip-table tfoot tr th,
        .slip-table tfoot tr td {
            height: 32px;
            line-height: 1.428571429;
            vertical-align: middle;
            border-top: 1px solid #ddd;
            padding: 0 10px;
        }
        .slip-table {
            width: 100%;
            max-width: 100%;
            background-color: transparent;
            border-collapse: collapse;
            border-spacing: 0;
        }
        .slip-table-bordered tfoot tr th,
        .slip-table-bordered tfoot tr td {
            border-top: 1px solid #000 !important;
            border: 0;
            vertical-align: middle;
            padding: 0.1rem;
            font-size: 14px;
            padding: 0 10px;
        }
        .B_class .slip-table thead tr th {
            text-align: right;
        }
        .B_class .slip-table thead tr th:nth-child(1) {
            text-align: left;
        }
        .B_class .slip-table tbody tr td {
            text-align: right;
            height: 28px;
        }
        .B_class .slip-table tbody tr td:nth-child(1) {
            text-align: left;
        }
        .B_class .slip-table tfoot tr td {
            text-align: right;
            height: 34px;
        }
        .B_class .slip-table tfoot tr td:nth-child(1) {
            text-align: left;
            width: 200px !important;
        }
        .B_class hr {
            margin-top: 20px;
            margin-bottom: 10px;
            border: 0;
            border-top: 1px solid #000000;
        }
        .tableP {
            height: 34px;
            padding: 6.4px;
            border: 1px solid;
            margin-top: -6px !important;
            width: 99.9%;
        }
        .tableP span {
            margin-left: 40px;
            text-transform: capitalize;
        }
        .AddressSide {
            height: 53px;
            color: #000;
            text-align: center;
        }
        .AddressSide h1 {
            font-size: 18px;
            text-transform: capitalize;
            font-weight: 700;
            margin: 5px 0;
            color: #000;
        }
        .AddressSide h2 {
            font-size: 18px;
            text-align: right;
            font-weight: 400;
            color: #000;
            margin: 5px 0;
        }
        .AddressSide h3 {
            font-size: 18px;
            text-transform: capitalize;
            font-weight: 700;
            margin: 5px 0;
            color: #000;
        }
        .AddressSide h4 {
            font-size: 18px;
            text-transform: capitalize;
            font-weight: 700;
            margin-bottom: 15px;
            color: #000;
            margin: 5px 0;
        }
        .AddressSide p {
            font-size: 16px;
            font-weight: 500;
            max-width: 350px;
            text-transform: capitalize;
            color: #000;
        }
        .bigBox {
            width: 100%;
            border: 1px solid #000;
            height: 400px;
            margin: 50px 0 20px;
        }
        .Box01 {
            width: 45.5%;
            display: inline-block;
            padding: 10px;
            color: #000;
        }
        .Box02 {
            width: 46%;
            display: inline-block;
            padding: 10px 20px;
            margin-left: -5px;
            color: #000;
        }
        .Box03 {
            width: 50%;
            display: inline-block;
        }
        .Box04 {
            width: 50%;
            margin-left: -5px;
            display: inline-block;
        }
        .Box05 {
            width: 100%;
            display: block;
        }
        .OneLine {
            width: 100%;
            height: 28px;
        }
        .OneLine h3 {
            display: inline-block;
            font-size: 16px;
            font-weight: 700;
            width: 35%;
            margin: 3px 0;
            color: #000;
        }
        .OneLine p {
            display: inline-block;
            font-size: 16px;
            width: 60%;
            margin: 3px 0;
            color: #000;
        }
        .totalSalaries {
            font-weight: bold !important;
            color: #188ae2 !important;
            font-size: 16px !important;
        }
        .line-four {
            width: 99.9%;
            display: block;
        }
        #tblPDF td {
            border: 1px solid;
            border-right: none;
            border-top: none;
        }
    </style>
    
	
	<style>
        .scroller{
            overflow-x: scroll !important;
   
        }
        th {
            position: sticky;
            top: 0; /* Don't forget this, required for the stickiness */
            background: #f7f7f7;
            color: #343a40;
        }

        .col-lg-4.col-md-6.col-sm-12 button#btnView {
            padding: 3px 24px;
            margin-top: 3px !important;
        }
     
        

        .green-background {
            background-color: rgb(152, 251, 152);
            color: rgb(255, 255, 255);
            font-weight: 600;
        }

        div#myModal2 {
            background-color: #3b3e4796 !important;
        }

        .modal-dialog {
            z-index: 9999;
            opacity: 1;
        }

        #modalbtnSave {
            cursor: pointer;
        }

        .modal-open {
            overflow: hidden;
        }

        .AD_btnn.two {
            font-size: 14px !important;
            margin: 0 !important;
            padding: 0 !important;
            cursor: pointer;
        }

        .AD_btnn {
            margin: 0 10px;
            font-size: 18px;
            font-weight: 700;
            color: #003780;
            background: none;
            border: 0;
            border-bottom: 2px solid transparent !important;
            border-radius: 0;
            padding: 5px;
        }

            .AD_btnn:hover {
                color: #000;
                border-bottom: 2px solid #000 !important;
            }

        .SaveBTn {
            padding: 20px 30px !important;
            margin: 0px !important;
        }

        div#myModal2 .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
            line-height: 1.3 !important;
        }

        .Time-in h4, .Time-out h4, .Breaks h4, .BreakIN h4, .Breakout h4, .Rmarks h4 {
            color: #000;
            font-weight: 600;
            margin: 10px 0 5px;
            text-align: left;
        }

        .Time-in, .Time-out, .Breaks, .BreakIN, .Breakout, .Rmarks {
            padding: 0px 15px;
            text-align: left;
        }

        .content-header h1 {
            margin-left: 8px !important;
        }

        #chartdiv {
            width: 100%;
            height: 500px;
        }

        .color_infoP h3 {
            font-size: 15px;
            margin: 0 !important;
        }

        .attendance-wrapper {
            text-align: right;
        }

        .color_infoP h3 span {
            margin-left: 15px;
        }

        .pr_0 {
            padding-right: 0 !important;
        }

        input#txtStartDate, input#txtEndDate {
            border-radius: 5px;
        }

        .form-group {
            display: block;
        }

        div#myModal .modal-content .content-header h1 {
            margin-left: 0px !important;
            font-size: 18px !important;
            padding: 0;
        }

        #LeavesGuage {
            width: 100%;
            height: 500px;
        }

        .table-bordered > tbody > tr > th {
            border-top-style: none !important;
        }

        #PieChart {
            width: 100%;
            height: 500px;
        }

        #LeavesPieChart {
            width: 100%;
            height: 500px;
        }

        .widget-body {
            padding-top: 0;
        }

        .calender {
        }

        .calenderHeading {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .form-group.color_infoP {
            width: fit-content;
            display: inline-block;
            font-size: 16px;
        }

        .content-header h1 {
            font-size: 20px !important;
        }

        .calenderCell {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .wrap.p-t-0.tf-navbar.tf-footer-wrapper {
            right: 0 !important;
        }

        g[aria-labelledby="id-66-title"] {
            display: none;
        }

        div#myModal .modal-content {
            top: 122px;
            width: 20%;
            position: fixed;
            z-index: 1050;
            left: 80%;
            height: 70%;
            box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
        }

        .modal-content .content-header {
            border-top-left-radius: 11px;
            border-top-right-radius: 11px;
            padding-top: 10px;
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
                left: 70%;
                width: 30%;
            }

            table#gvAttendanceSummary {
                width: 991px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
                left: 70%;
                width: 30%;
            }

            table#gvAttendanceSummary {
                width: 1024px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .container-fuild div {
                overflow-x: scroll;
                overflow-y: hidden;
                padding-bottom: 35px;
            }

            .modal-content {
                height: 70%;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }

            table#gvAttendanceSummary {
                width: 1024px;
            }

            .container-fuild div::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #FFF;
            }

            .container-fuild div::-webkit-scrollbar {
                height: 10px;
                background-color: #ffffff;
            }

            .container-fuild div::-webkit-scrollbar-thumb {
                background-color: #003780;
                border: 2px solid #003780;
                border-radius: 10px;
            }
        }

        @media only screen and (max-width: 1439px) and (min-width: 1200px) {
            .single-key h3 {
                font-size: 15px;
                padding: 5px;
            }
        }

        @media only screen and (max-width: 1505px) and (min-width: 1440px) {
            .single-key h3 {
                font-size: 18px;
            }
        }

        .mini-stat.clearfix.present {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #1fb5ac !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            margin-top: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.absent {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #fa8564 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.leave {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #a48ad4 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.holiday {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f4b9b9 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.missing {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #f9c851 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat.clearfix.short {
            background: #fff;
            padding: 5px;
            margin-bottom: 20px;
            border-left-style: solid;
            border-color: #aec785 !important;
            text-align: center;
            border-left-width: thick;
            border-radius: 0px;
            box-shadow: 0 2px 5px 1px #ccc;
        }

        .mini-stat-info {
            font-size: 12px;
            padding-top: 2px;
            color: #767676 !important;
            font-weight: 700;
        }

            .mini-stat-info span {
                display: block;
                font-size: 24px;
                font-weight: 600;
                color: #767676 !important;
            }



        .mini-stat {
            border-radius: 10px !important;
        }

        .btnAdjustmentTrue {
            display: none;
        }

       /* .gv-overflow-scrool div {
            overflow-x: auto;
            overflow-y: auto;
            height: 450px;
            padding-right: 5px;
        }*/

        .AD_btn {
            font-size: 16px;
            border: 0;
            padding: 0px;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
          <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Loan & Advance Payment Schedule</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                               
                                         <button class="AD_btn" id="btnView" runat="server" onserverclick="btnPDF_ServerClick" type="button">PDF</button>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="pull-right flex">
                            </div>
                        </div>
                        <!-- Modal -->
                    




                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
								<div class="col-md-3">
                                    <h4>&nbsp;</h4>
                                
                                </div>
                             
                      
                                	
                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <h4>  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPayrollDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />

                                    </div>
                                </div>

                                
                            
								<div class="col-sm-4">
								
							</div>


                            </div>


                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12"></div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
             
                   
                </section>

          </div>
            
            	<div class="container-fuild" id="slip"  runat="server">
						<div>

							<div class="B_class" id="ReportPayroll" runat="server">
								<img id="hdnCompanyLogo"  runat="server" src="/assets/images/abs-labs-pvt-limited_32002.png" alt="Company Logo" style="width: 46px; position: absolute;" />
								<div class="AddressSide">
									<h1 runat="server" id="hdnCompanyName"></h1>
									<h3>Fund Status Report</h3>
								
								</div>
								<div style="width: 100%; display: block; border: 1px solid #000;">
									<div class="">
										<div class="Box01 col-md-6 col-lg-6 col-sm-6">
											<div class="OneLine">
												<h3>Employee Code :</h3>
												<asp:Label ID="employeecode" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Name :</h3>

												<asp:Label ID="Employeename" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Location :</h3>
												<asp:Label ID="location" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Loan ID :</h3>
												<asp:Label ID="Loanid" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Loan Type :</h3>
												<asp:Label ID="loantype" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Loan Amount :</h3>
												<asp:Label ID="loanamount" runat="server"></asp:Label>
											</div>
											
											
											
										</div>
										<div class="Box02 col-md-6 col-lg-6 col-sm-6">
											<div class="OneLine">
												<h3>No of Installment :</h3>
												<asp:Label ID="noi" runat="server"></asp:Label>
											</div>
											
                                            <div class="OneLine">
												<h3>Recieved Installment :</h3>
												<asp:Label ID="receiveins" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Amount / Installment :</h3>
												<asp:Label ID="deductpermonth" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Sanctioned Amount :</h3>
												<asp:Label ID="totaldues" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Recieved Amount :</h3>
												<asp:Label ID="totalpaid" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Deduction Start :</h3>
												<asp:Label ID="effectivefrom" runat="server"></asp:Label>
											</div>
											
											
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
								<div width="100%">
								
									
								

                                    <br />
									<div class="">
										<div >
											<div class="table-1">
													<table style="background-color: #c0c0c0;width:100%;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Deductions From Clearance</td>
														</tr>
													</tbody>
												</table>
                                                 
												  <br />
												  <asp:GridView Width="100%" ID="payschedule" runat="server"  ClientIDMode="Static" AutoGenerateColumns="false">

                                <Columns>
                                 
                                    <asp:TemplateField HeaderText="Installment No">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("noofinstallment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Period">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("periodic") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Opening">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("opening","{0:N}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Addition">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("addition","{0:N}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lumpsum">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("lumpsum","{0:N}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Writeoff">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("writeoff","{0:N}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Closing">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("closeamount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Base Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("baseamount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Additional Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("additionalamount","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               <asp:TemplateField HeaderText="Deduction">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("deduction","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Outstanding">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("outstanding","{0:N}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
											</div>
										</div>
									</div>
									
									

                                    </div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
							
									<div>
										<p style="text-align: center;">Techno Financials - This is auto generated pay slip and needs no signature.</p>
									</div>
								</div>
							</div>
							<style type="text/css">
								
							</style>
							<div class="table-1 B_class" id="ReportPayrollFotter" runat="server">
								<table>
									<tbody>
										<tr>
											<td><asp:Label runat="server" ID="hdnCompanyNamefooter"> </asp:Label> - Human Capital Management</td>
											<td>© <asp:Label runat="server" ID="hdnCompanyNamecopyrights"> </asp:Label></td>
											<td>
												<asp:Label ID="Datetime" runat="server"></asp:Label></td>
											<td style="display: none;">USER:
												<asp:Label ID="username" runat="server"></asp:Label></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>



			<div id="elementH"></div>

            <!-- #dash-content -->
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
                
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
               <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

		<!-- jQuery library -->


   
    </form>
</body>
</html>
