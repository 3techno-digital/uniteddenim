﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.View
{
    public partial class EmployeeList : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            hdnIsRedirect.Value = "0";
            if (!Page.IsPostBack)
            {
                CheckSessions();
                BindWorkingShift();
                GetData();
                divAlertMsg.Visible = false;
            }
        }

        private void BindWorkingShift()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlWorkingShift.DataSource = objDB.GetApproveAllWorkingShiftsByCompanyID(ref errorMsg);
                ddlWorkingShift.DataTextField = "ShiftName";
                ddlWorkingShift.DataValueField = "ShiftID";
                ddlWorkingShift.DataBind();
                ddlWorkingShift.Items.Insert(0, new ListItem("--- Select Shift  ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void GetData()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.directMgr = Convert.ToInt32(Session["EmployeeID"]);
           // commented on 9 sep 2021
           // dt = objDB.GetAllEmployeesByManagerID(ref errorMsg);
            dt = objDB.GetAllEmployeesByDirectReporting(ref errorMsg);
            
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
           Common.addlog("ViewAll", "ESS", "All Employees Viewed", "Employees");
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }
    }
}