﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.View
{
    public partial class EmployeeOverTimeRequests : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                BindDropdowns();
                txtFromDate.Text = DateTime.Now.AddDays(-30).ToString("dd-MMM-yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
                ddlEmployee.Items.FindByValue(Session["EmployeeId"].ToString()).Selected = true;
                GetData();

            }
        }

        void BindDropdowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDepartment.DataTextField = "DeptName";
            ddlDepartment.DataValueField = "DeptID";
            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

            ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
            ddlDesignation.DataTextField = "DesgTitle";
            ddlDesignation.DataValueField = "DesgID";
            ddlDesignation.DataBind();
            ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

            //ddlEmployee.DataSource = null;
            //ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg);
            //ddlEmployee.DataTextField = "EmployeeName";
            //ddlEmployee.DataValueField = "EmployeeID";
            //ddlEmployee.DataBind();
            //ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

        }

        private void GetData(string ApprovalStatus = "")
        {
            CheckSessions();
            DataTable dt = new DataTable();

            objDB.CompanyID = Convert.ToInt16(Session["CompanyID"]);
            objDB.DesgID = Convert.ToInt16(ddlDesignation.SelectedValue);
            objDB.DeptID = Convert.ToInt16(ddlDepartment.SelectedValue);
            objDB.EmployeeID = Convert.ToInt16(ddlEmployee.SelectedValue);
            objDB.FromDate = txtFromDate.Text;
            objDB.ToDate = txtToDate.Text;

            dt = objDB.GetEmployeesOverTime(Session["EmployeeID"].ToString(), ApprovalStatus, ref errorMsg);
            
            gv.DataSource = null;
            gv.DataBind();
            
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.DataSource = dt;
                    gv.DataBind();
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

            Common.addlog("ViewAll", "ESS", "All employee-OverTime Viewed", "EmployeeAttendance");
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }
        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            DateTime toDate = Convert.ToDateTime(txtToDate.Text);
            DateTime fromDate = Convert.ToDateTime(txtFromDate.Text);

            if (toDate > DateTime.Now)
            {
                ClientScript.RegisterOnSubmitStatement(this.GetType(), "alert", fromDate > DateTime.Now ? "From " : "To " + "Date should not be greater than Current Date.");
                return;
            }

            if (fromDate > toDate)
            {
                ClientScript.RegisterOnSubmitStatement(this.GetType(), "alert", "From Date should not be greater than To Date.");
                return;
            }

            GetData();
        }

        protected void btnApprove_ServerClick(object sender, EventArgs e)
        {

        }

        protected void checkAll_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}