﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.View
{
    public partial class AuthorizationLogs : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            hdnIsRedirect.Value = "0";
            if (!Page.IsPostBack)
            {
                CheckSessions();

                GetEmployees();
                divAlertMsg.Visible = false;
            }
        }

        private void GetEmployees()
        {
            DataTable dtEmployees = new DataTable();

            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            dtEmployees = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);

            ddlEmployee.DataSource = dtEmployees;
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();


            ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            try
            {

               // GetData(10, 0);
                GetData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                gv.DataSource = null;
                gv.DataBind();
            }
        }

        private void GetData()
        {
            divAlertMsg.Visible = false;

          
            DataTable dt = new DataTable();
            string res = string.Empty;
            DateTime fromdate, todate;
            fromdate = DateTime.Parse(txtFromDate.Text);
            todate = DateTime.Parse(txtToDate.Text);
            todate = todate.AddDays(1);


            objDB.FromDate = fromdate.ToString();
            objDB.ToDate = todate.ToString();

            DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
            DateTime.TryParse(txtFromDate.Text, out Fdate);
            DateTime.TryParse(txtToDate.Text, out Tdate);
            if (Tdate < Fdate)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "From Date should be less than To Date";
                gv.DataSource = "";
                gv.DataBind();
                return;
            }
            if (Fdate > DateTime.Now)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "From Date should be Equal or less than Current Date";
                gv.DataSource = "";
                gv.DataBind();
                return;
            }

            dt = objDB.GetAuthorizationLogsByEmployeeID(Session["EmployeeID"].ToString(), Convert.ToInt16(ddlEmployee.SelectedValue),  ref errorMsg);

           
            
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;


                }
            }

            Common.addlog("ViewAll", "ESS", "Authorization logs Viewed", "AuthorizationLogs");
        }
        //private void GetData(int numberOfRecords, int from)
        //{
        //    divAlertMsg.Visible = false;

        //    DataSet ds = new DataSet();
        //    DataTable dt = new DataTable();
        //    string res = string.Empty;
        //    DateTime fromdate, todate;
        //    fromdate = DateTime.Parse(txtFromDate.Text);
        //    todate = DateTime.Parse(txtToDate.Text);
        //    todate = todate.AddDays(1);


        //    objDB.FromDate = fromdate.ToString();
        //    objDB.ToDate = todate.ToString();

        //    DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
        //    DateTime.TryParse(txtFromDate.Text, out Fdate);
        //    DateTime.TryParse(txtToDate.Text, out Tdate);
        //    if (Tdate < Fdate)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = "From Date should be less than To Date";
        //        _gv.DataSource = "";
        //        _gv.DataBind();
        //        return;
        //    }
        //    if (Fdate > DateTime.Now)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = "From Date should be Equal or less than Current Date";
        //        _gv.DataSource = "";
        //        _gv.DataBind();
        //        return;
        //    }

        //    ds = objDB.GetAuthorizationLogsByEmployeeID(Session["EmployeeID"].ToString(), Convert.ToInt16(ddlEmployee.SelectedValue), numberOfRecords, from, ref errorMsg);
        //    dt = ds.Tables[0];
        //    _gv.VirtualItemCount = Convert.ToInt32(ds.Tables[1].Rows[0]["column1"]);
        //    _gv.DataSource = dt;
        //    _gv.DataBind();
        //    if (dt != null)
        //    {
        //        if (dt.Rows.Count > 0)
        //        {
        //            _gv.UseAccessibleHeader = true;
        //            _gv.HeaderRow.TableSection = TableRowSection.TableHeader;


        //        }
        //    }

        //    Common.addlog("ViewAll", "ESS", "Authorization logs Viewed", "AuthorizationLogs");
        //}

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }





        //protected void gv_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        //{
        //    _gv.PageIndex = e.NewPageIndex;
        //    GetData(10, e.NewPageIndex);
        //}
    }
}