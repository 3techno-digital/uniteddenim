﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeList.aspx.cs" Inherits="Technofinancials.ESS.View.EmployeeList" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        .edit-class {
            border: 0px !important;
            background-color: transparent !important;
            color: #003780 !important;
            font-weight: 800 !important;
        }

        .table_out_data {
            margin: 10px 0 0 0;
        }

            .table_out_data input {
                float: none;
                width: 10%;
            }

        td label {
            margin-top: 0px;
        }

        .table_out_data label {
            display: inline-block;
            margin-top: -6px !important;
        }

        textarea#txtNotes {
            width: 565px !important;
            height: 101px !important;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sb2" runat="server" />
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">

            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">My Team</h1>
                            </div>
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-md-4">
                        </div>

                        <div class="col-md-4">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </div>
                    <div class="row ">
                        <asp:HiddenField ID="hdnIsRedirect" runat="server" />
                        <asp:HiddenField ID="hdnLinkRedirect" runat="server" />
                        <div class="col-sm-12">
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12 gv-overflow-scrool">
                                        <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No.">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Image">
                                                    <ItemTemplate>
                                                        <asp:Image runat="server" class="tableUserImage" ID="imgEmployee" ImageUrl='<%# Eval("EmployeePhoto") %>'></asp:Image>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Code">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("WorkDayID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cost Center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCostCenter" Text='<%# Eval("CostCenterName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Designation">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("DesgTitle") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol11" Text='<%# Eval("GradeName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Shift">
                                                    <ItemTemplate>
                                                        <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/employee-self-service/manage/ChangeWorkingShift/change-shift-by-employee-id-" + Eval("EmployeeID") %>"><%# Eval("ShiftName")%></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               <asp:TemplateField HeaderText="Overtime">
                                                    <ItemTemplate>
                                                        <label id="lblOverTime<%# Eval("EmployeeID")%>"><%# Eval("isOverTime")%></label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                               <asp:TemplateField HeaderText="Holiday working">
                                                    <ItemTemplate>
                                                        <label id="lblHolidayOverTime<%# Eval("EmployeeID")%>"><%# Eval("holidayworking")%></label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Authorize">
                                                    <ItemTemplate>
                                                          
                                                            <input type="checkbox" class="checkker" id="chkisAuthorize<%# Eval("EmployeeID")%>" name="<%# Eval("EmployeeID")%>"   <%# (int)(Eval("authorize")) == 1 ? "checked": "false" %> onclick="AuthorizeForPendingApproval(this)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <input type="hidden" id="txtShifID<%# Eval("EmployeeID")%>" value="<%# Eval("ShiftID")%>" />
                                                        <button type="button" id="btnEditText<%# Eval("EmployeeID")%>" onclick="OpenEmpModel(<%# Eval("EmployeeID")%>);" class="edit-class">Edit</button>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="clearfix">&nbsp;</div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="clearfix">&nbsp;</div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="modal fade M_set" id="UpdateEmployeeModel" role="dialog">
                        <div class="modal-dialog">
                             <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <input type="hidden" id="txtEmployeeID" value="0" />
                                    <h1 class="m-0 text-dark">Edit</h1>
                                    <div class="add_new">
                                        <button type="button" class="AD_btn" value="Save" id="btnCopyFields" onclick="ModifyEmployee();">Save</button>
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="tab-content">
                                                <div class="tab-pane active row">
                                                    <div class="col-sm-6" style="display: none">
                                                        <div class="form-group">
                                                            <h4>Working Shift<span style="color: red !important;">*</span></h4>
                                                            <asp:DropDownList ID="ddlWorkingShift" runat="server" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6 ">
                                                        <div class="table_out_data">
                                                            <div>
                                                                <input type="checkbox" id="chkisOverTime" name="chkisOverTime" class="checkk" />
                                                                <label for="chkisOverTime" class="checkk_lab">Allow Overtime</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                       <div class="col-sm-6 ">
                                                        <div class="table_out_data">
                                                            <div>
                                                                <input type="checkbox" id="chkisHolidayWorking" name="chkisHolidayWorking" class="checkk" />
                                                                <label for="chkisHolidayWorking" class="checkk_lab">Allow Holiday Working</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <h4>Notes</h4>
                                                        </div>
                                                        <textarea class="form-control" id="txtNotes" placeholder="Notes"></textarea>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
               </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>
    


            function OpenEmpModel(EmpID) {
                 $("#txtEmployeeID").val(EmpID);

                var isOverTime = $('#lblOverTime' + EmpID).html();
				var isHolidayWorking = $('#lblHolidayOverTime' + EmpID).html();
                if (isOverTime == 'Yes') {
                    $("#chkisOverTime").prop("checked", true);
                }
				if (isOverTime == 'No') {
					$("#chkisOverTime").prop("checked", false);
                }
				if (isHolidayWorking == 'Yes') {
					$("#chkisHolidayWorking").prop("checked", true);
				}
                if (isHolidayWorking == 'No') {
                    $("#chkisHolidayWorking").prop("checked", false);
                }
                else {

				}
                $('#UpdateEmployeeModel').modal('show');
            }
            function ModifyEmployee() {

                var Empid = $("#txtEmployeeID").val();
                var OverTime = 'False';
                var HolidayWorking = 'False';
                if ($("#chkisOverTime").prop("checked") == true) {
                    OverTime = 'True';
                }
				if ($("#chkisHolidayWorking").prop("checked") == true) {
					HolidayWorking = 'True';
				}
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/ChangeShiftAndOvertime',
					data: '{EmployeeID: ' + JSON.stringify(Empid) + ',isOverTime: ' + JSON.stringify(OverTime) + ',HolidayWorking: ' + JSON.stringify(HolidayWorking) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        if (response.d == 'Employee Data Updated' || response.d == 'Succesfully Inserted') {
                           // $('#lblOverTime' + Empid).html(OverTime);
                             $('#UpdateEmployeeModel').modal('hide');
                        }

                        alert(response.d)
                        location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }


            function AuthorizeForPendingApproval(obj) {
                var id = ($(obj).attr("id"));
                var name =  ($(obj).attr("name"));
              
                var status = 'False';
                if ($("#" + id).prop("checked") == true) {
                    status = 'True';
                }

                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/MakeIndirectMgr',
                    data: '{IndirectMgr: ' + JSON.stringify(name) + ',status: ' + JSON.stringify(status) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        if (response.d == 'Reporting Line Updated' || response.d == 'Succesfully Inserted') {
                         
                            $('#UpdateEmployeeModel').modal('hide');
							alert(response.d)
							location.reload();
							
                        }

                        
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }
		</script>
    </form>
</body>
</html>

