﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.View
{
    public partial class LoanAndAdvance : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected string PersonName
        {
            get
            {
                if (ViewState["PersonName"].ToString() != "")
                {
                    return ViewState["PersonName"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["PersonName"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["PersonName"] = "";
                CheckSessions();
                GetData();
            }
        }
       
     
        private void GetData()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            dt = objDB.GetLoanAndAdvanceByEmployeeID(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
			if (dt != null)
			{
				if (dt.Rows.Count > 0)
				{
					gv.UseAccessibleHeader = true;
					gv.HeaderRow.TableSection = TableRowSection.TableHeader;
				}
			}
			Common.addlog("ViewAll", "ESS", "Loans Viewed", "");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

       
    }
}