﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.View
{
    public partial class AttendanceAdjustment : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                GetData();
                ddlAdjustmentStatus.DataSource = objDB.GetAttendanceAdjustmentStatus(ref errorMsg);
                ddlAdjustmentStatus.DataTextField = "AdjustmentStatus";
                ddlAdjustmentStatus.DataValueField = "AdjustmentStatus";
                ddlAdjustmentStatus.DataBind();
                ddlAdjustmentStatus.Items.Insert(0, new ListItem("ALL", "0"));
            }
        }
        protected void gv_Sorting(object sender, EventArgs e)
        {
            gv.DataBind();
        }
        private DataTable FilterData(DataTable dt)
        {
            if (dt == null)
            {
                return dt;
            }
            DataTable dtFilter = new DataTable();

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.DocName = "Loans";

            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");

                    if (dtTemp != null)
                        dtFilter.Merge(dtTemp);

                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }
                }
            }

            return dtFilter;
        }

        private void GetData(string AttendanceAdjustmentStatus = "0")
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            int EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            dt = objDB.GetAllAttendanceAdjustmentByDirectReportingPerson(ref errorMsg, EmployeeID, AttendanceAdjustmentStatus);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dt = FilterData(dt);
                    dt.DefaultView.Sort = "PreparedDate desc";
                    gv.DataSource = dt;
                    gv.DataBind();

                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All employee-bonus Viewed", "AttendanceAdjustment");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void ddlAdjustmentStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetData(ddlAdjustmentStatus.SelectedValue);
        }
    }
}