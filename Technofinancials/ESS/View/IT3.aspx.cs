﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.View
{
    public partial class IT3 : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int OtherExpenseID
        {
            get
            {
                if (ViewState["OtherExpenseID"] != null)
                {
                    return (int)ViewState["OtherExpenseID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["OtherExpenseID"] = value;
            }
        }
        protected string Attachment
        {
            get
            {
                if (ViewState["Attachment"] != null)
                {
                    return (string)ViewState["Attachment"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["Attachment"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {

                    divAlertMsg.Visible = false;
                    CheckSessions();
                    GetData();

                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                Random rand = new Random((int)DateTime.Now.Ticks);
                int randnum = 0;
                randnum = rand.Next(1, 100000);

                if (FileUpload1.HasFile)
                {


                    if (FileUpload1.PostedFile.ContentLength > 2097152)
                    {
                        LblMessage.Text = "File Size(2MB) Exceeded";
                        LblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                    else
                    {
                        string filenam = FileUpload1.FileName.ToString();
                        string destDir = Server.MapPath("~/assets/Attachments/IT3Forms/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");

                        string fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;
                        if (!Directory.Exists(destDir))
                        {
                            Directory.CreateDirectory(destDir);
                        }

                        string path = "";
                        path = destDir + fn + filenam;
                        FileUpload1.PostedFile.SaveAs(path);
                        string logoPath = "/assets/Attachments/IT3Forms/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + filenam;


                        double Total_calim = double.Parse(txtSubj.Text.ToString());
                        string payrollcycle = PayrollCycle.Text;
                        string payrollcyle1 = "01-" + payrollcycle;
                        DateTime payrolldate = DateTime.ParseExact(payrollcyle1, "dd-MMM-yyyy", provider);
                        objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                        objDB.FilePath = logoPath;
                        objDB.TotalClaim = Total_calim;
                        objDB.PayRollCycle = payrolldate;
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.Status = "Submitted";
                        objDB.AddIT3FORM(ref errorMsg);
                        FileUpload1 = null;
                        GetData();

                        string res = "IT-3 Form Submitted";

                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                        pAlertMsg.InnerHtml = res;
                        PayrollCycle.Text = "";
                        txtSubj.Text = "";
                    }
                }
                // objDB.PayRollCycle= 
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void GetData()
        {
            CheckSessions();

            DataTable dt = new DataTable();
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            dt = objDB.GetIT3_Submission(ref errorMsg);

            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

           // Common.addlog("ViewAll", "HR", "All Employees Viewed", "Employees");

        }


    }
}