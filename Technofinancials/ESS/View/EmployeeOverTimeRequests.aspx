﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeOverTimeRequests.aspx.cs" Inherits="Technofinancials.ESS.View.EmployeeOverTimeRequests" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        button#btnView {
    padding: 4px 24px;
    margin-top: 15px;
}
        div#myModal2 {
            background-color: #3b3e4796 !important;

        }
         .modal-dialog{
           z-index:9999;
            opacity:1;


         }
        #modalbtnSave {
cursor:pointer;
        }
          .modal-open {
    overflow: hidden;
}
          </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <div class="modal fade M_set in" id="myModal2" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <h1 class="m-0 text-dark">Overtime Request</h1>
                                <input name="AttendanceID" type="hidden" id="txtAttendanceID" value="0" />
                                <input name="EmployeeId" type="hidden" id="txtEmployeeID" value="0" />
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <div style="text-align: right;">
                                    <span class="AD_btnn two">Close</span>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="start-date">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Date</h4>
                                <br />

                                <div class="input-group date">
                                    <input name="DateAdjust" type="text" id="DateAdjust" class="form-control datetime-picker" data-date-format="DD-MMM-YYYY" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Time In</h4>
                                <br />
                                <div class="input-group date">
                                    <input name="TimeinAdjust" type="text" id="TimeinAdjust" class="form-control datetime-picker" data-date-format="hh:mm A" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="Time-out">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Time Out</h4>
                                <br />
                                <div class="input-group date">
                                    <input name="TimeoutAdjust" type="text" id="TimeoutAdjust" class="form-control datetime-picker" data-date-format="hh:mm A" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <h4>Remarks</h4>
                                <br />
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="RamarksAdjust" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<%--                <div class="Rmarks">
                    <div class="row">
                        
                    </div>
                </div>--%>

                <div class="SaveBTn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <center><a class="AD_btn_inn" onclick="AddAdjustment()" id="modalbtnSave">Save</a></center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Over Time-Details</h1>
                            </div>
                          
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>   
                
                <section class="app-content">
                 
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>From Date
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtFromDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                               
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>To Date
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtToDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="form-group">
                                    <h4>Employee</h4>                                        
                                    <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="false"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="clear-fix">&nbsp;</div>
                                    <div>
                                        <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12" style="display:none;">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Department Name</h4>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" onchange="GetDesignationsByDepartmentIdAndCompanyId()" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Designation Name</h4>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" onchange="GetEmployeesByDesignationIdAndDepartmentId()" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row responsive" style="display: inline;">
                        <div class="col-md-12">
                            <div class="tab-content ">
                                <div class="tab-pane active">
                                    <div class="col-md-12">
                          
                                        <asp:GridView ID="gv" runat="server" CssClass="table table-bordered table-resposive gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                            <Columns>

                                                <asp:TemplateField HeaderText="Sr.No.">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox runat="server" Visible="false" AutoPostBack="true" OnCheckedChanged="checkAll_CheckedChanged" ID="checkAll" ToolTip="Click To Select All"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="check" ></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="EmployeeName">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblAttendanceDate" Text='<%# Convert.ToDateTime( Eval("AttendanceDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Time In">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTimeIn" Text='<%# Convert.ToDateTime( Eval("TimeIn")).ToString("hh:mm tt") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Time Out">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTimeOut" Text='<%# Convert.ToDateTime( Eval("TimeOut")).ToString("hh:mm tt") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Over Time Hours">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblBreak" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
    
                                                <asp:TemplateField HeaderText=" Create / View">
                                                    <ItemTemplate>
                                                         <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/employee-self-service/manage/OverTime-Details/edit-OverTime-Details-" + Eval("AttendanceID") %>" class="AD_stock"><%#(Eval("Status") == DBNull.Value ? "Create" : "View") %></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblResultant" Text='<%#  Eval("Resultant") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeID" Text='<%# Eval("EmployeeID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblBreakStart" Text='<%# Convert.ToDateTime( Eval("BreakTime")).ToString("hh:mm tt") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblBreakEnd" Text='<%# Convert.ToDateTime( Eval("BreakEndTime")).ToString("hh:mm tt") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=""><br /></div>
                       
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }
             div#gv_wrapper{
                 margin-bottom:20%;
             }

            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757 !important;
                border-radius: 0px !important;
                box-shadow: 0 8px 6px -5px #cacaca !important;
                height: 38px !important;
                width: 37px !important;
                display: inline-block !important;
                padding: 5px !important;
                position: relative !important;
                text-align: center !important;
            }
        </style>
    </form>
</body>
<script>
            
            function PopUpModal(EmpID, AttID, Adjdate, timein, timeout,overtime) {
                var modal2 = document.getElementById("myModal2");

                modal2.style.display = "block";
                $("#txtEmployeeID").val(EmpID);
                $("#txtAttendanceID").val(AttID);
                $("#DateAdjust").val(Adjdate);
                $("#DateAdjust").prop("disabled", true);
                $("#TimeinAdjust").val(timein);
                $("#TimeoutAdjust").val(timeout);
            }
            function AddAdjustment()
            {
                var date=  $("#DateAdjust").val();
                var AttID = '#btnAdjustment' + $("#txtAttendanceID").val();
                var StatusLabel = '#lblStatus' + $("#txtAttendanceID").val();
                var Empid = $("#txtEmployeeID").val();
                var timein=  $("#TimeinAdjust").val();
                var timeout=  $("#TimeoutAdjust").val();
                var remarks=  $("#RamarksAdjust").val();
                debugger;               
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/AttendanceAdjustment',
                    data: '{DateAdjust: ' + JSON.stringify(date) + ', timein: ' + JSON.stringify(timein) + ',timeout: ' + JSON.stringify(timeout) + ',remarks: ' + JSON.stringify(remarks) + ',EmpID: ' + JSON.stringify(Empid) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response)
                    {
                        
                        if (response.d == 'New Attendance Adjustment Added')
                        {
                            var modal2 = document.getElementById("myModal2");
                            modal2.style.display = "none";
                            $("#txtAttendanceID").val(0);
                            $(StatusLabel).html('Data Submitted for Review');
                            $(AttID).css("display", "none");
                        }

                        alert(response.d)
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });           
            }
</script>

</html>
