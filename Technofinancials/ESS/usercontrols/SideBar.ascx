﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SideBar.ascx.cs" Inherits="Technofinancials.ESS.usercontrols.SideBar" %>
<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar tf-sidebar">

    <!-- .app-user -->
    <asp:HiddenField ID="HasReporting" Value="" runat="server" />
      <asp:HiddenField ID="hasAccess" Value="" runat="server" />
       <asp:HiddenField ID="currentWDID" Value="" runat="server" />
    <div class="menubar-scroll">
        <div class="menubar-scroll-inner tf-sidebar-menu-items">
            <ul class="app-menu">
                     <% if(hasAccess.Value != "Limited") { %>
                <li class="tf-first-menu">
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/dashboard"); %>">
                        <img src="/assets/images/dashboard__2.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Dashboard</span>
                    </a>
                </li>
                 <% } %>
                     <% else{ %>
                <li class="tf-first-menu">
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/dashboardUS"); %>">
                        <img src="/assets/images/dashboard__2.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Dashboard</span>
                    </a>
                </li>
                 <% } %>
                <%-- 
                <li class="tf-first-menu">
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/dashboard3"); %>">
                        <img src="/assets/images/dashboard__2.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Dashboard 3</span>
                    </a>
                </li>--%>

                <%-- <li class="has-submenu">
                    <a href="javascript:void(0)" class="submenu-toggle">                                    
                        <img src="/assets/images/hr-02__2.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Budgeting & Forecasting</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/new-vacancies"); %>">
                                <span class="menu-text">New Vacancy</span>
                            </a>
                        </li>

                    </ul>
                </li>--%>

                <li class="has-submenu">
                    <a href="javascript:void(0)" class="submenu-toggle">
                        <img src="/assets/images/hr-04__2.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Personal Details</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                    </a>
                    <ul class="submenu">

                        <li class="has-submenu">
                            <a href="javascript:void(0)" class="submenu-toggle">
                                <%--<img src="/assets/images/hr-04__2.png" class="img-responsive tf-sidebar-icons" />--%>
                                <span class="menu-text">Personnel Management</span>
                                <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                            </a>
                            <ul class="submenu">
                             <%--   <li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/details"); %>"><span class="menu-text">Employment Details</span></a></li>
                               --%>
                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/basic-information"); %>">
                                        <span class="menu-text">Basic Information</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/qualification-experience"); %>">
                                        <span class="menu-text">Qualification & Experience</span>
                                    </a>
                                </li>
                                  <% if(hasAccess.Value != "Limited") { %>
                                 <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/payroll"); %>">
                                        <span class="menu-text">My Payroll</span>
                                    </a>
                                </li>
                              
                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/bank-details"); %>">
                                        <span class="menu-text">Bank Details</span>
                                    </a>
                                </li>
                                  <% } %>
                                <%--                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/AddOnsExpense"); %>">
                                        <span class="menu-text">AddOns Expense</span>
                                    </a>
                                </li>--%>
                            </ul>
                        </li>





                        <%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
                        <%--      <li>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence"); %>">
                               
                                <span class="menu-text">Attendence</span>
                            </a>
                        </li>--%>
                        <li>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/shifts"); %>">
                                <%--<img src="/assets/images/shift_white.png" class="img-responsive tf-sidebar-icons" />--%>
                                <span class="menu-text">My Shifts</span>
                            </a>
                        </li>

                        <%--<li>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/seperation"); %>">
                                <span class="menu-text">Seperation</span>
                            </a>
                        </li>--%>



                        <%--  <li>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/assets"); %>">
                                <span class="menu-text">Assets</span>
                            </a>
                        </li>--%>

                        <%--                        <li>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/loans"); %>">
                                <span class="menu-text">Loans</span>
                            </a>
                        </li>--%>

                        <%-- <li>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/employee-leaves"); %>">
                                <span class="menu-text">Leaves</span>
                            </a>
                        </li>--%>
                    </ul>
                </li>

                <% if(HasReporting.Value == "True") { %>
    <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/team-members"); %>">
                        <img src="/assets/images/users.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">My Team</span>
                    </a>
                </li>

                 
<% } %>
               
                                <li class="has-submenu ">
                    <a href="javascript:void(0)" class="submenu-toggle">
                        <img src="/assets/images/expenses.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Expense</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                    </a>
                    <ul class="submenu">
                                                           <% if(hasAccess.Value != "Limited") { %>
                        <li>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/provinent-fund"); %>">
                                <%--<img src="/assets/images/Assets__03.png" class="img-responsive tf-sidebar-icons" />--%>
                                <span class="menu-text">Provident Fund</span>
                            </a>
                        </li>
                        <% } %>
                                        <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/other-expense"); %>">
                        <%--<i class="fa fa-list-alt img-responsive tf-sidebar-icons" aria-hidden="true"></i>--%>
                        <span class="menu-text">Other Expenses</span>
                    </a>
                </li>
                <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/AddOnsExpense"); %>">
                        <%--<img src="/assets/images/Assets__03.png" class="img-responsive tf-sidebar-icons" />--%>
                        <span class="menu-text">Employee Add-ons/Deductions</span>
                    </a>
                </li>
                          <% if(HasReporting.Value == "True") { %>
                         <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/AddonsUploader"); %>">
                        <%--<img src="/assets/images/Assets__03.png" class="img-responsive tf-sidebar-icons" />--%>
                        <span class="menu-text">Add-ons/Deductions Uploader</span>
                    </a>
                </li>
                        <% } %>

            <% if(currentWDID.Value == "101342") { %>
                           <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/loansNadvanceapproval"); %>">
                        <%--<img src="/assets/images/Assets__03.png" class="img-responsive tf-sidebar-icons" />--%>
                        <span class="menu-text">Loan & Advance Approval</span>
                    </a>
                </li>

                          <% } %>
                         <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/loansNadvance"); %>">
                        <%--<img src="/assets/images/Assets__03.png" class="img-responsive tf-sidebar-icons" />--%>
                        <span class="menu-text">Loan & Advance</span>
                    </a>
                </li>
                    </ul>
                    </li>

                  <% if(HasReporting.Value == "True") { %>
                <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/ChangeWorkingShift"); %>">
                        <img src="/assets/images/clock-1.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Change Shifts</span>
                    </a>
                </li>
                <% } %>
                <%--<li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/OverTime-Details"); %>">
                        <i class="fa fa-clock-o img-responsive tf-sidebar-icons" aria-hidden="true"></i>
                        <span class="menu-text">Over Time Details</span>
                    </a>
                </li>--%>
                  <% if(hasAccess.Value != "Limited") { %>
                <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/holidays"); %>">
                        <i class="fa fa-calendar img-responsive tf-sidebar-icons" aria-hidden="true"></i>
<%--                        <img src="/assets/images/shift_white.png" class="img-responsive tf-sidebar-icons" />--%>
                        <span class="menu-text">Holidays</span>
                    </a>
                </li>
                  <% } %>
                <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/announcements"); %>">
                        <img src="/assets/images/white/icon_announcment.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Announcements</span>
                    </a>
                </li>
                <li class="has-submenu ">
                    <a href="javascript:void(0)" class="submenu-toggle">
                        <img src="/assets/images/reports-icon-02.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Reports</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                    </a>
                    <ul class="submenu">
                                   <% if(HasReporting.Value == "True") { %>
  

                 <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/audit"); %>">
                    
                        <span class="menu-text">Authorization Logs</span>
                    </a>
                </li>
<% } %>
                        <li class="has-submenu">
                            <a href="javascript:void(0)" class="submenu-toggle">
                                <%--<img src="/assets/images/hr-04__2.png" class="img-responsive tf-sidebar-icons" />--%>
                                <span class="menu-text">Attendance</span>
                                <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                            </a>
                            <ul class="submenu">
                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">
                                        <%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
                                        <span class="menu-text">Attendance Detail</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendance-sheet"); %>">
                                        <%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
                                        <span class="menu-text">Attendance Sheet</span>
                                    </a>
                                </li>
                            <%--    <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendance-sheet-flagwise"); %>">
                                       --%> <%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
                                    <%--    <span class="menu-text">Attendance Sheet Flagwise</span>
                                    </a>
                                </li>--%>


                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendance-adjustment"); %>">
                                        <%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
                                        <span class="menu-text">Attendance Adjustments</span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/break-adjustment"); %>">
                                        <%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
                                        <span class="menu-text">Break Adjustments</span>
                                    </a>
                                </li>
                                  <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/balance-leaves"); %>">
                                        <%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
                                        <span class="menu-text">My Leaves</span>
                                    </a>
                                </li>
                                      <% if(HasReporting.Value == "True") { %>
  
                 <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/tracking"); %>">
                       
                        <span class="menu-text">Tracking</span>
                    </a>
                </li>
<% } %>
               
                            </ul>
                        </li>
                        <%--<li class="has-submenu">
                            <a href="javascript:void(0)" class="submenu-toggle">
                                <span class="menu-text">Payroll Details</span>
                                <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                            </a>
                            <ul class="submenu">
                               

                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/payroll-detailed-report-ess"); %>">
                                        <span class="menu-text">Payroll Reports</span>
                                    </a>
                                </li>
                            </ul>
                        </li>--%>                                



                    </ul>
                </li>
                  <% if(hasAccess.Value != "Limited") { %>
                <li class="has-submenu ">
                    <a href="javascript:void(0)" class="submenu-toggle">
                        <img src="/assets/images/tax.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Tax</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/employee-tax-calculator"); %>">
                                <span class="menu-text">Basic Tax Calculator</span>
                            </a>
                        </li>
                        <li>
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/IT3"); %>">
                                <span class="menu-text">IT-3 Form</span>
                            </a>
                        </li>
                    </ul>
                </li>
                    <% } %>     
                  <% if(hasAccess.Value != "Limited") { %>
                <li class="has-submenu">
                            <a href="javascript:void(0)" class="submenu-toggle">
                                <img src="/assets/images/payslip.png" class="img-responsive tf-sidebar-icons" />
                                <span class="menu-text">Payslip</span>
                                <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                            </a>
                            <ul class="submenu">
                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/generate-pay-slip"); %>">
                                        <%--<img src="/assets/images/Assets__03.png" class="img-responsive tf-sidebar-icons" />--%>
                                        <span class="menu-text">Generate Payslip</span>
                                    </a>
                                </li>

<%--                                <li>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/generate-minipay-slip"); %>">
                                   
                                        <span class="menu-text">Generate Mini-Payslip</span>
                                    </a>
                                </li>--%>
                            </ul>
                        </li>
                   <% } %>    
                <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attachments"); %>">
                        <img src="/assets/images/white/polices-&-procedures.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Policies & Procedures</span>
                    </a>
                </li>

                <%--  <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/leads"); %>">
                        <img src="/assets/images/CRM/LeadsW.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Leads</span>
                    </a>
                </li>--%>
            </ul>
            <!-- .app-menu -->
        </div>
        <!-- .menubar-scroll-inner -->
    </div>
    <div class="tf-vno">tf-v2.9 <%Response.Write(DateTime.Now.ToString("MM-dd-yyyy")); %></div>
    <!-- .menubar-scroll -->
</aside>

<style>
    .slimScrollDiv {
        height: 503px !important;
        position: relative !;
        overflow: hidden !important;
        width: auto !important;
    }

    .app-menu a {
        padding-left: 8px !important;
    }
</style>
<!--========== END app aside -->
