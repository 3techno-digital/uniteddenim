﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.usercontrols
{
    public partial class SideBar : System.Web.UI.UserControl
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check if person is manager of any staff
            bool check = isManager(Session["EmployeeID"].ToString());
            HasReporting.Value = check.ToString();
            string accessLevel = Session["UserAccess"].ToString();
            hasAccess.Value = accessLevel;
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            currentWDID.Value = objDB.GetWorkdayID(ref errorMsg).Rows[0][0].ToString();

        }
        private bool isManager(string empID)
        {
            DataTable dt = new DataTable();
            objDB.EmployeeID = Convert.ToInt32(empID);
            dt = objDB.CheckIfManager(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    return true;
                }
            }
            else
            {
                return false;
            }
            return false;
        }
        private string AccessLevel(string empID)
        {
            DataTable dt = new DataTable();
            objDB.EmployeeID = Convert.ToInt32(empID);
            dt = objDB.GetAccessLevel(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    return dt.Rows[0][0].ToString();
                }
            }
            else
            {
                return "";
            }
            return "";
        }

    }
}