﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QualificationAndExperience.aspx.cs" Inherits="Technofinancials.ESS.QualificationAndExperience" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Qualification & Experience</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4" style="display:none;">
                                <div style="text-align: right;">
                                    <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">
              

                    <div class="row">
                        <div class="">
                            <div id="exTab2" class="">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#1" data-toggle="tab">Experience</a>
                                    </li>
                                    <li><a href="#2" data-toggle="tab">Qualification</a>
                                    </li>
                                    <li><a href="#3" data-toggle="tab">Certification</a>
                                    </li>
                                    <li><a href="#4" data-toggle="tab">Skill Set</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane row active" id="1">
                                        <div class="col-lg-12">
                                         
                                            <asp:GridView ID="gvExperience" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr. No">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Title">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Organization">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblOrganization" Text='<%# Eval("Organization") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="Start Year">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblJoiningYear" Text='<%# Eval("StartYear") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Joining Month">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblJoiningMonth" Text='<%# Eval("StartMonth") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Leaving Year">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblYear" Text='<%# Eval("EndYear") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Leaving Month">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblMonth" Text='<%# Eval("EndMonth") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Remarks">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblRemarks" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>


                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="tab-pane row" id="2">
                                        <div class="col-lg-12">
                                          
                                            <asp:GridView ID="gvQualification" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr. No">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Degree Level">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblDegree" Text='<%# Eval("Degree") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Year">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblYear" Text='<%# Eval("Year") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Month">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblMonth" Text='<%# Eval("Month") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Grade">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblGrade" Text='<%# Eval("Grade") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Institution">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblInstitution" Text='<%# Eval("Institution") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remarks">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblRemarks" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="tab-pane row" id="3">
                                        <div class="col-lg-12">
                                        
                                            <asp:GridView ID="gvCertification" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr. No">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Title">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Year">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblYear" Text='<%# Eval("Year") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Month">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblMonth" Text='<%# Eval("Month") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Score">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblScore" Text='<%# Eval("Score") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Institution">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblInstitution" Text='<%# Eval("Institution") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remarks">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblRemarks" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>


                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>


                                    <div class="tab-pane row" id="4">
                                        <div class="col-lg-12">
                                         
                                            <asp:GridView ID="gvSkillSet" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr. No">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Skill">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSkill" Text='<%# Eval("Skill") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Level">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblLevel" Text='<%# Eval("Level") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    <style>
        div#exTab2 {
    padding-left: 11px;
    padding-right:11px;
}
        div#gvExperience_wrapper , div#gvQualification_wrapper , div#gvCertification_wrapper , div#gvSkillSet_wrapper{
            margin-bottom:15%;
        }
    </style>
</body>
</html>
