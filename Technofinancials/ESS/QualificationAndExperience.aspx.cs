﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class QualificationAndExperience : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int employeeID = 0;
        protected string empPhoto = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();

                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/dashboard";

                divAlertMsg.Visible = false;



                if (Session["EmployeeID"] != null)
                {
                    employeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                    getExperienceByEmployeeID(employeeID);
                    getQualificationByEmployeeID(employeeID);
                    getCertificationByEmployeeID(employeeID);
                    getSkillSetByEmployeeID(employeeID);
                }
            }
        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        
        private DataTable dtQualification = new DataTable();
        protected int QualificationSrNo = 1;
        private DataTable createQualificationTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Degree");
            dt.Columns.Add("Month");
            dt.Columns.Add("Year");
            dt.Columns.Add("Grade");
            dt.Columns.Add("Institution");
            dt.Columns.Add("Remarks");

            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindQualificationTable()
        {
            if (dtQualification == null)
            {
                dtQualification = createQualificationTable();
            }

            gvQualification.DataSource = dtQualification;
            gvQualification.DataBind();

           

            gvQualification.UseAccessibleHeader = true;
            gvQualification.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        private void getQualificationByEmployeeID(int EmpID)
        {
            dtQualification = null;
            dtQualification = new DataTable();
            dtQualification = createQualificationTable();

            DataTable dt = new DataTable();
            objDB.EmployeeID = EmpID;
           
            dt = objDB.GetAllEmployeeQualificationByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtQualification.Rows[0][0].ToString() == "")
                    {
                        dtQualification.Rows[0].Delete();
                        dtQualification.AcceptChanges();

                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtQualification.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["DegreeTitle"],
                             dt.Rows[i]["DegreeMonth"],
                            dt.Rows[i]["DegreeYear"],
                            dt.Rows[i]["Grade"],
                            dt.Rows[i]["DegreeInst"],
                            dt.Rows[i]["Remark"]

                    });
                    }
                    QualificationSrNo = Convert.ToInt32(dtQualification.Rows[dtQualification.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindQualificationTable();
        }


        private DataTable dtExperience = new DataTable();
        protected int ExperienceSrNo = 1;
        private DataTable createExperienceTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("Organization");
            dt.Columns.Add("StartYear");
            dt.Columns.Add("StartMonth");
            dt.Columns.Add("EndYear");
            dt.Columns.Add("EndMonth");
            dt.Columns.Add("Remarks");

            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindExperienceTable()
        {
            if (dtExperience == null)
            {
                dtExperience = createExperienceTable();
            }

            gvExperience.DataSource = dtExperience;
            gvExperience.DataBind();

           

            gvExperience.UseAccessibleHeader = true;
            gvExperience.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        private void getExperienceByEmployeeID(int EmpID)
        {
            dtExperience = null;
            dtExperience = new DataTable();
            dtExperience = createExperienceTable();

            DataTable dt = new DataTable();
            objDB.EmployeeID = EmpID;
            //  dt = objDB.GetAllSurveyInvitations(ref errorMsg);
            dt = objDB.GetAllEmployeeExperienceByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtExperience.Rows[0][0].ToString() == "")
                    {
                        dtExperience.Rows[0].Delete();
                        dtExperience.AcceptChanges();
                      
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtExperience.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["JobTitle"],
                            dt.Rows[i]["Organization"],
                            dt.Rows[i]["StartYear"],
                             dt.Rows[i]["StartMonth"],
                            dt.Rows[i]["EndYear"],
                               dt.Rows[i]["EndMonth"],

                            dt.Rows[i]["Remark"]

                    });
                    }
                    ExperienceSrNo = Convert.ToInt32(dtExperience.Rows[dtExperience.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            Common.addlog("ViewAll", "ESS", "Qualification And Experience Viewed", "");

            BindExperienceTable();
        }


        private DataTable dtSkillSet = new DataTable();
        protected int skillSetSrNo = 1;
        private DataTable createSkillSetTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Skill");
            dt.Columns.Add("Level");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindSkillSetTable()
        {
            if (dtSkillSet == null)
            {
                dtSkillSet = createSkillSetTable();
            }

            gvSkillSet.DataSource = dtSkillSet;
            gvSkillSet.DataBind();

            gvSkillSet.UseAccessibleHeader = true;
            gvSkillSet.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        private void getSkillSetByEmployeeID(int EmpID)
        {
            dtSkillSet = null;
            dtSkillSet = new DataTable();
            dtSkillSet = createSkillSetTable();

            DataTable dt = new DataTable();
            objDB.EmployeeID = EmpID;
            //  dt = objDB.GetAllSurveyInvitations(ref errorMsg);
            dt = objDB.GetAllEmployeeSkillSetByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtSkillSet.Rows[0][0].ToString() == "")
                    {
                        dtSkillSet.Rows[0].Delete();
                        dtSkillSet.AcceptChanges();
                       
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtSkillSet.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["SkillTitle"],
                            dt.Rows[i]["SkillLevel"]

                    });
                    }
                    skillSetSrNo = Convert.ToInt32(dtSkillSet.Rows[dtSkillSet.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindSkillSetTable();
        }

        private DataTable dtCertification = new DataTable();
        protected int CertificationSrNo = 1;
        private DataTable createCertificationTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("Month");
            dt.Columns.Add("Year");
            dt.Columns.Add("Score");
            dt.Columns.Add("Institution");
            dt.Columns.Add("Remarks");

            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindCertificationTable()
        {
            if (dtCertification == null)
            {
                dtCertification = createCertificationTable();
            }

            gvCertification.DataSource = dtCertification;
            gvCertification.DataBind();

        

            gvCertification.UseAccessibleHeader = true;
            gvCertification.HeaderRow.TableSection = TableRowSection.TableHeader;
        }     
        private void getCertificationByEmployeeID(int EmpID)
        {
            dtCertification = null;
            dtCertification = new DataTable();
            dtCertification = createCertificationTable();

            DataTable dt = new DataTable();
            objDB.EmployeeID = EmpID;
            //  dt = objDB.GetAllSurveyInvitations(ref errorMsg);
            dt = objDB.GetAllEmployeeCertAndTraByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtCertification.Rows[0][0].ToString() == "")
                    {
                        dtCertification.Rows[0].Delete();
                        dtCertification.AcceptChanges();
                        
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtCertification.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                             dt.Rows[i]["Month"],
                            dt.Rows[i]["Year"],
                            dt.Rows[i]["Score"],
                            dt.Rows[i]["Inst"],
                            dt.Rows[i]["Remark"]

                    });
                    }
                    CertificationSrNo = Convert.ToInt32(dtCertification.Rows[dtCertification.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindCertificationTable();
        }

    }
}