﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class TaxCalculator : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        int selectedindex = 0;
        protected void Page_Load(object sender, EventArgs e)
        {


             //selectedindex=ddlYear.SelectedIndex ;
        }

        protected void btnCalculate_ServerClick(object sender, EventArgs e)
        {
            objDB.Salary = txtSalary.Value;
            objDB.Bonus = txtBonus.Value;
            objDB.Year = ddlYear.Value;
             selectedindex = ddlYear.SelectedIndex;

            DataTable dt = new DataTable();
            dt = objDB.CalculateTaxWithBonuses(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtMonthlySalary.Value = string.Format("{0:0,0}", dt.Rows[0]["MonthlyIncome"]);
                    txtMonthlyTax.Value = string.Format("{0:0,0}", dt.Rows[0]["MonthlyTax"]);
                    txtMonthlyTaxableSalary.Value = string.Format("{0:0,0}", dt.Rows[0]["MonthlyTaxableIncome"]);
                    txtMonthlyExemptedTax.Value = string.Format("{0:0,0}", dt.Rows[0]["ExemptedAmount"]);
                    txtSalaryAfterTax.Value = string.Format("{0:0,0}", dt.Rows[0]["SalaryAfterTax"]);
                    txtYearlySalary.Value = string.Format("{0:0,0}", dt.Rows[0]["YearlyIncome"]);
                    txtTaxableYearlySalary.Value = string.Format("{0:0,0}", dt.Rows[0]["YearlyTaxableIncome"]);
                    txtYearlyTax.Value = string.Format("{0:0,0}", dt.Rows[0]["YearlyTax"]);
                    txtyearlySalaryAfter.Value = string.Format("{0:0,0}", dt.Rows[0]["YearlyIncomeAfterTax"]);
                }
            }
             ddlYear.SelectedIndex= selectedindex;
        }
    }
}