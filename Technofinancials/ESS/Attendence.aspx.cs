﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.ESS
{
    public partial class Attendence : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        
        private   bool chkMonday = true;
        private   bool chkTuesday = true;
        private   bool chkWednesday = true;
        private   bool chkThursday = true;
        private   bool chkFriday = true;
        private   bool chkSaturday = true;
        private   bool chkSunday = true;

        private void getWorkingDaysByShiftID(int shiftID)
        {
            DataTable dtShifts = new DataTable();
            objDB.WorkingShiftID = shiftID;
            dtShifts = objDB.GetWorkingShiftByID(ref errorMsg);

            if (dtShifts != null)
            {
                if (dtShifts.Rows.Count > 0)
                {
                    chkMonday = getCheckBoxValue(dtShifts.Rows[0]["Monday"].ToString());
                    chkTuesday = getCheckBoxValue(dtShifts.Rows[0]["Tuesday"].ToString());
                    chkWednesday = getCheckBoxValue(dtShifts.Rows[0]["Wednesday"].ToString());
                    chkThursday = getCheckBoxValue(dtShifts.Rows[0]["Thursday"].ToString());
                    chkFriday = getCheckBoxValue(dtShifts.Rows[0]["Friday"].ToString());
                    chkSaturday = getCheckBoxValue(dtShifts.Rows[0]["Saturday"].ToString());
                    chkSunday = getCheckBoxValue(dtShifts.Rows[0]["Sunday"].ToString());
                }
            }
        }

        private bool getCheckBoxValue(string val)
        {
            if (val == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private DataTable dtHolidays;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                ddlEmployee_SelectedIndexChanged(null, null);
                Common.addlog("ViewAll", "ESS", "Attendence Viewed", "");

            }
        }

        private void getHolidaysByCompanyID()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dtHolidays = objDB.GetAllHolidaysByCompanyID(ref errorMsg);
        }



        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        private   DataTable dtAttendanceSheet;
        private DataTable createAttendanceTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Date");
            dt.Columns.Add("Login Time");
            dt.Columns.Add("Logout Time");
            dt.Columns.Add("Late / Over Time Reason");
            dt.Columns.Add("On Leave");
            dt.Columns.Add("Resultant");

            dt.AcceptChanges();

            return dt;
        }
        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtAttendanceSheet = null;
            gv.DataSource = null;
            gv.DataBind();

            int shiftID = 0;

            if (ddlMonth.SelectedIndex != 0 && ddlYear.SelectedIndex != 0)
            {
                if (dtAttendanceSheet == null)
                {
                    dtAttendanceSheet = createAttendanceTable();
                }



                int month = Convert.ToInt32(ddlMonth.SelectedValue);
                int year = Convert.ToInt32(ddlYear.SelectedValue);



                int days = DateTime.DaysInMonth(year, month);
                for (int day = 1; day <= days; day++)
                {
                    string date = "";
                    string logintime = "";
                    string logouttime = "";
                    string latereason = "";
                    string onleave = "";
                    string resultant = "";

                    date = new DateTime(year, month, day).ToString("dd-MMM-yyyy");


                    objDB.date = new DateTime(year, month, day).ToString("dd-MMM-yyyy");
                    objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());

                    DataTable dtTemp = objDB.GetEmployeeAttendanceByDate(ref errorMsg);
                    if (dtTemp != null)
                    {
                        if (dtTemp.Rows.Count > 0)
                        {
                            shiftID = Convert.ToInt32(dtTemp.Rows[0]["ShiftID"].ToString());

                            if (dtTemp.Rows[0]["TimeIn"].ToString() != "")
                                logintime = DateTime.Parse(dtTemp.Rows[0]["TimeIn"].ToString()).ToString("hh:mm:ss tt");

                            if (dtTemp.Rows[0]["TimeOut"].ToString() != "")
                                logouttime = DateTime.Parse(dtTemp.Rows[0]["TimeOut"].ToString()).ToString("hh:mm:ss tt");

                            if (dtTemp.Rows[0]["LateReason"].ToString() != "")
                                latereason = dtTemp.Rows[0]["LateReason"].ToString();

                            if (dtTemp.Rows[0]["Resultant"].ToString() != "")
                                resultant = dtTemp.Rows[0]["Resultant"].ToString();

                            if (dtTemp.Rows[0]["isOnLeave"].ToString() == "True")
                                onleave = "Yes";
                            else
                                onleave = "No";
                        }
                    }

                    DataRow dr = dtAttendanceSheet.NewRow();
                    dr[0] = new DateTime(year, month, day).ToString("dd-MMM-yyyy");
                    dr[1] = logintime;
                    dr[2] = logouttime;
                    dr[3] = latereason;
                    dr[4] = onleave;
                    dr[5] = resultant;

                    dtAttendanceSheet.Rows.Add(dr);
                    dtAttendanceSheet.AcceptChanges();


                }

                getHolidaysByCompanyID();
                getWorkingDaysByShiftID(shiftID);

                gv.DataSource = dtAttendanceSheet;
                gv.DataBind();
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int rowIndex = e.Row.RowIndex;
                DateTime currDate = DateTime.Parse(e.Row.Cells[0].Text);

                string dayName = currDate.DayOfWeek.ToString();
                if (dayName == "Monday" && chkMonday == false)
                    e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                else if (dayName == "Monday" && chkMonday == false)
                    e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                else if (dayName == "Tuesday" && chkTuesday == false)
                    e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                else if (dayName == "Wednesday" && chkWednesday == false)
                    e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                else if (dayName == "Thursday" && chkThursday == false)
                    e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                else if (dayName == "Friday" && chkFriday == false)
                    e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                else if (dayName == "Saturday" && chkSaturday == false)
                    e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                else if (dayName == "Sunday" && chkSunday == false)
                    e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);

                if (dtHolidays != null)
                {
                    if (dtHolidays.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtHolidays.Rows.Count; i++)
                        {
                            DateTime holidayStartDate = DateTime.Parse(dtHolidays.Rows[i]["StartDate"].ToString());
                            DateTime holidayEndDate = DateTime.Parse(dtHolidays.Rows[i]["EndDate"].ToString());

                            if (currDate >= holidayStartDate && currDate <= holidayEndDate)
                                e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                        }
                    }
                }
            }
        }
    }
}