﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Shits.aspx.cs" Inherits="Technofinancials.ESS.Shits" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        #shiftDiv {
            pointer-events: none;
            opacity: 0.7;
        }

    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>

                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-md-8">
                                        <h1 class="m-0 text-dark">My Shifts</h1>
                                    </div>
                                    <div class="col-md-4">
                                        <div style="text-align: right;">
                                            <%--<a class="AD_btn" id="btnBack" runat="server">Back</a>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                        </div>

                        <section class="app-content">
                            <div class="tab-content">
                                <div class="tab-pane active row">

                                    <div class="col-sm-12" id="shiftDiv" runat="server">
                                        <div class="row">
                                            <div class="col-md-4 form-group">
                                                <div class="row">
                                                    <div class="col-md-6 form-group">
                                                        <h4>Shift   </h4>
                                                        <asp:DropDownList ID="ddlShifts" runat="server" CssClass="form-control select2" data-plugin="select2" ClientIDMode="Static" ></asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                        <h4>Working Hours </h4>
                                                        <input class="form-control" id="txtHours" value="8" type="number" runat="server" />
                                                    </div>
                                                        <div class="col-sm-6">
									

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="allowHolidayWorking" runat="server" name="returnedLockerKeys" />
															<label for="allowHolidayWorking">Allow Holiday Working</label>
														</div>
													
								</div>
                                                </div>
                                            </div>
                                        

                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                                                <div class="form-group" id="divAlertMsg" runat="server">
                                                    <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                        <span>
                                                            <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                        </span>
                                                        <p id="pAlertMsg" runat="server">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="row" id="shiftDaysDiv" runat="server">
                                            <div class="col-sm-12 ">

                                                <div>
                                                    <table class="table table-bordered ">
                                                    <thead>
                                                        <tr>
                                                            <th>Day</th>   
                                                            <th>Off Day (OT)</th>
                                                            <th>Flexible Shift</th>
                                                            <th>Time In Exemption</th>
                                                            <th>Start Time</th>
                                                            <th>Late In Time</th>
                                                            <th>Break Start Time</th>
                                                            <th>Break End Time</th>
                                                            <th style="display: none;">Half Day Start</th>
                                                            <th>Early Out</th>
                                                            <th>End Time</th>
                                                            <th>Day<span style="color: red">+1</span> Change</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                         <tr>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" checked="checked" id="chkMonday" runat="server" onchange="CheckWeekDayOtSpecial('chkMondayIsSpecial','chkMondayOT')" name="returnedLockerKeys" />
                                                            <label for="chkMonday">Monday</label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkMondayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkMondayIsSpecial','chkMonday')" />
                                                            <label for="chkMondayOT"></label>
                                                        </div>
                                                    </div>

                                                </td>
                                                <td>
                                                    <div class="col-sm-2">

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" onchange="CheckWeekDayOtSpecial('chkMondayOT','chkMonday')" id="chkMondayIsSpecial" runat="server" name="IsSpecial" />
                                                            <label for="chkMondayIsSpecial"></label>
                                                        </div>
                                                    </div>

                                                </td>
                                                <td>
                                                    <input class="form-control exemption-timein" value="1" id="txtMonTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:00 AM" id="txtMONStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:11 AM" id="txtMONLateInTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtMONBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="1:30 PM" id="txtMONBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td style="display: none;">
                                                    <input class="form-control time-picker" value="2:30 PM" id="txtMONHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:29 PM" id="txtMONEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:30 PM" id="txtMONEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <div class="col-sm-2">

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkMonNightShift" class="NightShift" runat="server" name="Night Shift" />
                                                            <label for="chkMonNightShift"></label>
                                                        </div>
                                                    </div>

                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" checked="checked" onchange="CheckWeekDayOtSpecial('chkTuesdayIsSpecial','chkTuesdayOT')" id="chkTuesday" runat="server" name="returnedLockerKeys" />
                                                            <label for="chkTuesday">Tuesday</label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkTuesdayOT" onchange="CheckWeekDayOtSpecial('chkTuesdayIsSpecial','chkTuesday')" runat="server" name="OT" />
                                                            <label for="chkTuesdayOT"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkTuesdayIsSpecial" runat="server" onchange="CheckWeekDayOtSpecial('chkTuesdayOT','chkTuesday')" name="IsSpecial" />
                                                            <label for="chkTuesdayIsSpecial"></label>
                                                        </div>
                                                    </div>

                                                </td>
                                                <td>
                                                    <input class="form-control exemption-timein" value="1" id="txtTueTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:00 AM" id="txtTUEStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:11 AM" id="txtTUELateInTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtTUEBreakEarlyStartTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtTUEBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="1:30 PM" id="txtTUEBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:31 PM" id="txtTUEBreakLateTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td style="display: none;">
                                                    <input class="form-control time-picker" value="2:30 PM" id="txtTUEHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:29 PM" id="txtTUEEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:30 PM" id="txtTUEEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <div class="col-sm-2">

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkTUENightShift" class="NightShift" runat="server" name="Night Shift" />
                                                            <label for="chkTUENightShift"></label>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" checked="checked" id="chkWednesdat" runat="server" onchange="CheckWeekDayOtSpecial('chkWednesdayIsSpecial','chkWednesdayOT')" name="returnedLockerKeys" />
                                                            <label for="chkWednesdat">Wednesday</label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkWednesdayOT" runat="server" onchange="CheckWeekDayOtSpecial('chkWednesdayIsSpecial','chkWednesdat')" name="OT" />
                                                            <label for="chkWednesdayOT"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkWednesdayIsSpecial" runat="server" name="IsSpecial" onchange="CheckWeekDayOtSpecial('chkWednesdat','chkWednesdayOT')" />
                                                            <label for="chkWednesdayIsSpecial"></label>
                                                        </div>
                                                    </div>

                                                </td>
                                                <td>
                                                    <input class="form-control exemption-timein" value="1" id="txtWedTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:00 AM" id="txtWEDStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:11 AM" id="txtWEDLateInTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtWEDBreakEarlyStartTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtWEDBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="1:30 PM" id="txtWEDBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:31 PM" id="txtWEDBreakLateTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td style="display: none;">
                                                    <input class="form-control time-picker" value="2:30 PM" id="txtWEDHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:29 PM" id="txtWEDEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:30 PM" id="txtWEDEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <div class="col-sm-2">

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkWEDNightShift" class="NightShift" runat="server" name="Night Shift" />
                                                            <label for="chkWEDNightShift"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" checked="checked" id="chkThursday" runat="server" onchange="CheckWeekDayOtSpecial('chkThursdayOT','chkThursdayIsSpecial')" name="returnedLockerKeys" />
                                                            <label for="chkThursday">Thursday</label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkThursdayOT" runat="server" onchange="CheckWeekDayOtSpecial('chkThursdayIsSpecial','chkThursday')" name="OT" />
                                                            <label for="chkThursdayOT"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkThursdayIsSpecial" runat="server" onchange="CheckWeekDayOtSpecial('chkThursdayOT','chkThursday')" name="IsSpecial" />
                                                            <label for="chkThursdayIsSpecial"></label>
                                                        </div>
                                                    </div>

                                                </td>
                                                <td>
                                                    <input class="form-control exemption-timein" value="1" id="txtThursTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:00 AM" id="txtTHUStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:11 AM" id="txtTHULateInTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtTHUBreakEarlyStartTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtTHUBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="1:30 PM" id="txtTHUBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:31 PM" id="txtTHUBreakLateTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td style="display: none;">
                                                    <input class="form-control time-picker" value="2:30 PM" id="txtTHUHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:29 PM" id="txtTHUEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:30 PM" id="txtTHUEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <div class="col-sm-2">

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkTHUNightShift" class="NightShift" runat="server" name="Night Shift" />
                                                            <label for="chkTHUNightShift"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" checked="checked" id="chkFriday" runat="server" name="returnedLockerKeys" onchange="CheckWeekDayOtSpecial('chkFridayOT','chkFridayIsSpecial')" />
                                                            <label for="chkFriday">Friday</label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkFridayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkFridayIsSpecial','chkFriday')" />
                                                            <label for="chkFridayOT"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkFridayIsSpecial" runat="server" name="IsSpecial" onchange="CheckWeekDayOtSpecial('chkFridayOT','chkFriday')" />
                                                            <label for="chkFridayIsSpecial"></label>
                                                        </div>
                                                    </div>

                                                </td>
                                                <td>
                                                    <input class="form-control exemption-timein" value="1" id="txtFriTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:00 AM" id="txtFRIStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:11 AM" id="txtFRILateInTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtFRIBreakEarlyStartTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtFRIBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="2:30 PM" id="txtFRIBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="2:31 PM" id="txtFRIBreakLateTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td style="display: none;">
                                                    <input class="form-control time-picker" value="3:00 PM" id="txtFRIHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="6:29 PM" id="txtFRIEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="6:30 PM" id="txtFRIEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <div class="col-sm-2">

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkFRINightShift" class="NightShift" runat="server" name="Night Shift" />
                                                            <label for="chkFRINightShift"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkSaturday" runat="server" name="returnedLockerKeys" onchange="CheckWeekDayOtSpecial('chkSaturdayOT','chkSaturdayIsSpecial')" />
                                                            <label for="chkSaturday">Saturday</label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkSaturdayOT" onchange="CheckWeekDayOtSpecial('chkSaturdayIsSpecial','chkSaturday')" runat="server" name="OT" />
                                                            <label for="chkSaturdayOT"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkSaturdayIsSpecial" onchange="CheckWeekDayOtSpecial('chkSaturdayOT','chkSaturday')" runat="server" name="IsSpecial" />
                                                            <label for="chkSaturdayIsSpecial"></label>
                                                        </div>
                                                    </div>

                                                </td>
                                                <td>
                                                    <input class="form-control exemption-timein" value="1" id="txtSatTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server"/></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:00 AM" id="txtSATStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:11 AM" id="txtSATLateInTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtSATBreakEarlyStartTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtSATBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="1:30 PM" id="txtSATBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:31 PM" id="txtSATBreakLateTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td style="display: none;">
                                                    <input class="form-control time-picker" value="2:30 PM" id="txtSATHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:29 PM" id="txtSATEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:30 PM" id="txtSATEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <div class="col-sm-2">

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkSATNightShift" class="NightShift" runat="server" name="Night Shift" />
                                                            <label for="chkSATNightShift"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkSunday" onchange="CheckWeekDayOtSpecial('chkSundayOT','chkSundayIsSpecial')" runat="server" name="returnedLockerKeys" />
                                                            <label for="chkSunday">Sunday</label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkSundayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkSundayIsSpecial','chkSunday')" />
                                                            <label for="chkSundayOT"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="col-sm-2">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkSundayIsSpecial" runat="server" name="IsSpecial" onchange="CheckWeekDayOtSpecial('chkSundayOT','chkSunday')" />
                                                            <label for="chkSundayIsSpecial"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <input class="form-control exemption-timein" value="1" id="txtSunTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:00 AM" id="txtSUNStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="9:11 AM" id="txtSUNLateInTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtSUNBreakEarlyStartTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td>
                                                    <input class="form-control time-picker" value="1:00 PM" id="txtSUNBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="1:30 PM" id="txtSUNBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <%--<td>
                                                    <input class="form-control time-picker" value="1:31 PM" id="txtSUNBreakLateTime" placeholder="00:00" type="text" runat="server" /></td>--%>
                                                <td style="display: none;">
                                                    <input class="form-control time-picker" value="2:30 PM" id="txtSUNHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:29 PM" id="txtSUNEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <input class="form-control time-picker" value="5:30 PM" id="txtSUNEndTime" placeholder="00:00" type="text" runat="server" /></td>
                                                <td>
                                                    <div class="col-sm-2">

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="chkSUNNightShift" class="NightShift" runat="server" name="Night Shift" />
                                                            <label for="chkSUNNightShift"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                                    </tbody>
                                                </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="clearfix">&nbsp;</div>
                        </section>
                    </ContentTemplate>
                
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
        </style>
        <script>
            $('#chkNightShift').change(function () {
                if ($(this).is(':checked')) {
                    $('.NightShift').prop('checked', true);
                }
                else {
                    $('.NightShift').prop('checked', false);
                }

            }
            );


        </script>
        <!-- Modal -->
    </form>
</body>
</html>
