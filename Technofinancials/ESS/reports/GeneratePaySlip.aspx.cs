﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Technofinancials.business.classes;

namespace Technofinancials.ESS.reports
{
    public partial class GeneratePaySlip : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected string PersonName
        {
            get
            {
                if (ViewState["PersonName"].ToString() != "")
                {
                    return ViewState["PersonName"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["PersonName"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();

                hdnCompanyName.InnerText = Session["CompanyName"].ToString();
                hdnCompanyNamefooter.Text = Session["CompanyName"].ToString();
                hdnCompanyNamecopyrights.Text = Session["CompanyName"].ToString();
                hdnCompanyLogo.Src = Session["CompanyLogo"].ToString();
                if (!Page.IsPostBack)
                {
                    BindDropDown();
                    PrintBtn.Visible = false;
                    ReportPayroll.Visible = false;
                    ReportPayrollFotter.Visible = false;
                    divAlertMsg.Visible = false;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        void BindDropDown()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);

            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void ddlYear_SelectedIndexChangedNew(object sender, EventArgs e)
        {
            try
            {
                decimal totalMedicalYTD = 0;
                decimal totalYTDminus = 0;
                decimal sumTaxableAmount2 = 0;
                decimal AnnualTaxableIncome2 = 0;
                decimal minusValue = 0;
                decimal CalSum = 0;
                decimal sumYTDTAX = 0;
                decimal sumYTDAddTax = 0;

                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);// Convert.ToInt32(Session["EmployeeID"]);
                objDB.PayrollDate = txtPayrollDate.Text;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                DataTable dt = new DataTable();
                dt = objDB.GetEmployeeByID(ref errorMsg);

                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        PrintBtn.Visible = true;
                        Month.Text = txtPayrollDate.Text;
                        empname.Text = dt.Rows[0]["EmployeeName"].ToString();
                        empDpt.Text = dt.Rows[0]["DeptName"].ToString();
                        //DateofConf.Text = dt.Rows[0]["DateOfJoining"].ToString();
                        CNIC.Text = dt.Rows[0]["CNIC"].ToString();
                        BankName.Text = dt.Rows[0]["BankName"].ToString();
                        empCode.Text = dt.Rows[0]["WDID"].ToString();
                        empLocation.Text = dt.Rows[0]["PlaceOFPost"].ToString();
                        empDesg.Text = dt.Rows[0]["DesgTitle"].ToString();
                        DateofJoin.Text = Convert.ToDateTime(dt.Rows[0]["DateOfJoining"]).ToString("dd-MM-yyyy");
                        DOB.Text = Convert.ToDateTime(dt.Rows[0]["DOB"]).ToString("dd-MM-yyyy"); 
                        empAccount.Text = dt.Rows[0]["AccountNo"].ToString();
                        empType.Text = dt.Rows[0]["EmploymentType"].ToString();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                    ReportPayroll.Visible = false;
                    ReportPayrollFotter.Visible = false;
                }

                DataTable dt2 = objDB.GetPaySlipKTByEmployeeID(ref errorMsg);
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    try
                    {
                        double total, total2, NetAmount;

                        username.Text = dt.Rows[0]["EmployeeName"].ToString();
                        Datetime.Text = DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss");
                        BasicSalary.Text = Convert.ToDecimal(dt2.Rows[0]["EmployeeSalary"]).ToString("N2");
                        lblPresentDays.Text = (Convert.ToInt16(dt2.Rows[0]["TotalDays"]) - Convert.ToInt16(dt2.Rows[0]["AbsentDays"])).ToString();
                        lblTotalDays.Text = dt2.Rows[0]["TotalDays"].ToString();

                        string basicSalary = dt2.Rows[0]["BasicSalary"].ToString();
                        string SalesComissions = dt2.Rows[0]["Commission"].ToString();
                        string houseRents = dt2.Rows[0]["HouseRent"].ToString();
                        string UtilityAllows = dt2.Rows[0]["UtilityAllowence"].ToString();
                        string mdeicalAllowance = dt2.Rows[0]["MedicalAllowence"].ToString();
                        string ExpenseReims = dt2.Rows[0]["OtherExpenses"].ToString();
                        string EidBonuss = dt2.Rows[0]["EidBonus"].ToString();
                        string Spiffss = dt2.Rows[0]["Spiffs"].ToString();
                        string Overtimes = dt2.Rows[0]["OverTimeGeneralAmount"].ToString();
                        string OvertimeHolidayss = dt2.Rows[0]["OvertimeHolidayAmount2"].ToString();
                        string IncomeTaxs = dt2.Rows[0]["Tax"].ToString();
                        string AddIncomeTaxs = dt2.Rows[0]["AdditionalTax"].ToString();
                        string EOBIs = dt2.Rows[0]["EOBI"].ToString();
                        string OtherAllowances = dt2.Rows[0]["Other_Allowances"].ToString();
                        string PFNew = dt2.Rows[0]["PF"].ToString();
                        string OtherDeduction = dt2.Rows[0]["OtherDeductions"].ToString();
                        string TotSal2 = dt2.Rows[0]["NetSalry2"].ToString();
                        double totalsumm = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss) + double.Parse(EidBonuss);
                        TotSal2 = totalsumm.ToString();
                        double totalgs = double.Parse(basicSalary) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows);
                        decimal tts = decimal.Parse(totalgs.ToString()).RoundDecimalTwoDigit();
                        sumTaxableAmount2 = decimal.Parse(basicSalary) + decimal.Parse(houseRents) + decimal.Parse(UtilityAllows);
                        total = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss);
                        total2 = double.Parse(IncomeTaxs) + double.Parse(EOBIs) + double.Parse(PFNew) + double.Parse(OtherDeduction) + double.Parse(AddIncomeTaxs);
                        NetAmount = total - total2;

                        decimal bs = decimal.Parse(basicSalary.ToString()).RoundDecimalTwoDigit();
                        decimal ttt = decimal.Parse(TotSal2.ToString()).RoundDecimalTwoDigit();
                        decimal sc = decimal.Parse(SalesComissions.ToString()).RoundDecimalTwoDigit();
                        decimal hr = decimal.Parse(houseRents.ToString()).RoundDecimalTwoDigit();
                        decimal ua = decimal.Parse(UtilityAllows.ToString()).RoundDecimalTwoDigit();
                        decimal er = decimal.Parse(ExpenseReims.ToString()).RoundDecimalTwoDigit();
                        decimal eb = decimal.Parse(EidBonuss.ToString()).RoundDecimalTwoDigit();
                        decimal sp = decimal.Parse(Spiffss.ToString()).RoundDecimalTwoDigit();
                        decimal ot = decimal.Parse(Overtimes.ToString()).RoundDecimalTwoDigit();
                        decimal oth = decimal.Parse(OvertimeHolidayss.ToString()).RoundDecimalTwoDigit();
                        decimal it = decimal.Parse(IncomeTaxs.ToString()).RoundDecimalTwoDigit();
                        decimal ait = decimal.Parse(AddIncomeTaxs.ToString()).RoundDecimalTwoDigit();
                        decimal eobi = decimal.Parse(EOBIs.ToString()).RoundDecimalTwoDigit();
                        decimal ma = decimal.Parse(mdeicalAllowance.ToString()).RoundDecimalTwoDigit();
                        decimal ollow = decimal.Parse(OtherAllowances.ToString()).RoundDecimalTwoDigit();
                        decimal pf = decimal.Parse(PFNew.ToString()).RoundDecimalTwoDigit();
                        decimal odnew = decimal.Parse(OtherDeduction.ToString()).RoundDecimalTwoDigit();

                        OtherDeductiontxt.Text = odnew.ToString("N2").GetDashWhenZero();
                        PFtxt.Text = pf.ToString("N2").GetDashWhenZero();
                        basicSalry.Text = bs.ToString("N2").GetDashWhenZero();
                        SalesComission.Text = sc.ToString("N2").GetDashWhenZero();
                        houseRent.Text = hr.ToString("N2").GetDashWhenZero();
                        UtilityAllow.Text = ua.ToString("N2").GetDashWhenZero();
                        ExpenseReim.Text = er.ToString("N2").GetDashWhenZero();
                        EidBonus.Text = eb.ToString("N2").GetDashWhenZero();
                        Spiffs.Text = sp.ToString("N2").GetDashWhenZero();
                        Overtime.Text = ot.ToString("N2").GetDashWhenZero();
                        OvertimeHolidays.Text = oth.ToString("N2").GetDashWhenZero();
                        IncomeTax.Text = it.ToString("N2").GetDashWhenZero();
                        AddIncomeTax.Text = ait.ToString("N2").GetDashWhenZero();
                        EOBI.Text = eobi.ToString("N2").GetDashWhenZero();
                        mdecialAllow.Text = ma.ToString("N2").GetDashWhenZero();
                        Total1.Text = ttt.ToString("N2").GetDashWhenZero();
                        Total3.Text = total2.ToString("N2").GetDashWhenZero();
                        NetAmnt.Text = NetAmount.ToString("N2").GetDashWhenZero();
                        TotalGross.Text = tts.ToString("N2").GetDashWhenZero();
                        AmountWrds.Text = NumberToWords(Convert.ToInt32(NetAmount)) + " only-";
                        OtherAllowance.Text = ollow.ToString("N2").GetDashWhenZero();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";

                    ReportPayroll.Visible = false;
                    ReportPayrollFotter.Visible = false;
                    return;
                }

                string date = "01-" + txtPayrollDate.Text;
                objDB.PayrollDate = date;
                DateTime dateTime12 = Convert.ToDateTime(date);
                int months = int.Parse(dateTime12.ToString("MM"));
                int RemainingMonths = 0;
                double TotalSalary2 = 0;

                RemainingMonths = months > 6 ? 18 - months : 6 - months;

                if (RemainingMonths != 0)
                {
                    double subtract = double.Parse(dt.Rows[0]["GrossAmount"].ToString()) - double.Parse(dt.Rows[0]["MedicalAllowance"].ToString());
                    TotalSalary2 = RemainingMonths * subtract;
                }

                DataTable dt3 = objDB.GetPaySlipByEmployeeID_YTD(ref errorMsg);
                double sumYTD = 0;
                double SumI = 0;
                double AnnualTaxableIncome = 0;
                if (dt3 != null && dt3.Rows.Count > 0)
                {
                    decimal bsytd = decimal.Parse(dt3.Rows[0]["BasicSalaryYTD"].ToString().ToString()).RoundDecimalTwoDigit();
                    decimal scytd = decimal.Parse(dt3.Rows[0]["CommissionYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal HRYTD = decimal.Parse(dt3.Rows[0]["HouseRentYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal MAYTD = decimal.Parse(dt3.Rows[0]["MedicalAllowenceYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal UAYTD = decimal.Parse(dt3.Rows[0]["UtilityAllowenceYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal OEYTD = decimal.Parse(dt3.Rows[0]["OtherExpensesYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal ebytd = decimal.Parse(dt3.Rows[0]["EidBonusYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal sytd = decimal.Parse(dt3.Rows[0]["SpiffsYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal otytd = decimal.Parse(dt3.Rows[0]["OverTimeYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal ebhytd = decimal.Parse(dt3.Rows[0]["OvertimeHolidayYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal itytd = decimal.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal eobiytd = decimal.Parse(dt3.Rows[0]["EobiYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal overtimetoalytd = decimal.Parse(dt3.Rows[0]["OverTimeTotalAmountsYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal otytds = decimal.Parse(dt3.Rows[0]["Other_AllowancesYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal pfytd = decimal.Parse(dt3.Rows[0]["PFYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal odytd = decimal.Parse(dt3.Rows[0]["OtherDeductionsYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal totalgsYTD = bsytd + HRYTD + MAYTD + UAYTD;
                    decimal TOTYTD = decimal.Parse(dt3.Rows[0]["TotalSalry2"].ToString()).RoundDecimalTwoDigit();

                    double totalsummytd = double.Parse(bsytd.ToString())
                        + double.Parse(otytds.ToString())
                        + double.Parse(scytd.ToString())
                        + double.Parse(HRYTD.ToString())
                        + double.Parse(MAYTD.ToString())
                        + double.Parse(UAYTD.ToString())
                        + double.Parse(OEYTD.ToString())
                        + double.Parse(sytd.ToString())
                         + double.Parse(ebytd.ToString())
                        + double.Parse(overtimetoalytd.ToString());

                    totalYTDminus = decimal.Parse(totalsummytd.ToString());
                    string TotSal2ytds = totalsummytd.ToString();

                    TotalGrossYTD.Text = totalgsYTD.ToString("N2").GetDashWhenZero();
                    OtherDeductiontxtYTD.Text = odytd.ToString("N2").GetDashWhenZero();
                    PFYTD.Text = pfytd.ToString("N2").GetDashWhenZero();
                    OtherAllowanceYTD.Text = otytds.ToString("N2").GetDashWhenZero();
                    basicSalryYTD.Text = bsytd.ToString("N2").GetDashWhenZero();
                    SalesComissionYTD.Text = scytd.ToString("N2").GetDashWhenZero();
                    houseRentYTD.Text = HRYTD.ToString("N2").GetDashWhenZero();
                    mdecialAllowYTD.Text = MAYTD.ToString("N2").GetDashWhenZero();
                    UtilityAllowYTD.Text = UAYTD.ToString("N2").GetDashWhenZero();
                    ExpenseReimYTD.Text = OEYTD.ToString("N2").GetDashWhenZero();
                    EidBonusYTD.Text = ebytd.ToString("N2").GetDashWhenZero();
                    SpiffsYTD.Text = sytd.ToString("N2").GetDashWhenZero();
                    OvertimeYTD.Text = otytd.ToString("N2").GetDashWhenZero();
                    OvertimeHolidaysYTD.Text = ebhytd.ToString("N2").GetDashWhenZero();
                    IncomeTaxYTD.Text = itytd.ToString("N2").GetDashWhenZero();
                    EOBIYTD.Text = eobiytd.ToString("N2").GetDashWhenZero();

                    double adtytdd = Convert.ToDouble(dt3.Rows[0]["AddIncomeTaxYTD"].ToString());
                    AddIncomeTaxYTD.Text = adtytdd.ToString("N2").GetDashWhenZero();
                    SumI = double.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString()) + double.Parse(dt3.Rows[0]["EobiYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherDeductionsYTD"].ToString()) + double.Parse(dt3.Rows[0]["PFYTD"].ToString()) + double.Parse(dt3.Rows[0]["AddIncomeTaxYTD"].ToString());
                    sumYTD = double.Parse(dt3.Rows[0]["BasicSalaryYTD"].ToString()) + double.Parse(dt3.Rows[0]["Other_AllowancesYTD"].ToString()) + double.Parse(dt3.Rows[0]["CommissionYTD"].ToString()) + double.Parse(dt3.Rows[0]["HouseRentYTD"].ToString()) + double.Parse(dt3.Rows[0]["MedicalAllowenceYTD"].ToString()) + double.Parse(dt3.Rows[0]["UtilityAllowenceYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherExpensesYTD"].ToString()) + double.Parse(dt3.Rows[0]["EidBonusYTD"].ToString()) + double.Parse(dt3.Rows[0]["SpiffsYTD"].ToString()) + double.Parse(dt3.Rows[0]["OverTimeYTD"].ToString()) + double.Parse(dt3.Rows[0]["OvertimeHolidayYTD"].ToString());
                    Total2.Text = totalsummytd.ToString("N2").GetDashWhenZero();
                    Total4.Text = SumI.ToString("N2").GetDashWhenZero();
                    sumYTDTAX = itytd + decimal.Parse(adtytdd.ToString());
                    minusValue = totalYTDminus - totalMedicalYTD;

                    if (RemainingMonths != 0)
                    {
                        CalSum = sumTaxableAmount2 * RemainingMonths;
                    }
                    else if (RemainingMonths == 0)
                    {
                        CalSum = sumTaxableAmount2;
                    }

                    AnnualTaxableIncome2 = minusValue + CalSum;
                    AnnualTaxableIncome2 = Math.Round(AnnualTaxableIncome2, 2);

                    AnnualTaxableIncome = double.Parse(dt3.Rows[0]["TotalSalry2"].ToString()) + TotalSalary2;
                    AnnualTaxableIncome = Math.Round(AnnualTaxableIncome, 2);
                    AnnualTaxInc.Text = AnnualTaxableIncome2.ToString("N2").GetDashWhenZero();
                    double itp = Convert.ToDouble(dt3.Rows[0]["IncomeTaxYTD"].ToString());
                    IncomeTaxPaid.Text = sumYTDTAX.ToString("N2").GetDashWhenZero();
                }

                objDB.PayAmount = float.Parse(AnnualTaxableIncome.ToString());
                objDB.PayAmount = float.Parse(AnnualTaxableIncome2.ToString());
                objDB.PayrollDate = date;
                DataTable dt4 = objDB.GetAnnualTaxableIncomePayslips(ref errorMsg);
                if (dt4 != null && dt4.Rows.Count > 0)
                {
                    double TITL = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
                    TotalIncomeTaxLia.Text = TITL.ToString("N2").GetDashWhenZero();
                    sumYTDAddTax = decimal.Parse(TITL.ToString()) - sumYTDTAX;
                }

                double incometaxLia = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
                double reamainingIncomeTax = incometaxLia - double.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString());

                RemIncomeTaxPayable.Text = sumYTDAddTax.ToString("N2").GetDashWhenZero();

                DataTable dt5 = objDB.GetPaySlipPFKTByEmployeeID(ref errorMsg);
                if (dt5 != null && dt5.Rows.Count > 0)
                {
                    EmployeePF.Text = dt5.Rows[0]["OpeningPF_Employee"].ToString();
                    EmployeerPF.Text = dt5.Rows[0]["OpeningPF_Company"].ToString();
                    Label47.Text = dt5.Rows[0]["CurrentPF_Employee"].ToString();
                    Label48.Text = dt5.Rows[0]["closingEmployee"].ToString();
                    Label49.Text = dt5.Rows[0]["CurrentPF_Company"].ToString();
                    Label50.Text = dt5.Rows[0]["ClosingEmployeer"].ToString();
                    Label51.Text = dt5.Rows[0]["sumcurrent"].ToString();
                    Label52.Text = dt5.Rows[0]["totalsum2"].ToString();
                    TotalPF.Text = dt5.Rows[0]["SumOpening"].ToString();
                }

                divAlertMsg.Visible = false;
                ReportPayroll.Visible = true;
                ReportPayrollFotter.Visible = true;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                ReportPayroll.Visible = false;
                ReportPayrollFotter.Visible = false;
            }
        }

        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " Million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string content = "";
                string header = "";
                string footer = "";
                content = printPaySlip();

                Common.generatePDF(header, footer, content, "Pay Slip-" + PersonName + " (" + txtPayrollDate.Text + ")", "A4", "Portrait", 30, 30);

            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private string printPaySlip()
        {
            string content = "";

            try
            {
                string footer = "";
                string header = "";
                decimal totalYTDminus = 0;
                decimal sumTaxableAmount2 = 0;
                decimal AnnualTaxableIncome2 = 0;
                decimal minusValue = 0;
                decimal CalSum = 0;
                decimal sumYTDTAX = 0;
                decimal sumYTDAddTax = 0;
                double TITL = 0;

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);//Convert.ToInt32(Session["EmployeeID"].ToString());
                objDB.PayrollDate = txtPayrollDate.Text;
                objDB.DocType = "Pay Slip";

                dt = objDB.GetDocumentDesign(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                DataTable dtEmp = new DataTable();
                dtEmp = objDB.GetEmployeeByID(ref errorMsg);

                if (dtEmp != null && dtEmp.Rows.Count > 0)
                {
                    content = content.Replace("##COMPANY_NAME##", Session["CompanyName"].ToString());
                    content = content.Replace("##COMPANY_LOGO##", Session["CompanyLogo"].ToString());
                    content = content.Replace("##PAYROLL_MONTH##", txtPayrollDate.Text);
                    content = content.Replace("##LOCATION##", dtEmp.Rows[0]["Placeofpost"].ToString());
                    content = content.Replace("##EMPLOYEE_CODE##", dtEmp.Rows[0]["EmployeeCode"].ToString());
                    content = content.Replace("##DESIGNATION##", dtEmp.Rows[0]["DesgTitle"].ToString());
                    content = content.Replace("##DATE_OF_JOINING##", dtEmp.Rows[0]["DateOfJoining"].ToString() == "" ? "" : Convert.ToDateTime(dtEmp.Rows[0]["DateOfJoining"]).ToString("dd-MM-yyyy"));
                    content = content.Replace("##DATE_OF_BIRTH##", dtEmp.Rows[0]["DOB"].ToString() == "" ? "" : Convert.ToDateTime(dtEmp.Rows[0]["DOB"]).ToString("dd-MM-yyyy"));
                    content = content.Replace("##ACCOUNT_NO##", dtEmp.Rows[0]["AccountNo"].ToString());
                    content = content.Replace("EMPLOYEE_NAME", dtEmp.Rows[0]["EmployeeName"].ToString());
                    PersonName = dtEmp.Rows[0]["EmployeeName"].ToString();
                    content = content.Replace("##DEPARTMENT##", dtEmp.Rows[0]["DeptName"].ToString());
                    content = content.Replace("##EMPLOYEE_TYPE##", dtEmp.Rows[0]["EmploymentType"].ToString());
                    content = content.Replace("##CNIC##", dtEmp.Rows[0]["CNIC"].ToString());
                    content = content.Replace("##BANK_NAME##", dtEmp.Rows[0]["BankName"].ToString());

                    string st = "";
                    DataTable dt2 = objDB.GetSumUpPaySlipKTByEmployeeID(ref errorMsg);

                    double total, total2, NetAmount;
                    content = content.Replace("##BASIC_SALARY##", Convert.ToDecimal(dt2.Rows[0]["EmployeeSalary"]).ToString("N2"));
                    string basicSalary = dt2.Rows[0]["BasicSalary2"].ToString();
                    string SalesComissions = dt2.Rows[0]["Commission2"].ToString();
                    string houseRents = dt2.Rows[0]["HouseRent2"].ToString();
                    string UtilityAllows = dt2.Rows[0]["UtilityAllowence2"].ToString();
                    string mdeicalAllowance = dt2.Rows[0]["MedicalAllowence2"].ToString();
                    string ExpenseReims = dt2.Rows[0]["OtherExpenses2"].ToString();
                    string EidBonuss = dt2.Rows[0]["EidBonus2"].ToString();
                    string OtherBonuss = dt2.Rows[0]["OtherBonuses2"].ToString();
                    string Spiffss = dt2.Rows[0]["Spiffs2"].ToString();
                    string Severance = dt2.Rows[0]["Severance2"].ToString();
                    string LeaveEncash = dt2.Rows[0]["LeaveEncashment2"].ToString();
                    string Arrears = dt2.Rows[0]["Arrears2"].ToString();
                    string Overtimes = dt2.Rows[0]["OverTimeGeneralAmount2"].ToString();
                    string OvertimeHolidayss = dt2.Rows[0]["OvertimeHolidayAmount2"].ToString();
                    string IncomeTaxs = dt2.Rows[0]["Tax2"].ToString();
                    string TaxCredit = dt2.Rows[0]["TaxCredit2"].ToString();
                    string AddIncomeTaxs = dt2.Rows[0]["AdditionalTax2"].ToString();
                    string EOBIs = dt2.Rows[0]["EOBI2"].ToString();
                    string AbsentAmount = dt2.Rows[0]["AbsentAmount2"].ToString();
                    string OtherAllowances = dt2.Rows[0]["Other_Allowances"].ToString();
                    string PFNew = dt2.Rows[0]["PF2"].ToString();
                    string OtherDeduction = dt2.Rows[0]["OtherDeductions2"].ToString();
                    string LoanDeduct = dt2.Rows[0]["LoanDeduct"].ToString();
                    string LoanOrAdvance = dt2.Rows[0]["LoanOrAdvance"].ToString();
                    string TotSal2 = dt2.Rows[0]["GrossSalary2"].ToString();
                    double totalsumm = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(LeaveEncash) + double.Parse(Arrears) + double.Parse(Severance) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss) + double.Parse(EidBonuss) + double.Parse(OtherBonuss);
                    TotSal2 = totalsumm.ToString();
                    double totalgs = double.Parse(basicSalary) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows);
                    decimal tts = decimal.Parse(totalgs.ToString());
                    tts = decimal.Round(tts, 2, MidpointRounding.AwayFromZero);
                    sumTaxableAmount2 = decimal.Parse(basicSalary) + decimal.Parse(houseRents) + decimal.Parse(UtilityAllows);
                    total = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(LoanOrAdvance) + double.Parse(TaxCredit) + double.Parse(ExpenseReims) + double.Parse(LeaveEncash) + double.Parse(Arrears) + double.Parse(Severance) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss) + double.Parse(EidBonuss) + double.Parse(OtherBonuss); ;
                    total2 = double.Parse(IncomeTaxs) + double.Parse(EOBIs) + double.Parse(PFNew) + double.Parse(AbsentAmount) + double.Parse(OtherDeduction) + double.Parse(LoanDeduct) + double.Parse(AddIncomeTaxs);
                    NetAmount = total - total2;

                    decimal bs = decimal.Parse(basicSalary.ToString()).RoundDecimalTwoDigit();
                    decimal ttt = decimal.Parse(TotSal2.ToString()).RoundDecimalTwoDigit();
                    decimal sc = decimal.Parse(SalesComissions.ToString()).RoundDecimalTwoDigit();
                    decimal hr = decimal.Parse(houseRents.ToString()).RoundDecimalTwoDigit();
                    decimal ua = decimal.Parse(UtilityAllows.ToString()).RoundDecimalTwoDigit();
                    decimal er = decimal.Parse(ExpenseReims.ToString()).RoundDecimalTwoDigit();
                    decimal eb = decimal.Parse(EidBonuss.ToString()).RoundDecimalTwoDigit();
                    decimal ob = decimal.Parse(OtherBonuss.ToString()).RoundDecimalTwoDigit();
                    decimal tc = decimal.Parse(TaxCredit.ToString()).RoundDecimalTwoDigit();
                    decimal sp = decimal.Parse(Spiffss.ToString()).RoundDecimalTwoDigit();
                    decimal sv = decimal.Parse(Severance.ToString()).RoundDecimalTwoDigit();
                    decimal en = decimal.Parse(LeaveEncash.ToString()).RoundDecimalTwoDigit();
                    decimal arr = decimal.Parse(Arrears.ToString()).RoundDecimalTwoDigit();
                    decimal ot = decimal.Parse(Overtimes.ToString()).RoundDecimalTwoDigit();
                    decimal oth = decimal.Parse(OvertimeHolidayss.ToString()).RoundDecimalTwoDigit();
                    decimal it = decimal.Parse(IncomeTaxs.ToString()).RoundDecimalTwoDigit();
                    decimal ait = decimal.Parse(AddIncomeTaxs.ToString()).RoundDecimalTwoDigit();
                    decimal eobi = decimal.Parse(EOBIs.ToString()).RoundDecimalTwoDigit();
                    decimal ma = decimal.Parse(mdeicalAllowance.ToString()).RoundDecimalTwoDigit();
                    decimal ollow = decimal.Parse(OtherAllowances.ToString()).RoundDecimalTwoDigit();
                    decimal pf = decimal.Parse(PFNew.ToString()).RoundDecimalTwoDigit();
                    decimal odnew = decimal.Parse(OtherDeduction.ToString()).RoundDecimalTwoDigit();
                    decimal ld = decimal.Parse(LoanDeduct.ToString()).RoundDecimalTwoDigit();
                    decimal loanadvance = decimal.Parse(LoanOrAdvance.ToString()).RoundDecimalTwoDigit();
                    decimal ab = decimal.Parse(AbsentAmount.ToString()).RoundDecimalTwoDigit();
                    string date = "01-" + txtPayrollDate.Text;
                    objDB.PayrollDate = date;
                    DateTime dateTime12 = Convert.ToDateTime(date);
                    int months = int.Parse(dateTime12.ToString("MM"));
                    int RemainingMonths = 0;
                    double TotalSalary2 = 0;

                    RemainingMonths = months > 6 ? (18 - months) : 6 - months;

                    if (RemainingMonths != 0)
                    {
                        double subtract = double.Parse(dtEmp.Rows[0]["GrossAmount"].ToString()) - double.Parse(dtEmp.Rows[0]["MedicalAllowance"].ToString());
                        TotalSalary2 = RemainingMonths * subtract;
                    }

                    DataTable dt3 = objDB.GetPaySlipByEmployeeID_YTD(ref errorMsg);
                    double sumYTD = 0;
                    double SumI = 0;
                    double AnnualTaxableIncome = 0;

                    decimal bsytd = decimal.Parse(dt3.Rows[0]["BasicSalaryYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal scytd = decimal.Parse(dt3.Rows[0]["CommissionYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal HRYTD = decimal.Parse(dt3.Rows[0]["HouseRentYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal MAYTD = decimal.Parse(dt3.Rows[0]["MedicalAllowenceYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal UAYTD = decimal.Parse(dt3.Rows[0]["UtilityAllowenceYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal OEYTD = decimal.Parse(dt3.Rows[0]["OtherExpensesYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal ebytd = decimal.Parse(dt3.Rows[0]["EidBonusYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal obytd = decimal.Parse(dt3.Rows[0]["OtherBonusesYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal sytd = decimal.Parse(dt3.Rows[0]["SpiffsYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal enytd = decimal.Parse(dt3.Rows[0]["LeaveEncashmentYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal svytd = decimal.Parse(dt3.Rows[0]["SeveranceYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal arrytd = decimal.Parse(dt3.Rows[0]["ArrearsYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal otytd = decimal.Parse(dt3.Rows[0]["OverTimeYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal ebhytd = decimal.Parse(dt3.Rows[0]["OvertimeHolidayYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal itytd = decimal.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal eobiytd = decimal.Parse(dt3.Rows[0]["EobiYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal overtimetoalytd = decimal.Parse(dt3.Rows[0]["OverTimeTotalAmountsYTD"].ToString());
                    decimal otytds = decimal.Parse(dt3.Rows[0]["Other_AllowancesYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal pfytd = decimal.Parse(dt3.Rows[0]["PFYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal odytd = decimal.Parse(dt3.Rows[0]["OtherDeductionsYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal ldytd = decimal.Parse(dt3.Rows[0]["LoanDeductYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal loanadvanceytd = decimal.Parse(dt3.Rows[0]["LoanorAdvanceYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal abytd = decimal.Parse(dt3.Rows[0]["AbsentAmountYTD"].ToString()).RoundDecimalTwoDigit();
                    decimal totalgsYTD = bsytd + HRYTD + MAYTD + UAYTD;
                    decimal TOTYTD = decimal.Parse(dt3.Rows[0]["TotalSalry2"].ToString()).RoundDecimalTwoDigit();
                    decimal sumYTDTaxCredit = decimal.Parse(dt3.Rows[0]["TaxCreditYTD"].ToString()).RoundDecimalTwoDigit();
                    double totalsummytd = double.Parse(bsytd.ToString())
                        + double.Parse(otytds.ToString())
                        + double.Parse(scytd.ToString())
                        + double.Parse(HRYTD.ToString())
                        + double.Parse(MAYTD.ToString())
                        + double.Parse(UAYTD.ToString())
                        + double.Parse(OEYTD.ToString())
                        + double.Parse(sytd.ToString())
                        + double.Parse(svytd.ToString())
                         + double.Parse(enytd.ToString())
                        + double.Parse(arrytd.ToString())
                        + double.Parse(ebytd.ToString())
                         + double.Parse(obytd.ToString())
                        + double.Parse(overtimetoalytd.ToString());

                    totalYTDminus = decimal.Parse(totalsummytd.ToString());

                    string TotSal2ytds = totalsummytd.ToString();
                    double adtytdd = Convert.ToDouble(dt3.Rows[0]["AddIncomeTaxYTD"].ToString());

                    SumI = double.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString()) + double.Parse(dt3.Rows[0]["EobiYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherDeductionsYTD"].ToString()) + double.Parse(dt3.Rows[0]["PFYTD"].ToString()) + double.Parse(dt3.Rows[0]["AddIncomeTaxYTD"].ToString());
                    sumYTD = double.Parse(dt3.Rows[0]["BasicSalaryYTD"].ToString()) + double.Parse(dt3.Rows[0]["Other_AllowancesYTD"].ToString()) + double.Parse(dt3.Rows[0]["CommissionYTD"].ToString()) + double.Parse(dt3.Rows[0]["HouseRentYTD"].ToString()) + double.Parse(dt3.Rows[0]["MedicalAllowenceYTD"].ToString()) + double.Parse(dt3.Rows[0]["UtilityAllowenceYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherExpensesYTD"].ToString()) + double.Parse(dt3.Rows[0]["EidBonusYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherBonusesYTD"].ToString()) + double.Parse(dt3.Rows[0]["LeaveEncashmentYTD"].ToString()) + double.Parse(dt3.Rows[0]["SeveranceYTD"].ToString()) + double.Parse(dt3.Rows[0]["ArrearsYTD"].ToString()) + double.Parse(dt3.Rows[0]["SpiffsYTD"].ToString()) + double.Parse(dt3.Rows[0]["OverTimeYTD"].ToString()) + double.Parse(dt3.Rows[0]["OvertimeHolidayYTD"].ToString());

                    sumYTDTAX = itytd + decimal.Parse(adtytdd.ToString());
                    minusValue = totalYTDminus - MAYTD;
                    CalSum = RemainingMonths == 0 ? sumTaxableAmount2 : sumTaxableAmount2 * RemainingMonths;
                    AnnualTaxableIncome2 = minusValue + CalSum;
                    AnnualTaxableIncome2 = Math.Round(AnnualTaxableIncome2, 2);

                    AnnualTaxableIncome = double.Parse(dt3.Rows[0]["TotalSalry2"].ToString()) + TotalSalary2;
                    AnnualTaxableIncome = Math.Round(AnnualTaxableIncome, 2);
                    double itp = Convert.ToDouble(dt3.Rows[0]["IncomeTaxYTD"].ToString());

                    objDB.PayAmount = float.Parse(AnnualTaxableIncome.ToString());
                    objDB.PayAmount = float.Parse(AnnualTaxableIncome2.ToString());
                    objDB.PayrollDate = date;

                    DataTable dt4 = objDB.GetAnnualTaxableIncomePayslips(ref errorMsg);

                    if (dt4 != null && dt4.Rows.Count > 0)
                    {
                        TITL = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
                        TotalIncomeTaxLia.Text = TITL.ToString("N2");
                        sumYTDAddTax = decimal.Parse(TITL.ToString()) - sumYTDTAX;
                    }

                    double incometaxLia = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
                    double reamainingIncomeTax = incometaxLia - double.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString());
                    DataTable dt5 = objDB.GetPaySlipPFKTByEmployeeID(ref errorMsg);

                    RemIncomeTaxPayable.Text = sumYTDAddTax.ToString("N2");
                    EmployeePF.Text = dt5.Rows[0]["OpeningPF_Employee"].ToString();
                    EmployeerPF.Text = dt5.Rows[0]["OpeningPF_Company"].ToString();
                    Label47.Text = dt5.Rows[0]["CurrentPF_Employee"].ToString();
                    Label48.Text = dt5.Rows[0]["closingEmployee"].ToString();
                    Label49.Text = dt5.Rows[0]["CurrentPF_Company"].ToString();
                    Label50.Text = dt5.Rows[0]["ClosingEmployeer"].ToString();
                    Label51.Text = dt5.Rows[0]["sumcurrent"].ToString();
                    Label52.Text = dt5.Rows[0]["totalsum2"].ToString();
                    TotalPF.Text = dt5.Rows[0]["SumOpening"].ToString();

                    divAlertMsg.Visible = false;

                    #region Allowances Section

                    st += @"  <table style='width: 100%;'>
          <tbody>
            <tr style='border - bottom: 2px solid #000;'>
              <td>Allowance</td>
              <td style='text-align:center;'>Amount</td>
              <td style='text-align:center;'>YTD</td>
            </tr>
            <tr>
              <td><span id=''>BasicSalary</span></td>
              <td style = 'text-align: right;'><span id=''>" + bs.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + bsytd.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td><span id=''>House Rent Allowance</span></td>
              <td style = 'text-align: right;'><span id=''>" + hr.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + HRYTD.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td><span id=''>Medical Allowance</span></td>
              <td style = 'text-align: right;'><span id=''>" + ma.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + MAYTD.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td><span id=''>Utility Allowance</span></td>
              <td style = 'text-align: right;'><span id=''>" + ua.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + UAYTD.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td ><b><span id=''>Total </span></b></td>
              <td style='border-top: 1px solid; border-bottom: 1px solid; text-align: right;'><b><span id=''>" + tts.ToString("N2") + @"</span></b></td>
              <td style='border-top: 1px solid; border-bottom: 1px solid; text-align: right;'><b><span id=''>" + totalgsYTD.ToString("N2") + @"</span></b></td>
            </tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
            <tr>
              <td><span id=''>Sales Commission</span></td>
              <td style = 'text-align: right;'><span id=''>" + sc.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + scytd.ToString("N2") + @"</span></td>
            </tr>
            
            <tr>
              <td><span id=''>Expense Reimbursement</span></td>
              <td style = 'text-align: right;'><span id=''>" + er.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + OEYTD.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td><span id=''>Eid Bonus</span></td>
              <td style = 'text-align: right;'><span id=''>" + eb.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + ebytd.ToString("N2") + @"</span></td>
            </tr>
   <tr>
              <td><span id=''>Other Bonuses</span></td>
              <td style = 'text-align: right;'><span id=''>" + ob.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + obytd.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td><span id=''>Spiffs</span></td>
              <td style = 'text-align: right;'><span id=''>" + sp.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + sytd.ToString("N2") + @"</span></td>
            </tr>

 <tr>
              <td><span id=''>Arrears</span></td>
              <td style = 'text-align: right;'><span id=''>" + arr.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + arrytd.ToString("N2") + @"</span></td>
            </tr>
<tr>
              <td><span id=''>Leave Encashment</span></td>
              <td style = 'text-align: right;'><span id=''>" + en.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + enytd.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td><span id=''>Overtime Hours - General</span></td>
              <td style = 'text-align: right;'><span id=''>" + ot.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + otytd.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td><span id=''>Overtime Holidays</span></td>
              <td style = 'text-align: right;'><span id=''>" + oth.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + ebhytd.ToString("N2") + @"</span></td>
            </tr>
               <tr>
              <td><span id=''>Tax Credit</span></td>
              <td style = 'text-align: right;'><span id=''>" + tc.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + sumYTDTaxCredit.ToString("N2") + @"</span></td>
            </tr>
<tr>
              <td><span id=''>Loan/Advance</span></td>
              <td style = 'text-align: right;'><span id=''>"+ loanadvance.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>"+ loanadvanceytd.ToString("N2") + @" </span></td>
            </tr>
        </tbody>
        </table>";

                    content = content.Replace("##ALLOWANCE_TABLE##", st);
                    #endregion

                    #region Allowances' Total Section
                    st = "";
                    st += @"
                      <table class='slip-table slip-table-bordered' style='width: 100%; margin-top: 0px;'>
                        <tbody>
                        <tr>
                          <td style='width: 58%;'><b><span id=''>Total</span></b></td>
                            <td  style='text-align: left;'><b><span id=''>" + ttt.ToString("N2") + @"</span></b></td>
                          <td><b><span id=''>" + totalsummytd.ToString("N2") + @"</span></b></td>
                        </tr>
                    </tbody>
                    </table>";

                    content = content.Replace("##ALLOWANCE_TOTAL##", st);
                    #endregion

                    #region Deduction Section
                    st = "";
                    st += @"
    <table style='width: 100%; padding-bottom: 6.2px;'>
          <tbody>
            <tr style='border-bottom: 2px solid #000;'>
              <td scope='col'>Deductions</td>
              <td scope='col' style='text-align:center;'>Amount</td>
              <td scope='col' style='text-align:center;'>YTD</td>
            </tr>
                <tr>
              <td><span id=''>Income Tax</span></td>
              <td style = 'text-align: right;'><span id=''>" + it.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + itytd.ToString("N2") + @"</span></td>
            </tr>
             <tr>
                  <td><span id=''>Additional Income Tax</span></td>
                  <td style = 'text-align: right;'><span id=''>" + ait.ToString("N2") + @"</span></td>
                  <td style = 'text-align: right;'><span id=''>" + adtytdd.ToString("N2") + @"</span></td>
                </tr>
                 <tr>
              <td><span id=''>EOBI Deduction</span></td>
              <td style = 'text-align: right;'><span id=''>" + eobi.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + eobiytd.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td><span id=''>Employee PF</span></td>
              <td style = 'text-align: right;'><span id=''>" + pf.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + pfytd.ToString("N2") + @"</span></td>
            </tr>
  <tr>
              <td><span id=''>Absent</span></td>
              <td style = 'text-align: right;'><span id=''>" + ab.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + abytd.ToString("N2") + @"</span></td>
            </tr>
<tr>
              <td><span id=''>Loan/Advance Installment</span></td>
              <td style = 'text-align: right;'><span id=''>" + ld.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + ldytd.ToString("N2") + @"</span></td>
            </tr>
             <tr>
              <td><span id=''>Others</span></td>
              <td style = 'text-align: right;'><span id=''>" + odnew.ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + odytd.ToString("N2") + @"</span></td>
            </tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr>
			<tr>
				<td></td><td></td><td></td>
			</tr><tr>
				<td></td><td></td><td></td>
			</tr>
<tr>
				<td></td><td></td><td></td>
			</tr><tr>
				<td></td><td></td><td></td>
			</tr>
<tr>
				<td></td><td></td><td></td>
			</tr>
<tr>
				<td></td><td></td><td></td>
			</tr>
        </tbody>          
        </table>";

                    content = content.Replace("##DEDUCTION_TABLE##", st);
                    #endregion

                    #region Deductions and Net Total Section
                    st = "";
                    st += @"
                      <table class='slip-table slip-table-bordered' style='width: 100%; margin-top: 0px;'>
                        <tbody>
                        <tr>
                          <td style='width: 58%;'><b><span id=''>Total</span></b></td>
                          <td><b><span id=''>" + total2.ToString("N2") + @"</span></b></td>
                          <td><b><span id=''>" + SumI.ToString("N2") + @"</span></b></td>
                        </tr>
                    </tbody>
                    </table>";

                    content = content.Replace("##DEDUCTION_TOTAL##", st);
                    content = content.Replace("##NET_AMOUNT##", (total - total2).ToString("N2"));
                    content = content.Replace("##TOTAL_AMOUNT_IN_WORDS##", Common.NumberToWords((total - total2)));
                    #endregion

                    #region Tax Details Section
                    st = "";
                    st += @"
        <table style='margin-top: -1px; width: 100%;'>
          <tbody>
            <tr>
              <td style='font-weight: bold;'><span id=''>Annual Taxable Income</span></td>
              <td style='text-align: right;'><span id=''>" + AnnualTaxableIncome2.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td style='font-weight: bold;'><span id=''>Total Income Tax Liability</span></td>
              <td style='text-align: right;'><span id=''>" + TITL.ToString("N2") + @"</span></td>
              
            </tr>
            <tr>
              <td style='font-weight: bold;'><span id=''>Income Tax Paid</span></td>
              <td style='text-align: right;'><span id=''>" + (sumYTDTAX - sumYTDTaxCredit).ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td style='font-weight: bold;'><span id=''>Advance Tax Adjustment</span></td>
              <td style='text-align: right;'><span id=''>-</span></td>
            </tr>
            <tr>
              <td style='font-weight: bold;'><span id=''>Tax Credit</span></td>
              <td style='text-align: right;'><span id=''>" + sumYTDTaxCredit.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td style='font-weight: bold;'><span id=''>Remaining Income Tax Payable</span></td>
              <td style='text-align: right;'><span id=''>" + sumYTDAddTax.ToString("N2") + @"</span></td>
            </tr> 
        </tbody>
        </table>";

                    content = content.Replace("##TAX_TABLE##", st);
                    #endregion



                    #region Loan Advance Details Section
                    st = "";
                    st += @"
        <table style='margin-top: -1px; width: 100%;'>
          <tbody>
            <tr>
              <td style='font-weight: bold;'><span id=''>Loan/Advance</span></td>
              <td style='text-align: right;'><span id=''>" + loanadvanceytd.ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td style='font-weight: bold;'><span id=''>Deduction Installment</span></td>
              <td style='text-align: right;'><span id=''>" + ldytd.ToString("N2") + @"</span></td>
              
            </tr>
            <tr>
              <td style='font-weight: bold;'><span id=''>Remaining Installment</span></td>
              <td style='text-align: right;'><span id=''>" + Math.Abs(loanadvanceytd- ldytd).ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
            </tr>
           <tr>
              <td></td>
              <td></td>
            </tr>
             <tr>
              <td></td>
              <td></td>
            </tr>
			 <tr>
              <td></td>
              <td></td>
            </tr> <tr>
              <td></td>
              <td></td>
            </tr> <tr>
              <td></td>
              <td></td>
            </tr> <tr>
              <td></td>
              <td></td>
            </tr> <tr>
              <td></td>
              <td></td>
            </tr> <tr>
              <td></td>
              <td></td>
            </tr>
<tr>
              <td></td>
              <td></td>
            </tr>
        </tbody>
        </table>";

                    content = content.Replace("##LOAN_TABLE##", st);
                    #endregion



                    #region PF Section
                    st = "";
                    total = 0;
                    st += @"
         <table style='width: 100%; border-right: 1px solid; border-top:none; margin-top: -29px;' id='tblPDF'>
          <tfoot>
            <tr>
            <td style='font-weight: bold; border-left: none;'></td>
            <td style = 'text-align:center;font-weight:bold;'> Opening </td>
            <td style = 'text-align:center;font-weight:bold;'> Current Month </td>
            <td style = 'text-align:center;font-weight:bold;'> Closing </td>
            </tr>                
            <tr>
              <td style = 'font-weight: bold;border-left: none;'><span id=''>Employee Contribution</span></td>
              <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["OpeningPF_Employee"]).ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["CurrentPF_Employee"]).ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["closingEmployee"]).ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td style = 'font-weight: bold; border-left: none;'><span id=''>Employer Contribution</span></td>
              <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["OpeningPF_Company"]).ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["CurrentPF_Company"]).ToString("N2") + @"</span></td>
              <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["ClosingEmployeer"]).ToString("N2") + @"</span></td>
            </tr>
            <tr>
              <td style = 'font-weight: bold; border-left: none; border-bottom: none;'><span id=''>Total</span></td>
              <td style = 'text-align: right; border-bottom: none;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["SumOpening"]).ToString("N2") + @"</span></td>
              <td style = 'text-align: right; border-bottom: none;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["sumcurrent"]).ToString("N2") + @"</span></td>
              <td style = 'text-align: right; border-bottom: none;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["totalsum2"]).ToString("N2") + @"</span></td>
            </tr>            
          </tfoot>
        </table>";

                    content = content.Replace("##PF_TABLE##", st);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                ReportPayroll.Visible = false;
                ReportPayrollFotter.Visible = false;
            }

            return content;
        }

        //     private string printPaySlip()
        //     {
        //         string content = "";

        //         try
        //         {
        //             string footer = "";
        //             string header = "";
        //             decimal totalYTDminus = 0;
        //             decimal sumTaxableAmount2 = 0;
        //             decimal AnnualTaxableIncome2 = 0;
        //             decimal minusValue = 0;
        //             decimal CalSum = 0;
        //             decimal sumYTDTAX = 0;
        //             decimal sumYTDAddTax = 0;
        //             double TITL = 0;

        //             DataTable dt = new DataTable();
        //             objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //             objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);//Convert.ToInt32(Session["EmployeeID"].ToString());
        //             objDB.PayrollDate = txtPayrollDate.Text;
        //             objDB.DocType = "Pay Slip";

        //             dt = objDB.GetDocumentDesign(ref errorMsg);

        //             if (dt != null)
        //             {
        //                 if (dt.Rows.Count > 0)
        //                 {
        //                     header = dt.Rows[0]["DocHeader"].ToString();
        //                     content = dt.Rows[0]["DocContent"].ToString();
        //                     footer = dt.Rows[0]["DocFooter"].ToString();
        //                 }
        //             }

        //             DataTable dtEmp = new DataTable();
        //             dtEmp = objDB.GetEmployeeByID(ref errorMsg);

        //             if (dtEmp != null && dtEmp.Rows.Count > 0)
        //             {
        //                 content = content.Replace("##COMPANY_NAME##", Session["CompanyName"].ToString()); 
        //                 content = content.Replace("##COMPANY_LOGO##", Session["CompanyLogo"].ToString());
        //                 content = content.Replace("##PAYROLL_MONTH##", txtPayrollDate.Text);
        //                 content = content.Replace("##LOCATION##", dtEmp.Rows[0]["Placeofpost"].ToString());
        //                 content = content.Replace("##EMPLOYEE_CODE##", dtEmp.Rows[0]["EmployeeCode"].ToString());
        //                 content = content.Replace("##DESIGNATION##", dtEmp.Rows[0]["DesgTitle"].ToString());
        //                 content = content.Replace("##DATE_OF_JOINING##", Convert.ToDateTime(dtEmp.Rows[0]["DateOfJoining"]).ToString("dd-MM-yyyy"));
        //                 content = content.Replace("##DATE_OF_BIRTH##", Convert.ToDateTime(dtEmp.Rows[0]["DOB"]).ToString("dd-MM-yyyy"));
        //                 content = content.Replace("##ACCOUNT_NO##", dtEmp.Rows[0]["AccountNo"].ToString());
        //                 content = content.Replace("EMPLOYEE_NAME", dtEmp.Rows[0]["EmployeeName"].ToString());
        //                 content = content.Replace("##DEPARTMENT##", dtEmp.Rows[0]["DeptName"].ToString());
        //                 content = content.Replace("##EMPLOYEE_TYPE##", dtEmp.Rows[0]["EmploymentType"].ToString());
        //                 content = content.Replace("##CNIC##", dtEmp.Rows[0]["CNIC"].ToString());
        //                 content = content.Replace("##BANK_NAME##", dtEmp.Rows[0]["BankName"].ToString());

        //                 string st = "";
        //                 DataTable dt2 = objDB.GetPaySlipKTByEmployeeID(ref errorMsg);

        //                 double total, total2, NetAmount;
        //                 content = content.Replace("##BASIC_SALARY##", Convert.ToDecimal(dt2.Rows[0]["EmployeeSalary"]).ToString("N2"));
        //                 string basicSalary = dt2.Rows[0]["BasicSalary"].ToString();
        //                 string SalesComissions = dt2.Rows[0]["Commission"].ToString();
        //                 string houseRents = dt2.Rows[0]["HouseRent"].ToString();
        //                 string UtilityAllows = dt2.Rows[0]["UtilityAllowence"].ToString();
        //                 string mdeicalAllowance = dt2.Rows[0]["MedicalAllowence"].ToString();
        //                 string ExpenseReims = dt2.Rows[0]["OtherExpenses"].ToString();
        //                 string EidBonuss = dt2.Rows[0]["EidBonus"].ToString();
        //                 string Spiffss = dt2.Rows[0]["Spiffs"].ToString();
        //                 string Overtimes = dt2.Rows[0]["OverTimeGeneralAmount"].ToString();
        //                 string OvertimeHolidayss = dt2.Rows[0]["OvertimeHolidayAmount2"].ToString();
        //                 string IncomeTaxs = dt2.Rows[0]["Tax"].ToString();
        //                 string AddIncomeTaxs = dt2.Rows[0]["AdditionalTax"].ToString();
        //                 string EOBIs = dt2.Rows[0]["EOBI"].ToString();
        //                 string OtherAllowances = dt2.Rows[0]["Other_Allowances"].ToString();
        //                 string PFNew = dt2.Rows[0]["PF"].ToString();
        //                 string OtherDeduction = dt2.Rows[0]["OtherDeductions"].ToString();
        //                 string TotSal2 = dt2.Rows[0]["GrossSalary"].ToString();
        //                 double totalsumm = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss) + double.Parse(EidBonuss);
        //                 TotSal2 = totalsumm.ToString();
        //                 double totalgs = double.Parse(basicSalary) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows);
        //                 decimal tts = decimal.Parse(totalgs.ToString());
        //                 tts = decimal.Round(tts, 2, MidpointRounding.AwayFromZero);
        //                 sumTaxableAmount2 = decimal.Parse(basicSalary) + decimal.Parse(houseRents) + decimal.Parse(UtilityAllows);
        //                 total = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss);
        //                 total2 = double.Parse(IncomeTaxs) + double.Parse(EOBIs) + double.Parse(PFNew) + double.Parse(OtherDeduction) + double.Parse(AddIncomeTaxs);
        //                 NetAmount = total - total2;

        //                 decimal bs = decimal.Parse(basicSalary.ToString()).RoundDecimalTwoDigit();
        //                 decimal ttt = decimal.Parse(TotSal2.ToString()).RoundDecimalTwoDigit();
        //                 decimal sc = decimal.Parse(SalesComissions.ToString()).RoundDecimalTwoDigit();
        //                 decimal hr = decimal.Parse(houseRents.ToString()).RoundDecimalTwoDigit();
        //                 decimal ua = decimal.Parse(UtilityAllows.ToString()).RoundDecimalTwoDigit();
        //                 decimal er = decimal.Parse(ExpenseReims.ToString()).RoundDecimalTwoDigit();
        //                 decimal eb = decimal.Parse(EidBonuss.ToString()).RoundDecimalTwoDigit();
        //                 decimal sp = decimal.Parse(Spiffss.ToString()).RoundDecimalTwoDigit();
        //                 decimal ot = decimal.Parse(Overtimes.ToString()).RoundDecimalTwoDigit();
        //                 decimal oth = decimal.Parse(OvertimeHolidayss.ToString()).RoundDecimalTwoDigit();
        //                 decimal it = decimal.Parse(IncomeTaxs.ToString()).RoundDecimalTwoDigit();
        //                 decimal ait = decimal.Parse(AddIncomeTaxs.ToString()).RoundDecimalTwoDigit();
        //                 decimal eobi = decimal.Parse(EOBIs.ToString()).RoundDecimalTwoDigit();
        //                 decimal ma = decimal.Parse(mdeicalAllowance.ToString()).RoundDecimalTwoDigit();
        //                 decimal ollow = decimal.Parse(OtherAllowances.ToString()).RoundDecimalTwoDigit();
        //                 decimal pf = decimal.Parse(PFNew.ToString()).RoundDecimalTwoDigit();
        //                 decimal odnew = decimal.Parse(OtherDeduction.ToString()).RoundDecimalTwoDigit();

        //                 string date = "01-" + txtPayrollDate.Text;
        //                 objDB.PayrollDate = date;
        //                 DateTime dateTime12 = Convert.ToDateTime(date);
        //                 int months = int.Parse(dateTime12.ToString("MM"));
        //                 int RemainingMonths = 0;
        //                 double TotalSalary2 = 0;

        //                 RemainingMonths = months > 6 ? (18 - months) : 6 - months;

        //                 if (RemainingMonths != 0)
        //                 {
        //                     double subtract = double.Parse(dtEmp.Rows[0]["GrossAmount"].ToString()) - double.Parse(dtEmp.Rows[0]["MedicalAllowance"].ToString());
        //                     TotalSalary2 = RemainingMonths * subtract;
        //                 }

        //                 DataTable dt3 = objDB.GetPaySlipByEmployeeID_YTD(ref errorMsg);
        //                 double sumYTD = 0;
        //                 double SumI = 0;
        //                 double AnnualTaxableIncome = 0;

        //                 decimal bsytd = decimal.Parse(dt3.Rows[0]["BasicSalaryYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal scytd = decimal.Parse(dt3.Rows[0]["CommissionYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal HRYTD = decimal.Parse(dt3.Rows[0]["HouseRentYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal MAYTD = decimal.Parse(dt3.Rows[0]["MedicalAllowenceYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal UAYTD = decimal.Parse(dt3.Rows[0]["UtilityAllowenceYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal OEYTD = decimal.Parse(dt3.Rows[0]["OtherExpensesYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal ebytd = decimal.Parse(dt3.Rows[0]["EidBonusYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal sytd = decimal.Parse(dt3.Rows[0]["SpiffsYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal otytd = decimal.Parse(dt3.Rows[0]["OverTimeYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal ebhytd = decimal.Parse(dt3.Rows[0]["OvertimeHolidayYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal itytd = decimal.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal eobiytd = decimal.Parse(dt3.Rows[0]["EobiYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal overtimetoalytd = decimal.Parse(dt3.Rows[0]["OverTimeTotalAmountsYTD"].ToString());
        //                 decimal otytds = decimal.Parse(dt3.Rows[0]["Other_AllowancesYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal pfytd = decimal.Parse(dt3.Rows[0]["PFYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal odytd = decimal.Parse(dt3.Rows[0]["OtherDeductionsYTD"].ToString()).RoundDecimalTwoDigit();
        //                 decimal totalgsYTD = bsytd + HRYTD + MAYTD + UAYTD;
        //                 decimal TOTYTD = decimal.Parse(dt3.Rows[0]["TotalSalry2"].ToString()).RoundDecimalTwoDigit();

        //                 double totalsummytd = double.Parse(bsytd.ToString())
        //                     + double.Parse(otytds.ToString())
        //                     + double.Parse(scytd.ToString())
        //                     + double.Parse(HRYTD.ToString())
        //                     + double.Parse(MAYTD.ToString())
        //                     + double.Parse(UAYTD.ToString())
        //                     + double.Parse(OEYTD.ToString())
        //                     + double.Parse(sytd.ToString())
        //                     + double.Parse(ebytd.ToString())
        //                     + double.Parse(overtimetoalytd.ToString());

        //                 totalYTDminus = decimal.Parse(totalsummytd.ToString());

        //                 string TotSal2ytds = totalsummytd.ToString();
        //                 double adtytdd = Convert.ToDouble(dt3.Rows[0]["AddIncomeTaxYTD"].ToString());

        //                 SumI = double.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString()) + double.Parse(dt3.Rows[0]["EobiYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherDeductionsYTD"].ToString()) + double.Parse(dt3.Rows[0]["PFYTD"].ToString()) + double.Parse(dt3.Rows[0]["AddIncomeTaxYTD"].ToString());
        //                 sumYTD = double.Parse(dt3.Rows[0]["BasicSalaryYTD"].ToString()) + double.Parse(dt3.Rows[0]["Other_AllowancesYTD"].ToString()) + double.Parse(dt3.Rows[0]["CommissionYTD"].ToString()) + double.Parse(dt3.Rows[0]["HouseRentYTD"].ToString()) + double.Parse(dt3.Rows[0]["MedicalAllowenceYTD"].ToString()) + double.Parse(dt3.Rows[0]["UtilityAllowenceYTD"].ToString()) + double.Parse(dt3.Rows[0]["OtherExpensesYTD"].ToString()) + double.Parse(dt3.Rows[0]["EidBonusYTD"].ToString()) + double.Parse(dt3.Rows[0]["SpiffsYTD"].ToString()) + double.Parse(dt3.Rows[0]["OverTimeYTD"].ToString()) + double.Parse(dt3.Rows[0]["OvertimeHolidayYTD"].ToString());
        //                 sumYTDTAX = itytd + decimal.Parse(adtytdd.ToString());
        //                 minusValue = totalYTDminus - MAYTD;
        //                 CalSum = RemainingMonths == 0 ? sumTaxableAmount2 : sumTaxableAmount2 * RemainingMonths;
        //                 AnnualTaxableIncome2 = minusValue + CalSum;
        //                 AnnualTaxableIncome2 = Math.Round(AnnualTaxableIncome2, 2);

        //                 AnnualTaxableIncome = double.Parse(dt3.Rows[0]["TotalSalry2"].ToString()) + TotalSalary2;
        //                 AnnualTaxableIncome = Math.Round(AnnualTaxableIncome, 2);
        //                 double itp = Convert.ToDouble(dt3.Rows[0]["IncomeTaxYTD"].ToString());

        //                 objDB.PayAmount = float.Parse(AnnualTaxableIncome.ToString());
        //                 objDB.PayAmount = float.Parse(AnnualTaxableIncome2.ToString());
        //                 objDB.PayrollDate = date;

        //                 DataTable dt4 = objDB.GetAnnualTaxableIncomePayslips(ref errorMsg);

        //                 if (dt4 != null && dt4.Rows.Count > 0)
        //                 {
        //                     TITL = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
        //                     TotalIncomeTaxLia.Text = TITL.ToString("N2");
        //                     sumYTDAddTax = decimal.Parse(TITL.ToString()) - sumYTDTAX;
        //                 }

        //                 double incometaxLia = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
        //                 double reamainingIncomeTax = incometaxLia - double.Parse(dt3.Rows[0]["IncomeTaxYTD"].ToString());
        //                 DataTable dt5 = objDB.GetPaySlipPFKTByEmployeeID(ref errorMsg);

        //                 RemIncomeTaxPayable.Text = sumYTDAddTax.ToString("N2");
        //                 EmployeePF.Text = dt5.Rows[0]["OpeningPF_Employee"].ToString();
        //                 EmployeerPF.Text = dt5.Rows[0]["OpeningPF_Company"].ToString();
        //                 Label47.Text = dt5.Rows[0]["CurrentPF_Employee"].ToString();
        //                 Label48.Text = dt5.Rows[0]["closingEmployee"].ToString();
        //                 Label49.Text = dt5.Rows[0]["CurrentPF_Company"].ToString();
        //                 Label50.Text = dt5.Rows[0]["ClosingEmployeer"].ToString();
        //                 Label51.Text = dt5.Rows[0]["sumcurrent"].ToString();
        //                 Label52.Text = dt5.Rows[0]["totalsum2"].ToString();
        //                 TotalPF.Text = dt5.Rows[0]["SumOpening"].ToString();

        //                 divAlertMsg.Visible = false;

        //                 #region Allowances Section

        //                 st += @"  <table style='width: 100%;'>
        //       <tbody>
        //         <tr style='border - bottom: 2px solid #000;'>
        //           <td>Allowance</td>
        //           <td style='text-align:center;'>Amount</td>
        //           <td style='text-align:center;'>YTD</td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>BasicSalary</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + bs.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + bsytd.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>House Rent Allowance</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + hr.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + HRYTD.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>Medical Allowance</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + ma.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + MAYTD.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>Utility Allowance</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + ua.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + UAYTD.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td ><b><span id=''>Total </span></b></td>
        //           <td style='border-top: 1px solid; border-bottom: 1px solid; text-align: right;'><b><span id=''>" + tts.ToString("N2") + @"</span></b></td>
        //           <td style='border-top: 1px solid; border-bottom: 1px solid; text-align: right;'><b><span id=''>" + totalgsYTD.ToString("N2") + @"</span></b></td>
        //         </tr>
        //<tr>
        //	<td></td>
        //	<td></td>
        //	<td></td>
        //</tr>
        //         <tr>
        //           <td><span id=''>Sales Commission</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + sc.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + scytd.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>Other Allowance</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + ollow.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + otytds.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>Expense Reimbursement</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + er.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + OEYTD.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>Eid Bouns</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + eb.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + ebytd.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>Spiffs</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + sp.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + sytd.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>Overtime Hours - General</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + ot.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + otytd.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>Overtime Holidays</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + oth.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + ebhytd.ToString("N2") + @"</span></td>
        //         </tr>
        //     </tbody>
        //     </table>";

        //                 content = content.Replace("##ALLOWANCE_TABLE##", st);
        //                 #endregion

        //                 #region Allowances' Total Section
        //                 st = "";
        //                 st += @"
        //                   <table class='slip-table slip-table-bordered' style='width: 100%; margin-top: 0px;'>
        //                     <tbody>
        //                     <tr>
        //                       <td style='width: 58%;'><b><span id=''>Total</span></b></td>
        //                         <td  style='text-align: left;'><b><span id=''>" + ttt.ToString("N2") + @"</span></b></td>
        //                       <td><b><span id=''>" + totalsummytd.ToString("N2") + @"</span></b></td>
        //                     </tr>
        //                 </tbody>
        //                 </table>";

        //                 content = content.Replace("##ALLOWANCE_TOTAL##", st);
        //                 #endregion

        //                 #region Deduction Section
        //                 st = "";
        //                 st += @"
        // <table style='width: 100%; padding-bottom: 6.2px;'>
        //       <tbody>
        //         <tr style='border-bottom: 2px solid #000;'>
        //           <td scope='col'>Deductions</td>
        //           <td scope='col' style='text-align:center;'>Amount</td>
        //           <td scope='col' style='text-align:center;'>YTD</td>
        //         </tr>
        //             <tr>
        //           <td><span id=''>Income Tax</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + it.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + itytd.ToString("N2") + @"</span></td>
        //         </tr>
        //          <tr>
        //               <td><span id=''>Additional Income Tax</span></td>
        //               <td style = 'text-align: right;'><span id=''>" + ait.ToString("N2") + @"</span></td>
        //               <td style = 'text-align: right;'><span id=''>" + adtytdd.ToString("N2") + @"</span></td>
        //             </tr>
        //              <tr>
        //           <td><span id=''>EOBI Deduction</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + eobi.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + eobiytd.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td><span id=''>Employee PF</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + pf.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + pfytd.ToString("N2") + @"</span></td>
        //         </tr>
        //          <tr>
        //           <td><span id=''>Others</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + odytd.ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + odytd.ToString("N2") + @"</span></td>
        //         </tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //<tr>
        //	<td></td><td></td><td></td>
        //</tr>
        //     </tbody>          
        //     </table>";

        //                 content = content.Replace("##DEDUCTION_TABLE##", st);
        //                 #endregion

        //                 #region Deductions and Net Total Section
        //                 st = "";
        //                 st += @"
        //                   <table class='slip-table slip-table-bordered' style='width: 100%; margin-top: 0px;'>
        //                     <tbody>
        //                     <tr>
        //                       <td style='width: 58%;'><b><span id=''>Total</span></b></td>
        //                       <td><b><span id=''>" + total2.ToString("N2") + @"</span></b></td>
        //                       <td><b><span id=''>" + SumI.ToString("N2") + @"</span></b></td>
        //                     </tr>
        //                 </tbody>
        //                 </table>";

        //                 content = content.Replace("##DEDUCTION_TOTAL##", st);
        //                 content = content.Replace("##NET_AMOUNT##", (total - total2).ToString("N2"));
        //                 content = content.Replace("##TOTAL_AMOUNT_IN_WORDS##", Common.NumberToWords((total - total2)));
        //                 #endregion

        //                 #region Tax Details Section
        //                 st = "";
        //                 st += @"
        //     <table style='margin-top: -1px; width: 100%;'>
        //       <tbody>
        //         <tr>
        //           <td style='font-weight: bold;'><span id=''>Annual Taxable Income</span></td>
        //           <td style='text-align: right;'><span id=''>" + AnnualTaxableIncome2.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td style='font-weight: bold;'><span id=''>Total Income Tax Liability</span></td>
        //           <td style='text-align: right;'><span id=''>" + TITL.ToString("N2") + @"</span></td>

        //         </tr>
        //         <tr>
        //           <td style='font-weight: bold;'><span id=''>Income Tax Paid</span></td>
        //           <td style='text-align: right;'><span id=''>" + sumYTDTAX.ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td style='font-weight: bold;'><span id=''>Advance Tax Adjustment</span></td>
        //           <td style='text-align: right;'><span id=''>-</span></td>
        //         </tr>
        //         <tr>
        //           <td style='font-weight: bold;'><span id=''>Tax Credit</span></td>
        //           <td style='text-align: right;'><span id=''>-</span></td>
        //         </tr>
        //         <tr>
        //           <td style='font-weight: bold;'><span id=''>Remaining Income Tax Payable</span></td>
        //           <td style='text-align: right;'><span id=''>" + sumYTDAddTax.ToString("N2") + @"</span></td>
        //         </tr> 
        //     </tbody>
        //     </table>";

        //                 content = content.Replace("##TAX_TABLE##", st);
        //                 #endregion

        //                 #region PF Section
        //                 st = "";
        //                 total = 0;
        //                 st += @"
        //      <table style='width: 100%; border-right: 1px solid; border-top:none; margin-top: -29px;' id='tblPDF'>
        //       <tfoot>
        //         <tr>
        //         <td style='font-weight: bold; border-left: none;'></td>
        //         <td style = 'text-align:center;font-weight:bold;'> Opening </td>
        //         <td style = 'text-align:center;font-weight:bold;'> Current Month </td>
        //         <td style = 'text-align:center;font-weight:bold;'> Closing </td>
        //         </tr>                
        //         <tr>
        //           <td style = 'font-weight: bold;border-left: none;'><span id=''>Employee Contribution</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["OpeningPF_Employee"]).ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["CurrentPF_Employee"]).ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["closingEmployee"]).ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td style = 'font-weight: bold; border-left: none;'><span id=''>Employer Contribution</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["OpeningPF_Company"]).ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["CurrentPF_Company"]).ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["ClosingEmployeer"]).ToString("N2") + @"</span></td>
        //         </tr>
        //         <tr>
        //           <td style = 'font-weight: bold; border-left: none; border-bottom: none;'><span id=''>Total</span></td>
        //           <td style = 'text-align: right; border-bottom: none;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["SumOpening"]).ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right; border-bottom: none;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["sumcurrent"]).ToString("N2") + @"</span></td>
        //           <td style = 'text-align: right; border-bottom: none;'><span id=''>" + Convert.ToDecimal(dt5.Rows[0]["totalsum2"]).ToString("N2") + @"</span></td>
        //         </tr>            
        //       </tfoot>
        //     </table>";

        //                 content = content.Replace("##PF_TABLE##", st);
        //                 #endregion
        //             }
        //         }
        //         catch (Exception ex)
        //         {
        //             divAlertMsg.Visible = true;
        //             divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //             pAlertMsg.InnerHtml = ex.Message;
        //             ReportPayroll.Visible = false;
        //             ReportPayrollFotter.Visible = false;
        //         }

        //         return content;
        //     }

    }
}