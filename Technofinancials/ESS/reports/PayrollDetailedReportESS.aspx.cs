﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.reports
{
    public partial class PayrollDetailedReportESS : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
            {
                if (!Page.IsPostBack)
                {

                    CheckSessions();
                    divAlertMsg.Visible = false;

                }
            }
            private void CheckSessions()
            {
                if (Session["UserID"] == null && Session["EmployeeID"] == null)
                    Response.Redirect("/login");
            }




            protected void ddlYear_SelectedIndexChangedNew(object sender, EventArgs e)
            {
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }
                CheckSessions();
                DataTable dt = new DataTable();
               
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                    objDB.PayrollDate = txtPayrollDate.Text;
                    dt = objDB.GetPaySlipKTByEmployeeID(ref errorMsg);
                    gv.DataSource = dt;
                    gv.DataBind();
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            gv.UseAccessibleHeader = true;
                            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                        }
                    }
                    Common.addlog("ViewAll", "HR", "All NewPayroll Report Viewed", "NewPayroll");
                
            }
        }
    }