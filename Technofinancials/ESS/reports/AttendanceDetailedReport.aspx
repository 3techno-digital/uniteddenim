﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttendanceDetailedReport.aspx.cs" Inherits="Technofinancials.ESS.reports.AttendanceDetailedReport" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
     <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
                    padding: 5px!important;
    height: 38px!important;
    width: 37px!important;
    display: inline-block!important;
    text-align: center!important;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

                    div div#gv_wrapper::-webkit-scrollbar-thumb {
                        background-color: #003780;
                        border: 2px solid #003780;
                        border-radius: 10px;
                    }
                    div div#gv_wrapper::-webkit-scrollbar {
    height: 10px;
    background-color: #ffffff;
}
                    div div#gv_wrapper::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #FFF;
}
            @media only screen and (max-width: 992px) and (min-width: 768px) {
                div div#gv_wrapper {
                    overflow-x: scroll;
                    overflow-y: hidden;
                    padding-bottom: 5rem;
                }
            }
            @media only screen and (max-width: 1024px) and (min-width: 993px) {
                div div#gv_wrapper {
                    overflow-x: scroll;
                    overflow-y: hidden;
                    padding-bottom: 5rem;
                }
            }
            @media only screen and (max-width: 1280px) and (min-width: 800px) {
                div div#gv_wrapper {
                    overflow-x: scroll;
                    overflow-y: hidden;
                    padding-bottom: 5rem;
                }
            }
        </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">

                  <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Attendance Sheet Flagwise</h1>
                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <section class="app-content">
                   <div class="row">
                                                <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                                                 <div class="col-sm-6">
                                     <div class="form-group">
                                         <h4>From Date
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                         <asp:TextBox runat="server" CssClass="form-control" ID="txtFromDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                     </div>
                                 </div>
                              <div class="col-sm-6">
                                     <div class="form-group">
                                         <h4>To Date
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                         <asp:TextBox runat="server" CssClass="form-control" ID="txtToDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY"  />
                                     </div>
                                 </div>                             
                                                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee Name</h4>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>WDID</h4>
                                        <asp:TextBox ID="txtKTId" runat="server" class="form-control" data-plugin="select2">
                                        </asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>
                          <div class="col-lg-4 col-md-6 col-sm-12">
                              <div class="row">
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Filter<span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlEmployee" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <asp:DropDownList ID="ddlAttendanceTypes" runat="server" CssClass="form-control select2" data-plugin="select2">
                                             <asp:ListItem Text="ALL" Value="%%"></asp:ListItem>
                                            <asp:ListItem Text="Check In" Value ="Checkin"></asp:ListItem>
                                            <asp:ListItem Text="Check Out" Value ="Checkout"></asp:ListItem>
                                            <asp:ListItem Text="Present only" Value ="Present"></asp:ListItem>
                                            <asp:ListItem Text="Absent only" Value ="Absent"></asp:ListItem>
                                            <asp:ListItem Text="Break In - Break out" Value ="3"></asp:ListItem>
                                            <asp:ListItem Text="Off Days Only" Value ="Off"></asp:ListItem>
                                            <asp:ListItem Text="Leaves Only" Value ="Leave"></asp:ListItem>
                                            <asp:ListItem Text="Holidays Only" Value ="Holiday"></asp:ListItem>
                                            
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Location</h4>
                                        <asp:DropDownList ID="ddlLocation" runat="server" class="form-control select2 ">
                                        </asp:DropDownList>
                                    </div>
                              </div>
                                                                                                <div class="col-sm-6">
                              <div class="clear-fix">&nbsp;</div>
                                  <div>
                                      <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
                                  </div>
                              </div>

                              </div>
                          </div>

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="col-sm-12">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group" id="divAlertMsg" runat="server">
                                            <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                <span>
                                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                </span>
                                                <p id="pAlertMsg" runat="server">
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                                       <div class="clearfix">&nbsp;</div>

                    <div class="row ">
                      <div class="col-sm-12">
                            <div class="tab-content ">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                     
                                  <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Employee Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("WorkDayID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Employee") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                  <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Date") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Day">
                                                <ItemTemplate>
                                                  <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("DayOfDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                           <%-- <asp:TemplateField HeaderText="WorkDay ID">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%#  DateTime.Parse(Eval("WDID").ToString()).ToString("MMM-yyyy")  %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Time In">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Time In") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Time In Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("TimeInStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Break In">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("BreakTime In") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                             
                                             <asp:TemplateField HeaderText="Break Out">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("BreakTime Out") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Break Minutes">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("TotalBreakMins") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="No. of Breaks">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("BreakCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                             <asp:TemplateField HeaderText="Time Out">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Time Out") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Time Out Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("TimeOutStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Attendance Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Resultant") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="OverTime in Hrs.">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("OverTimeHours") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Total Time">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("TimeInterval") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    

                    <div class="row ">
                      <div class="col-sm-12">
                            <div class="tab-content ">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                     
                                  <asp:GridView ID="gv2" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Employee Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Ename") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                  <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("AttendanceDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="Day">
                                                <ItemTemplate>
                                                  <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("dayofdate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                  
                                            <asp:TemplateField HeaderText="Break in">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("BreakIn") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Break out">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("BreakOut") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>     
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                </section>             
            </div>
            
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

    </form>
</body>
</html>

