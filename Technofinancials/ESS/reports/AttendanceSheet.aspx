﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttendanceSheet.aspx.cs" Inherits="Technofinancials.ESS.reports.AttendanceSheet" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        td:nth-child(2) {
            text-align: left !important;
        }

        table tbody td {
            text-align: left !important;
        }
                      button#btnView {
    padding: 3px 14px !important;
    margin-top: 2px !important;
}

        .tf-note-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-note-btn i {
                color: #fff !important;
            }

        .tf-disapproved-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-disapproved-btn i {
                color: #fff !important;
            }

        .tf-add-btn {
            background-color: #575757;
            padding: 12px 10px 8px 10px !important;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-add-btn i {
                color: #fff !important;
            }



        th.sorting {
            font-size: 12px !important;
            padding-left: 5px !important;
            padding-right: 5px !important;
        }

        td {
            text-align: center;
        }

        .offf {
            background: #ccc;
            color: #fff;
        }

        .color_infoP
        h3 {
            font-size: 16px;
            margin: 0 !important;
        }

            .color_infoP
            h3 span {
                margin-left: 15px;
            }

        .form-group.color_infoP {
            width: fit-content;
            display: inline-block;
            font-size: 16px;
        }
    </style>
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="UpdatePanel10"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Attendance Sheet</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                    <button class="AD_btn" id="btnPDF" runat="server" visible="false" onserverclick="btnPDF_ServerClick">PDF</button>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>From Date<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator26" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtFromDate" runat="server" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <h4>To Date<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <input type="text" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" class="form-control" id="txtToDate" runat="server" />
                                    </div>
                                </div>


                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnValidate" onserverclick="btnView_ServerClick">View</button>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Employee</h4>
                                        <asp:DropDownList ID="ddlEmployees" runat="server" class="form-control select2" data-plugin="select2">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                 <div class="col-md-12">
                                     <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                         <ContentTemplate>
                                             <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                                 <ContentTemplate>
                                                     <div class="col-lg-12 col-md-12 col-sm-12">
                                                         <div class="form-group" id="divAlertMsg" runat="server">
                                                             <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                                 <span>
                                                                     <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                                 </span>
                                                                 <p id="pAlertMsg" runat="server">
                                                                 </p>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </ContentTemplate>
                                             </asp:UpdatePanel>

                                         </ContentTemplate>
                                         <Triggers>
                                             <asp:PostBackTrigger ControlID="btnView" />
                                         </Triggers>
                                     </asp:UpdatePanel>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-2">
                                    <h1 class="m-0 text-dark">Attendance</h1>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-10">
                                    <div class="attendance-wrapper">
                                        <div class="form-group color_infoP">
                                            <h3><span class="pres">P</span> - Present </h3>
                                        </div>
                                        <div class="form-group color_infoP">
                                            <h3><span class="abs">A</span> - Absent</h3>
                                        </div>
                                        <div class="form-group color_infoP">
                                            <h3><span class="sick">S</span> - Sick Leave</h3>
                                        </div>
                                        <div class="form-group color_infoP">
                                            <h3><span class="casual">C</span> - Casual Leave</h3>
                                        </div>
                                        <div class="form-group color_infoP">
                                            <h3><span class="Off">O</span> - Off Day</h3>
                                        </div>
                                        <div class="form-group color_infoP">
                                            <h3><span class="year">Y</span> - Yearly Leave</h3>
                                        </div>
                                        <div class="form-group color_infoP">
                                            <h3><span class="mater">M</span> - Maternity Leave</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <div class="row">
                        <%--<asp:UpdatePanel runat="server">
                            <ContentTemplate>--%>
                        <div class="col-sm-12 gv-overflow-scrool">
                            <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" OnRowDataBound="gv_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lblEmployee" Text='<%# Eval("WorkDayID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lblEmployee" Text='<%# Eval("Employee") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblDate" Text='<%# Eval("Date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Resultant">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("Resultant") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Time In">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTimeIn" Text='<%# Eval("Time In") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Time In Status">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTimeInStatus" Text='<%# Eval("TimeInStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Break Time">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTimeOut" Text='<%# Eval("TotalBreakTime") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Time Out">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTimeOut" Text='<%# Eval("Time Out") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    <asp:TemplateField HeaderText="Time Out Status">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTimeOutStatus" Text='<%# Eval("TimeOutStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    

                                </Columns>
                            </asp:GridView>
                        </div>
                        <%--</ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>
            $(document).ready(function () {

                $('#example').DataTable();
            });
        </script>

        <script>

               var table = document.getElementById('gv');
                var tbody = table.getElementsByTagName('tbody')[0];
                var cells = tbody.getElementsByTagName('td');

                for (var i = 0, len = cells.length; i < len; i++) {
                    if (cells[i].innerHTML == 'P') {
                        cells[i].className = 'pres';
                    }
                    else if (cells[i].innerHTML == 'A') {
                        cells[i].className = 'abs';
                    }
                    else if (cells[i].innerHTML == 'O' || cells[i].innerHTML == 'H') {
                        cells[i].className = 'offf';
                    }
                    else if (cells[i].innerHTML == 'L') {
                        cells[i].className = 'sick';
                    }
                    else if (cells[i].innerHTML == 'A') {
                        cells[i].className = 'sick';
                    }
                    else {
                        cells[i].className = 'not-marekd';
                    }

                }

        </script>


    </form>
</body>
</html>
