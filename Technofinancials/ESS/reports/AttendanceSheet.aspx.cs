﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.reports
{
    public partial class AttendanceSheet : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false;
                    CheckSessions();
                    BindDropDown();
                    ddlEmployees.SelectedValue = Session["EmployeeID"].ToString();
                    DateTime now = DateTime.Now;
                    txtFromDate.Value = new DateTime(now.Year, now.Month, 1).ToString("dd-MMM-yyyy") ;
                    txtToDate.Value = new DateTime(now.Year, now.Month, 1).AddMonths(1).AddDays(-1).ToString("dd-MMM-yyyy");
                   
                    GetData();
                    
                    Common.addlog("ViewAll", "ESS", "All Attendance Report Viewed", "EmployeeAttendance");
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void BindDropDown()
        {
            try
            {
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                ddlEmployees.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
                ddlEmployees.DataTextField = "EmployeeName";
                ddlEmployees.DataValueField = "EmployeeID";
                ddlEmployees.DataBind();

                if (ddlEmployees != null && ddlEmployees.Items.Count > 1)
                {
                    ddlEmployees.Items.Insert(0, new ListItem("ALL", "0"));
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    foreach (DataControlFieldCell cell in e.Row.Cells)
                    {
                        if (cell.ContainingField is BoundField)
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void GetData()
        {
            try
            {
                string res = "";
                CheckSessions();
                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployees.SelectedValue);
                objDB.FromDate = txtFromDate.Value;
                objDB.ToDate = txtToDate.Value;
                DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
                DateTime.TryParse(txtFromDate.Value, out Fdate);
                DateTime.TryParse(txtToDate.Value, out Tdate);
                if (Tdate < Fdate)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be less than To Date";
                    return;
                }
                if (Fdate > DateTime.Now)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be Equal or less than Current Date";
                    return;
                }
                if ((Tdate - Fdate).Days > 31 || (Fdate.Day <= Tdate.Day && (Tdate.Month - Fdate.Month) > 0))
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "date range should be of 1 month";
                    return;
                }


                dt = objDB.GetAttendanceSheetESSKT(Convert.ToInt32(Session["EmployeeID"]), ref errorMsg);
                gv.DataSource = dt;
                gv.DataBind();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        divAlertMsg.Visible = false;
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gv.DataSource = null;
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                }
                else
                {
                    gv.DataSource = null;
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                }

                Common.addlog("ViewAll", "ESS", "All Employee Attendance Viewed", "EmployeeAttendance");
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                gv.DataSource = "";
                gv.DataBind();
                GetData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Attendance Sheet";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##DATE##", DateTime.Now.ToString("dd-MMM-yyyy"));
                content = content.Replace("##MONTH##", DateTime.Parse(txtFromDate.Value).ToString("MMM"));
                content = content.Replace("##YEAR##", DateTime.Parse(txtFromDate.Value).ToString("yyyy"));
                content = content.Replace("##SHIFT##", "");
                content = content.Replace("##TABLE##", GetTemplate(gv));
                Common.addlog("Report", "ESS", "Attendance Sheet Report Generated", "EmployeeAttendance");

                Common.generatePDF(header, footer, content, "Attendance Sheet - " + " " + " (" + txtFromDate.Value + ")", "A4", "Portrait");
            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private string GetTemplate(GridView gd)
        {
            try
            {
                StringBuilder sheetBody = new StringBuilder();
                StringWriter sw = new StringWriter(sheetBody);
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                gd.RenderControl(hw);
                return sheetBody.ToString();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                return "";
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
}