﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Text;
using SelectPdf;

namespace Technofinancials.ESS.reports
{
    public partial class GenerateReport : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        public string DocName
        {
            get
            {
                if (ViewState["DocName"] != null)
                {
                    return (string)ViewState["DocName"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocName"] = value;
            }
        }

        public int PrimaryValue
        {
            get
            {
                if (ViewState["PrimaryValue"] != null)
                {
                    return (int)ViewState["PrimaryValue"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrimaryValue"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                divAlertMsg.Visible = false;
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/generate-pay-slip";
                
                if ( HttpContext.Current.Items["PageType"].ToString() == "Pay Slip")
                {
                    DocName = HttpContext.Current.Items["PageType"].ToString();
                    getReport(DocName, PrimaryValue);
                    if (DocName == "Pay Slip")
                    {
                        reportsPageHeading.InnerHtml = "Pay Slip";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/generate-pay-slip";

                    }
                 
                }
                else if (HttpContext.Current.Items["PrimaryValue"] != null && HttpContext.Current.Items["PageType"] != null)
                {
                    PrimaryValue = Convert.ToInt32(HttpContext.Current.Items["PrimaryValue"].ToString());
                    DocName = HttpContext.Current.Items["PageType"].ToString();
                    getReport(DocName, PrimaryValue);
                    if (DocName == "Expense Voucher")
                    {
                        reportsPageHeading.InnerHtml = "Expense Voucher";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/expenses";

                    }
                 
                }
                else if (Session["ReportTitle"] != null && Session["ReportDataTable"] != null && Session["BackbtnLink"] != null && HttpContext.Current.Items["PageType"] != null)
                {
                    string s = @"<style>
.table{
margin: 0px auto;
}

.table tr:nth-last-child(odd) td {
                     background: #edf0f5;
color:#000;
border:1px solid white;
font-size: 12px;
padding: 5px 0px 4px 1px;
}

.table tr:nth-last-child(even) td {
  background: #fff;
color:#000;
border:1px solid white;
font-size: 12px;
padding: 5px 0px 4px 1px;
}

.table tr:first-child th {
  background: #188ae2;
  color:#fff;
font-size: 14px;
  font-weight:bold;
border:1px solid white;
}



        </style>";
                    getReport(HttpContext.Current.Items["PageType"].ToString(), s + Session["ReportDataTable"].ToString(), Session["ReportTitle"].ToString());
                    reportsPageHeading.InnerHtml = Session["ReportTitle"].ToString() + " Report";
                    btnBack.HRef = Session["BackbtnLink"].ToString();
                }
            }
        }

        private void getReport(string PageType, string ReportDT, string Title)
        {
            DataTable dt = new DataTable();
            objDB.DocType = PageType;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            dt = objDB.GetDocumentDesign(ref errorMsg);

            if (dt == null)
            {
                objDB.CompanyID = 2;
                objDB.DocType = PageType;
                dt = objDB.GetDocumentDesign(ref errorMsg);
            }

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                    string content = dt.Rows[0]["DocContent"].ToString();
                    content = content.Replace("##TITLE##", Title);

                    content = content.Replace("##TABLE##", ReportDT);
                    txtContent.Content = content;
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                }
            }
        }

        private void getReport(string PageType, int ID)
        {
            DataTable dt = new DataTable();
            string content = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.DocType = PageType;
            dt = objDB.GetDocumentDesign(ref errorMsg);
            if (dt == null)
            {
                objDB.CompanyID = 2;
                objDB.DocType = PageType;
                dt = objDB.GetDocumentDesign(ref errorMsg);
            }


            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                    content = dt.Rows[0]["DocContent"].ToString();
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                }
            }

            if (PageType == "Pay Slip")
            {
               // string YandM = Session["ReportDataTable"].ToString();
               //string[] YandMS = YandM.Split(',');

                DataTable Contentdt = new DataTable();
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                //objDB.Year = YandMS[0];
                //objDB.Month = YandMS[1];
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                Contentdt = Session["ReportDataTable"]as DataTable ;
                //Contentdt = objDB.GetPaySlipByEmployeeID(ref errorMsg);
                if (Contentdt != null)
                {
                    if (Contentdt.Rows.Count > 0)
                    {
                        content = content.Replace("##COMPANYNAME##", Session["CompanyName"].ToString());
                        //content = content.Replace("##MONTH##", YandMS[1]);
                        //content = content.Replace("##YEAR##", YandMS[0]);
                        content = content.Replace("##DATE##", DateTime.Now.ToString("dd-MM-yyyy"));
                        content = content.Replace("##NAME##", Contentdt.Rows[0]["EmployeeName"].ToString());
                        content = content.Replace("##DESIGNATION##", Contentdt.Rows[0]["DesgTitle"].ToString());
                        content = content.Replace("##MONTH##", Contentdt.Rows[0]["NewPayrollDate"].ToString());
                        content = content.Replace("##SALARYAMOUNT##", String.Format("{0:n}", float.Parse(Contentdt.Rows[0]["GrossSalaryWithOT"].ToString())  )) ;
                        content = content.Replace("##TAXAMOUNT##", String.Format("{0:n}", float.Parse(Contentdt.Rows[0]["Tax"].ToString())));
                        content = content.Replace("##ABSENTAMOUNT##", String.Format("{0:n}", float.Parse(Contentdt.Rows[0]["AbsentAmount"].ToString()))); 
                        content = content.Replace("##ADVANCEAMOUNT##", String.Format("{0:n}", float.Parse(Contentdt.Rows[0]["Advance"].ToString()))); 
                        content = content.Replace("##EOBI##", String.Format("{0:n}", float.Parse(Contentdt.Rows[0]["EOBI"].ToString()))); 
                        content = content.Replace("##TOTAL##", String.Format("{0:n}", float.Parse(Contentdt.Rows[0]["TotalSalryWithOT"].ToString()))); 
                        //content = content.Replace("##RUPEES##", Contentdt.Rows[0]["EmployeeName"].ToString());
      

                  



           
                    }
                }

                txtContent.Content = content;
            }
            
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Common.generatePDF(txtHeader.Content, txtFooter.Content, txtContent.Content, "Technofinancials" + DocName.Replace(" ", "-"), "A4", "Portrait");
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


    }
}