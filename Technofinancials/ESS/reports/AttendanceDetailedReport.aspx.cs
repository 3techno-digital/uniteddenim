﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.reports
{
    public partial class AttendanceDetailedReport : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                CheckSessions();
                BindDropdowns();
                divAlertMsg.Visible = false;
                gv2.Visible = false;

                txtToDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
                GetData();
            }
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void BindDropdowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

            ddlLocation.DataSource = objDB.GetAllLocationsNew(ref errorMsg);
            ddlLocation.DataTextField = "NAME";
            ddlLocation.DataValueField = "NAME";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("ALL", "0"));
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            GetData();
        }

        void GetData()
        {
            try
            {
                string res = "";
                CheckSessions();
                divAlertMsg.Visible = false;
                DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
                DateTime.TryParse(txtFromDate.Text, out Fdate);
                DateTime.TryParse(txtToDate.Text, out Tdate);
                if (Tdate < Fdate)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be less than To Date";
                    return;
                }
                if (Fdate > DateTime.Now)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be Equal or less than Current Date";
                    return;
                }
                if ((Tdate - Fdate).Days > 31 || (Fdate.Day <= Tdate.Day && (Tdate.Month - Fdate.Month) > 0))
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "date range should be of 1 month";
                    return;
                }

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.FromDate = txtFromDate.Text;
                objDB.ToDate = txtToDate.Text;
                objDB.WDID = Convert.ToInt32(string.IsNullOrEmpty(txtKTId.Text) ? "0" : txtKTId.Text);
                objDB.Location = ddlLocation.SelectedValue;

                if (Convert.ToInt32(DateTime.Parse(txtToDate.Text).ToString("yyyyMMdd")) < Convert.ToInt32(DateTime.Parse(txtFromDate.Text).ToString("yyyyMMdd")))
                {
                    res = "Invalid dates! to date must be greater than from date";
                }

                if (ddlAttendanceTypes.SelectedValue == "3")
                {
                    dt = objDB.GetBreakInBreakout(Convert.ToInt32(Session["EmployeeID"]), ref errorMsg);
                    gv2.DataSource = dt;
                    gv2.DataBind();
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            gv2.Visible = true;
                            gv.Visible = false;
                            divAlertMsg.Visible = false;
                            gv2.UseAccessibleHeader = true;
                            gv2.HeaderRow.TableSection = TableRowSection.TableHeader;
                        }
                        else
                        {
                            gv.DataSource = null;
                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                            pAlertMsg.InnerHtml = "No Record Found";
                        }
                    }
                    else
                    {
                        gv.DataSource = null;
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                }
                else
                {
                    gv2.Visible = false;
                    gv.Visible = true;
                    objDB.Resultant = ddlAttendanceTypes.SelectedValue;
                    dt = objDB.GetAttendanceSheetESSKTnew(Convert.ToInt32(Session["EmployeeID"]), ref errorMsg);
                    gv.DataSource = dt;
                    gv.DataBind();

                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            divAlertMsg.Visible = false;
                            gv.UseAccessibleHeader = true;
                            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                        }
                        else
                        {
                            gv.DataSource = null;
                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                            pAlertMsg.InnerHtml = "No Record Found";
                        }
                    }
                    else
                    {

                        gv.DataSource = null;
                        gv.DataBind();
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                    //Common.addlog("ViewAll", "ESS", "All Employee Attendance Viewed", "EmployeeAttendance");
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}