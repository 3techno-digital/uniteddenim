﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Attachments.aspx.cs" Inherits="Technofinancials.ESS.Attachments" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">

            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <h1 class="m-0 text-dark">Policies & Procedures</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4" style="display:none;">
                            <div style="text-align: right;">
                                <a class="AD_btn" id="btnBack" runat="server">Back</a>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>


            <div class="wrap">
                <section class="app-content">
                    <div class="clearfix">&nbsp;</div>




                    <div class="row">
                        <div class="col-sm-12">

                            <asp:GridView ID="gvFiles" runat="server" CssClass="table table-bordered table-sm gv dataTable " ClientIDMode="Static" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                              <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Title">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("PolicyProcedureTitle") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Description")  %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Attachment">
                                        <ItemTemplate>
                                            <a target="_blank" runat="server" class="AD_stock" id="lblTitle" href='<%# Eval("DocPath") %>'>Download</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            div#gvFiles_wrapper{
    margin-bottom: 15%;
}
        </style>
    </form>
</body>
</html>
