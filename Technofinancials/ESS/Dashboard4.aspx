﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard4.aspx.cs" Inherits="Technofinancials.ESS.Dashboard4" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet">
    <style>
        .slimScrollDiv {
            height: 85vh !important;
        }

        .mini-stat {
            background: #fff;
            padding: 17px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            margin-bottom: 20px;
        }

        .mini-stat-icon {
            width: 50px;
            height: 50px;
            display: inline-block;
            line-height: 45px;
            text-align: center;
            font-size: 30px;
            background: #eee;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            float: left;
            margin-right: 10px;
            color: #fff;
        }

        .profile-nav ul > li > a > i {
            color: #bcb3aa;
        }

        .orange {
            background: #fa8564 !important;
        }

        .mini-stat-info {
            font-size: 12px;
            padding-top: 2px;
            color: #767676 !important;
            font-weight: 700;
        }

            .mini-stat-info span {
                display: block;
                font-size: 24px;
                font-weight: 600;
                color: #767676 !important;
            }

        .tar {
            background: #1fb5ac !important;
        }

        .pink {
            background: #a48ad4 !important;
        }

        .mini-stat .green {
            background: #aec785 !important;
        }

        body {
            color: #767676 !important;
        }

        a.canvasjs-chart-credit {
            display: none;
        }

        .r1_maingraph {
            min-height: 180px;
            width: 100%;
            background: #ffffff;
        }

        .icon-purple {
            color: rgba(153, 114, 181, 1.0);
        }

        .icon-primary {
            color: rgba(31, 181, 172, 1);
        }

        .r1_maingraph .switch {
            cursor: hand;
            cursor: pointer;
            position: absolute;
            top: 15px;
            right: 30px;
            z-index: 99;
        }

        .panel {
            border: none;
            box-shadow: none;
            padding-bottom: 30px;
            margin-bottom: 0px;
        }

        .profile-nav .user-heading.clock-row {
            padding: 20px;
            border-radius: 0px;
        }

        .user-heading.alt {
            display: inline-block;
            width: 100%;
            text-align: left;
        }

        .profile-nav .user-heading {
            color: #fff;
            -webkit-border-radius: 4px 4px 0 0;
        }

            .profile-nav .user-heading h1 {
                font-size: 20px;
                font-weight: 300;
                margin-bottom: 5px;
            }

        .clock-row h1 {
            font-size: 22px !important;
            font-weight: 400 !important;
            letter-spacing: 1px;
            margin: 0 0 5px 0 !important;
            letter-spacing: 1px;
        }

        .profile-nav .user-heading p {
            font-size: 16px;
            color: #8b8b8b;
            line-height: 25px;
        }

        .clock-row p {
            font-size: 18px;
            font-weight: 300;
            color: #fff !important;
            margin: 0;
        }

        .profile-nav .user-heading p {
            font-size: 16px;
            color: #8b8b8b;
            line-height: 25px;
        }

        .clock-row p {
            font-size: 18px;
            font-weight: 300;
            color: #fff !important;
            margin: 0;
        }

        #clock {
            position: relative;
            width: 185px;
            height: 185px;
            margin: -95px auto 0;
            background: url(/assets/images/clockface.png) no-repeat;
            list-style: none;
        }

        .stopwatch {
            display: grid;
            justify-items: center;
            grid-row-gap: 23px;
            width: 100%;
        }

        .circle {
            display: flex;
            justify-content: center;
            align-items: center;
            color: #575757;
            height: 200px;
            width: 200px;
            border: 2px solid;
            border-radius: 50%;
        }

        .time {
            font-family: "Roboto Mono", monospace;
            font-weight: 300;
            font-size: 40px;
        }

        .gold {
            font-weight: 900;
            color: #f2c94c;
            text-shadow: 0 0 0px #fff, 0 0 50px #f2c94c;
        }

        .controls {
            display: flex;
            justify-content: space-between;
            width: 187px;
        }

        button {
            cursor: pointer;
            background: transparent;
            padding: 0;
            border: none;
            margin: 0;
            outline: none;
        }

        #playButton {
            display: block;
        }

        #pauseButton {
            display: none;
        }

        .clock-row {
            text-transform: uppercase;
            min-height: 170px;
        }

        .terques-bg {
            background-color: #4a88c2;
        }

        ul#clock li {
            border: none;
        }

        #sec {
            background: url(/assets/images/sechand.png);
            z-index: 3;
            overflow: hidden;
        }

        #min {
            background: url(/assets/images/minhand.png);
            z-index: 2;
        }

        #hour {
            background: url(/assets/images/hourhand.png);
            z-index: 1;
        }

        #sec, #min, #hour {
            position: absolute;
            width: 15px;
            height: 185px;
            top: 0px;
            left: 83px;
        }

        ul.clock-category {
            padding: 15px 0;
        }

            ul.clock-category li {
                display: inline-block;
                width: 24%;
                text-align: center;
                border: none;
            }

        .profile-nav ul > li {
            margin-top: 0;
            line-height: 30px;
        }

        ul.clock-category li a:hover, ul.clock-category li a.active {
            color: #1fb5ad !important;
            background: none !important;
        }

        canvas.canvasjs-chart-canvas {
            width: 100%;
            height: 295px;
        }

        ul.clock-category li a:hover i, ul.clock-category li a.active i {
            color: #1fb5ad !important;
        }

        ul.clock-category li a span {
            display: block;
            line-height: normal;
        }

        ul.clock-category li a i {
            font-size: 30px;
            padding-right: 0;
        }

        .event-calendar {
            background: #4a88c2;
            min-height: 290px;
            margin-bottom: 1.5rem;
        }

        .table > thead > tr > th, table-bordered > thead > tr > th {
            border: none !important;
        }

        .cal1 .clndr .clndr-table tr .day .day-contents {
            box-sizing: border-box;
            padding: 8px;
            font-size: 12px;
            text-align: center;
        }

        .table {
            color: #fff;
        }

        .calendar-block .day-contents {
            width: 30px;
            margin: auto;
        }

        .calendar-block {
            float: right !important;
            background: #fff;
            min-height: 474px;
            z-index: 1000;
        }

        .cal1 {
            margin: 30px auto;
            max-width: 600px;
            font-size: 14px;
        }

        .calendar-block .clndr {
            margin: 40px 0;
        }

        .cal1 .clndr .clndr-controls {
            display: inline-block;
            width: 100%;
            position: relative;
            margin-bottom: 10px;
            text-align: center;
        }

        .calendar-block .clndr-controls {
            margin: 45px 0px !important;
        }

        .cal1 .clndr .clndr-controls .clndr-control-button {
            display: inline-block;
            text-align: right;
        }

            .cal1 .clndr .clndr-controls .clndr-control-button .clndr-previous-button {
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                padding: 5px 10px;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                color: #C1C1C1;
            }

        .cal1 .clndr .clndr-controls .month {
            display: inline-block;
            text-align: center;
            color: #c1c1c1;
            font-size: 20px;
            text-transform: uppercase;
            font-family: 'Open Sans',sans-serif;
            font-weight: 300;
            padding: 5px 5px;
            position: relative;
            top: 2px;
        }

        .cal1 .clndr .clndr-controls .clndr-control-button.leftalign {
            text-align: left !important;
        }

        .cal1 .clndr .clndr-controls .clndr-control-button .clndr-next-button {
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            padding: 5px 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            color: #C1C1C1;
        }

        .cal1 .clndr .clndr-table {
            table-layout: fixed;
            width: 100%;
        }

            .cal1 .clndr .clndr-table .header-days {
                height: 40px;
                font-size: 10px;
                background: #fff;
            }

                .cal1 .clndr .clndr-table .header-days .header-day {
                    vertical-align: middle;
                    text-align: center;
                    border-left: 0px solid #000000;
                    border-top: 0px solid #000000;
                    color: #666;
                    font-size: 14px;
                }

            .cal1 .clndr .clndr-table tr {
                height: auto;
            }

                .cal1 .clndr .clndr-table tr .empty, .cal1 .clndr .clndr-table tr .adjacent-month {
                    border-left: 0px solid #000000;
                    border-top: 0px solid #000000;
                    width: 100%;
                    height: inherit;
                    /* background: #eee; */
                }

                .cal1 .clndr .clndr-table tr .day {
                    border-left: 0px solid #000000;
                    border-top: 0px solid #000000;
                    width: 100%;
                    height: inherit;
                }

                .cal1 .clndr .clndr-table tr td {
                    vertical-align: top;
                }

        .event-list-block {
            -webkit-border-radius: 5px 0px 0px 5px;
            -moz-border-radius: 5px 0px 0px 5px;
            border-radius: 5px 0px 0px 5px;
        }

        .cal-day {
            font-size: 24px;
            font-weight: 600;
            color: #fff;
            padding-top: 15px;
            padding-bottom: 20px;
        }

            .cal-day span {
                font-size: 14px;
                display: block;
            }

        .evnt-input, .evnt-input:focus {
            background: none;
            border: none;
            border-bottom: #fff 1px solid !important;
            border-color: none !important;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px;
            color: #fff !important;
            padding-left: 0px;
            margin-top: 10px;
        }

        .event-list {
            padding-left: 0;
        }

            .event-list li {
                background: rgba(255,255,255,.2);
                padding: 10px 30px 10px 10px;
                color: #fff;
                margin-bottom: 5px;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                position: relative;
                list-style: none;
            }

        span.mini-stat-icon img {
            width: 50%;
            height: 50%;
        }

        .event-close {
            display: inline-block;
            font-size: 17px;
            color: #fff;
            position: absolute;
            right: 12px;
            top: 8px;
        }

        .cal1 .clndr .clndr-table tr .day .day-contents {
            box-sizing: border-box;
            padding: 8px;
            font-size: 12px;
            text-align: center;
        }

        input.form-control.evnt-input {
            border-top: none !important;
            border-left: none !important;
            border-right: none !important;
        }

        .today .day-contents {
            width: 30px;
            background: #4a88c2;
            cursor: pointer;
            color: #fff;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .widget {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .display-table-cell {
            background-color: #fff;
            padding: 20px;
        }

        .ClassyCountdown-wrapper {
            padding: 20px;
        }

        span.mini-stat-icon.purple {
            background-color: #f4b9b9;
        }

        span.mini-stat-icon.yellow {
            background-color: #f9c851;
        }

        .card {
            border: none;
        }

        .m-b-20 {
            margin-bottom: 20px;
        }

        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
            padding-bottom: 0px;
        }

        .mb-4, .my-4 {
            margin-bottom: 1.5rem !important;
        }

        #boxscroll, #boxscroll2 {
            height: 405px;
            width: auto;
            overflow: auto;
        }

        .media {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: start;
            align-items: flex-start;
        }

        .float-left {
            float: left !important;
        }

        .new-user table td img, .new-user ul li img {
            margin-right: 8px;
            width: 50px;
        }

        .media-body {
            -ms-flex: 1;
            flex: 1;
        }

        .rounded-circle {
            border-radius: 50% !important;
        }

        .text-muted {
            color: #707070 !important;
        }

        img.text-danger.mr-1.pull-right.rounded-circle {
            float: right;
            position: absolute;
            top: 14%;
            left: 80%;
            width: 25px;
            height: 25px;
        }

        .calendar-block .day-contents:hover {
            width: 30px;
            background: #4a88c2;
            cursor: pointer;
            color: #fff;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .cal1 .clndr .clndr-controls .clndr-control-button .clndr-previous-button:hover {
            background: #f4f4f4;
            padding: 5px 10px;
        }

        .cal1 .clndr .clndr-controls .clndr-control-button .clndr-next-button:hover {
            background: #f4f4f4;
            padding: 5px 10px;
        }

        .ultra-todo-task {
            padding: 30px;
            margin-bottom: 30px;
        }

        .bg-primary {
            background: #4a88c2;
            padding: 3px 8px;
            color: #ffffff;
            height: 470px;
        }

        .wid-task-header {
            display: block;
            margin-bottom: 10px;
        }

            .wid-task-header .wid-icon {
                float: left;
            }

                .wid-task-header .wid-icon i {
                    font-size: 30px;
                    color: #fff;
                    margin: 3px 15px 0 0;
                }

            .wid-task-header .wid-text h4 {
                color: #ffffff;
                display: block;
                margin: 0px 0 0px 0;
                font-weight: 700;
            }

            .wid-task-header .wid-text span {
                display: block;
                color: #e1e1e1;
                white-space: nowrap;
            }

        .wid-all-tasks {
            display: block;
            color: #ffffff;
            margin: 15px 0;
        }

            .wid-all-tasks ul {
                overflow: hidden;
                position: relative;
                max-height: 297px;
            }

        .icheckbox_minimal-white.checked {
            background-position: -40px 0;
        }

        .wid-all-tasks ul li.checked label {
            color: #dddddd;
        }

        .wid-all-tasks ul li label:after {
            content: " ";
            height: 1px;
            background-color: transparent;
            position: relative;
            top: -11px;
            width: 100%;
            display: block;
        }

        .icheckbox_minimal-white, .iradio_minimal-white {
            display: inline-block;
            *display: inline;
            vertical-align: middle;
            margin: 0;
            padding: 0;
            width: 18px;
            height: 18px;
            background: url(/assets/images/white.png) no-repeat;
            border: none;
            cursor: pointer;
        }

        .wid-task-header .wid-text h4 {
            color: #ffffff;
            font-size: 24px !important;
            display: block;
            margin: 0px 0 0px 0;
            font-weight: 700;
        }

        .ultra-todo-task {
            padding: 30px;
            margin-bottom: 30px;
        }

        .small, small {
            font-size: 85%;
            color: #e1ded3;
        }

        [class*="icheckbox_"], [class*="iradio_"] {
            margin-right: 8px;
        }

        .wid-add-task {
            margin-top: 15px;
        }

        input[type=checkbox], input[type=color], input[type=date], input[type=datetime-local], input[type=datetime], input[type=email], input[type=month], input[type=number], input[type=password], input[type=radio], input[type=search], input[type=tel], input[type=text], input[type=time], input[type=url], input[type=week], select, textarea, .form-control {
            box-shadow: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            -ms-box-shadow: none;
            -o-box-shadow: none;
            border-radius: 0px;
            -o-border-radius: 0px;
            -ms-border-radius: 0px;
            -moz-border-radius: 0px;
            -webkit-border-radius: 0px;
            border: 1px solid #e1e1e1;
        }

        .panel-heading {
            border-color: #eff2f7;
            font-size: 13px;
            font-weight: 400;
            background: #fafafa;
            text-transform: uppercase;
            padding: 15px;
        }

        .tools a {
            margin-left: 10px;
            color: #a7a7a7;
            font-size: 12px;
        }

        .alert-success, .alert-danger, .alert-info, .alert-warning {
            border: none;
        }

        .alert-info {
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }

        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-info .alert-icon {
            background-color: #99cce3;
        }

        .alert-icon {
            width: 40px;
            height: 40px;
            display: inline-block;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
        }

        .notification-info {
            margin-left: 56px;
            margin-top: -40px;
        }

        .notification-meta {
            margin-bottom: 3px;
            padding-left: 0;
            list-style: none;
        }

        .notification-sender {
            color: #414147;
        }

        .notification-time {
            font-style: italic;
            color: #999;
        }

        .alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .alert-success {
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
        }

        .alert-warning {
            color: #8a6d3b;
            background-color: #fcf8e3;
            border-color: #faebcc;
        }

        .alert-danger .alert-icon {
            background-color: #fcb1ae;
        }

        .alert-success .alert-icon {
            background-color: #98d7ad;
        }

        .alert-warning .alert-icon {
            background-color: #ffe699;
        }

        .alert-icon i {
            width: 40px;
            height: 40px;
            display: block;
            text-align: center;
            line-height: 40px;
            font-size: 20px;
            color: #fff;
            font-family: 'FontAwesome';
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>

        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <%--<asp:UpdatePanel ID="updpnl" runat="server">
            <ContentTemplate>--%>
        <main id="app-main" class="app-main">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <h1 class="m-0 text-dark">Employee Self Service</h1>
                                </div>

                                <!-- /.col -->
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div style="text-align: right;">
                                        <%--<button class="AD_btn" id="btnTimeIN" runat="server" onserverclick="btnTimeIN_ServerClick" type="button">Time In</button>
                                        <button class="AD_btn" id="btnBreakTimeOut" runat="server" onserverclick="btnBreakTimeOut_ServerClick" type="button">Break Out</button>
                                        <button class="AD_btn" id="btnBreakTimeIN" runat="server" onserverclick="btnBreakTimeIN_ServerClick" type="button">Break In</button>
                                        <button class="AD_btn" id="btnTimeOut" runat="server" onserverclick="btnTimeOut_ServerClick" type="button">Time Out</button>--%>

                                        <button class="AD_btn" id="btnTimeIN" runat="server" disabled="disabled" onclick="TimeIN();" type="button">Time In</button>
                                        <button class="AD_btn" id="btnAway" runat="server" visible="false" disabled="disabled" onclick="BreakTimeOut();" type="button">Break Start</button>
                                        <button class="AD_btn" id="btnBreakTimeOut" runat="server" disabled="disabled" onclick="BreakTimeOut();" type="button">Break Start</button>
                                        <button class="AD_btn" id="btnActive" runat="server" visible="false" disabled="disabled" onclick="BreakTimeOut();" type="button">Break End</button>
                                        <button class="AD_btn" id="btnBreakTimeIN" runat="server" disabled="disabled" onclick="BreakTimeIN();" type="button">Break End</button>
                                        <button class="AD_btn" id="btnTimeOut" runat="server" disabled="disabled" onclick="TimeOut();" type="button">Time Out</button>

                                    </div>
                                    <asp:HiddenField ID="hdSec" Value="0" runat="server" />
                                    <asp:HiddenField ID="timer_is_on" Value="1" runat="server" />

                                    <asp:HiddenField ID="hdBreakInSec" Value="0" runat="server" />
                                    <asp:HiddenField ID="timer_is_on_BreakIn" Value="0" runat="server" />

                                    <asp:HiddenField ID="hdWorkHoursSec" Value="0" runat="server" />

                                    <div class="modal fade M_set" id="myModal" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h1 class="m-0 text-dark">Late Reason</h1>
                                                    <div class="add_new">
                                                        <button type="button" class="AD_btn" runat="server" id="btnSubmit" onserverclick="btnSubmit_ServerClick">Submit</button>
                                                        <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                                    </div>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        <textarea id="txtReason" runat="server" rows="5" placeholder="Reason (max 100 characters).." maxlength="100" class="form-control"></textarea>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon tar">
                                        <img src="/assets/images/present.png"></span>
                                    <div class="mini-stat-info">
                                        <span data-to="0" data-speed="1000">
                                            <asp:Literal ID="lblPresent" runat="server"></asp:Literal>
                                        </span>
                                        Presents
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon orange">
                                        <img src="/assets/images/absent.png"></span>
                                    <div class="mini-stat-info">
                                        <span data-to="7" data-speed="1500">
                                            <asp:Literal ID="lblAbsent" runat="server"></asp:Literal>
                                        </span>
                                        Absents
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon pink">
                                        <img src="/assets/images/leave.png"></span>
                                    <div class="mini-stat-info">
                                        <span data-to="0" data-speed="1000">
                                            <asp:Literal ID="lblLeave" runat="server"></asp:Literal>
                                        </span>
                                        Leaves
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon purple">
                                        <img src="/assets/images/holiday.png"></span>
                                    <div class="mini-stat-info">
                                        <span data-to="0" data-speed="1000">
                                            <asp:Literal ID="lblHalfDay" runat="server"></asp:Literal>
                                        </span>
                                        Holidays
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon yellow">
                                        <img src="/assets/images/Missing-punches.png"></span>
                                    <div class="mini-stat-info">
                                        <span data-to="0" data-speed="1000">
                                            <asp:Literal ID="lblMissingPunches" runat="server"></asp:Literal>
                                        </span>
                                        Missing Punches
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon green">
                                        <img src="/assets/images/short-duration.png"></span>
                                    <div class="mini-stat-info">
                                        <span data-to="0" data-speed="1000">
                                            <asp:Literal ID="lblEarlyOut" runat="server"></asp:Literal>
                                        </span>
                                        Short Durations
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="event-calendar clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-8 event-list-block">
                                    <div class="cal-day">Announcements</div>
                                    <div class="slimScrollDiv-on" style="position: relative; overflow: hidden; width: auto; height: 191px !important;">
                                        <asp:GridView ID="gvAnnouncement" runat="server" CssClass="table table-bordered  dataTable no-footer" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Announcement Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol6" Text='<%# Eval("Date") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Title">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("AnnouncementTitle") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Description")  %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                        <ul class="event-list" style="overflow: hidden; width: auto; height: 305px;">
                                            <table class="table" style="display: none;">
                                                <tbody>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Announcement Date</th>
                                                        <th>Title</th>
                                                        <th>Description</th>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>30-Aug-2019</td>
                                                        <td>Lorem Ipsum</td>
                                                        <td>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</td>
                                                    </tr>

                                                    <tr>
                                                        <td>1</td>
                                                        <td>30-Aug-2019</td>
                                                        <td>Lorem Ipsum</td>
                                                        <td>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </ul>
                                        <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 305px;"></div>
                                        <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="widget">
                                <header class="widget-header">
                                    <h4 class="widget-title">Daily Timer</h4>
                                </header>
                                <div class="widget-body">
                                    <div class="stopwatch">
                                        <div class="circle">
                                            <span class="time" id="display">00:00:00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="profile-nav alt">
                                <section class="panel">
                                    <div class="user-heading alt clock-row terques-bg">
                                        <h1>December 14</h1>
                                        <p class="text-left">2014, Friday</p>
                                        <p class="text-left">7:53 PM</p>
                                    </div>
                                    <ul id="clock">
                                        <li id="sec" style="transform: rotate(240deg);"></li>
                                        <li id="hour" style="transform: rotate(498.5deg);"></li>
                                        <li id="min" style="transform: rotate(222deg);"></li>
                                    </ul>
                                </section>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">

                            <div class="card bg-white m-b-20">
                                <div class="card-body new-user" id="theCarousel" runat="server">
                                    <h4 class="header-title mt-0 mb-4 widget-title">Birthday</h4>
                                    <ul class="list-unstyled mb-0 pr-3" id="boxscroll2" tabindex="1" style="overflow: auto; outline: none;">
                                        <asp:Repeater ID="repBirthday" runat="server">
                                            <ItemTemplate>
                                                <li class="p-3" style="margin-bottom:10px;">
                                                    <div class="media">
                                                        <div class="thumb float-left">
                                                            <a href="#">
                                                                <img src="<%# Eval("EmployeePhoto") %>" style="width: 50px;"><%--<img class="rounded-circle" src="/assets/images/profile.png">--%></a>
                                                        </div>
                                                        <div class="media-body">
                                                            <p class="media-heading mb-0">
                                                                <strong><%# Eval("EmployeeName") %></strong>
                                                                <img class="text-danger mr-1 pull-right rounded-circle" src="/assets/images/profile.png">
                                                            </p>
                                                            <small class="pull-right text-muted"><%# Convert.ToDateTime(Eval("DOB")).ToString("MMMM dd") %></small>
                                                            <small class="text-muted"><%# Eval("City") %></small>

                                                        </div>
                                                    </div>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>

                            <div class="widget map_radius announcment_area" style="height: 245px; display: none;">
                                <header class="widget-header">
                                    <h4 class="widget-title">Birthday</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-6">
                                            <div class="carousel slide multi-item-carousel">
                                                <div runat="server" id="divBtnArrows">
                                                    <a class="left carousel-control" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                                    <a class="right carousel-control" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="calendar-block">
                                <div class="cal1 ">
                                    <div class="clndr">
                                        <div class="clndr-controls">
                                            <div class="clndr-control-button"><span class="clndr-previous-button"><i class="fa fa-chevron-left"></i></span></div>
                                            <div class="month">December 2020</div>
                                            <div class="clndr-control-button leftalign"><span class="clndr-next-button"><i class="fa fa-chevron-right"></i></span></div>
                                        </div>
                                        <table class="clndr-table" border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr class="header-days">
                                                    <td class="header-day">S</td>
                                                    <td class="header-day">M</td>
                                                    <td class="header-day">T</td>
                                                    <td class="header-day">W</td>
                                                    <td class="header-day">T</td>
                                                    <td class="header-day">F</td>
                                                    <td class="header-day">S</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="day past adjacent-month last-month calendar-day-2020-11-29">
                                                        <div class="day-contents">29</div>
                                                    </td>
                                                    <td class="day past adjacent-month last-month calendar-day-2020-11-30">
                                                        <div class="day-contents">30</div>
                                                    </td>
                                                    <td class="day past calendar-day-2020-12-01">
                                                        <div class="day-contents">1</div>
                                                    </td>
                                                    <td class="day past calendar-day-2020-12-02">
                                                        <div class="day-contents">2</div>
                                                    </td>
                                                    <td class="day past calendar-day-2020-12-03">
                                                        <div class="day-contents">3</div>
                                                    </td>
                                                    <td class="day past calendar-day-2020-12-04">
                                                        <div class="day-contents">4</div>
                                                    </td>
                                                    <td class="day past calendar-day-2020-12-05">
                                                        <div class="day-contents">5</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="day today calendar-day-2020-12-06">
                                                        <div class="day-contents">6</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-07">
                                                        <div class="day-contents">7</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-08">
                                                        <div class="day-contents">8</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-09">
                                                        <div class="day-contents">9</div>
                                                    </td>
                                                    <td class="day event calendar-day-2020-12-10">
                                                        <div class="day-contents">10</div>
                                                    </td>
                                                    <td class="day event calendar-day-2020-12-11">
                                                        <div class="day-contents">11</div>
                                                    </td>
                                                    <td class="day event calendar-day-2020-12-12">
                                                        <div class="day-contents">12</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="day event calendar-day-2020-12-13">
                                                        <div class="day-contents">13</div>
                                                    </td>
                                                    <td class="day event calendar-day-2020-12-14">
                                                        <div class="day-contents">14</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-15">
                                                        <div class="day-contents">15</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-16">
                                                        <div class="day-contents">16</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-17">
                                                        <div class="day-contents">17</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-18">
                                                        <div class="day-contents">18</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-19">
                                                        <div class="day-contents">19</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="day calendar-day-2020-12-20">
                                                        <div class="day-contents">20</div>
                                                    </td>
                                                    <td class="day event calendar-day-2020-12-21">
                                                        <div class="day-contents">21</div>
                                                    </td>
                                                    <td class="day event calendar-day-2020-12-22">
                                                        <div class="day-contents">22</div>
                                                    </td>
                                                    <td class="day event calendar-day-2020-12-23">
                                                        <div class="day-contents">23</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-24">
                                                        <div class="day-contents">24</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-25">
                                                        <div class="day-contents">25</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-26">
                                                        <div class="day-contents">26</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="day calendar-day-2020-12-27">
                                                        <div class="day-contents">27</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-28">
                                                        <div class="day-contents">28</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-29">
                                                        <div class="day-contents">29</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-30">
                                                        <div class="day-contents">30</div>
                                                    </td>
                                                    <td class="day calendar-day-2020-12-31">
                                                        <div class="day-contents">31</div>
                                                    </td>
                                                    <td class="day adjacent-month next-month calendar-day-2021-01-01">
                                                        <div class="day-contents">1</div>
                                                    </td>
                                                    <td class="day adjacent-month next-month calendar-day-2021-01-02">
                                                        <div class="day-contents">2</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="ultra-widget ultra-todo-task bg-primary">
                                <div class="wid-task-header">
                                    <div class="wid-icon">
                                        <i class="fa fa-tasks"></i>
                                    </div>
                                    <div class="wid-text">
                                        <h4>To do Tasks</h4>
                                        <span>Wed, <small>11<sup>th</sup> March 2015</small></span>
                                    </div>
                                </div>
                                <div class="wid-all-tasks">

                                    <ul class="list-unstyled ps-container">

                                        <li class="checked">
                                            <div class="icheckbox_minimal-white checked" style="position: relative;">
                                                <input type="checkbox" id="task-1" class="icheck-minimal-white todo-task" checked="" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <label class="icheck-label form-label" for="task-1">Office Projects</label>
                                        </li>
                                        <li class="checked">
                                            <div class="icheckbox_minimal-white checked" style="position: relative;">
                                                <input type="checkbox" id="task-2" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <label class="icheck-label form-label" for="task-2">Generate Invoice</label>
                                        </li>
                                        <li class="checked">
                                            <div class="icheckbox_minimal-white checked" style="position: relative;">
                                                <input type="checkbox" id="task-2" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <label class="icheck-label form-label" for="task-2">Generate Invoice</label>
                                        </li>
                                        <li class="checked">
                                            <div class="icheckbox_minimal-white checked" style="position: relative;">
                                                <input type="checkbox" id="task-2" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <label class="icheck-label form-label" for="task-2">Generate Invoice</label>
                                        </li>

                                        <li>
                                            <div class="icheckbox_minimal-white" style="position: relative;">
                                                <input type="checkbox" id="task-3" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <label class="icheck-label form-label" for="task-3">Ecommerce Theme</label>
                                        </li>
                                        <li class="checked">
                                            <div class="icheckbox_minimal-white checked" style="position: relative;">
                                                <input type="checkbox" id="task-4" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <label class="icheck-label form-label" for="task-4">PHP and jQuery</label>
                                        </li>
                                        <li class="checked">
                                            <div class="icheckbox_minimal-white checked" style="position: relative;">
                                                <input type="checkbox" id="task-5" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            <label class="icheck-label form-label" for="task-5">Allocate&nbsp;Resource</label>
                                        </li>
                                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
                                            <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                        </div>
                                        <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                                            <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                        </div>
                                    </ul>

                                </div>
                                <div class="wid-add-task">
                                    <input type="text" class="form-control" placeholder="Add task">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <!--notification start-->
                            <section class="panel">
                                <header class="panel-heading">
                                    Notification <span class="tools pull-right">
                                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                                        <a href="javascript:;" class="fa fa-cog"></a>
                                        <a href="javascript:;" class="fa fa-times"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="alert alert-info clearfix">
                                        <span class="alert-icon"><i class="fa fa-envelope-o"></i></span>
                                        <div class="notification-info">
                                            <ul class="clearfix notification-meta">
                                                <li class="pull-left notification-sender"><span><a href="#">Jonathan Smith</a></span> send you a mail </li>
                                                <li class="pull-right notification-time">1 min ago</li>
                                            </ul>
                                            <p>
                                                Urgent meeting for next proposal
                                            </p>
                                        </div>
                                    </div>
                                    <div class="alert alert-danger">
                                        <span class="alert-icon"><i class="fa fa-facebook"></i></span>
                                        <div class="notification-info">
                                            <ul class="clearfix notification-meta">
                                                <li class="pull-left notification-sender"><span><a href="#">Jonathan Smith</a></span> mentioned you</li>
                                                <li class="pull-right notification-time">7 Hours Ago</li>
                                            </ul>
                                            <p>
                                                Very cool photo jack
                                            </p>
                                        </div>
                                    </div>
                                    <div class="alert alert-success ">
                                        <span class="alert-icon"><i class="fa fa-comments-o"></i></span>
                                        <div class="notification-info">
                                            <ul class="clearfix notification-meta">
                                                <li class="pull-left notification-sender">You have 5 message</li>
                                                <li class="pull-right notification-time">1 min ago</li>
                                            </ul>
                                            <p>
                                                <a href="#">Anjelina Mewlo, Jack Flip</a> and <a href="#">3 others</a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="alert alert-warning ">
                                        <span class="alert-icon"><i class="fa fa-bell-o"></i></span>
                                        <div class="notification-info">
                                            <ul class="clearfix notification-meta">
                                                <li class="pull-left notification-sender">Domain Renew Deadline 7 days</li>
                                                <li class="pull-right notification-time">5 Days Ago</li>
                                            </ul>
                                            <p>
                                                Next 5 July Thursday is the last day
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--notification end-->
                        </div>
                    </div>
                </section>

                <!-- now no container fluid just rows  -->
                <div class="clearfix">&nbsp;</div>

                <div class="clearfix">&nbsp;</div>


                <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>


        <!-- Resources -->

        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <!-- Chart code -->
        <!-- HTML -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
        <script>
            $(function () {

                setInterval(function () {
                    var seconds = new Date().getSeconds();
                    var sdegree = seconds * 6;
                    var srotate = "rotate(" + sdegree + "deg)";

                    $("#sec").css({ "-moz-transform": srotate, "-webkit-transform": srotate });

                }, 1000);


                setInterval(function () {
                    var hours = new Date().getHours();
                    var mins = new Date().getMinutes();
                    var hdegree = hours * 30 + (mins / 2);
                    var hrotate = "rotate(" + hdegree + "deg)";

                    $("#hour").css({ "-moz-transform": hrotate, "-webkit-transform": hrotate });

                }, 1000);


                setInterval(function () {
                    var mins = new Date().getMinutes();
                    var mdegree = mins * 6;
                    var mrotate = "rotate(" + mdegree + "deg)";

                    $("#min").css({ "-moz-transform": mrotate, "-webkit-transform": mrotate });

                }, 1000);

            });
        </script>
        <script>
            jQuery(function ($) {

                'use strict';

                var ULTRA_SETTINGS = window.ULTRA_SETTINGS || {};

                /*--------------------------------
                    Sparkline Chart
                 --------------------------------*/
                ULTRA_SETTINGS.dbSparklineChart = function () {

                    if ($.isFunction($.fn.sparkline)) {

                        $('.db_dynamicbar').sparkline([8.4, 9, 8.8, 8, 9.5, 9.2, 9.9, 9, 9, 8, 7, 9, 9, 9.5, 8, 9.5, 9.8], {
                            type: 'bar',
                            barColor: '#f5f5f5',
                            height: '40',
                            barWidth: '10',
                            barSpacing: 1,
                        });

                        $('.db_linesparkline').sparkline([2000, 3454, 5454, 2323, 3432, 4656, 2897, 3545, 4232, 5434, 4656, 3567, 4878, 3676, 3787], {
                            type: 'line',
                            width: '100%',
                            height: '40',
                            lineWidth: 2,
                            lineColor: '#f5f5f5',
                            fillColor: 'rgba(255,255,255,0.2)',
                            highlightSpotColor: '#ffffff',
                            highlightLineColor: '#ffffff',
                            spotRadius: 3,
                        });


                        // Bar + line composite charts
                        $('.db_compositebar').sparkline([4, 6, 7, 7, 4, 3, 2, 4, 6, 7, 7, 4, 3, 1, 4, 6, 5, 9], {
                            type: 'bar',
                            barColor: '#f5f5f5',
                            height: '40',
                            barWidth: '10',
                            barSpacing: 1,
                        });

                        $('.db_compositebar').sparkline([4, 1, 5, 7, 9, 9, 8, 8, 4, 7, 9, 9, 8, 8, 4, 2, 5, 6, 7], {
                            composite: true,
                            fillColor: 'rgba(153,114,181,0)',
                            type: 'line',
                            width: '100%',
                            height: '40',
                            lineWidth: 2,
                            lineColor: '#9972b5',
                            highlightSpotColor: '#fa8564',
                            highlightLineColor: '#9972b5',
                            spotRadius: 3,
                        });



                    }

                };




                /*--------------------------------
                    Easy PIE
                 --------------------------------*/
                ULTRA_SETTINGS.dbEasyPieChart = function () {

                    if ($.isFunction($.fn.easyPieChart)) {

                        $('.db_easypiechart1').easyPieChart({
                            barColor: '#9972b5',
                            trackColor: '#f5f5f5',
                            scaleColor: '#f5f5f5',
                            lineCap: 'square',
                            lineWidth: 6,
                            size: 120,
                            animate: 2000,
                            onStep: function (from, to, percent) {
                                $(this.el).find('.percent').text(Math.round(percent));
                            }
                        });
                    }

                };




                /*--------------------------------
                    Morris 
                 --------------------------------*/
                ULTRA_SETTINGS.dbMorrisChart = function () {


                    /*Area Graph*/
                    // Use Morris.Area instead of Morris.Line
                    Morris.Area({
                        element: 'db_morris_area_graph',
                        data: [{
                            x: '2009 Q1',
                            y: 3,
                            z: 2
                        }, {
                            x: '2010 Q2',
                            y: 2,
                            z: 1
                        }, {
                            x: '2011 Q3',
                            y: 1,
                            z: 2
                        }, {
                            x: '2011 Q4',
                            y: 2,
                            z: 2
                        }, {
                            x: '2012 Q5',
                            y: 4,
                            z: 2
                        }, {
                            x: '2012 Q6',
                            y: 2,
                            z: 4
                        }],
                        resize: true,
                        redraw: true,
                        xkey: 'x',
                        ykeys: ['y', 'z'],
                        labels: ['Y', 'Z'],
                        lineColors: ['#9972b5', '#1fb5ac'],
                        pointFillColors: ['#fa8564']
                    }).on('click', function (i, row) {
                        console.log(i, row);
                    });


                    /*Line Graph*/
                    /* data stolen from http://howmanyleft.co.uk/vehicle/jaguar_'e'_type */
                    var day_data = [{
                        "period": "2012-10-01",
                        "pageviews": 3407,
                        "unique": 660
                    }, {
                        "period": "2012-09-30",
                        "pageviews": 3351,
                        "unique": 629
                    }, {
                        "period": "2012-09-29",
                        "pageviews": 3269,
                        "unique": 618
                    }, {
                        "period": "2012-09-20",
                        "pageviews": 3246,
                        "unique": 661
                    }, {
                        "period": "2012-09-19",
                        "pageviews": 3257,
                        "unique": 667
                    }, {
                        "period": "2012-09-18",
                        "pageviews": 3248,
                        "unique": 627
                    }, {
                        "period": "2012-09-17",
                        "pageviews": 3171,
                        "unique": 660
                    }, {
                        "period": "2012-09-16",
                        "pageviews": 3171,
                        "unique": 676
                    }, {
                        "period": "2012-09-15",
                        "pageviews": 3201,
                        "unique": 656
                    }, {
                        "period": "2012-09-10",
                        "pageviews": 3215,
                        "unique": 622
                    }];
                    Morris.Line({
                        element: 'db_morris_line_graph',
                        data: day_data,
                        resize: true,
                        redraw: true,
                        xkey: 'period',
                        ykeys: ['pageviews', 'unique'],
                        labels: ['Page Views', 'Unique Visitors'],
                        lineColors: ['#9972b5', '#1fb5ac'],
                        pointFillColors: ['#fa8564']
                    });

                    /*Bar Graph*/
                    // Use Morris.Bar
                    Morris.Bar({
                        element: 'db_morris_bar_graph',
                        data: [{
                            x: '2011 Q1',
                            y: 3,
                            z: 2
                        }, {
                            x: '2011 Q2',
                            y: 2,
                            z: 1
                        }, {
                            x: '2011 Q3',
                            y: 1,
                            z: 2
                        }, {
                            x: '2011 Q4',
                            y: 2,
                            z: 2
                        }, {
                            x: '2011 Q5',
                            y: 4,
                            z: 2
                        }, {
                            x: '2011 Q6',
                            y: 2,
                            z: 4
                        }],
                        resize: true,
                        redraw: true,
                        xkey: 'x',
                        ykeys: ['y', 'z'],
                        labels: ['Y', 'Z'],
                        barColors: ['#9972b5', '#1fb5ac']
                    }).on('click', function (i, row) {
                        console.log(i, row);
                    });

                    $('.r1_maingraph .switch .fa').on('click', function () {

                        $('.r1_maingraph .switch .fa').removeClass("icon-default").addClass("icon-secondary");

                        if ($(this).hasClass("fa-bar-chart")) {
                            $(this).toggleClass("icon-secondary icon-default");
                            $("#db_morris_line_graph").hide();
                            $("#db_morris_area_graph").hide();
                            $("#db_morris_bar_graph").show();
                        }

                        if ($(this).hasClass("fa-line-chart")) {
                            $(this).toggleClass("icon-secondary icon-default");
                            $("#db_morris_line_graph").show();
                            $("#db_morris_area_graph").hide();
                            $("#db_morris_bar_graph").hide();
                        }

                        if ($(this).hasClass("fa-area-chart")) {
                            $(this).toggleClass("icon-secondary icon-default");
                            $("#db_morris_line_graph").hide();
                            $("#db_morris_area_graph").show();
                            $("#db_morris_bar_graph").hide();
                        }

                    });


                };



                /*--------------------------------
                    Rickshaw charts
                 --------------------------------*/
                ULTRA_SETTINGS.dbRickshawChart = function () {



                    /*------------------- extensions chart - start----------------------*/

                    // set up our data series with 100 random data points

                    var seriesData = [
                        [],
                        [],
                        [],
                        [],
                        [],
                        [],
                        [],
                        [],
                        []
                    ];
                    var random = new Rickshaw.Fixtures.RandomData(150);

                    for (var i = 0; i < 50; i++) {
                        random.addData(seriesData);
                    }

                    // instantiate our graph!

                    var graph = new Rickshaw.Graph({
                        element: document.getElementById("chart"),
                        width: $(".rickshaw_ext").width(),
                        height: 235,
                        renderer: 'area',
                        stroke: true,
                        preserve: true,
                        series: [{
                            color: '#1fb5ac',
                            data: seriesData[0],
                            name: 'Upload'
                        }, {
                            color: '#fa8564',
                            data: seriesData[1],
                            name: 'Download'
                        }, {
                            color: '#9972b5',
                            data: seriesData[2],
                            name: 'Speed'
                        }]
                    });

                    graph.render();

                    var preview = new Rickshaw.Graph.RangeSlider({
                        graph: graph,
                        element: document.getElementById('preview'),
                    });

                    var hoverDetail = new Rickshaw.Graph.HoverDetail({
                        graph: graph,
                        xFormatter: function (x) {
                            return new Date(x * 1000).toString();
                        }
                    });

                    var annotator = new Rickshaw.Graph.Annotate({
                        graph: graph,
                        element: document.getElementById('timeline')
                    });

                    var legend = new Rickshaw.Graph.Legend({
                        graph: graph,
                        element: document.getElementById('legend')

                    });

                    var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
                        graph: graph,
                        legend: legend
                    });

                    var order = new Rickshaw.Graph.Behavior.Series.Order({
                        graph: graph,
                        legend: legend
                    });

                    var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight({
                        graph: graph,
                        legend: legend
                    });

                    var smoother = new Rickshaw.Graph.Smoother({
                        graph: graph,
                        element: document.querySelector('#smoother')
                    });

                    var ticksTreatment = 'glow';

                    var xAxis = new Rickshaw.Graph.Axis.Time({
                        graph: graph,
                        ticksTreatment: ticksTreatment,
                        timeFixture: new Rickshaw.Fixtures.Time.Local()
                    });

                    xAxis.render();

                    var yAxis = new Rickshaw.Graph.Axis.Y({
                        graph: graph,
                        tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
                        ticksTreatment: ticksTreatment
                    });

                    yAxis.render();


                    var controls = new RenderControls({
                        element: document.querySelector('form#rickshaw_side_panel'),
                        graph: graph
                    });

                    // add some data every so often

                    var messages = [
                        "Changed home page welcome message",
                        "Minified JS and CSS",
                        "Changed button color from blue to green",
                        "Refactored SQL query to use indexed columns",
                        "Added additional logging for debugging",
                        "Fixed typo",
                        "Rewrite conditional logic for clarity",
                        "Added documentation for new methods"
                    ];

                    setInterval(function () {
                        random.removeData(seriesData);
                        random.addData(seriesData);
                        graph.update();

                    }, 3000);

                    function addAnnotation(force) {
                        if (messages.length > 0 && (force || Math.random() >= 0.95)) {
                            annotator.add(seriesData[2][seriesData[2].length - 1].x, messages.shift());
                            annotator.update();
                        }
                    }

                    addAnnotation(true);
                    setTimeout(function () {
                        setInterval(addAnnotation, 6000)
                    }, 6000);

                    var previewXAxis = new Rickshaw.Graph.Axis.Time({
                        graph: graph,
                        timeFixture: new Rickshaw.Fixtures.Time.Local(),
                        ticksTreatment: ticksTreatment
                    });

                    previewXAxis.render();


                    /*------------------- extensions chart - end----------------------*/



                };





                /*--------------------------------
                     gauge meter
                 --------------------------------*/
                ULTRA_SETTINGS.dbGaugemeter = function () {

                    if ($("#gauge-meter").length) {
                        var opts = {
                            lines: 1, // The number of lines to draw
                            angle: 0.05, // The length of each line
                            lineWidth: 0.30, // The line thickness
                            pointer: {
                                length: 0.40, // The radius of the inner circle
                                strokeWidth: 0.038, // The rotation offset
                                color: '#ffffff' // Fill color
                            },
                            limitMax: 'false', // If true, the pointer will not go past the end of the gauge
                            colorStart: '#9972b5', // Colors
                            colorStop: '#9972b5', // just experiment with them
                            strokeColor: '#ffffff', // to see which ones work best for you
                            generateGradient: false
                        };
                        var target = document.getElementById('gauge-meter'); // your canvas element
                        var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
                        gauge.maxValue = 100; // set max gauge value
                        gauge.animationSpeed = 90; // set animation speed (32 is default value)

                        gauge.set(56); // set actual value
                        gauge.setTextField(document.getElementById("gauge-meter-text"));
                        randomGauge();

                    }

                    function randomGauge() {
                        setTimeout(function () {
                            var val = Math.random() * 99;
                            gauge.set(val); // set actual va{lue
                            AnimationUpdater.run();
                            randomGauge();
                        }, 2000);
                    }

                };



                /******************************
                 initialize respective scripts 
                 *****************************/
                $(document).ready(function () {
                    ULTRA_SETTINGS.dbSparklineChart();
                    ULTRA_SETTINGS.dbEasyPieChart();
                    ULTRA_SETTINGS.dbMorrisChart();
                    ULTRA_SETTINGS.dbRickshawChart();
                    ULTRA_SETTINGS.dbGaugemeter();
                });

                $(window).resize(function () {
                    ULTRA_SETTINGS.dbSparklineChart();
                });

                $(window).load(function () { });

            });
        </script>

    </form>

</body>


</html>
