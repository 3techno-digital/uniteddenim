﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BreakAdjustment.aspx.cs" Inherits="Technofinancials.ESS.BreakAdjustment" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        button#btnView {
    padding: 4px 24px;
}
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Break Adjustments</h1>
                            </div>
                          
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                    <button class="AD_btn" id="btnApprove" runat="server" onserverclick="btnApprove_ServerClick" validationgroup="btnValidate" type="button"><%--<i class="fa fa-thumbs-up"></i>--%> Approve</button>
                                    <button class="AD_btn"   data-toggle="modal" data-target="#notes-modal" validationgroup="btnValidate" type="button" title="Reject"><%--<i class="fa fa-thumbs-down"></i>--%> Reject</button>
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/manage/break-adjustment/add-new-break-adjustment"); %>" class="AD_btn">Add</a>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>   
                               <!-- Modal -->
                                <div class="modal fade M_set" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="m-0 text-dark">Reason</h1>
                                                <div class="add_new">
                                                    <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                                                    <button type="button" class="AD_btn" runat="server"    id="btnDisapprove"   onserverclick="btnApprove_ServerClick" data-dismiss="modal">Save</button>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtAdjustmentNote" runat="server" rows="5" placeholder="Reason.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                <section class="app-content">

                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>From Date
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtFromDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>To Date
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtToDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                <div class="col-md-12">

                                    <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee</h4>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2" data-plugin="select2">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Adjustment Status</h4>
                                        <asp:DropDownList ID="ddlAdjustmentStatus" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                         <ContentTemplate>
                                             <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                                 <ContentTemplate>
                                                     <div class="col-lg-12 col-md-12 col-sm-12">
                                                         <div class="form-group" id="divAlertMsg" runat="server">
                                                             <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                                 <span>
                                                                     <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                                 </span>
                                                                 <p id="pAlertMsg" runat="server">
                                                                 </p>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </ContentTemplate>
                                             </asp:UpdatePanel>

                                         </ContentTemplate>

                                     </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                                          <div class="clearfix">&nbsp;</div>
                    <div class="row ">
                      <div class="col-sm-12">
                            <div class="tab-content ">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                  <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" EmptyDataText="No Records Found" ShowHeaderWhenEmpty="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No.">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chckchanged" ID="checkAll" ToolTip="Click To Select All"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" Enabled='<%# Eval("DocStatus").ToString()=="Data Submitted for Review"||Eval("DocStatus").ToString()=="Saved as Draft"? true : false %>' ID="check" ></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="EmployeeName">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblAttendanceDate" Text='<%# Convert.ToDateTime( Eval("AttendanceDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Break Time In">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblBreakTimeIn" Text='<%# Convert.ToDateTime( Eval("BreakTimeIn")).ToString("hh:mm tt") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Break Time Out">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblBreakTimeOut" Text='<%# Convert.ToDateTime( Eval("BreakTimeOut")).ToString("hh:mm tt") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Break">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblBreak" Text='<%# Eval("TotalBreakTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblDocStatus" Text='<%# Eval("DocStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Prepared Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblPrepareDate" Text='<%# Convert.ToDateTime( Eval("PreparedDate")).ToString("dd-MMM-yyy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View">
                                                    <ItemTemplate>
                                                        <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/employee-self-service/manage/break-adjustment/edit-break-adjustment-" + Eval("BreakAdjustmentID") %>" class="AD_stock">View </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblResultant" Text='<%#  Eval("Resultant") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeID" Text='<%# Eval("EmployeeID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblBreakAdjustmentID" Text='<%# Eval("BreakAdjustmentID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>   
                            </div>
                                </div>
                        </div>
                      </div>
                    </div>

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

       
    </form>
</body>
</html>
