﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
	public partial class AttendanceAdjustment : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                DataTable dtEmployees = new DataTable();
                btnApprove.Visible = btnDisapprove.Visible = false;
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                dtEmployees = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);

                ddlEmployee.DataSource = dtEmployees;
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();


               

                if (dtEmployees != null && dtEmployees.Rows.Count == 1)
                {
                    btnDisapprove.Visible = btnApprove.Visible = false;
                }
                else
                {
                    ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));
                    btnDisapprove.Visible = btnApprove.Visible = true;
                }

                ddlAdjustmentStatus.DataSource = objDB.GetAttendanceAdjustmentStatus(ref errorMsg);
                ddlAdjustmentStatus.DataTextField = "AdjustmentStatus";
                ddlAdjustmentStatus.DataValueField = "AdjustmentStatus";
                ddlAdjustmentStatus.DataBind();
                ddlAdjustmentStatus.Items.Insert(0, new ListItem("ALL", "0"));
                ddlAdjustmentStatus.Text = "Data Submitted for Review";
                DateTime now = DateTime.Now;
                txtFromDate.Text = new DateTime(now.Year, now.Month, 1).ToString("dd-MMM-yyyy");
                txtToDate.Text = now.ToString("dd-MMM-yyyy");

                GetData("Data Submitted for Review");
            }
        }

        private DataTable FilterData(DataTable dt)
        {
            if (dt == null)
            {
                return dt;
            }
            DataTable dtFilter = new DataTable();

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.DocName = "Loans";

            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");

                    if (dtTemp != null)
                        dtFilter.Merge(dtTemp);

                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }
                }
            }

            return dtFilter;
        }

        private void GetData(string AttendanceAdjustmentStatus = "0")
        {
            divAlertMsg.Visible = false;
            DataTable dt = new DataTable();
            string res = string.Empty;
            DateTime fromdate, todate;
            fromdate = DateTime.Parse(txtFromDate.Text);
            todate = DateTime.Parse(txtToDate.Text);

            objDB.EmployeeID = Convert.ToInt16(ddlEmployee.SelectedValue);
            objDB.FromDate = fromdate.ToString();
            objDB.ToDate = todate.ToString();

            DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
            DateTime.TryParse(txtFromDate.Text, out Fdate);
            DateTime.TryParse(txtToDate.Text, out Tdate);
            if (Tdate < Fdate)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "From Date shoul be less than To Date";
                gv.DataSource = "";
                gv.DataBind();
                return;
            }
            if (Fdate > DateTime.Now)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "From Date shoul be Equal or less than Current Date";
                gv.DataSource = "";
                gv.DataBind();
                return;
            }
            if ((Tdate - Fdate).Days > 31 || (Fdate.Day <= Tdate.Day && (Tdate.Month - Fdate.Month) > 0))
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "date range should be of 1 month";
                gv.DataSource = "";
                gv.DataBind();
                return;
            }
            dt = objDB.GetAllAttendanceAdjustmentByEmployeeID(ddlEmployee.SelectedValue == Session["EmployeeID"].ToString() ? "0" : Session["EmployeeID"].ToString(), ref errorMsg, AttendanceAdjustmentStatus);
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.DataSource = dt;
                    gv.DataBind();
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;

                    btnApprove.Visible = btnDisapprove.Visible = ddlEmployee.Items.Count > 1;
                }
            }

            Common.addlog("ViewAll", "ESS", "All employee-Adjustments Viewed", "AttendanceAdjustment");
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        //protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    DataTable dt = (DataTable)gv.DataSource;
        //    foreach (DataRow row in dt.Rows)
        //    {
        //         row.Field("");
        //        CheckBox chkcheck = (CheckBox)row.FindControl("check");
        //        chkcheck.Checked = chkSelectAll.Checked;
        //    }
        //}

        protected void chckchanged(object sender, EventArgs e)

        {
            CheckBox chckheader = (CheckBox)gv.HeaderRow.FindControl("checkAll");

            foreach (GridViewRow row in gv.Rows)
            {
                if (((Label)row.FindControl("lblDocStatus")).Text == "Data Submitted for Review" || ((Label)row.FindControl("lblDocStatus")).Text == "Saved as Draft")
                {
                    ((CheckBox)row.FindControl("check")).Checked = chckheader.Checked;
                }
            }
        }
        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            try
            {
                btnApprove.Visible = btnDisapprove.Visible = false;

                GetData(ddlAdjustmentStatus.SelectedValue);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                gv.DataSource = null;
                gv.DataBind();
            }
        }

        protected void btnApprove_ServerClick(object sender, EventArgs e)
        {
            btnApprove.Visible = false;
            btnDisapprove.Visible = false;
            CheckSessions();
            string alertMsg = "";
            int recordsCount = 0;
            foreach (GridViewRow gvr in gv.Rows)
            {
                if (((CheckBox)gvr.FindControl("check")).Checked)
                {
                    string employeeID = ((Label)gvr.FindControl("lblEmployeeID")).Text;
                    int attendanceAdjustmentId = Convert.ToInt32(((Label)gvr.FindControl("lblAttendanceAdjustmentID")).Text);
                    objDB.FindIndirectMgr = Convert.ToInt32(Session["EmployeeID"].ToString());

                    if (employeeID != Session["EmployeeID"].ToString())
					{
						string requestStatus = ApproveAttendanceAdjustmentRequest(attendanceAdjustmentId, sender, employeeID);
						recordsCount++;
					}
                    
                    

                    else if(objDB.GetIndirectReportedEmployeeByEmployeeID(ref errorMsg).Rows[0][0].ToString() == Session["EmployeeID"].ToString())
					{
                        // self approval

                        // only if direct mgr belongs to US with limited user access and current login is indirect mgr 
                        if(objDB.CheckIFDirectMgrBelongsToUnitedState(ref errorMsg).Rows[0][0].ToString() == "1")
						{
                            string requestStatus = ApproveAttendanceAdjustmentRequest(attendanceAdjustmentId, sender, employeeID);
                            recordsCount++;
                        }

						else
						{
                            // self approval not allowed
                        }
                      
                    }
					else
					{

					}



				}
            }

            GetData(ddlAdjustmentStatus.SelectedValue);
            alertMsg = recordsCount > 0 ? "" + recordsCount + " Records Updated Successfully" : "No Records Updated ";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + alertMsg + "')", true);
        }

        private string ApproveAttendanceAdjustmentRequest(int attendanceAdjustmentId, object sender, string employeeID)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "AttendanceAdjustment", "AttendanceAdjustmentID", attendanceAdjustmentId.ToString(), Session["UserName"].ToString());
           Common.addlogNew( res, "AttendanceAdjustment",
                "Attendance Adjustment \"" + objDB.Title + "\" Status",
                "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + attendanceAdjustmentId.ToString(),
                "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + attendanceAdjustmentId.ToString(), "Attendance Adjustment", "AttendanceAdjustment", "ESS",Convert.ToInt32(employeeID));

            if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
            {
                objDB.AttendanceAdjustmentID = attendanceAdjustmentId;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.ApprovedBy = Session["UserName"].ToString();
                objDB.ApproveAttendanceAdjustment();
            }
            else if (res == "Rejected & Disapproved Sucessfull" || res == "Disapproved" || res == "Rejected" || res == "Disapproved")
            {
                objDB.AttendanceAdjustmentID = attendanceAdjustmentId;
                objDB.EnableAttendanceAdjustment();
                objDB.AttendanceAdjustmentID = attendanceAdjustmentId;
                objDB.AttendanceAdjustmentRemarks = txtAdjustmentNote.Value;
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.RejectAttendanceAdjustment();
            }

            return res;
        }
    }
}

