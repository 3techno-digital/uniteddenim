﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashboardUS.aspx.cs" Inherits="Technofinancials.ESS.DashboardUS" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Orbitron&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <style>

        .graph-wrapper {
            width: 100%;
            max-width: 750px;
            margin: 0 auto;
        }

        .graph {
            height: 300px;
            width: 100%;
            max-width: 750px;
            padding: 25px;
            background-color: #FFF;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0,0,0,.2);
        }
        .morris-hover.morris-default-style .morris-hover-row-label {
    background-color: #2d3b48;
    color: #ffffff;
    padding: 4px;
    border-radius: 5px 5px 0 0;
    margin: -10px -12px 10px;
}
        .slimScrollDiv {
            height: 85vh !important;
        }
        div#theCarousel .media-body {
            padding-left:10px;
        }
        .morris-hover {
    position: absolute;
    z-index: 1000;
}
        div#theCarousel .thumb.float-left {
            width: 50px;
            height: 50px;
        }
        .input-search:focus {
        background-image:none;
    }
        .mini-stat {
            background: #fff;
            padding: 17px;
            box-shadow: 0 2px 3px 0px #ccc;
            margin-bottom: 20px;
            border-radius: 10px;
        }

        .panel-body p {
            line-height: 2.2;
        }

        .mini-stat-icon {
            width: 50px;
            height: 50px;
            display: inline-block;
            line-height: 45px;
            text-align: center;
            font-size: 30px;
            background: #eee;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            float: left;
            margin-right: 10px;
            color: #fff;
        }

        .profile-nav ul > li > a > i {
            color: #bcb3aa;
        }

        .orange {
            background: #fa8564 !important;
        }

        .mini-stat-info {
            font-size: 12px;
            padding-top: 2px;
            color: #767676 !important;
            font-weight: 700;
        }

            .mini-stat-info span {
                display: block;
                font-size: 24px;
                font-weight: 600;
                color: #767676 !important;
            }

        .tar {
            background: #1fb5ac !important;
        }

        .pink {
            background: #a48ad4 !important;
        }

        .mini-stat .green {
            background: #aec785 !important;
        }

        body {
            color: #767676 !important;
        }

        a.canvasjs-chart-credit {
            display: none;
        }

        .r1_maingraph {
            min-height: 180px;
            width: 100%;
            background: #ffffff;
        }

        .icon-purple {
            color: rgba(153, 114, 181, 1.0);
        }

        .icon-primary {
            color: rgba(31, 181, 172, 1);
        }

        .r1_maingraph .switch {
            cursor: hand;
            cursor: pointer;
            position: absolute;
            top: 15px;
            right: 30px;
            z-index: 99;
        }

        .panel {
            border: none;
            box-shadow: none;
            border-radius: 20px;
            box-shadow: 0 2px 3px 0px #ccc;
            background: #e8e8e8;
        }

        .profile-nav .user-heading.clock-row {
            padding: 20px;
            border-radius: 0px;
            border-top-left-radius: 20px;
    border-top-right-radius: 20px;
        }

        .user-heading.alt {
            display: inline-block;
            width: 100%;
            text-align: left;
        }

        .profile-nav .user-heading {
            color: #fff;
            -webkit-border-radius: 4px 4px 0 0;
        }

            .profile-nav .user-heading h1 {
                font-size: 20px;
                font-weight: 300;
                margin-bottom: 5px;
            }

        .clock-row h1 {
            font-size: 22px !important;
            font-weight: 400 !important;
            letter-spacing: 1px;
            margin: 0 0 5px 0 !important;
            letter-spacing: 1px;
        }

        .profile-nav .user-heading p {
            font-size: 16px;
            color: #8b8b8b;
            line-height: 25px;
        }

        .clock-row p {
            font-size: 18px;
            font-weight: 300;
            color: #fff !important;
            margin: 0;
        }

        .profile-nav .user-heading p {
            font-size: 16px;
            color: #8b8b8b;
            line-height: 25px;
        }

        .clock-row p {
            font-size: 18px;
            font-weight: 300;
            color: #fff !important;
            margin: 0;
        }

        #jqclock {
            position: relative;
            width: 185px;
            height: 235px;
            margin: -95px auto 0;
            background: url(/assets/images/clockface.png) no-repeat;
            list-style: none;
        }

        .stopwatch {
            background-color: #4a88c2;
            display: grid;
            justify-items: center;
            grid-row-gap: 23px;
            width: 100%;
            height: 186px;
            padding: 3%;
            text-align: center;
        }

        .circle {
            display: flex;
            justify-content: center;
            align-items: center;
            color: #575757;
            height: 200px;
            width: 200px;
            border: 2px solid;
            border-radius: 50%;
        }

        .time {
            font-family: 'Orbitron', sans-serif;
            font-weight: 300;
            font-size: 40px;
        }

        .gold {
            font-weight: 900;
            color: #f2c94c;
            text-shadow: 0 0 0px #fff, 0 0 50px #f2c94c;
        }

        .controls {
            display: flex;
            justify-content: space-between;
            width: 187px;
        }

        button {
            cursor: pointer;
            background: transparent;
            padding: 0;
            border: none;
            margin: 0;
            outline: none;
        }

        #playButton {
            display: block;
        }

        #pauseButton {
            display: none;
        }

        .clock-row {
            text-transform: uppercase;
            min-height: 170px;
        }

        .terques-bg {
            background-color: #4a88c2;
        }

        ul#clock li {
            border: none;
        }

        #sec {
            background: url(/assets/images/sechand.png);
            z-index: 3;
            overflow: hidden;
        }

        #min {
            background: url(/assets/images/minhand.png);
            z-index: 2;
        }

        a.nav-link.user {
            padding: 3px 7px !important;
            height: 31px !important;
        }

        #hour {
            background: url(/assets/images/hourhand.png);
            z-index: 1;
        }

        #sec, #min, #hour {
            position: absolute;
            width: 15px;
            height: 185px;
            top: 0px;
            left: 83px;
        }

        i.fa.fa-bell {
            padding-right: 0px !important;
            padding-left: 3px !important;
            text-align: center;
            font-size: 12px !important;
        }

        i.fa.fa-user {
            font-size: 12px !important;
        }

        ul.clock-category {
            padding: 15px 0;
        }

            ul.clock-category li {
                display: inline-block;
                width: 24%;
                text-align: center;
                border: none;
            }

        .profile-nav ul > li {
            margin-top: 0;
            line-height: 30px;
        }

        ul.clock-category li a:hover, ul.clock-category li a.active {
            color: #1fb5ad !important;
            background: none !important;
        }

        canvas.canvasjs-chart-canvas {
            width: 100%;
            height: 295px;
        }

        ul.clock-category li a:hover i, ul.clock-category li a.active i {
            color: #1fb5ad !important;
        }

        ul.clock-category li a span {
            display: block;
            line-height: normal;
        }

        ul.clock-category li a i {
            font-size: 30px;
            padding-right: 0;
        }

        span label {
            font-size: 40px !important;
            text-align: center;
            padding: 0px 8px;
            margin: 0px;
            font-family: 'Orbitron', sans-serif;
        }

        .event-calendar {
            background: #4a88c2;
            min-height: 290px;
            margin-bottom: 1.5rem;
        }

        .table > thead > tr > th, table-bordered > thead > tr > th {
            border: none !important;
        }

        .cal1 .clndr .clndr-table tr .day .day-contents {
            box-sizing: border-box;
            padding: 8px;
            font-size: 12px;
            text-align: center;
        }

        .table {
            color: #fff;
        }

        .calendar-block .day-contents {
            width: 30px;
            margin: auto;
        }

        .calendar-block {
            float: right !important;
            background: #fff;
            min-height: 474px;
            z-index: 1000;
        }

        .cal1 {
            margin: 30px auto;
            max-width: 600px;
            font-size: 14px;
        }

        .calendar-block .clndr {
            margin: 40px 0;
        }

        .cal1 .clndr .clndr-controls {
            display: inline-block;
            width: 100%;
            position: relative;
            margin-bottom: 10px;
            text-align: center;
        }

        .calendar-block .clndr-controls {
            margin: 45px 0px !important;
        }

        .line-through {
            text-decoration: line-through;
        }

        .cal1 .clndr .clndr-controls .clndr-control-button {
            display: inline-block;
            text-align: right;
        }

            .cal1 .clndr .clndr-controls .clndr-control-button .clndr-previous-button {
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                padding: 5px 10px;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                color: #C1C1C1;
            }

        .cal1 .clndr .clndr-controls .month {
            display: inline-block;
            text-align: center;
            color: #c1c1c1;
            font-size: 20px;
            text-transform: uppercase;
            font-family: 'Open Sans',sans-serif;
            font-weight: 300;
            padding: 5px 5px;
            position: relative;
            top: 2px;
        }

        .cal1 .clndr .clndr-controls .clndr-control-button.leftalign {
            text-align: left !important;
        }

        .cal1 .clndr .clndr-controls .clndr-control-button .clndr-next-button {
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            padding: 5px 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            color: #C1C1C1;
        }

        .cal1 .clndr .clndr-table {
            table-layout: fixed;
            width: 100%;
        }

            .cal1 .clndr .clndr-table .header-days {
                height: 40px;
                font-size: 10px;
                background: #fff;
            }

                .cal1 .clndr .clndr-table .header-days .header-day {
                    vertical-align: middle;
                    text-align: center;
                    border-left: 0px solid #000000;
                    border-top: 0px solid #000000;
                    color: #666;
                    font-size: 14px;
                }

            .cal1 .clndr .clndr-table tr {
                height: auto;
            }

                .cal1 .clndr .clndr-table tr .empty, .cal1 .clndr .clndr-table tr .adjacent-month {
                    border-left: 0px solid #000000;
                    border-top: 0px solid #000000;
                    width: 100%;
                    height: inherit;
                    /* background: #eee; */
                }

                .cal1 .clndr .clndr-table tr .day {
                    border-left: 0px solid #000000;
                    border-top: 0px solid #000000;
                    width: 100%;
                    height: inherit;
                }

                .cal1 .clndr .clndr-table tr td {
                    vertical-align: top;
                }

        .event-list-block {
            -webkit-border-radius: 5px 0px 0px 5px;
            -moz-border-radius: 5px 0px 0px 5px;
            border-radius: 5px 0px 0px 5px;
        }

        .cal-day {
            font-size: 24px;
            font-weight: 600;
            color: #fff;
            padding-top: 15px;
            padding-bottom: 20px;
        }

            .cal-day span {
                font-size: 14px;
                display: block;
            }

        .evnt-input, .evnt-input:focus {
            background: none;
            border: none;
            border-bottom: #fff 1px solid !important;
            border-color: none !important;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px;
            color: #fff !important;
            padding-left: 0px;
            margin-top: 10px;
        }

        .event-list {
            padding-left: 0;
        }

            .event-list li {
                background: rgba(255,255,255,.2);
                padding: 10px 30px 10px 10px;
                color: #fff;
                margin-bottom: 5px;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                position: relative;
                list-style: none;
            }

        span.mini-stat-icon img {
            width: 50%;
            height: 50%;
        }

        .event-close {
            display: inline-block;
            font-size: 17px;
            color: #fff;
            position: absolute;
            right: 12px;
            top: 8px;
        }

        .cal1 .clndr .clndr-table tr .day .day-contents {
            box-sizing: border-box;
            padding: 8px;
            font-size: 12px;
            text-align: center;
        }

        input.form-control.evnt-input {
            border-top: none !important;
            border-left: none !important;
            border-right: none !important;
        }

        .today .day-contents {
            width: 30px;
            background: #4a88c2;
            cursor: pointer;
            color: #fff;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .widget {
            -webkit-box-shadow: none;
            box-shadow: none;
                box-shadow: 0 2px 3px 0px #ccc;
    background: #e8e8e8;
    border-radius: 20px;
}
        }

        .display-table-cell {
            background-color: #fff;
            padding: 20px;
        }

        .ClassyCountdown-wrapper {
            padding: 20px;
        }

        span.mini-stat-icon.purple {
            background-color: #f4b9b9;
        }

        span.mini-stat-icon.yellow {
            background-color: #f9c851;
        }

        .card {
            border: none;
        }

        .m-b-20 {
            margin-bottom: 20px;
        }
        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px grey;
            border-radius: 10px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #003780;
            border-radius:10px;
        }

            /* Handle on hover */
            ::-webkit-scrollbar-thumb:hover {
                background: #003780;
            }

        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding-bottom: 5px;
    padding-right: 10px;
        }

        .mb-4, .my-4 {
            margin-bottom: 1.5rem !important;
        }

        #boxscroll, #boxscroll2 {
            height: 330px;
            width: auto;
            overflow: auto;
            padding-top: 1.25rem;
/*            padding-left: 1.25rem;
*/        }

        .media {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: start;
            align-items: flex-start;
        }

        .float-left {
            float: left !important;
        }

        .new-user table td img, .new-user ul li img {
            margin-right: 8px;
            width: 50px;
        }

        .media-body {
            -ms-flex: 1;
            flex: 1;
        }

        .rounded-circle {
            border-radius: 50% !important;
        }

        .text-muted {
            color: #707070 !important;
        }

        img.text-danger.mr-1.pull-right.rounded-circle {
            float: right;
            position: relative;
            top: 14%;
            left: -15%;
            width: 25px;
            height: 25px;
        }

        .calendar-block .day-contents:hover {
            width: 30px;
            background: #4a88c2;
            cursor: pointer;
            color: #fff;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .cal1 .clndr .clndr-controls .clndr-control-button .clndr-previous-button:hover {
            background: #f4f4f4;
            padding: 5px 10px;
        }

        .cal1 .clndr .clndr-controls .clndr-control-button .clndr-next-button:hover {
            background: #f4f4f4;
            padding: 5px 10px;
        }

        .ultra-todo-task {
            padding: 30px;
            margin-bottom: 30px;
        }

        .bg-primary {
            background: #4a88c2;
            padding: 3px 8px;
            color: #ffffff;
            height: 470px;
        }

        .wid-task-header {
            display: block;
            margin-bottom: 10px;
        }

        input[type=checkbox], input[type=color], input[type=date], input[type=datetime-local], input[type=datetime], input[type=email], input[type=month], input[type=number], input[type=password], input[type=radio], input[type=search], input[type=tel], input[type=text], input[type=time], input[type=url], input[type=week], select, textarea, .form-control {
            border: none !important;
        }

        .wid-task-header .wid-icon {
            float: left;
        }

            .wid-task-header .wid-icon i {
                font-size: 30px;
                color: #fff;
                margin: 3px 15px 0 0;
            }

        .wid-task-header .wid-text h4 {
            color: #ffffff;
            display: block;
            margin: 0px 0 0px 0;
            font-weight: 700;
        }

        .wid-task-header .wid-text span {
            display: block;
            color: #e1e1e1;
            white-space: nowrap;
        }

        .wid-all-tasks {
            display: block;
            color: #ffffff;
            margin: 15px 0;
        }

            .wid-all-tasks ul {
                overflow: hidden;
                position: relative;
                max-height: 297px;
            }

        .icheckbox_minimal-white.checked {
            background-position: -40px 0;
        }

        .wid-all-tasks ul li.checked label {
            color: #dddddd;
        }

        .wid-all-tasks ul li label:after {
            content: " ";
            height: 1px;
            background-color: transparent;
            position: relative;
            top: -11px;
            width: 100%;
            display: block;
        }

        .icheckbox_minimal-white, .iradio_minimal-white {
            display: inline-block;
            *display: inline;
            vertical-align: middle;
            margin: 0;
            padding: 0;
            width: 18px;
            height: 18px;
            background: url(/assets/images/white.png) no-repeat;
            border: none;
            cursor: pointer;
        }

        .wid-task-header .wid-text h4 {
            color: #ffffff;
            font-size: 24px !important;
            display: block;
            margin: 0px 0 0px 0;
            font-weight: 700;
        }

        .ultra-todo-task {
            padding: 30px;
            margin-bottom: 30px;
        }

        .small, small {
            font-size: 85%;
            color: #e1ded3;
        }

        [class*="icheckbox_"], [class*="iradio_"] {
            margin-right: 8px;
        }

        .wid-add-task {
            margin-top: 15px;
        }

        input[type=checkbox], input[type=color], input[type=date], input[type=datetime-local], input[type=datetime], input[type=email], input[type=month], input[type=number], input[type=password], input[type=radio], input[type=search], input[type=tel], input[type=text], input[type=time], input[type=url], input[type=week], select, textarea, .form-control {
            box-shadow: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            -ms-box-shadow: none;
            -o-box-shadow: none;
            border-radius: 0px;
            -o-border-radius: 0px;
            -ms-border-radius: 0px;
            -moz-border-radius: 0px;
            -webkit-border-radius: 0px;
            border: 1px solid #e1e1e1;
        }

        .panel-heading {

    border-bottom: 1px solid #e8e8e8;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
    text-align: center;
    color: #212529 !important;
    margin-top: 0px;
    font-size: 1.25rem;
    padding-top: 20px !important;
    font-weight: 700;
    padding: 15px;
        }
        .row.active i.fa{
    font-size: 25px;
}
        .tools a {
            margin-left: 10px;
            color: #a7a7a7;
            font-size: 12px;
        }

        .alert-success, .alert-danger, .alert-info, .alert-warning {
            border: none;
        }

        .alert-info {
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }

        .alert {
            padding: 10px;
            margin-bottom: 5px;
            margin-top: 5px;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-info .alert-icon {
            background-color: #99cce3;
        }

        .alert-icon {
            width: 40px;
            height: 40px;
            display: inline-block;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
        }

        .notification-info {
            margin-left: 56px;
            margin-top: -40px;
        }

        .notification-meta {
            margin-bottom: 3px;
            padding-left: 0;
            list-style: none;
            padding: 0px 5px;
        }

        .notification-info p {
            padding: 0px 5px;
        }

        .notification-sender {
            color: #414147;
        }

        .notification-time {
            font-style: italic;
            color: #999;
        }

        .alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .alert-success {
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
        }

        .alert-warning {
            color: #8a6d3b;
            background-color: #fcf8e3;
            border-color: #faebcc;
        }

        .alert-danger .alert-icon {
            background-color: #fcb1ae;
        }

        .alert-success .alert-icon {
            background-color: #98d7ad;
        }

        .widget {
            box-shadow: 0 2px 3px 0px #ccc;
        }

        .alert-warning .alert-icon {
            background-color: #ffe699;
        }

        .alert-icon i {
            width: 40px;
            height: 40px;
            display: block;
            text-align: center;
            line-height: 40px;
            font-size: 20px;
            color: #fff;
            font-family: 'FontAwesome';
        }

        .btn-todo-select button, .btn-add-task button {
            width: 100%;
            font-size: 12px;
        }

        .btn-default {
            background-color: #c7cbd6;
            border-color: #c7cbd6;
            color: #fff;
        }

            .btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .open .dropdown-toggle.btn-default {
                background-color: #b0b5b9;
                border-color: #b0b5b9;
                color: #fff;
            }

        .btn-primary {
            background-color: #4a88c2;
            border-color: #4a88c2;
            color: #FFFFFF;
        }

            .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
                background-color: #213e7d;
                border-color: #213e7d;
                color: #FFFFFF;
            }

        .todo-actionlist {
            position: absolute;
            right: 15px;
            top: 13px;
        }

            .todo-actionlist a {
                height: 24px;
                width: 24px;
                display: inline-block;
                float: left;
            }

        .to-do-list li {
            background: #f3f3f3;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            position: relative;
            padding: 13px;
            margin-bottom: 5px;
            cursor: pointer;
            list-style: none;
            margin-right: 10px;
        }

        .todo-search {
            -moz-border-radius: 3px !important;
            -webkit-border-radius: 3px !important;
            border-radius: 3px !important;
        }

        .search {
            -webkit-transition: all .3s ease;
            -moz-transition: all .3s ease;
            -ms-transition: all .3s ease;
            -o-transition: all .3s ease;
            transition: all .3s ease;
            border: 1px solid #fff;
            box-shadow: none;
            background: #f6f6f6 !important;
            padding: 10px;
            color: #e1e1e1;
            border-radius: 100px;
            -webkit-border-radius: 100px;
            height: 35px !important;
        }

        .pr-0 {
            padding-right: 0px !important;
        }

        .pl-0 {
            padding-left: 0px !important;
        }

        .search:focus {
            width: 100%;
        }

        .drag-marker i {
            height: 2px;
            width: 2px;
            display: block;
            background: #ccc;
            box-shadow: 5px 0 0 0px #ccc, 0px 5px 0 0px #ccc, 5px 5px 0 0px #ccc, 0px 10px 0 0px #ccc, 5px 10px 0 0px #ccc, 0px 15px 0 0px #ccc, 5px 15px 0 0px #ccc;
            -webkit-box-shadow: 5px 0 0 0px #ccc, 0px 5px 0 0px #ccc, 5px 5px 0 0px #ccc, 0px 10px 0 0px #ccc, 5px 10px 0 0px #ccc, 0px 15px 0 0px #ccc, 5px 15px 0 0px #ccc;
            -moz-box-shadow: 5px 0 0 0px #ccc,0px 5px 0 0px #ccc,5px 5px 0 0px #ccc,0px 10px 0 0px #ccc,5px 10px 0 0px #ccc,0px 15px 0 0px #ccc,5px 15px 0 0px #ccc;
        }

        .drag-marker {
            height: 17px;
            display: block;
            float: left;
            width: 7px;
            position: relative;
            top: 2px;
        }

        .todo-check {
            width: 20px;
            position: relative;
            margin-right: 10px;
            margin-left: 10px;
        }
        p.todo-title.line-through {
            padding-top: 4px;
        }
        .to-do-list li .todo-check input[type=checkbox] {
            cursor: pointer;
            position: absolute;
            width: 20px;
            height: 20px;
            top: 5px;
            left: 0px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
            border: #ccc 1px solid;
            margin-top: 0px;
        }

        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: 700;
        }
                    .card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-clip: border-box;

}

            .card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1rem;
}
        .todo-check label:after {
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            filter: alpha(opacity=0);
            opacity: 0;
            content: '; position: absolute;
            width: 13px;
            height: 8px;
            background: transparent;
            top: 3px;
            left: 3px;
            border: 3px solid #cfcfcf;
            border-top: none;
            border-right: none;
            -webkit-transform: rotate(-45deg);
            -moz-transform: rotate(-45deg);
            -o-transform: rotate(-45deg);
            -ms-transform: rotate(-45deg);
            transform: rotate(-45deg);
        }

        span.pull-right {
            line-height: 0.6;
        }
        li.p-3 {
    padding: 5px 0px;
}
        .todo-actionlist span i {
            height: 24px;
            width: 24px;
            display: inline-block;
            text-align: center;
            line-height: 24px;
            color: #ccc;
            cursor: pointer;
            padding-top:3px;
            padding-left:14px;
        }

        .btn-todo-select button i, .btn-add-task button i {
            padding-right: 10px;
        }



        button[disabled] {
            /*cursor: default;*/
            pointer-events: none;
            opacity: 0.5;
        }

            button[disabled]:hover {
            }

        .todo-check input[type=checkbox]:checked + input:after {
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
            filter: alpha(opacity=100);
            opacity: 1;
        }

        .todo-check input:after {
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            filter: alpha(opacity=0);
            opacity: 0;
            content: '; position: absolute;
            width: 13px;
            height: 8px;
            background: transparent;
            top: 3px;
            left: 3px;
            border: 3px solid #cfcfcf;
            border-top: none;
            border-right: none;
            -webkit-transform: rotate(-45deg);
            -moz-transform: rotate(-45deg);
            -o-transform: rotate(-45deg);
            -ms-transform: rotate(-45deg);
            transform: rotate(-45deg);
        }

        .rpwwt-post-author {
            font-size: 13px;
            text-transform: uppercase;
        }

        .rpwwt-post-date {
            font-size: 13px;
            color: #ababab;
            text-transform: uppercase;
        }

        .rpwwt-widget ul {
            list-style: outside none none;
            margin-left: 0;
            margin-right: 0;
            padding-left: 0;
            padding-right: 0;
        }

        .rpwwt-post-title {
            display: block;
            font-size: 13px;
            font-weight: bold;
            line-height: 1.5;
            letter-spacing: 0.5px;
            margin: 0 0 0.6em;
        }

        .clock {
            font-family: 'Orbitron', sans-serif;
            color: #fff;
            font-size: 40px;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            border:none;
        }

        .rpwwt-widget {
            overflow: auto;
            outline: none;
            height: 340px;
        }

            .rpwwt-widget ul li {
                padding: 5px 0px;
            }
            div#theCarousel .thumb.float-left {
    width: 50px !important;
    height: 50px !important;
}
            .morris-hover.morris-default-style {
    border-radius: 5px;
    padding: 10px 12px;
    background: #ffffff;
    border: none;
    font-family: "Rubik", sans-serif;
    -webkit-box-shadow: 0 5px 25px 5px rgba(0, 0, 0, 0.14);
    box-shadow: 0 5px 25px 5px rgba(0, 0, 0, 0.14);
}
            .morris-hover.morris-default-style .morris-hover-row-label {
    background-color: #2d3b48;
    color: #ffffff;
    padding: 4px;
    border-radius: 5px 5px 0 0;
    margin: -10px -12px 10px;
}

.morris-hover.morris-default-style .morris-hover-row-label {
    font-weight: bold;
text-align:center;
}
.morris-hover.morris-default-style .morris-hover-point {
    font-weight: 500;
    font-size: 14px;
    color: #2d3b48 !important;
}
.morris-hover.morris-default-style .morris-hover-point {
    white-space: nowrap;
    margin: 0.1em 0;
}

        .act-hour-p {
font-size: 14px !important; color: #fff; padding: 10px; margin:0px; font-weight:bold;
padding-bottom:5px;display: inline-block;
}
            .act-hour-p label{
font-size: 14px !important; color: #fff; padding: 0px 5px; margin:0px;
}
        @media only screen and (max-width: 1599px) and (min-width: 1201px) {
            .mini-stat {
                padding: 10px 11px;
                    border-radius: 10px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }
                        .mini-stat-info {
                font-size: 11px;
            }
            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }
                @media only screen and (max-width: 1349px) and (min-width: 1201px) {
            .mini-stat {
                padding:8px 8px;
                                    border-radius: 10px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }

            .mini-stat-icon {
                width: 40px;
                height: 40px;
                line-height: 33px;
            }
            img.text-danger.mr-1.pull-right.rounded-circle{
                left:-11px;
            }
            div#theCarousel .media-body {
    padding-left: 0px;
}
            .media-heading{
                font-size: 11px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
                            .mini-stat-info {
                font-size: 9px;
            }
        }
        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .mini-stat {
                padding: 8px 8px;
                                    border-radius: 10px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }
            .mini-stat-info span {
                font-size: 20px;
            }
            .mini-stat-icon {
                width: 40px;
                height: 40px;
                line-height: 33px;
            }
            .mini-stat-info {
                font-size: 9px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }
        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            .mini-stat {
                padding: 10px 11px;
                                    border-radius: 10px;
            }
            .alert {
                padding: 7px 5px;
            }
            .panel-heading{
                font-size:1.2rem;
            }
            img.text-danger.mr-1.pull-right.rounded-circle{
                left:0%;
            }
            .rpwwt-post-title {
                display: block;
                font-size: 10px;
                font-weight: bold;
                line-height: 1.2;
            }
             .notification-info {
    margin-left: 0px;
    margin-top: 0px;
}
            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }
            .mini-stat-info {
                font-size: 9px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
                .btn{
                    padding:8px 6px;
                }
                .profile-nav .user-heading.clock-row{
                    margin-bottom:11px;
                }
        }
        @media only screen and (max-width: 991px) and (min-width: 768px) {
            .mini-stat {
                padding: 10px 11px;
                                    border-radius: 10px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }

        }    
        .rpwwt-widget a {
    color: #767676;
}
        section.app-content{
            margin-bottom:25px;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <%--<asp:UpdatePanel ID="updpnl" runat="server">
            <ContentTemplate>--%>
        <main id="app-main" class="app-main">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <h1 class="m-0 text-dark">Employee Self Service</h1>
                                </div>

                                <!-- /.col -->
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                  
                                    <asp:HiddenField ID="hdSec" Value="0" runat="server" />
                                    <asp:HiddenField ID="timer_is_on" Value="1" runat="server" />

                                    <asp:HiddenField ID="hdBreakInSec" Value="0" runat="server" />
                                    <asp:HiddenField ID="timer_is_on_BreakIn" Value="0" runat="server" />

                                    <asp:HiddenField ID="hdWorkHoursSec" Value="0" runat="server" />

                                    <div class="modal fade M_set" id="myModal" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h1 class="m-0 text-dark">Late Reason</h1>
                                                    <div class="add_new">
                                                        <button type="button" class="AD_btn" runat="server" id="btnSubmit" onserverclick="btnSubmit_ServerClick">Submit</button>
                                                        <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                                    </div>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        <textarea id="txtReason" runat="server" rows="5" placeholder="Reason (max 100 characters).." maxlength="100" class="form-control"></textarea>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="wrap">
                <section class="app-content">
                    <div class="row active">
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail-Present"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon tar">
                                        <i class="fa fa-calendar-check-o" aria-hidden="true"></i> </span>
                                    <div class="mini-stat-info">
                                        <span data-to="0" data-speed="1000">
                                            <asp:Literal ID="lblPresent" runat="server"></asp:Literal>
                                        </span>
                                        Present Days
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail-Absent"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon orange">
                                        <i class="fa fa-calendar-times-o" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info">
                                        <span data-to="7" data-speed="1500">
                                            <asp:Literal ID="lblAbsent" runat="server"></asp:Literal>
                                        </span>
                                        Absent Days
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail-Leaves"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon pink">
                                        <img src="/assets/images/leave.png"></span>
                                    <div class="mini-stat-info">
                                        <span data-to="0" data-speed="1000">
                                            <asp:Literal ID="lblLeave" runat="server"></asp:Literal>
                                        </span>
                                        Leaves
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail-Holiday"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon purple">
                                        <i class="fa fa-calendar" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info">
                                        <span data-to="0" data-speed="1000">
                                            <asp:Literal ID="lblHalfDay" runat="server"></asp:Literal>
                                        </span>
                                        Holidays
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail-MissingPunches"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon yellow">
                                        <img src="/assets/images/Missing-punches.png"></span>
                                    <div class="mini-stat-info">
                                        <span data-to="0" data-speed="1000">
                                            <asp:Literal ID="lblMissingPunches" runat="server"></asp:Literal>
                                        </span>
                                        Missing Punches
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail-ShortDurations"); %>">
                                <div class="mini-stat clearfix">
                                    <span class="mini-stat-icon green">
                                        <img src="/assets/images/short-duration.png"></span>
                                    <div class="mini-stat-info">
                                        <span data-to="0" data-speed="1000">
                                            <asp:Literal ID="lblEarlyOut" runat="server"></asp:Literal>
                                        </span>
                                        Short Hours
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4">
                            <div class="widget">
                                <header class="panel-heading">
                                    Active Hours <span class="tools pull-right"></span>
                                </header>
                                <div class="widget-body" style="height: 245px; padding: 0; padding-top: 10px; padding-bottom: 15px;">
                                    <div class="stopwatch">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="time clock" id="display" style="display: block; padding-top: 20px; padding-bottom: 0px; width: 100%">
                                                    <label runat="server" id="txtWorkHoursH">0</label>:<label runat="server" id="txtWorkHoursM">0</label>:<label runat="server" id="txtWorkHours">0</label>
                                                    <br />
                                                </span>
                                            </div>
                                            <div style="width: 100%;">
                                                <p class="act-hour-p">
                                                    Time In:  
                                                    <label id="lblStartTime" runat="server" />
                                                </p>
                                                <p class="act-hour-p" id="divBreakStart">
                                                    Break Start:     
                                                    <label id="lblBreakStart" runat="server" text="-" />
                                                </p>
                                                <p class="act-hour-p">
                                                    Time Out:     
                                                    <label id="lblEndTime" runat="server" text="-" />
                                                </p>
                                                <p class="act-hour-p">
                                                    Rem Break Time: 
                                                     <label id="lblRemainingBreakTime" runat="server" text="60 M" />
                                                </p>

                                                <%-- <div class="row">
                                                    <div class="col-md-12">
                                                        
                                                    </div>
                                                    <div class="col-md-12">
                                                        Time Out
                                                        
                                                    </div>
                                                    <div class="col-md-12">
                                                        
                                                    </div>
                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="widget">
                                <header class="panel-heading">
                                    Announcements <span class="tools pull-right"></span>
                                </header>
                                <section class="panel" style="padding-right: 10px; padding-bottom: 10px;">
                                    <div class="panel-body" style="position: relative;     background: #e8e8e8; overflow: hidden; overflow: auto; outline: none; height: 235px;">
                                        <asp:Repeater ID="RptrNotifications" runat="server" OnItemDataBound="RptrNotifications_ItemDataBound">
                                            <ItemTemplate>
                                                <div id="notDiv" runat="server">
                                                    <span class="alert-icon"><i class="fa fa-bell-o"></i></span>
                                                    <div class="notification-info" style="overflow: auto; outline: none;">
                                                        <ul class="clearfix notification-meta">
                                                            <li class="pull-left notification-sender"><span><%--<a href="#">Jonathan Smith</a>--%></span> <%# Eval("AnnouncementTitle") %> </li>
                                                            <li class="pull-right notification-time"><%# Eval("Date") %> </li>
                                                        </ul>
                                                        <p>
                                                            <%# Eval("Description") %>
                                                        </p>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </section>
                                <div class="event-calendar clearfix" style="display: none;">
                                    <div class="col-lg-12 col-md-12 col-sm-8 event-list-block">
                                        <div class="cal-day">Announcements</div>
                                        <div class="slimScrollDiv-on" style="overflow: hidden; width: auto; height: 191px !important; overflow: auto; outline: none; height: 240px;">
                                            <asp:GridView ID="gvAnnouncement" runat="server" CssClass="table table-bordered  dataTable no-footer" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Announcement Date">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblCol6" Text='<%# Eval("Date") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Title">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("AnnouncementTitle") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Description")  %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                            <ul class="event-list" style="overflow: hidden; width: auto; height: 305px;">
                                                <table class="table" style="display: none;">
                                                    <tbody>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Announcement Date</th>
                                                            <th>Title</th>
                                                            <th>Description</th>
                                                        </tr>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>30-Aug-2019</td>
                                                            <td>Lorem Ipsum</td>
                                                            <td>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</td>
                                                        </tr>

                                                        <tr>
                                                            <td>1</td>
                                                            <td>30-Aug-2019</td>
                                                            <td>Lorem Ipsum</td>
                                                            <td>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</td>
                                                        </tr>


                                                    </tbody>
                                                </table>
                                            </ul>
                                            <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 305px;"></div>
                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="widget">
                                <div class="profile-nav alt">
                                <section class="panel">
                                    <div class="user-heading alt clock-row terques-bg">
                                        <h1 runat="server" id="hMonthDay">December 14</h1>
                                        <p class="text-left" runat="server" id="hYearDay">2014, Friday</p>
                                        <p style="display:none;" class="text-left FromTime" runat="server" id="hTime">7:53 PM</p>

                                        <input type="hidden" value="17" id="SysClockHor" runat="server" />
                                        <input type="hidden" value="32" id="SysClockMin" runat="server" />
                                        <input type="hidden" value="00" id="SysClockSec" runat="server" />
                                    </div>
                                    <ul id="jqclock" class="jqclock">
                                        <li id="sec" style="transform: rotate(240deg);"></li>
                                        <li id="hour" style="transform: rotate(498.5deg);"></li>
                                        <li id="min" style="transform: rotate(222deg);"></li>
                                    </ul>
                                </section>

                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">

                            <div class="panel">
                                <div class="card  m-b-20">
                                <header class="panel-heading">
                                    Birthdays <span class="tools pull-right"></span>
                                </header>
                                <div class="card-body new-user" id="theCarousel" runat="server">
                                    <div id="boxscroll2" style="margin-bottom: 10px;">
                                        <ul class="list-unstyled mb-0 pr-3" tabindex="1" style="overflow: auto; outline: none;">
                                            <asp:Repeater ID="repBirthday" runat="server">
                                                <ItemTemplate>
                                                    <li class="p-3">
                                                        <div class="media">
                                                            <div class="thumb float-left">
                                                                <a href="#">
                                                                    <img src="<%# Eval("EmployeePhoto") %>" style="width: 50px;"><%--<img class="rounded-circle" src="/assets/images/profile.png">--%></a>
                                                            </div>
                                                            <div class="media-body">
                                                                <p class="media-heading mb-0">
                                                                    <strong><%# Eval("EmployeeName") %></strong>
                                                                    <img class="text-danger mr-1 pull-right rounded-circle" src="/assets/images/crown.png">
                                                                </p>
                                                                <small class="pull-right text-muted"><%# Convert.ToDateTime(Eval("DOB")).ToString("MMMM dd") %></small>
                                                                <small class="text-muted"><%# Eval("City") %></small>

                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="widget map_radius announcment_area" style="height: 245px; display: none;">
                                <header class="widget-header">
                                    <h4 class="widget-title">Birthday</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-6">
                                            <div class="carousel slide multi-item-carousel">
                                                <div runat="server" id="divBtnArrows">
                                                    <a class="left carousel-control" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                                    <a class="right carousel-control" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <section class="panel">
                                <header class="panel-heading">
                                    To Do List <span class="tools pull-right"></span>
                                </header>
                                <div class="panel-body">
                                    <div class="slimScrollDiv-on" style="position: relative; overflow: hidden; width: auto; height: 310px;">
                                        <ul class="to-do-list ui-sortable" id="sortable-todo" style="overflow: hidden; width: auto; height: 300px; margin-bottom: 10px; overflow: auto; outline: none;">
                                        </ul>
                                        <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px;top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 247.253px;"></div>
                                        <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                    <div class="todo-action-bar">
                                        <div class="row">
                                            <div class="col-xs-8 col-sm-6 todo-search-wrap pr-0">
                                                <input type="text" id="txtTodoDescription" class="form-control search" style="color: #767676;" placeholder="To Do Task">
                                            </div>
                                            <div class="col-xs-4 col-sm-6 btn-add-task">
                                                <button type="button" onclick="AddToDoListItem();" class="btn btn-default btn-primary">Add Task</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-md-4">
                            <div class="widget">
                                <header class="panel-heading">
                                    News <span class="tools pull-right"></span>
                                </header>
                                <!-- .widget-header -->
                                <div class="widget-body">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-6">
                                            <div class="rpwwt-widget">
                                                <ul>

                                                    <%
                                                        List<RSSFeed> lst = Session["RSSFeedData"] as List<RSSFeed>;

                                                        lst.RemoveAt(0);
                                                        foreach (var item in lst)
                                                        {
                                                            Response.Write("<li>");
                                                            Response.Write("<div class='new' style='padding-top: 24px'>");
                                                            Response.Write("<a href = '" + item.Link + "' target = '_blank'>");
                                                            Response.Write("<img width='80px' height='80px' class= 'attachment-80x80 size-80x80 wp-post-image' src='" + item.image + "' alt ='testing' />");
                                                            Response.Write("</a>");
                                                            Response.Write("</div>");
                                                            Response.Write("<div style='margin-top: -48px; margin-left: 95px;'>");
                                                            Response.Write("<a href = '" + item.Link + "' target = '_blank'>");
                                                            Response.Write("<span class='rpwwt-post-title'>" + item.Title + "</span>");
                                                            Response.Write("</a>");
                                                            //Response.Write("<div class='rpwwt-post-author'>" + item.Title + "</div>");
                                                            //Response.Write("<div class='rpwwt-post-date'>" + item.PubDate + "</div>");
                                                            Response.Write("</div>");
                                                            Response.Write("</li>");
                                                        }



                                                    %>

                                                    <%--                                                    <li>
                                                        <div>
                                                            <a href="https://dessign.net/madd-magazine-theme/where-is-technology-and-mobile-taking-us/">
                                                                <img width="80" height="80" src="https://dessign.net/madd-magazine-theme/wp-content/uploads/2018/01/StockSnap_F7AEBU9901-150x150.jpg" class="attachment-80x80 size-80x80 wp-post-image" alt="" loading="lazy" />
                                                            </a>
                                                        </div>
                                                        <div style="margin-top: -85px; margin-left: 95px;">
                                                            <span class="rpwwt-post-title">Where is Technology and Mobile Taking Us</span>
                                                            <div class="rpwwt-post-author">By madd</div>
                                                            <div class="rpwwt-post-date">January 23, 2018</div>
                                                        </div>
                                                    </li>--%>
                                                    <%--<li>
                                                        <div>
                                                            <a href="https://dessign.net/madd-magazine-theme/where-is-technology-and-mobile-taking-us/">
                                                                <img width="80" height="80" src="https://dessign.net/madd-magazine-theme/wp-content/uploads/2018/01/StockSnap_F7AEBU9901-150x150.jpg" class="attachment-80x80 size-80x80 wp-post-image" alt="" loading="lazy">
                                                            </a>
                                                        </div>
                                                        <div style="margin-top: -85px; margin-left: 95px;">
                                                            <span class="rpwwt-post-title">Where is Technology and Mobile Taking Us</span>
                                                            <div class="rpwwt-post-author">By madd</div>
                                                            <div class="rpwwt-post-date">January 23, 2018</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>
                                                            <a href="https://dessign.net/madd-magazine-theme/where-is-technology-and-mobile-taking-us/">
                                                                <img width="80" height="80" src="https://dessign.net/madd-magazine-theme/wp-content/uploads/2018/01/StockSnap_F7AEBU9901-150x150.jpg" class="attachment-80x80 size-80x80 wp-post-image" alt="" loading="lazy">
                                                            </a>
                                                        </div>
                                                        <div style="margin-top: -85px; margin-left: 95px;">
                                                            <span class="rpwwt-post-title">Where is Technology and Mobile Taking Us</span>
                                                            <div class="rpwwt-post-author">By madd</div>
                                                            <div class="rpwwt-post-date">January 23, 2018</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>
                                                            <a href="https://dessign.net/madd-magazine-theme/where-is-technology-and-mobile-taking-us/">
                                                                <img width="80" height="80" src="https://dessign.net/madd-magazine-theme/wp-content/uploads/2018/01/StockSnap_F7AEBU9901-150x150.jpg" class="attachment-80x80 size-80x80 wp-post-image" alt="" loading="lazy">
                                                            </a>
                                                        </div>
                                                        <div style="margin-top: -85px; margin-left: 95px;">
                                                            <span class="rpwwt-post-title">Where is Technology and Mobile Taking Us</span>
                                                            <div class="rpwwt-post-author">By madd</div>
                                                            <div class="rpwwt-post-date">January 23, 2018</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>
                                                            <a href="https://dessign.net/madd-magazine-theme/where-is-technology-and-mobile-taking-us/">
                                                                <img width="80" height="80" src="https://dessign.net/madd-magazine-theme/wp-content/uploads/2018/01/StockSnap_F7AEBU9901-150x150.jpg" class="attachment-80x80 size-80x80 wp-post-image" alt="" loading="lazy">
                                                            </a>
                                                        </div>
                                                        <div style="margin-top: -85px; margin-left: 95px;">
                                                            <span class="rpwwt-post-title">Where is Technology and Mobile Taking Us</span>
                                                            <div class="rpwwt-post-author">By madd</div>
                                                            <div class="rpwwt-post-date">January 23, 2018</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>
                                                            <a href="https://dessign.net/madd-magazine-theme/where-is-technology-and-mobile-taking-us/">
                                                                <img width="80" height="80" src="https://dessign.net/madd-magazine-theme/wp-content/uploads/2018/01/StockSnap_F7AEBU9901-150x150.jpg" class="attachment-80x80 size-80x80 wp-post-image" alt="" loading="lazy">
                                                            </a>
                                                        </div>
                                                        <div style="margin-top: -85px; margin-left: 95px;">
                                                            <span class="rpwwt-post-title">Where is Technology and Mobile Taking Us</span>
                                                            <div class="rpwwt-post-author">By madd</div>
                                                            <div class="rpwwt-post-date">January 23, 2018</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>
                                                            <a href="https://dessign.net/madd-magazine-theme/where-is-technology-and-mobile-taking-us/">
                                                                <img width="80" height="80" src="https://dessign.net/madd-magazine-theme/wp-content/uploads/2018/01/StockSnap_F7AEBU9901-150x150.jpg" class="attachment-80x80 size-80x80 wp-post-image" alt="" loading="lazy">
                                                            </a>
                                                        </div>
                                                        <div style="margin-top: -85px; margin-left: 95px;">
                                                            <span class="rpwwt-post-title">Where is Technology and Mobile Taking Us</span>
                                                            <div class="rpwwt-post-author">By madd</div>
                                                            <div class="rpwwt-post-date">January 23, 2018</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>
                                                            <a href="https://dessign.net/madd-magazine-theme/where-is-technology-and-mobile-taking-us/">
                                                                <img width="80" height="80" src="https://dessign.net/madd-magazine-theme/wp-content/uploads/2018/01/StockSnap_F7AEBU9901-150x150.jpg" class="attachment-80x80 size-80x80 wp-post-image" alt="" loading="lazy">
                                                            </a>
                                                        </div>
                                                        <div style="margin-top: -85px; margin-left: 95px;">
                                                            <span class="rpwwt-post-title">Where is Technology and Mobile Taking Us</span>
                                                            <div class="rpwwt-post-author">By madd</div>
                                                            <div class="rpwwt-post-date">January 23, 2018</div>
                                                        </div>
                                                    </li>--%>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-lg-8 col-xl-8" style="display:none">
                            <div class="card m-b-30" >
                                <header class="panel-heading">
                                    Commissions & Bonus  <span class="tools pull-right"></span>
                                </header>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                                <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
                                <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
                                <link href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" rel="stylesheet" />

                                <div id="chart" style="background-color: #fff !important;"></div>
                            </div>
                        </div>
                    </div>
                </section>
<div class="clearfix">&nbsp;</div>
            </div>
            <!-- now no container fluid just rows  -->

            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>


        <!-- Resources -->

        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <!-- Chart code -->
        <!-- HTML -->

        <!-- Todo List Script Start -->
        <script>
            Morris.Line({
                element: 'chart',
                data: [
                    { period: '2016-05-10', park1: 200, park2: 200, park3: 50, park4: 10, park5: 0 },
                    { period: '2016-05-11', park1: 15, park2: 275, park3: 5, park4: 60, park5: 0 },
                    { period: '2016-05-12', park1: 80, park2: 20, park3: 30, park4: 30, park5: 0 },
                    { period: '2016-05-13', park1: 100, park2: 200, park3: 250, park4: 50, park5: 0 },
                    { period: '2016-05-14', park1: 50, park2: 60, park3: 20, park4: 10, park5: 0 },
                    { period: '2016-05-15', park1: 75, park2: 65, park3: 10, park4: 60, park5: 0 },
                    { period: '2016-05-16', park1: 175, park2: 95, park3: 110, park4: 30, park5: 0 },
                    { period: '2016-05-17', park1: 150, park2: 95, park3: 90, park4: 111, park5: 0 },
                    { period: '2016-05-18', park1: 120, park2: 95, park3: 60, park4: 47, park5: 0 },
                    { period: '2016-05-19', park1: 60, park2: 95, park3: 50, park4: 231, park5: 0 },
                    { period: '2016-05-20', park1: 10, park2: 95, park3: 100, park4: 80, park5: 0 }
                ],
                lineColors: ['#819C79', '#fc8710', '#FF6541', '#A4ADD3', '#766B56'],
                xkey: 'period',
                ykeys: ['park1', 'park2', 'park3', 'park4', 'park5'],
                labels: ['PARK 1', 'PARK 2', 'PARK 3', 'PARK 4', 'PARK 5'],
                xLabels: 'day',
                xLabelAngle: 45,
                xLabelFormat: function (d) {
                    var weekdays = new Array(7);
                    weekdays[0] = "SUN";
                    weekdays[1] = "MON";
                    weekdays[2] = "TUE";
                    weekdays[3] = "WED";
                    weekdays[4] = "THU";
                    weekdays[5] = "FRI";
                    weekdays[6] = "SAT";

                    return weekdays[d.getDay()] + '-' +
                        ("0" + (d.getMonth() + 1)).slice(-2) + '-' +
                        ("0" + (d.getDate())).slice(-2);
                },
                resize: true
            });
        </script>
        <script>
            function AddToDoListItem() {

                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/AddToDoListItem',
                    data: '{Description: ' + JSON.stringify($('#txtTodoDescription').val()) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        GetToDoListByUserID();
                        $('#txtTodoDescription').val('');
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }


            function UpdateToDoListItem() {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/UpdateToDoListItem',
                    data: '{ToDoListID: ' + JSON.stringify(id) + ',Description: ' + JSON.stringify(id) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        GetToDoListByUserID();
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }

            function DeleteToDoListItem(id) {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/DeleteToDoListItem',
                    data: '{ToDoListID: ' + JSON.stringify(id) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        GetToDoListByUserID();
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }

            function UpdateToDoListItemStatus(id) {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/UpdateToDoListItemStatus',
                    data: '{ToDoListID: ' + JSON.stringify(id) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        GetToDoListByUserID();
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }


            function GetToDoListByUserID() {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/GetToDoListByUserID',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var data = response.d;
                        var lstTodo = $("#sortable-todo");
                        lstTodo.empty();

                        $.each(data, function (key, data) {
                            if (data.Checked == "checked") {
                                lstTodo.append("<li class='clearfix '><div class='todo-check pull-left'><input type='checkbox' value='None' " + data.Checked + " onchange='UpdateToDoListItemStatus(" + data.ToDoListID + ")' id='todo-check" + data.ToDoListID + "'><label for='todo-check2" + data.ToDoListID + "'></label></div><p class='todo-title line-through'>" + data.Description + "</p><div class='todo-actionlist pull-right clearfix'><span id='close' onclick='DeleteToDoListItem(" + data.ToDoListID + ")' class='todo-remove'><i class='fa fa-times' aria-hidden=true></i></span></div></li>");
                            } else {
                                lstTodo.append("<li class='clearfix'><div class='todo-check pull-left'><input type='checkbox' value='None' " + data.Checked + " onchange='UpdateToDoListItemStatus(" + data.ToDoListID + ")' id='todo-check" + data.ToDoListID + "'><label for='todo-check2" + data.ToDoListID + "'></label></div><p class='todo-title'>" + data.Description + "</p><div class='todo-actionlist pull-right clearfix'><span id='close' onclick='DeleteToDoListItem(" + data.ToDoListID + ")' class='todo-remove'><i class='fa fa-times' aria-hidden=true></i></span></div></li>");
                            }
                        })

                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }

        </script>

        <!-- Todo List Script End -->


        <!-- Attendance Script Start -->

        <script>


            function checkStatus() {
                $("#btnTimeOut").prop("disabled", true);
                $("#btnTimeIN").prop("disabled", true);
                $("#btnBreakTimeOut").prop("disabled", true);
                $("#btnBreakTimeIN").prop("disabled", true);
               // console.log($('#hdWorkHoursSec').val());
                $('#timer_is_on').val('0');
                $('#timer_is_on_BreakIn').val('0');
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/checkStatus',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var data = response.d;
                        $.each(data, function (key, data) {
                            if (data.BtnStatus == 'OnHoliday') {
                                $("#btnTimeIN").prop("disabled", false);
                            }
                            else if (data.BtnStatus == 'Off') {
                                $("#btnTimeIN").prop("disabled", false);
                            }                           
                            else if (data.BtnStatus == 'None') {
                                $('#hdWorkHoursSec').val(data.WorkHoursSec);
                                if (data.TimeIN != '0') {
                                    $('#lblStartTime').text(data.StartTime);
                                    $('#lblRemainingBreakTime').text(data.RemainingBreakTime);
                                    $('#lblEndTime').text(data.EndTime);
                                    $('#divBreakStart').css("display", "none");
                                    //$('#lblEndTime').text(data.EndTime);
                                }
                            }
                            else if (data.BtnStatus == 'TimeOut') {
                                debugger;
                                $("#btnTimeOut").prop("disabled", false);
                                $("#btnBreakTimeOut").prop("disabled", false);
                                $('#hdWorkHoursSec').val(data.WorkHoursSec);
                                $('#timer_is_on').val('1');
                                $('#lblStartTime').text(data.StartTime);
                                $('#lblRemainingBreakTime').text(data.RemainingBreakTime);
                                $('#divBreakStart').css("display", "none");
                            }
                            else if (data.BtnStatus == 'BreakEndTime') {
                                $("#btnBreakTimeIN").prop("disabled", false);
                                $("#btnBreakTimeOut").prop("disabled", true);
                                $('#hdWorkHoursSec').val(data.WorkHoursSec);
                                $('#lblStartTime').text(data.StartTime);
                                $('#lblRemainingBreakTime').text(data.RemainingBreakTime);
                                $('#lblBreakStart').text(data.BreakStartTime);
                                $('#divBreakStart').css("display", "");
                            }
                            else if (data.BtnStatus == 'BreakTime') {
                                debugger;
                                $("#btnBreakTimeOut").prop("disabled", false);
                                $("#btnTimeOut").prop("disabled", false);
                                $('#hdWorkHoursSec').val(data.WorkHoursSec);
                                $('#timer_is_on').val('1');
                                $('#lblStartTime').text(data.StartTime);
                                $('#lblRemainingBreakTime').text(data.RemainingBreakTime);
                                $('#divBreakStart').css("display", "none");
                            }
                            else if (data.BtnStatus == 'TimeIn') {
                                $("#btnTimeIN").prop("disabled", false);
                                $('#divBreakStart').css("display", "none");
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }
            
            function TimeIN() {
                debugger;
                if(confirm("Do you want to proceed?") == false)
                {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/TimeIN',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        checkStatus();
                        alert(response.d);
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }

            function BreakTimeOut() {
                if (confirm("Do you want to proceed?") == false) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/BreakTimeOut',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        debugger;
                        checkStatus();
                        alert(response.d);
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }

            function BreakTimeIN() {
                if (confirm("Do you want to proceed?") == false) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/BreakTimeIN',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        debugger;
                        checkStatus();
                        alert(response.d);
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }

            function TimeOut() {
                if (confirm("Do you want to proceed?") == false) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/TimeOut',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        checkStatus();
                        alert(response.d);
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }

            function whCount() {
                if (document.getElementById("timer_is_on").value == '1') {
                    var c = parseInt(document.getElementById("hdWorkHoursSec").value);
                   // console.log(c);
                    document.getElementById("txtWorkHoursH").innerHTML = Math.floor(c / 3600);
                    document.getElementById("txtWorkHoursM").innerHTML = Math.floor((c / 60) % 60);
                    document.getElementById("txtWorkHours").innerHTML = Math.floor(c % 60);
                    c = c + 1;
                    document.getElementById("hdWorkHoursSec").value = c;

                }
                else {
                    var c = parseInt(document.getElementById("hdWorkHoursSec").value);
                    document.getElementById("txtWorkHoursH").innerHTML = Math.floor(c / 3600);
                    document.getElementById("txtWorkHoursM").innerHTML = Math.floor((c / 60) % 60);
                    document.getElementById("txtWorkHours").innerHTML = Math.floor(c % 60);
                }

                timeOutwhCount = setTimeout(whCount, 1000);

            }

            function pageLoad(sender, args) {
                checkStatus();
                whCount();
                GetToDoListByUserID();
            }

        </script>

        <!-- Attendance Script End-->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
        <%--clock--%>
        <script>
            $(function () {
                setInterval(function () {
                    var seconds = parseInt($('#SysClockSec').val());
                    var sdegree = 0;
                    if (seconds < 60) {
                        sdegree = seconds * 6;
                    }
                 //   console.log('sys ' + $('#SysClockHor').val() + ':' + $('#SysClockMin').val() + ':' + seconds);
                    var srotate = "rotate(" + sdegree + "deg)";
                    seconds = seconds + 1;
                    //console.log(new Date().getSeconds());
                    //console.log(seconds);
                    if (seconds >= 60) {
                        //  console.log('aaaaa');
                        $('#SysClockSec').val('0');
                        var min = parseInt($('#SysClockMin').val()) + 1;
                        $('#SysClockMin').val(min);
                        if (min >= 60) {
                            var hor = parseInt($('#SysClockHor').val()) + 1;
                            if (min >= 24) {
                                $('#SysClockHor').val('0');
                            }
                        }
                    }
                    else {
                        $('#SysClockSec').val(seconds);
                    }
                    $("#sec").css({ "-moz-transform": srotate, "-webkit-transform": srotate });
                    var hour = $('#SysClockHor').val();
                    var tt = "AM";
                    if (hour > 12) {
                        hour = hour - 12;
                        tt = "PM";
                    }
                    $('#hTime').html(hour + ':' + $('#SysClockMin').val() + '  ' + tt);
                }, 1000);
                setInterval(function () {
                    var mins = parseInt($('#SysClockMin').val());
                    var hours = parseInt($('#SysClockHor').val());
                    var hdegree = hours * 30 + (mins / 2);
                    var hrotate = "rotate(" + hdegree + "deg)";
                    $("#hour").css({ "-moz-transform": hrotate, "-webkit-transform": hrotate });
                }, 1000);
                setInterval(function () {
                    var mins = parseInt($('#SysClockMin').val());
                    var mdegree = mins * 6;
                    var mrotate = "rotate(" + mdegree + "deg)";
                    $("#min").css({ "-moz-transform": mrotate, "-webkit-transform": mrotate });
                }, 1000);
            });
        </script>
        <%--to do list--%>
        <script>
            window.onload = function () {
                document.getElementById('close').onclick = function () {
                    this.parentNode.parentNode.parentNode
                        .removeChild(this.parentNode.parentNode);
                    return false;
                };
            };

            $('.todo-check input').click(function () {
                $(this).parents('li').children('.todo-title').toggleClass('line-through');
            });
        </script>
            
    </form>
</body>


</html>
