﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="Technofinancials.business.Details" %>


<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Employment Details</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4" style="display:none;">
                                <div style="text-align: right;">
                                    <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>


                <section class="app-content">



                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-12  form-group">
                                    <h4><strong>Employment Details</strong></h4>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4 class="control-label">Employment Type</h4>
                                    <asp:DropDownList ID="ddlEmploymentType" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">--- Please Select ---</asp:ListItem>
                                        <asp:ListItem Value="Temporary">Temporary</asp:ListItem>
                                        <asp:ListItem Value="Permanant">Permanent</asp:ListItem>
                                        <asp:ListItem Value="Contractor">Contractor</asp:ListItem>
                                        <asp:ListItem Value="Adhoc">Adhoc</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Date of Joining</h4>
                                        <input type="text"  class="form-control" name="dateOfJoining" id="txtDOJ" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4 class="control-label">Job Description</h4>
                                    <asp:DropDownList ID="ddlJD" runat="server" CssClass="form-control " AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4 class="control-label">Grade</h4>
                                    <asp:DropDownList ID="ddlGrades" runat="server" CssClass="form-control " AutoPostBack="true" OnSelectedIndexChanged="ddlGrades_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>

                                <div class="col-sm-6 form-group">
                                    <h4>Department<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlDept" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                    <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control " ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Designation<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDesg" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                    <asp:DropDownList ID="ddlDesg" runat="server" CssClass="form-control ">
                                    </asp:DropDownList>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                                                                                <div class="col-sm-12  form-group">
                                    <h4><strong>Direct Reporting To</strong></h4>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Department</h4>
                                    <asp:DropDownList ID="ddlDirectReportToDept" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDirectReportToDept_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Designation</h4>
                                    <asp:DropDownList ID="ddlDirectreportTo" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <div class="form-group">
                                        <h4>Person</h4>
                                    </div>
                                    <asp:DropDownList ID="ddlDirectReportingPerson" runat="server" ClientIDMode="Static" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                         <div class="row">
                                                             <div class="col-sm-12 form-group">
                                    
                                    <h4><strong>Indirect Reporting To</strong></h4>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Department</h4>
                                    <asp:DropDownList ID="ddlInDirectReportToDept" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlInDirectReportToDept_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Designation</h4>
                                    <asp:DropDownList ID="ddlInDirectreportTo" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <div class="form-group">
                                        <h4>Person</h4>
                                    </div>
                                    <asp:DropDownList ID="ddlInDirectReportingPerson" runat="server" ClientIDMode="Static" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
                                </div>
                             <div class="col-md-12">
                                 <div class="form-group" id="divAlertMsg" runat="server">
                                     <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                         <span>
                                             <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                         </span>
                                         <p id="pAlertMsg" runat="server">
                                         </p>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <%--<div class="content-header second_heading" style="padding-left:0px;">
                        <div class="container-fluid" style="padding-left:0px;">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark" style="margin-left:0px !important;">Accountabilities and Responsibilities</h1>
                                </div>
                                <!-- /.col -->

                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>--%>
                    <%--<div class="row">

                        <div class="col-lg-8">

                            <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblKPIType" Text='<%# Eval("KPIType") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblDesc" Text='<%# Eval("KPIDesc") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>--%>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    <style>
        @media only screen and (max-width: 1599px) and (min-width: 1201px) {
            .form-control{
                             font-size:11px !important;
                         }
                        .select2-container{
                             font-size:11px !important;
                         }
        }
    </style>
</body>
</html>
