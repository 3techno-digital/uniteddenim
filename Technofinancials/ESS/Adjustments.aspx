﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Adjustments.aspx.cs" Inherits="Technofinancials.ESS.Adjustments" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        .lblLateReasonFalse {
            display: none;
        }

        .txtLateReasonTrue {
            display: none;
        }

        .btnUpdateTrue {
            display: none;
        }
        
        .btnAdjustFalse {
            display: none;
        }
        button#btnSerach{
            padding: 3px 14px !important;
            margin-top: 5px !important;
        }
    </style>
</head>

<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <%--<asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>

            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h1 class="m-0 text-dark">Time In-Out Report</h1>
                            </div>
                            <!-- /.col -->

                            <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                <ContentTemplate>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="pull-right">
                                            <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <%--<Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>--%>
                            </asp:UpdatePanel>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <section class="app-content">

                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <h4>Month</h4>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtMonth" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtMonth" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
                                            </div>
                                        </div>

                                        <div class="col-md-5" >
                                            <div class="form-group">
                                                <h4>Employee </h4>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlEmployee" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="margin-top: 34px;">
                                            <button class="AD_btn_inn" id="btnSerach" runat="server" onserverclick="btnSerach_ServerClick" validationgroup="btnValidate" type="button">View</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group" id="divAlertMsg" runat="server">
                                                    <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                        <span>
                                                            <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                        </span>
                                                        <p id="pAlertMsg" runat="server">
                                                        </p>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">

                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" OnRowCommand="gv_RowCommand" OnRowDataBound="gv_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="AttendanceID">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblAttendanceID" Text='<%# Eval("AttendanceID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblDate" Text='<%# Eval("Date") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Login Time">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblLiTime" Text='<%# Eval("Login Time") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Logout Time">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblLoTime" Text='<%# Eval("Logout Time") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="On Leave">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblOnLeave" Text='<%# Eval("On Leave") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Resultant">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblResultant" Text='<%# Eval("Resultant") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Late / Over Time Reason" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblLateReason" Text='<%# Eval("Late / Over Time Reason") %>' CssClass='<%# "lblLateReason"+Eval("isUpdated").ToString()  %>'></asp:Label>
                                                    <asp:TextBox runat="server" ID="txtLateReason" placeholder="Late Reason" Text='<%# Eval("Late / Over Time Reason") %>' CssClass='<%# "form-control txtLateReason"+Eval("isUpdated").ToString()  %>'></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Edit" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="UpdateAdjustment" CommandArgument='<%# Container.DataItemIndex %>' CssClass='<%# "AD_stock btnUpdate"+Eval("isUpdated").ToString()  %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Adjust">
                                                <ItemTemplate>
                                                    <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/employee-self-service/manage/attendance-adjustment/edit-attendance-adjust-" + Eval("AttendanceID") %>" class="AD_stock" > Adjust </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>

                                </div>
                            </div>
                        </section>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer2" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="Scripts1" runat="server"></uc:Scripts>
    </form>
</body>
</html>




