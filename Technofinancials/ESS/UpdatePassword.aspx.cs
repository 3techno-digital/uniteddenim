﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Data;
using System.IO;

namespace Technofinancials.ESS
{
    public partial class UpdatePassword : System.Web.UI.Page
    {


        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                divAlertMsg.Visible = false;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {
            string returnMsg = "";
            if (Session["UserPassword"].ToString() == txtOldPassword.Value)
            {
                Session["UserPassword"] = txtNewPassword.Value;
                objDB.UserID = Convert.ToInt32(Session["UserID"]);
                objDB.Password = txtNewPassword.Value;
                returnMsg = objDB.UpdatePassword();
            }
            else
            {
                returnMsg = "Wrong Password";
            }

            if (returnMsg == "Password Updated")
            {
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                DataTable dt =  objDB.GetEmployeeByID(ref errorMsg);
                string Email = null;
                if (dt != null)
                {
                    Email= dt.Rows[0]["Email"].ToString(); 
                    string EmployeeName= dt.Rows[0]["EmployeeName"].ToString();
                 //   objDB.AccessLevel = "Directors";
                    sendEmail(EmployeeName, Email, txtNewPassword.Value.ToString());

                }

                Common.addlog("Update", "ESS", "User \"" + Session["UserName"] + "\" Password Updated", "Users", objDB.UserID);
                
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = returnMsg;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = returnMsg;
            }
        }

        private void sendEmail(string Name, string UserName, string Password)
        {
            string returnMsg = "";
            try
            {
                DBQueries objDB = new DBQueries();
                string errorMsg = "";
                string file = "";
                string html = "";

                file = System.Web.HttpContext.Current.Server.MapPath("/assets/emails/PasswordChanged.html");
                StreamReader sr = new StreamReader(file);
                FileInfo fi = new FileInfo(file);

                if (System.IO.File.Exists(file))
                {
                    html += sr.ReadToEnd();
                    sr.Close();
                }

              //  html = html.Replace("##CREATED_BY##", Session["UserName"].ToString());
                html = html.Replace("##NAME##", Name);
                html = html.Replace("##USER_NAME##", UserName);
                html = html.Replace("##USER_PASS##", Password);

                MailMessage mail = new MailMessage();
                mail.Subject = "User Account Password Changed from Technofinancials";
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), "Techno Financials");
                mail.To.Add(UserName);
                mail.Body = html;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["NoReplySMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NoReplyPort"].ToString()));
                smtp.EnableSsl = true;
                NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["NoReplyPassword"].ToString());
                smtp.Credentials = netCre;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                returnMsg = ex.Message;
            }

        }



    }
}