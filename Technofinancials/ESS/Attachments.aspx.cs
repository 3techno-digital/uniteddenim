﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class Attachments : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int EmpID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();

                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/dashboard";

                divAlertMsg.Visible = false;

                getData();

                //if (Session["EmployeeID"] != null)
                //{
                //    EmpID = Convert.ToInt32(Session["EmployeeID"].ToString());
                //    getAttachementsByEmployeeID(EmpID);

                //}
            }
        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }



        protected void getData() 
        {
            Common.addlog("ViewAll", "ESS", "Attachments Viewed", "");
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetAllApprovePolicyProcedureByCompanyID(ref errorMsg);
            gvFiles.DataSource = dt;
            gvFiles.DataBind();

            if (dt != null && dt.Rows.Count >0)
            {
                gvFiles.UseAccessibleHeader = true;
                gvFiles.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            
        }

        private void getAttachementsByEmployeeID(int employeeID)
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetAllApprovePolicyProcedureByCompanyID(ref errorMsg);
            dtFiles = new DataTable();
            dtFiles = createFiles();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtFiles.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["AttachmentTitle"],
                            dt.Rows[i]["AttachmentPath"]
                        });
                    }
                }
            }
            Common.addlog("ViewAll", "ESS", "Attachments Viewed", "");

            BindFilesTable();
        }



        private  DataTable dtFiles = new DataTable();
        protected  int FilesSrNo = 1;
        private DataTable createFiles()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("FilePath");
            dt.AcceptChanges();

            return dt;
        }
        protected void BindFilesTable()
        {
            if (dtFiles == null)
            {
                dtFiles = createFiles();
            }

            gvFiles.DataSource = dtFiles;
            gvFiles.DataBind();

            if (gvFiles.Rows.Count > 0)
            {
                gvFiles.UseAccessibleHeader = true;
                gvFiles.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }


    }
}