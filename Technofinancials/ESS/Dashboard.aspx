﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Technofinancials.ESS.Dashboard" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        .profile_image {
            height: 65px;
        }

        #countdown {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
            padding: 25px 0px;
        }

        .countdown-item.days {
            animation-delay: 1s;
        }

        .countdown-item.show {
            animation: fadein 2s forwards;
        }

        .countdown-item::before {
            position: absolute;
            content: ' ';
            width: 100px;
            height: 100px;
            border-radius: 50%;
            background-color: #343a40;
            left: 0;
            top: 0;
            opacity: .1;
        }

        .countdown-item.hours::after {
            content: attr(data-hours);
        }

        .countdown-item {
            font-size: 25px;
            border-radius: 50%;
            text-align: center;
            width: 100px;
            height: 100px;
            line-height: 100px;
            position: relative;
            margin: 5px;
            background-color: rgb(72 136 197);
            color: #fff;
        }

        .sticky-container {
            /* background-color: #333; */
            padding: 0px;
            margin: 0px;
            position: fixed;
            right: -60px;
            top: 130px;
            width: 100px;
        }

        .sticky li img {
            float: left;
            margin: 5px 5px;
            margin-right: 10px;
        }

        .sticky li p {
            padding: 0px;
            margin: 0px;
            text-transform: uppercase;
            line-height: 43px;
        }

        .sticky li {
            border-bottom-left-radius: 5px;
            border-top-left-radius: 5px;
            list-style-type: none;
            background-color: #efefef;
            color: #333;
            height: 43px;
            padding: 0px;
            margin: 0px 0px 1px 0px;
            -webkit-transition: all 0.25s ease-in-out;
            -moz-transition: all 0.25s ease-in-out;
            -o-transition: all 0.25s ease-in-out;
            transition: all 0.25s ease-in-out;
            cursor: pointer;
            filter: url(data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale);
            filter: gray;
            -webkit-filter: grayscale(100%);
        }

            .sticky li:hover {
                margin-left: -115px;
                /* background-color: #8e44ad; */
                filter: url(data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'1 0 0 0 0, 0 1 0 0 0, 0 0 1 0 0, 0 0 0 1 0\'/></filter></svg>#grayscale);
                -webkit-filter: grayscale(0%);
            }

        .widget {
            margin-bottom: 1rem;
        }

        span#lblCol3 {
            word-break: break-word;
        }

        .content-header h1 {
            margin-left: 8px !important;
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            .time h4 {
                font-size: 16px;
            }

            tbody td {
                max-width: 50px;
            }

            span label {
                font-size: 17px !important;
                padding: 0px 5px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }

            .tile-wrapper {
                margin-bottom: 1rem;
            }

            .copyright.pull-right {
                float: none !important;
                text-align: center;
            }

            .content-header h1 {
                font-size: 23px !important;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            .time h4 {
                font-size: 14px;
            }

            .countdown-item {
                width: 150px;
            }

            .content-header h1 {
                font-size: 23px !important;
            }

            .countdown-item::before {
                background-color: transparent;
            }

            .profile_image {
                height: 35px;
            }

            #countdown {
                flex-wrap: nowrap;
            }

            tbody td {
                max-width: 50px;
            }

            span label {
                font-size: 17px !important;
                padding: 0px 5px;
            }

            .pr_0 {
                padding-right: .75rem !important;
            }

            .tile-wrapper {
                margin-bottom: 1rem;
            }

            .copyright.pull-right {
                float: none !important;
                text-align: center;
            }

            .widget-body div#MyClockDisplay {
                font-size: 36px;
            }

            .d-md-block p {
                font-size: 10px;
            }

            .d-md-block {
                padding-top: 30px;
            }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .pr_0 {
                padding-right: .75rem !important;
            }

            .time h4 {
                font-size: 15px;
            }

            .content-header h1 {
                font-size: 23px !important;
            }

            .countdown-item::before {
                background-color: transparent;
            }

            #countdown {
                flex-wrap: nowrap;
            }

            .profile_image {
                height: 60px;
            }

            tbody td {
                max-width: 50px;
            }

            .tile-wrapper {
                margin-bottom: 1rem;
            }

            .d-md-block p {
                font-size: 10px;
            }

            span label {
                font-size: 19px !important;
            }

            .copyright.pull-right {
                float: none !important;
                text-align: center;
            }

            .widget-body div#MyClockDisplay {
                font-size: 42px;
            }
        }

        @media only screen and (max-width: 1439px) and (min-width: 1201px) {
            .text-center.ng-binding {
                font-size: 17px;
            }

            tbody td {
                max-width: 50px;
            }
        }

        .widget-body {
            padding-top: 0;
        }
        /*            .carousel-control.left , .carousel-control.right{
                      top:40px;
            }*/
        .calender {
        }

        .calenderHeading {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .calenderCell {
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        .wrap.p-t-0.tf-navbar.tf-footer-wrapper {
            right: 0 !important;
        }

        .boxes_row .widget-body {
            padding: 0 !important;
        }

        .pr_0 {
            padding-right: 0 !important;
        }

        .row.boxes_row {
            margin-bottom: 15px;
        }


        g[aria-labelledby="id-66-title"] {
            display: none;
        }

        #chartdiv {
            width: 100%;
            height: 500px;
        }

        .d-md-block p {
            margin-bottom: 0px;
            background: rgb(72,136,197, 0.4);
            color: #fff;
        }

        .d-md-block h5 {
            padding: 5px 5px;
            height: 40px;
        }

        .time {
            border-right: 1px solid #eee;
            height: 125px;
            padding: 0px 10px;
            text-align: center;
        }

        .d-md-block img {
            padding-top: 10px;
        }

        span#lblBreakStartEndTime, span#lblStartTime span#lblEndTime {
            font-size: 15px;
        }

        span label {
            font-size: 20px !important;
        }

        .carousel-wrap {
            margin: 90px auto;
            padding: 0 5%;
            width: 80%;
            position: relative;
        }

        /* fix blank or flashing items on carousel */
        .owl-carousel .item {
            position: relative;
            z-index: 100;
            -webkit-backface-visibility: hidden;
        }

        /* end fix */
        .owl-nav > div {
            margin-top: -26px;
            position: absolute;
            top: 50%;
            color: #cdcbcd;
        }

        .owl-nav i {
            font-size: 52px;
        }

        .owl-nav .owl-prev {
            left: -30px;
        }

        .owl-nav .owl-next {
            right: -30px;
        }

        a.left.carousel-control, .carousel-control.right {
            background: none;
        }

            a.left.carousel-control i, .carousel-control.right i {
                color: #343a40;
            }

        .d-md-block {
            display: block;
            text-align: center;
            border-radius: 10px;
            background-image: url(/assets/images/background-image.jpg);
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: initial;
            height: 160px;
            border: 2px solid #282828;
            background: #F0F0F0;
        }

        button[disabled] {
            /*cursor: default;*/
            pointer-events: none;
            opacity: 0.5;
        }

            button[disabled]:hover {
            }
    </style>

</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>

        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <%--<asp:UpdatePanel ID="updpnl" runat="server">
            <ContentTemplate>--%>
        <main id="app-main" class="app-main">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <h1 class="m-0 text-dark">Employee Self Service</h1>
                                </div>

                                <!-- /.col -->
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div style="text-align: right;">
                                        <%--<button class="AD_btn" id="btnTimeIN" runat="server" onserverclick="btnTimeIN_ServerClick" type="button">Time In</button>
                                        <button class="AD_btn" id="btnBreakTimeOut" runat="server" onserverclick="btnBreakTimeOut_ServerClick" type="button">Break Out</button>
                                        <button class="AD_btn" id="btnBreakTimeIN" runat="server" onserverclick="btnBreakTimeIN_ServerClick" type="button">Break In</button>
                                        <button class="AD_btn" id="btnTimeOut" runat="server" onserverclick="btnTimeOut_ServerClick" type="button">Time Out</button>--%>

                                        <button class="AD_btn" id="btnTimeIN" runat="server" disabled="disabled" onclick="TimeIN();" type="button">Time In</button>
                                        <button class="AD_btn" id="btnAway" runat="server" visible="false" disabled="disabled" onclick="BreakTimeOut();" type="button">Break Start</button>
                                        <button class="AD_btn" id="btnBreakTimeOut" runat="server" disabled="disabled" onclick="BreakTimeOut();" type="button">Break Start</button>
                                        <button class="AD_btn" id="btnActive" runat="server" visible="false" disabled="disabled" onclick="BreakTimeOut();" type="button">Break End</button>
                                        <button class="AD_btn" id="btnBreakTimeIN" runat="server" disabled="disabled" onclick="BreakTimeIN();" type="button">Break End</button>
                                        <button class="AD_btn" id="btnTimeOut" runat="server" disabled="disabled" onclick="TimeOut();" type="button">Time Out</button>

                                    </div>
                                    <asp:HiddenField ID="hdSec" Value="0" runat="server" />
                                    <asp:HiddenField ID="timer_is_on" Value="0" runat="server" />

                                    <asp:HiddenField ID="hdBreakInSec" Value="0" runat="server" />
                                    <asp:HiddenField ID="timer_is_on_BreakIn" Value="0" runat="server" />

                                    <asp:HiddenField ID="hdWorkHoursSec" Value="0" runat="server" />

                                    <div class="modal fade M_set" id="myModal" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h1 class="m-0 text-dark">Late Reason</h1>
                                                    <div class="add_new">
                                                        <%--<button type="button" class="AD_btn" runat="server" id="btnSubmit" onserverclick="btnSubmit_ServerClick">Submit</button>--%>
                                                        <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                                    </div>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        <textarea id="txtReason" runat="server" rows="5" placeholder="Reason (max 100 characters).." maxlength="100" class="form-control"></textarea>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="wrap">
                <section class="app-content">
                    <!-- now no container fluid just rows  -->
                    <div class="row boxes_row">
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <div class="widget-body">
                                <div class="summary">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">

                                        <div class="tile-wrapper count-up" style="background: linear-gradient(to right, #10c469, #1d976c); width: 100%;">
                                            <span class="diamond-wrapper bg-error dker text-center">
                                                <p class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="0" data-speed="1000">
                                                    <asp:Literal ID="lblPresent" runat="server"></asp:Literal>
                                                </p>
                                                <div class="text-center ng-binding">Presents</div>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <div class="widget-body">
                                <div class="summary">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">
                                        <div class="tile-wrapper count-up" style="background: linear-gradient(to right, #ec4c4c, #f04c4b, #f54d4b, #f94d4a, #fd4e49); width: 100%;">
                                            <span class="diamond-wrapper bg-error dker text-center">
                                                <p class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="7" data-speed="1500">
                                                    <asp:Literal ID="lblAbsent" runat="server"></asp:Literal>
                                                </p>
                                                <div class="text-center ng-binding">Absents</div>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <div class="widget-body">
                                <div class="summary">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">
                                        <div class="tile-wrapper count-up" style="background: linear-gradient(to right, #3c90cb, #4c98cf, #5a9fd3, #67a7d8, #74afdc); width: 100%;">
                                            <span class="diamond-wrapper bg-error dker text-center">
                                                <p class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="0" data-speed="1000">
                                                    <asp:Literal ID="lblLeave" runat="server"></asp:Literal>
                                                </p>
                                                <div class="text-center ng-binding">Leaves</div>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <div class="widget-body">
                                <div class="summary">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">

                                        <div class="tile-wrapper count-up" style="background-image: linear-gradient(to right, #3cb9cb, #56c4d3, #6bcfda, #7fdae3, #92e5eb); width: 100%;">
                                            <span class="diamond-wrapper bg-error dker text-center">
                                                <p class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="0" data-speed="1000">
                                                    <asp:Literal ID="lblHalfDay" runat="server"></asp:Literal>
                                                </p>
                                                <div class="text-center ng-binding">Holidays</div>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <div class="widget-body">
                                <div class="summary">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">

                                        <div class="tile-wrapper count-up" style="background-image: linear-gradient(to right, #8585eb, #8b82ee, #917ef1, #987af3, #a076f4); width: 100%;">
                                            <span class="diamond-wrapper bg-error dker text-center">
                                                <p class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="0" data-speed="1000">
                                                    <asp:Literal ID="lblMissingPunches" runat="server">0</asp:Literal>
                                                </p>
                                                <div class="text-center ng-binding">Missing Punches</div>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                            <div class="widget-body">
                                <div class="summary">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">

                                        <div class="tile-wrapper count-up" style="background: linear-gradient(315deg, #575757 0%, #94918d 74%); width: 100%;">
                                            <span class="diamond-wrapper bg-error dker text-center">
                                                <p class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="0" data-speed="1000">
                                                    <asp:Literal ID="lblEarlyOut" runat="server"></asp:Literal>
                                                </p>
                                                <div class="text-center ng-binding">Short Durations</div>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-lg-8 col-xs-12">
                            <div class="widget map_radius announcment_area">
                                <!-- <header class="widget-header">
                                            <h4 class="widget-title">Announcement
                                            </h4>
                                        </header>   -->
                                <!-- .widget-header -->
                                <!-- .widget-header -->
                                <header class="widget-header">
                                    <h4 class="widget-title">Daily Timer</h4>
                                </header>
                                <!-- .widget-header -->
                                <!-- .widget-header -->
                                <hr class="widget-separator" />
                                <div class="widget-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <%-- <div class="col-md- col-lg-4 col-sm-4 col-xs-12">
                                                <div class="time">
                                                    <h4>Time In</h4>
                                                    <span>
                                                        <label runat="server" id="txtH">0</label>
                                                        : 
                                        <label runat="server" id="txtM">0</label>
                                                        : 
                                        <label runat="server" id="txt">0</label>
                                                    </span></br>
                                                                        <div class="clearfix">&nbsp;</div>
                                                    <asp:Label ID="lblStartTime" runat="server"></asp:Label>
                                                </div>
                                            </div>--%>
                                            <%-- <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12" >
                                                <div class="time">
                                                    <h4>Break Time</h4>
                                                    <span>
                                                        <label runat="server" id="txtBreakInH">0</label>
                                                        : 
                                                                <label runat="server" id="txtBreakInM">0</label>
                                                        :
                                                                <label runat="server" id="txtBreakIn">0</label>
                                                    </span></br>
                                                                        <div class="clearfix">&nbsp;</div>
                                                    <asp:Label ID="lblBreakStartEndTime" runat="server"></asp:Label>
                                                </div>
                                            </div>--%>
                                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                                                <div class="time" style="border-right: 0px solid #eee;">
                                                    <h4>Working Time</h4>
                                                    <span>
                                                        <label runat="server" id="txtWorkHoursH">0</label>
                                                        : 
                                                              <label runat="server" id="txtWorkHoursM">0</label>
                                                        : 
                                                              <label runat="server" id="txtWorkHours">0</label>
                                                    </span></br>
                                                                        <div class="clearfix">&nbsp;</div>
                                                    <asp:Label ID="lblEndTime" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-lg-4 col-xs-12">
                            <div class="widget map_radius" style="height: 220px;">
                                <header class="widget-header">
                                    <h4 class="widget-title">System Time</h4>
                                </header>
                                <hr class="widget-separator" />
                                <div id="clock-pad" class="bg-grey" style="">
                                    <div class="row">
                                        <div class="widget-body">
                                            <div id="MyClockDisplay" class="clock" onload="showTime()"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- .widget -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
                            <div class="widget map_radius announcment_area" style="height: 245px;">
                                <header class="widget-header">
                                    <h4 class="widget-title">Birthday</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-6">
                                            <div class="carousel slide multi-item-carousel" id="theCarousel" runat="server">
                                                <div class="carousel-inner BirthdayRepeater">
                                                    <%--<div class="item">
                                                        <div class="col-md-4 col-sm-4 col-sx-12 col-lg-4">
                                                            <div class="d-md-block">
                                                                <img src="/assets/images/profile.png" style="width: 50px;">
                                                                <h5><strong>Afshan</strong></h5>
                                                                <p>Today Is Friday </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-md-4 col-sm-4 col-sx-12 col-lg-4">
                                                            <div class="d-md-block">
                                                                <img src="/assets/images/profile.png" style="width: 50px;">
                                                                <h5><strong>Afshan</strong></h5>
                                                                <p>Today Is Friday </p>
                                                            </div>
                                                        </div>
                                                    </div>--%>
                                                    <asp:Repeater ID="repBirthday" runat="server">
                                                        <ItemTemplate>
                                                            <div class="item">
                                                                <div class="col-md-4 col-sm-4 col-sx-12 col-lg-4">
                                                                    <div class="d-md-block">
                                                                        <div class="profile_image">
                                                                            <img src="<%# Eval("EmployeePhoto") %>" style="width: 50px;">
                                                                        </div>
                                                                        <h5><strong><%# Eval("EmployeeLastName") %></strong></h5>
                                                                        <p><%# Convert.ToDateTime(Eval("DOB")).ToString("MMMM dd") %></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <%--  <div class="col-md-2">
                                                                <div class="carousel-caption d-md-block">
                                                                    <img src='' class="birthday-carousel-img img-100" src='<%# Eval("EmployeePhoto") %>'>
                                                                </div>
                                                                <h5>
                                                                    <strong class="ng-binding"><%#Eval("EmployeeFirstName").ToString()+' '+Eval("EmployeeMiddleName").ToString()+' '+Eval("EmployeeLastName").ToString()  %></strong>
                                                                </h5>
                                                                <p class="ng-binding"><%# Convert.ToDateTime(Eval("DOB")).ToString("MMMM dd") %></p>
                                                            </div>--%>
                                                        </ItemTemplate>
                                                    </asp:Repeater>


                                                    <%--                                                    <div class="item">
                                                        <div class="col-md-4 col-sm-4 col-sx-12 col-lg-4">
                                                            <div class="d-md-block">
                                                                <img src="/assets/images/user profile.png">
                                                                <h5><strong>Afshan</strong></h5>
                                                                <p>Today Is Friday </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-md-4 col-sm-4 col-sx-12 col-lg-4">
                                                            <div class="d-md-block">
                                                                <img src="/assets/images/user profile.png">
                                                                <h5><strong>Afshan</strong></h5>
                                                                <p>Today Is Friday </p>
                                                            </div>
                                                        </div>
                                                    </div>--%>
                                                    <!--  Example item end -->
                                                </div>
                                                <div runat="server" id="divBtnArrows">
                                                    <a class="left carousel-control" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                                    <a class="right carousel-control" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
                            <div class="widget map_radius announcment_area" style="height: 245px;">
                                <header class="widget-header">
                                    <h4 class="widget-title">Away Hours</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-6">
                                            <div id="countdown">
                                                <div class="countdown-item hours show">0h</div>
                                                <div class="countdown-item minutes show">0m</div>
                                                <div class="countdown-item seconds show">0s</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
                            <div class="widget map_radius announcment_area" style="height: 280px;">
                                <header class="widget-header">
                                    <h4 class="widget-title">Announcements</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator" />
                                <div class="widget-body announcement">
                                    <asp:GridView ID="gvAnnouncement" runat="server" CssClass="table table-bordered  dataTable no-footer" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="#">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Announcement Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol6" Text='<%# Eval("Date") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Title">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("AnnouncementTitle") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Description")  %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                    </div>
                    <!-- .widget -->
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>

                </section>

            </div>
            <div class="clearfix">&nbsp;</div>

            <div class="clearfix">&nbsp;</div>


            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>


        <!-- Resources -->

        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <!-- Chart code -->
        <script>
            //$(document).ready(function () {

            //});
            //var timeOutwhCount;
            //var timeOutBreakin;
            //var timeOuttimedCount;

            function checkStatus() {

                $("#btnTimeOut").prop("disabled", true);
                $("#btnTimeIN").prop("disabled", true);
                $("#btnBreakTimeOut").prop("disabled", true);
                $("#btnBreakTimeIN").prop("disabled", true);

                console.log($('#hdWorkHoursSec').val());

                $('#timer_is_on').val('0');
                $('#timer_is_on_BreakIn').val('0');

                //$('#hdSec').val('0');
                //$('#hdBreakInSec').val('0');
                //$('#hdWorkHoursSec').val('0');

                //alert('disabled');

                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/checkStatus',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var data = response.d;

                        $.each(data, function (key, data) {

                            if (data.BtnStatus == 'OnLeave') {
                            }
                            else if (data.BtnStatus == 'OnHoliday') {
                                $("#btnTimeIN").prop("disabled", false);
                            }
                            else if (data.BtnStatus == 'Off') {
                                $("#btnTimeIN").prop("disabled", false);
                            }
                            else if (data.BtnStatus == 'None') {
                                //$('#hdSec').val(data.TimeIN);
                                //$('#hdBreakInSec').val(data.BreakInSec);
                                $('#hdWorkHoursSec').val(data.WorkHoursSec);

                                if (data.TimeIN != '0') {
                                    //$('#lblStartTime').text(data.StartTime);
                                    //$('#lblBreakStartEndTime').text(data.BreakStartEndTime);
                                    //$('#lblEndTime').text(data.EndTime);
                                }
                            }
                            else if (data.BtnStatus == 'TimeOut') {

                                $("#btnTimeOut").prop("disabled", false);
                                //$('#hdSec').val(data.TimeIN);
                                //$('#hdBreakInSec').val(data.BreakInSec);
                                $('#hdWorkHoursSec').val(data.WorkHoursSec);
                                $('#timer_is_on').val('1');
                                //$('#lblStartTime').text(data.StartTime);
                                //$('#lblBreakStartEndTime').text(data.BreakStartEndTime);
                            }
                            else if (data.BtnStatus == 'BreakEndTime') {

                                $("#btnBreakTimeIN").prop("disabled", false);
                                //$('#hdSec').val(data.TimeIN);
                                //$('#hdBreakInSec').val(data.BreakInSec);
                                $('#hdWorkHoursSec').val(data.WorkHoursSec);
                                //$('#lblStartTime').text(data.StartTime);

                                //$('#timer_is_on').val('1');
                                //$('#timer_is_on_BreakIn').val('1');
                            }
                            else if (data.BtnStatus == 'BreakTime') {

                                $("#btnBreakTimeOut").prop("disabled", false);
                                $("#btnTimeOut").prop("disabled", false);
                                //$('#hdSec').val(data.TimeIN);
                                $('#hdWorkHoursSec').val(data.WorkHoursSec);
                                $('#timer_is_on').val('1');
                                //$('#lblStartTime').text(data.StartTime);
                            }
                            else if (data.BtnStatus == 'TimeIn') {
                                $("#btnTimeIN").prop("disabled", false);
                            }

                        });

                        //whCount();

                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });


            }

            function TimeIN() {
                debugger;
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/TimeIN',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        checkStatus();
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });


            }

            function BreakTimeOut() {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/BreakTimeOut',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        checkStatus();
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });

            }

            function BreakTimeIN() {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/BreakTimeIN',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        checkStatus();
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });

            }

            function TimeOut() {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/TimeOut',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        checkStatus();
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });

            }


            //function timedBreakInCount() {
            //    if (document.getElementById("timer_is_on_BreakIn").value == '1') {
            //        var c = parseInt(document.getElementById("hdBreakInSec").value);
            //        document.getElementById("txtBreakInH").innerHTML = Math.floor(c / 3600);
            //        document.getElementById("txtBreakInM").innerHTML = Math.floor((c / 60) % 60);
            //        document.getElementById("txtBreakIn").innerHTML = Math.floor(c % 60);
            //        c = c + 1;
            //        document.getElementById("hdBreakInSec").value = c;
            //        timeOutBreakin = setTimeout(timedBreakInCount, 1000);
            //    }
            //    else {
            //        var c = parseInt(document.getElementById("hdBreakInSec").value);
            //        document.getElementById("txtBreakInH").innerHTML = Math.floor(c / 3600);
            //        document.getElementById("txtBreakInM").innerHTML = Math.floor((c / 60) % 60);
            //        document.getElementById("txtBreakIn").innerHTML = Math.floor(c % 60);

            //    }
            //}

            //function timedCount() {
            //    if (document.getElementById("timer_is_on").value == '1') {
            //        var c = parseInt(document.getElementById("hdSec").value);
            //        document.getElementById("txtH").innerHTML = Math.floor(c / 3600);
            //        document.getElementById("txtM").innerHTML = Math.floor((c / 60) % 60);
            //        document.getElementById("txt").innerHTML = Math.floor(c % 60);
            //        c = c + 1;
            //        document.getElementById("hdSec").value = c;
            //        timeOuttimedCount = setTimeout(timedCount, 1000);
            //        //alert('Timer on');
            //    }
            //    else {
            //        var c = parseInt(document.getElementById("hdSec").value);
            //        document.getElementById("txtH").innerHTML = Math.floor(c / 3600);
            //        document.getElementById("txtM").innerHTML = Math.floor((c / 60) % 60);
            //        document.getElementById("txt").innerHTML = Math.floor(c % 60);

            //        //alert('Timer Off');
            //    }

            //}

            function whCount() {
                if (document.getElementById("timer_is_on").value == '1') {
                    var c = parseInt(document.getElementById("hdWorkHoursSec").value);
                    console.log(c);
                    document.getElementById("txtWorkHoursH").innerHTML = Math.floor(c / 3600);
                    document.getElementById("txtWorkHoursM").innerHTML = Math.floor((c / 60) % 60);
                    document.getElementById("txtWorkHours").innerHTML = Math.floor(c % 60);
                    c = c + 1;
                    document.getElementById("hdWorkHoursSec").value = c;
                    
                }
                else {
                    var c = parseInt(document.getElementById("hdWorkHoursSec").value);
                    document.getElementById("txtWorkHoursH").innerHTML = Math.floor(c / 3600);
                    document.getElementById("txtWorkHoursM").innerHTML = Math.floor((c / 60) % 60);
                    document.getElementById("txtWorkHours").innerHTML = Math.floor(c % 60);
                }

                timeOutwhCount = setTimeout(whCount, 1000);

            }

            function pageLoad(sender, args) {
                checkStatus();
                whCount();
                $('.BirthdayRepeater').each(function () {
                    $(this).find('.item').first().addClass('active');
                });
            }

        </script>

        <script>
            // Instantiate the Bootstrap carousel
            $('.multi-item-carousel').carousel({
                interval: false
            });

            // for every slide in carousel, copy the next slide's item in the slide.
            // Do the same for the next, next item.
            $('.multi-item-carousel .item').each(function () {
                var next = $(this).next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));

                if (next.next().length > 0) {
                    next.next().children(':first-child').clone().appendTo($(this));
                } else {
                    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
                }
            });
        </script>


        <script>
            function openModal() {
                $('#myModal').modal('show');

            }
        </script>

        <script type="text/javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        if (e.get_error() != null) {
                            var ex = e.get_error();
                            var mesg = "HttpStatusCode: " + ex.httpStatusCode;
                            mesg += "<br />Name: " + ex.name;
                            mesg += "<br />Message: " + ex.message;
                            mesg += "<br />Description: " + ex.description;
                            console.log(mesg);
                        }
                    }
                });
            };
        </script>



        <script>
            $('.counter-count').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {

                    //chnage count up speed here
                    duration: 1500,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        </script>

        <script>
            function showTime() {
                var date = new Date();
                var h = date.getHours(); // 0 - 23
                var m = date.getMinutes(); // 0 - 59
                var s = date.getSeconds(); // 0 - 59
                var session = "";

                if (h == 0) {
                    h = 12;
                }

                if (h > 12) {
                    h = h - 12;
                    session = "";
                }

                h = (h < 10) ? "0" + h : h;
                m = (m < 10) ? "0" + m : m;
                s = (s < 10) ? "0" + s : s;

                var time = h + ":" + m + ":" + s + " " + session;
                document.getElementById("MyClockDisplay").innerText = time;
                document.getElementById("MyClockDisplay").textContent = time;

                setTimeout(showTime, 1000);

            }

            showTime();

        </script>
        <script>

        </script>
        <!-- HTML -->


    </form>

</body>


</html>
