﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class Adjustments : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";


        protected string AttendanceDate
        {
            get
            {
                if (ViewState["AttendanceDate"] != null)
                {
                    return (string)ViewState["AttendanceDate"];
                }
                else
                {
                    return DateTime.Now.ToString("MMM-yyyy");
                }
            }

            set
            {
                ViewState["AttendanceDate"] = value;
            }
        }

        protected string ShowMessage
        {
            get
            {
                if (ViewState["ShowMessage"] != null)
                {
                    return (string)ViewState["ShowMessage"];
                }
                else
                {
                    return "1";
                }
            }

            set
            {
                ViewState["ShowMessage"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                BindEmployeeDropdown();
                divAlertMsg.Visible = false;
                txtMonth.Text = AttendanceDate;
                ShowMessage = "0";
                //btnSerach_ServerClick(null, null);
                ShowMessage = "1";
                //ddlEmployee_SelectedIndexChanged(null, null);
                Common.addlog("ViewAll", "ESS", "Attendence Viewed", "");
                gv.Columns[0].Visible = false;


            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "UpdateAdjustment")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gv.Rows[index];


                Label attendanceID = (Label)row.FindControl("lblAttendanceID");
                TextBox reason = (TextBox)row.FindControl("txtLateReason");
                objDB.AttendanceID = Convert.ToInt32(attendanceID.Text);
                objDB.LateReason = reason.Text;
                if (objDB.LateReason != null && objDB.LateReason != "")
                {
                    string msg = objDB.AddAdjustment(ref errorMsg);
                    if (msg == "Adjustment Updated")
                    {
                        AttendanceDate = txtMonth.Text;
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                        pAlertMsg.InnerHtml = "Adjustment Updated";
                        Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/adjustment");
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Please provide reason";
                }
            }
        }
        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnSerach_ServerClick(object sender, EventArgs e)
        {
            gv.DataSource = null;
            gv.DataBind();

            divAlertMsg.Visible = false;

            if (txtMonth.Text != null && txtMonth.Text != null)
            {
                objDB.date = txtMonth.Text;
                //objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                DataTable dt = objDB.GetEmployeeAttendanceByMonthForAdjustment(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        gv.DataSource = dt;
                        gv.DataBind();
                    }
                    else if (ShowMessage == "1")
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No record available";
                    }
                }
                else if(ShowMessage == "1")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No record available";
                }

            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //If Salary is less than 10000 than set the row Background Color to Cyan  
                //if (Convert.ToInt32(e.Row.Cells[3].Text) < 10000)
                //{
                //    e.Row.BackColor = Color.Cyan;
                //}
                Label lblReason = e.Row.FindControl("lblLateReason") as Label;
                TextBox txtReason = e.Row.FindControl("txtLateReason") as TextBox;
                Button btnAddReason =e.Row.FindControl("btnUpdate") as Button;
                DataRowView drv = (DataRowView)e.Row.DataItem;
                if (drv != null)
                {
                    string IsLate = drv["LateIn"].ToString();
                    if (lblReason.Text == "" && IsLate == "1")
                    {
                        txtReason.Visible = true;
                        btnAddReason.Visible = true;
                        lblReason.Visible = false;
                    }
                    else
                    {
                        txtReason.Visible = false;
                        btnAddReason.Visible = false;
                        lblReason.Visible = true;
                    }
                }
            }
        }
    }
}