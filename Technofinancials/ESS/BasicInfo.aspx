﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BasicInfo.aspx.cs" Inherits="Technofinancials.ESS.BasicInfo" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Basic Information</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4" style="display:none;">
                                <div style="text-align: right;">
                                    <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">




                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <h4>Employee No.</h4>
                                    <input class="form-control" placeholder="Emp No" type="text" id="txtEmployeeCode" runat="server" name="employerNo" disabled="disabled" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Workday ID</h4>
                                    <input class="form-control" placeholder="Emp No" type="text" id="WorkdayID" runat="server" name="employerNo" disabled="disabled" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>National Identity</h4>
                                    <input class="form-control" placeholder="CNIC" type="text" id="txtCNIC" runat="server" disabled="disabled" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Full Name</h4>
                                    <input class="form-control" placeholder="First Name" type="text" id="txtFirstName" runat="server" name="firstName" disabled="disabled" />
                                </div>
                                <div class="col-sm-4 form-group"  style="display:none;">
                                    <h4>Middle Name</h4>
                                    <input class="form-control" placeholder="Middle Name" type="text" id="txtMiddleName" runat="server" name="middleName" disabled="disabled" />
                                </div>
                                <div class="col-sm-4 form-group" style="display:none;">
                                    <h4>Last Name</h4>
                                    <input class="form-control" placeholder="Last Name" type="text" id="txtLastName" runat="server" name="lastName" disabled="disabled" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Gender</h4>
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlGender" Enabled="false">
                                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                                        <asp:ListItem Value="Male">Male</asp:ListItem>
                                        <asp:ListItem Value="Female">Female</asp:ListItem>
                                        <asp:ListItem Value="Other">Other</asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Contact No</h4>
                                    <input class="form-control" placeholder="Contact no" type="text" name="contactNo" id="txtContactNo" runat="server" disabled="disabled" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Email Address</h4>
                                    <input class="form-control" placeholder="Email address" type="email" name="emailAddress" id="txtEmail" runat="server" disabled="disabled" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Personal Email Address</h4>
                                    <input class="form-control" placeholder="Email address" type="email" name="emailAddress" id="Email1" runat="server" disabled="disabled" />
                                </div>
                                
                               


                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="col-sm-6 form-group">
                                    <h4>Country</h4>
                                    <input class="form-control" placeholder="Email address" type="text" name="Country" id="txtCountry" runat="server" disabled="disabled" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>State</h4>
                                    <input class="form-control" placeholder="Email address" type="text" name="State" id="txtState" runat="server" disabled="disabled" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>City</h4>
                                    <input class="form-control" placeholder="Email address" type="text" name="City" id="txtCity" runat="server" disabled="disabled" />
                                </div>
                                 <div class="col-sm-6 form-group">
                                    <h4>Zip Code<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtZIPCode" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                    <input class="form-control" placeholder="Zip Code" type="text" name="zipCode" id="txtZIPCode" runat="server" disabled="disabled " />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Address Line 1<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtAddressLine1" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                    <input class="form-control" placeholder="Address Line 1" type="text" name="addressLine1" id="txtAddressLine1" runat="server" disabled="disabled" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Address Line 2<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtAddressLine2" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                    <input class="form-control" placeholder="Address Line 2" type="text" id="txtAddressLine2" runat="server" disabled="disabled" />
                                </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="user-img-div">
                                        <img src="/assets/images/3techno-Logo.png" class="img-responsive" id="imgPhoto" clientidmode="static" runat="server" />
                                        <%--                                                   <asp:FileUpload ID="updPhoto" runat="server" ClientIDMode="Static" Style="display: none;" />
                                                    <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('updPhoto').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>--%>
                                    </div>
                                </div>


                            </div>
                        </div>
                        
                    </div>
                      <div class="content-header" style="padding-left:0px;">
                        <div class="container-fluid" style="padding-left:0px;">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark" style="margin-left:0px !important;">Detail</h1>
                                </div>
                                <!-- /.col -->

                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-12  form-group">
                                    <h4><strong>Employment Details</strong></h4>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4 class="control-label">Employment Type</h4>
                                    <asp:DropDownList ID="ddlEmploymentType" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">--- Please Select ---</asp:ListItem>
                                        <asp:ListItem Value="Temporary">Temporary</asp:ListItem>
                                        <asp:ListItem Value="Permanant">Permanent</asp:ListItem>
                                        <asp:ListItem Value="Contractor">Contractor</asp:ListItem>
                                        <asp:ListItem Value="Adhoc">Adhoc</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Date of Joining</h4>
                                        <input type="text"  class="form-control" name="dateOfJoining" id="txtDOJ" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4 class="control-label">Job Description</h4>
                                    <asp:DropDownList ID="ddlJD" runat="server" CssClass="form-control " AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4 class="control-label">Grade</h4>
                                    <asp:DropDownList ID="ddlGrades" runat="server" CssClass="form-control " AutoPostBack="true" OnSelectedIndexChanged="ddlGrades_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>

                                <div class="col-sm-6 form-group">
                                    <h4>Department<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlDept" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                    <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control " ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Designation<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDesg" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                    <asp:DropDownList ID="ddlDesg" runat="server" CssClass="form-control ">
                                    </asp:DropDownList>
                                </div>
                            
                                <div class="col-sm-6 form-group">
                                    <h4></h4>
                                    <input class="form-control" placeholder="Person Name" type="text" name="personName" id="txtEmergencyPerson" runat="server" disabled="disabled" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4></h4>
                                    <input class="form-control" placeholder="Phone number" type="text" name="phoneNumber" id="txtEmergencyPhone" runat="server" disabled="disabled" />
                                </div>

                           
                       
                       
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                                                                                <div class="col-sm-12  form-group">
                                    <h4><strong>Direct Reporting To</strong></h4>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Department</h4>
                                    <asp:DropDownList ID="ddlDirectReportToDept" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDirectReportToDept_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Designation</h4>
                                    <asp:DropDownList ID="ddlDirectreportTo" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <div class="form-group">
                                        <h4>Person</h4>
                                    </div>
                                    <asp:DropDownList ID="ddlDirectReportingPerson" runat="server" ClientIDMode="Static" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                         <div class="row">
                                                             <div class="col-sm-12 form-group">
                                    
                                    <h4><strong>Indirect Reporting To</strong></h4>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Department</h4>
                                    <asp:DropDownList ID="ddlInDirectReportToDept" runat="server" ClientIDMode="Static" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlInDirectReportToDept_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <h4>Designation</h4>
                                    <asp:DropDownList ID="ddlInDirectreportTo" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <div class="form-group">
                                        <h4>Person</h4>
                                    </div>
                                    <asp:DropDownList ID="ddlInDirectReportingPerson" runat="server" ClientIDMode="Static" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
                                </div>
                         
                         </div>
                     </div>
                    </div>

               

                    <div class="row">
                        
                    </div>

                    <div class="col-sm-12" style="display: none">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4>Country<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCountries" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                <asp:DropDownList ID="ddlCountries" runat="server" class="form-control select2" data-plugin="select2" OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged" AutoPostBack="true" Enabled="false"></asp:DropDownList>
                            </div>
                            <div class="col-sm-4">
                                <h4>State<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlStates" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                <asp:DropDownList ID="ddlStates" runat="server" class="form-control select2" data-plugin="select2" OnSelectedIndexChanged="ddlStates_SelectedIndexChanged" AutoPostBack="true" Enabled="false"></asp:DropDownList>
                            </div>
                            <div class="col-sm-4">
                                <h4>City<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlCities" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                <asp:DropDownList ID="ddlCities" runat="server" class="form-control select2" data-plugin="select2" Enabled="false"></asp:DropDownList>
                            </div>

                            <%--                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="AD_btn" "Save" id="button"><i class="fas fa-paper-plane"></i></button>
                                            </div>
                                            <div class="col-md-4 col-md-offset-4">
                                            </div>
                                        </div>--%>
                        </div>
                    </div>

                    
                                <div class="col-md-12">
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imgPhoto').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#updPhoto").change(function () {
                readURL(this);
            });
        </script>
        <style>
            .user-img-div img {
                display: block !important;
                margin: 32px auto 0px !important;
                margin-left: 55px !important;
                border-radius: 5%;
            }
            .tf-upload-btn {
                background-color: #575757;
                padding: 8px 9px 8px 9px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
                display: inline-block !important;
                margin-top: -113px;
                margin-left: 21px !important;
                bottom: 2px !important;
                position: absolute !important;
            }

            .user-img-div button {
                display: block !important;
                margin: 0 auto !important;
                margin-top: 10px !important;
            }

            .user-img-div img {
                border: 1px solid #cccccc !important;
/*                display: inline-block !important;*/
                height: 152px !important;
                width: 150px !important;
            }


                .user-img-div button {
                    display: inline-block !important;
                    margin-top: 0px !important;
                    margin: 0 auto;
                }

            .tf-upload-btn {
                margin-left: 21px !important;
            }

            button.tf-upload-btn {
                margin-left: 10px !important;
            }
            .form-control{
               font-size: 11px !important;
            }
        </style>
    </form>
</body>
</html>
