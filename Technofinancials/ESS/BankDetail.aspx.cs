﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class BankDetail : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected  int employeeID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
       
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/dashboard";

                divAlertMsg.Visible = false;
               


                if (Session["EmployeeID"] != null)
                {
                    employeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                    //getCandidateByID(candidateID);
                    getEmployeeByID(employeeID);

                }
            }
        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

    
      
        private void getEmployeeByID(int EmpID)
        {
            objDB.EmployeeID = EmpID;
            DataTable dt = objDB.GetEmployeeByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    txtBankName.Value = dt.Rows[0]["BankName"].ToString();
                    txtBranchName.Value = dt.Rows[0]["BranchName"].ToString();
                    txtAccountNo.Value = dt.Rows[0]["AccountNo"].ToString();
                    txtAccountTitle.Value = dt.Rows[0]["AccountTitle"].ToString();



                }
            }
            Common.addlog("ViewAll", "ESS", "Bank Detail Viewed", "");

        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            string res = "";
           
            
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);


            objDB.BankName = txtBankName.Value;
            objDB.BranchName = txtBranchName.Value;
            objDB.AccountTitle = txtAccountTitle.Value;
            objDB.AccountNo = txtAccountNo.Value;
            objDB.EmployeeID = int.Parse(Session["EmployeeID"].ToString());
            objDB.ModifiedBy = Session["UserName"].ToString();
       
            res = objDB.UpdateEmployeeBankDetails();



            if (res == "Account Details are Updated")
            {
                res = "Bank Details are Updated";
                Common.addlog("Update", "ESS", "Account \"" + objDB.AccountTitle + "\" Updated", "BalanceTransfer", employeeID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }


    }
}