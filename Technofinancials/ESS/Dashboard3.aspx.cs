﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Net;
using System.Xml.Linq;

namespace Technofinancials.ESS
{
    public partial class Dashboard3 : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                RSSFeed();
                if (Session["UserAccess"].ToString() != "Normal User")
                {
                    Session["OldUserAccess"] = Session["UserAccess"];
                }

                hMonthDay.InnerHtml = DateTime.Now.ToString("MMMM dd");
                hYearDay.InnerHtml = DateTime.Now.ToString("yyyy, dddd");
                hTime.InnerHtml = DateTime.Now.ToString("h:mm tt");

                SysClockHor.Value = DateTime.Now.Hour.ToString();
                SysClockMin.Value = DateTime.Now.Minute.ToString();
                SysClockSec.Value = DateTime.Now.Second.ToString();

                Session["UserAccess"] = "Normal User";
                string CurrDate = DateTime.Now.ToString();
                //checkStatus();
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                BindBirthdayRepeater();
                GetAnnouncementsByCompanyID();
                DataTable dt = new DataTable();
                dt = objDB.GetESSDashboardWidgets(ref errorMsg);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lblPresent.Text = dt.Rows[0]["Present"].ToString();
                    lblAbsent.Text = dt.Rows[0]["Absent"].ToString();
                    lblEarlyOut.Text = dt.Rows[0]["ShortHours"].ToString();
                    lblHalfDay.Text = dt.Rows[0]["HalfDay"].ToString();
                    lblLeave.Text = dt.Rows[0]["Leave"].ToString();
                    lblMissingPunches.Text = dt.Rows[0]["MissingPunches"].ToString();
                }

                DataTable autoFlag = objDB.CheckAutoAttendanceStatus(ref errorMsg);
                isAutoAttendance.Value = autoFlag.Rows[0][0].ToString();
                Common.addlog("ViewAll", "ESS", "Dashboard Viewed", "");
            }
        }

        public void RSSFeed()
        {
            WebClient wclient = new WebClient();
            string RSSData = wclient.DownloadString("https://www.thenews.com.pk/rss/1/4");

            XDocument xml = XDocument.Parse(RSSData);

            List<RSSFeed> RSSFeedData = (from x in xml.Descendants("item")
                               select new RSSFeed
                               {
                                   Title = ((string)x.Element("title")),
                                   Link = ((string)x.Element("link")),
                                   Description = ((string)x.Element("description")),
                                   PubDate = ((string)x.Element("pubDate")),
                                   image = ((string)x.Element("image"))
                               }).Take(50).ToList();

            foreach (var item in RSSFeedData)
            {
                string desc = item.Description;
                item.image = desc.Substring(10,desc.IndexOf("\"/>")-10);
                item.Description = desc.Substring(desc.IndexOf("\"/>")+3);
            }
            Session["RSSFeedData"] = RSSFeedData;
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        private void GetAnnouncementsByCompanyID()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.Department = Session["DeptName"].ToString();
            dt = objDB.GetTop5AnnouncementByCompanyID(ref errorMsg);
            RptrNotifications.DataSource = dt;
            RptrNotifications.DataBind();

            gvAnnouncement.DataSource = dt;
            gvAnnouncement.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvAnnouncement.UseAccessibleHeader = true;
                    gvAnnouncement.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

            Common.addlog("ViewAll", "HR", "All Announcement Viewed", "Announcement");
        }


        #region old
        protected bool isTimeIn
        {
            get
            {
                if (ViewState["isTimeIn"] != null)
                {
                    return (bool)ViewState["isTimeIn"];
                }
                else
                {
                    return true;
                }
            }

            set
            {
                ViewState["isTimeIn"] = value;
            }
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            DateTime CurTime = DateTime.Now;
            DateTime lateIn, StartTime, halfDay, EndTime, OverTime, BeforeTime;
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            DataTable Empdt = objDB.GetWorkingShiftByEmployeeID(ref errorMsg);
            if (Empdt != null)
            {
                if (Empdt.Rows.Count > 0)
                {
                    lateIn = DateTime.Parse(Empdt.Rows[0]["LateInTime"].ToString());
                    StartTime = DateTime.Parse(Empdt.Rows[0]["StartTime"].ToString());
                    halfDay = DateTime.Parse(Empdt.Rows[0]["HalfDayStart"].ToString());
                    objDB.ShiftID = int.Parse(Empdt.Rows[0]["ShiftID"].ToString());

                    if (isTimeIn)
                    {
                        objDB.LateReason = txtReason.Value;
                        if (CurTime >= halfDay)
                        {
                            objDB.TimeIn = CurTime.ToString("dd-MMM-yyyy HH:mm tt");
                            objDB.Resultant = "Half Day";
                        }
                        else if (CurTime >= lateIn)
                        {
                            objDB.TimeIn = CurTime.ToString("dd-MMM-yyyy HH:mm tt");
                            objDB.Resultant = "Late In";
                        }
                        objDB.AddEmployeeAttendance();
                        Response.Redirect(Request.Url.AbsolutePath, false);
                        btnTimeIN.Visible = false;
                        btnTimeOut.Visible = true;
                    }
                    else
                    {
                        EndTime = DateTime.Parse(Empdt.Rows[0]["EndTime"].ToString());
                        OverTime = DateTime.Parse(Empdt.Rows[0]["EndTime"].ToString());
                        BeforeTime = DateTime.Parse(Empdt.Rows[0]["HalfDayStart"].ToString());
                        OverTime.AddMinutes(15);
                        objDB.ShiftID = int.Parse(Empdt.Rows[0]["ShiftID"].ToString());
                        objDB.TimeOUt = CurTime.ToString("dd-MMM-yyyy HH:mm tt");

                        if (CurTime >= OverTime)
                        {
                            objDB.Resultant = "Over Time";
                        }

                        else
                        {
                            objDB.Resultant = "Early out";
                        }
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        objDB.LateReason = txtReason.Value;
                        objDB.TimeIn = DateTime.Now.ToString("dd-MMM-yyyy");
                        objDB.AddEmployeeAttendance();
                        Response.Redirect(Request.Url.AbsolutePath, false);

                    }
                }
            }
        }

        #endregion
        //private void checkStatus()
        //{
        //    btnTimeOut.Disabled = true;
        //    btnTimeIN.Disabled = true;
        //    btnBreakTimeOut.Disabled = true;
        //    btnBreakTimeIN.Disabled = true;
        //    objDB.date = DateTime.Now.ToString();
        //    //objDB.date = "12/16/2019 01:20:00 PM";


        //    objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());

        //    DataTable dtStatus = objDB.CheckAttendanceButtonStatusWithTimeNew(ref errorMsg);

        //    timer_is_on.Value = "0";
        //    timer_is_on_BreakIn.Value = "0";

        //    hdSec.Value = "0";
        //    hdBreakInSec.Value = "0";
        //    hdWorkHoursSec.Value = "0";

        //    string btnStatus = "None";

        //    if (dtStatus != null && dtStatus.Rows.Count > 0)
        //    {
        //        btnStatus = dtStatus.Rows[0]["BtnStatus"].ToString();
        //    }

        //    if (btnStatus == "OnLeave")
        //    {
        //    }
        //    else if (btnStatus == "OnHoliday")
        //    {
        //        btnTimeIN.Disabled = false;
        //    }
        //    else if (btnStatus == "Off")
        //    {
        //        btnTimeIN.Disabled = false;

        //    }
        //    else if (btnStatus == "None")

        //    {
        //        hdSec.Value = dtStatus.Rows[0]["TimeIN"].ToString();
        //        hdBreakInSec.Value = dtStatus.Rows[0]["BreakTime"].ToString();
        //        hdWorkHoursSec.Value = (int.Parse(dtStatus.Rows[0]["TimeIN"].ToString()) - int.Parse(dtStatus.Rows[0]["BreakTime"].ToString())).ToString();

        //        if (dtStatus.Rows[0]["BreakTime"].ToString() != "0")
        //        {
        //            lblStartTime.Text = Convert.ToDateTime(dtStatus.Rows[0]["StartTime"]).ToString("hh:mm tt");
        //            lblBreakStartEndTime.Text = Convert.ToDateTime(dtStatus.Rows[0]["BreakStartTime"]).ToString("hh:mm tt") + "-" + Convert.ToDateTime(dtStatus.Rows[0]["BreakEndTime"]).ToString("hh:mm tt");
        //            lblEndTime.Text = Convert.ToDateTime(dtStatus.Rows[0]["EndTime"]).ToString("hh:mm tt");
        //        }

        //    }
        //    else if (btnStatus == "TimeOut")
        //    {
        //        btnTimeOut.Disabled = false;
        //        hdSec.Value = dtStatus.Rows[0]["TimeIN"].ToString();
        //        hdBreakInSec.Value = dtStatus.Rows[0]["BreakTime"].ToString();
        //        hdWorkHoursSec.Value = (int.Parse(dtStatus.Rows[0]["TimeIN"].ToString()) - int.Parse(dtStatus.Rows[0]["BreakTime"].ToString())).ToString();
        //        timer_is_on.Value = "1";
        //        lblStartTime.Text = Convert.ToDateTime(dtStatus.Rows[0]["StartTime"]).ToString("hh:mm tt");
        //        lblBreakStartEndTime.Text = Convert.ToDateTime(dtStatus.Rows[0]["BreakStartTime"]).ToString("hh:mm tt") + " - " + Convert.ToDateTime(dtStatus.Rows[0]["BreakEndTime"]).ToString("hh:mm tt");

        //    }
        //    else if (btnStatus == "BreakEndTime")
        //    {
        //        btnBreakTimeIN.Disabled = false;
        //        hdSec.Value = dtStatus.Rows[0]["TimeIN"].ToString();
        //        hdBreakInSec.Value = dtStatus.Rows[0]["BreakTime"].ToString();
        //        hdWorkHoursSec.Value = (int.Parse(dtStatus.Rows[0]["TimeIN"].ToString()) - int.Parse(dtStatus.Rows[0]["BreakTime"].ToString())).ToString();
        //        lblStartTime.Text = Convert.ToDateTime(dtStatus.Rows[0]["StartTime"]).ToString("hh:mm tt");

        //        timer_is_on.Value = "1";
        //        timer_is_on_BreakIn.Value = "1";
        //    }
        //    else if (btnStatus == "BreakTime")
        //    {
        //        btnBreakTimeOut.Disabled = false;
        //        hdSec.Value = dtStatus.Rows[0]["TimeIn"].ToString();
        //        hdWorkHoursSec.Value = dtStatus.Rows[0]["TimeIn"].ToString();
        //        timer_is_on.Value = "1";
        //        lblStartTime.Text = Convert.ToDateTime(dtStatus.Rows[0]["StartTime"]).ToString("hh:mm tt");

        //    }
        //    else if (btnStatus == "TimeIn")
        //    {
        //        btnTimeIN.Disabled = false;
        //    }
        //    //GetAnnouncementsByCompanyID();
        //}
        protected void btnTimeIN_ServerClick(object sender, EventArgs e)
        {
            objDB.date = DateTime.Now.ToString();
            // objDB.date = "12/16/2019 01:20:00 PM";
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            string ss = objDB.AttendanceMarkTimeINNew();
            //checkStatus();
        }

        protected void btnBreakTimeOut_ServerClick(object sender, EventArgs e)
        {
            objDB.date = DateTime.Now.ToString();
            // objDB.date = "12/16/2019 1:01:00 PM";

            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            string ss = objDB.AttendanceMarkBreakOut();
            //checkStatus(); 

        }

        protected void btnBreakTimeIN_ServerClick(object sender, EventArgs e)
        {
            objDB.date = DateTime.Now.ToString();
            // objDB.date = "12/16/2019 2:01:00 PM";
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            objDB.AttendanceMarkBreakIn();
            //checkStatus();
        }
        protected void btnTimeOut_ServerClick(object sender, EventArgs e)
        {
            objDB.date = DateTime.Now.ToString();
            //       objDB.date = "12/16/2019 10:01:00 PM";
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            string ss = objDB.AttendanceMarkTimeOutNew();
            //checkStatus();
        }
        private void BindBirthdayRepeater()
        {
            DataTable dt = new DataTable();
            //theCarousel.Visible = false;
            divBtnArrows.Visible = false;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetBirthdayOfNext7DaysByCompanyID(ref errorMsg);
            if (dt != null && dt.Rows.Count > 0)
            {
                repBirthday.DataSource = dt;
                repBirthday.DataBind();
                //theCarousekl.Visible = true;

                if (dt.Rows.Count > 3)
                {
                    divBtnArrows.Visible = true;
                }
            }
        }

        protected void RptrNotifications_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlGenericControl notDiv = e.Item.FindControl("notDiv") as HtmlGenericControl;
                int index = e.Item.ItemIndex;
                notDiv.Attributes["class"] = getclass(index);
            }                                                
        }

        protected string getclass(int i) 
        {
            i = (i != 0 && i % 5 == 0)? 1 : i;
              
            string retclas = "alert alert-info clearfix";
           
            if (i == 1)
            {
                retclas = "alert alert-danger";
            }
            else if (i == 2)
            {
                retclas = "alert alert-warning";
            }
            else if (i == 4)
            {
                retclas = "alert alert-success";
            }
            else if (i == 5)
            {
                retclas = "alert alert-info";
            }

            return retclas;
        }


    }


   

}

public class RSSFeed
{
    public string Title { get; set; }
    public string Link { get; set; }
    public string Description { get; set; }
    public string PubDate { get; set; }
    public string image { get; set; }
}