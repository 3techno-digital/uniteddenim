﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class TravelExpense : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                GetData();
                Access.Value = Session["UserAccess"].ToString();

            }
        }
        private DataTable FilterData2(DataTable dt)
        {
            if (dt == null)
            {
                return dt;
            }

            DataTable dtFilter = new DataTable();
            DataTable dtTemp = new DataTable();
            dtTemp = Common.filterTable(dt, "Status", "Saved as Draft");
            dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());
            dtFilter = Common.ReversefilterTable(dt, "Status", "Saved as Draft");
            if (dtTemp != null)
                dtFilter.Merge(dtTemp);

            return dtFilter;
        }


        private void GetData()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            dt = objDB.GetAllOtherExpensesByEmployeeID(ref errorMsg);
            //gvAnnouncement.DataSource = FilterData2(dt);
            gvAnnouncement.DataSource = dt;
            gvAnnouncement.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvAnnouncement.UseAccessibleHeader = true;
                    gvAnnouncement.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All Announcement Viewed", "Announcement");

        }
        private DataTable FilterData(DataTable dt)
        {
            DataTable dtFilter = new DataTable();

            if (dt == null)
            {
                return dt;
            }

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.DocName = "OtherExpenses";

            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");

                    if (dtTemp != null)
                        dtFilter.Merge(dtTemp);

                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }

                }
            }

            return dtFilter;
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            int ID = Convert.ToInt32(btn.CommandArgument);
            string docstatus = btn.CommandName;
            if(docstatus == "Data Submitted for Review")
			{
                objDB.OtherExpenseID = ID;
                objDB.DeletedBy = Session["UserName"].ToString();

                objDB.DeleteOtherExpense();
            }
       
            GetData();
        }
    }
}