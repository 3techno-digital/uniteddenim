﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class AddOnsexpense : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int AddOnsExpenseID
        {
            get
            {
                if (ViewState["AddOnsExpenseID"] != null)
                {
                    return (int)ViewState["AddOnsExpenseID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AddOnsExpenseID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();

                if (Session["TeamCount"].ToString() == "0")
                {
                    addButton.Visible = false;
                }


                GetData();

            }
        }

        private DataTable FilterData2(DataTable dt)
        {
            if (dt == null)
            {
                return dt;
            }

            DataTable dtFilter = new DataTable();
            DataTable dtTemp = new DataTable();
            dtTemp = Common.filterTable(dt, "Status", "Saved as Draft");
            dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());
            dtFilter = Common.ReversefilterTable(dt, "Status", "Saved as Draft");
            if (dtTemp != null)
                dtFilter.Merge(dtTemp);

            return dtFilter;
        }

        private void GetData()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            dt = objDB.GetAllAddOnsExpenseByEmployeeID(ref errorMsg);
            //gvAddOns.DataSource = FilterData2(dt);
            gvAddOns.DataSource = dt;
            gvAddOns.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvAddOns.UseAccessibleHeader = true;
                    gvAddOns.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All AddOns Viewed", "AddOns");

        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            int ID = Convert.ToInt32(btn.CommandArgument);
            objDB.AddOnsExpenseID = ID;
            objDB.DeletedBy = Session["UserName"].ToString();

            objDB.DeleteAddOnsExpenseByID();
            GetData();
        }

    }
}