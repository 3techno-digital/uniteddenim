﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS
{
    public partial class Dashboard : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                
                if (Session["UserAccess"].ToString() != "Normal User")
                {
                    Session["OldUserAccess"] = Session["UserAccess"];
                }
                
                Session["UserAccess"] = "Normal User";
                string CurrDate = DateTime.Now.ToString();
                //checkStatus();
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                BindBirthdayRepeater();
                GetAnnouncementsByCompanyID();
                DataTable dt = new DataTable();
                dt = objDB.GetESSDashboardWidgets(ref errorMsg);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lblPresent.Text = dt.Rows[0]["Present"].ToString();
                    lblAbsent.Text = dt.Rows[0]["Absent"].ToString();
                    lblEarlyOut.Text = dt.Rows[0]["ShortDuration"].ToString();
                    lblHalfDay.Text = dt.Rows[0]["HalfDay"].ToString();
                    lblLeave.Text = dt.Rows[0]["Leave"].ToString();
                    lblMissingPunches.Text = dt.Rows[0]["MissingPunches"].ToString();
                }
                Common.addlog("ViewAll", "ESS", "Dashboard Viewed", "");

            }
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        private void GetAnnouncementsByCompanyID()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetTop5AnnouncementByCompanyID(ref errorMsg);
            gvAnnouncement.DataSource = dt;
            gvAnnouncement.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvAnnouncement.UseAccessibleHeader = true;
                    gvAnnouncement.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All Announcement Viewed", "Announcement");
        }

        private void BindBirthdayRepeater()
        {
            DataTable dt = new DataTable();
            theCarousel.Visible = false;
            divBtnArrows.Visible = false;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetBirthdayOfNext7DaysByCompanyID(ref errorMsg);
            if (dt != null && dt.Rows.Count > 0)
            {
                repBirthday.DataSource = dt;
                repBirthday.DataBind();
                theCarousel.Visible = true;

                if (dt.Rows.Count > 3)
                {
                    divBtnArrows.Visible = true;
                }

            }

        }
    }
}