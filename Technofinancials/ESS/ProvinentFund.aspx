﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProvinentFund.aspx.cs" Inherits="Technofinancials.ESS.ProvinentFund" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">

               
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Provident Fund</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4" style="display:none;">
                                <div style="text-align: right;">
                                <a class="AD_btn" id="btnBack" runat="server">Back</a>

                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">

                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                           <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h4>Employee Contribution</h4>
                                <input type="text" class="form-control"  id="txtEmployeePF" runat="server" disabled="disabled" placeholder="0" />

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h4>Company Contribution</h4>
                                <input type="text" class="form-control" id="txtCompanyPF" runat="server" disabled="disabled" placeholder="0" />

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h4>Total</h4>
                                <input type="text" class="form-control"  id="txtTotalAmount" runat="server" disabled="disabled" placeholder="0" />

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h4>Withdraw</h4>
                                <input type="text" class="form-control"  id="txtWithdrawAmount" runat="server" disabled="disabled" placeholder="0"/>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h4>Total Balance</h4>
                                <input type="text" class="form-control"  id="txtRemaningAmount" runat="server" disabled="disabled" placeholder="0" />

                            </div>
                        </div>
                    </div>
                        </div>

                           <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="p1" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                                <div class="clearfix">&nbsp;</div>
                    

                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="content-header second_heading" style="display:none">
                                    <div class="container-fluid">
                                        <div class="row mb-2">
                                            <div class="col-sm-6">
                                                <h1 class="m-0 text-dark">Transaction Details</h1>
                                            </div>
                                            <!-- /.col -->

                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <!-- /.container-fluid -->
                    </div>
                  
                    <div class="row" style="display:none">
                        <div class="col-sm-12">

                          
                            <asp:GridView ID="gvProvinentFund" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("TransactionType") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblOrganization" Text='<%# Eval("Amount") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblJoiningYear" Text='<%# Eval("TransactionDate") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>




                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
</body>
</html>
