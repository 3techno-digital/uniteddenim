﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payroll.aspx.cs" Inherits="Technofinancials.ESS.Payroll" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">My Payroll </h1>
                            </div>
                            <!-- /.col -->
                            <%--<div class="col-sm-4">
                                <div style="text-align: right;">
                                    <a class="AD_btn" id="btnBack" runat="server">Back</a>

                                </div>
                            </div>--%>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>


                <section class="app-content">
                    <div class="row stock1Pad">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="col-sm-12 col-xs-12 form-group pl-0">
                                <h4><strong>Allowances</strong></h4>
                            </div>
                            <div class="table_out_data">
                                <label>
                                    Basic Salary 
                                </label>

                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator15" ControlToValidate="txtBasicSalary" ErrorMessage="Basic Salary Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                <input class="form-control" placeholder="0" type="text" id="txtBasicSalary" runat="server" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    Medical Allowance</label>
                                <input class="form-control" placeholder="0" disabled="disable" type="text" id="txtMedicalAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    House Rent Allowance</label>
                                <input class="form-control" placeholder="0" disabled="disable" type="text" id="txtHouseRentAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    Utility Allowance</label>
                                <input class="form-control" placeholder="0" type="text" disabled="disable" id="txtUtility" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    Maintenance  Allowance</label>
                                <input class="form-control" placeholder="0" disabled="disable" type="text" runat="server" id="txtMaintenanceAllowance" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    Food Allowance</label>
                                <input class="form-control" placeholder="0" type="text" disabled="disable" id="txtFoodAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    Fuel Allowance</label>
                                <input class="form-control" placeholder="0" disabled="disable" type="text" id="txtFuelAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    Other Allowances</label>
                                <input class="form-control" placeholder="0" type="text" disabled="disable" id="txtOtherAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    Gross Salary</label>
                                <input class="form-control" placeholder="0" type="text" disabled="disable" id="txtGrossSalary" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="col-sm-12 col-xs-12 form-group">
                                <h4><strong>Deductions</strong></h4>
                            </div>
                            <div class="table_out_data">
                                <label>
                                    EOBI</label>
                                <input class="form-control" placeholder="0" type="text" disabled="disable" id="txtEOBIAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    IESSI</label>
                                <input class="form-control" placeholder="0" type="text" disabled="disable" id="txtIESSI" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    Other Deduction</label>
                                <input class="form-control" placeholder="0" type="text" disabled="disable" id="txtOtherDeductions" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                            <div class="table_out_data">
                                <label>
                                    Total Deduction</label>
                                <input class="form-control" placeholder="0" type="text" disabled="disable" id="txtTotalDeduction" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row stock1Pad">
                        <div class="col-md-6 col-sm-12 col-lg-4">
                            <div class="table_out_data">
                                <label>
                                    Net Salary</label>
                                <input class="form-control" placeholder=" 0" type="text" disabled="disable" id="txtNetSalary" runat="server" clientidmode="static" />
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="exTab2" class="">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#1" data-toggle="tab">Additions</a>
                                    </li>
                                    <li><a href="#2" data-toggle="tab">Deductions</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane row active" id="1">
                                        <div class="col-lg-12">

                                            <asp:GridView ID="gvAdditions" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr. No">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Title">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Type">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblType" Text='<%# Eval("Type") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Amount/Per">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblAmount" Text='<%# Eval("Amount") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField FooterStyle-HorizontalAlign="Center">

                                                        <FooterTemplate>
                                                            <asp:Label ID="lblTotal" CssClass="total" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="tab-pane row" id="2">
                                        <div class="col-lg-12">

                                            <asp:GridView ID="gvDeductions" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr. No">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Title">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Type">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblType" Text='<%# Eval("Type") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Amount/Per">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblAmount" Text='<%# Eval("Amount") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField FooterStyle-HorizontalAlign="Center">

                                                        <FooterTemplate>
                                                            <asp:Label ID="lblTotal" CssClass="total" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <br />
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    <style>
        .form-group {
            padding-left: 0px !important;
        }

        .total {
            color: #003780;
            font-weight: 600;
        }

        @media only screen and (max-width: 1280px) and (min-width: 800px) {
            .table_out_data {
                width: 330px;
                font-size: 11px !important;
            }

            label {
                font-size: 11px !important;
            }
        }
    </style>
</body>
</html>
