﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Technofinancials.ESS
{
    public partial class Absents : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {                
                CheckSessions();
                BindEmployeeDropdown();
               
                DataTable dt = new DataTable();
                DateTime now = DateTime.Now;
                objDB.FromDate = new DateTime(now.Year, now.Month, 1).ToString("dd-MMM-yyyy");
                objDB.ToDate = new DateTime(now.Year, now.Month, 1).AddMonths(1).AddDays(-1).ToString("dd-MMM-yyyy");
                txtStartDate.Value = objDB.FromDate;
                txtEndDate.Value = objDB.ToDate;
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());

                //if (HttpContext.Current.Items["SelectedEmp"] != null)
                //{

                //    objDB.SelectedEmp = HttpContext.Current.Items["SelectedEmp"]?.ToString();
                //}
                if (HttpContext.Current.Items["AttendanceFilter"]!= null)
				{
                    objDB.Status = HttpContext.Current.Items["AttendanceFilter"]?.ToString();
				}
                GetAllAttendenceInformation();
                if (objDB.SelectedEmp == null || objDB.SelectedEmp == "")
                {
                    objDB.SelectedEmp = Session["EmployeeID"].ToString();
                }

                dt = objDB.GetAllEmployeeAttendenceSummaryByDate(ref errorMsg);

                if (dt != null && dt.Rows.Count > 0)
                {
                    DataView dv = dt.DefaultView;
                    dv.Sort = "AttDate DESC";
                    gv.DataSource = dv;
                    gv.DataBind();
                }
            }
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }
        private void GetAllAttendenceInformation()
        {
            DataTable dt = new DataTable();
            if(objDB.SelectedEmp ==null || objDB.SelectedEmp == "")
			{
                objDB.SelectedEmp = Session["EmployeeID"].ToString();
            }
			
         
            DateTime firstDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            objDB.FromDate = firstDay.ToString() ;
            objDB.ToDate = firstDay.AddMonths(1).AddDays(-1).ToString();
            //dt = objDB.GetAttendanceSummaryKTByEmployeeID(ref errorMsg);
            dt = objDB.GetESSDashboardDetailWidgets(ref errorMsg);
            if (dt != null && dt.Rows.Count > 0)
            {
                lblPresent.Text = dt.Rows[0]["Present"].ToString();
                lblAbsent.Text = dt.Rows[0]["Absent"].ToString();
                lblEarlyLeft.Text = dt.Rows[0]["EarlyOut"].ToString();
                lblLate.Text = dt.Rows[0]["Late"].ToString();
                lblMissingPunches.Text = dt.Rows[0]["MissingPunches"].ToString();
                //lblShortHours.Text = dt.Rows[0]["Late In"].ToString();
                lblHalfDay.Text = dt.Rows[0]["HalfDay"].ToString();
                lblWorkingHour.Text = dt.Rows[0]["wh"].ToString();
                lblExpectedHours.Text = dt.Rows[0]["ExpectedHour"].ToString();
                lblHoliday.Text = dt.Rows[0]["Holiday"].ToString();
                lblLeave.Text = dt.Rows[0]["Leave"].ToString();
                lblShortHour.Text = dt.Rows[0]["ShortHours"].ToString();
                lblWorkingDay.Text = dt.Rows[0]["WorkingDays"].ToString();

            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //string where = "";

            //if (ddlEmployee.SelectedValue != null && ddlEmployee.SelectedValue != "")
            //{
            //    if (where != "")
            //    {
            //        where += " And ";
            //    }

            //    where += "Employees.EmployeeID = " + ddlEmployee.SelectedValue.ToString();
            //}


            DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
            DateTime.TryParse(txtStartDate.Value, out Fdate);
            DateTime.TryParse(txtEndDate.Value, out Tdate);
            if (Tdate < Fdate)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = "From Date shoul be less than To Date";


                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('From Date shoul be less than To Date')", true);
                gv.DataSource = null;
                gv.DataBind();
                return;
            }
            if (Fdate > DateTime.Now)
            {

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('From Date shoul be Equal or less than Current Date')", true);
                gv.DataSource = null;
                gv.DataBind();
                return;
            }
            if ((Tdate - Fdate).Days > 31 || (Fdate.Day <= Tdate.Day && (Tdate.Month - Fdate.Month) > 0))
            {

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('date range should be of 1 month')", true);
                gv.DataSource = null;
                gv.DataBind();
                return;
            }
            if (ddlEmployee.SelectedIndex <= 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Employee Name Should be Provided')", true);
            gv.DataSource = null;
            gv.DataBind();
            return; 
            }

            DataTable dt = new DataTable();
            objDB.SelectedEmp = ddlEmployee.SelectedValue;
            objDB.FromDate = txtStartDate.Value;
            objDB.ToDate = txtEndDate.Value;
          //  objDB.WhereClause = where;
            //dt = objDB.GetAttendanceSummaryKTByEmployeeIDAndDate(ref errorMsg);
            dt = objDB.GetESSDashboardDetailWidgets(ref errorMsg);
            if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["wh"].ToString() != "")
            {
                //string WorkingHour =  (Convert.ToInt32(dt.Rows[0]["wh"].ToString()) < 0 ? 0 : Convert.ToInt32(dt.Rows[0]["wh"].ToString()).ToString()).ToString();
                string WorkingHour = dt.Rows[0]["wh"].ToString();
                lblPresent.Text = dt.Rows[0]["Present"].ToString();
                lblAbsent.Text = dt.Rows[0]["Absent"].ToString();
                lblEarlyLeft.Text = dt.Rows[0]["EarlyOut"].ToString();
                lblLate.Text = dt.Rows[0]["Late"].ToString();
                lblMissingPunches.Text = dt.Rows[0]["MissingPunches"].ToString();
                //lblShortHours.Text = dt.Rows[0]["Late In"].ToStri
                lblHalfDay.Text = dt.Rows[0]["HalfDay"].ToString();
                lblWorkingHour.Text = dt.Rows[0]["wh"].ToString();// WorkingHour.ToString();
                lblExpectedHours.Text = dt.Rows[0]["ExpectedHour"].ToString();
                lblHoliday.Text = dt.Rows[0]["Holiday"].ToString();
                lblLeave.Text = dt.Rows[0]["Leave"].ToString();
                lblShortHour.Text = (Convert.ToDouble(dt.Rows[0]["ShortHours"].ToString())).ToString();
                lblWorkingDay.Text = dt.Rows[0]["WorkingDays"].ToString();
            }
            DataTable dtEmployeSummary = new DataTable();
            dtEmployeSummary = objDB.GetAllEmployeeAttendenceSummaryByDate(ref errorMsg);
            gv.DataSource = dtEmployeSummary;
            gv.DataBind();
        }
        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void gvAttendanceSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblCheckInStatus = (e.Row.FindControl("lblCheckInStatus") as Label);
                Label lblCheckOutStatus = (e.Row.FindControl("lblCheckOutStatus") as Label);
                Label lblTimeout = (e.Row.FindControl("lblTimeout") as Label);
                Label lblShortHours = (e.Row.FindControl("lblShortHours") as Label);
                Label lblTimeIn = (e.Row.FindControl("lblTimeIn") as Label);

                if (lblCheckInStatus.Text == "Holiday")
                {
                    lblCheckInStatus.Text = "H";
                    lblCheckInStatus.CssClass = "weekend-option";
                }
                else if (lblCheckInStatus.Text == "OverTime")
                {
                    lblCheckInStatus.Text = "O";
                    lblCheckInStatus.CssClass = "overtime-option";
                }
                else if (lblCheckInStatus.Text == "Present" || lblCheckInStatus.Text == "On Time")
                {
                    if (lblShortHours.Text != "0")
                    {
                        lblTimeIn.Text = "S";
                        lblTimeIn.CssClass = "short-option";
                    }
                    
                    lblCheckInStatus.Text = "P";
                    lblCheckInStatus.CssClass = "present-option";
                }
                else if (lblCheckInStatus.Text == "Off")
                {
                    lblCheckInStatus.Text = "D";
                    lblCheckInStatus.CssClass = "day-option";
                }
                else if (lblCheckInStatus.Text == "Leave")
                {
                    lblCheckInStatus.Text = "L";
                    lblCheckInStatus.CssClass = "rest-option";
                }
                else if (lblCheckInStatus.Text == "Absent")
                {
                    lblCheckInStatus.Text = "A";
                    lblCheckInStatus.CssClass = "absent-option";
                }
                else if (lblCheckInStatus.Text == "Short Duration" || lblCheckInStatus.Text == "Early" || lblCheckInStatus.Text == "Early Out" || lblCheckInStatus.Text == "Late" || lblCheckInStatus.Text == "Late In")
                {
                    if (lblShortHours.Text != "0")
                    {
                        lblTimeIn.Text = "S";
                        lblTimeIn.CssClass = "short-option";
                    }

                    lblCheckInStatus.Text = "P";
                    lblCheckInStatus.CssClass = "present-option";
                }
                else if (lblCheckInStatus.Text == "Not Marked" || lblCheckInStatus.Text == "M")
                {
                    lblCheckInStatus.Text = "M";
                    lblCheckInStatus.CssClass = "missing-option";
                }

                if (lblCheckOutStatus.Text == "Holiday")
                {
                    lblCheckOutStatus.Text = "H";
                    lblCheckOutStatus.CssClass = "weekend-option";
                }
                else if (lblCheckOutStatus.Text == "OverTime")
                {
                    lblCheckOutStatus.Text = "O";
                    lblCheckOutStatus.CssClass = "overtime-option";
                }
                else if (lblCheckOutStatus.Text == "Present" || lblCheckOutStatus.Text == "On Time")
                {
                    if (lblShortHours.Text != "0")
                    {
                        lblTimeout.Text = "S";
                        lblTimeout.CssClass = "short-option";
                    }

                    lblCheckOutStatus.Text = "P";
                    lblCheckOutStatus.CssClass = "present-option";
                }
                else if (lblCheckOutStatus.Text == "Off")
                {
                    lblCheckOutStatus.Text = "D";
                    lblCheckOutStatus.CssClass = "day-option";
                }
                else if (lblCheckOutStatus.Text == "Leave")
                {
                    lblCheckOutStatus.Text = "L";
                    lblCheckOutStatus.CssClass = "rest-option";
                }
                else if (lblCheckOutStatus.Text == "Absent")
                {
                    lblCheckOutStatus.Text = "A";
                    lblCheckOutStatus.CssClass = "absent-option";
                }
                else if (lblCheckOutStatus.Text == "Short Duration" || lblCheckOutStatus.Text == "Early" || lblCheckOutStatus.Text == "Late")
                {
                    if (lblShortHours.Text != "0")
                    {
                        lblTimeout.Text = "S";
                        lblTimeout.CssClass = "short-option";
                    }

                    lblCheckOutStatus.Text = "P";
                    lblCheckOutStatus.CssClass = "present-option";
                }
                else if (lblCheckOutStatus.Text == "Not Marked" || lblCheckOutStatus.Text == "M")
                {
                    lblCheckOutStatus.Text = "M";
                    lblCheckOutStatus.CssClass = "missing-option";
                }
            }
        }
    }
}