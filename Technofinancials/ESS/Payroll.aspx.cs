﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.ESS
{
    public partial class Payroll : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int employeeID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                //btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employees";

                divAlertMsg.Visible = false;
                txtBasicSalary.Disabled = true;

                if (Session["EmployeeID"] != null)
                {
                    employeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                    getEmployeeByID(employeeID);

                }
            }
        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        private void getEmployeeAdditionsByEmployeeID(int employeeID)
        {
            DataTable dt = new DataTable();
            objDB.EmployeeID = employeeID;
            dt = objDB.GetAllEmployeeBudgetByEmployeeID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dt = Common.filterTable(dt, "TransictionType", "Additions");
                }
            }

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtJDAdditions.Rows[0][0].ToString() == "")
                    {
                        dtJDAdditions.Rows[0].Delete();
                        dtJDAdditions.AcceptChanges();
                        //showAdditionsFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtJDAdditions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                    }
                    AdditionsSrNo = Convert.ToInt32(dtJDAdditions.Rows[dtJDAdditions.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindAdditionsTable();
        }
        private void getEmployeeDeductionsByEmployeeID(int employeeID)
        {
            DataTable dt = new DataTable();
            objDB.EmployeeID = employeeID;
            dt = objDB.GetAllEmployeeBudgetByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dt = Common.filterTable(dt, "TransictionType", "Deductions");
                }
            }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtJDDeductions.Rows[0][0].ToString() == "")
                    {
                        dtJDDeductions.Rows[0].Delete();
                        dtJDDeductions.AcceptChanges();
                        //showDeductionsFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtJDDeductions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                    }
                    DeductionsSrNo = Convert.ToInt32(dtJDDeductions.Rows[dtJDDeductions.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            BindDeductionsTable();
        }

        private void getEmployeeByID(int EmpID)
        {
            objDB.EmployeeID = EmpID;
            DataTable dt = objDB.GetEmployeeByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtBasicSalary.Value = dt.Rows[0]["BasicSalary"].ToString();
                    txtFuelAllowance.Value = dt.Rows[0]["FuelAllowance"].ToString();
                    txtFoodAllowance.Value = dt.Rows[0]["FoodAllowance"].ToString();
                    txtMedicalAllowance.Value = dt.Rows[0]["MedicalAllowance"].ToString();
                    txtNetSalary.Value = dt.Rows[0]["NetSalary"].ToString();
                    txtHouseRentAllowance.Value = dt.Rows[0]["HouseAllownace"].ToString();
                    txtUtility.Value = dt.Rows[0]["UtitlityAllowance"].ToString();
                    txtEOBIAllowance.Value = dt.Rows[0]["EOBI"].ToString();
                    txtMaintenanceAllowance.Value = dt.Rows[0]["MaintenanceAllowance"].ToString();
                    txtMedicalAllowance.Value = dt.Rows[0]["MedicalAllowance"].ToString();
               //     txtEOBIAllowance.Value = dt.Rows[0]["MedicalAllowance"].ToString();
                    txtIESSI.Value = dt.Rows[0]["IESSI"].ToString();

                    AdditionsSrNo = 1;
                    //showAdditionsFirstRow = false;
                    dtJDAdditions = createJDAdditions();

                    DeductionsSrNo = 1;
                    //showDeductionsFirstRow = false;
                    dtJDDeductions = createJDAdditions();

                    getEmployeeAdditionsByEmployeeID(EmpID);
                    getEmployeeDeductionsByEmployeeID(EmpID);
              
                }
            }
            Common.addlog("ViewAll", "ESS", "Payroll Viewed", "");

        }




        private void getJDAdditionsByNodeID(int nodeID)
        {
            DataTable dt = new DataTable();
            objDB.NodeID = nodeID;
            dt = objDB.GetAllJDSBudgetByNodeID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dt = Common.filterTable(dt, "TransictionType", "Additions");
                }
            }

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtJDAdditions.Rows[0][0].ToString() == "")
                    {
                        dtJDAdditions.Rows[0].Delete();
                        dtJDAdditions.AcceptChanges();
                        //showAdditionsFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtJDAdditions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                    }
                    AdditionsSrNo = Convert.ToInt32(dtJDAdditions.Rows[dtJDAdditions.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindAdditionsTable();
        }
        private DataTable dtJDAdditions = new DataTable();
        // bool showAdditionsFirstRow = false;
        protected int AdditionsSrNo = 1;
        private DataTable createJDAdditions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("Type");
            dt.Columns.Add("Amount");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindAdditionsTable()
        {
            if (dtJDAdditions == null)
            {
                dtJDAdditions = createJDAdditions();
            }

            gvAdditions.DataSource = dtJDAdditions;
            gvAdditions.DataBind();

            //if (showAdditionsFirstRow)
            //    gvAdditions.Rows[0].Visible = true;
            //else
            //    gvAdditions.Rows[0].Visible = false;

            gvAdditions.UseAccessibleHeader = true;
            gvAdditions.HeaderRow.TableSection = TableRowSection.TableHeader;

            float total = 0;
            if (dtJDAdditions != null)
            {
                if (dtJDAdditions.Rows.Count > 0)
                {
                    for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
                    {
                        if (dtJDAdditions.Rows[i][3].ToString() != "")
                        {
                            if (txtBasicSalary.Value != "")
                            {
                                if (dtJDAdditions.Rows[i][2].ToString() == "Percentage")
                                {
                                    total += ((float.Parse(dtJDAdditions.Rows[i][3].ToString()) / 100) * float.Parse(txtBasicSalary.Value));
                                }
                                else
                                {
                                    total += float.Parse(dtJDAdditions.Rows[i][3].ToString());
                                }
                            }
                        }
                    }
                }
            }
            ((Label)gvAdditions.FooterRow.FindControl("lblTotal")).Text = "Total : " + string.Format("{0:f2}", total);
            txtOtherAllowance.Value = total.ToString();
            CalcullateSalaries();
        }



        private void CalcullateSalaries()
        {
            try
            {
                //txtBasicSalary.Value = txtCalcBasicSalary.Value;
                //txtHouseRentAllowance.Value = txtCalcHouseRent.Value;
                //txtUtility.Value = txtCalcUtilities.Value;

                float medicalAllowance = txtMedicalAllowance.Value != "" ? float.Parse(txtMedicalAllowance.Value) : 0;
                float otherAllowance = txtOtherAllowance.Value != "" ? float.Parse(txtOtherAllowance.Value) : 0;
                float MaintenanceAllowance = txtMaintenanceAllowance.Value != "" ? float.Parse(txtMaintenanceAllowance.Value) : 0;
                float foodAllowance = txtFoodAllowance.Value != "" ? float.Parse(txtFoodAllowance.Value) : 0;
                float travelAllowance =  0;
                float carAllowance =  0;
                float mobileAllowance = 0;
                float fuelAllowance = txtFuelAllowance.Value != "" ? float.Parse(txtFuelAllowance.Value) : 0;
                float utility = txtUtility.Value != "" ? float.Parse(txtUtility.Value) : 0;
                float houseRentAllowance = txtHouseRentAllowance.Value != "" ? float.Parse(txtHouseRentAllowance.Value) : 0;
                float basicSalry = txtBasicSalary.Value != "" ? float.Parse(txtBasicSalary.Value) : 0;

                float EOBI = txtEOBIAllowance.Value != "" ? float.Parse(txtEOBIAllowance.Value) : 0;
                float IESSI = txtIESSI.Value != "" ? float.Parse(txtIESSI.Value) : 0;
                float OtherDeductions = txtOtherDeductions.Value != "" ? float.Parse(txtOtherDeductions.Value) : 0;
                                                           
                float netSalary = medicalAllowance + otherAllowance + foodAllowance + travelAllowance + carAllowance + mobileAllowance + MaintenanceAllowance + fuelAllowance + utility + houseRentAllowance + basicSalry - EOBI - OtherDeductions - IESSI;
                txtNetSalary.Value = netSalary.ToString("N2");

                float grossSalary = medicalAllowance + otherAllowance + foodAllowance + travelAllowance + carAllowance + mobileAllowance + MaintenanceAllowance + fuelAllowance + utility + houseRentAllowance + basicSalry;
                float totDed = EOBI + IESSI + OtherDeductions;

                txtTotalDeduction.Value = totDed.ToString("N2");
                txtGrossSalary.Value = grossSalary.ToString("N2");
            }
            catch (Exception)
            {
            }
        }

        //protected void gvAdditions_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Footer)
        //    {
        //        Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
        //        txtSrNo.Text = AdditionsSrNo.ToString();
        //    }
        //}
        //protected void gvAdditions_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    int index = e.RowIndex;

        //    dtJDAdditions.Rows[index].SetField(1, ((TextBox)gvAdditions.Rows[e.RowIndex].FindControl("txtEditTitle")).Text);
        //    dtJDAdditions.Rows[index].SetField(2, ((DropDownList)gvAdditions.Rows[e.RowIndex].FindControl("ddlEditType")).SelectedItem.Text);
        //    dtJDAdditions.Rows[index].SetField(3, ((TextBox)gvAdditions.Rows[e.RowIndex].FindControl("txtEditAmount")).Text);

        //    dtJDAdditions.AcceptChanges();

        //    gvAdditions.EditIndex = -1;
        //    BindAdditionsTable();
        //}
        //protected void gvAdditions_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        //{
        //    gvAdditions.EditIndex = -1;
        //    BindAdditionsTable();
        //}
        //protected void gvAdditions_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    gvAdditions.EditIndex = e.NewEditIndex;
        //    BindAdditionsTable();
        //}
        //protected void lnkRemoveAdditions_Command(object sender, CommandEventArgs e)
        //{
        //    LinkButton lnk = (LinkButton)sender as LinkButton;
        //    string delSr = lnk.CommandArgument.ToString();
        //    for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
        //    {
        //        if (dtJDAdditions.Rows[i][0].ToString() == delSr)
        //        {
        //            dtJDAdditions.Rows[i].Delete();
        //            dtJDAdditions.AcceptChanges();
        //        }
        //    }
        //    for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
        //    {
        //        dtJDAdditions.Rows[i].SetField(0, i + 1);
        //        dtJDAdditions.AcceptChanges();
        //    }
        //    if (dtJDAdditions.Rows.Count < 1)
        //    {
        //        DataRow dr = dtJDAdditions.NewRow();
        //        dtJDAdditions.Rows.Add(dr);
        //        dtJDAdditions.AcceptChanges();
        //        showAdditionsFirstRow = false;
        //    }
        //    if (showAdditionsFirstRow)
        //        AdditionsSrNo = dtJDAdditions.Rows.Count + 1;
        //    else
        //        AdditionsSrNo = 1;

        //    BindAdditionsTable();
        //}
        //protected void btnAddAdditions_Click(object sender, EventArgs e)
        //{
        //    DataRow dr = dtJDAdditions.NewRow();
        //    dr[0] = AdditionsSrNo.ToString();
        //    dr[1] = ((TextBox)gvAdditions.FooterRow.FindControl("txtTitle")).Text;
        //    dr[2] = ((DropDownList)gvAdditions.FooterRow.FindControl("ddlType")).SelectedItem.Text;
        //    dr[3] = ((TextBox)gvAdditions.FooterRow.FindControl("txtAmount")).Text;
        //    dtJDAdditions.Rows.Add(dr);
        //    dtJDAdditions.AcceptChanges();


        //    if (dtJDAdditions.Rows[0][0].ToString() == "")
        //    {
        //        dtJDAdditions.Rows[0].Delete();
        //        dtJDAdditions.AcceptChanges();
        //        showAdditionsFirstRow = true;
        //    }


        //    AdditionsSrNo += 1;
        //    BindAdditionsTable();
        //    ((Label)gvAdditions.FooterRow.FindControl("txtSrNo")).Text = AdditionsSrNo.ToString();
        //}






        private void getJDDeductionsByNodeID(int nodeID)
        {
            DataTable dt = new DataTable();
            objDB.NodeID = nodeID;
            dt = objDB.GetAllJDSBudgetByNodeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dt = Common.filterTable(dt, "TransictionType", "Deductions");
                }
            }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtJDDeductions.Rows[0][0].ToString() == "")
                    {
                        dtJDDeductions.Rows[0].Delete();
                        dtJDDeductions.AcceptChanges();
                        //showDeductionsFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtJDDeductions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                    }
                    DeductionsSrNo = Convert.ToInt32(dtJDDeductions.Rows[dtJDDeductions.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindDeductionsTable();
        }
        private DataTable dtJDDeductions = new DataTable();
        // bool showDeductionsFirstRow = false;
        protected int DeductionsSrNo = 1;
        private DataTable createJDDeductions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("Type");
            dt.Columns.Add("Amount");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindDeductionsTable()
        {
            if (dtJDDeductions == null)
            {
                dtJDDeductions = createJDDeductions();
            }

            gvDeductions.DataSource = dtJDDeductions;
            gvDeductions.DataBind();

            //if (showDeductionsFirstRow)
            //    gvDeductions.Rows[0].Visible = true;
            //else
            //    gvDeductions.Rows[0].Visible = false;

            gvDeductions.UseAccessibleHeader = true;
            gvDeductions.HeaderRow.TableSection = TableRowSection.TableHeader;

            float total = 0;
            if (dtJDDeductions != null)
            {
                if (dtJDDeductions.Rows.Count > 0)
                {
                    for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
                    {
                        if (dtJDDeductions.Rows[i][3].ToString() != "")
                        {
                            if (txtBasicSalary.Value != "")
                            {
                                if (dtJDDeductions.Rows[i][2].ToString() == "Percentage")
                                {
                                    total += ((float.Parse(dtJDDeductions.Rows[i][3].ToString()) / 100) * float.Parse(txtBasicSalary.Value));
                                }
                                else
                                {
                                    total += float.Parse(dtJDDeductions.Rows[i][3].ToString());
                                }
                            }
                        }
                    }
                }
            }
            ((Label)gvDeductions.FooterRow.FindControl("lblTotal")).Text = "Total : " + string.Format("{0:f2}", total);
            txtOtherDeductions.Value = total.ToString();
            CalcullateSalaries();
        }
        //protected void gvDeductions_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Footer)
        //    {
        //        Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
        //        txtSrNo.Text = DeductionsSrNo.ToString();
        //    }
        //}
        //protected void gvDeductions_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    int index = e.RowIndex;

        //    dtJDDeductions.Rows[index].SetField(1, ((TextBox)gvDeductions.Rows[e.RowIndex].FindControl("txtEditTitle")).Text);
        //    dtJDDeductions.Rows[index].SetField(2, ((DropDownList)gvDeductions.Rows[e.RowIndex].FindControl("ddlEditType")).SelectedItem.Text);
        //    dtJDDeductions.Rows[index].SetField(3, ((TextBox)gvDeductions.Rows[e.RowIndex].FindControl("txtEditAmount")).Text);

        //    dtJDDeductions.AcceptChanges();

        //    gvDeductions.EditIndex = -1;
        //    BindDeductionsTable();
        //}
        //protected void gvDeductions_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        //{
        //    gvDeductions.EditIndex = -1;
        //    BindDeductionsTable();
        //}
        //protected void gvDeductions_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    gvDeductions.EditIndex = e.NewEditIndex;
        //    BindDeductionsTable();
        //}
        //protected void lnkRemoveDeductions_Command(object sender, CommandEventArgs e)
        //{
        //    LinkButton lnk = (LinkButton)sender as LinkButton;
        //    string delSr = lnk.CommandArgument.ToString();
        //    for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
        //    {
        //        if (dtJDDeductions.Rows[i][0].ToString() == delSr)
        //        {
        //            dtJDDeductions.Rows[i].Delete();
        //            dtJDDeductions.AcceptChanges();
        //        }
        //    }
        //    for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
        //    {
        //        dtJDDeductions.Rows[i].SetField(0, i + 1);
        //        dtJDDeductions.AcceptChanges();
        //    }
        //    if (dtJDDeductions.Rows.Count < 1)
        //    {
        //        DataRow dr = dtJDDeductions.NewRow();
        //        dtJDDeductions.Rows.Add(dr);
        //        dtJDDeductions.AcceptChanges();
        //        showDeductionsFirstRow = false;
        //    }
        //    if (showDeductionsFirstRow)
        //        DeductionsSrNo = dtJDDeductions.Rows.Count + 1;
        //    else
        //        DeductionsSrNo = 1;

        //    BindDeductionsTable();
        //}
        //protected void btnAddDeductions_Click(object sender, EventArgs e)
        //{
        //    DataRow dr = dtJDDeductions.NewRow();
        //    dr[0] = DeductionsSrNo.ToString();
        //    dr[1] = ((TextBox)gvDeductions.FooterRow.FindControl("txtTitle")).Text;
        //    dr[2] = ((DropDownList)gvDeductions.FooterRow.FindControl("ddlType")).SelectedItem.Text;
        //    dr[3] = ((TextBox)gvDeductions.FooterRow.FindControl("txtAmount")).Text;
        //    dtJDDeductions.Rows.Add(dr);
        //    dtJDDeductions.AcceptChanges();


        //    if (dtJDDeductions.Rows[0][0].ToString() == "")
        //    {
        //        dtJDDeductions.Rows[0].Delete();
        //        dtJDDeductions.AcceptChanges();
        //        showDeductionsFirstRow = true;
        //    }


        //    DeductionsSrNo += 1;
        //    BindDeductionsTable();
        //    ((Label)gvDeductions.FooterRow.FindControl("txtSrNo")).Text = DeductionsSrNo.ToString();
        //}

    }
}