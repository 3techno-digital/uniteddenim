﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard2.aspx.cs" Inherits="Technofinancials.ESS.Dashboard2" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>

        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <%--<asp:UpdatePanel ID="updpnl" runat="server">
            <ContentTemplate>--%>
        <main id="app-main" class="app-main">

            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="UpdatePanel10"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
                    <div class="wrap">
                        
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="content-header">
                                    <div class="container-fluid">
                                        <div class="row mb-2">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <h1 class="m-0 text-dark">Employee Self Service</h1>
                                                <!--<button type="button" onclick="startCount()">Start count</button>-->
                                                <%-- <label runat="server" id="txtH">0</label>
                                                : 
                                        <label runat="server" id="txtM">0</label>
                                                : 
                                        <label runat="server" id="txt">0</label>
                                                <asp:HiddenField ID="hdSec" Value="0" runat="server" />
                                                <asp:HiddenField ID="timer_is_on" Value="1" runat="server" />--%>
                                                <!--<button type="button" onclick="stopCount()">Stop count</button> -->

                                                <!-- HTML -->


                                            </div>

                                            <!-- /.col -->
                                            <div class="col-sm-4">
                                                <div style="text-align: right;">
                                                    <button class="AD_btn" id="btnTimeOut" runat="server" onserverclick="btnTimeOut_ServerClick" type="button">Time Out</button>
                                                    <button class="AD_btn" id="btnTimeIN" runat="server" onserverclick="btnTimeIN_ServerClick" type="button">Time In</button>
                                                    <button class="AD_btn" id="btnBreakTimeIN" runat="server" onserverclick="btnBreakTimeIN_ServerClick" type="button">Break In</button>
                                                    <button class="AD_btn" id="btnBreakTimeOut" runat="server" onserverclick="btnBreakTimeOut_ServerClick" type="button">Break Out</button>
                                                </div>
                                                <asp:HiddenField ID="hdSec" Value="0" runat="server" />
                                                <asp:HiddenField ID="timer_is_on" Value="1" runat="server" />

                                                <div class="modal fade M_set" id="myModal" role="dialog">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h1 class="m-0 text-dark">Late Reason</h1>
                                                                <div class="add_new">
                                                                    <button type="button" class="AD_btn" runat="server" id="btnSubmit" >Submit</button>
                                                                    <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                                                </div>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    <textarea id="txtReason" runat="server" rows="5" placeholder="Reason (max 100 characters).." maxlength="100" class="form-control"></textarea>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <!-- /.container-fluid -->
                                </div>
                           </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                    <ContentTemplate>
                                        </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnTimeIN" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <div id="updpnl">
                            <main id="app" class="app-main in">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="widget" style="height: 170px; margin-bottom: 0.5rem !important;">
                                            <header class="widget-header">
                                                <span class="pull-left">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                <h4 class="widget-title">Attendance Summary</h4>
                                            </header>
                                            <!-- .widget-header -->
                                            <hr class="widget-separator">
                                            <div class="widget-body">
                                                <div class="summary">
                                                    <div class="row">
                                                        <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendence-detail"); %>">
                                                            <div class="col-md-2 tile-wrapper count-up" style="background: #ED213A; /* fallback for old browsers */
                                                            background: -webkit-linear-gradient(to right, #93291E, #ED213A); /* chrome 10-25, safari 5.1-6 */
                                                            background: linear-gradient(to right, #93291E, #ED213A); /* w3c, ie 10+/ edge, firefox 16+, chrome 26+, opera 12+, safari 7+ */">
                                                                <div class="text-center ng-binding">Absents</div>
                                                                <span class="diamond-wrapper bg-error dker text-center">
                                                                    <span class="tile-with-double tile-body-single text-center">
                                                                        <i class="fa fa-users" aria-hidden="true"></i>
                                                                    </span>
                                                                    <h3 class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="7" data-speed="1500">
                                                                        <asp:Literal ID="lblAbsent" runat="server"></asp:Literal></h3>
                                                                </span>
                                                            </div>
                                                        </a>
                                                        <div class="col-md-2 tile-wrapper count-up" style="background: #642B73; /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #C6426E, #642B73); /* chrome 10-25, safari 5.1-6 */
background: linear-gradient(to right, #C6426E, #642B73); /* w3c, ie 10+/ edge, firefox 16+, chrome 26+, opera 12+, safari 7+ */">
                                                            <div class="text-center ng-binding">Missing Punches</div>
                                                            <span class="diamond-wrapper bg-error dker text-center">
                                                                <span class="tile-with-double tile-body-single text-center">
                                                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                                </span>
                                                                <h3 class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="0" data-speed="1000">0</h3>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-2 tile-wrapper count-up" style="background-image: linear-gradient(to right, #3cb9cb, #56c4d3, #6bcfda, #7fdae3, #92e5eb);">
                                                            <div class="text-center ng-binding">Short Duration</div>
                                                            <span class="diamond-wrapper bg-error dker text-center">
                                                                <span class="tile-with-double tile-body-single text-center">
                                                                    <i class="fa fa-line-chart" aria-hidden="true"></i>
                                                                </span>
                                                                <h3 class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="0" data-speed="1000">
                                                                    <asp:Literal ID="lblEarlyOut" runat="server"></asp:Literal></h3>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-2 tile-wrapper count-up" style="background-image: linear-gradient(to right, #3c90cb, #4c98cf, #5a9fd3, #67a7d8, #74afdc);">
                                                            <div class="text-center ng-binding">Late Arrivals</div>
                                                            <span class="diamond-wrapper bg-error dker text-center">
                                                                <span class="tile-with-double tile-body-single text-center">
                                                                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                                                                </span>
                                                                <h3 class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="0" data-speed="1000">
                                                                    <asp:Literal ID="lblLate" runat="server"></asp:Literal></h3>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-2 tile-wrapper count-up" style="background: #f46b45; /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #eea849, #f46b45); /* chrome 10-25, safari 5.1-6 */
background: linear-gradient(to right, #eea849, #f46b45); /* w3c, ie 10+/ edge, firefox 16+, chrome 26+, opera 12+, safari 7+ */">
                                                            <div class="text-center ng-binding">Leave</div>
                                                            <span class="diamond-wrapper bg-error dker text-center">
                                                                <span class="tile-with-double tile-body-single text-center">
                                                                    <i class="fa fa-file" aria-hidden="true"></i>
                                                                </span>
                                                                <h3 class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="0" data-speed="1000">
                                                                    <asp:Literal ID="lblLeave" runat="server"></asp:Literal></h3>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-2 tile-wrapper count-up" style="background: #52c234; /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #061700, #52c234); /* chrome 10-25, safari 5.1-6 */
background: linear-gradient(to right, #061700, #52c234); /* w3c, ie 10+/ edge, firefox 16+, chrome 26+, opera 12+, safari 7+ */">
                                                            <div class="text-center ng-binding">Present</div>
                                                            <span class="diamond-wrapper bg-error dker text-center">
                                                                <span class="tile-with-double tile-body-single text-center">
                                                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                                                </span>
                                                                <h3 class="m-0 attendance-report-value ng-scope timer count-title count-number counter-count" data-to="0" data-speed="1000">
                                                                    <asp:Literal ID="lblPresent" runat="server"></asp:Literal></h3>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--<div data-plugin="chart" data-options="{
                                                                                      tooltip : {
                                                                                        trigger: 'axis'
                                                                                      },
                                                                                      legend: {
                                                                                        data:['Time-In','Time-Out']
                                                                                      },
                                                                                      calculable : true,
                                                                                      xAxis : [
                                                                                        {
                                                                                          type : 'category',
                                                                                          data : ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']

                                                                                        }
                                                                                      ],
                                                                                      yAxis : [
                                                                                        {
                                                                                          type : 'value'
                                                                                        }
                                                                                      ],
                                                                                      series : [
                                                                                        {
                                                                                          name:'Time-In',
                                                                                          type:'line',
                                                                                          data:[9.0, 11.0, 9.0, 9.0, 9.0, , , , 10.0, 9.0, 9.0, 9.0,, , , 9.0, 9.0, 9.0, 9.0, , , 9.0, 9.0, 9.0,9.0, 9.0, , , 9.0, 9.0],
                                                                                          markPoint : {
                                                                                            data : [
                                                                                              {type : 'max', name: 'Max'},
                                                                                              {type : 'min', name: 'Min'}
                                                                                            ]
                                                                                          },
                                                                                          markLine : {
                                                                                            data : [
                                                                                              {type : 'average', name: 'Average'}
                                                                                            ]
                                                                                          }
                                                                                        },
                                                                                        {
                                                                                          name:'Time-Out',
                                                                                          type:'line',
                                                                                          data:[17.0, 18.0, 18.0, 17.0, 17.0, , , , 17.0, 17.0, 18.0, 17.0,, , , 17.0, 17.0, 17.0, 17.0, , , 17.0, 17.0, 17.0,17.0, 17.0,, , 17.0, 17.0],
                                                                                          markPoint : {
                                                                                            data : [
                                                                                              {type : 'max', name: 'Max'},
                                                                                              {type : 'min', name: 'Min'}
                                                                                            ]
                                                                                          },
                                                                                          markLine : {
                                                                                            data : [
                                                                                              {type : 'average', name : 'Average'}
                                                                                            ]
                                                                                          }
                                                                                        }
                                                                                      ]
                                                                                    }"
                                                                    style="height: 300px;">
                                                                </div>  -->
                                            </div>
                                            <!-- .widget-body -->
                                        </div>
                                    </div>
                                </div>
                            </main>
                        </div>
                        <!-- .widget -->
                        <section class="app-content">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="widget map_radius announcment_area" style="height: 240px;">
                                        <!-- <header class="widget-header">
                                            <h4 class="widget-title">Announcement
                                            </h4>
                                        </header>   -->
                                        <!-- .widget-header -->
                                        <!-- .widget-header -->
                                        <header class="widget-header">
                                            <h4 class="widget-title">Daily Timer</h4>
                                            </span>
                                        </header>
                                        <!-- .widget-header -->
                                        <!-- .widget-header -->
                                        <hr class="widget-separator" />
                                        <div class="widget-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="col-md-3">
                                                        <div class="time">
                                                            <h4>Time In</h4>
                                                            <span>
                                                                <label runat="server" id="txtH">0</label>
                                                                : 
                                        <label runat="server" id="txtM">0</label>
                                                                : 
                                        <label runat="server" id="txt">0</label>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="time">
                                                            <h4>Break In</h4>
                                                            <span>
                                                                <label runat="server" id="Label1">0</label>
                                                                : 
                                                            <label runat="server" id="Label2">0</label>
                                                                : 
                                                            <label runat="server" id="Label3">0</label>
                                                                <asp:HiddenField ID="HiddenField3" Value="0" runat="server" />
                                                                <asp:HiddenField ID="HiddenField4" Value="1" runat="server" />
                                                        </div>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="time">
                                                            <h4>Break Out</h4>
                                                            <span>
                                                                <label runat="server" id="Label4">0</label>
                                                                : 
                                                              <label runat="server" id="Label5">0</label>
                                                                : 
                                                              <label runat="server" id="Label6">0</label>
                                                                <asp:HiddenField ID="HiddenField5" Value="0" runat="server" />
                                                                <asp:HiddenField ID="HiddenField6" Value="1" runat="server" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="time" style="border-right: none !important;">
                                                            <h4>Time Out</h4>
                                                            <span>
                                                                <label runat="server" id="Label7">0</label>
                                                                : 
                                                           <label runat="server" id="Label8">0</label>
                                                                : 
                                                           <label runat="server" id="Label9">0</label>
                                                                <asp:HiddenField ID="HiddenField7" Value="0" runat="server" />
                                                                <asp:HiddenField ID="HiddenField8" Value="1" runat="server" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="widget-body">
                                            <div class="table-responsive anounceTab" id="style-1">
                                                <asp:GridView ID="gvAnnouncement" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Announcement Date">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblCol6" Text='<%# Eval("Date") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("AnnouncementTitle") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Description")  %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>    -->
                                        <!-- .widget-body -->
                                    </div>
                                </div>
                                <div class="col-sm-4 ">
                                    <div class="widget map_radius" style="height: 240px;">
                                        <header class="widget-header">
                                            <span class="custom-font">
                                                <i class="fa fa-clock-o" style="color: #003780"></i>
                                                <h4 class="widget-title">System Time</h4>
                                            </span>
                                            <ul class="control">
                                                <li>
                                                    <a>
                                                        <span class="minimize">
                                                            <i class="fa fa-angle-down" name="angle-down"></i>
                                                        </span>
                                                        <span class="expand">
                                                            <i class="fa fa-angle-up" name="angle-up"></i>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </header>

                                        <!--<header class="widget-header">
                                            <h4 class="widget-title">Attendence</h4>
                                        </header>   -->
                                        <!-- .widget-header -->
                                        <div id="clock-pad" class="bg-grey" style="">
                                            <div class="row">
                                                <div class="widget-body">
                                                    <div id="MyClockDisplay" class="clock" onload="showTime()"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- .widget-body -->
                                    </div>
                                    <!-- .widget -->
                                </div>
                            </div>
                            <main id="app-carousel" class="app-carousel in">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="widget" style="height: 220px; margin-bottom: 0.5rem !important;">
                                            <header class="widget-header">
                                                <span class="pull-left">
                                                    <i class="fa fa-gift" aria-hidden="true"></i></span>
                                                <h4 class="widget-title">Birthday</h4>
                                            </header>
                                            <!-- .widget-header -->
                                            <hr class="widget-separator">
                                            <div class="row">
                                                <div class="widget-body carousel-item">
                                                    <asp:Repeater ID="repBirthday" runat="server">
                                                        <ItemTemplate>
                                                            <div class="col-md-2">
                                                                <div class="carousel-caption d-md-block">
                                                                    <img ng-src='<%# Eval("EmployeePhoto") %>' class="birthday-carousel-img img-100" src='<%# Eval("EmployeePhoto") %>'>
                                                                </div>
                                                                <h5>
                                                                    <strong class="ng-binding"><%#Eval("EmployeeFirstName").ToString()+' '+Eval("EmployeeMiddleName").ToString()+' '+Eval("EmployeeLastName").ToString()  %></strong>
                                                                </h5>
                                                                <p class="ng-binding"><%# Convert.ToDateTime(Eval("DOB")).ToString("MMMM dd") %></p>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <%--                                                    <div class="col-md-2">
                                                        <div class="carousel-caption d-md-block">
                                                            <img ng-src="/assets/images/arnold-avatar.jpg" class="birthday-carousel-img img-100" src="/assets/images/arnold-avatar.jpg">
                                                        </div>
                                                        <h5>
                                                            <strong class="ng-binding">Wallis Watson</strong>
                                                        </h5>
                                                        <p class="ng-binding">November 13</p>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="carousel-caption d-md-block">
                                                            <img ng-src="/assets/images/arnold-avatar.jpg" class="birthday-carousel-img img-100" src="/assets/images/arnold-avatar.jpg">
                                                        </div>
                                                        <h5>
                                                            <strong class="ng-binding">Wallis Watson</strong>
                                                        </h5>
                                                        <p class="ng-binding">November 13</p>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="carousel-caption d-md-block">
                                                            <img ng-src="/assets/images/arnold-avatar.jpg" class="birthday-carousel-img img-100" src="/assets/images/arnold-avatar.jpg">
                                                        </div>
                                                        <h5>
                                                            <strong class="ng-binding">Wallis Watson</strong>
                                                        </h5>
                                                        <p class="ng-binding">November 13</p>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="carousel-caption d-md-block">
                                                            <img ng-src="/assets/images/arnold-avatar.jpg" class="birthday-carousel-img img-100" src="/assets/images/arnold-avatar.jpg">
                                                        </div>
                                                        <h5>
                                                            <strong class="ng-binding">Wallis Watson</strong>
                                                        </h5>
                                                        <p class="ng-binding">November 13</p>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="carousel-caption d-md-block">
                                                            <img ng-src="/assets/images/arnold-avatar.jpg" class="birthday-carousel-img img-100" src="/assets/images/arnold-avatar.jpg">
                                                        </div>
                                                        <h5>
                                                            <strong class="ng-binding">Wallis Watson</strong>
                                                        </h5>
                                                        <p class="ng-binding">November 13</p>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="carousel-caption d-md-block">
                                                            <img ng-src="/assets/images/arnold-avatar.jpg" class="birthday-carousel-img img-100" src="/assets/images/arnold-avatar.jpg">
                                                        </div>
                                                        <h5>
                                                            <strong class="ng-binding">Wallis Watson</strong>
                                                        </h5>
                                                        <p class="ng-binding">November 13</p>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="carousel-caption d-md-block">
                                                            <img ng-src="/assets/images/arnold-avatar.jpg" class="birthday-carousel-img img-100" src="/assets/images/arnold-avatar.jpg">
                                                        </div>
                                                        <h5>
                                                            <strong class="ng-binding">Wallis Watson</strong>
                                                        </h5>
                                                        <p class="ng-binding">November 13</p>
                                                    </div>--%>
                                                </div>
                                            </div>
                                            <!-- .widget-body -->
                                        </div>
                                    </div>
                                </div>
                            </main>
                            <div class="col-sm-6 D_NONE">

                                <div class="widget map_radius" style="height: 400px;">
                                    <header class="widget-header">
                                        <h4 class="widget-title">Calendar
                                        </h4>
                                    </header>
                                    <!-- .widget-header -->
                                    <hr class="widget-separator">
                                    <div class="widget-body">
                                        <asp:GridView ID="gv" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                            <Columns>

                                                <asp:TemplateField HeaderText="Day">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("DayName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Start Time">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("StartTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Break Start Time">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("BreakStartTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Break End Time">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("BreakEndTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="End Time">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("EndTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <!-- .widget-body -->
                                </div>

                                <%--<div class="row">
                                        <div class="col-sm-12">
                                           
                                        </div>
                                    </div>--%>
                            </div>
                            <div class="col-sm-6 D_NONE">

                                <div class="widget map_radius" style="height: 400px;">
                                    <header class="widget-header">
                                        <h4 class="widget-title">Basic Information
                                        </h4>
                                    </header>
                                    <!-- .widget-header -->
                                    <hr class="widget-separator">
                                    <div class="widget-body">
                                        <table class="   table-responsive table-bordered">
                                            <tr>
                                                <td style="font-weight: bold">Shift Name:
                                                </td>
                                                <td id="tdShiftName" runat="server"></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold">Start Time:
                                                </td>
                                                <td id="tdStartTime" runat="server"></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold">End Time:
                                                </td>
                                                <td id="tdEndTime" runat="server"></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold">Late in:
                                                </td>
                                                <td id="tdLateIn" runat="server"></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold">Half Day:
                                                </td>
                                                <td id="tdHalfDay" runat="server"></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold">Early Out:
                                                </td>
                                                <td id="tdEarlyOut" runat="server"></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold">Working Days:
                                                </td>
                                                <td id="tdWorkingDays" runat="server"></td>
                                            </tr>
                                        </table>

                                    </div>
                                    <!-- .widget-body -->
                                </div>

                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="clearfix">&nbsp;</div>
                    </div>

                            <div class="row" style="display: none">
                                <div class="col-md-6">
                                    <div class="widget">
                                        <header class="widget-header">
                                            <h4 class="widget-title">Leaves Gauge</h4>
                                        </header>
                                        <!-- .widget-header -->
                                        <hr class="widget-separator">
                                        <div class="widget-body">
                                            <%-- <div data-plugin='chart' data-options='{toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: "Leaves",type: "gauge",data: [{value: 50, name: "Completed"}]}]}' style='height: 300px;'>
            </div>--%>

                                            <asp:Literal ID="ltrLeaves" runat="server"></asp:Literal>




                                        </div>
                                        <!-- .widget-body -->
                                    </div>


                                </div>
                                <div class="col-md-6">
                                    <div class="widget">
                                        <header class="widget-header">
                                            <h4 class="widget-title">Loans</h4>
                                        </header>
                                        <!-- .widget-header -->
                                        <hr class="widget-separator">
                                        <div class="widget-body">

                                            <asp:Literal ID="ltrPFChart" runat="server"></asp:Literal>


                                        </div>
                                        <!-- .widget-body -->
                                    </div>
                                </div>
                                <!-- .widget -->
                            </div>
                            <div class="clearfix">&nbsp;</div>

                            <div class="clearfix">&nbsp;</div>
                                </div>
                                <!-- END column -->


                                <div class="col-md-12 D_NONE">
                                    <div class="widget map_radius">
                                        <header class="widget-header">
                                            <h4 class="widget-title">Attendance
                                            </h4>
                                        </header>
                                        <!-- .widget-header -->
                                        <hr class="widget-separator">
                                        <div class="widget-body">

                                            <div id="LeavesGuage"></div>
                                        </div>
                                        <!-- .widget-body -->
                                    </div>

                                </div>

                                <div class="col-md-12 D_NONE">
                                    <div class="widget map_radius">
                                        <header class="widget-header">
                                            <h4 class="widget-title">Attendance
                                            </h4>
                                        </header>
                                        <!-- .widget-header -->
                                        <hr class="widget-separator">
                                        <div class="widget-body">

                                            <div id="chartdiv"></div>
                                        </div>
                                        <!-- .widget-body -->
                                    </div>

                                </div>


                                <!-- .widget -->
                            </div>

                        </section>
                    </div>
                    <uc:Footer ID="footer1" runat="server"></uc:Footer>
                </main>
        <%--            </ContentTemplate>
        </asp:UpdatePanel>--%>

        <style>
            #chartdiv {
                width: 100%;
                height: 500px;
            }
        </style>
        <!-- Resources -->
        <script src="https://www.amcharts.com/lib/4/core.js"></script>
        <script src="https://www.amcharts.com/lib/4/charts.js"></script>
        <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
        <script src="https://www.amcharts.com/lib/4/core.js"></script>
        <script src="https://www.amcharts.com/lib/4/charts.js"></script>
        <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script>


</script>

        <!-- Chart code -->
        <script>

            function timedCount() {
                if (document.getElementById("timer_is_on").value == '1') {
                    var c = parseInt(document.getElementById("hdSec").value);
                    document.getElementById("txtH").innerHTML = Math.floor(c / 3600);
                    document.getElementById("txtM").innerHTML = Math.floor((c / 60) % 60);
                    document.getElementById("txt").innerHTML = Math.floor(c % 60);
                    c = c + 1;
                    document.getElementById("hdSec").value = c;
                    setTimeout(timedCount, 1000);
                }

            }

            function pageLoad(sender, args) {
                console.log(document.getElementById("timer_is_on").value);
                timedCount();
            }

            am4core.ready(function () {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart instance
                var chart = am4core.create("chartdiv", am4charts.XYChart3D);

                // Add data
                chart.data = [{
                    "country": "1",
                    "visits": 10
                }, {
                    "country": "2",
                    "visits": 9
                }, {
                    "country": "3",
                    "visits": 9
                }, {
                    "country": "4",
                    "visits": 10
                }, {
                    "country": "5",
                    "visits": 11
                }, {
                    "country": "6",
                    "visits": 10
                }, {
                    "country": "7",
                    "visits": 10
                }, {
                    "country": "8",
                    "visits": 10
                }, {
                    "country": "9",
                    "visits": 10
                }, {
                    "country": "10",
                    "visits": 10
                }, {
                    "country": "11",
                    "visits": 10
                }, {
                    "country": "12",
                    "visits": 10
                }, {
                    "country": "13",
                    "visits": 10
                }, {
                    "country": "14",
                    "visits": 10
                }, {
                    "country": "15",
                    "visits": 10
                }, {
                    "country": "16",
                    "visits": 12
                }, {
                    "country": "17",
                    "visits": 10
                }, {
                    "country": "18",
                    "visits": 10
                }, {
                    "country": "19",
                    "visits": 10
                }, {
                    "country": "20",
                    "visits": 10
                }, {
                    "country": "21",
                    "visits": 10
                }, {
                    "country": "22",
                    "visits": 10
                }, {
                    "country": "23",
                    "visits": 10
                }, {
                    "country": "24",
                    "visits": 10
                }, {
                    "country": "25",
                    "visits": 10
                }, {
                    "country": "26",
                    "visits": 10
                }, {
                    "country": "27",
                    "visits": 10
                }, {
                    "country": "28",
                    "visits": 10
                }, {
                    "country": "29",
                    "visits": 10
                }, {
                    "country": "3017",
                    "visits": 10
                }];

                // Create axes
                let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "country";
                categoryAxis.renderer.labels.template.rotation = 270;
                categoryAxis.renderer.labels.template.hideOversized = false;
                categoryAxis.renderer.minGridDistance = 20;
                categoryAxis.renderer.labels.template.horizontalCenter = "right";
                categoryAxis.renderer.labels.template.verticalCenter = "middle";
                categoryAxis.tooltip.label.rotation = 270;
                categoryAxis.tooltip.label.horizontalCenter = "right";
                categoryAxis.tooltip.label.verticalCenter = "middle";

                let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.title.text = "Time In";
                valueAxis.title.fontWeight = "bold";

                // Create series
                var series = chart.series.push(new am4charts.ColumnSeries3D());
                series.dataFields.valueY = "visits";
                series.dataFields.categoryX = "country";
                series.name = "Visits";
                series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
                series.columns.template.fillOpacity = .8;

                var columnTemplate = series.columns.template;
                columnTemplate.strokeWidth = 2;
                columnTemplate.strokeOpacity = 1;
                columnTemplate.stroke = am4core.color("#FFFFFF");

                columnTemplate.adapter.add("fill", function (fill, target) {
                    return chart.colors.getIndex(target.dataItem.index);
                })

                columnTemplate.adapter.add("stroke", function (stroke, target) {
                    return chart.colors.getIndex(target.dataItem.index);
                })

                chart.cursor = new am4charts.XYCursor();
                chart.cursor.lineX.strokeOpacity = 0;
                chart.cursor.lineY.strokeOpacity = 0;

            }); // end am4core.ready()
        </script>



        <script>
            function openModal() {
                $('#myModal').modal('show');

            }
        </script>

        <script type="text/javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        if (e.get_error() != null) {
                            var ex = e.get_error();
                            var mesg = "HttpStatusCode: " + ex.httpStatusCode;
                            mesg += "<br />Name: " + ex.name;
                            mesg += "<br />Message: " + ex.message;
                            mesg += "<br />Description: " + ex.description;
                            console.log(mesg);
                        }
                    }
                });
            };
        </script>

        <script>
            am4core.ready(function () {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end

                var chart = am4core.create("LeavesGuage", am4charts.XYChart);
                chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

                chart.data = [
                    {
                        category: "Jan",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },
                    {
                        category: "Feb",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },
                    {
                        category: "Mar",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },

                    {
                        category: "Apr",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },
                    {
                        category: "May",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },
                    {
                        category: "Jun",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },
                    {
                        category: "july",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },
                    {
                        category: "Aug",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },
                    {
                        category: "Sep",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },
                    {
                        category: "Oct",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },
                    {
                        category: "Nov",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    },
                    {
                        category: "Dec",
                        value1: 2,
                        value2: 12,
                        value3: 20,
                        value4: 26,
                        value5: 22,
                        value6: 13
                    }

                ];

                chart.colors.step = 2;
                chart.padding(30, 30, 10, 30);
                chart.legend = new am4charts.Legend();

                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.grid.template.location = 0;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.min = 0;
                valueAxis.max = 100;
                valueAxis.strictMinMax = true;
                valueAxis.calculateTotals = true;
                valueAxis.renderer.minWidth = 50;


                var series1 = chart.series.push(new am4charts.ColumnSeries());
                series1.columns.template.width = am4core.percent(80);
                series1.columns.template.tooltipText =
                    "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                series1.name = "Prenset";
                series1.dataFields.categoryX = "category";
                series1.dataFields.valueY = "value1";
                series1.dataFields.valueYShow = "totalPercent";
                series1.dataItems.template.locations.categoryX = 0.5;
                series1.stacked = true;
                series1.tooltip.pointerOrientation = "vertical";

                var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
                bullet1.interactionsEnabled = false;
                bullet1.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                bullet1.label.fill = am4core.color("#ffffff");
                bullet1.locationY = 0.5;

                var series2 = chart.series.push(new am4charts.ColumnSeries());
                series2.columns.template.width = am4core.percent(80);
                series2.columns.template.tooltipText =
                    "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                series2.name = "Casual Leaves";
                series2.dataFields.categoryX = "category";
                series2.dataFields.valueY = "value2";
                series2.dataFields.valueYShow = "totalPercent";
                series2.dataItems.template.locations.categoryX = 0.5;
                series2.stacked = true;
                series2.tooltip.pointerOrientation = "vertical";

                var bullet2 = series2.bullets.push(new am4charts.LabelBullet());
                bullet2.interactionsEnabled = false;
                bullet2.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                bullet2.locationY = 0.5;
                bullet2.label.fill = am4core.color("#ffffff");

                var series3 = chart.series.push(new am4charts.ColumnSeries());
                series3.columns.template.width = am4core.percent(80);
                series3.columns.template.tooltipText =
                    "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                series3.name = "Sick Leaves";
                series3.dataFields.categoryX = "category";
                series3.dataFields.valueY = "value3";
                series3.dataFields.valueYShow = "totalPercent";
                series3.dataItems.template.locations.categoryX = 0.5;
                series3.stacked = true;
                series3.tooltip.pointerOrientation = "vertical";

                var bullet3 = series3.bullets.push(new am4charts.LabelBullet());
                bullet3.interactionsEnabled = false;
                bullet3.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                bullet3.locationY = 0.5;
                bullet3.label.fill = am4core.color("#ffffff");


                var series4 = chart.series.push(new am4charts.ColumnSeries());
                series4.columns.template.width = am4core.percent(80);
                series4.columns.template.tooltipText =
                    "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                series4.name = "Earned Leaves";
                series4.dataFields.categoryX = "category";
                series4.dataFields.valueY = "value4";
                series4.dataFields.valueYShow = "totalPercent";
                series4.dataItems.template.locations.categoryX = 0.5;
                series4.stacked = true;
                series4.tooltip.pointerOrientation = "vertical";

                var bullet4 = series4.bullets.push(new am4charts.LabelBullet());
                bullet4.interactionsEnabled = false;
                bullet4.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                bullet4.locationY = 0.5;
                bullet4.label.fill = am4core.color("#ffffff");


                var series5 = chart.series.push(new am4charts.ColumnSeries());
                series5.columns.template.width = am4core.percent(80);
                series5.columns.template.tooltipText =
                    "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                series5.name = "Annual Leaves";
                series5.dataFields.categoryX = "category";
                series5.dataFields.valueY = "value5";
                series5.dataFields.valueYShow = "totalPercent";
                series5.dataItems.template.locations.categoryX = 0.5;
                series5.stacked = true;
                series5.tooltip.pointerOrientation = "vertical";

                var bullet5 = series5.bullets.push(new am4charts.LabelBullet());
                bullet5.interactionsEnabled = false;
                bullet5.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                bullet5.locationY = 0.5;
                bullet5.label.fill = am4core.color("#ffffff");

                var series6 = chart.series.push(new am4charts.ColumnSeries());
                series6.columns.template.width = am4core.percent(80);
                series6.columns.template.tooltipText =
                    "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                series6.name = "Leaves without Pay";
                series6.dataFields.categoryX = "category";
                series6.dataFields.valueY = "value6";
                series6.dataFields.valueYShow = "totalPercent";
                series6.dataItems.template.locations.categoryX = 0.5;
                series6.stacked = true;
                series6.tooltip.pointerOrientation = "vertical";

                var bullet6 = series6.bullets.push(new am4charts.LabelBullet());
                bullet6.interactionsEnabled = false;
                bullet6.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                bullet6.locationY = 0.5;
                bullet6.label.fill = am4core.color("#ffffff");

                chart.scrollbarX = new am4core.Scrollbar();

            }); // end am4core.ready()
        </script>
        <script>
            am4core.ready(function () {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart
                var chart = am4core.create("PieChart", am4charts.PieChart);
                chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

                chart.data = [
                    {
                        country: "Casual Leaves",
                        value: 260
                    },
                    {
                        country: "Sick Leaves",
                        value: 230
                    },
                    {
                        country: "Earned Leaves",
                        value: 200
                    },
                    {
                        country: "Annual Leaves",
                        value: 165
                    },
                    {
                        country: "Leaves without Pay",
                        value: 139
                    }
                ];

                var series = chart.series.push(new am4charts.PieSeries());
                series.dataFields.value = "value";
                series.dataFields.radiusValue = "value";
                series.dataFields.category = "country";
                series.slices.template.cornerRadius = 6;
                series.colors.step = 3;

                series.hiddenState.properties.endAngle = -90;

                chart.legend = new am4charts.Legend();

            }); // end am4core.ready()
        </script>


        <style>
            #LeavesGuage {
                width: 100%;
                height: 500px;
            }

            #PieChart {
                width: 100%;
                height: 500px;
            }

            #LeavesPieChart {
                width: 100%;
                height: 500px;
            }
        </style>

        <style>
            .widget-body {
                padding-top: 0;
            }

            .calender {
            }

            .calenderHeading {
                text-align: center;
                vertical-align: middle;
                padding: 10px;
            }

            .calenderCell {
                text-align: center;
                vertical-align: middle;
                padding: 10px;
            }

            .wrap.p-t-0.tf-navbar.tf-footer-wrapper {
                right: 0 !important;
            }



            g[aria-labelledby="id-66-title"] {
                display: none;
            }
        </style>
        <%--   <script>
            
               jQuery(document).ready(function(){
                    $(".calenderCell").each(function(){
                        if((this)[0].html == '') 
                            jQuery(this)[0].hide();
                    });
                });
        </script>--%>


        <script>
            am4core.ready(function () {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart instance
                var chart = am4core.create("LeavesPieChart", am4charts.PieChart);

                // Add and configure Series
                var pieSeries = chart.series.push(new am4charts.PieSeries());
                pieSeries.dataFields.value = "litres";
                pieSeries.dataFields.category = "country";

                // Let's cut a hole in our Pie chart the size of 30% the radius
                chart.innerRadius = am4core.percent(30);

                // Put a thick white border around each Slice
                pieSeries.slices.template.stroke = am4core.color("#fff");
                pieSeries.slices.template.strokeWidth = 2;
                pieSeries.slices.template.strokeOpacity = 1;
                pieSeries.slices.template
                    // change the cursor on hover to make it apparent the object can be interacted with
                    .cursorOverStyle = [
                        {
                            "property": "cursor",
                            "value": "pointer"
                        }
                    ];

                pieSeries.alignLabels = false;
                pieSeries.labels.template.bent = true;
                pieSeries.labels.template.radius = 3;
                pieSeries.labels.template.padding(0, 0, 0, 0);

                pieSeries.ticks.template.disabled = true;

                // Create a base filter effect (as if it's not there) for the hover to return to
                var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
                shadow.opacity = 0;

                // Create hover state
                var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

                // Slightly shift the shadow and make it more prominent on hover
                var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
                hoverShadow.opacity = 0.7;
                hoverShadow.blur = 5;

                // Add a legend
                chart.legend = new am4charts.Legend();

                chart.data = [{
                    "country": "Casual Leaves",
                    "litres": 501.9
                }, {
                    "country": "Sick Leaves",
                    "litres": 165.8
                }, {
                    "country": "Earned Leaves",
                    "litres": 139.9
                }, {
                    "country": "Annual Leaves",
                    "litres": 128.3
                }, {
                    "country": "Leaves without Pay",
                    "litres": 99
                }];



            }); // end am4core.ready()
        </script>

        <script>
            $('.counter-count').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {

                    //chnage count up speed here
                    duration: 1500,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        </script>

        <script>
            function showTime() {
                var date = new Date();
                var h = date.getHours(); // 0 - 23
                var m = date.getMinutes(); // 0 - 59
                var s = date.getSeconds(); // 0 - 59
                var session = "";

                if (h == 0) {
                    h = 12;
                }

                if (h > 12) {
                    h = h - 12;
                    session = "";
                }

                h = (h < 10) ? "0" + h : h;
                m = (m < 10) ? "0" + m : m;
                s = (s < 10) ? "0" + s : s;

                var time = h + ":" + m + ":" + s + " " + session;
                document.getElementById("MyClockDisplay").innerText = time;
                document.getElementById("MyClockDisplay").textContent = time;

                setTimeout(showTime, 1000);

            }

            showTime();

        </script>
        <script>
            $('.CalP').prop('title', 'Present');
            $('.CalOff').prop('title', 'Off/Holiday');
            $('.CalC').prop('title', 'On Casual Leave');
            $('.CalS').prop('title', 'On Sick Leave');
        </script>
        <!-- HTML -->


    </form>

</body>


</html>