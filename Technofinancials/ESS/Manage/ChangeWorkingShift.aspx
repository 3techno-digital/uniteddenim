﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangeWorkingShift.aspx.cs" Inherits="Technofinancials.ESS.Manage.ChangeWorkingShift" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">
			<asp:UpdateProgress ID="updProgress"
				AssociatedUpdatePanelID="upd1"
				runat="server">
				<ProgressTemplate>
					<div class="upd_panel">
						<div class="center">
							<img src="/assets/images/Loading.gif" />
						</div>
					</div>
				</ProgressTemplate>
			</asp:UpdateProgress>

			<div class="wrap">
				<asp:UpdatePanel ID="upd1" runat="server">
					<ContentTemplate>
						<div class="content-header">
							<div class="container-fluid">
								<div class="row mb-2">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<h1 class="m-0 text-dark">Change Shift</h1>
									</div>
									<!-- /.col -->
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
										<div style="text-align: right;">
											<button class="AD_btn" id="btnSubmit" runat="server" onserverclick="btnSave_ServerClickNew" validationgroup="btnValidate" type="button">Save</button>
											<button style="display: none" class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClickNew" validationgroup="btnValidate" type="button">Save</button>
											<a class="AD_btn" id="btnBack" runat="server">Back</a>
										</div>
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.container-fluid -->
						</div>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
					</Triggers>
				</asp:UpdatePanel>

				<section class="app-content">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Employee</h4>
										<asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged"></asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Working Shift<span style="color: red !important;">*</span>
											<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlWorkingShift" InitialValue="0" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
										<asp:DropDownList ID="ddlWorkingShift" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlWorkingShift_SelectedIndexChanged"></asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group" style="display: none;">
										<h4>Effective Date<span style="color: red !important;">*</span>
										</h4>
										<input type="text" class="form-control datetime-picker" data-date-format="DD-MMM-YYYY" id="txtDate" runat="server" />
									</div>
								</div>
								<div class="col-md-12" style="display: none">
									<div class="form-group">
										<h4>Notes</h4>
									</div>
									<textarea class="form-control" id="txtNotes" placeholder="Notes" type="text" runat="server" />
								</div>

							</div>
						</div>

						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6 form-group">
									<h4>Working Hours </h4>
									<input class="form-control" id="txtHours" value="" readonly="true" type="number" runat="server" />
										
													
								</div>
									<br />
								<div class="col-sm-6 form-group">
												<div class="checkbox checkbox-primary">
															<input type="checkbox" id="allowHolidayWorking" runat="server" name="returnedLockerKeys" />
															<label for="allowHolidayWorking">Allow Holiday Working</label>	
											</div>		
								
							</div>	</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-md-12">
									<asp:UpdatePanel ID="UpdatePanel4" runat="server">
										<ContentTemplate>
											<div class="form-group" id="divAlertMsg" runat="server">
												<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
													<span>
														<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
													</span>
													<p id="pAlertMsg" runat="server">
													</p>
												</div>
											</div>
										</ContentTemplate>
									</asp:UpdatePanel>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane active row">

						<div class="col-sm-12" id="shiftDiv" runat="server">
							<div class="row">
								<div class="col-md-4 form-group">
									<div class="row">
									</div>
								</div>
							</div>
							<div class="clearfix">&nbsp;</div>
							<div class="row" id="shiftDaysDiv" runat="server">
								<div class="col-sm-12">
									<asp:Button ID="btnCopytoAll" CssClass="AD_btn_inn" runat="server" OnClick="btnCopytoAll_Click" Text="Copy To All" />
									<div class="clearfix">&nbsp;</div>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Day</th>
												<th>Off Day (OT)</th>
												<th>Flexible Shift</th>
												<th>Time In Exemption</th>
												<th>Start Time</th>
												<th>Late In Time</th>
												<th>Break Start Time</th>
												<th>Break End Time</th>
												<th style="display: none;">Half Day Start</th>
												<th>Early Out</th>
												<th>End Time</th>
												<th>Day<span style="color: red">+1</span> Change</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" checked="checked" id="chkMonday" runat="server"  onchange="CheckWeekDayOtSpecial('chkMondayIsSpecial','chkMondayOT')" />
															<label for="chkMonday">Monday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkMondayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkMondayIsSpecial','chkMonday')" />
															<label for="chkMondayOT"></label>
														</div>
													</div>

												</td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkMondayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkMondayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtMonTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtMONStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtMONLateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtMONBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtMONBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtMONHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtMONEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtMONEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkMonNightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkMonNightShift"></label>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" checked="checked" id="chkTuesday" runat="server" onchange="CheckWeekDayOtSpecial('chkTuesdayIsSpecial','chkTuesdayOT')" />
															<label for="chkTuesday">Tuesday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkTuesdayOT" runat="server" name="OT"  onchange="CheckWeekDayOtSpecial('chkTuesdayIsSpecial','chkTuesday')" />
															<label for="chkTuesdayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkTuesdayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkTuesdayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtTueTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtTUEStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtTUELateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtTUEBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtTUEBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtTUEHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtTUEEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtTUEEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkTUENightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkTUENightShift"></label>
														</div>
													</div>

												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" checked="checked" id="chkWednesdat" runat="server" onchange="CheckWeekDayOtSpecial('chkWednesdayIsSpecial','chkWednesdayOT')" />
															<label for="chkWednesdat">Wednesday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkWednesdayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkWednesdayIsSpecial','chkWednesdat')"  />
															<label for="chkWednesdayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkWednesdayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkWednesdayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtWedTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtWEDStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtWEDLateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtWEDBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtWEDBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtWEDHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtWEDEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtWEDEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkWEDNightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkWEDNightShift"></label>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" checked="checked" id="chkThursday" runat="server" onchange="CheckWeekDayOtSpecial('chkThursdayOT','chkThursdayIsSpecial')" />
															<label for="chkThursday">Thursday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkThursdayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkThursdayIsSpecial','chkThursday')"  />
															<label for="chkThursdayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkThursdayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkThursdayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtThursTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtTHUStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtTHULateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtTHUBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtTHUBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtTHUHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtTHUEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtTHUEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkTHUNightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkTHUNightShift"></label>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" checked="checked" id="chkFriday" runat="server" name="returnedLockerKeys" onchange="CheckWeekDayOtSpecial('chkFridayOT','chkFridayIsSpecial')"/>
															<label for="chkFriday">Friday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkFridayOT" runat="server" name="OT"  onchange="CheckWeekDayOtSpecial('chkFridayIsSpecial','chkFriday')" />
															<label for="chkFridayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkFridayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkFridayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtFriTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtFRIStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtFRILateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtFRIBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="2:30 PM" id="txtFRIBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="3:00 PM" id="txtFRIHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="6:29 PM" id="txtFRIEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="6:30 PM" id="txtFRIEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkFRINightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkFRINightShift"></label>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSaturday" runat="server" name="returnedLockerKeys" onchange="CheckWeekDayOtSpecial('chkSaturdayOT','chkSaturdayIsSpecial')"/>
															<label for="chkSaturday">Saturday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSaturdayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkSaturdayIsSpecial','chkSaturday')" />
															<label for="chkSaturdayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSaturdayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkSaturdayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtSatTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtSATStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtSATLateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtSATBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtSATBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtSATHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtSATEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtSATEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSATNightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkSATNightShift"></label>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSunday" runat="server" onchange="CheckWeekDayOtSpecial('chkSundayOT','chkSundayIsSpecial')" />
															<label for="chkSunday">Sunday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSundayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkSundayIsSpecial','chkSunday')" />
															<label for="chkSundayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSundayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkSundayIsSpecial"></label>
														</div>
													</div>
												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtSunTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtSUNStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtSUNLateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtSUNBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtSUNBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtSUNHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtSUNEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtSUNEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSUNNightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkSUNNightShift"></label>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>

								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- #dash-content -->
			</div>
			<div class="clearfix">&nbsp;</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>
		<style>
			input#btnCopytoAll {
				padding: 3px 24px;
			}

			@media only screen and (max-width: 1599px) and (min-width: 1201px) {
				.select2-container {
					font-size: 11px !important;
				}
			}

			a#btnBack {
				font-weight: 700 !important
			}

			.tf-back-btn {
				background-color: #575757;
				padding: 10px 10px 10px 10px;
				border-radius: 100px;
				border: none !important;
				color: #fff;
			}

				.tf-back-btn i {
					color: #fff !important;
				}

			input[type="radio"], input[type="checkbox"] {
				display: none !important;
			}

			.total {
				font-weight: bold;
				font-size: 20px;
				color: #188ae2;
			}
		</style>
		<!-- Modal -->
<script type="text/javascript">  
	function CheckWeekDayOtSpecial(control1, control2) {
		$('#' + control1).prop("checked", false);
		$('#' + control2).prop("checked", false);
	};

			$(document).ready(function () {
				
				$(".exemption-timein").keypress(function (e) {
					if (e.which < 49 || e.which > 54 || $(this).val() > 0) {
						$("#errmsg").html("Digits Only").show().fadeOut("slow");
						return false;
					}
				});

				$('.exemption-timein').bind('copy paste cut', function (e) {
					e.preventDefault(); //disable cut,copy,paste

				});

			

			});
</script>
	</form>
</body>
</html>
