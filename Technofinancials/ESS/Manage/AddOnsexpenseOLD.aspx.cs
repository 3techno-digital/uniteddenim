﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.Manage
{
    public partial class AddOnsexpenseOLD : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int AddOnsExpenseID
        {
            get
            {
                if (ViewState["AddOnsExpenseID"] != null)
                {
                    return (int)ViewState["AddOnsExpenseID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AddOnsExpenseID"] = value;
            }
        }

        protected int EmployeeID
        {
            get
            {
                if (ViewState["EmployeeID"] != null)
                {
                    return (int)ViewState["EmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["EmployeeID"] = value;
            }
        }

        protected string Attachment
        {
            get
            {
                if (ViewState["Attachment"] != null)
                {
                    return (string)ViewState["Attachment"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["Attachment"] = value;
            }
        }

        protected int DirectReportEmployeeID
        {
            get
            {
                if (ViewState["DirectReportEmployeeID"] != null)
                {
                    return (int)ViewState["DirectReportEmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DirectReportEmployeeID"] = value;
            }
        }
        
        protected string DocumentStatus
        {
            get
            {
                if (ViewState["DocumentStatus"] != null)
                {
                    return (string)ViewState["DocumentStatus"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocumentStatus"] = value;
            }
        }

        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return "Saved as Draft";
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    CheckSessions();
                    DocStatus = "Saved as Draft";
                    ViewState["AddOnsExpenseID"] = null;
                    BindCurrencyDropdown();
                    BindEmployeeDropdown();

                    btnReview.Visible =
                    lnkReject.Visible =
                    lnkDelete.Visible =
                    btnSubForReview.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/AddOnsExpense";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["AddOnsExpenseID"] != null)
                    {
                        AddOnsExpenseID = Convert.ToInt32(HttpContext.Current.Items["AddOnsExpenseID"].ToString());
                        btnSave.Visible = false;
                        txtAmount.Enabled = ddlAddOn.Enabled = ddlCurrency.Enabled = ddlEmployee.Enabled = false;
                        txtNotes.Disabled = true;
                        
                        getAddOnsExpenseByID(AddOnsExpenseID);

                        if (EmployeeID.ToString() == Session["EmployeeID"].ToString())
                        {
                            ddlEmployee.Visible = false;
                            btnSave.Visible = false;
                            txtEmployee.Visible = true;
                            txtEmployee.Value = Session["UserName"].ToString();
                        }
                        else
                        {
                            //CheckAccess();
                            checkDocStatus();
                        }
                    }
                    else
                    {
                        ddlAddOn.Items.Clear();
                        ddlAddOn.DataSource = null;
                        ddlAddOn.DataBind();
                        ddlAddOn.Items.Insert(0, new ListItem("--- Select Employe First ---", "0"));
                        DocumentStatus = "Saved as Draft";
                        txtAmount.Enabled = ddlAddOn.Enabled = ddlCurrency.Enabled = ddlEmployee.Enabled = true;
                        txtNotes.Disabled = false;

                    }
                }                
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void checkDocStatus()
        {
            if (Session["EmployeeID"].ToString() == ddlEmployee.SelectedValue)
            {
                    btnReview.Visible = lnkDelete.Visible = lnkReject.Visible = btnSubForReview.Visible = false;
                    btnSave.Visible = true;
            }
            else if (Session["EmployeeID"].ToString() == DirectReportEmployeeID.ToString())
            {
                if (DocumentStatus == "Saved as Draft" && DocStatus != "Approved")
                {
                    btnReview.Visible = lnkReject.Visible = btnSubForReview.Visible =
                        btnSave.Visible = lnkDelete.Visible = true;
                }
                else
                {
                    btnReview.Visible = lnkReject.Visible = btnSubForReview.Visible =
                        btnSave.Visible = lnkDelete.Visible = false;
                }
            }
        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnReview.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;

                btnReview.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
               

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "AddOns";
                objDB.PrimaryColumnnName = "AddOnsID";
                objDB.PrimaryColumnValue = AddOnsExpenseID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getAddOnsExpenseByID(int AddOnsExpenseID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.AddOnsExpenseID = AddOnsExpenseID;
                dt = objDB.GetAddOnsExpenseByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        DocStatus = dt.Rows[0]["DocStatus"].ToString();
                        DocumentStatus = dt.Rows[0]["Status"].ToString();
                        txtAmount.Text = dt.Rows[0]["Amount"].ToString();
                        txtNotes.Value = dt.Rows[0]["Note"].ToString();
                        txtCurrencyAmount.Text = Convert.ToDouble(dt.Rows[0]["NetAmount"]).ToString("N2");
                        txtRate.Text = dt.Rows[0]["CurrencyRate"].ToString();
                        ddlCurrency.SelectedValue = dt.Rows[0]["CurrencyID"].ToString();
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"].ToString());
                        gvAttachment.DataSource = dt;
                        gvAttachment.DataBind();
                        objDB.EmployeeID = EmployeeID;
                        DataTable dtEmp = objDB.GetEmployeeByID(ref errorMsg);

                        if (dtEmp != null)
                        {
                            if (dtEmp.Rows.Count > 0 && dtEmp.Rows[0]["DirectReportingPerson"].ToString() != "")
                            {
                                DirectReportEmployeeID = Convert.ToInt32(dtEmp.Rows[0]["DirectReportingPerson"].ToString());
                            }
                        }

                        BindAddOnsHeadDropdown();
                        ddlAddOn.SelectedValue = dt.Rows[0]["AddOnsHeadID"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";
                //uploadFile();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Amount = txtAmount.Text;
                objDB.Notes = txtNotes.Value;
                objDB.CurrencyRate = Convert.ToDouble(txtRate.Text);
                objDB.AmountAfterCurrency = Convert.ToDouble(txtCurrencyAmount.Text);
                objDB.CurrencyID = Convert.ToInt32(ddlCurrency.SelectedValue);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.AddOnsID = Convert.ToInt32(ddlAddOn.SelectedValue);
                objDB.Status = Session["EmployeeID"].ToString() == DirectReportEmployeeID.ToString() ? "Submited By Manager" : "Saved as Draft";
                objDB.PreparedBy = Session["UserName"].ToString();
                objDB.Attachment = Attachment;

                if (HttpContext.Current.Items["AddOnsExpenseID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.AddOnsExpenseID = AddOnsExpenseID;
                    res = objDB.UpdateAddOnsExpense();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddAddOnsExpense();
                    clearFields();
                }

                if (res == "New AddOns Added Successfully" || res == "AddOns  Data Updated")
                {
                    if (res == "New AddOns Added Successfully") { Common.addlog("Add", "ESS", "New AddOns \"" + objDB.CurrencyName + "\" Added", "AddOns"); }
                    if (res == "AddOns  Data Updated") { Common.addlog("Update", "ESS", "AddOns \"" + objDB.AddOnsID + "\" Updated", "AddOns", objDB.AddOnsExpenseID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void clearFields()
        {
            txtAmount.Text = "0";
            txtRate.Text = "1";
            txtCurrencyAmount.Text = "0";
            ddlCurrency.SelectedValue = "0";
            ddlEmployee.SelectedValue = "0";
            ddlAddOn.SelectedValue = "0";
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;

                if (btn.ID.ToString() == "btnSubForReview")
                {
                    if (Session["EmployeeID"] != null)
                    {
                        if (ddlEmployee.SelectedValue == Session["EmployeeID"].ToString())
                        {
                            objDB.Status = "Submited By Employee";
                            objDB.AddOnsExpenseID = AddOnsExpenseID;
                            res = objDB.UpdateAddOnstatus();

                        }
                        else if (DirectReportEmployeeID == Convert.ToInt32(Session["EmployeeID"].ToString()))
                        {
                            objDB.Status = "Submited By Manager";
                            objDB.AddOnsExpenseID = AddOnsExpenseID;
                            res = objDB.UpdateAddOnstatus();
                        }
                    }
                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            //CheckAccess();
            //checkDocStatus();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "AddOns", "AddOnsExpenseID", HttpContext.Current.Items["AddOnsExpenseID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "AddOns of ID\"" + HttpContext.Current.Items["AddOnsExpenseID"].ToString() + "\" Status Changed", "AddOns", AddOnsExpenseID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.AddOnsExpenseID = AddOnsExpenseID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteCurrency();
                Common.addlog("AddOns Rejected", "HR", "AddOns of ID\"" + HttpContext.Current.Items["AddOnsExpenseID"].ToString() + "\" AddOns Rejected", "AddOns", AddOnsExpenseID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Currency Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        private void BindCurrencyDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlCurrency.DataSource = objDB.GetAllCurrencyByCompanyID(ref errorMsg);
                ddlCurrency.DataTextField = "CurrencyName";
                ddlCurrency.DataValueField = "CurrencyID";
                ddlCurrency.DataBind();
                ddlCurrency.Items.Insert(0, new ListItem("--- Select Currency ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        private void BindAddOnsHeadDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = ddlEmployee.SelectedIndex;
                ddlAddOn.Items.Clear();

                if (DocStatus != "Saved as Draft")
                {
                    ddlAddOn.DataSource = objDB.GetAllApprovedAddOnsHeadByCompanyID(ref errorMsg);
                    ddlAddOn.Enabled = false;
                }
                else
                {
                    ddlAddOn.Enabled = true;
                    ddlAddOn.DataSource = objDB.GetAllApprovedAddOnsHeadByCompanyIDandEmployeeIDForESS(ref errorMsg);
                }
                ddlAddOn.DataTextField = "Tittle";
                ddlAddOn.DataValueField = "AddOnsID";
                ddlAddOn.DataBind();
                ddlAddOn.Items.Insert(0, new ListItem("--- Select Add Ons Head ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            string errorMsg = "";
            if (ddlAddOn.SelectedValue != "0")
            {
                DataTable dt = new DataTable();
                objDB.CurrencyID = Convert.ToInt32(ddlAddOn.SelectedValue);
                dt = objDB.getCurrencyByID(ref errorMsg);
                if (dt != null && dt.Rows.Count > 0)
                {
                    txtRate.Text = dt.Rows[0]["Rate"].ToString();
                    txtCurrencyAmount.Text = (Convert.ToDouble(txtAmount.Text) * Convert.ToDouble(txtRate.Text)).ToString("N2");
                }
            }
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            txtCurrencyAmount.Text = (Convert.ToDouble(txtAmount.Text) * Convert.ToDouble(txtRate.Text)).ToString("N2");
        }

        protected void ddlAddOn_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.AddOnsID = Convert.ToInt32(ddlAddOn.SelectedValue);
                dt = objDB.GetAddOnsByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ddlCurrency.SelectedValue = dt.Rows[0]["CurrencyID"].ToString();
                        ddlCurrency_SelectedIndexChanged(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlEmployee.SelectedValue == "0")
                {
                    CheckSessions();
                    ddlAddOn.Items.Clear();
                    ddlAddOn.DataSource = null;
                    ddlAddOn.DataBind();
                    ddlAddOn.Items.Insert(0, new ListItem("--- Select Employee First ---", "0"));
                }
                else
                {
                    BindAddOnsHeadDropdown();

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void BtnUpload_Click(object sender, EventArgs e)
        {
            uploadFile();
        }

        protected void BtnFileRemove_Click(object sender, EventArgs e)
        {
            RemoveFile();
        }
        protected void uploadFile()
        {
            Page.ClientScript.GetPostBackEventReference(this, string.Empty);
            string ctrlName = Request.Params.Get("__EVENTTARGET");
            string ctrlArgs = Request.Params.Get("__EVENTARGUMENT");

            if (FileUpload1.HasFile)
            {
                try
                {
                    if (FileUpload1.PostedFile.ContentLength > 2097152)
                    {
                        LblMessage.Text = "Maximum File Size(2MB) Exceeded";
                        LblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        //Session["FilePath"] = Server.MapPath("~/assets/Attachments/AddOnExpenses/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + FileUpload1.FileName);
                        String destdir = Server.MapPath("~/assets/Attachments/AddOnExpenses/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");

                        if (!Directory.Exists(destdir))
                        {
                            Directory.CreateDirectory(destdir);
                        }

                        FileUpload1.SaveAs(destdir + FileUpload1.FileName);
                        LblMessage.Text = $"{FileUpload1.FileName}";
                        LblMessage.ForeColor = System.Drawing.Color.Green;
                        FileUpload1.Visible = false;
                        Attachment = "/assets/Attachments/AddOnExpenses/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + FileUpload1.FileName;
                        //       Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/" + ctrlName + "/SeperationDeductions/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
                    }
                }
                catch (Exception ex)
                {
                    LblMessage.Text = ex.Message;
                    LblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                LblMessage.Text = "Please Select a File";
                LblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void RemoveFile()
        {
            if (Attachment.Equals(""))
            {
                LblMessage.Text = "No File Exists";
                LblMessage.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                LblMessage.Text = FileUpload1.FileName + "File Removed";
                LblMessage.ForeColor = System.Drawing.Color.Green;
                Attachment = "";
            }
            FileUpload1.Visible = true;
        }

        //protected void uploadFile()
        //{
        //    if (fileUpload != null)
        //    {
        //        Random rand = new Random((int)DateTime.Now.Ticks);
        //        int randnum = 0;

        //        string fn = "";
        //        string exten = "";

        //        string destDir = Server.MapPath("~/assets/Attachments/EmployeeAddOns/");
        //        randnum = rand.Next(1, 100000);
        //        fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

        //        if (!Directory.Exists(destDir))
        //        {
        //            Directory.CreateDirectory(destDir);
        //        }

        //        string fname = Path.GetFileName(fileUpload.FileName);
        //        exten = Path.GetExtension(fileUpload.FileName);
        //        fileUpload.SaveAs(destDir + fn + exten);

        //        Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/Attachments/EmployeeAddOns/" + fn + exten;
        //    }

        //    if (fileUpload != null)
        //    {
        //        //foreach (HttpPostedFile file in updAttachments.PostedFile)
        //        //{

        //        //HttpPostedFile file = fileUpload.file;
        //        Random rand = new Random((int)DateTime.Now.Ticks);
        //        int randnum = 0;

        //        string fn = "";
        //        string exten = "";
        //        string destDir = Server.MapPath("~/assets/Attachments/EmployeeAddOns/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
        //        randnum = rand.Next(1, 100000);
        //        fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

        //        if (!Directory.Exists(destDir))
        //        {
        //            Directory.CreateDirectory(destDir);
        //        }
        //        string fname = Path.GetFileName(fileUpload.FileName);
        //        exten = Path.GetExtension(fileUpload.FileName);
        //        fileUpload.SaveAs(destDir + fn + exten);

        //        Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/Attachments/EmployeeAddOns/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
        //    }
        //}
    }
}