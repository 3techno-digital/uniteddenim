﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.Manage
{
    public partial class Leads : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    CheckSessions();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/leads";
                    divAlertMsg.Visible = false;
                    ViewState["FilesSrNo"] = null;
                    ViewState["dtFiles"] = null;
                    FilesSrNo = 1;
                    dtFiles = null;
                    dtFiles = new DataTable();
                    dtFiles = createFiles();
                    BindFilesTable();
                    ViewState["ReviewsSrNo"] = null;
                    ViewState["dtReviews"] = null;
                    ViewState["showReviewsFirstRow"] = null;
                    ReviewsSrNo = 1;
                    dtReviews = null;
                    dtReviews = new DataTable();
                    dtReviews = createReviews();
                    showReviewsFirstRow = false;
                    BindReviewsTable();
                    ViewState["TasksSrNo"] = null;
                    ViewState["dtTasks"] = null;
                    ViewState["showTasksFirstRow"] = null;
                    TasksSrNo = 1;
                    dtTasks = null;
                    dtTasks = new DataTable();
                    dtTasks = createTasks();
                    showTasksFirstRow = false;
                    BindTasksTable();
                    txtTitle.Disabled = true;
                    txtClient.Disabled = true;
                    txtBD.Disabled = true;
                    txtGD.Disabled = true;
                    txtQS.Disabled = true;
                    txtPOC.Disabled = true;
                    txtTV.Disabled = true;
                    txtDescription.Disabled = true;

                    if (HttpContext.Current.Items["LeadID"] != null)
                    {
                        GetLeadByID(Convert.ToInt32(HttpContext.Current.Items["LeadID"].ToString()));
                    }
                }

              
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }



  

    

        
        private void GetLeadByID(int LeadID)
        {
            DataTable dt = new DataTable();
            objDB.LeadID = LeadID;
            dt = objDB.GetLeadByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                  
                    txtTitle.Value = dt.Rows[0]["Title"].ToString();
                    txtClient.Value = dt.Rows[0]["CompanyName"].ToString();
                    txtBD.Value = dt.Rows[0]["BDName"].ToString();
                    txtGD.Value = DateTime.Parse(dt.Rows[0]["GenerationDate"].ToString()).ToString("dd-MMM-yyyy");
                    txtQS.Value = dt.Rows[0]["QualificationStatus"].ToString();
                    txtPOC.Value = dt.Rows[0]["POC"].ToString();
                    txtTV.Value = dt.Rows[0]["TotalValue"].ToString();
                    txtDescription.Value = dt.Rows[0]["Description"].ToString();

                    objDB.DocID = LeadID;
                    objDB.DocType = "Leads";
                    ltrNotesTable.Text = objDB.GetDocNotes();

                    

                    getReviewsByLeadID(LeadID);
                    GetAllLeadAttachmentsByLeadID(LeadID);
                    getTasksByLeadID(LeadID);

                }
            }
            Common.addlog("ViewAll", "ESS", "Leads Viewed", "Leads");

        }


        private void getTasksByLeadID(int LeadID)
        {
            DataTable dt = new DataTable();
            objDB.LeadID = LeadID;
            objDB.EmployeeID= Convert.ToInt32(Session["EmployeeID"].ToString());
            dt = objDB.GetAllLeadTaskByLeadIDAndEmployeeID(ref errorMsg);
            gvTasks.DataSource = dt;
            gvTasks.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvTasks.UseAccessibleHeader = true;
                    gvTasks.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            
        }



        private void getReviewsByLeadID(int LeadID)
        {
            dtReviews = null;
            dtReviews = new DataTable();
            dtReviews = createReviews();

            DataTable dt = new DataTable();
            objDB.LeadID = LeadID;
            dt = objDB.GetAllLeadReviewsByLeadID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtReviews.Rows[0][0].ToString() == "")
                    {
                        dtReviews.Rows[0].Delete();
                        dtReviews.AcceptChanges();
                        showReviewsFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtReviews.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["EmployeeID"],
                            dt.Rows[i]["PreparedBy"],
                            dt.Rows[i]["Review"]
                        });
                    }
                    ReviewsSrNo = Convert.ToInt32(dtReviews.Rows[dtReviews.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindReviewsTable();
        }


        private void GetAllLeadAttachmentsByLeadID(int LeadID)
        {
            DataTable dt = new DataTable();
            objDB.LeadID = LeadID;
            dt = objDB.GetAllLeadAttachmentsByLeadID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtFiles.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Attachment"]
                        });
                    }
                }
            }

            BindFilesTable();
        }




        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }


        protected void btnSave_ServerClick(object sender, EventArgs e)

        {

            try
            {

                CheckSessions();
                string res = "";
                int LeadID = 0;

                if (HttpContext.Current.Items["LeadID"] != null)
                {
                    LeadID = Convert.ToInt32(HttpContext.Current.Items["LeadID"].ToString());

                    objDB.DocType = "Leads";
                    objDB.DocID = LeadID;
                    objDB.Notes = txtNotes.Value;
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.AddDocNotes();


                    objDB.LeadID = LeadID;
                    objDB.DeletedBy = Session["UserName"].ToString();
                    objDB.DelLeadReview();

                    dtReviews = (DataTable)ViewState["dtReviews"] as DataTable;
                    if (dtReviews != null)
                    {
                        if (dtReviews.Rows.Count > 0)
                        {
                            if (!showReviewsFirstRow)
                            {
                                dtReviews.Rows[0].Delete();
                                dtReviews.AcceptChanges();
                            }

                            for (int i = 0; i < dtReviews.Rows.Count; i++)
                            {
                                objDB.LeadID = LeadID;
                                objDB.EmployeeID = Convert.ToInt32(dtReviews.Rows[i]["EmployeeID"].ToString());
                                objDB.Review = dtReviews.Rows[i]["Review"].ToString();
                                objDB.CreatedBy = dtReviews.Rows[i]["EmployeeName"].ToString();
                                res = objDB.AddLeadReview();
                            }
                        }
                    }

                    if (dtReviews.Rows.Count == 0)
                    {
                        ViewState["ReviewsSrNo"] = null;
                        ViewState["dtReviews"] = null;
                        ViewState["showReviewsFirstRow"] = null;
                    }


                }


                BindFilesTable();
                BindReviewsTable();
                BindTasksTable();



                if (res == "Lead Review Added")
                {
                    Common.addlog("Add", "ESS", " Lead review \"" + objDB.Review + "\" Added", "LeadsReview"); 

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }




        private DataTable dtReviews
        {
            get
            {
                if (ViewState["dtReviews"] != null)
                {
                    return (DataTable)ViewState["dtReviews"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtReviews"] = value;
            }
        }
        bool showReviewsFirstRow
        {
            get
            {
                if (ViewState["showReviewsFirstRow"] != null)
                {
                    return (bool)ViewState["showReviewsFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showReviewsFirstRow"] = value;
            }
        }
        protected int ReviewsSrNo
        {
            get
            {
                if (ViewState["ReviewsSrNo"] != null)
                {
                    return (int)ViewState["ReviewsSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["ReviewsSrNo"] = value;
            }
        }
        private DataTable createReviews()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("EmployeeID");
            dt.Columns.Add("EmployeeName");
            dt.Columns.Add("Review");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindReviewsTable()
        {
            if (ViewState["dtReviews"] == null)
            {
                dtReviews = createReviews();
                ViewState["dtReviews"] = dtReviews;
            }

            gvReviews.DataSource = dtReviews;
            gvReviews.DataBind();

            if (showReviewsFirstRow)
                gvReviews.Rows[0].Visible = true;
            else
                gvReviews.Rows[0].Visible = false;

            gvReviews.UseAccessibleHeader = true;
            gvReviews.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gvReviews_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = ReviewsSrNo.ToString();

            }
        }
        protected void lnkRemoveReview_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtReviews.Rows.Count; i++)
            {
                if (dtReviews.Rows[i][0].ToString() == delSr)
                {
                    dtReviews.Rows[i].Delete();
                    dtReviews.AcceptChanges();
                }
            }
            for (int i = 0; i < dtReviews.Rows.Count; i++)
            {
                dtReviews.Rows[i].SetField(0, i + 1);
                dtReviews.AcceptChanges();
            }
            if (dtReviews.Rows.Count < 1)
            {
                DataRow dr = dtReviews.NewRow();
                dtReviews.Rows.Add(dr);
                dtReviews.AcceptChanges();
                showReviewsFirstRow = false;
            }
            if (showReviewsFirstRow)
                ReviewsSrNo = dtReviews.Rows.Count + 1;
            else
                ReviewsSrNo = 1;

            BindReviewsTable();
        }
        protected void btnAddReview_Click(object sender, EventArgs e)
        {
            DataRow dr = dtReviews.NewRow();
            dr[0] = ReviewsSrNo.ToString();
            dr[1] = Session["EmployeeID"].ToString();
            dr[2] = Session["UserName"].ToString();
            dr[3] = ((System.Web.UI.HtmlControls.HtmlTextArea)gvReviews.FooterRow.FindControl("txtReview")).Value;
            dtReviews.Rows.Add(dr);
            dtReviews.AcceptChanges();


            if (dtReviews.Rows[0][0].ToString() == "")
            {
                dtReviews.Rows[0].Delete();
                dtReviews.AcceptChanges();
                showReviewsFirstRow = true;
            }

            ReviewsSrNo += 1;
            BindReviewsTable();
            ((Label)gvReviews.FooterRow.FindControl("txtSrNo")).Text = ReviewsSrNo.ToString();
        }


        private DataTable dtTasks
        {
            get
            {
                if (ViewState["dtTasks"] != null)
                {
                    return (DataTable)ViewState["dtTasks"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtTasks"] = value;
            }
        }
        bool showTasksFirstRow
        {
            get
            {
                if (ViewState["showTasksFirstRow"] != null)
                {
                    return (bool)ViewState["showTasksFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showTasksFirstRow"] = value;
            }
        }
        protected int TasksSrNo
        {
            get
            {
                if (ViewState["TasksSrNo"] != null)
                {
                    return (int)ViewState["TasksSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["TasksSrNo"] = value;
            }
        }
        private DataTable createTasks()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("EmployeeID");
            dt.Columns.Add("EmployeeName");
            dt.Columns.Add("Title");
            dt.Columns.Add("Description");
            dt.Columns.Add("IsCompleted");
            dt.Columns.Add("DeadLine");
            dt.Columns.Add("TaskID");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindTasksTable()
        {
            if (ViewState["dtTasks"] == null)
            {
                dtTasks = createTasks();
                ViewState["dtTasks"] = dtTasks;
            }

            gvTasks.DataSource = dtTasks;
            gvTasks.DataBind();

            if (showTasksFirstRow)
                gvTasks.Rows[0].Visible = true;
            else
                gvTasks.Rows[0].Visible = false;

            gvTasks.UseAccessibleHeader = true;
            gvTasks.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gvTasks_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = TasksSrNo.ToString();

                DropDownList ddList = (DropDownList)e.Row.FindControl("ddlEmployees");
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddList.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddList.DataTextField = "EmployeeName";
                ddList.DataValueField = "EmployeeID";

                ddList.DataBind();
                ddList.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
        }
        protected void lnkCompleteTask_Command(object sender, CommandEventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender as LinkButton;
                objDB.TaskID = Convert.ToInt32(lnk.CommandArgument.ToString());
                objDB.ModifiedBy = Session["UserName"].ToString();
                string res = objDB.CompleteLeadTask();
                if (HttpContext.Current.Items["LeadID"] != null)
                {
                    getTasksByLeadID(Convert.ToInt32(HttpContext.Current.Items["LeadID"].ToString()));
                }
                if (res == "Task Completed")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res + " Successfully !!";
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
      

        private DataTable dtFiles
        {
            get
            {
                if (ViewState["dtFiles"] != null)
                {
                    return (DataTable)ViewState["dtFiles"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtFiles"] = value;
            }
        }
        protected int FilesSrNo
        {
            get
            {
                if (ViewState["FilesSrNo"] != null)
                {
                    return (int)ViewState["FilesSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["FilesSrNo"] = value;
            }
        }
        private DataTable createFiles()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("FilePath");
            dt.AcceptChanges();

            return dt;
        }
        protected void BindFilesTable()
        {
            if (ViewState["dtFiles"] == null)
            {
                dtFiles = createFiles();
                ViewState["dtFiles"] = dtFiles;
            }

            gvFiles.DataSource = dtFiles;
            gvFiles.DataBind();

            if (gvFiles.Rows.Count > 0)
            {
                gvFiles.UseAccessibleHeader = true;
                gvFiles.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void lnkRemoveFile_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtFiles.Rows.Count; i++)
            {
                if (dtFiles.Rows[i][0].ToString() == delSr)
                {
                    dtFiles.Rows[i].Delete();
                    dtFiles.AcceptChanges();
                }
            }
            for (int i = 0; i < dtFiles.Rows.Count; i++)
            {
                dtFiles.Rows[i].SetField(0, i + 1);
                dtFiles.AcceptChanges();
            }

            FilesSrNo = dtFiles.Rows.Count + 1;
            BindFilesTable();
        }



        protected void ddlClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtClient = objDB.GetClientByID(ref errorMsg);

                if (dtClient == null)
                {
                    if (dtClient.Rows.Count > 0)
                    {
                        ViewState["ClientEmail1"] = dtClient.Rows[0]["CompanyEmail"].ToString();
                        ViewState["ClientEmail2"] = dtClient.Rows[0]["Email"].ToString();
                    }
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}