﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.Manage
{
    public partial class TravelExpense : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int OtherExpenseID
        {
            get
            {
                if (ViewState["OtherExpenseID"] != null)
                {
                    return (int)ViewState["OtherExpenseID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["OtherExpenseID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {
                    CheckSessions();
                    ViewState["OtherExpenseID"] = null;


                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["OtherExpenseID"] != null)
                    {
                        OtherExpenseID = Convert.ToInt32(HttpContext.Current.Items["OtherExpenseID"].ToString());                       
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }                   
    }
}