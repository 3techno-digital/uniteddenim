﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanAndAdvanceApplication.aspx.cs" Inherits="Technofinancials.ESS.Manage.LoanAndAdvanceApplication" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
         <asp:HiddenField ID="hdnPFAmount" runat="server" ClientIDMode="Static" />
         <asp:HiddenField ID="hdnCompPF" runat="server" ClientIDMode="Static" />
         <asp:HiddenField ID="hdnZakatpercentage" runat="server" ClientIDMode="Static" />
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <asp:UpdatePanel ID="upd1" runat="server">

                    <ContentTemplate>



                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <h1 class="m-0 text-dark">Application for Provident Fund Withdrawal</h1>
                                    </div>
                                    <!-- /.col -->
                                    <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                        <ContentTemplate>
                                            <div class="col-sm-4">
                                                <div style="text-align: right;">
                                                    <button class="AD_btn" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                                    <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Review</button>
                                                    <button class="AD_btn" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                                    <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                                    <button class="AD_btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button><%--Disapprove--%>
                                                    <button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                                    <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Send</button><%--Send--%>
                                                    <button class="AD_btn" id="btnSave" runat="server" visible="false" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                                     <button class="AD_btn"  onclick="SubmitRequest()"  type="button">Save</button>
                                                    <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                                </div>
                                            </div>

                                            <!-- Modal -->
                                            <div class="modal fade M_set" id="notes-modal" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <h1 class="m-0 text-dark">Notes</h1>
                                                            <div class="add_new">
                                                                <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                                                <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>
                                                                <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                            </p>
                                                            <p>
                                                                <textarea id="Textarea1" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>

                                                            </p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>


                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                          <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-6 form-group">
                                            <h4>Loan Title</h4>
                                           <asp:DropDownList ID="txtLoanTitle" runat="server" data-plugin="select2" class="form-control select2">

                                               <asp:ListItem Value="Loan against PF">Loan against PF</asp:ListItem>
                                                   <asp:ListItem Value="Advance Salary">Advance Salary</asp:ListItem>
                                           </asp:DropDownList>
                                            
                               
                                        </div>
                                      <div class="col-md-6 form-group">
                                            <h4>Grant Date  <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtGrantDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            <input class="form-control" id="txtGrantDate" data-plugin="datetimepicker" placeholder="Grant Date" type="text" runat="server" data-date-format="DD-MMM-YYYY" />
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <h4>Loan Amount  <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtLoanAmount" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            <input class="form-control" id="txtLoanAmount" placeholder="Loan Amount" type="number" runat="server" />
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <h4>PF Withdrawal Type  <span style="color: red !important;">*</span><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="withdrawltype" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                 <asp:DropDownList ID="withdrawltype" runat="server" onchange="DisplayInstallmentBox()" data-plugin="select2" class="form-control select2"></asp:DropDownList>
                               
                                        </div>
                                        <div style="display:none;" class="col-md-6 form-group">
                                            <h4> Currency </h4>
                                                 <asp:DropDownList ID="Currency" runat="server"  data-plugin="select2" class="form-control select2"></asp:DropDownList>
                               
                                        </div>
                                       
                                         <div id="zakat" style="display:none;" class="col-md-12">
                                            <h4  style="font-weight: 600;"> Zakat </h4>
                                                 <asp:DropDownList ID="Zakatddl" runat="server" onchange="CalculateZakat()" data-plugin="select2" class="form-control select2" style="width:338px;">
                                                     <asp:ListItem Value="1">Deduct Zakat</asp:ListItem>
                                                         <asp:ListItem Value="0">Do not Deduct Zakat</asp:ListItem>

                                                 </asp:DropDownList>
                               
                                        </div>
                                          <div style="display:none;" class="col-md-6 form-group">
                                            <h4>Zakat Amount </h4> <input class="form-control" id="zakatamount" placeholder="0" readonly="true" type="number" runat="server" />
                                        </div>
                                           <div id="showInstallmentbox" style="display:none;"  class="col-md-6 form-group">
                                            <h4>No of Installments</h4>
                                            <input class="form-control" id="txtNoOfInstallments" placeholder="No of Installments" type="number" runat="server" />
                                        </div>
                                        <div  id="showtemppurpose" style="display:none;" class="col-md-12">
                                            <div>
                                                <h4 style="font-weight: 600;">Purpose
                                                </h4>
                                                <asp:DropDownList ID="temppurposeparent" runat="server"  onchange="Getsubpurpose(id)" CssClass="form-control select2" data-plugin="select2" style="width:338px;"  ></asp:DropDownList>
                                            </div>
                                        </div>
                                         
                                        <div  id="showpermpurpose" style="display:none;" class="col-md-12">
                                            <h4 style="font-weight: 600;">Purpose </h4>
                                                 <asp:DropDownList ID="permpurposeparent" runat="server"  onchange="Getsubpurpose(id)" data-plugin="select2" class="form-control select2" style="width:338px;"></asp:DropDownList>
                               
                                        </div>
                                         <div  id="showsubpurpose" runat="server" style="display:none;" class="col-md-12">
                                            <h4 style="font-weight: 600;"> Sub-Purpose </h4>
                                                 <asp:DropDownList ID="subpurpose" runat="server" onchange="GetRequiredDocs(id)" data-plugin="select2" class="form-control select2" style="width:338px;"></asp:DropDownList>
                               
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <h4>Notes</h4>
                                            <textarea class="form-control" id="txtNotes" placeholder="Notes" type="text" runat="server" />
                                        </div>
                                              </ContentTemplate>
                                
                                </asp:UpdatePanel>
                                    </div>
                                </div>
                                 <div  class="col-lg-8 col-md-8 col-sm-12" >
                                     <div class="row">
                                         <div class="col-md-8">
                         <asp:UpdatePanel ID="SalaryPanel" runat="server">
                                    <ContentTemplate>
                                                <div>
                                        <div class="">
                                             <div class="form-group">
                                        <h4>Provident Fund Detail</h4>
                                    </div>
                                           
                                            <asp:GridView ID="gvPFDetails" runat="server" CssClass="table table-bordered" ClientIDMode="Static" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
                                            <Columns>
                                              

                                                <asp:TemplateField HeaderText="EmployeePF">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("EmployeePF") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CompanyPF">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("CompanyPF")  %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="WithdrawAmount">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("WithdrawAmount")  %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Amount">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("RemaningAmount")  %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            


                                            </Columns>
                                        </asp:GridView>
                                           
                                            </div>
                                            </div> 
                            
                                        </ContentTemplate>
                                
                                </asp:UpdatePanel>
                                             </div>
                                         </div>
                                      <div class="row"><br /></div>
                                 
  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                          <div class="row">
                               <div class="">
                                   <div class="row">
                                       
                                       <div class="col-md-8">
                                            <div class="form-group">
                                        <h4>Required Documents</h4>
                                    </div>
                                          <table id="RequiredDocs" class="table table-bordered">
                                               <tr>
                                                  <th>Document</th>
                                                    <th>Attach</th>
                                              </tr>
                                              
                                          </table>
                                        
                                         <%--      
                                            <asp:GridView ID="RequiredDocs" runat="server" CssClass="table table-bordered" ClientIDMode="Static" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
                                            <Columns>
                                              

                                                <asp:TemplateField HeaderText="Document">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("Attachmentname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Document">
                                                    <ItemTemplate>
                                                       
                                           <div class="form-group">
                                               <h4>Attachments</h4>
                                           </div>

                                           <div class="file-upload">
                                               <div class="image-upload-wrap">
                                                   <input id="attach_'<%# Eval("Attachmentoptionid") %>'" class="file-upload-input" runat="server" type="file" name="uploadFile" id="updLogo" onchange="readURL(this);" accept="image/*" />
                                                   <div class="drag-text">
                                                       <h3>Drag and drop a file or select add Image (MaxSize: 2mb)</h3>
                                                   </div>
                                               </div>
                                               <div class="file-upload-content">
                                                   <img class="file-upload-image" src="/assets/images/3techno-Logo.png" alt="" runat="server" id="imgLogo" />
                                                   <div class="image-title-wrap">
                                                       <button id="Attachment" type="button" onclick="removeUpload()" class="remove-image">
                                                           Remove  <span><i class="fa fa-trash" aria-hidden="true"></i></span>

                                                       </button>
                                                   </div>
                                               </div>
                                           </div>
                                  
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            


                                            </Columns>
                                        </asp:GridView>
                                          --%>
                                       </div>
                                     
                                      
                                       
                                   </div>
                               </div>
                             
                               <div class="col-lg-4 col-md-6 col-sm-12">
                                   <div class="row">
                                       <div class="col-md-12">
                                           <div runat="server"  id="btnAddDiv">
                                         <%--      <button type="button" style="margin-top: 2.4rem;" class="AD_btn_inn stock_add" runat="server" onserverclick="btnAddExpDetails_ServerClick" validationgroup="btnAddExpDetailsValidate" id="btnAddExpDetails">Add</button>
                                          --%> </div>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-lg-4 col-md-6 col-sm-12">
                                   <div class="row">
                                       <div runat="server" id="btnUpdateDiv" style="text-align: right;">
                                      <%--     <asp:HiddenField runat="server" ID="hdnExpenseSrNO" Value="0" />
                                           <button type="button" style="margin-top: 18px;" class="AD_btn_inn stock_add" runat="server" onserverclick="btnUpdateExpDetails_ServerClick" validationgroup="btnAddExpDetailsValidate" id="btnUpdateExpDetails">Update</button>
                                           <button type="button" style="margin-top: 18px;" class="AD_btn_inn stock_add" runat="server" onserverclick="btnCancelExpDetails_ServerClick1" id="btnCancelExpDetails">Cancel</button>
                                       --%></div>
                                   </div>
                               </div>
                            </div>
                        </ContentTemplate>

                                         <%-- <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAddExpDetails" />
                                    <asp:PostBackTrigger ControlID="btnUpdateExpDetails" />
                                </Triggers>--%>

                    </asp:UpdatePanel>
                            </div> 
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                            <span>
                                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                            </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
        </style>
        <!-- Modal -->
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>

			var countDocs = 0;
			var DocArray = [];
			function DisplayInstallmentBox() {
				var val = $('#withdrawltype :selected').text();
				if (val == 'Temporary') {
					$('#showInstallmentbox').css('display', 'block');
					$('#showtemppurpose').css('display', 'block');
					$('#showpermpurpose').css('display', 'none');
                    $('#showsubpurpose').css('display', 'none');
					$('#zakat').css('display', 'none');

				}
				else {
					$('#showInstallmentbox').css('display', 'none');
					$('#showpermpurpose').css('display', 'block');
					$('#showtemppurpose').css('display', 'none');
                    $('#showsubpurpose').css('display', 'none');
					$('#zakat').css('display', 'block');
				}

			}


            function CalculateZakat() {
                var EmployerContribution = document.getElementById('hdnCompPF').value;
				var rate = document.getElementById('hdnZakatpercentage').value;
                var isZakatDeduct = $('#Zakatddl').val();
                if (isZakatDeduct == "1") {
					$('#zakatamount').val(Number(EmployerContribution) * (Number(rate)/100));
                }
                else {
					$('#zakatamount').val(0);
				}
			}
			function GetRequiredDocs(id) {
				var selectedsubpurpose = '0';

				selectedsubpurpose = $('#' + id + '').val();

				$.ajax({
					type: "POST",
					url: '/business/services/EssService.asmx/usp_TF_GetWithdrawalDocs',
					data: '{val: ' + JSON.stringify(selectedsubpurpose) + '}',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response) {


						var data = response.d;
						html = '<tr><th> Document</th ><th>Attach</th</tr>';
						if (data.length == 0) {
							html += '<tr><td> no documents required</td></tr>';
						}
						else {
							for (var i = 0; i < data.length; i++) {
								html += '<tr><td> ' + data[i].attachmentname + '</td>';
								html += '<td> <input id=attach_' + data[i].attachmentid + ' class="AD_btn_inn"  type="file" name="uploadFile" /> </td></tr>';
							}
						}


						$('#RequiredDocs').empty();
						$('#RequiredDocs').append(html);
						countDocs = data.length;
						DocArray = data;
						console.log(DocArray);


					},

					error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); alert(errorThrown) }
				});
			}

			function Getsubpurpose(id) {
				var selectedparent = '0';

				selectedparent = $('#' + id + '').val();


				$.ajax({
					type: "POST",
					url: '/business/services/EssService.asmx/Getsubpurposeofwithdrawal',
					data: '{val: ' + JSON.stringify(selectedparent) + '}',
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response) {


						var data = response.d;
						var html = '<option value=0> ----Select----</option>';
						for (var i = 0; i < data.length; i++) {
							html += '<option value=' + data[i].subpurposeid + '>' + data[i].subpurposename + '</option>'
						}

						$('#subpurpose').empty();
						$('#subpurpose').append(html);
						$('#showsubpurpose').css('display', 'block');
						GetRequiredDocs(id);

					},

					error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); alert(errorThrown) }
				});


			}
			function SubmitRequest() {

				if (window.FormData == undefined)
					alert("Error: FormData is undefined");

				else {


					var fileData = new FormData();



					//   Looping over all files and add it to FormData object  
					for (var i = 0; i < DocArray.length; i++) {
						var fileUpload = $("#attach_" + DocArray[i].attachmentid + "").get(0);
						var files = fileUpload.files;
						fileData.append(DocArray[i].attachmentid, files[0]);
                    }

					var Limit = document.getElementById('hdnPFAmount').value;
                    var zakat = $('#Zakatddl').val();
                    var zakatamount = $('#zakatamount').val();
					var rateofzakat = document.getElementById('hdnZakatpercentage').value;
					var withdrawaltype = $('#withdrawltype :selected').text();
					var temppurpose = 0;
					var permpurpose = 0;
					var subpurpose = 0;
					var noofinstallment = 0;
					var amount = $('#txtLoanAmount').val();
					var notes = $('#txtNotes').val();
					var title = $('#txtLoanTitle').val();
					var requireddate = $('#txtGrantDate').val();
					var currency = $('#Currency').val();
					if (withdrawaltype == 'Temporary') {


						temppurpose = $('#temppurposeparent').val();

						subpurpose = $('#subpurpose').val();
						noofinstallment = $('#txtNoOfInstallments').val();
						if (noofinstallment == '' || temppurpose == '0') {
							alert('Kindly fill all fields');
							return;
						}

					}
					if (withdrawaltype == 'Permanent') {



						permpurpose = $('#permpurposeparent').val();
						subpurpose = $('#subpurpose').val();


					}
					

					if (amount == '') {

						alert('Kindly enter loan amount');
						return;
					}
                    if (Number(amount) > Number(Limit)) {

                        if (title == 'Advance Salary')
                        {
                            // go on
                        }
                        else
                        {
							alert('Loan amount cannot be larger than PF Total amount');
							return;
						}
						
					}

					if ($("#subpurpose option").length > 1) {

						if (subpurpose == 0) {
							alert('Subpurpose is required');
							return;
						}
                    }
                    if (zakat == '0') {
                        var iszakatAffidavit = $("#attach_" + DocArray[i - length - 1].attachmentid + "").get(0);
                        var isattached = fileUpload.files;
                        if (isattached.length == 0) {
							alert('Please attach affidavit');
							return;
						}
                     
					}
					fileData.append('withdrawaltype', withdrawaltype);
					fileData.append('temppurpose', temppurpose);
					fileData.append('permpurpose', permpurpose);
					fileData.append('subpurpose', subpurpose);
					fileData.append('noofinstallment', noofinstallment);
					fileData.append('amount', amount);
                    fileData.append('currency', currency);
                    fileData.append('zakat', zakat);
                    fileData.append('zakatamount', zakatamount);
					fileData.append('rateofzakat', rateofzakat);
					fileData.append('notes', notes);
					fileData.append('title', title);
					fileData.append('requireddate', requireddate);
					$.ajax({
						url: '/business/services/EssService.asmx/SubmitWithdrawalRequest',
						type: 'post',
						datatype: 'json',
						contentType: false,
						processData: false,
						async: false,
						data: fileData,
						success: function (response) {
							alert("Request Submitted");
							location.reload();
						},
						error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); alert(errorThrown) }
					});
				}
			}


   //         function SubmitRequest() {
   //             var Limit = document.getElementById('hdnPFAmount').value;

			//	var withdrawaltype = $('#withdrawltype :selected').text();
			//	var temppurpose = 0;
			//	var permpurpose = 0;
   //             var subpurpose = 0;
   //             var noofinstallment = 0;
			//	var amount = $('#txtLoanAmount').val();
   //             var notes = $('#txtNotes').val();
   //             var title = $('#txtLoanTitle').val();
			//	var requireddate = $('#txtGrantDate').val();
			//	var currency = $('#Currency').val();
   //             if (withdrawaltype == 'Temporary') {


   //                 temppurpose = $('#temppurposeparent').val();
   //                 permpurpose = 0;
   //                 subpurpose = $('#subpurpose').val();
   //                 noofinstallment = $('#txtNoOfInstallments').val();
   //                 if (noofinstallment == '' || temppurpose == '0') {
   //                     alert('Kindly fill all fields');
   //                     return;
   //                 }

   //             }

   //             if (amount > Limit ) {

			//		alert('Loan amount cannot be larger than PF Total amount');
			//		return;
   //             }

			//	if (amount == '') {

			//		alert('Kindly enter loan amount');
			//		return;
			//	}
   //             else {
   //                 temppurpose = 0;
			//		permpurpose = $('#permpurposeparent').val();
   //                 subpurpose = $('#subpurpose').val();

   //             }

   //             if ($("#subpurpose option").length > 1) {

			//		if (subpurpose==0) {
   //                     alert('Subpurpose is required');
   //                     return;
			//		}
   //             }


   // //            var reader = new FileReader();
   // //            var arrayOfAttachments = [{ name: '', filedata: '' }];

			//	//for (var i = 0; i < DocArray.length; i++) {
			//	//	var fileUpload = $("#attach_" + DocArray[i].attachmentid + "").get(0);
   // //                var files = fileUpload.files;
			//	//	//reader = new FileReader();
   // //               // reader.readAsDataURL(files[0]);
			//	//	//var byteData = files;
			//	//	//byteData = byteData.split(';')[1].replace("base64,", "");
			//	//	arrayOfAttachments.push({ name: files[0].name, filedata: files[0] });
			//	//}









			//	//// Create FormData object  
			//	//var fileData = new FormData();
   // //            // Looping over all files and add it to FormData object  
   // //            for (var i = 0; i < DocArray.length; i++) {
   // //                var fileUpload = $("#attach_" + DocArray[i].attachmentid +"").get(0);
			//	//	var files = fileUpload.files;
			//	//	fileData.append(files[0].name, files[0]);
			//	//}






			//	//// Adding one more key to FormData object  
			//	//fileData.append('withdrawaltype', withdrawaltype);
   // //            fileData.append('temppurpose', temppurpose);
			//	//fileData.append('permpurpose', permpurpose);
   // //            fileData.append('subpurpose', subpurpose);
			//	//fileData.append('noofinstallment', noofinstallment);
			//	//fileData.append('amount', amount);
			//	//fileData.append('currency', currency);
   // //            fileData.append('notes', notes);
   // //            fileData.append('title', title);
			//	//fileData.append('requireddate', requireddate);

			//	var fileUpload = $("#attach_4").get(0);
			//	var files = fileUpload.files;

			//	var fileData = new FormData();

			//	fileData.append(files[0].name, files[0]);
			//	$.ajax({
			//		type: "POST",
			//		url: '/business/services/EssService.asmx/SubmitWithdrawalRequest',
			//		data: '{withdrawaltype: ' + JSON.stringify(withdrawaltype) + ',temppurpose: ' + JSON.stringify(temppurpose) + ',permpurpose: ' + JSON.stringify(permpurpose) + ',subpurpose: ' + JSON.stringify(subpurpose) + ',noofinstallment: ' + JSON.stringify(noofinstallment) + ',amount: ' + JSON.stringify(amount) + ',currency: ' + JSON.stringify(currency) + ',notes: ' + JSON.stringify(notes) + ',title: ' + JSON.stringify(title) + ',requireddate: ' + JSON.stringify(requireddate) + ',fileData : ' + fileData+'}',
			//		/*data: '{fileData : ' + JSON.stringify(fileData) + ' }',*/
   //                 contentType: "application/json; charset=utf-8",
			//		dataType: "json",
			//		success: function (response) {

   //                     alert(response.d)
   //                     location.reload();


			//		},

			//		error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); alert(errorThrown) }
			//	});



			//}


		</script>
    </form>
    <style>


    </style>
</body>
</html>
