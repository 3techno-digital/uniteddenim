﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.Manage
{
    public partial class AttendanceAdjustment : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int DirectReportEmployeeID
        {
            get
            {
                if (ViewState["DirectReportEmployeeID"] != null)
                {
                    return (int)ViewState["DirectReportEmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DirectReportEmployeeID"] = value;
            }
        }

        protected string DocumentStatus
        {
            get
            {
                if (ViewState["DocumentStatus"] != null)
                {
                    return (string)ViewState["DocumentStatus"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocumentStatus"] = value;
            }
        }

        protected int AttendanceAdjustmentID
        {
            get
            {
                if (ViewState["AttendanceAdjustmentID"] != null)
                {
                    return (int)ViewState["AttendanceAdjustmentID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AttendanceAdjustmentID"] = value;
            }
        }

        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return "Saved as Draft";
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    CheckSessions();
                    ViewState["AttendanceAdjustmentID"] = null;
                    BindEmployeeDropdown();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/attendance-adjustment";
                    ddlEmployee.SelectedValue = Session["EmployeeID"].ToString();
                    btnApprove.Visible = btnReview.Visible = btnRevApprove.Visible = lnkReject.Visible =
                    lnkDelete.Visible = btnSubForReview.Visible = btnDisapprove.Visible = btnRejDisApprove.Visible = false;

                    divAlertMsg.Visible = false;
                    clearFields();
                    checkDocStatus();
                    
                    if (HttpContext.Current.Items["AttendanceAdjustmentID"] != null)
                    {
                        AttendanceAdjustmentID = Convert.ToInt32(HttpContext.Current.Items["AttendanceAdjustmentID"].ToString());
                        getAttendanceAdjustmentByID(AttendanceAdjustmentID);
                        CheckAccess();
                    }
                    else if (HttpContext.Current.Items["AttendanceID"] != null)
                    {
                        objDB.AttendanceID = Convert.ToInt32(HttpContext.Current.Items["AttendanceID"].ToString());
                        DataTable dt = objDB.GetEmployeeAttendanceByID(ref errorMsg);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                            txtDescription.Value = dt.Rows[0]["Resultant"].ToString();
                            txtStartTime.Value = DateTime.Parse(dt.Rows[0]["TimeIn"].ToString()).ToString("hh:mm tt");
                            txtEndTime.Value = DateTime.Parse(dt.Rows[0]["TimeOut"].ToString()).ToString("hh:mm tt");
                            txtDate.Value = DateTime.Parse(dt.Rows[0]["TimeIn2"].ToString()).ToString("dd-MMM-yyyy");
                            objDB.EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"].ToString());

                            DataTable dtEmp = objDB.GetEmployeeByID(ref errorMsg);
                            if (dtEmp != null)
                            {
                                if (dtEmp.Rows.Count > 0 && dtEmp.Rows[0]["DirectReportingPerson"].ToString() != "")
                                {
                                    DirectReportEmployeeID = Convert.ToInt32(dtEmp.Rows[0]["DirectReportingPerson"].ToString());
                                }
                            }

                            ddlEmployee.Enabled = false;
                            txtStartTime.Disabled =
                            txtEndTime.Disabled =
                            txtDate.Disabled =
                            txtDescription.Disabled = true;
                        }
                    }
                    else
                    {
                        ddlEmployee.Enabled = true;
                        txtStartTime.Disabled =
                        txtEndTime.Disabled =
                        txtDate.Disabled =
                        txtDescription.Disabled = false;
                        DocumentStatus = "Data Submitted for Review";
                        btnSave.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void checkDocStatus()
        {
            btnSave.Visible = btnReview.Visible = lnkDelete.Visible = btnSubForReview.Visible = btnApprove.Visible =
            btnReview.Visible = btnRevApprove.Visible = lnkReject.Visible = lnkDelete.Visible = btnDisapprove.Visible =
            btnRejDisApprove.Visible = false;

            if (Session["EmployeeID"].ToString() == ddlEmployee.SelectedValue)
            {
                
                btnApprove.Visible = false;
                btnDisapprove.Visible = false;
                btnReview.Visible = false;
                lnkReject.Visible = false;
                btnSave.Visible = false;
            }
            else if (Session["EmployeeID"].ToString() == DirectReportEmployeeID.ToString())
            {
                if (DocumentStatus == "Data Submitted for Review")
                {
                    btnReview.Visible = true;
                    lnkReject.Visible = true;
                    btnSave.Visible = true;
                }
                else if (DocumentStatus == "Reviewed")
                {
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                    btnSave.Visible = true;
                }
            }
     
        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = btnReview.Visible = lnkDelete.Visible = btnSubForReview.Visible = btnApprove.Visible =
                btnReview.Visible = btnRevApprove.Visible = lnkReject.Visible = lnkDelete.Visible = btnDisapprove.Visible =
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "AttendanceAdjustment";
                objDB.PrimaryColumnnName = "AttendanceAdjustmentID";
                objDB.PrimaryColumnValue = AttendanceAdjustmentID.ToString();
                objDB.DocName = "Loans";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                   
                }
                
                



            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getAttendanceAdjustmentByID(int AttendanceAdjustmentID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                dt = objDB.GetAttendanceAdjustmentByID(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        DocumentStatus = dt.Rows[0]["DocStatus"].ToString();
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        txtDescription.Value = dt.Rows[0]["Resultant"].ToString();

                        txtStartTime.Value = DateTime.Parse(dt.Rows[0]["TimeIn"].ToString()).ToString("hh:mm tt");
                        txtEndTime.Value = DateTime.Parse(dt.Rows[0]["TimeOut"].ToString()).ToString("hh:mm tt");

                        txtDate.Value = DateTime.Parse(dt.Rows[0]["AttendanceDate"].ToString()).ToString("dd-MMM-yyyy"); ;
                        objDB.EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"].ToString());
                        DataTable dtEmp = objDB.GetEmployeeByID(ref errorMsg);
                        if (dtEmp != null)
                        {
                            string str = dtEmp.Rows[0]["DirectReportingPerson"].ToString();
                            if (dtEmp.Rows.Count > 0 && !string.IsNullOrEmpty(dtEmp.Rows[0]["DirectReportingPerson"].ToString()))
                            {
                                DirectReportEmployeeID = Convert.ToInt32(dtEmp.Rows[0]["DirectReportingPerson"].ToString());
                            }
                        }
                    }
                }

                Common.addlog("View", "ESS", "AttendanceAdjustment \"" + ddlEmployee.SelectedItem.Text + "\" Viewed", "AttendanceAdjustment", AttendanceAdjustmentID);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            divAlertMsg.Visible = false;

            try
            {
                string res = "";
                string today = DateTime.Parse(txtDate.Value).ToShortDateString();
                string tommorow = DateTime.Parse(txtDate.Value).AddDays(1).ToShortDateString();
                DateTime Fromdate = DateTime.Parse(txtStartTime.Value), Todate = DateTime.Parse(txtEndTime.Value);
                DateTime Date = DateTime.Parse(txtDate.Value);
                Fromdate = DateTime.Parse(Date.ToString("dd-MMM-yyyy") + " " + Fromdate.ToString("hh:mm:ss tt"));
                Todate = DateTime.Parse(Date.ToString("dd-MMM-yyyy") + " " + Todate.ToString("hh:mm:ss tt"));
                Todate = Todate < Fromdate ? Todate.AddDays(1) : Todate;

                if (Date > DateTime.Now)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Date Should be Less than Current Date";
                    return;
                }

                double hours = (Todate - Fromdate).TotalHours;
                if (hours > 16)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Adjustment should not exceed 16 hours";
                    return;
                }
                else
                {
                    objDB.Date = Date.ToString("dd-MMM-yyyy");
                    objDB.StartDate = Fromdate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                    objDB.EndDate = Todate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                    objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                }

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.TimeIn = Fromdate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                objDB.TimeOUt = Todate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                objDB.BreakStartTime = "";
                objDB.BreakEndTime = "";
                objDB.Resultant = txtDescription.Value;
                objDB.Date = Date.ToString("dd-MMM-yyyy");

                int AdjustmentId = 0;

                if (HttpContext.Current.Items["AttendanceAdjustmentID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                    res = objDB.UpdateAttendanceAdjustment();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddAttendanceAdjustment(ref AdjustmentId);

                    clearFields();
                }

                if (res == "New Attendance Adjustment Added" || res == "Attendance Adjustment Data Updated")
                {
                    if (res == "New Attendance Adjustment Added")
                    {
                        Common.addlog("Add", "ESS", "New AttendanceAdjustment \"" + objDB.Title + "\" Added", "AttendanceAdjustment");
                        Common.addlogNew("Data Submitted for Review", "AttendanceAdjustment",
                  "Add Attendance Adjustment \"" + objDB.Title + "\" Added",
                  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + AdjustmentId.ToString(),
                  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + AdjustmentId.ToString(), "Attendance Adjustment", "AttendanceAdjustment", "Direct Reporting");
                    }

                    if (res == "AttendanceAdjustment Data Updated")
                    {
                        Common.addlog("Update", "ESS", "AttendanceAdjustment \"" + objDB.Title + "\" Updated", "AttendanceAdjustment", AttendanceAdjustmentID);
                    }

                    if (DocStatus == "Approved")
                    {
                        objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.ApprovedBy = Session["UserName"].ToString();
                        objDB.ApproveAttendanceAdjustment();
                    }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void clearFields()
        {
            //ddlEmployee.SelectedIndex = -1;
            txtStartTime.Value =
            txtEndTime.Value =
            txtDate.Value =
            txtDescription.Value = "";
        }

        private void BindEmployeeDropdown()
        {
            try
            {
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            divAlertMsg.Visible = false;

            try
            {

                if(ddlEmployee.SelectedValue == Session["EmployeeID"].ToString())
				{


                    objDB.FindIndirectMgr = Convert.ToInt32(Session["EmployeeID"].ToString());
                     if (objDB.GetIndirectReportedEmployeeByEmployeeID(ref errorMsg).Rows[0][0].ToString() == Session["EmployeeID"].ToString())
                    {
                        // self approval

                        // only if direct mgr belongs to US with limited user access and current login is indirect mgr 
                        if (objDB.CheckIFDirectMgrBelongsToUnitedState(ref errorMsg).Rows[0][0].ToString() == "1")
                        {
                            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                            string res = Common.addAccessLevels(btn.ID.ToString(), "AttendanceAdjustment", "AttendanceAdjustmentID", HttpContext.Current.Items["AttendanceAdjustmentID"].ToString(), Session["UserName"].ToString());

                        }

                        else
                        {
                            // self approval not allowed
                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                            pAlertMsg.InnerHtml = "Self Approval not allowed";
                        }

                    }
                    else
                    {
                        System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                        string res = Common.addAccessLevels(btn.ID.ToString(), "AttendanceAdjustment", "AttendanceAdjustmentID", HttpContext.Current.Items["AttendanceAdjustmentID"].ToString(), Session["UserName"].ToString());

                        if (res == "Reviewed Sucessfull")
                        {
                            DocumentStatus = "Reviewed";
                        }
                        if (btn.ID.ToString() == "btnSubForReview")
                        {
                            if (Session["EmployeeID"] != null)
                            {
                                if (ddlEmployee.SelectedValue == Session["EmployeeID"].ToString())
                                {
                                    objDB.Status = "Submited By Employee";
                                    objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                                    res = objDB.UpdateAttendanceAdjustmentStatus();

                                }
                                else if (DirectReportEmployeeID == Convert.ToInt32(Session["EmployeeID"].ToString()))
                                {
                                    objDB.Status = "Submited By Manager";
                                    objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                                    res = objDB.UpdateAttendanceAdjustmentStatus();
                                }
                            }
                        }

                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                        pAlertMsg.InnerHtml = res;
                        CheckAccess();
                    }



                }
                else
				{
                    System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                    string res = Common.addAccessLevels(btn.ID.ToString(), "AttendanceAdjustment", "AttendanceAdjustmentID", HttpContext.Current.Items["AttendanceAdjustmentID"].ToString(), Session["UserName"].ToString());

                    if (res == "Reviewed Sucessfull")
                    {
                        DocumentStatus = "Reviewed";
                    }
                    if (btn.ID.ToString() == "btnSubForReview")
                    {
                        if (Session["EmployeeID"] != null)
                        {
                            if (ddlEmployee.SelectedValue == Session["EmployeeID"].ToString())
                            {
                                objDB.Status = "Submited By Employee";
                                objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                                res = objDB.UpdateAttendanceAdjustmentStatus();

                            }
                            else if (DirectReportEmployeeID == Convert.ToInt32(Session["EmployeeID"].ToString()))
                            {
                                objDB.Status = "Submited By Manager";
                                objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                                res = objDB.UpdateAttendanceAdjustmentStatus();
                            }
                        }
                    }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                    CheckAccess();
                }
                
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            checkDocStatus();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            divAlertMsg.Visible = false;

            try
            {
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "AttendanceAdjustment", "AttendanceAdjustmentID", HttpContext.Current.Items["AttendanceAdjustmentID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "ESS", "AttendanceAdjustment of ID \"" + HttpContext.Current.Items["AttendanceAdjustmentID"].ToString() + "\" deleted", "AttendanceAdjustment", AttendanceAdjustmentID);
                if (res == "Deleted Succesfully" || res == "Rejected")
                {
                    objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                    objDB.EnableAttendanceAdjustment();
                }
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            divAlertMsg.Visible = false;

            try
            {
                string res;

                if (string.IsNullOrEmpty(txtAdjustmentNote.Value))
                {
                    res = "Please add a rejection note.";
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Please add a rejection note.";
                    return;
                }

                LinkButton btn = new LinkButton();
                res = Common.addAccessLevels("Reject", "AttendanceAdjustment", "AttendanceAdjustmentID", HttpContext.Current.Items["AttendanceAdjustmentID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "ESS", "AttendanceAdjustment of ID \"" + HttpContext.Current.Items["AttendanceAdjustmentID"].ToString() + "\" deleted", "AttendanceAdjustment", AttendanceAdjustmentID);

                if (res == "Rejected")
                {
                    objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                    objDB.AttendanceAdjustmentRemarks = txtAdjustmentNote.Value;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.RejectAttendanceAdjustment();
                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}