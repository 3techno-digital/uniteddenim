﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Leads.aspx.cs" Inherits="Technofinancials.ESS.Manage.Leads" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/CRM/Leads.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Leads</h3>
                        </div>


                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>

                                <div class="col-sm-4" style="margin-top: 10px;">
                                    <div class="pull-right">
                                        <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                        <button class="tf-save-btn" type="button" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>

                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                            </ContentTemplate>

                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSave" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h4>Title</h4>
                                        <input type="text" class="form-control" placeholder="Title" name="Title" id="txtTitle" runat="server" />
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <h4>Client</h4>
                                    <input type="text" class="form-control" placeholder="Client" name="Client" id="txtClient" runat="server" />
                                </div>

                                <div class="col-sm-4">
                                    <h4>Business Division</h4>
                                    <input type="text" class="form-control" placeholder="Business Division" name="Business Division" id="txtBD" runat="server" />
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h4>Generation Date</h4>
                                        <input type="text" class="form-control" placeholder="Generation Date" name="Generation Date" id="txtGD" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h4>Qualification Status</h4>
                                        <input type="text" class="form-control" placeholder="Qualification Status" name="Qualification Status" id="txtQS" runat="server" />
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h4>POC</h4>
                                        <input type="text" class="form-control" placeholder="POC" name="POC" id="txtPOC" runat="server" />
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h4>Total Value</h4>
                                        <input type="text" class="form-control" placeholder="Total Value" name="Total Value" id="txtTV" runat="server" />
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h4>Description</h4>
                                        <textarea class="form-control" rows="5" name="companyDescription" id="txtDescription" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="clearfix">&nbsp;</div>
                    <h3>Attachments
                    </h3>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:GridView ID="gvFiles" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Attachment">
                                        <ItemTemplate>
                                            <a runat="server" id="lblTitle" href='<%# Eval("FilePath") %>'><%# Eval("Title") %></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <h3>Tasks</h3>
                    <div class="clearfix">&nbsp;</div>
                    <asp:UpdatePanel ID="UpdPnl" runat="server">
                        <ContentTemplate>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:GridView ID="gvTasks" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" ShowFooter="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr. No">
                                                <ItemTemplate>
                                                     <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblEmployee" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Title">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="DeadLine">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblDeadLine" Text='<%# Eval("DeadLine") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblDesription" Text='<%# Eval("Description") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField ShowHeader="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkIsCompleted" runat="server" CssClass="delete-class" Visible='<%# Eval("IsCompleted").ToString() == "True" ? false:true%>' CommandArgument='<%# Eval("TaskID")%>' Text='Complete' OnCommand="lnkCompleteTask_Command"></asp:LinkButton>
                                                    <asp:Label runat="server" ID="lblCompleted" Text='Completed' Visible='<%# Eval("IsCompleted").ToString() == "True" ? true:false%>'></asp:Label>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                    <div>


                        <div class="clearfix">&nbsp;</div>
                        <h3>Reviews</h3>
                        <div class="clearfix">&nbsp;</div>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:GridView ID="gvReviews" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvReviews_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr. No">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Added By">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <%--<asp:DropDownList ID="ddlEmployees" runat="server" data-plugin="select2" CssClass="form-control"></asp:DropDownList>--%>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Review   <span style='color:red !important;'>*</span>">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Review") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator21" ControlToValidate="txtReview" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidater" ForeColor="Red" />
                                                        <textarea class="form-control" rows="2" name="Review" id="txtReview" runat="server"></textarea>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="false">
                                                    <ItemTemplate>
                                                        <%--<asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveReview_Command"></asp:LinkButton>--%>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Button ID="btnAddReview" runat="server" CssClass="form-control rev-add-btn" Text="Add" ValidationGroup="btnValidater" CausesValidation="true" OnClick="btnAddReview_Click"></asp:Button>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="clearfix">&nbsp;</div>

                    </div>
       
                </section>

                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .month-table-show {
                display: none;
                margin-top: 40px;
            }

            .modal-dialog {
                width: 450px;
                margin: 30px auto;
            }

            section.app-content {
                padding-bottom: 53px;
            }

            .modal-footer {
                border-top: 0px solid #e5e5e5;
            }

            .hiring-create-new-btn {
                margin-top: 17px;
                border: none;
                background: none;
                color: #01769a;
            }

            .user-img-div img {
                width: 169px !important;
                display: block;
                margin: 0 auto;
            }

            .user-img-div {
                width: auto;
            }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
        </style>
        <script>
            $("#updFiles").change(function () {
                __doPostBack('LinkUploadFiles', '');
            });

        </script>

        <%--     <script>
         function pageLoad(sender, args) {
               
   $('#txtEditDate').datetimepicker();
              
   
            };
             
        </script>--%>
    </form>
</body>
</html>

