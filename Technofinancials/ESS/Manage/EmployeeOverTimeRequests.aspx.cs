﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.Manage
{
    public partial class EmployeeOverTimeRequests : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int DirectReportEmployeeID
        {
            get
            {
                if (ViewState["DirectReportEmployeeID"] != null)
                {
                    return (int)ViewState["DirectReportEmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DirectReportEmployeeID"] = value;
            }
        }

        protected string DocumentStatus
        {
            get
            {
                if (ViewState["DocumentStatus"] != null)
                {
                    return (string)ViewState["DocumentStatus"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocumentStatus"] = value;
            }
        }

        protected int AttendanceAdjustmentID
        {
            get
            {
                if (ViewState["AttendanceAdjustmentID"] != null)
                {
                    return (int)ViewState["AttendanceAdjustmentID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AttendanceAdjustmentID"] = value;
            }
        }

        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return "Saved as Draft";
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    CheckSessions();
                    ViewState["AttendanceAdjustmentID"] = null;
                    BindEmployeeDropdown();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/OverTime-Details";

                    btnReview.Visible = false;
                    ddlEmployee.Enabled = true;
                    txtStartTime.Disabled =
                    txtEndTime.Disabled =
                    txtDate.Disabled = false;

                    divAlertMsg.Visible = false;
                    clearFields();
                    if (HttpContext.Current.Items["AttendanceID"] != null)
                    {
                        int AttendanceID = Convert.ToInt32(HttpContext.Current.Items["AttendanceID"].ToString());
                        DataTable dt = objDB.GetEmployeeOverTimeRecord(AttendanceID, ref errorMsg);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                            txtStartTime.Value = DateTime.Parse(dt.Rows[0]["TimeIn"].ToString()).ToString("hh:mm tt");
                            txtEndTime.Value = DateTime.Parse(dt.Rows[0]["TimeOut"].ToString()).ToString("hh:mm tt");
                            txtDate.Value = DateTime.Parse(dt.Rows[0]["AttendanceDate"].ToString()).ToString("dd-MMM-yyyy");
                            txtRequestremarks.Value = dt.Rows[0]["RequestRemarks"].ToString();
                            txtRequestOTHours.Value = dt.Rows[0]["RequestOTHours"].ToString();
                            txtReviewedremarks.Value = dt.Rows[0]["ReviewedRemarks"].ToString();
                            txtReviewedOTHours.Value = dt.Rows[0]["ReviewedOTHours"].ToString();

                            ddlEmployee.Enabled = false;
                            txtStartTime.Disabled =
                            txtEndTime.Disabled =
                            txtDate.Disabled = true;
                            
                            btnReview.Visible = false;
                            btnSave.Visible = false;
                            
                            if (dt.Rows[0]["Status"] == DBNull.Value)
                            {
                                btnSave.Visible = true;
                                txtRequestOTHours.Disabled = false;
                                txtRequestremarks.Disabled = false;
                                txtReviewedOTHours.Visible = false;
                                txtReviewedremarks.Visible = false;
                            }
                            else if (dt.Rows[0]["Status"].ToString() == "Submitted for Review")
                            {
                                btnReview.Visible = true;
                                txtRequestOTHours.Disabled = true;
                                txtRequestremarks.Disabled = true;
                                txtReviewedOTHours.Visible = true;
                                txtReviewedremarks.Visible = true;

                            }
                            else if (dt.Rows[0]["Status"].ToString() == "Reviewed")
                            {
                                txtRequestOTHours.Disabled = true;
                                txtRequestremarks.Disabled = true;
                                txtReviewedOTHours.Disabled = true;
                                txtReviewedremarks.Disabled = true;
                                txtReviewedOTHours.Visible = true;
                                txtReviewedremarks.Visible = true;

                            }
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                int EmployeeId, AttendanceId;
                EmployeeId = int.Parse(ddlEmployee.SelectedValue);
                AttendanceId = int.Parse(HttpContext.Current.Items["AttendanceID"].ToString());
                float OTHours = 0;
                float.TryParse(txtRequestOTHours.Value, out OTHours);
                txtRequestOTHours.Value = OTHours.ToString();
                int CurrentEmployeeId = int.Parse(Session["EmployeeId"].ToString());
                string errormsg = "";
                DataTable dt = objDB.OverTimeRequest(AttendanceId, EmployeeId, OTHours, txtRequestremarks.Value, CurrentEmployeeId, ref errormsg);
                if (dt.Rows.Count > 0)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = dt.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            ddlEmployee.SelectedIndex = -1;
            txtStartTime.Value =
            //txtBreakTime.Value =
            //txtBreakEndTime.Value =
            txtEndTime.Value =
            txtDate.Value = "";
           // txtDescription.Value = "";
        }

        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnReview_ServerClick(object sender, EventArgs e)
        {
            try
            {
                divAlertMsg.Visible = false;
                if (txtReviewedOTHours.Value != txtRequestOTHours.Value && txtReviewedremarks.Value != "")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Remarks Required";
                    return;
                }
                CheckSessions();
                int EmployeeId, AttendanceId;
                EmployeeId = int.Parse(ddlEmployee.SelectedValue);
                AttendanceId = int.Parse(HttpContext.Current.Items["AttendanceID"].ToString());
                float OTHours = 0;
                float.TryParse(txtReviewedOTHours.Value, out OTHours);
                txtReviewedOTHours.Value = OTHours.ToString();
                int CurrentEmployeeId = int.Parse(Session["EmployeeId"].ToString());
                DateTime date = DateTime.Parse(txtDate.Value);
                string errormsg = "";
                DataTable dt = objDB.OverTimeReview(date, EmployeeId, OTHours, txtReviewedremarks.Value, CurrentEmployeeId, ref errormsg);
                if (dt.Rows.Count > 0)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = dt.Rows[0][0].ToString();
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}