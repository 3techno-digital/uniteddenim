﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.Manage
{
    public partial class Leaves : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int LeaveID
        {
            get
            {
                if (ViewState["LeaveID"] != null)
                {
                    return (int)ViewState["LeaveID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["LeaveID"] = value;
            }
        }
        // string AttendanceDate
        //{
        //    get
        //    {
        //        if (ViewState["AttendanceDate"] != null)
        //        {
        //            return ViewState["AttendanceDate"].ToString();
        //        }
        //        else
        //        {
        //            return "";
        //        }
        //    }

        //    set
        //    {
        //        ViewState["AttendanceDate"] = value;
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {
                    CheckSessions();

                    ViewState["LeaveID"] = null;
                    btnReview.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/employee-leaves";

                    divAlertMsg.Visible = false;

                    

                    if (HttpContext.Current.Items["LeaveID"] != null)
                    {
                        //ddlLeaves.Enabled = false;
                        LeaveID = Convert.ToInt32(HttpContext.Current.Items["LeaveID"].ToString());

                        getLeaveByID(LeaveID);
                        ContainerddlLeave.Visible = false;
                        ContainertxtLeave.Visible = true;
                        txtLeaves.Value = ddlLeaves.SelectedItem.Text;

                        CheckAccess();
                    }
                    else
                    {
                        
                        BindEmployeesLeaves(Convert.ToInt32(Session["EmployeeID"].ToString()));
                        ContainerddlLeave.Visible = true;
                        ContainertxtLeave.Visible = false;
                        txtLeaves.Value = "";
                        //CheckAccess();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                //btnApprove.Visible = false;
                btnReview.Visible = false;
                //btnRevApprove.Visible = false;
                //lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                //btnDisapprove.Visible = false;
                //btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "EMPLeaves";
                objDB.PrimaryColumnnName = "EMPLeaveID";
                objDB.PrimaryColumnValue = LeaveID.ToString();
                objDB.DocName = "OtherExpenses";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    //lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    //btnApprove.Visible = true;
                    //btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    //btnRevApprove.Visible = true;

                    //btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    //btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getLeaveByID(int LeaveID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.EMPLeaveID = LeaveID;
                dt = objDB.GetEMPLeaveByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtFromDate.Value = DateTime.Parse(dt.Rows[0]["StartDate"].ToString()).ToString("dd-MMM-yyyy");
                        txtToDate.Value = DateTime.Parse(dt.Rows[0]["EndDate"].ToString()).ToString("dd-MMM-yyyy");
                        //ddlEmployees.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        BindEmployeesLeaves(Convert.ToInt32(dt.Rows[0]["EmployeeID"].ToString()));
                        //ddlEmployee_SelectedIndexChanged(null, null);
                        ddlLeaves.SelectedValue = dt.Rows[0]["LeaveID"].ToString();
                        objDB.DocID = LeaveID;
                        objDB.DocType = "Leaves";
                        txtNotes.Value = objDB.GetDocNotes();
                    }
                }
                Common.addlog("View", "HR", "Leaves \"" + dt.Rows[0]["EmployeeName"].ToString() + "\" Viewed", "EMPLeave", objDB.EMPLeaveID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }
        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (CheckLeavesAvailability())
                {
                    string res = "";
                    System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;

                    if (btn.ID.ToString() == "btnApprove" || btn.ID.ToString() == "btnRevApprove" || btn.ID.ToString() == "btnApproveM")
                    {
                        objDB.EMPLeaveID = LeaveID;
                        res = objDB.AddLeavesToEmployeeAttendance();
                        if (res == "Added")
                        {
                            res = Common.addAccessLevels(btn.ID.ToString(), "EMPLeaves", "EMPLeaveID", HttpContext.Current.Items["LeaveID"].ToString(), Session["UserName"].ToString());
                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                            pAlertMsg.InnerHtml = res;
                        }
                        else
                        {
                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                            pAlertMsg.InnerHtml = res;
                        }
                    }
                    else
                    {
                        res = Common.addAccessLevels(btn.ID.ToString(), "EMPLeaves", "EMPLeaveID", HttpContext.Current.Items["LeaveID"].ToString(), Session["UserName"].ToString());
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                        pAlertMsg.InnerHtml = res;
                    }

                    //res = Common.addAccessLevels(btn.ID.ToString(), "EMPLeaves", "EMPLeaveID", HttpContext.Current.Items["LeaveID"].ToString(), Session["UserName"].ToString());
                    //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                    //{
                    //    objDB.EMPLeaveID = LeaveID;
                    //    objDB.AddLeavesToEmployeeAttendance();

                    //}
                    Common.addlogNew(res, "ESS", "Leave of ID\"" + HttpContext.Current.Items["LeaveID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/view/employee-leaves", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/leaves/edit-leave-" + HttpContext.Current.Items["LeaveID"].ToString(), "Leave \"" + txtLeaves.Value + "\"", "EMPLeaves", "Holidays", Convert.ToInt32(HttpContext.Current.Items["LeaveID"].ToString()));

                    //Common.addlog(res, "HR", "Leave of ID\"" + HttpContext.Current.Items["LeaveID"].ToString() + "\" Status Changed", "EMPLeaves", LeaveID);
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Can't Proceed because selected no of leaves is not avalible to this employee";

                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        bool CheckLeavesAvailability()
        {
            string s;
            objDB.LeaveID = ddlLeaves.SelectedValue;
            objDB.StartDate = txtFromDate.Value;
            objDB.EndDate = txtToDate.Value;
            s = objDB.CheckLeavesAvailability();
            if (s == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.StartDate = txtFromDate.Value;
                objDB.EndDate = txtToDate.Value;
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.LeaveID = ddlLeaves.SelectedValue;
                int Docid = 0;
                if (HttpContext.Current.Items["LeaveID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.EMPLeaveID = LeaveID;
                    string id = "";
                    id = objDB.UpdateEMPLeave();

                    if (!(int.TryParse(id, out Docid)))
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = id;
                        return;
                    }

                    Docid = Convert.ToInt32(id);
                    res = "Leave Data Updated";
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddEMPLeave();
                    //ClearFields();

                }

                objDB.DocType = "Leaves";
                objDB.DocID = Docid;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

                if (res == "New Leave Added" || res == "Leave Data Updated")
                {
                    if (res == "New Leave Added")
                    {
                        Common.addlog("Add", "ESS", "New Leave \"" + (objDB.EMPLeaveID).ToString() + "\" Added", "EMPLeaves");
                        clearFields();
                    }
                    if (res == "Leave Data Updated") { Common.addlog("Update", "ESS", "Leave \"" + (objDB.EMPLeaveID).ToString() + "\" Updated", "EMPLeaves", objDB.EMPLeaveID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        //public void ClearFields()
        //{
        //    //ddlEmployees.SelectedValue = "0";
        //    ddlLeaves.SelectedValue = "0";
        //    txtFromDate.Value = "";
        //    txtNotes.Value = "";
        //    txtToDate.Value = "";
        //}

        //private void BindEmployeesDropDown()
        //{
        //    try
        //    {
        //        CheckSessions();
        //        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
        //        ddlEmployees.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
        //        ddlEmployees.DataTextField = "EmployeeName";
        //        ddlEmployees.DataValueField = "EmployeeID";
        //        ddlEmployees.DataBind();
        //        ddlEmployees.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }

        //}

        private void clearFields()
        {
            txtFromDate.Value = "";
            txtToDate.Value = "";
            ddlLeaves.Items.Clear();
            BindEmployeesLeaves(Convert.ToInt32(Session["EmployeeID"].ToString()));
            //ddlLeaves.Enabled = false;
            //ddlEmployees.SelectedIndex = 0;
        }

        private void BindEmployeesLeaves(int EmployeeID)
        {
            try
            {
                CheckSessions();
                ddlLeaves.Items.Clear();
                objDB.EmployeeID = EmployeeID;
                ddlLeaves.DataSource = objDB.GetAllLeavesByEmployeeIDForDropDown(ref errorMsg);
                ddlLeaves.DataTextField = "LeavesDetail";
                ddlLeaves.DataValueField = "LeaveID";
                ddlLeaves.DataBind();
                ddlLeaves.Items.Insert(0, new ListItem("--- Select Leave Type ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }


        private void getEmployeeAttendanceByEmployeeID(int EmpID)
        {

        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {


        }

        protected void ddlLeaves_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "EMPLeaves", "EMPLeaveID", HttpContext.Current.Items["LeaveID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "Leave of ID \"" + objDB.EMPLeaveID + "\" deleted", "EMPLeaves", objDB.EMPLeaveID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                objDB.EMPLeaveID = LeaveID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteHoliday();
                Common.addlog("Leave Rejected", "HR", "Leave of ID\"" + HttpContext.Current.Items["LeaveID"].ToString() + "\" Leave Rejected", "EMPLeaves", LeaveID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Holiday Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }
    }
}