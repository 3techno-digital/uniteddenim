﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TravelExpense.aspx.cs" Inherits="Technofinancials.ESS.Manage.TravelExpense" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h1 class="m-0 text-dark">Other Expenses</h1>
                            </div>
                            <!-- /.col -->

                            <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                <ContentTemplate>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div style="text-align: right;">
                                            <button class="AD_btn" id="btnSave" runat="server" validationgroup="btnValidate" type="button">Save</button>
                                              <a class="AD_btn"  href="/ESS/TravelExpense.aspx">Back</a>
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>



                <section class="app-content">


                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>


                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Code <span style="color: red !important;">*</span> </label>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtCode" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" disabled="disabled" placeholder="001" id="txtCode" runat="server" />
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <label>Data Entry Date <span style="color: red !important;">*</span> </label>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="29-12-1993" id="txtDate" disabled="disabled" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="input-group input-group-lg">
                                                <label>
                                                    Title <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="txtTitle" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                <input type="text" class="form-control" value="Travel Expense" id="txtTitle" runat="server" placeholder="Title" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-sm-12">
                                        <div class="form-group">
                                            <h4>Notes</h4>
                                            <textarea style="width:430px !important; min-height:107px !important;   " class="form-control" rows="3" name="Description" id="Textarea1" runat="server"></textarea>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="amount">
                        <h5>Amount $<span id="TxtAmount">100,000</span></h5>
                        
                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group" id="divAlertMsg" runat="server">
                                                    <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                        <span>
                                                            <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                        </span>
                                                        <p id="pAlertMsg" runat="server">
                                                        </p>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>

                            </div>

                            

                            <div id="divPay" runat="server">
                            </div>
                            <div class="clearfix">&nbsp;</div>
                             <div class="content-header second_heading">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-sm-6">
                                        <h1 class="m-0 text-dark">Expense Details</h1>
                                    </div>
                                    <!-- /.col -->

                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="add-table-div">
                                        <div class="card-body table-responsive tbl-cont ">

                                            <table class="table table-hover tbl-pagenation table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Exp. Type</th>
                                                        <th>Description</th>
                                                        <th>Amount</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                        <td>1
                                                        </td>
                                                        <td>
                                                            <select class="select2">
                                                                <option value="value">Airfare</option>
                                                                <option value="value">Lodging</option>
                                                                <option value="value">Ground Transportation</option>
                                                                <option value="value">Meals & Tips</option>
                                                                <option value="value">Conferences and Seminars</option>
                                                                <option value="value">Miles</option>
                                                                <option value="value">Mileage Reimbursement</option>
                                                                <option value="value">Miscellaneous</option>

                                                            </select>
                                                        </td>
                                                        <td>KHI-DBX
                                                        </td>

                                                        <td>75,000
                                                        </td>
                                                        <td>
                                                            <button type="button" class="AD_btn_inn stock_add" id="btnAddItemsToStocks">
                                                                <i class="fa fa-plus" aria-hidden="true"></i> Delete
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2
                                                        </td>
                                                        <td>
                                                            <select class="select2">
                                                                <option value="value">Meal</option>

                                                            </select>
                                                        </td>
                                                        <td>Dinner With Client
                                                        </td>

                                                        <td>25,000
                                                        </td>
                                                        <td>
                                                            <button type="button" class="AD_btn_inn stock_add" id="btnAddItemsToStocks">
                                                                <i class="fa fa-plus" aria-hidden="true"></i> Delete
                                                            </button>
                                                        </td>
                                                    </tr>

                                                     <tr>
                                                        <td>3
                                                        </td>
                                                        <td>
                                                            <select class="select2">
                                                                <option value="value">Airfare</option>
                                                                <option value="value">Lodging</option>
                                                                <option value="value">Ground Transportation</option>
                                                                <option value="value">Meals & Tips</option>
                                                                <option value="value">Conferences and Seminars</option>
                                                                <option value="value">Miles</option>
                                                                <option value="value">Mileage Reimbursement</option>
                                                                <option value="value">Miscellaneous</option>

                                                            </select>
                                                        </td>
                                                        <td><input type="text" />
                                                        </td>

                                                        <td><input type="number"  />
                                                        </td>
                                                        <td>
                                                            <button type="button" class="AD_btn_inn stock_add" id="btnAddItemsToStocks">
                                                                <i class="fa fa-plus" aria-hidden="true"></i> Add
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="content-header second_heading">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-sm-6">
                                        <h1 class="m-0 text-dark">Document</h1>
                                    </div>
                                    <!-- /.col -->

                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>
                      
                        <div class="clearfix">&nbsp;</div>

                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="row">
                                    
                                   
                                    <div class="col-md-12">
                                            <div class="input-group input-group-lg form-group">
                                                <label>
                                                    Document Title 
                                                    </label>
                                                <input type="text" class="form-control" value="" id="Text1" runat="server" placeholder="Title" />
                                            </div>
                                        </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h4>Attachments</h4>
                                        </div>
                                        <div class="file-upload">
                                            <div class="image-upload-wrap">
                                                <input class="file-upload-input" type="file" name="uploadFile" id="updLogo" onchange="readURL(this);" accept="image/*">
                                                <div class="drag-text">
                                                    <h3>Drag and drop a file or select add Image (MaxSize: 2mb)</h3>
                                                </div>
                                            </div>
                                            <div class="file-upload-content">
                                                <img class="file-upload-image" src="#" alt="Supplier image">
                                                <div class="image-title-wrap">
                                                    <button type="button" onclick="removeUpload()" class="remove-image">
                                                        Remove  <span><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="row">
                                    <div class="col-lg-12">

                                        <div class="card-body table-responsive tbl-cont ">
                                            <table class="table table-hover tbl-pagenation table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Title</th>
                                                        <th>Document</th>
                                                        <th>Download</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td> 
                                                        <td><a href="#">KHI-DBX Ticket</a></td>
                                                        <td> TravelTicket.pdf </td>
                                                        <td><button type="button" class="AD_btn_inn stock_add" id="btnAddItemsToStocks">
                                                                 Download
                                                            </button></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td> 
                                                        <td><a href="#">Dinner</a></td>
                                                        <td> Bill.pdf </td>
                                                        <td><button type="button" class="AD_btn_inn stock_add" id="btnAddItemsToStocks2">
                                                                 Download
                                                            </button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>


                            <%--<div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <h4>Notes</h4>
                                                <textarea class="form-control" rows="3" name="Description" id="txtDescription" runat="server"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h4>Attachment</h4>
                                            </div>
                                            <div class="file-upload">
                                                <div class="image-upload-wrap">
                                                    <input class="file-upload-input" type="file" name="uploadFile" id="updLogo" onchange="readURL(this);" accept="image/*">
                                                    <div class="drag-text">
                                                        <h3>Drag and drop a file or select add Image (MaxSize: 2mb)</h3>
                                                    </div>
                                                </div>
                                                <div class="file-upload-content">
                                                    <img class="file-upload-image" src="#" alt="Supplier image">
                                                    <div class="image-title-wrap">
                                                        <button type="button" onclick="removeUpload()" class="remove-image">
                                                            Remove  <span><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>
                                
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>
                            </div>--%>


                            <div class="row">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div>&nbsp;</div>
                    <div style="display: none;" class="row">
                        <div class="col-md-12">
                            <div class="form-group add-atr-file-upload">
                                <asp:FileUpload ID="itemAttachments" class="btn btn-primary" ClientIDMode="Static" runat="server" AllowMultiple="true" Style="display: none;"></asp:FileUpload>
                                <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('itemAttachments').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                Include Files (Attach Here)
                            </div>
                        </div>
                    </div>

                    <div>&nbsp;</div>
                    <div style="display:none;" class="dashboard-left inner-page" id="divAttachments" runat="server">
                        <h4>Attachments</h4>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <asp:GridView ID="gvAttachments" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeader="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="File Name">
                                            <ItemTemplate>
                                                <a href='<%# Eval("AttachmentPath") %>'><%# "File - " + (Container.DataItemIndex+1).ToString() %></a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                    <!-- Modal -->



                    <div class="modal fade M_set" id="paynow-modal" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="m-0 text-dark">Notes</h1>
                                    <div class="add_new">
                                        <button type="button" id="btnPay" "Receive" runat="server" validationgroup="btnValidatePN" class="AD_btn">Receive</button>
                                        <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                    </div>
                                </div>
                                <div class="modal-body">

                                    <asp:UpdatePanel ID="UPPayNow" runat="server">
                                        <ContentTemplate>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Remaining Amount</label>
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" value="0" placeholder="0" id="txtRemainingAmount" disabled="disabled" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Date</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="DD-MM-YYYY" id="txtDatePN" disabled="disabled" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Amount</label><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtAmountPN" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" />
                                                    <asp:CompareValidator runat="server" ID="cmpNumbers" ControlToValidate="txtAmountPN" ControlToCompare="txtRemainingAmount" Operator="LessThanEqual" Type="Double" ErrorMessage="The amount must be less then or equal to remaining amount" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" />
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" value="0" placeholder="0" id="txtAmountPN" runat="server" />
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row">

                                                <div class="col-md-4">
                                                    <label>Receive By</label>
                                                    <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlPayBy" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" /></label>
                                                <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlPayBy">
                                                    <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                                    <asp:ListItem Value="Bank">Bank</asp:ListItem>
                                                </asp:DropDownList>
                                                </div>
                                                <div class="col-md-4" id="accDiv" runat="server">
                                                    <label>Account</label>
                                                    <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlAccounts">
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="col-md-4">
                                                    <label>COA</label>
                                                    <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="ddlCOAPN" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidatePN" ForeColor="Red" /></label>
                                                <asp:DropDownList ClientIDMode="Static" data-plugin="select2" runat="server" class="form-control form-text input-group js-example-basic-single select2" ID="ddlCOAPN">
                                                </asp:DropDownList>
                                                </div>



                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                            </div>
                        </div>
                    </div>




                    <div class="modal fade M_set" id="notes-modal" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="m-0 text-dark">Notes</h1>
                                    <div class="add_new">
                                        <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                        <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <uc:Footer ID="footer1" runat="server" />
            <!-- .wrap -->
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            input.form-control.input-sm {
    display: none;
}
            .dataTables_filter {
    margin-bottom: 5px;
    display: none;
}
            .dt-buttons {
    display: none;
}
            .amount {
    text-align: center;
    padding-top: 50px;
}
            .amount h5 {
    font-size: 18px;
}
            .amount h5 {
    font-family: Noto-Regular !important;
}
            .amount span {
    font-family: Noto-SemiBold !important;
    display: block;
    font-size: 30px;
}
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
        </style>
        <!-- Modal -->
    </form>
</body>
</html>
