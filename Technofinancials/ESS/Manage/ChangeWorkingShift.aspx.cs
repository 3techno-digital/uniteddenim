﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.Manage
{
	public partial class ChangeWorkingShift : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int ShiftID
        {
            get
            {
                if (ViewState["ShiftID"] != null)
                {
                    return (int)ViewState["ShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["ShiftID"] = value;
            }
        }

        protected int EmployeeID
        {
            get
            {
                if (ViewState["EmployeeID"] != null)
                {
                    return (int)ViewState["EmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["EmployeeID"] = value;
            }
        }

        protected int PrevShiftID
        {
            get
            {
                if (ViewState["PrevShiftID"] != null)
                {
                    return (int)ViewState["PrevShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrevShiftID"] = value;
            }
        }
        
        protected int PrevParentShiftID
        {
            get
            {
                if (ViewState["PrevParentShiftID"] != null)
                {
                    return (int)ViewState["PrevParentShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrevParentShiftID"] = value;
            }
        }
        
        protected int ChangeWorkingShiftID
        {
            get
            {
                if (ViewState["ChangeWorkingShiftID"] != null)
                {
                    return (int)ViewState["ChangeWorkingShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["ChangeWorkingShiftID"] = value;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    shiftDaysDiv.Visible = false;
                    CheckSessions();
                    ViewState["ChangeWorkingShiftID"] = null;
                    BindEmployeeDropdown();
                    BindWorkingShift();

                    btnBack.HRef = ("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/ChangeWorkingShift");
                    divAlertMsg.Visible = false;


                    if (HttpContext.Current.Items["EmployeeID"] != null)
                    {
                        EmployeeID = Convert.ToInt32(HttpContext.Current.Items["EmployeeID"].ToString());
                        ddlEmployee.SelectedValue = EmployeeID.ToString();
                        getShiftsByEmployeeID(EmployeeID);

                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClickNew(object sender, EventArgs e)
        {
            string Response = "";
            divAlertMsg.Visible = false;
            CheckSessions();

            try
            {
                objDB.Hours = txtHours.Value;
                Response = CheckShiftDays();
                DataTable dt3;
                if (Response == "Ok")
                {
                    string response = CheckWorkingShiftDays();

                    if (Convert.ToInt32(ddlEmployee.SelectedValue) == 1)
                    {
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                        dt3 = objDB.GetAllApproveEmployeesAcceptHimByCompanyID(ref errorMsg);

                        foreach (DataRow row in dt3.Rows)
                        {
                            string res = "";

                            objDB.EmployeeID = Convert.ToInt32(row["EmployeeID"].ToString());
                            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                            objDB.WorkingShiftName = ddlWorkingShift.SelectedItem.Text;
                            objDB.parentShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
                            objDB.Monday = chkMonday.Checked;
                            objDB.Tuesday = chkTuesday.Checked;
                            objDB.Wednesday = chkWednesdat.Checked;
                            objDB.Thursday = chkThursday.Checked;
                            objDB.Friday = chkFriday.Checked;
                            objDB.Saturday = chkSaturday.Checked;
                            objDB.Sunday = chkSunday.Checked;
                            objDB.CreatedBy = Session["UserName"].ToString();

                            if (response == "New Shift")
                            {
                                ShiftID = Convert.ToInt32(objDB.AddShiftFromESS());
                                res = "New Shift Added and Assigned";
                                AddShiftDays();
                                objDB.ShiftID = ShiftID;
                                objDB.isSpecial = allowHolidayWorking.Checked == true ? true : false;
                                objDB.AllowWorkingHoliday();
                            }
                            else
                            {
                                objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
                                ShiftID = Convert.ToInt32(objDB.UpdateEmployeeShift());
                                objDB.ShiftID = ShiftID;
                                objDB.isSpecial = allowHolidayWorking.Checked == true ? true : false;
                                objDB.AllowWorkingHoliday();
                                res = "Shift Assigned";
                            }

                            if (res == "New Shift Added and Assigned" || res == "Shift Assigned")
                            {
                                PrevShiftID = ShiftID;
                                PrevParentShiftID = objDB.parentShiftID;

                                if (res == "New Shift Added and Assigned") { Common.addlog("Add", "ESS", "New Shift \"" + objDB.WorkingShiftName + "\" Added", "WorkingShifts"); }
                                if (res == "Shift Assigned") { Common.addlog("Update", "ESS", "Shift \"" + objDB.WorkingShiftName + "\" Updated", "WorkingShifts", objDB.ShiftID); }

                                divAlertMsg.Visible = true;
                                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                                pAlertMsg.InnerHtml = res;
                            }

                        }
                    }
                    else
                    {
                        string res = "";

                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.WorkingShiftName = ddlWorkingShift.SelectedItem.Text;
                        objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                        objDB.parentShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
                        objDB.Monday = chkMonday.Checked;
                        objDB.Tuesday = chkTuesday.Checked;
                        objDB.Wednesday = chkWednesdat.Checked;
                        objDB.Thursday = chkThursday.Checked;
                        objDB.Friday = chkFriday.Checked;
                        objDB.Saturday = chkSaturday.Checked;
                        objDB.Sunday = chkSunday.Checked;

                        objDB.CreatedBy = Session["UserName"].ToString();
                        if (response == "New Shift")
                        {
                            ShiftID = Convert.ToInt32(objDB.AddShiftFromESS());
                            res = "New Shift Added and Assigned";
                            AddShiftDays();
                            objDB.ShiftID = ShiftID;
                            objDB.isSpecial = allowHolidayWorking.Checked == true ? true : false;
                            objDB.AllowWorkingHoliday();
                        }
                        else
                        {
                            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
                            ShiftID = Convert.ToInt32(objDB.UpdateEmployeeShift());
                            objDB.ShiftID = ShiftID;
                            objDB.isSpecial = allowHolidayWorking.Checked == true ? true : false;
                            objDB.AllowWorkingHoliday();
                            res = "Shift Assigned";
                        }

                        if (res == "New Shift Added and Assigned" || res == "Shift Assigned")
                        {

                            PrevShiftID = ShiftID;
                            PrevParentShiftID = objDB.parentShiftID;

                            if (res == "New Shift Added and Assigned") { Common.addlog("Add", "ESS", "New Shift \"" + objDB.WorkingShiftName + "\" Added", "WorkingShifts"); }
                            if (res == "Shift Assigned") { Common.addlog("Update", "ESS", "Shift \"" + objDB.WorkingShiftName + "\" Updated", "WorkingShifts", objDB.ShiftID); }

                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                            pAlertMsg.InnerHtml = res;
                        }
                        else
                        {
                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                            pAlertMsg.InnerHtml = res;
                        }
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Working hours did not match";
                }
                clearFields();

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private string CheckShiftDays()
        {
            string res = "";
            #region Monday
            objDB.StartTime = txtMONStartTime.Value;
            objDB.LateInTime = txtMONLateInTime.Value;
            objDB.BreakStartTime = txtMONBreakStartTime.Value;
            objDB.BreakEndTime = txtMONBreakEndTime.Value;
            objDB.HalfDayStart = txtMONHalfDayStart.Value;
            objDB.EarlyOut = txtMONEarlyOut.Value;
            objDB.EndTime = txtMONEndTime.Value;
            objDB.isNightShift = chkMonNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Monday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkMonday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Tuesday

            objDB.StartTime = txtTUEStartTime.Value;
            objDB.LateInTime = txtTUELateInTime.Value;
            objDB.BreakStartTime = txtTUEBreakStartTime.Value;
            objDB.BreakEndTime = txtTUEBreakEndTime.Value;
            objDB.HalfDayStart = txtTUEHalfDayStart.Value;
            objDB.EarlyOut = txtTUEEarlyOut.Value;
            objDB.EndTime = txtTUEEndTime.Value;
            objDB.isNightShift = chkTUENightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Tuesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkTuesday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Wednesday
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.LateInTime = txtWEDLateInTime.Value;
            objDB.BreakStartTime = txtWEDBreakStartTime.Value;
            objDB.BreakEndTime = txtWEDBreakEndTime.Value;
            objDB.HalfDayStart = txtWEDHalfDayStart.Value;
            objDB.EarlyOut = txtWEDEarlyOut.Value;
            objDB.EndTime = txtWEDEndTime.Value;
            objDB.isNightShift = chkWEDNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Wednesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkWednesdat.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Thursday
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.LateInTime = txtTHULateInTime.Value;
            objDB.BreakStartTime = txtTHUBreakStartTime.Value;
            objDB.BreakEndTime = txtTHUBreakEndTime.Value;
            objDB.HalfDayStart = txtTHUHalfDayStart.Value;
            objDB.EarlyOut = txtTHUEarlyOut.Value;
            objDB.EndTime = txtTHUEndTime.Value;
            objDB.isNightShift = chkTHUNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Thursday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkThursday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Friday
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.LateInTime = txtFRILateInTime.Value;
            objDB.BreakStartTime = txtFRIBreakStartTime.Value;
            objDB.BreakEndTime = txtFRIBreakEndTime.Value;
            objDB.HalfDayStart = txtFRIHalfDayStart.Value;
            objDB.EarlyOut = txtFRIEarlyOut.Value;
            objDB.EndTime = txtFRIEndTime.Value;
            objDB.isNightShift = chkFRINightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Friday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkFriday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Saturday
            objDB.StartTime = txtSATStartTime.Value;
            objDB.LateInTime = txtSATLateInTime.Value;
            objDB.BreakStartTime = txtSATBreakStartTime.Value;
            objDB.BreakEndTime = txtSATBreakEndTime.Value;
            objDB.HalfDayStart = txtSATHalfDayStart.Value;
            objDB.EarlyOut = txtSATEarlyOut.Value;
            objDB.EndTime = txtSATEndTime.Value;
            objDB.isNightShift = chkSATNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Saturday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSaturday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Sunday
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.LateInTime = txtSUNLateInTime.Value;
            objDB.BreakStartTime = txtSUNBreakStartTime.Value;
            objDB.BreakEndTime = txtSUNBreakEndTime.Value;
            objDB.HalfDayStart = txtSUNHalfDayStart.Value;
            objDB.EarlyOut = txtSUNEarlyOut.Value;
            objDB.EndTime = txtSUNEndTime.Value;
            objDB.isNightShift = chkSUNNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Sunday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSunday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }

            #endregion


            return "Ok";

        }

        private void AddShiftDays()
        {

            #region Monday
            objDB.isOffDayOT = chkMondayOT.Checked;
            objDB.IsFlexible = chkMondayIsSpecial.Checked;
            objDB.StartTime = txtMONStartTime.Value;
            objDB.LateInTime = txtMONLateInTime.Value;
            objDB.TimeInExemption = txtMonTimeInExemption.Value;
            objDB.BreakStartTime = txtMONBreakStartTime.Value;
            objDB.BreakEndTime = txtMONBreakEndTime.Value;
            objDB.HalfDayStart = txtMONBreakStartTime.Value;
            objDB.EarlyOut = txtMONEarlyOut.Value;
            objDB.EndTime = txtMONEndTime.Value;
            objDB.isNightShift = chkMonNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Monday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkMonday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Tuesday

            objDB.isOffDayOT = chkTuesdayOT.Checked;
            objDB.IsFlexible = chkTuesdayIsSpecial.Checked;
            objDB.StartTime = txtTUEStartTime.Value;
            objDB.LateInTime = txtTUELateInTime.Value;
            objDB.TimeInExemption = txtTueTimeInExemption.Value;
            //objDB.BreakEarlyStartTime = txtTUEBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtTUEBreakStartTime.Value;
            objDB.HalfDayStart = txtTUEBreakStartTime.Value;
            objDB.BreakEndTime = txtTUEBreakEndTime.Value;
            //objDB.BreakLateTime = txtTUEBreakLateTime.Value;
            objDB.EarlyOut = txtTUEEarlyOut.Value;
            objDB.EndTime = txtTUEEndTime.Value;
            objDB.isNightShift = chkTUENightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Tuesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkTuesday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Wednesday
            objDB.isOffDayOT = chkWednesdayOT.Checked;
            objDB.IsFlexible = chkWednesdayIsSpecial.Checked;
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.TimeInExemption = txtWedTimeInExemption.Value;
            objDB.LateInTime = txtWEDLateInTime.Value;
            //objDB.BreakEarlyStartTime = txtWEDBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtWEDBreakStartTime.Value;
            objDB.HalfDayStart = txtWEDBreakStartTime.Value;
            objDB.BreakEndTime = txtWEDBreakEndTime.Value;
            //objDB.BreakLateTime = txtWEDBreakLateTime.Value;
            objDB.EarlyOut = txtWEDEarlyOut.Value;
            objDB.EndTime = txtWEDEndTime.Value;
            objDB.isNightShift = chkWEDNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Wednesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkWednesdat.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Thursday
            objDB.isOffDayOT = chkThursdayOT.Checked;
            objDB.IsFlexible = chkThursdayIsSpecial.Checked;
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.TimeInExemption = txtThursTimeInExemption.Value;
            objDB.LateInTime = txtTHULateInTime.Value;
            //objDB.BreakEarlyStartTime = txtTHUBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtTHUBreakStartTime.Value;
            objDB.HalfDayStart = txtTHUBreakStartTime.Value;
            objDB.BreakEndTime = txtTHUBreakEndTime.Value;
            //objDB.BreakLateTime = txtTHUBreakLateTime.Value;
            objDB.EarlyOut = txtTHUEarlyOut.Value;
            objDB.EndTime = txtTHUEndTime.Value;
            objDB.isNightShift = chkTHUNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Thursday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkThursday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Friday
            objDB.isOffDayOT = chkFridayOT.Checked;
            objDB.IsFlexible = chkFridayIsSpecial.Checked;
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.TimeInExemption = txtFriTimeInExemption.Value;
            objDB.LateInTime = txtFRILateInTime.Value;
            //objDB.BreakEarlyStartTime = txtFRIBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtFRIBreakStartTime.Value;
            objDB.HalfDayStart = txtFRIBreakStartTime.Value;
            objDB.BreakEndTime = txtFRIBreakEndTime.Value;
            //objDB.BreakLateTime = txtFRIBreakLateTime.Value;

            objDB.EarlyOut = txtFRIEarlyOut.Value;
            objDB.EndTime = txtFRIEndTime.Value;

            objDB.isNightShift = chkFRINightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Friday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkFriday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Saturday
            objDB.isOffDayOT = chkSaturdayOT.Checked;
            objDB.IsFlexible = chkSaturdayIsSpecial.Checked;
            objDB.StartTime = txtSATStartTime.Value;
            objDB.LateInTime = txtSATLateInTime.Value;
            objDB.TimeInExemption = txtSatTimeInExemption.Value;
            //objDB.BreakEarlyStartTime = txtSATBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtSATBreakStartTime.Value;
            objDB.HalfDayStart = txtSATBreakStartTime.Value;
            objDB.BreakEndTime = txtSATBreakEndTime.Value;
            //objDB.BreakLateTime = txtSATBreakLateTime.Value;
            objDB.EarlyOut = txtSATEarlyOut.Value;
            objDB.EndTime = txtSATEndTime.Value;
            objDB.isNightShift = chkSATNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Saturday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSaturday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Sunday
            objDB.isOffDayOT = chkSundayOT.Checked;
            objDB.IsFlexible = chkSundayIsSpecial.Checked;
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.LateInTime = txtSUNLateInTime.Value;
            objDB.TimeInExemption = txtSunTimeInExemption.Value;
            //objDB.BreakEarlyStartTime = txtSUNBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtSUNBreakStartTime.Value;
            objDB.HalfDayStart = txtSUNBreakStartTime.Value;
            objDB.BreakEndTime = txtSUNBreakEndTime.Value;
            //objDB.BreakLateTime = txtSUNBreakLateTime.Value;
            objDB.EarlyOut = txtSUNEarlyOut.Value;
            objDB.EndTime = txtSUNEndTime.Value;
            objDB.isNightShift = chkSUNNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Sunday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSunday.Checked;

            objDB.AddWorkingShiftsDays2();

            #endregion

        }

        private string CheckWorkingShiftDays()
        {

            #region Monday
            objDB.IsFlexible = chkMondayIsSpecial.Checked;
            objDB.isOverTime = chkMondayOT.Checked;
            objDB.StartTime = txtMONStartTime.Value;
            objDB.TimeInExemption = txtMonTimeInExemption.Value;
            objDB.EndTime = txtMONEndTime.Value;
            objDB.isNightShift = chkMonNightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Monday";
            objDB.isExpectedZero = !chkMonday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Tuesday

            objDB.IsFlexible = chkTuesdayIsSpecial.Checked;
            objDB.isOverTime = chkTuesdayOT.Checked;
            objDB.StartTime = txtTUEStartTime.Value;
            objDB.TimeInExemption = txtTueTimeInExemption.Value;
            objDB.EndTime = txtTUEEndTime.Value;
            objDB.isNightShift = chkTUENightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Tuesday";
            objDB.isExpectedZero = !chkTuesday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Wednesday
            objDB.IsFlexible = chkWednesdayIsSpecial.Checked;
            objDB.isOverTime = chkWednesdayOT.Checked;
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.TimeInExemption = txtWedTimeInExemption.Value;
            objDB.EndTime = txtWEDEndTime.Value;
            objDB.isNightShift = chkWEDNightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Wednesday";
            objDB.isExpectedZero = !chkWednesdat.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Thursday
            objDB.IsFlexible = chkThursdayIsSpecial.Checked;
            objDB.isOverTime = chkThursdayOT.Checked;
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.TimeInExemption = txtThursTimeInExemption.Value;
            objDB.EndTime = txtTHUEndTime.Value;
            objDB.isNightShift = chkTHUNightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Thursday";
            objDB.isExpectedZero = !chkThursday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Friday
            objDB.IsFlexible = chkFridayIsSpecial.Checked;
            objDB.isOverTime = chkFridayOT.Checked;
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.TimeInExemption = txtFriTimeInExemption.Value;
            objDB.EndTime = txtFRIEndTime.Value;
            objDB.isNightShift = chkFRINightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Friday";
            objDB.isExpectedZero = !chkFriday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Saturday
            objDB.IsFlexible = chkSaturdayIsSpecial.Checked;
            objDB.isOverTime = chkSaturdayOT.Checked;
            objDB.StartTime = txtSATStartTime.Value;
            objDB.TimeInExemption = txtSatTimeInExemption.Value;
            objDB.EndTime = txtSATEndTime.Value;
            objDB.isNightShift = chkSATNightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue); ;
            objDB.ShiftDayName = "Saturday";
            objDB.isExpectedZero = !chkSaturday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Sunday
            objDB.IsFlexible = chkSundayIsSpecial.Checked;
            objDB.isOverTime = chkSundayOT.Checked;
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.TimeInExemption = txtSunTimeInExemption.Value;
            objDB.EndTime = txtSUNEndTime.Value;
            objDB.isNightShift = chkSUNNightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Sunday";
            objDB.isExpectedZero = !chkSunday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            return "Existing Shift";
        }

        private void clearFields()
        {
            txtNotes.Value = "";
            txtDate.Value = "";
            //ddlEmployee.SelectedValue = "0";
            ddlWorkingShift.SelectedValue = "0";
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();

            try
            {
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "WorkingShiftChange", "WorkingShiftChangeID", HttpContext.Current.Items["ChangeWorkingShiftID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "ESS", "Working Shift of ID\"" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/view/ChangeWorkingShift", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/ChangeWorkingShift/edit-ChangeWorkingShift-" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString(), "AddOn \"" + ChangeWorkingShiftID + "\"", "WorkingShiftChange", "Announcement", Convert.ToInt32(HttpContext.Current.Items["ChangeWorkingShiftID"].ToString()));

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();

            try
            {
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "WorkingShiftChange", "ChangeWorkingShiftID", HttpContext.Current.Items["ChangeWorkingShiftID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "ESS", "Working Shift of ID\"" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString() + "\" Status Changed", "WorkingShiftChange", ChangeWorkingShiftID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();

            try
            {
                objDB.ChangeWorkingShiftID = ChangeWorkingShiftID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteAddOnsExpenseByID();
                Common.addlog("Working Shift Rejected", "ESS", "Working Shift of ID\"" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString() + "\" Working Shift Rejected", "WorkingShiftChange", ChangeWorkingShiftID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Working Shift Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesAcceptHimByCompanyID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();                
                //ddlEmployee.Items.Insert(0, new ListItem("-- Select Employee --", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        
        private void BindWorkingShift()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlWorkingShift.DataSource = objDB.GetApproveAllWorkingShiftsByCompanyID(ref errorMsg);
                ddlWorkingShift.DataTextField = "ShiftName";
                ddlWorkingShift.DataValueField = "ShiftID";
                ddlWorkingShift.DataBind();
                ddlWorkingShift.Items.Insert(0, new ListItem("--- Change Shift  ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();

            try
            {
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Notes = txtNotes.Value;
                objDB.ShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                if (HttpContext.Current.Items["ChangeWorkingShiftID"] != null)
                {
                    objDB.EffectiveDate = Convert.ToDateTime(txtDate.Value);
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.ChangeWorkingShiftID = ChangeWorkingShiftID;
                    res = objDB.UpdateWorkingShiftChanged();
                }
                else
                {
                    objDB.EffectiveDate = DateTime.Now;
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddApprovedWorkingShiftChanged();
                    clearFields();
                }

                if (res == "Succesfully Updated" || res == "Succesfully Inserted")
                {
                    if (res == "Succesfully Inserted") { Common.addlog("Add", "ESS", "Working Shift is Added", "WorkingShiftChange"); }
                    if (res == "Succesfully Updated") { Common.addlog("Update", "ESS", "Working Shift is Updated", "WorkingShiftChange", objDB.ChangeWorkingShiftID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getShiftsByEmployeeID(int EmpID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.EmployeeID = EmpID;
                dt = objDB.GetWorkingShiftByEmployeeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        PrevParentShiftID = Convert.ToInt32(dt.Rows[0]["ParentID"].ToString());
                        PrevShiftID = Convert.ToInt32(dt.Rows[0]["ShiftID"].ToString());

                        if (PrevParentShiftID == 0)
                        {
                            PrevParentShiftID = PrevShiftID;
                        }

                        ddlWorkingShift.SelectedValue = PrevParentShiftID.ToString();

                        shiftDaysDiv.Visible = true;
                        getShiftsByShiftID(PrevShiftID);
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getShiftsByShiftID(int ShiftID)
        {
            try
            {
                shiftDaysDiv.Visible = true;
                DataTable dt, dtday = new DataTable();
                objDB.WorkingShiftID = ShiftID;
                dt = objDB.GetWorkingShiftByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtHours.Value = dt.Rows[0]["Hours"].ToString();
                        chkMonday.Checked = getCheckBoxValue(dt.Rows[0]["Monday"].ToString());
                        chkTuesday.Checked = getCheckBoxValue(dt.Rows[0]["Tuesday"].ToString());
                        chkWednesdat.Checked = getCheckBoxValue(dt.Rows[0]["Wednesday"].ToString());
                        chkThursday.Checked = getCheckBoxValue(dt.Rows[0]["Thursday"].ToString());
                        chkFriday.Checked = getCheckBoxValue(dt.Rows[0]["Friday"].ToString());
                        chkSaturday.Checked = getCheckBoxValue(dt.Rows[0]["Saturday"].ToString());
                        chkSunday.Checked = getCheckBoxValue(dt.Rows[0]["Sunday"].ToString());

                        objDB.DocID = ShiftID;
                        objDB.DocType = "Shifts";
                    }
                }
                dtday = objDB.GetWorkingShiftDaysByShiftID(ref errorMsg);
                if (dtday != null && dtday.Rows.Count > 0)
                {
                    allowHolidayWorking.Checked = dtday.Rows[0]["IsSpecial"].ToString() == "True" ? true : false;
                    for (int i = 0; i < dtday.Rows.Count; i++)
                    {
                        switch (dtday.Rows[i]["DayName"].ToString())
                        {
                            case "Monday":
                                {
                                    chkMondayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkMonNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtMONStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtMONLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtMONBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtMONBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtMONHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtMONEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtMONEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkMondayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }

                            case "Tuesday":
                                {
                                    chkTuesdayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkTUENightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtTUEStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtTUELateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtTUEBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtTUEBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtTUEHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtTUEEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtTUEEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkTuesdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                            case "Wednesday":
                                {
                                    chkWednesdayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkWEDNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtWEDStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtWEDLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtWEDBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtWEDBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtWEDHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtWEDEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtWEDEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkWednesdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                            case "Thursday":
                                {
                                    chkThursdayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkTHUNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtTHUStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtTHULateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtTHUBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtTHUBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtTHUHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtTHUEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtTHUEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkThursdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                            case "Friday":
                                {
                                    chkFridayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkFRINightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtFRIStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtFRILateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtFRIBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtFRIBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtFRIHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtFRIEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtFRIEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkFridayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                            case "Saturday":
                                {
                                    chkSaturdayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkSATNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtSATStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtSATLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtSATBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtSATBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtSATHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtSATEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtSATEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkSaturdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                            case "Sunday":
                                {
                                    chkSundayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkSUNNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtSUNStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtSUNLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtSUNBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtSUNBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtSUNHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtSUNEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtSUNEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkSundayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private bool getCheckBoxValue(string val)
        {
            return val.ToLower() == "true";
        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();

            if (ddlEmployee.SelectedValue != "" || ddlEmployee.SelectedValue != null)
            {
                getShiftsByEmployeeID(Convert.ToInt32(ddlEmployee.SelectedValue));
            }
        }
        
        protected void ddlWorkingShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();

            getShiftsByShiftID(Convert.ToInt32(ddlWorkingShift.SelectedValue));
        }

        protected void btnCopytoAll_Click(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();

            #region Tuesday
            if (chkTuesday.Checked)
            {
                txtTUEStartTime.Value = txtMONStartTime.Value;
                txtTUELateInTime.Value = txtMONLateInTime.Value;
                txtTueTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtTUEBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtTUEBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtTUEBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtTUEBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtTUEHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtTUEEarlyOut.Value = txtMONEarlyOut.Value;
                txtTUEEndTime.Value = txtMONEndTime.Value;
                chkTUENightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Wednesday
            if (chkWednesdat.Checked)
            {
                txtWEDStartTime.Value = txtMONStartTime.Value;
                txtWEDLateInTime.Value = txtMONLateInTime.Value;
                txtWedTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtWEDBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtWEDBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtWEDBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtWEDBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtWEDHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtWEDEarlyOut.Value = txtMONEarlyOut.Value;
                txtWEDEndTime.Value = txtMONEndTime.Value;
                chkWEDNightShift.Checked = chkMonNightShift.Checked;

            }
            #endregion
            #region Thursday
            if (chkThursday.Checked)
            {
                txtTHUStartTime.Value = txtMONStartTime.Value;
                txtTHULateInTime.Value = txtMONLateInTime.Value;
                txtThursTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtTHUBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtTHUBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtTHUBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtTHUBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtTHUHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtTHUEarlyOut.Value = txtMONEarlyOut.Value;
                txtTHUEndTime.Value = txtMONEndTime.Value;
                chkTHUNightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Friday
            if (chkFriday.Checked)
            {
                txtFRIStartTime.Value = txtMONStartTime.Value;
                txtFRILateInTime.Value = txtMONLateInTime.Value;
                txtFriTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtFRIBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtFRIBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtFRIBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtFRIBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtFRIHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtFRIEarlyOut.Value = txtMONEarlyOut.Value;
                txtFRIEndTime.Value = txtMONEndTime.Value;
                chkFRINightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Saturday
            if (chkSaturday.Checked)
            {
                txtSATStartTime.Value = txtMONStartTime.Value;
                txtSATLateInTime.Value = txtMONLateInTime.Value;
                txtSatTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtSATBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtSATBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtSATBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtSATBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtSATHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtSATEarlyOut.Value = txtMONEarlyOut.Value;
                txtSATEndTime.Value = txtMONEndTime.Value;
                chkSATNightShift.Checked = chkSunday.Checked ? chkMonNightShift.Checked : false;
            }
            #endregion
            #region Sunday
            if (chkSunday.Checked)
            {
                txtSUNStartTime.Value = txtMONStartTime.Value;
                txtSUNLateInTime.Value = txtMONLateInTime.Value;
                txtSunTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtSUNBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtSUNBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtSUNBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtSUNBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtSUNHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtSUNEarlyOut.Value = txtMONEarlyOut.Value;
                txtSUNEndTime.Value = txtMONEndTime.Value;
                chkSUNNightShift.Checked = chkSunday.Checked ? chkMonNightShift.Checked : false;
            }
            #endregion
        }
    }
}