﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.Manage
{
    public partial class LoanAndAdvance : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int DirectReportEmployeeID
        {
            get
            {
                if (ViewState["DirectReportEmployeeID"] != null)
                {
                    return (int)ViewState["DirectReportEmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DirectReportEmployeeID"] = value;
            }
        }
        protected string PersonName
        {
            get
            {
                if (ViewState["PersonName"].ToString() != "")
                {
                    return ViewState["PersonName"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["PersonName"] = value;
            }
        }
        protected int loanID
        {
            get
            {
                if (ViewState["loanID"] != null)
                {
                    return (int)ViewState["loanID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["loanID"] = value;
            }
        }
        protected DataTable Attachments
        {
            get
            {
                if (ViewState["Attachments"] != null)
                {
                    return (DataTable)ViewState["Attachments"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["Attachments"] = value;
            }
        }
        protected string DocumentStatus
        {
            get
            {
                if (ViewState["DocumentStatus"] != null)
                {
                    return (string)ViewState["DocumentStatus"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocumentStatus"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {
                    ViewState["PersonName"] = "";
                    hdnPFAmount.Value = "0";
                    CheckSessions();
                    ViewState["loanID"] = null;
                    ViewState["Attachments"] = null;
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/loansNadvance";
            

                    BindDropDown();
                    GetPFDeatils();
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;



                    divAlertMsg.Visible = false;
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                    DataTable dtEmp = objDB.GetEmployeeByID(ref errorMsg);

                    if (dtEmp != null)
                    {
                        if (dtEmp.Rows.Count > 0 && dtEmp.Rows[0]["DirectReportingTo"].ToString() != "")
                        {
                            DirectReportEmployeeID = Convert.ToInt32(dtEmp.Rows[0]["DirectReportingTo"].ToString());
                        }
                    }


                    if (HttpContext.Current.Items["LoanID"] != null)
                    {
                        loanID = Convert.ToInt32(HttpContext.Current.Items["LoanID"].ToString());
                        CheckAccess();
                        checkDocStatus();

                        getLoanByID(loanID);
                        btnSave.Visible = false;
                        Schedule.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/payment-schedule-" + loanID + "";
                    }
                    else
                    {
                        DocumentStatus = "Saved as Draft";
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void GetPFDeatils()
        {
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString()); 
            DataTable dt = objDB.GetProvinentFundByEmployeeID(ref errorMsg);
            hdnPFAmount.Value = dt.Rows[0]["RemaningAmount"].ToString();
           
            gvPFDetails.DataSource = dt;
            gvPFDetails.DataBind();
        }
        private void BindDropDown()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
               
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getLoanByID(int LoanID)
        {
            try
            {
                DataSet ds = new DataSet();
              
                objDB.LoanID = LoanID;
                ds = objDB.GetEmployeeLoanNAdvanceByID(ref errorMsg);

                DataTable dt = ds.Tables[0];
                DataTable loandocs = ds.Tables[1];
                DataTable transhistory = ds.Tables[2];
                
                RequiredDocs.DataSource = loandocs;
                RequiredDocs.DataBind();
                Attachments = loandocs;
                trans.DataSource = transhistory;
                trans.DataBind();

              

                if (ds != null)
                {
                        employeename.Value= dt.Rows[0]["employeename"].ToString();
                        mainpurpose.Value = dt.Rows[0]["mainoptionname"].ToString(); 
                        subpurpose.Value= dt.Rows[0]["subpurposename"].ToString(); 
                        txtLoanTitle.Value = dt.Rows[0]["Title"].ToString();
                        txtLoanAmount.Value = dt.Rows[0]["Amount"].ToString();
                        Remamount.Value = dt.Rows[0]["Remamount"].ToString();
                        txtGrantDate.Value = Convert.ToDateTime(dt.Rows[0]["RequestDate"]).ToString("dd-MMM-yyyy");
                        txteffective.Value = dt.Rows[0]["EffectiveFrom"].ToString()=="" ? "" : Convert.ToDateTime(dt.Rows[0]["EffectiveFrom"]).ToString("dd-MMM-yyyy");
                        txtNoOfInstallments.Value = dt.Rows[0]["NoOfInstallment"].ToString();
                        remInstallment.Value = dt.Rows[0]["RemInstallment"].ToString();
                        txtNotes.Value = dt.Rows[0]["Details"].ToString();
                        withdrawltype.Value= dt.Rows[0]["LoanType"].ToString();
                        Currency.Value=dt.Rows[0]["currencyname"].ToString();
                        zakatamount.Value = dt.Rows[0]["ZakatAmount"].ToString();
                        zakatdeduct.Checked = dt.Rows[0]["isZakatDeduct"].ToString() == "True" ? true : false;
                        allowdeduction.Checked = dt.Rows[0]["isdeduct"].ToString() == "True" ? true : false;
                        objDB.DocID = LoanID;
                        objDB.DocType = "loans";
                        ltrNotesTable.Text = objDB.GetDocNotes();
                        
                    if(dt.Rows[0]["DocStatus"].ToString() == "Data Submitted for Review")
					{
                        btnSave.Visible = true;
                        btnApprove.Visible = false;
                        btnDisapprove.Visible = false;
                    }
                    else if (dt.Rows[0]["DocStatus"].ToString() == "Reviewed")
                    {
                        btnSave.Visible = true;
                        btnApprove.Visible = true;
                        btnDisapprove.Visible = true;
                    }
                    else if (dt.Rows[0]["DocStatus"].ToString() == "Approved")
                        {
                        btnApprove.Visible = false;
                        btnSave.Visible = false;
                        btnDisapprove.Visible = false;
                     
                        intrst.Disabled = true;
                        //btnNotes.Visible = true;
                    }
                        else
                        {

                        btnApprove.Visible = false;
                        btnSave.Visible = false;
                        btnDisapprove.Visible = false;
                     
                        intrst.Disabled = true;
                            //btnNotes.Visible = false;
                        }
                    
                }
                Common.addlog("View", "Finance", "Loan \"" + txtLoanTitle.Value + "\" Viewed", "Loan", loanID);


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                objDB.LoanTitle = txtLoanTitle.Value;
                objDB.LoanAmount = float.Parse(txtLoanAmount.Value);
                objDB.LoanGrantDate = txtGrantDate.Value;
                objDB.NoOfInstallments = int.Parse(txtNoOfInstallments.Value);
                objDB.DeductionPerSalary = float.Parse(Math.Round(double.Parse(txtLoanAmount.Value) / double.Parse(txtNoOfInstallments.Value), 0).ToString());
                objDB.LoanNotes = txtNotes.Value;

                if (HttpContext.Current.Items["LoanID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.LoanID = loanID;
                    res = objDB.UpdateEmployeeLoan();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    int tempID = 0;
                    res = objDB.AddEmployeeLoan();

                    if (int.TryParse(res, out tempID))
                    {
                        res = "New Loan Added";
                        clearFields();

                    }
                    loanID = tempID;
                }


                if (res == "New Loan Added" || res == "Loan Data Updated")
                {

                    objDB.DocType = "loans";
                    objDB.DocID = loanID;
                    objDB.Notes = txtNotes.Value;
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.AddDocNotes();

                    if (res == "New Loan Added") { Common.addlog("Add", "ESS", "New Loan \"" + objDB.LoanTitle + "\" Added", "Loans"); }
                    if (res == "Loan Data Updated") { Common.addlog("Update", "ESS", "Loan \"" + objDB.LoanTitle + "\" Updated", "Loans", loanID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {

            txtLoanTitle.Value = "";
            txtLoanAmount.Value = "";
            txtGrantDate.Value = "";
            txtNoOfInstallments.Value = "";
            txtNotes.Value = "";
        }
 
        protected void ButtonAttachments_ServerClick(object sender, EventArgs e)
        {
            List<byte[]> BytesList = new List<byte[]>();
            List<string> AttachmentsName = new List<string>();
            // Specifying a file
            // make ABS formatted application
         
            string header = "";
            string footer = "";
            string content = printWithdrawalApplciation();
            AttachmentsName.Add("PF_Withdrawal-" + PersonName + ".pdf");
            BytesList.Add(Common.GeneratePDFFileBytes(header, footer, content, "A4", "Portrait", 30, 30));



			// get attached documents by employee

			if (Attachments.Rows.Count > 0)
			{
                for (int i = 0; i < Attachments.Rows.Count; i++)
                {

                    var filename = Path.GetFileName((Attachments.Rows[i]["FilePath"].ToString()));
                    string destDir = Server.MapPath("~/assets/Attachments/PFWithdrawal/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");

                    destDir = Path.Combine(destDir, filename);
                    AttachmentsName.Add(filename);

                    // Calling the ReadAllBytes() function
                    byte[] readText = File.ReadAllBytes(destDir);
                    BytesList.Add(readText);
                }

            }



            if (BytesList.Count > 0)
            {
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    // Creates the .zip
                    using (ZipArchive archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                    {
                        int Index = 0;

                        foreach (byte[] f in BytesList)
                        {
                            
                            System.IO.Compression.ZipArchiveEntry zipItem = archive.CreateEntry($"{AttachmentsName[Index] }");
                            // add the item bytes to the zip entry by opening the original file and copying the bytes
                            using (System.IO.MemoryStream originalFileMemoryStream = new System.IO.MemoryStream(f))
                            {
                                using (System.IO.Stream entryStream = zipItem.Open())
                                {
                                    originalFileMemoryStream.CopyTo(entryStream);
                                }
                            }
                            Index++;
                        }
                    }
                    
                    sendOutZIP(ms.ToArray(), "Loan&Advance-"+ PersonName+"-" + DateTime.Now.ToString("ddMMyyyyHH:mm:ss") + ".zip");
                }
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = $"File (s) Downloaded Successfully";
            }
            

          
        }
        private void sendOutZIP(byte[] zippedFiles, string filename)
        {
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/x-compressed";
            Response.Charset = string.Empty;
            Response.Cache.SetCacheability(System.Web.HttpCacheability.Public);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(zippedFiles);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
            Response.End();
        }

        private string printWithdrawalApplciation()
        {
            string content = "";
            try
            {

               
                string header = "";
                string footer = "";
                int LoanID = loanID;
                DataTable design = new DataTable();
                DataSet ds = new DataSet();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                objDB.DocType = "PF Withdrawal";

                design = objDB.GetDocumentDesign(ref errorMsg);

                if (design != null)
                {
                    if (design.Rows.Count > 0)
                    {

                        content = design.Rows[0]["DocContent"].ToString();

                    }
                }
                DataTable dt = new DataTable();
                objDB.LoanID = LoanID;
                ds = objDB.GetLoanAndAdvanceDetailsByLoanID(ref errorMsg);
                dt = ds.Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    content = content.Replace("##Grant_Date##", Convert.ToDateTime(dt.Rows[0]["RequestDate"].ToString()).ToString("dd/MM/yyyy"));
                    content = content.Replace("##Loan_Amount##", dt.Rows[0]["Amount"].ToString());
                    content = content.Replace("##No_Of_Installment##", dt.Rows[0]["NoOfInstallment"].ToString());
                    content = content.Replace("##is_Perm_Applied##", dt.Rows[0]["iSPermApplied"].ToString() == "Yes" ? "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />" : "");
                    content = content.Replace("##is_Temp_Applied##", dt.Rows[0]["iSTempApplied"].ToString() == "Yes" ? "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />" : "");
                    content = content.Replace("##EmployeeName##", dt.Rows[0]["Employeename"].ToString());
                    PersonName = dt.Rows[0]["wdid"].ToString();
                    content = content.Replace("##WDID##", dt.Rows[0]["wdid"].ToString());
                    content = content.Replace("##CNIC##", dt.Rows[0]["CNIC"].ToString());
                    content = content.Replace("##Designation##", dt.Rows[0]["DesgTitle"].ToString());
                    content = content.Replace("##DOB##", Convert.ToDateTime(dt.Rows[0]["DOB"].ToString()).ToString("dd/MM/yyyy"));
                    content = content.Replace("##Location##", dt.Rows[0]["Placeofpost"].ToString());


                    content = content.Replace("##HRNAME##", dt.Rows[0]["HRDept"].ToString());
                    content = content.Replace("##HRApprovaldate##", dt.Rows[0]["HRClearedon"].ToString() == "" ? "" : Convert.ToDateTime(dt.Rows[0]["HRClearedon"].ToString()).ToString("dd/MM/yyyy"));

                    content = content.Replace("##FinanceName##", dt.Rows[0]["FinanceDept"].ToString());
                    content = content.Replace("##FinanceApprovaldate##", dt.Rows[0]["FinanceClearedon"].ToString() == "" ? "" : Convert.ToDateTime(dt.Rows[0]["FinanceClearedon"].ToString()).ToString("dd/MM/yyyy"));

                    content = content.Replace("##TrusteesName##", dt.Rows[0]["TrusteesDept"].ToString());
                    content = content.Replace("##TrusteesApprovaldate##", dt.Rows[0]["TrusteesClearedon"].ToString() == "" ? "" : Convert.ToDateTime(dt.Rows[0]["FinanceClearedon"].ToString()).ToString("dd/MM/yyyy"));


                    content = content.Replace("##Zakatyes##", dt.Rows[0]["isZakatDeduct"].ToString() == "True" ? "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />" : "");
                    content = content.Replace("##Zakatno##", dt.Rows[0]["isZakatDeduct"].ToString() == "False" ? "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />" : "");


                    for (int i = 1; i <= Convert.ToInt32(ds.Tables[1].Rows[0][0].ToString()); i++)
                    {
                        if (i.ToString() == dt.Rows[0]["mainoptionid"].ToString())
                        {
                            content = content.Replace("##Option" + i + "##", "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />");
                        }
                        if (i.ToString() == dt.Rows[0]["subpurposeid"].ToString())
                        {

                            content = content.Replace("##SubOption" + i + "##", "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />");
                        }
                        else
                        {
                            content = content.Replace("##Option" + i + "##", "");
                            content = content.Replace("##SubOption" + i + "##", "");
                        }

                    }


                }


                return content;



            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }

            return content;
        }




        protected void ButtonDownload_ServerClick(object sender, EventArgs e)
        {
            try
            {
               
                    string content = "";
                    string header = "";
                    string footer = "";
                    int LoanID = loanID;
                    DataTable design = new DataTable();
                    DataSet ds = new DataSet();
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                    objDB.DocType = "PF Withdrawal";

                    design = objDB.GetDocumentDesign(ref errorMsg);

                    if (design != null)
                    {
                        if (design.Rows.Count > 0)
                        {

                            content = design.Rows[0]["DocContent"].ToString();

                        }
                    }
                    DataTable dt = new DataTable();
                    objDB.LoanID = LoanID;
                    ds = objDB.GetLoanAndAdvanceDetailsByLoanID(ref errorMsg);
                    dt = ds.Tables[0];

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        content = content.Replace("##Grant_Date##", Convert.ToDateTime(dt.Rows[0]["RequestDate"].ToString()).ToString("dd/MM/yyyy"));
                        content = content.Replace("##Loan_Amount##", dt.Rows[0]["Amount"].ToString());
                        content = content.Replace("##No_Of_Installment##", dt.Rows[0]["NoOfInstallment"].ToString());
                        content = content.Replace("##is_Perm_Applied##", dt.Rows[0]["iSPermApplied"].ToString() == "Yes" ? "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />" : "");
                        content = content.Replace("##is_Temp_Applied##", dt.Rows[0]["iSTempApplied"].ToString() == "Yes" ? "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />" : "");
                        content = content.Replace("##EmployeeName##", dt.Rows[0]["Employeename"].ToString());
                        PersonName = dt.Rows[0]["wdid"].ToString();
                        content = content.Replace("##WDID##", dt.Rows[0]["wdid"].ToString());
                        content = content.Replace("##CNIC##", dt.Rows[0]["CNIC"].ToString());
                        content = content.Replace("##Designation##", dt.Rows[0]["DesgTitle"].ToString());
                        content = content.Replace("##DOB##", Convert.ToDateTime(dt.Rows[0]["DOB"].ToString()).ToString("dd/MM/yyyy"));
                        content = content.Replace("##Location##", dt.Rows[0]["Placeofpost"].ToString());


                        content = content.Replace("##HRNAME##", dt.Rows[0]["HRDept"].ToString());
                        content = content.Replace("##HRApprovaldate##", dt.Rows[0]["HRClearedon"].ToString() == "" ? "" : Convert.ToDateTime(dt.Rows[0]["HRClearedon"].ToString()).ToString("dd/MM/yyyy"));

                        content = content.Replace("##FinanceName##", dt.Rows[0]["FinanceDept"].ToString());
                        content = content.Replace("##FinanceApprovaldate##", dt.Rows[0]["FinanceClearedon"].ToString() == "" ? "" : Convert.ToDateTime(dt.Rows[0]["FinanceClearedon"].ToString()).ToString("dd/MM/yyyy"));

                        content = content.Replace("##TrusteesName##", dt.Rows[0]["TrusteesDept"].ToString());
                        content = content.Replace("##TrusteesApprovaldate##", dt.Rows[0]["TrusteesClearedon"].ToString() == "" ? "" : Convert.ToDateTime(dt.Rows[0]["FinanceClearedon"].ToString()).ToString("dd/MM/yyyy"));


                        content = content.Replace("##Zakatyes##", dt.Rows[0]["isZakatDeduct"].ToString() == "True" ? "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />" : "");
                        content = content.Replace("##Zakatno##", dt.Rows[0]["isZakatDeduct"].ToString() == "False" ? "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />" : "");


                        for (int i = 1; i <= Convert.ToInt32(ds.Tables[1].Rows[0][0].ToString()); i++)
                        {
                            if (i.ToString() == dt.Rows[0]["mainoptionid"].ToString())
                            {
                                content = content.Replace("##Option" + i + "##", "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />");
                            }
                            if (i.ToString() == dt.Rows[0]["subpurposeid"].ToString())
                            {

                                content = content.Replace("##SubOption" + i + "##", "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />");
                            }
                            else
                            {
                                content = content.Replace("##Option" + i + "##", "");
                                content = content.Replace("##SubOption" + i + "##", "");
                            }

                        }


                    }


                    Common.generatePDF(header, footer, content, "PF_Withdrawal-" + PersonName + "", "A4", "Portrait", 30, 30);


                

            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }


        }
         protected void Button1_ServerClick(object sender, EventArgs e)
        {
            //try
            //{
            //    CheckSessions();
            //    System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            //    string res = Common.addAccessLevels(btn.ID.ToString(), "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
            //    Common.addlogNew(res, "ESS", "Loan of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/view/loans", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/loans/edit-loan-" + HttpContext.Current.Items["LoanID"].ToString(), "Loan \"" + txtLoanTitle.Value + "\"", "Loans", "loans", Convert.ToInt32(HttpContext.Current.Items["LoanID"].ToString()));

            //    //Common.addlog(res, "ESS", "Loan of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "Loans", loanID);

            //    divAlertMsg.Visible = true;
            //    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            //    pAlertMsg.InnerHtml = res;
            //    btnSubForReview.Visible = false;
            //    btnSave.Visible = false;
            //    btnNotes.Visible = false;
            //    lnkDelete.Visible = false;
            //}
            //catch (Exception ex)
            //{
            //    divAlertMsg.Visible = true;
            //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
            //    pAlertMsg.InnerHtml = ex.Message;
            //}
            try
            {
                CheckSessions();
                string res = "";
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;

                if (btn.ID.ToString() == "btnSubForReview")
                {
                    if (Session["EmployeeID"] != null)
                    {

                        if (DirectReportEmployeeID == Convert.ToInt32(Session["EmployeeID"].ToString()))
                        {
                            objDB.Status = "Submited By Manager";
                            objDB.LoanID = loanID;
                            res = objDB.UpdateLoanStatus();
                        }
                        else
                        {
                            objDB.Status = "Submited By Employee";
                            objDB.LoanID = loanID;
                            res = objDB.UpdateLoanStatus();
                        }
                    }
                    //res = Common.addAccessLevels(btn.ID.ToString(), "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
                    //Common.addlogNew(res, "HR", "OtherExpense of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/other-expense", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/other-expense/edit-other-expense-" + LoanID, "OtherExpense of ID \"" + LoanID + "\"", "Loans", "Loans", LoanID);
                }


                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
            checkDocStatus();

        }
        private void checkDocStatus()
        {
            CheckSessions();
            //if (Session["EmployeeID"].ToString() == ddlEmployee.SelectedValue)
            //{
            //    if (DocumentStatus != "Saved as Draft")
            //    {
            //        btnSubForReview.Visible = false;
            //        btnSave.Visible = false;
            //        lnkDelete.Visible = false;
            //    }
            //}
            if (Session["EmployeeID"].ToString() == DirectReportEmployeeID.ToString())
            {
                if (DocumentStatus == "Submited By Manager")
                {
                    btnRevApprove.Visible = false;
                    btnSave.Visible = false;
                    lnkDelete.Visible = false;
                }
            }
            else
            {
                if (DocumentStatus != "Saved as Draft")
                {
                    btnSubForReview.Visible = false;
                    btnSave.Visible = false;
                    lnkDelete.Visible = false;
                }
            }
        }
        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnReview.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;

                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "Loans";
                objDB.PrimaryColumnnName = "LoanID";
                objDB.PrimaryColumnValue = HttpContext.Current.Items["LoanID"].ToString();
                objDB.DocName = "Loans";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    //lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    //btnApprove.Visible = true;
                    //btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    //btnRevApprove.Visible = true;

                    //btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    //btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "ESS", "Loan of ID \"" + HttpContext.Current.Items["LoanID"].ToString() + "\" deleted", "Loans", loanID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

		//protected void Getsubpurpose_SelectedIndexChanged(object sender, EventArgs e)
		//{

  //          objDB.MainPuroposeID = temppurposeparent.SelectedValue;
		//	if (objDB.MainPuroposeID == "0")
		//	{
  //              objDB.MainPuroposeID = permpurposeparent.SelectedValue;
  //          }
  //          showsubpurpose.Visible = true;
  //          subpurpose.DataSource = objDB.GetSubPurpose(ref errorMsg);
  //          subpurpose.DataTextField = "optionid";
  //          subpurpose.DataValueField = "optionname";
  //          subpurpose.DataBind();
  //          subpurpose.Items.Insert(0, new ListItem("--- Select ---", "0"));

  //      }
    }
}