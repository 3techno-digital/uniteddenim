﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.Manage
{
    public partial class TravelExpense2 : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int OtherExpenseID
        {
            get
            {
                if (ViewState["OtherExpenseID"] != null)
                {
                    return (int)ViewState["OtherExpenseID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["OtherExpenseID"] = value;
            }
        }

        protected int DirectReportEmployeeID
        {
            get
            {
                if (ViewState["DirectReportEmployeeID"] != null)
                {
                    return (int)ViewState["DirectReportEmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DirectReportEmployeeID"] = value;
            }
        }

        protected int InDirectReportEmployeeID
        {
            get
            {
                if (ViewState["InDirectReportEmployeeID"] != null)
                {
                    return (int)ViewState["InDirectReportEmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["InDirectReportEmployeeID"] = value;
            }
        }
        protected string Attachment
        {
            get
            {
                if (ViewState["Attachment"] != null)
                {
                    return (string)ViewState["Attachment"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["Attachment"] = value;
            }
        }
        
        protected string DocumentStatus
        {
            get
            {
                if (ViewState["DocumentStatus"] != null)
                {
                    return (string)ViewState["DocumentStatus"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocumentStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    CheckSessions();

                    btnUpdateDiv.Visible = false;
                    
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/other-expense";
                    BindDropDowns();

                    Attachment = "";
                    divAlertMsg.Visible = false;
                    ViewState["ExpDetailsSrNo"] = null;
                    ViewState["dtExpDetails"] = null;
                    lblAmount.InnerText = "00.00";


                    txtDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");

                    dtExpDetails = new DataTable();
                    dtExpDetails = createExpDetails();
                    BindExpDetailsTable();

                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    txtCode.Value = objDB.GenerateOtherExpenseCode();

                    if (HttpContext.Current.Items["OtherExpenseID"] != null)
                    {
                        OtherExpenseID = Convert.ToInt32(HttpContext.Current.Items["OtherExpenseID"].ToString());
                        getOtherExpensesByID(OtherExpenseID);
                        //CheckAccess();
                        txtTitle.Disabled = true;
                        ddlEmployee.Enabled = false;
                        checkDocStatus();
						

                    }
                    else
                    {
                        ddlEmployee.SelectedValue = Session["EmployeeID"].ToString();
                        DocumentStatus = "Data Submitted for Review";
                        DocStatus.Value = "Data Submitted for Review";
                        isMgr.Value = "false";
                    }
                }

                Page.ClientScript.GetPostBackEventReference(this, string.Empty);
                string ctrlName = Request.Params.Get("__EVENTTARGET");
                string ctrlArgs = Request.Params.Get("__EVENTARGUMENT");

                if (!String.IsNullOrEmpty(ctrlName) && ctrlName == "attachments")
                {
                    if (updLogo != null)
                    {
                        //foreach (HttpPostedFile file in updAttachments.PostedFile)
                        //{

                        HttpPostedFile file = updLogo.PostedFile;
                        Random rand = new Random((int)DateTime.Now.Ticks);
                        int randnum = 0;

                        string fn = "";
                        string exten = "";
                        string destDir = Server.MapPath("~/assets/" + ctrlName + "/OtherExpenses/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
                        randnum = rand.Next(1, 100000);
                        fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

                        if (!Directory.Exists(destDir))
                        {
                            Directory.CreateDirectory(destDir);
                        }
                        string fname = Path.GetFileName(file.FileName);
                        exten = Path.GetExtension(file.FileName);
                        file.SaveAs(destDir + fn + exten);

                        Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/" + ctrlName + "/OtherExpenses/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
                        imgLogo.Src = Attachment;
                        //dr[3] = ddlDocType.SelectedValue;

                        //}
                    }
                }



            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnReview.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;

                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "OtherExpenses";
                objDB.PrimaryColumnnName = "OtherExpenseID";
                objDB.PrimaryColumnValue = HttpContext.Current.Items["OtherExpenseID"].ToString();
                objDB.DocName = "OtherExpenses";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    //lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    //btnApprove.Visible = true;
                    //btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    //btnRevApprove.Visible = true;

                    //btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    //btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getOtherExpensesByID(int OtherExpenseID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.OtherExpenseID = OtherExpenseID;
               
                dt = objDB.GetOtherExpenseByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        DocumentStatus = dt.Rows[0]["DocStatus"].ToString();
                        txtCode.Value = dt.Rows[0]["OtherExpenseCode"].ToString();
                        txtDate.Value = Convert.ToDateTime(dt.Rows[0]["OtherExpenseDate"].ToString()).ToString("dd-MMM-yyyy");
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();

                        objDB.EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"].ToString());

                        DataTable dtEmp = objDB.GetEmployeeByID(ref errorMsg);
                        if (dtEmp != null)
                        {
                            if (dtEmp.Rows.Count > 0 && dtEmp.Rows[0]["DirectReportingPerson"].ToString() != "")
                            {
                                DirectReportEmployeeID = Convert.ToInt32(dtEmp.Rows[0]["DirectReportingPerson"].ToString());
                                InDirectReportEmployeeID = Convert.ToInt32(dtEmp.Rows[0]["InDirectReportingPerson"].ToString());
                            }
                        }

                        txtTitle.Value = dt.Rows[0]["title"].ToString();
                        txtNotes.Value = dt.Rows[0]["Notes"].ToString();
                        lblAmount.InnerText = dt.Rows[0]["NetAmount"].ToString();

                    }
                }

                Common.addlog("View", "HR", "OtherExpense \"" + txtCode.Value + "\" Viewed", "OtherExpenses", objDB.OtherExpenseID);
                getOtherExpDetailsByID(OtherExpenseID);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getOtherExpDetailsByID(int OtherExpenseID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.OtherExpenseID = OtherExpenseID;
                dt = objDB.GetOtherExpenseDetailsByOtherExpenseID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtExpDetails.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["ExpType"],
                            dt.Rows[i]["Description"],
                            dt.Rows[i]["NetAmount"],
                            dt.Rows[i]["FilePath"]
                        });
                        }
                    }
                }
                BindExpDetailsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private bool CheckOtherDetailsItemsCount()
        {
            bool isValid = true;
            dtExpDetails = (DataTable)ViewState["dtExpDetails"] as DataTable;
            if (!(dtExpDetails != null && dtExpDetails.Rows.Count > 0))
            {
                isValid = false;
            }
            return isValid;
        }

        private DataTable dtExpDetails
        {
            get
            {
                if (ViewState["dtExpDetails"] != null)
                {
                    return (DataTable)ViewState["dtExpDetails"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtExpDetails"] = value;
            }
        }
        protected int ExpDetailsSrNo
        {
            get
            {
                if (ViewState["ExpDetailsSrNo"] != null)
                {
                    return (int)ViewState["ExpDetailsSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["ExpDetailsSrNo"] = value;
            }
        }

        private DataTable createExpDetails()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("ExpType");
            dt.Columns.Add("Description");
            dt.Columns.Add("Amount");
            dt.Columns.Add("FilePath");
            dt.AcceptChanges();
            return dt;
        }
        protected void BindExpDetailsTable()
        {
            try
            {
                if (ViewState["dtExpDetails"] == null)
                {
                    dtExpDetails = createExpDetails();
                    ViewState["dtExpDetails"] = dtExpDetails;
                }

                gvExpDetails.DataSource = dtExpDetails;
                gvExpDetails.DataBind();

                if (gvExpDetails.Rows.Count > 0)
                {
                    gvExpDetails.UseAccessibleHeader = true;
                    gvExpDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void lnkRemoveFile_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                if (dtExpDetails.Rows[i][0].ToString() == delSr)
                {
                    double.Parse(dtExpDetails.Rows[i]["Amount"].ToString());
                    lblAmount.InnerText = (double.Parse(lblAmount.InnerText) - double.Parse(dtExpDetails.Rows[i]["Amount"].ToString())).ToString();

                    dtExpDetails.Rows[i].Delete();
                    dtExpDetails.AcceptChanges();
                }
            }
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                dtExpDetails.Rows[i].SetField(0, i + 1);
                dtExpDetails.AcceptChanges();
            }

            ExpDetailsSrNo = dtExpDetails.Rows.Count + 1;
            BindExpDetailsTable();

            if (btnUpdateDiv.Visible)
            {
                hdnExpenseSrNO.Value = "0";

                txtDescriptionDetail.Value = "";
                txtAmountDetail.Value = "";
                ddlExpType.SelectedValue = "0";
                Attachment = "";

                btnUpdateDiv.Visible = false;
                btnAddDiv.Visible = true;

            }


        }

        private void BindDropDowns()
        {
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlEmployee.Items.Clear();
            ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee- --", "0"));

       
            ddlExpType.Items.Clear();
            ddlExpType.DataSource = objDB.GetAllExpenseHeads(ref errorMsg);
            ddlExpType.DataTextField = "Tittle";
            ddlExpType.DataValueField = "Tittle";
            ddlExpType.DataBind();
            ddlExpType.Items.Insert(0, new ListItem("--- Select ---", "0"));


        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void uploadFile() 
        {

            if (updLogo != null)
            {
                Random rand = new Random((int)DateTime.Now.Ticks);
                int randnum = 0;

                string fn = "";
                string exten = "";

                string destDir = Server.MapPath("~/assets/Attachments/OtherExpenses/");
                randnum = rand.Next(1, 100000);
                fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }

                string fname = Path.GetFileName(updLogo.PostedFile.FileName);
                exten = Path.GetExtension(updLogo.PostedFile.FileName);
				if (exten == "")
				{
                    Attachment = "";
                }
				else
				{
                    updLogo.PostedFile.SaveAs(destDir + fn + exten);
                    Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/Attachments/OtherExpenses/" + fn + exten;
                }
         

              
            }



            if (updLogo != null)
                {
                    //foreach (HttpPostedFile file in updAttachments.PostedFile)
                    //{

                    HttpPostedFile file = updLogo.PostedFile;
                    Random rand = new Random((int)DateTime.Now.Ticks);
                    int randnum = 0;

                    string fn = "";
                    string exten = "";
                    string destDir = Server.MapPath("~/assets/Attachments/OtherExpenses/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
                    randnum = rand.Next(1, 100000);
                    fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

                    if (!Directory.Exists(destDir))
                    {
                        Directory.CreateDirectory(destDir);
                    }
                    string fname = Path.GetFileName(file.FileName);
                    exten = Path.GetExtension(file.FileName);
				if (exten == "")
				{
                    Attachment = "";

                }
				else
				{
                    file.SaveAs(destDir + fn + exten);

                    Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/Attachments/OtherExpenses/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;

                }
            }
            


        }

        protected void btnAddExpDetails_ServerClick(object sender, EventArgs e)
        {
            uploadFile();
            DataRow dr = dtExpDetails.NewRow();
            dr[0] = ExpDetailsSrNo.ToString();
            dr[1] = ddlExpType.SelectedValue;
            dr[2] = txtDescriptionDetail.Value;
            dr[3] = txtAmountDetail.Value;
            dr[4] = Attachment;

            lblAmount.InnerText = (double.Parse(lblAmount.InnerText) + double.Parse(txtAmountDetail.Value)).ToString();
            dtExpDetails.Rows.Add(dr);
            dtExpDetails.AcceptChanges();
            ExpDetailsSrNo += 1;
            BindExpDetailsTable();

            txtDescriptionDetail.Value = "";
            txtAmountDetail.Value = "";
            ddlExpType.SelectedValue = "0";
            Attachment = "";
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)  
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.NetAmount = float.Parse(lblAmount.InnerText);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.Code = txtCode.Value;
                objDB.Notes = txtNotes.Value;
                objDB.Date = txtDate.Value;
                objDB.Title = txtTitle.Value;

                int Docid = 0;

                if (CheckOtherDetailsItemsCount())
                {
                    if (HttpContext.Current.Items["OtherExpenseID"] != null)
                    {
                        objDB.ModifiedBy = Session["UserName"].ToString();
                        objDB.OtherExpenseID = OtherExpenseID;
                        res = objDB.UpdateOtherExpense();
                        Docid = OtherExpenseID;
                    }
                    else
                    {
                        objDB.CreatedBy = Session["UserName"].ToString();
                        res = objDB.AddOtherExpense();
                        if (int.TryParse(res, out Docid))
                        {
                            clearFields();
                            res = "New Expense Added";
                        }

                    }
                    OtherExpenseID = Docid;
                    objDB.OtherExpenseID = OtherExpenseID;
                    objDB.DeletedBy = Session["UserName"].ToString();
                    objDB.DelOtherExpenseDetailsByOtherExpnseID();

                    dtExpDetails = (DataTable)ViewState["dtExpDetails"] as DataTable;
                    if (dtExpDetails != null)
                    {
                        if (dtExpDetails.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
                            {
                                objDB.ExpType = dtExpDetails.Rows[i]["ExpType"].ToString();
                                objDB.Description = dtExpDetails.Rows[i]["Description"].ToString();
                                objDB.NetAmount = float.Parse(dtExpDetails.Rows[i]["Amount"].ToString());
                                objDB.FilePath = dtExpDetails.Rows[i]["FilePath"].ToString();
                                objDB.CreatedBy = Session["UserName"].ToString();
                                string rs = objDB.AddOtherExpenseDetailsDetails();
                            }
                        }
                    }
                }
                else
                {
                    res = "Expense detail table can not be empty!";
                }
                
                if (res == "New Expense Added" || res == "OtherExpense Updated Successfully")
                {
                    objDB.DocType = "OtherExpenses";
                    objDB.DocID = Docid;
                    // objDB.Notes = txtNotes.Value;
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.AddDocNotes();

                    if (res == "New Expense Added")
                    {
                        ViewState["ExpDetailsSrNo"] = null;
                        ViewState["dtExpDetails"] = null;

                        dtExpDetails = new DataTable();
                        dtExpDetails = createExpDetails();
                        BindExpDetailsTable();
                        Common.addlog("Add", "HR", "New OtherExpenses \"" + OtherExpenseID + "\" Added", "OtherExpenses");
                        Common.addlogNew("Data Submitted for Review", "OtherExpenses",
                "Add Other Expenses \"" + objDB.Title + "\" Added",
                "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/Other-expense/edit-Other-expense-" + OtherExpenseID.ToString(),
                "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/Other-expense/edit-Other-expense-" + OtherExpenseID.ToString(), "Other Expense", "OtherExpenses", "Direct Reporting");

                    }
                    if (res == "OtherExpense Updated Successfully") { 
                        Common.addlog("Update", "HR", "OtherExpenses \"" + OtherExpenseID + "\" Updated", "OtherExpenses", OtherExpenseID);
                        


                    }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


        private void clearFields()
        {
            txtCode.Value = "";
            txtDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");
            txtNotes.Value = "";
            lblAmount.InnerText = "00.00";
            ddlEmployee.SelectedValue = "0";
            txtCode.Value = objDB.GenerateOtherExpenseCode();
            btnUpdateDiv.Visible = false;
            btnAddDiv.Visible = true;
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                objDB.FindIndirectMgr = Convert.ToInt32(Session["EmployeeID"].ToString());
                if (ddlEmployee.SelectedValue != Session["EmployeeID"].ToString())
                {
                    System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                    string res = Common.addAccessLevels(btn.ID.ToString(), "OtherExpenses", "OtherExpenseID", HttpContext.Current.Items["OtherExpenseID"].ToString(), Session["UserName"].ToString());
                    if (res == "Reviewed Sucessfull")
                    {
                        DocumentStatus = "Reviewed";
                    }

                    //Common.addlogNew(res, "ESS", "OtherExpense of ID\"" + HttpContext.Current.Items["OtherExpenseID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/ess/view/other-expense", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/ess/manage/other-expense/edit-other-expense-" + OtherExpenseID, "OtherExpense of ID \"" + OtherExpenseID + "\"", "OtherExpenses", "OtherExpenses", OtherExpenseID);
                    int ess_id = Convert.ToInt32(ddlEmployee.SelectedValue);
                    Common.addlogNew(res, "OtherExpenses",
                    "Other Expenses \"" + objDB.Title + "\" Status",
                    "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/Other-expense/edit-Other-expense-" + OtherExpenseID.ToString(),
                    "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/Other-expense/edit-Other-expense-" + OtherExpenseID.ToString(), "Other Expense", "OtherExpenses", "ESS", ess_id);



                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }



                else if (objDB.GetIndirectReportedEmployeeByEmployeeID(ref errorMsg).Rows[0][0].ToString() == Session["EmployeeID"].ToString())
                {
                    // self approval

                    // only if direct mgr belongs to US with limited user access and current login is indirect mgr 
                    if (objDB.CheckIFDirectMgrBelongsToUnitedState(ref errorMsg).Rows[0][0].ToString() == "1")
                    {
                        System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                        string res = Common.addAccessLevels(btn.ID.ToString(), "OtherExpenses", "OtherExpenseID", HttpContext.Current.Items["OtherExpenseID"].ToString(), Session["UserName"].ToString());
                        if (res == "Reviewed Sucessfull")
                        {
                            DocumentStatus = "Reviewed";
                        }

                        //Common.addlogNew(res, "ESS", "OtherExpense of ID\"" + HttpContext.Current.Items["OtherExpenseID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/ess/view/other-expense", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/ess/manage/other-expense/edit-other-expense-" + OtherExpenseID, "OtherExpense of ID \"" + OtherExpenseID + "\"", "OtherExpenses", "OtherExpenses", OtherExpenseID);
                        int ess_id = Convert.ToInt32(ddlEmployee.SelectedValue);
                        Common.addlogNew(res, "OtherExpenses",
                        "Other Expenses \"" + objDB.Title + "\" Status",
                        "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/Other-expense/edit-Other-expense-" + OtherExpenseID.ToString(),
                        "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/Other-expense/edit-Other-expense-" + OtherExpenseID.ToString(), "Other Expense", "OtherExpenses", "ESS", ess_id);



                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                        pAlertMsg.InnerHtml = res;
                    }

                    else
                    {
                        // self approval not allowed

                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "Self review not allowed";
                    }

                }
                else
                {
                    // self approval not allowed

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Self review not allowed";

                }

              
             
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            //CheckAccess();
            checkDocStatus();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "OtherExpenses", "OtherExpenseID", HttpContext.Current.Items["OtherExpenseID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "OtherExpenses of ID\"" + HttpContext.Current.Items["OtherExpenseID"].ToString() + "\" Status Changed", "OtherExpenses", OtherExpenseID);

               

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                objDB.OtherExpenseID = OtherExpenseID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteOtherExpense();
                Common.addlog("OtherExpense Rejected", "HR", "OtherExpense of ID\"" + HttpContext.Current.Items["OtherExpenseID"].ToString() + "\" OtherExpense Rejected", "OtherExpenses", OtherExpenseID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "OtherExpense Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void checkDocStatus()
        {
            CheckSessions();
            DocStatus.Value = DocumentStatus;
            if (Session["EmployeeID"].ToString() == ddlEmployee.SelectedValue)
            {
                objDB.FindIndirectMgr = Convert.ToInt32(ddlEmployee.SelectedValue);
                if (objDB.GetIndirectReportedEmployeeByEmployeeID(ref errorMsg).Rows[0][0].ToString() == ddlEmployee.SelectedValue.ToString())
				{
                    isMgr.Value = "false";
                    btnAddExpDetails.Visible = true;
                    btnReview.Visible = true;
                    btnSave.Visible = true;
                    lnkReject.Visible = true;
                }
                    
                else if (DocumentStatus == "Data Submitted for Review")
                {
                    isMgr.Value = "false";
                    btnReview.Visible = false;
                    btnSave.Visible = true;
                    lnkReject.Visible = false;
                   
                }
                else
                {
                    isMgr.Value = "false";
                    btnAddExpDetails.Visible = false;
                    btnReview.Visible = false;
                    btnSave.Visible = false;
                    lnkReject.Visible = false;
                }
               
            }
            else 
            {
                isMgr.Value = "true";
                if (DocumentStatus == "Data Submitted for Review")
                {
                    btnAddExpDetails.Visible = false;
                    btnReview.Visible = true;
                    btnSave.Visible = false;
                    lnkReject.Visible = true;
                }
                else
                {
                    btnAddExpDetails.Visible = false;
                    btnReview.Visible = false;
                    btnSave.Visible = false;
                    lnkReject.Visible = false;
                }

            }


        }

        protected void lnk_Command(object sender, CommandEventArgs e)
        {

        }

        protected void lnkEdit_Command(object sender, CommandEventArgs e)
        {
            
            //dt.Columns.Add("SrNo");
            //dt.Columns.Add("ExpType");
            //dt.Columns.Add("Description");
            //dt.Columns.Add("Amount");
            //dt.Columns.Add("FilePath");
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                if (dtExpDetails.Rows[i][0].ToString() == delSr)
                {
                    ddlExpType.SelectedValue = dtExpDetails.Rows[i]["ExpType"].ToString();
                    txtDescriptionDetail.Value= dtExpDetails.Rows[i]["Description"].ToString();
                    txtAmountDetail.Value= dtExpDetails.Rows[i]["Amount"].ToString();
                    imgLogo.Src = dtExpDetails.Rows[i]["FilePath"].ToString();
                    hdnExpenseSrNO.Value = delSr;
                    btnUpdateDiv.Visible = true;
                    btnAddDiv.Visible = false;
                }
            }
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                dtExpDetails.Rows[i].SetField(0, i + 1);
                dtExpDetails.AcceptChanges();
            }

            ExpDetailsSrNo = dtExpDetails.Rows.Count + 1;
            BindExpDetailsTable();
        }

       
        protected void btnUpdateExpDetails_ServerClick(object sender, EventArgs e)
        {

            uploadFile();
            string delSr = hdnExpenseSrNO.Value;
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                if (dtExpDetails.Rows[i][0].ToString() == delSr)
                {

                    lblAmount.InnerText = (double.Parse(lblAmount.InnerText) + (double.Parse(txtAmountDetail.Value) - double.Parse(dtExpDetails.Rows[i]["Amount"].ToString()))).ToString();
                    dtExpDetails.Rows[i]["ExpType"] = ddlExpType.SelectedValue;
                    dtExpDetails.Rows[i]["Description"] = txtDescriptionDetail.Value ;
                    dtExpDetails.Rows[i]["Amount"] = txtAmountDetail.Value ;
                    dtExpDetails.Rows[i]["FilePath"] = Attachment;
                    dtExpDetails.AcceptChanges();

                    hdnExpenseSrNO.Value = "0";
                    
                    txtDescriptionDetail.Value = "";
                    txtAmountDetail.Value = "";
                    ddlExpType.SelectedValue = "0";
                    Attachment = "";

                    btnUpdateDiv.Visible = false;
                    btnAddDiv.Visible = true;
                    BindExpDetailsTable();

                    break;
                }
            }
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                dtExpDetails.Rows[i].SetField(0, i + 1);
                dtExpDetails.AcceptChanges();
            }
        }

        protected void btnCancelExpDetails_ServerClick1(object sender, EventArgs e)
        {
            btnUpdateDiv.Visible = false;
            btnAddDiv.Visible = true;
        }

        
    }
}