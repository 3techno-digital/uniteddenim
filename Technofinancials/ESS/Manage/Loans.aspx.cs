﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.ESS.Manage
{
    public partial class Loans : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int DirectReportEmployeeID
        {
            get
            {
                if (ViewState["DirectReportEmployeeID"] != null)
                {
                    return (int)ViewState["DirectReportEmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DirectReportEmployeeID"] = value;
            }
        }
        protected int loanID
        {
            get
            {
                if (ViewState["loanID"] != null)
                {
                    return (int)ViewState["loanID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["loanID"] = value;
            }
        }
        protected string DocumentStatus
        {
            get
            {
                if (ViewState["DocumentStatus"] != null)
                {
                    return (string)ViewState["DocumentStatus"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocumentStatus"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {
                    CheckSessions();
                    ViewState["loanID"] = null;
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/loans";


                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;



                    divAlertMsg.Visible = false;
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                    DataTable dtEmp = objDB.GetEmployeeByID(ref errorMsg);

                    if (dtEmp != null)
                    {
                        if (dtEmp.Rows.Count > 0 && dtEmp.Rows[0]["DirectReportingTo"].ToString() != "")
                        {
                            DirectReportEmployeeID = Convert.ToInt32(dtEmp.Rows[0]["DirectReportingTo"].ToString());
                        }
                    }


                    if (HttpContext.Current.Items["LoanID"] != null)
                    {
                        loanID = Convert.ToInt32(HttpContext.Current.Items["LoanID"].ToString());
                        CheckAccess();
                        checkDocStatus();

                        getLoanByID(loanID);

                    }
                    else
                    {
                        DocumentStatus = "Saved as Draft";
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void getLoanByID(int LoanID)
        {
            try
            {

                DataTable dt = new DataTable();
                objDB.LoanID = LoanID;
                dt = objDB.GetEmployeeLoanByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {

                        txtLoanTitle.Value = dt.Rows[0]["LoanTitle"].ToString();
                        txtLoanAmount.Value = dt.Rows[0]["LoanAmount"].ToString();
                        txtGrantDate.Value = Convert.ToDateTime(dt.Rows[0]["GrantDate"]).ToString("dd-MMM-yyyy");
                        txtNoOfInstallments.Value = dt.Rows[0]["NoOfInstallments"].ToString();
                        txtNotes.Value = dt.Rows[0]["LoanNotes"].ToString();

                        objDB.DocID = LoanID;
                        objDB.DocType = "loans";
                        ltrNotesTable.Text = objDB.GetDocNotes();

                        if (dt.Rows[0]["DocStatus"].ToString() == "Saved as Draft")
                        {
                            btnSubForReview.Visible = true;
                            lnkDelete.Visible = true;
                            btnSave.Visible = true;
                            //btnNotes.Visible = true;
                        }
                        else
                        {
                            btnSubForReview.Visible = false;
                            lnkDelete.Visible = false;
                            btnSave.Visible = false;
                            //btnNotes.Visible = false;
                        }
                    }
                }
                Common.addlog("View", "ESS", "Loan \"" + txtLoanTitle.Value + "\" Viewed", "Loan", loanID);


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
                objDB.LoanTitle = txtLoanTitle.Value;
                objDB.LoanAmount = float.Parse(txtLoanAmount.Value);
                objDB.LoanGrantDate = txtGrantDate.Value;
                objDB.NoOfInstallments = int.Parse(txtNoOfInstallments.Value);
                objDB.DeductionPerSalary = float.Parse(Math.Round(double.Parse(txtLoanAmount.Value) / double.Parse(txtNoOfInstallments.Value), 0).ToString());
                objDB.LoanNotes = txtNotes.Value;

                if (HttpContext.Current.Items["LoanID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.LoanID = loanID;
                    res = objDB.UpdateEmployeeLoan();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    int tempID = 0;
                    res = objDB.AddEmployeeLoan();

                    if (int.TryParse(res, out tempID))
                    {
                        res = "New Loan Added";
                        clearFields();

                    }
                    loanID = tempID;
                }


                if (res == "New Loan Added" || res == "Loan Data Updated")
                {

                    objDB.DocType = "loans";
                    objDB.DocID = loanID;
                    objDB.Notes = txtNotes.Value;
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.AddDocNotes();

                    if (res == "New Loan Added") { Common.addlog("Add", "ESS", "New Loan \"" + objDB.LoanTitle + "\" Added", "Loans"); }
                    if (res == "Loan Data Updated") { Common.addlog("Update", "ESS", "Loan \"" + objDB.LoanTitle + "\" Updated", "Loans", loanID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {

            txtLoanTitle.Value = "";
            txtLoanAmount.Value = "";
            txtGrantDate.Value = "";
            txtNoOfInstallments.Value = "";
            txtNotes.Value = "";
        }



        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            //try
            //{
            //    CheckSessions();
            //    System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            //    string res = Common.addAccessLevels(btn.ID.ToString(), "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
            //    Common.addlogNew(res, "ESS", "Loan of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/view/loans", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/loans/edit-loan-" + HttpContext.Current.Items["LoanID"].ToString(), "Loan \"" + txtLoanTitle.Value + "\"", "Loans", "loans", Convert.ToInt32(HttpContext.Current.Items["LoanID"].ToString()));

            //    //Common.addlog(res, "ESS", "Loan of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "Loans", loanID);

            //    divAlertMsg.Visible = true;
            //    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            //    pAlertMsg.InnerHtml = res;
            //    btnSubForReview.Visible = false;
            //    btnSave.Visible = false;
            //    btnNotes.Visible = false;
            //    lnkDelete.Visible = false;
            //}
            //catch (Exception ex)
            //{
            //    divAlertMsg.Visible = true;
            //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
            //    pAlertMsg.InnerHtml = ex.Message;
            //}
            try
            {
                CheckSessions();
                string res = "";
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;

                if (btn.ID.ToString() == "btnSubForReview")
                {
                    if (Session["EmployeeID"] != null)
                    {

                        if (DirectReportEmployeeID == Convert.ToInt32(Session["EmployeeID"].ToString()))
                        {
                            objDB.Status = "Submited By Manager";
                            objDB.LoanID = loanID;
                            res = objDB.UpdateLoanStatus();
                        }
                        else
                        {
                            objDB.Status = "Submited By Employee";
                            objDB.LoanID = loanID;
                            res = objDB.UpdateLoanStatus();
                        }
                    }
                    //res = Common.addAccessLevels(btn.ID.ToString(), "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
                    //Common.addlogNew(res, "HR", "OtherExpense of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/other-expense", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/other-expense/edit-other-expense-" + LoanID, "OtherExpense of ID \"" + LoanID + "\"", "Loans", "Loans", LoanID);
                }


                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
            checkDocStatus();

        }
        private void checkDocStatus()
        {
            CheckSessions();
            //if (Session["EmployeeID"].ToString() == ddlEmployee.SelectedValue)
            //{
            //    if (DocumentStatus != "Saved as Draft")
            //    {
            //        btnSubForReview.Visible = false;
            //        btnSave.Visible = false;
            //        lnkDelete.Visible = false;
            //    }
            //}
            if (Session["EmployeeID"].ToString() == DirectReportEmployeeID.ToString())
            {
                if (DocumentStatus == "Submited By Manager")
                {
                    btnRevApprove.Visible = false;
                    btnSave.Visible = false;
                    lnkDelete.Visible = false;
                }
            }
            else
            {
                if (DocumentStatus != "Saved as Draft")
                {
                    btnSubForReview.Visible = false;
                    btnSave.Visible = false;
                    lnkDelete.Visible = false;
                }
            }
        }
        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnReview.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;

                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "Loans";
                objDB.PrimaryColumnnName = "LoanID";
                objDB.PrimaryColumnValue = HttpContext.Current.Items["LoanID"].ToString();
                objDB.DocName = "Loans";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    //lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    //btnApprove.Visible = true;
                    //btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    //btnRevApprove.Visible = true;

                    //btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    //btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "ESS", "Loan of ID \"" + HttpContext.Current.Items["LoanID"].ToString() + "\" deleted", "Loans", loanID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


    }
}