﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddOnsexpenseOLD.aspx.cs" Inherits="Technofinancials.ESS.Manage.AddOnsexpenseOLD" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/ESS/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>

<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar1" runat="server"></uc:SideBar>

        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="upd1"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">
                <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>
                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <h1 class="m-0 text-dark">Employee Add-ons</h1>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div style="text-align: right;">

                                            <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Review</button>
                                            <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                            <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Send</button>
                                            <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                            <a class="AD_btn" id="btnBack" runat="server">Back</a>

                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>
                        <!-- Modal -->
                        <div class="modal fade M_set" id="notes-modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="m-0 text-dark">Notes</h1>
                                        <div class="add_new">
                                            <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                            <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                        </p>
                                        <p>
                                            <textarea id="Textarea1" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>
                </asp:UpdatePanel>

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Employee<span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="ddlEmployee" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                </h4>

                                                <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged" ></asp:DropDownList>

                                                <input type="text" class="form-control" id="txtEmployee" runat="server" visible="false" disabled="disabled" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Add-ons<span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlAddOn" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                </h4>
                                                <asp:DropDownList ID="ddlAddOn" runat="server" CssClass="form-control select2" data-plugin="select2" OnSelectedIndexChanged="ddlAddOn_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Currency<span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlCurrency" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                </h4>
                                                <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="form-control select2" data-plugin="select2" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Amount<span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtAmount" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                                </h4>
                                                <asp:TextBox ID="txtAmount" runat="server" placeholder="Amount" TextMode="Number" OnTextChanged="txtAmount_TextChanged" AutoPostBack="true" class="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-6" style="display:none">
                                            <div class="form-group">
                                                <h4>Currency Rate<span style="color: red !important;">*</span>
                                                </h4>
                                                <asp:TextBox runat="server" ID="txtRate" CssClass="form-control" Text="0" ReadOnly="true" />

                                            </div>
                                        </div>
                                        <div class="col-sm-6" style="display:none">
                                            <div class="form-group">
                                                <h4>Currency Amount<span style="color: red !important;">*</span>
                                                </h4>
                                                <asp:TextBox ID="txtCurrencyAmount" ReadOnly="true" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <h4>Notes</h4>
                                    </div>
                                    <textarea class="form-control" id="txtNotes" placeholder="Notes" type="text" runat="server" />
                                </div>
                                                      <div class="col-md-12">
                                    <div class="form-group">
                                        <h4>Attachments</h4>
                                    </div>
                                    <div class="file-upload">

                                        <asp:FileUpload ID="FileUpload1" runat="server" text="Browse" />
                                         
                                        
                                        <p>
                                            <asp:Label ID="LblMessage" runat="server" Font-Bold="true"></asp:Label>
                                                      <asp:GridView ID="gvAttachment" runat="server" CssClass="table table-bordered" ClientIDMode="Static" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
                                                <Columns>  
                                                           <asp:TemplateField HeaderText="Attachment" >
                                                        <ItemTemplate>
                                                             
                                                            <a runat="server" target="_blank" class="AD_stock_inn" id="LblMessage1" href='<%# Eval("attachmentpath") %>'>Download <i class="fa fa-download" aria-hidden="true"></i></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
     </Columns>
                                            </asp:GridView>
                                        </p>

                                         <div class="addons">
                                        
                                        <asp:Button ID="BtnFileUpload" runat="server" Text="Upload" OnClick="BtnUpload_Click" />
                                
                                        <asp:Button ID="BtnFileRemove" runat="server" OnClick="BtnFileRemove_Click" Text="Remove"  />
                                                 
                                        </div>
                                    </div>
                                    </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                 <div class="col-md-12">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <%--<div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4>Attachments</h4>
                                </div>

                                <div class="file-upload">
                                    <div class="image-upload-wrap">
                                        <%--<asp:FileUpload ID="updLogo" onchange="readURL(this);" class="file-upload-input" runat="server" ClientIDMode="Static" accept="All/*" />--%>
                                        <%--<input class="file-upload-input" runat="server" type="file" name="uploadFile" id="updLogo" onchange="readURL(this);" accept="All/*" />
                                        <div class="drag-text">
                                            <h3>Drag and drop a file (MaxSize: 2mb)</h3>
                                        </div>
                                    </div>
                                    <div class="file-upload-content">
                                        <img class="file-upload-image" src="/assets/images/3techno-Logo.png" alt="" runat="server" id="imgLogo" />
                                        <div class="image-title-wrap">
                                            <button type="button" onclick="removeUpload()" class="remove-image">
                                                Remove  <span><i class="fa fa-trash" aria-hidden="true"></i></span>

                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                                
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

         <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var ext = input.files[0].name.split('.').pop().toLowerCase();
                    //if ($.inArray(ext, ['png', 'jpg', 'jpeg','docx']) == -1) {
                    //    removeUpload();
                    //    alert('invalid extension!');
                    //}
                    //else {
                        debugger;
                        var reader = new FileReader();
                        var myimg = '';
                        reader.onload = function (e) {
                            $('.image-upload-wrap').hide();
                            $('.file-upload-image').attr('src', e.target.result);
                            $('.file-upload-content').show();
                            $('.image-title').html(input.files[0].name);
                        };
                        reader.readAsDataURL(input.files[0]);
                    //}
                }
                else {
                    removeUpload();
                }
            }
            function removeUpload() {
                $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                $('.file-upload-content').hide();
                $('.image-upload-wrap').show();
            }
            $('.image-upload-wrap').bind('dragover', function () {
                $('.image-upload-wrap').addClass('image-dropping');
            });
            $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
            });
         </script>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
            input#BtnFileUpload, input#BtnFileRemove {
    margin: 0;
    background: #003780;
    padding: 3px 24px;
    color: #fff;
    border: 1px solid #003780;
    border-radius: 4px;
    font-weight: 900;
    transition: all .2s linear;
    cursor: pointer;
}
            .file-upload {
    background-color: #eeeeee;
    width: 100%;
    margin: 0;
    padding: 15px;
    border: 2px dashed #000;
    border-radius: 5px;
}
        </style>
        <!-- Modal -->
    </form>
</body>


</html>
