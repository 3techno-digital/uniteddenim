﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials
{
    public partial class SecurityEmail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
            }
        }
        
        protected void btnResetEmail_ServerClick(object sender, EventArgs e)
        {
            string returnMsg = "";
            string errorMsg = "";
            DBQueries objDB = new DBQueries();
            objDB.Email = txtUserEmail.Value;
            DataTable dt = objDB.GetUserDetailsByEmail(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    returnMsg = "User Exist";

                    if (dt.Rows[0][0].ToString() != "Invalid Email Address" && dt.Rows[0][0].ToString() != "Account Not Approved")
                    {
                        Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                        Session["EmployeeID"] = dt.Rows[0]["EmployeeID"].ToString();
                        Session["UserEmail"] = dt.Rows[0]["Email"].ToString();
                        Session["UserPassword"] = dt.Rows[0]["Password"].ToString();
                        Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                        Session["UserPhoto"] = dt.Rows[0]["Photo"].ToString();
                        Session["CompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                        Session["CompanyName"] = dt.Rows[0]["CompanyName"].ToString();
                        Session["DeptID"] = dt.Rows[0]["DeptID"].ToString();
                        Session["DeptName"] = dt.Rows[0]["DeptName"].ToString();
                        Session["DesgID"] = dt.Rows[0]["DesgID"].ToString();
                        Session["CompanyLogo"] = dt.Rows[0]["CompanyLogo"].ToString();
                    }
                    else
                        returnMsg = dt.Rows[0][0].ToString();
                }
                else
                {
                    returnMsg = errorMsg;
                }
            }
            else
            {
                returnMsg = errorMsg;
            }

            if (returnMsg != "User Exist")
            {
                divAlertMsg.Visible = true;
                pAlertMsg.InnerHtml = returnMsg;
            }
            else
            {
                Response.Redirect("/reset-options");
            }
        }
    }
}