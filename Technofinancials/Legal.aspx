﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Legal.aspx.cs" Inherits="Technofinancials.Legal" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%--<%@ Register Src="~/usercontrols/Footer2.ascx" TagName="Footer" TagPrefix="uc" %>--%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />

</head>
<body class="slider-body">
    <form id="form1" runat="server">
        <section>
          <div class="container-fluid  pl-0 pr-0">
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="header-bg-div">
                        <div class="header-name-div">
                            <h2 class="header-name-slide">Legal</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="terms-of-use-main-div">
                        <br>
                        <h3 class="m-0 pb-0">Patent Information</h3>
                        <p class="">Features and services within 3Techno products may be the subject matter of pending and issued patents assigned to 3Techno Digital Private Limited.</p>

                        <h3 class="m-0 pb-0">Product Information</h3>
                        <p class="">3techno, 3techno logo, Techno Financials, Ecommerce in a Box, Switch, Switchpro, Smart Menu, and Smart Feedback, among others, are products and services of 3Techno Digital Private Limited.</p>

                        <h3 class="m-0 pb-0">Copyright Information</h3>
                        <p class="">3Techno's products, services, content, content marketing and related materials are owned by 3Techno Digital Private Limited, and all rights are reserved by 3Techno Digital Private Limited.</p>

                        <h3 class="m-0 pb-0">Licensing and Support</h3>
                        <p class="">For licensing and product support related inquiries, please refer technofinancials.com or email us at techsupport@3techno.com.</p>

                    </div>
                </div>
            </div>
                                        <div class="clearfix">&nbsp;</div>
          <div class="clearfix">&nbsp;</div>
          <div class="clearfix">&nbsp;</div>
<br />
                <br />
              <br />
        </div>


    <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-5">
                    <p>© 2018-2020 TechnoFinancials. All rights reserved.</p>
                </div>
                <div class="col-sm-4 d-flex justify-content-center p-0">
                    <ul class="<%--list-group list-group-horizontal fotor_ul--%> fotor_ul list-inline">
                        <li><a href="/privacy-policy">Policy</a></li>
                        <li><a href="/security">Security</a></li>
                        <li><a href="/terms-of-use">Terms of Service</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
<%--                    <div class="copyright pull-right">
                        <a href="http://3techno.com" target="_blank" class="tf-footer-name tf-font-times-new-roman">A Product of
                    <img src="/assets/images/footer_logo.png" class="tf-footer-logo">
                            3techno</a>
                    </div>--%>
                </div>
            </div>
        </div>
    </footer>
        </section>

        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
     <!--<div class="container-fluid" style="padding-left:0px;padding-right:0px;">
                <div class="row" style="margin-left:0px;margin-right:0px;">
                    <div class="col-sm-12" style="padding-left:0px;padding-right:0px;">
                        <div class="header-bg-div">
                            <div class="header-name-div">
                                <h2 class="header-name-slide">Legal</h2>
                                <h4  class="header-name-slide" style="font-size:15px;">Last Updated: March 12, 2019</h4>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row"   style="margin-left:0px;margin-right:0px;">
                    <div class="col-sm-12" >
                        <div class="terms-of-use-main-div">
                          <br />
                            <h3>Patent Information</h3>
                            Features and services within 3Techno products may be the subject matter of pending and
                            issued patents assigned to 3Techno Digital Private Limited.
                            <h3>Product Information</h3>
                            3techno, 3techno logo, Techno Financials, Ecommerce in a Box, Switch, Switchpro, Smart
                            Menu, and Smart Feedback, among others, are products and services of 3Techno Digital
                            Private Limited.
                            <h3>Copyright Information</h3>
                            3Techno&#39;s products, services, content, content marketing and related materials are owned
                            by 3Techno Digital Private Limited, and all rights are reserved by 3Techno Digital Private
                            Limited.
                            <h3>Licensing and Support</h3>
                            For licensing and product support related inquiries, please refer technofinancials.com or
                            email us at techsupport@3techno.com.
                        </div>
                    </div>
                </div>


            </div>-->
<%--                    <uc:Footer ID="Footer1" runat="server"></uc:Footer>--%>

    <style>
               ul{
            list-style-type: disc !important;
            padding: 0px 35px !important;
            margin-top:0px !important;
            margin-bottom:10px !important;
        }
          .terms-of-use-main-div p {
    font-size: 14px;
    text-align: justify;
}
        .spanHeading {
            /*font-weight: bold;
            color: #6a6c6f;
            font-size: 21px;*/
            font-weight: bold;
    color: rgb(0, 55, 128);
    font-size: 18px;
    padding: 5px 0px;
        }
        .terms-of-use-main-div{
    margin:auto 60px !important;
    padding-right:20px;
}
        .container-fluid{
            padding-left:0px !important; 
        }
 .main-footer {
    color: #000 !important;
    font-size: 14px;
    padding: 10px 30px;
    box-shadow: inset 0px 0 9px 0px #c3c1c1;
    border-top: 1px solid;
    position: fixed;
    width: 100%;
    bottom: 0;
    background: #fff;
    right: 0;
    transition: all 0.3s;
    z-index: 1;
}
        .main-footer ul {
    list-style-type: none;
    margin-bottom: 0px !important;
}
        .main-footer ul li {
    padding: 0 10px;
}
        .main-footer ul li a {
    font-family: Noto-Regular;
    color: inherit;
    text-decoration: none;
    transition: all 0.3s;
    outline: none !important;
}
    </style>
</body>
</html>
