﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default2.aspx.cs" Inherits="Technofinancials.Default2" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Service Provider</title>
	
</head>
<body>
    <form id="defaultForm" runat="server">
    <div>
		<h1>Welcome to the Service Provider Site</h1>
		<p>
			You are logged in as <%= Context.User.Identity.Name %>.
		</p>
      
                                            <div class="form-group" id="divAlertMsg" visible="false" runat="server">
                                                <div class="alert tf-alert-danger">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                       
        <div id="attributesDiv" runat="server" visible="false">
            <h2>User Attributes</h2>
            <asp:repeater id="attributesRepeater" runat="server">
	            <ItemTemplate>
		            <p><%# DataBinder.Eval(Container.DataItem, "AttributeName") %>: <%# DataBinder.Eval(Container.DataItem, "AttributeValue") %></p>
	            </ItemTemplate>
            </asp:repeater>
        </div>
        <asp:Button ID="logoutButton" runat="server" Text="Logout" OnClick="logoutButton_Click" />
    </div>
        <asp:HiddenField ID="hdnCountryName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCityName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnIP" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnState" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnlatitude" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnlongitude" runat="server" ClientIDMode="Static" />
    </form>

        
</body>
</html>

 <script>
            $(document).ready(function () {
                $.ajax({
                    url: 'https://geoip-db.com/jsonp',
                    jsonpCallback: 'callback',
                    dataType: 'jsonp',
                    success: function (location) {
                        $("#hdnCountryName").val(location.country_name);
                        $("#hdnCityName").val(location.city);
                        $("#hdnIP").val(location.IPv4);
                        $("#hdnState").val(location.state);
                        $("#hdnlatitude").val(location.latitude);
                        $("#hdnlongitude").val(location.longitude);
                        callDBMethod();
                    }
                });
            });

            function callDBMethod() {
                var countryName = $("#hdnCountryName").val();
                var cityName = $("#hdnCityName").val();
                var ip = $("#hdnIP").val();
                var state = $("#hdnState").val();
                var latitude = $("#hdnlatitude").val();
                var longitude = $("#hdnlongitude").val();
                var pageURL = window.location.href;

                $.ajax({
                    type: "POST",
                    url: "<%= ResolveClientUrl("~/business/LogService.asmx/addActivityLog")%>",
                    data: '{countryName: ' + JSON.stringify(countryName) + ', cityName: ' + JSON.stringify(cityName) + ', pageURL:' + JSON.stringify(pageURL) + ', ip:' + JSON.stringify(ip) + ',state:' + JSON.stringify(state) + ',latitude:' + JSON.stringify(latitude) + ',longitude:' + JSON.stringify(longitude) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log("data saved");
                    }
                });
            }
 </script>
