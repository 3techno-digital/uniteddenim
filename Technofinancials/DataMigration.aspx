﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataMigration.aspx.cs" Inherits="Technofinancials.DataMigration" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />
    <style>
        span.select2-container.select2-container--default.select2-container--open ul li:before {
            content: "";
        }

        #login-box {
            box-sizing: border-box;
            width: 350px;
            box-shadow: 1px 1px 3px #DDD;
            margin: auto;
            line-height: 20px;
            display: block;
            border: 1px solid #EAEAEA;
            padding: 35px 20px;
            font-family: 'Open Sans', 'Helvetica', sans-serif;
            font-size: 14px;
            border-radius: 5px;
        }

        #form1 {
            width: 100%;
            height: 100vh;
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            -ms-flex-pack: center;
            -ms-flex-align: center;
            padding: 15px;
            position: relative;
            z-index: 1;
            background-color: #fff;
        }

        .img-pad {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }

        .mb-20 {
            margin-bottom: 20px;
        }

        .mb-15 {
            margin-bottom: 15px;
        }

        .mb-10 {
            margin-bottom: 10px;
        }

        input#userName, input#password {
            width: 100%;
        }

        .privacy_statement_inline_block {
            width: 100% !important;
            font-family: 'Open Sans','Helvetica',sans-serif;
            font-size: 12px;
            color: #999999;
        }

        w100, .row {
            width: auto;
        }

        .descP {
            /* margin-top: 20px; */
            text-align: center;
            font-size: 12px;
            max-width: 285px;
            display: block;
            margin: 10px auto 0;
        }

        .box-header {
            border: none;
            padding: 0px;
        }

        .login_img {
            margin: 20px 0;
            text-align: center;
            display: block;
            margin: 20px auto;
            max-width: 90%;
        }
    </style>
</head>
<body class="slider-body">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScrptMangr" runat="server"></asp:ScriptManager>
        <section>
            <div class="container-fluid" style="padding-left: 12px; padding-right: 12px;">
                <div class="row ">
                    <div class="box" id="login-box">
                        <div id="login-box-header" class="box-header">
                            <div class="row wrap-image text-center mb-20">
                                <span class="img-pad"></span>
                                <img class=" img-responsive login_img logo-img" src="/assets/images/new-tf-logo-newone.png" alt="Alternate Text" align="center" id="login-logo">
                            </div>
                        </div>

                        <asp:UpdatePanel ID="UpdPnl" runat="server">
                            <ContentTemplate>
                                <div class="col-sm-12">
                                    <div class="signup-inner">
                                        <div>
                                            <div class="signup-form">
                                                <div class="form-group">
                                                    <h4>Upload File</h4>

                                                </div>
                                                <asp:Button ID="btnSubmit" Style="width: 100%;" runat="server" Text="Submit" CssClass="btn-sucess btn btn-default btn-login" OnClick="btnSubmit_Click" ValidationGroup="btnValidate"></asp:Button>
                                            </div>

                                        </div>
                                        <div class="col-md-12" style="margin-top: 10px;">
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <!-- Begin DigiCert site seal HTML and JavaScript -->
                                        <div id="DigiCertClickID_BkDeAnmk" data-language="en">
                                        </div>
                                        <script type="text/javascript">
                                            var __dcid = __dcid || []; __dcid.push(["DigiCertClickID_BkDeAnmk", "15", "s", "black", "BkDeAnmk"]); (function () { var cid = document.createElement("script"); cid.async = true; cid.src = "//seal.digicert.com/seals/cascade/seal.min.js"; var s = document.getElementsByTagName("script"); var ls = s[(s.length - 1)]; ls.parentNode.insertBefore(cid, ls.nextSibling); }());
                                        </script>
                                        <!-- End DigiCert site seal HTML and JavaScript -->
                                    </div>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div id="login-box-footer" class="box-footer">
                            <div class="row mb-15 privacy_statement_static privacy_statement_inline_block descP">
                                By clicking on the Sign In button, you understand and agree to our
                                        <a href="/Home/news" class="link">News</a> ,
                                        <a href="/Home/blogs" class="link">Blogs</a> , <a href="/Home/events" class="link">Events</a> and <a href="/Home/features" class="link">Fatures</a>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
        </section>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>
            var slideIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                slideIndex++;
                if (slideIndex > x.length) { slideIndex = 1 }
                x[slideIndex - 1].style.display = "block";
                setTimeout(carousel, 5000); // Change image every 5 seconds
            }
        </script>
    </form>
</body>
</html>
