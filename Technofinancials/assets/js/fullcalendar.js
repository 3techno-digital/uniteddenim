
$('.external-event').draggable({
    revert: true,
    revertDuration: 0
});

var cal = $('#fullcalendar').fullCalendar({
    theme: true,
    height: 650,
    editable: true,
    droppable: true,
    defaultDate: '2018-09-11',
    header: {
        left: 'prev, today, next',
        center: 'title',
        right: 'month, agendaWeek, agendaDay'
    },
   
    buttonText: {
        prev: '',
        next: '',
        today: 'Today',
        month: 'Month',
        agendaWeek: 'Week',
        agendaDay: 'Day'
    },
    events: [
        {
            "category": "warning",
            "title": "Imporatnt Event",
            "start": "2018-09-11",
            "end": "2018-09-11"
        }
    ],
    drop: function (date) {
        if ($('#drop-remove').is(':checked')) {
            $(this).remove();
        }
    },
    dayClick: function (date, jsEvent, view) {
        //modal.find('#event_start').val(date.format());
        window.location = '/al-bario-engineering-pvt-ltd-/people-management/view/candidates-tests?DateFilter=' + date.format();
    }
    //eventClick: function (event, jsEvent, view) {
        
    //    $('#event_title').val(event.title);
    //    $('#event_category').val(event.category);
    //    $('#event_start').val(event.start);
    //    $("#new_event_modal").show();
    //}
});

$.fn.windowCheck = function () {
    var ww = $(window).width();
    if (ww > 768) {
        cal.fullCalendar('changeView', 'month');
    } else if (ww < 768 && ww > 540) {
        cal.fullCalendar('changeView', 'basicWeek');
    } else {
        cal.fullCalendar('changeView', 'basicDay');
    }
};

$(window).on('resize', function (e) {
    $().windowCheck();
});

$('.fc-prev-button').append('<i class="fa fa-chevron-left"><i>');
$('.fc-next-button').append('<i class="fa fa-chevron-right"><i>');

function addEvent() {
    var title = $('#event_title').val();
    var category = $('#event_category').val();
    var start = $('#event_start').val();

    cal.fullCalendar('addEventSource', [
        {
            title: title,
            start: start,
            className: [category]
        }
    ])
    $('#new_event_modal').modal('hide');

}
