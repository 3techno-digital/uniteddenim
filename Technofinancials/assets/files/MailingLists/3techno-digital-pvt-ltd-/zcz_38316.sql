USE [stoven]
GO
/****** Object:  Table [dbo].[CityMaster]    Script Date: 3/25/2019 7:39:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CityMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[StateID] [int] NULL,
	[DocStatus] [nvarchar](max) NULL,
	[PreparedBy] [nvarchar](max) NULL,
	[PreparedDate] [smalldatetime] NULL,
	[ReviewedDate] [smalldatetime] NULL,
	[ApprovedDate] [smalldatetime] NULL,
	[ReviewedBy] [nvarchar](max) NULL,
	[ApprovedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_CityMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CountryMaster]    Script Date: 3/25/2019 7:39:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CountryMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[CountryCode] [varchar](5) NULL,
	[DocStatus] [nvarchar](max) NULL,
	[PreparedBy] [nvarchar](max) NULL,
	[PreparedDate] [smalldatetime] NULL,
	[ReviewedDate] [smalldatetime] NULL,
	[ApprovedDate] [smalldatetime] NULL,
	[ReviewedBy] [nvarchar](max) NULL,
	[ApprovedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_CountryMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StateMaster]    Script Date: 3/25/2019 7:39:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StateMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[CountryID] [int] NULL,
	[DocStatus] [nvarchar](max) NULL,
	[PreparedBy] [nvarchar](max) NULL,
	[PreparedDate] [smalldatetime] NULL,
	[ReviewedDate] [smalldatetime] NULL,
	[ApprovedDate] [smalldatetime] NULL,
	[ReviewedBy] [nvarchar](max) NULL,
	[ApprovedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_StateMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CityMaster]  WITH NOCHECK ADD  CONSTRAINT [FK_CityMaster_StateMaster] FOREIGN KEY([StateID])
REFERENCES [dbo].[StateMaster] ([ID])
GO
ALTER TABLE [dbo].[CityMaster] CHECK CONSTRAINT [FK_CityMaster_StateMaster]
GO
ALTER TABLE [dbo].[StateMaster]  WITH NOCHECK ADD  CONSTRAINT [FK_StateMaster_CountryMaster] FOREIGN KEY([CountryID])
REFERENCES [dbo].[CountryMaster] ([ID])
GO
ALTER TABLE [dbo].[StateMaster] CHECK CONSTRAINT [FK_StateMaster_CountryMaster]
GO
/****** Object:  StoredProcedure [dbo].[usp_TF_GetAllCitiesByStateID]    Script Date: 3/25/2019 7:39:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TF_GetAllCitiesByStateID]
@StateID int
AS
BEGIN
BEGIN TRANSACTION
BEGIN TRY
select * from CityMaster where StateID = @StateID
END TRY
BEGIN CATCH
SELECT
ERROR_MESSAGE() AS ErrorMessage
IF @@TRANCOUNT > 0
ROLLBACK TRANSACTION
END CATCH
IF @@TRANCOUNT > 0
COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[usp_TF_GetAllCountries]    Script Date: 3/25/2019 7:39:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TF_GetAllCountries]
AS
BEGIN
BEGIN TRANSACTION
BEGIN TRY
select * from CountryMaster
END TRY
BEGIN CATCH
SELECT
ERROR_MESSAGE() AS ErrorMessage
IF @@TRANCOUNT > 0
ROLLBACK TRANSACTION
END CATCH
IF @@TRANCOUNT > 0
COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[usp_TF_GetAllStatesByCountryID]    Script Date: 3/25/2019 7:39:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TF_GetAllStatesByCountryID]
@CountryID int
AS
BEGIN
BEGIN TRANSACTION
BEGIN TRY
select * from StateMaster where CountryID = @CountryID
END TRY
BEGIN CATCH
SELECT
ERROR_MESSAGE() AS ErrorMessage
IF @@TRANCOUNT > 0
ROLLBACK TRANSACTION
END CATCH
IF @@TRANCOUNT > 0
COMMIT TRANSACTION
END
GO
