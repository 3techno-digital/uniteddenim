﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials
{
    public partial class Default : System.Web.UI.Page
    {
        
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                //CheckSessions();
                CheckCookies();
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] != null)
            {
                checkAccessLevel();   
            }
        }

        private string checkAccessLevel()
        {
            
            string url = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/";

            if (Session["UserAccess"].ToString() == "Directors")
            {
                Common.addlog("Login", "Admin", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "directors/view/company-profile";
            }
            if (Session["UserAccess"].ToString() == "Procrument")
            {
                Common.addlog("Login", "Procrument", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "procrument-management/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Human Resource")
            {
                Common.addlog("Login", "HR", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "people-management/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Normal User")
            {
                Common.addlog("Login", "ESS", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "employee-self-service/view/dashboard";
            }
            // for US employee
            else if (Session["UserAccess"].ToString() == "Limited")
            {
                Common.addlog("Login", "ESS", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "employee-self-service/view/dashboardUS";
            }
            else if (Session["UserAccess"].ToString() == "Fixed Asset")
            {
                Common.addlog("Login", "FAM", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "fixed-asset/view/dashboard";
            }

            else if (Session["UserAccess"].ToString() == "CRM")
            {
                Common.addlog("Login", "CRM", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "crm/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Finance")
            {
                Common.addlog("Login", "Finance", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "finance/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Finance Admin")
            {
                Common.addlog("Login", "Finance", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "finance/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "CRM General")
            {
                Common.addlog("Login", "CRM General", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url += "crm-general/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "OM")
            {
                Common.addlog("Login", "Operation Management", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url = "http://operations.technofinancials.com/admin/index";
           
            }
            else if (Session["UserAccess"].ToString() == "Super Admin")
            {
                //Common.addlog("Login", "SuperUser", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));

                url = "/technofinancials/super-admin/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Trading Management System")
            {
                Common.addlog("Login", "Trading Management System", Session["UserName"].ToString() + "\" Login", "Users", Convert.ToInt32(Session["UserID"].ToString()));
                url = "technofinancials/trading-management-system/view/dashboard";
            }
            return url;
        }

        private void CheckCookies()
        {
            if (Request.Cookies["TFUserName"] != null)
            {
                if (Request.Cookies["TFUserEmail"] != null)
                {
                    txtUserEmail.Value = Request.Cookies["TFUserEmail"].Value;
                    txtUserPassword.Attributes.Add("value", Convert.ToString(Request.Cookies["TFPassword"].Value));
                    txtUserPassword.Text = Convert.ToString(Request.Cookies["TFPassword"].Value);
                    chkRememberMe.Checked = true;
                }
            }
        }

        private void RemeberCredentials()
        {
            if (chkRememberMe.Checked)
            {
                Response.Cookies["TFUserName"].Expires = DateTime.Now.AddDays(30);
                Response.Cookies["TFPassword"].Expires = DateTime.Now.AddDays(30);
                Response.Cookies["TFUserName"].Value = txtUserEmail.Value.Trim();
                Response.Cookies["TFPassword"].Value = txtUserPassword.Text.Trim();
            }
        }

        protected void btnLogin_ServerClick(object sender, EventArgs e)
        {
            if (chkRememberMe.Checked == true)
            {
                RemeberCredentials();
            }

            objDB.Email = txtUserEmail.Value    ;
            objDB.Password = txtUserPassword.Text;
            DataTable dt = objDB.AuthenticateUser(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "Invalid Email Address" && dt.Rows[0][0].ToString() != "Account Not Approved" && dt.Rows[0][0].ToString() != "Wrong Password!!")
                    {
                        Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                        Session["EmployeeID"] = dt.Rows[0]["EmployeeID"].ToString();
                        Session["UserEmail"] = dt.Rows[0]["Email"].ToString();
                        Session["UserPassword"] = dt.Rows[0]["Password"].ToString();
                        Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                        Session["UserPhoto"] = dt.Rows[0]["Photo"].ToString();
                        Session["CompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                        Session["CompanyName"] = dt.Rows[0]["CompanyName"].ToString();
                        Session["DeptID"] = dt.Rows[0]["DeptID"].ToString();
                        Session["DeptName"] = dt.Rows[0]["DeptName"].ToString();
                        Session["DesgID"] = dt.Rows[0]["DesgID"].ToString();
                        Session["CompanyLogo"] = dt.Rows[0]["CompanyLogo"].ToString();
                        Session["UserAccess"] = dt.Rows[0]["UserAccess"].ToString();
                        Session["OldUserAccess"] = dt.Rows[0]["UserAccess"].ToString();
                        Session["CompanyShortName"] = dt.Rows[0]["CompanyShortName"].ToString();
                        Session["ShiftID"] = dt.Rows[0]["ShiftID"].ToString();
                        Session["LoginCountryName"] = hdnCountryName.Value;
                        Session["LoginCityName"] = hdnCityName.Value;
                        Session["LoginIP"] = hdnIP.Value;
                        Session["LoginState"] = hdnState.Value;
                        Session["Loginlatitude"] = hdnlatitude.Value;
                        Session["Loginlongitude"] = hdnlongitude.Value;
                        Session["Records"] = dt.Rows[0]["Records"].ToString();
                        Session["TeamCount"] = dt.Rows[0]["TeamCount"].ToString();

                        if (dt.Rows[0]["ParentCompanyID"] == null)
                        {
                            Session["ParentCompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                        }
                        else if (dt.Rows[0]["ParentCompanyID"].ToString() == "" || dt.Rows[0]["ParentCompanyID"].ToString() == "0")
                        {
                            Session["ParentCompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                        }
                        else
                        {
                            Session["ParentCompanyID"] = dt.Rows[0]["ParentCompanyID"].ToString();
                        }
                        if (dt.Rows[0]["isNew"].ToString() == "True")
                        {
                            Session["isNewUser"] = "1";
                            Response.Redirect("/update-security-questions");
                        }

                        if (dt.Rows[0]["UserAccess"].ToString() == "Custom" || dt.Rows[0]["Records"].ToString() != "1")
                        {
                            Session["isCustom"] = "True";
                
                            Response.Redirect("/switch-panel");

                        }
                        else
                        {
                            Session["isCustom"] = "False";
                            Response.Redirect(checkAccessLevel());
                        }
                    }
                    else
                    {
                        divAlertMsg.Visible = true;
                        pAlertMsg.InnerHtml = dt.Rows[0][0].ToString();
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    pAlertMsg.InnerHtml = "Could not connect with database";
                }
            }
            else
            {
                divAlertMsg.Visible = true;
                pAlertMsg.InnerHtml = "Could not connect with database";
            }
        }
   
    }
}