﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials
{
    public partial class SwitchPanel : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        public static string userID = "";
        public static string userEmail = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;

                CheckSessions();

                if (Convert.ToInt32(Session["Records"].ToString()) > 1)
                {
                    divCompanies.Visible = true;
                    BindCompaniesDropDown();
                }
                else
                {
                    divCompanies.Visible = false;
                    BindAccessLevelDropDown();

                }

                // BindAccessLevelDropDown();
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }


        private void BindAccessLevelDropDown()
        {
            try
            {
                CheckSessions();

                string textField = "";
                DataTable dt = new DataTable();

                ddlAccessLevel.Items.Clear();
                ddlAccessLevel.DataSource = null;
                ddlAccessLevel.DataBind();

                ddlAccessLevel.Items.Insert(0, new ListItem("--- Select Access Level ---", "0"));

                objDB.UserID = Convert.ToInt32(Session["UserID"].ToString());
                dt = objDB.GetCustomAccessByUserID(ref errorMsg);

                if (dt == null)
                {
                    dt = objDB.GetUserDetailsByID(ref errorMsg);
                }
                else if (dt.Rows.Count == 0)
                {
                    dt = objDB.GetUserDetailsByID(ref errorMsg);
                }

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            switch (dt.Rows[i]["UserAccess"].ToString())
                            {
                                case "Directors":
                                    textField = "Administrator";
                                    break;
                                case "CRM":
                                    textField = "Customer Relationship Management";
                                    break;
                                case "IT":
                                    textField = "Roster Management";
                                    break;
                                case "Fixed Asset":
                                    textField = "Fixed Asset Management";
                                    break;
                                case "Human Resource":
                                    textField = "Human Capital Management";
                                    break;
                                case "Procrument":
                                    textField = "Procurement Management";
                                    break;
                                case "Finance":
                                    textField = "Financial Management";
                                    break;
                                case "OM":
                                    textField = "Operation Management";
                                    break;
                                case "CRM General":
                                    textField = "CRM General";
                                    break;
                                case "Production":
                                    textField = "Production Management";
                                    break;
                                case "Normal User":
                                    textField = "ESS";
                                    break;
                            }

                            ddlAccessLevel.Items.Insert((i + 1), new ListItem(textField, dt.Rows[i]["UserAccess"].ToString()));
                        }
                    }
                }

                ddlAccessLevel.DataBind();

                var list = ddlAccessLevel.Items.Cast<ListItem>().OrderBy(x => x.Text).ToList();

                ddlAccessLevel.DataSource = list;
                ddlAccessLevel.DataTextField = "Text";
                ddlAccessLevel.DataValueField = "Value";
                ddlAccessLevel.DataBind();

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
                
        private void BindCompaniesDropDown()
        {
            try
            {
                CheckSessions();
               
                ddlCompanies.DataSource = null;

                DataTable dt = new DataTable();
                objDB.Email = Session["UserEmail"].ToString();
                dt = objDB.GetUsersByEmail(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ddlCompanies.DataSource = dt;
                        ddlCompanies.DataTextField = "CompanyName";
                        ddlCompanies.DataValueField = "UserID";
                    }
                }

                ddlCompanies.DataBind();
                ddlCompanies.Items.Insert(0, new ListItem("--- Select Company ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlAccessLevel.SelectedValue != "0")
                {
                    Session["UserAccess"] = ddlAccessLevel.SelectedValue;
                    Session["OldUserAccess"] = ddlAccessLevel.SelectedValue;
                    Response.Redirect(checkAccessLevel());
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;

                
            }
        }

        
        private string checkAccessLevel()
        {
            string url = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/";

            if (Session["UserAccess"].ToString() == "Directors")
            {
                url += "directors/view/company-profile";
            }
            if (Session["UserAccess"].ToString() == "Procrument")
            {
                url += "procrument-management/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Human Resource")
            {
                url += "people-management/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Normal User")
            {
                url += "employee-self-service/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "IT")
            {
                url += "IT/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Fixed Asset")
            {
                url += "fixed-asset/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "CRM")
            {
                url += "crm/view/dashboard";
            }

            else if (Session["UserAccess"].ToString() == "Finance")
            {
                url += "finance/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "CRM General")
            {
                url += "crm-general/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Production")
            {
                url += "production/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "OM")
            {
                url = "http://operations.technofinancials.com/admin/index";
            }

            return url;
        }

        protected void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCompanies.SelectedItem.Value  != "0")
                {
                    objDB.UserID = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
                    DataTable dt = objDB.AuthenticateUserByUserID(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                            Session["EmployeeID"] = dt.Rows[0]["EmployeeID"].ToString();
                            Session["UserEmail"] = dt.Rows[0]["Email"].ToString();
                            Session["UserPassword"] = dt.Rows[0]["Password"].ToString();
                            Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                            Session["UserPhoto"] = dt.Rows[0]["Photo"].ToString();
                            Session["CompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            Session["CompanyName"] = dt.Rows[0]["CompanyName"].ToString();
                            Session["DeptID"] = dt.Rows[0]["DeptID"].ToString();
                            Session["DeptName"] = dt.Rows[0]["DeptName"].ToString();
                            Session["DesgID"] = dt.Rows[0]["DesgID"].ToString();
                            Session["CompanyLogo"] = dt.Rows[0]["CompanyLogo"].ToString();
                            Session["UserAccess"] = dt.Rows[0]["UserAccess"].ToString();
                            Session["CompanyShortName"] = dt.Rows[0]["CompanyShortName"].ToString();
                            Session["ShiftID"] = dt.Rows[0]["ShiftID"].ToString();

                            Session["Records"] = dt.Rows[0]["Records"].ToString();
                            if (dt.Rows[0]["ParentCompanyID"] == null)
                            {
                                Session["ParentCompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            }
                            else if (dt.Rows[0]["ParentCompanyID"].ToString() == "")
                            {
                                Session["ParentCompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            }
                            else
                            {
                                Session["ParentCompanyID"] = dt.Rows[0]["ParentCompanyID"].ToString();
                            }

                            BindAccessLevelDropDown();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}