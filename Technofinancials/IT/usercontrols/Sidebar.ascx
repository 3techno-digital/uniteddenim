﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Sidebar.ascx.cs" Inherits="Technofinancials.IT.usercontrols.Sidebar" %>
<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar tf-sidebar">


	<div class="menubar-scroll">
		<div class="menubar-scroll-inner tf-sidebar-menu-items">
			<ul class="app-menu">
				<li class="tf-first-menu">
					<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/dashboard"); %>">
						<img src="/assets/images/dashboard__2.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Dashboard</span>
					</a>
				</li>
				<li>
					<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/ChangeWorkingShift"); %>">
						<i class="fa fa-clock-o img-responsive tf-sidebar-icons" aria-hidden="true"></i>
						<span class="menu-text">Change Shifts</span>
					</a>
				</li>
				<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/auto-attendance"); %>">
					<i class="fa fa-clock-o img-responsive tf-sidebar-icons" aria-hidden="true"></i><span class="menu-text">Auto Attendance
				                                                                                                                                                                     </span></a></li>
				<li>
					<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/shifts"); %>">
						<img src="/assets/images/shift.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Shifts</span>
					</a>
				</li>
				<li class="has-submenu ">
					<a href="javascript:void(0)" class="submenu-toggle">
						<i class="fa fa-handshake-o img-responsive tf-sidebar-icons" aria-hidden="true"></i>
<%--						<img src="/assets/images/shift.png" class="img-responsive tf-sidebar-icons" />--%>
						<span class="menu-text">FnF Settlement</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
					<ul class="submenu">
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/employee-clearance"); %>"><span class="menu-text">Employee Clearance Approvals</span></a></li>

					</ul>
				</li>
				<li class="has-submenu ">
					<a href="javascript:void(0)" class="submenu-toggle">
						<img src="/assets/images/reports-icon-02.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Reports</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
					<ul class="submenu">

						<li class="has-submenu">
							<a href="javascript:void(0)" class="submenu-toggle">
								<%--<img src="/assets/images/hr-04__2.png" class="img-responsive tf-sidebar-icons" />--%>
								<span class="menu-text">Attendance Reports</span>
								<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
							</a>
							<ul class="submenu">
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/attendance-sheet"); %>"><span class="menu-text">Attendance Sheet</span></a></li>
								<%--<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/attendance-summmary"); %>"><span class="menu-text">Attendance Summary</span></a></li>--%>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/attendance-sheet-flagwise"); %>"><span class="menu-text">Attendance Sheet Flagwise Report</span></a></li>

							</ul>
						</li>
						<li class="has-submenu">
							<a href="javascript:void(0)" class="submenu-toggle">
								<%--<img src="/assets/images/hr-04__2.png" class="img-responsive tf-sidebar-icons" />--%>
								<span class="menu-text">Employees Reports</span>
								<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
							</a>
							<ul class="submenu">
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/Employee-Roaster"); %>"><span class="menu-text">Shift Wise Employees Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/Employee-Absent"); %>"><span class="menu-text">Employees Absent Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/Employee-Leave"); %>"><span class="menu-text">Employees Leave Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/New-Joiners"); %>"><span class="menu-text">New Joiners Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/Left-Employee"); %>"><span class="menu-text">Left Employee Report</span></a></li>
								<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/Employee-Adjustment-Report"); %>"><span class="menu-text">Employee Adjustment Report</span></a></li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
			<!-- .app-menu -->
		</div>
		<!-- .menubar-scroll-inner -->
	</div>
	<div class="tf-vno">tf-v2.9 <%Response.Write(DateTime.Now.ToString("MM-dd-yyyy")); %></div>
	<!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->

