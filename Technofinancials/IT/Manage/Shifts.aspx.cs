﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.IT.Manage
{
    public partial class Shifts : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int ShiftID
        {
            get
            {
                if (ViewState["ShiftID"] != null)
                {
                    return (int)ViewState["ShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["ShiftID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                try
                {
                    
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/shifts";

                    divAlertMsg.Visible = false;
                    clearFields();
                    if (HttpContext.Current.Items["ShiftID"] != null)
                    {
                        ShiftID = Convert.ToInt32(HttpContext.Current.Items["ShiftID"].ToString());
                        getShiftsByID(ShiftID);
                        CheckAccess();
                        txtHours.Disabled = true;
                    }
                }
                catch (Exception ex)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = ex.Message;
                }
            }
        }
        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "WorkingShifts";
                objDB.PrimaryColumnnName = "ShiftID";
                objDB.PrimaryColumnValue = ShiftID.ToString();
                objDB.DocName = "Shifts";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
                chkAccessLevel = "Can Edit, Review & Approve";  
                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    //btnApprove.Visible = true;
                    //btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    //btnRevApprove.Visible = true;
                    //btnRejDisApprove.Visible = true;

                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getShiftsByID(int ShiftID)
        {
            try
            {
                DataTable dt, dtday = new DataTable();
                objDB.WorkingShiftID = ShiftID;
                dt = objDB.GetWorkingShiftByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtShiftTitle.Value = dt.Rows[0]["ShiftName"].ToString();
                        txtHours.Value = dt.Rows[0]["Hours"].ToString();
                        //txtStartTime.Value = dt.Rows[0]["StartTime"].ToString();
                        //txtLateIn.Value = dt.Rows[0]["LateInTime"].ToString();
                        //txtHalfDay.Value = dt.Rows[0]["HalfDayStart"].ToString();
                        //txtEarlyOut.Value = dt.Rows[0]["EarlyOut"].ToString();
                        //txtEndTime.Value = dt.Rows[0]["EndTime"].ToString();
                        chkMonday.Checked = getCheckBoxValue(dt.Rows[0]["Monday"].ToString());
                        chkTuesday.Checked = getCheckBoxValue(dt.Rows[0]["Tuesday"].ToString());
                        chkWednesdat.Checked = getCheckBoxValue(dt.Rows[0]["Wednesday"].ToString());
                        chkThursday.Checked = getCheckBoxValue(dt.Rows[0]["Thursday"].ToString());
                        chkFriday.Checked = getCheckBoxValue(dt.Rows[0]["Friday"].ToString());
                        chkSaturday.Checked = getCheckBoxValue(dt.Rows[0]["Saturday"].ToString());
                        chkSunday.Checked = getCheckBoxValue(dt.Rows[0]["Sunday"].ToString());

                        objDB.DocID = ShiftID;
                        objDB.DocType = "Shifts";
                        ltrNotesTable.Text = objDB.GetDocNotes();
                    }
                }
                dtday = objDB.GetWorkingShiftDaysByShiftID(ref errorMsg);
                if (dtday != null && dtday.Rows.Count > 0)
                {
                    for (int i = 0; i < dtday.Rows.Count; i++)
                    {
                        if (dtday.Rows[i]["DayName"].ToString() == "Monday")
                        {
                            //string a = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt"); 
                            chkMonNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtMONStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtMONLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtMONBreakEarlyStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEarlyStartTime"]).ToString("hh:mm tt");
                            txtMONBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtMONBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtMONBreakLateTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakLateTime"]).ToString("hh:mm tt");
                            txtMONHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtMONEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtMONEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Tuesday")
                        {
                            chkTUENightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtTUEStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtTUELateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtTUEBreakEarlyStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEarlyStartTime"]).ToString("hh:mm tt");
                            txtTUEBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtTUEBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtTUEBreakLateTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakLateTime"]).ToString("hh:mm tt");
                            txtTUEHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtTUEEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtTUEEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Wednesday")
                        {
                            chkWEDNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtWEDStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtWEDLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtWEDBreakEarlyStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEarlyStartTime"]).ToString("hh:mm tt");
                            txtWEDBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtWEDBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtWEDBreakLateTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakLateTime"]).ToString("hh:mm tt");
                            txtWEDHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtWEDEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtWEDEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Thursday")
                        {
                            chkTHUNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtTHUStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtTHULateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtTHUBreakEarlyStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEarlyStartTime"]).ToString("hh:mm tt");
                            txtTHUBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtTHUBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtTHUBreakLateTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakLateTime"]).ToString("hh:mm tt");
                            txtTHUHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtTHUEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtTHUEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");

                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Friday")
                        {
                            chkFRINightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtFRIStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtFRILateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtFRIBreakEarlyStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEarlyStartTime"]).ToString("hh:mm tt");
                            txtFRIBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtFRIBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtFRIBreakLateTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakLateTime"]).ToString("hh:mm tt");
                            txtFRIHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtFRIEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtFRIEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Saturday")
                        {
                            chkSATNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtSATStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtSATLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtSATBreakEarlyStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEarlyStartTime"]).ToString("hh:mm tt");
                            txtSATBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtSATBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtSATBreakLateTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakLateTime"]).ToString("hh:mm tt");
                            txtSATHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtSATEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtSATEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Sunday")
                        {
                            chkSUNNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtSUNStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtSUNLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtSUNBreakEarlyStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEarlyStartTime"]).ToString("hh:mm tt");
                            txtSUNBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtSUNBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtSUNBreakLateTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakLateTime"]).ToString("hh:mm tt");
                            txtSUNHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtSUNEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtSUNEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                        }
                    }

                }


                Common.addlog("View", "HR", "Working Shifts \"" + txtShiftTitle.Value + "\" Viewed", "WorkingShifts", objDB.ShiftID);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("IT", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            string Response = "";
            try
            {
                //if (DateTime.Parse(txtStartTime.Value) < DateTime.Parse(txtLateIn.Value) && DateTime.Parse(txtLateIn.Value) < DateTime.Parse(txtHalfDay.Value) && DateTime.Parse(txtHalfDay.Value) < DateTime.Parse(txtEarlyOut.Value) && DateTime.Parse(txtEarlyOut.Value) < DateTime.Parse(txtEndTime.Value))
                //{
                CheckSessions();
                objDB.Hours = txtHours.Value;
                Response = CheckShiftDays();
                if (Response == "Ok")
                {
                    string res = "";

                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.WorkingShiftName = txtShiftTitle.Value;
                    //objDB.StartTime = txtStartTime.Value;
                    //objDB.LateInTime = txtLateIn.Value;
                    //objDB.HalfDayStart = txtHalfDay.Value;
                    //objDB.EarlyOut = txtEarlyOut.Value;
                    //objDB.EndTime = txtEndTime.Value;
                    objDB.Monday = chkMonday.Checked;
                    objDB.Tuesday = chkTuesday.Checked;
                    objDB.Wednesday = chkWednesdat.Checked;
                    objDB.Thursday = chkThursday.Checked;
                    objDB.Friday = chkFriday.Checked;
                    objDB.Saturday = chkSaturday.Checked;
                    objDB.Sunday = chkSunday.Checked;

                    objDB.parentShiftID = 0;
                    if (HttpContext.Current.Items["ShiftID"] != null)
                    {
                        objDB.ModifiedBy = Session["UserName"].ToString();
                        objDB.WorkingShiftID = ShiftID;
                        res = objDB.UpdateShift();
                        objDB.DelWorkingShiftDaysByShiftID();


                        AddShiftDays();
                    }
                    else
                    {
                        objDB.CreatedBy = Session["UserName"].ToString();
                        ShiftID = Convert.ToInt32(objDB.AddShift());
                        res = "New Shift Added";
                        AddShiftDays();
                        clearFields();
                    }


                    objDB.DocType = "Shifts";
                    objDB.DocID = ShiftID;
                    objDB.Notes = txtNotes.Value;
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.AddDocNotes();

                    if (res == "New Shift Added" || res == "Shift Data Updated")
                    {
                        if (res == "New Shift Added") { Common.addlog("Add", "HR", "New Shift \"" + objDB.WorkingShiftName + "\" Added", "WorkingShifts"); }
                        if (res == "Shift Data Updated") { Common.addlog("Update", "HR", "Shift \"" + objDB.WorkingShiftName + "\" Updated", "WorkingShifts", objDB.ShiftID); }

                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                        pAlertMsg.InnerHtml = res;
                    }
                    else
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = res;
                    }

                    //}
                    //else
                    //{
                    //    divAlertMsg.Visible = true;
                    //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    //    pAlertMsg.InnerHtml = "Invalid Entries!!";
                    //}

                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Working hours did not match";
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private string CheckShiftDays()
        {
            string res = "";
            #region Monday
            objDB.StartTime = txtMONStartTime.Value;
            objDB.LateInTime = txtMONLateInTime.Value;
            objDB.BreakEarlyStartTime = txtMONBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtMONBreakStartTime.Value;
            objDB.BreakEndTime = txtMONBreakEndTime.Value;
            objDB.BreakLateTime = txtMONBreakLateTime.Value;
            objDB.HalfDayStart = txtMONHalfDayStart.Value;
            objDB.EarlyOut = txtMONEarlyOut.Value;
            objDB.EndTime = txtMONEndTime.Value;

            objDB.isNightShift = chkMonNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Monday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkMonday.Checked;


            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Tuesday

            objDB.StartTime = txtTUEStartTime.Value;
            objDB.LateInTime = txtTUELateInTime.Value;
            objDB.BreakEarlyStartTime = txtTUEBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtTUEBreakStartTime.Value;
            objDB.BreakEndTime = txtTUEBreakEndTime.Value;
            objDB.BreakLateTime = txtTUEBreakLateTime.Value;
            objDB.HalfDayStart = txtTUEHalfDayStart.Value;
            objDB.EarlyOut = txtTUEEarlyOut.Value;
            objDB.EndTime = txtTUEEndTime.Value;

            objDB.isNightShift = chkTUENightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Tuesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkTuesday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Wednesday
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.LateInTime = txtWEDLateInTime.Value;
            objDB.BreakEarlyStartTime = txtWEDBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtWEDBreakStartTime.Value;
            objDB.BreakEndTime = txtWEDBreakEndTime.Value;
            objDB.BreakLateTime = txtWEDBreakLateTime.Value;
            objDB.HalfDayStart = txtWEDHalfDayStart.Value;
            objDB.EarlyOut = txtWEDEarlyOut.Value;
            objDB.EndTime = txtWEDEndTime.Value;

            objDB.isNightShift = chkWEDNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Wednesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkWednesdat.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Thursday
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.LateInTime = txtTHULateInTime.Value;
            objDB.BreakEarlyStartTime = txtTHUBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtTHUBreakStartTime.Value;
            objDB.BreakEndTime = txtTHUBreakEndTime.Value;
            objDB.BreakLateTime = txtTHUBreakLateTime.Value;
            objDB.HalfDayStart = txtTHUHalfDayStart.Value;
            objDB.EarlyOut = txtTHUEarlyOut.Value;
            objDB.EndTime = txtTHUEndTime.Value;

            objDB.isNightShift = chkTHUNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Thursday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkThursday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Friday
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.LateInTime = txtFRILateInTime.Value;
            objDB.BreakEarlyStartTime = txtFRIBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtFRIBreakStartTime.Value;
            objDB.BreakEndTime = txtFRIBreakEndTime.Value;
            objDB.BreakLateTime = txtFRIBreakLateTime.Value;
            objDB.HalfDayStart = txtFRIHalfDayStart.Value;
            objDB.EarlyOut = txtFRIEarlyOut.Value;
            objDB.EndTime = txtFRIEndTime.Value;

            objDB.isNightShift = chkFRINightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Friday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkFriday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Saturday
            objDB.StartTime = txtSATStartTime.Value;
            objDB.LateInTime = txtSATLateInTime.Value;
            objDB.BreakEarlyStartTime = txtSATBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtSATBreakStartTime.Value;
            objDB.BreakEndTime = txtSATBreakEndTime.Value;
            objDB.BreakLateTime = txtSATBreakLateTime.Value;
            objDB.HalfDayStart = txtSATHalfDayStart.Value;
            objDB.EarlyOut = txtSATEarlyOut.Value;
            objDB.EndTime = txtSATEndTime.Value;

            objDB.isNightShift = chkSATNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Saturday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSaturday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Sunday
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.LateInTime = txtSUNLateInTime.Value;
            objDB.BreakEarlyStartTime = txtSUNBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtSUNBreakStartTime.Value;
            objDB.BreakEndTime = txtSUNBreakEndTime.Value;
            objDB.BreakLateTime = txtSUNBreakLateTime.Value;
            objDB.HalfDayStart = txtSUNHalfDayStart.Value;
            objDB.EarlyOut = txtSUNEarlyOut.Value;
            objDB.EndTime = txtSUNEndTime.Value;

            objDB.isNightShift = chkSUNNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Sunday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSunday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }

            #endregion


            return "Ok";

        }


        private void AddShiftDays()
        {

            #region Monday
            objDB.isOffDayOT = chkMondayOT.Checked;
            objDB.StartTime = txtMONStartTime.Value;
            objDB.LateInTime = txtMONLateInTime.Value;
            objDB.BreakEarlyStartTime = txtMONBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtMONBreakStartTime.Value;
            objDB.BreakEndTime = txtMONBreakEndTime.Value;
            objDB.BreakLateTime = txtMONBreakLateTime.Value;
            objDB.HalfDayStart = txtMONBreakStartTime.Value;
            objDB.EarlyOut = txtMONEarlyOut.Value;
            objDB.EndTime = txtMONEndTime.Value;

            objDB.isNightShift = chkMonNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Monday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkMonday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Tuesday

            objDB.isOffDayOT = chkTuesdayOT.Checked;
            objDB.StartTime = txtTUEStartTime.Value;
            objDB.LateInTime = txtTUELateInTime.Value;
            objDB.BreakEarlyStartTime = txtTUEBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtTUEBreakStartTime.Value;
            objDB.HalfDayStart = txtTUEBreakStartTime.Value;
            objDB.BreakEndTime = txtTUEBreakEndTime.Value;
            objDB.BreakLateTime = txtTUEBreakLateTime.Value;
            objDB.EarlyOut = txtTUEEarlyOut.Value;
            objDB.EndTime = txtTUEEndTime.Value;

            objDB.isNightShift = chkTUENightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Tuesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkTuesday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Wednesday
            objDB.isOffDayOT = chkWednesdayOT.Checked;
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.LateInTime = txtWEDLateInTime.Value;
            objDB.BreakEarlyStartTime = txtWEDBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtWEDBreakStartTime.Value;
            objDB.HalfDayStart = txtWEDBreakStartTime.Value;
            objDB.BreakEndTime = txtWEDBreakEndTime.Value;
            objDB.BreakLateTime = txtWEDBreakLateTime.Value;
            objDB.EarlyOut = txtWEDEarlyOut.Value;
            objDB.EndTime = txtWEDEndTime.Value;

            objDB.isNightShift = chkWEDNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Wednesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkWednesdat.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Thursday
            objDB.isOffDayOT = chkThursdayOT.Checked;
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.LateInTime = txtTHULateInTime.Value;
            objDB.BreakEarlyStartTime = txtTHUBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtTHUBreakStartTime.Value;
            objDB.HalfDayStart = txtTHUBreakStartTime.Value;
            objDB.BreakEndTime = txtTHUBreakEndTime.Value;
            objDB.BreakLateTime = txtTHUBreakLateTime.Value;

            objDB.EarlyOut = txtTHUEarlyOut.Value;
            objDB.EndTime = txtTHUEndTime.Value;

            objDB.isNightShift = chkTHUNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Thursday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkThursday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Friday
            objDB.isOffDayOT = chkFridayOT.Checked;
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.LateInTime = txtFRILateInTime.Value;
            objDB.BreakEarlyStartTime = txtFRIBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtFRIBreakStartTime.Value;
            objDB.HalfDayStart = txtFRIBreakStartTime.Value;
            objDB.BreakEndTime = txtFRIBreakEndTime.Value;
            objDB.BreakLateTime = txtFRIBreakLateTime.Value;

            objDB.EarlyOut = txtFRIEarlyOut.Value;
            objDB.EndTime = txtFRIEndTime.Value;

            objDB.isNightShift = chkFRINightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Friday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkFriday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Saturday
            objDB.isOffDayOT = chkSaturdayOT.Checked;
            objDB.StartTime = txtSATStartTime.Value;
            objDB.LateInTime = txtSATLateInTime.Value;
            objDB.BreakEarlyStartTime = txtSATBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtSATBreakStartTime.Value;
            objDB.HalfDayStart = txtSATBreakStartTime.Value;
            objDB.BreakEndTime = txtSATBreakEndTime.Value;
            objDB.BreakLateTime = txtSATBreakLateTime.Value;
            objDB.EarlyOut = txtSATEarlyOut.Value;
            objDB.EndTime = txtSATEndTime.Value;
            objDB.isNightShift = chkSATNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Saturday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSaturday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Sunday
            objDB.isOffDayOT = chkSundayOT.Checked;
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.LateInTime = txtSUNLateInTime.Value;
            objDB.BreakEarlyStartTime = txtSUNBreakEarlyStartTime.Value;
            objDB.BreakStartTime = txtSUNBreakStartTime.Value;
            objDB.HalfDayStart = txtSUNBreakStartTime.Value;
            objDB.BreakEndTime = txtSUNBreakEndTime.Value;
            objDB.BreakLateTime = txtSUNBreakLateTime.Value;

            objDB.EarlyOut = txtSUNEarlyOut.Value;
            objDB.EndTime = txtSUNEndTime.Value;

            objDB.isNightShift = chkSUNNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Sunday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSunday.Checked;

            objDB.AddWorkingShiftsDays2();

            #endregion

        }

        private void clearFields()
        {
            txtShiftTitle.Value = "";
            txtHours.Value = "";

            txtMONStartTime.Value = "9:00 AM";
            txtMONLateInTime.Value = "9:11 AM";
            txtMONBreakEarlyStartTime.Value = "1:00 PM";
            txtMONBreakStartTime.Value = "1:00 PM";
            txtMONBreakEndTime.Value = "1:30 PM";
            txtMONBreakLateTime.Value = "1:31 PM";
            txtMONHalfDayStart.Value = "2:30 PM";
            txtMONEarlyOut.Value = "5:45 PM";
            txtMONEndTime.Value = "6:00 PM";

            txtTUEStartTime.Value = "9:00 AM";
            txtTUELateInTime.Value = "9:11 AM";
            txtTUEBreakEarlyStartTime.Value = "1:00 PM";
            txtTUEBreakStartTime.Value = "1:00 PM";
            txtTUEBreakEndTime.Value = "1:30 PM";
            txtTUEBreakLateTime.Value = "1:31 PM";
            txtTUEHalfDayStart.Value = "2:30 PM";
            txtTUEEarlyOut.Value = "5:45 PM";
            txtTUEEndTime.Value = "6:00 PM";

            txtWEDStartTime.Value = "9:00 AM";
            txtWEDLateInTime.Value = "9:11 AM";
            txtWEDBreakEarlyStartTime.Value = "1:00 PM";
            txtWEDBreakStartTime.Value = "1:00 PM";
            txtWEDBreakEndTime.Value = "1:30 PM";
            txtWEDBreakLateTime.Value = "1:31 PM";
            txtWEDHalfDayStart.Value = "2:30 PM";
            txtWEDEarlyOut.Value = "5:45 PM";
            txtWEDEndTime.Value = "6:00 PM";

            txtTHUStartTime.Value = "9:00 AM";
            txtTHULateInTime.Value = "9:11 AM";
            txtTHUBreakEarlyStartTime.Value = "1:00 PM";
            txtTHUBreakStartTime.Value = "1:00 PM";
            txtTHUBreakEndTime.Value = "1:30 PM";
            txtTHUBreakLateTime.Value = "1:31 PM";
            txtTHUHalfDayStart.Value = "2:30 PM";
            txtTHUEarlyOut.Value = "5:45 PM";
            txtTHUEndTime.Value = "6:00 PM";

            txtFRIStartTime.Value = "9:00 AM";
            txtFRILateInTime.Value = "9:11 AM";
            txtFRIBreakEarlyStartTime.Value = "1:00 PM";
            txtFRIBreakStartTime.Value = "1:00 PM";
            txtFRIBreakEndTime.Value = "2:30 PM";
            txtFRIBreakLateTime.Value = "2:31 PM";
            txtFRIHalfDayStart.Value = "3:30 PM";
            txtFRIEarlyOut.Value = "6:29 PM";
            txtFRIEndTime.Value = "6:30 PM";

            txtSATStartTime.Value = "9:00 AM";
            txtSATLateInTime.Value = "9:11 AM";
            txtSATBreakEarlyStartTime.Value = "1:00 PM";
            txtSATBreakStartTime.Value = "1:00 PM";
            txtSATBreakEndTime.Value = "1:30 PM";
            txtSATBreakLateTime.Value = "1:31 PM";
            txtSATHalfDayStart.Value = "2:30 PM";
            txtSATEarlyOut.Value = "5:45 PM";
            txtSATEndTime.Value = "6:00 PM";

            txtSUNStartTime.Value = "9:00 AM";
            txtSUNLateInTime.Value = "9:11 AM";
            txtSUNBreakEarlyStartTime.Value = "1:00 PM";
            txtSUNBreakStartTime.Value = "1:00 PM";
            txtSUNBreakEndTime.Value = "1:30 PM";
            txtSUNBreakLateTime.Value = "1:31 PM";
            txtSUNHalfDayStart.Value = "2:30 PM";
            txtSUNEarlyOut.Value = "5:45 PM";
            txtSUNEndTime.Value = "6:00 PM";

            //txtStartTime.Value = "";
            //txtEndTime.Value = "";
            //txtHalfDay.Value = "";
            //txtLateIn.Value = "";
            //txtEndTime.Value = "";
            //txtEarlyOut.Value = "";

        }

        private bool getCheckBoxValue(string val)
        {
            if (val == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "WorkingShifts", "ShiftID", HttpContext.Current.Items["ShiftID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "Working Shift  of ID\"" + HttpContext.Current.Items["ShiftID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/IT/view/shifts", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/IT/manage/shifts/edit-shift-" + HttpContext.Current.Items["ShiftID"].ToString(), "Shift \"" + txtShiftTitle.Value + "\"", "WorkingShifts", "Shifts", Convert.ToInt32(HttpContext.Current.Items["ShiftID"].ToString()));

                //Common.addlog(res, "HR", "Working Shifts of ID\"" + HttpContext.Current.Items["ShiftID"].ToString() + "\" Status Changed", "WorkingShifts", ShiftID);
                getShiftsByID(ShiftID);
                
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                CheckAccess();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }



        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "WorkingShifts", "ShiftID", HttpContext.Current.Items["ShiftID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "Working Shifts of ID \"" + HttpContext.Current.Items["ShiftID"].ToString() + "\" deleted", "WorkingShifts", ShiftID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void btnCopytoAll_Click(object sender, EventArgs e)
        {
            #region Tuesday
            if (chkTuesday.Checked)
            {
                txtTUEStartTime.Value = txtMONStartTime.Value;
                txtTUELateInTime.Value = txtMONLateInTime.Value;
                txtTUEBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtTUEBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtTUEBreakEndTime.Value = txtMONBreakEndTime.Value;
                txtTUEBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtTUEHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtTUEEarlyOut.Value = txtMONEarlyOut.Value;
                txtTUEEndTime.Value = txtMONEndTime.Value;
                chkTUENightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Wednesday
            if (chkWednesdat.Checked)
            {
                txtWEDStartTime.Value = txtMONStartTime.Value;
                txtWEDLateInTime.Value = txtMONLateInTime.Value;
                txtWEDBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtWEDBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtWEDBreakEndTime.Value = txtMONBreakEndTime.Value;
                txtWEDBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtWEDHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtWEDEarlyOut.Value = txtMONEarlyOut.Value;
                txtWEDEndTime.Value = txtMONEndTime.Value;
                chkWEDNightShift.Checked = chkMonNightShift.Checked;

            }
            #endregion
            #region Thursday
            if (chkThursday.Checked)
            {
                txtTHUStartTime.Value = txtMONStartTime.Value;
                txtTHULateInTime.Value = txtMONLateInTime.Value;
                txtTHUBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtTHUBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtTHUBreakEndTime.Value = txtMONBreakEndTime.Value;
                txtTHUBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtTHUHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtTHUEarlyOut.Value = txtMONEarlyOut.Value;
                txtTHUEndTime.Value = txtMONEndTime.Value;
                chkTHUNightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Friday
            if (chkFriday.Checked)
            {
                txtFRIStartTime.Value = txtMONStartTime.Value;
                txtFRILateInTime.Value = txtMONLateInTime.Value;
                txtFRIBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtFRIBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtFRIBreakEndTime.Value = txtMONBreakEndTime.Value;
                txtFRIBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtFRIHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtFRIEarlyOut.Value = txtMONEarlyOut.Value;
                txtFRIEndTime.Value = txtMONEndTime.Value;
                chkFRINightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Saturday
            if (chkSaturday.Checked)
            {
                txtSATStartTime.Value = txtMONStartTime.Value;
                txtSATLateInTime.Value = txtMONLateInTime.Value;
                txtSATBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtSATBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtSATBreakEndTime.Value = txtMONBreakEndTime.Value;
                txtSATBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtSATHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtSATEarlyOut.Value = txtMONEarlyOut.Value;
                txtSATEndTime.Value = txtMONEndTime.Value;
                chkSATNightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Sunday
            if (chkSunday.Checked)
            {
                txtSUNStartTime.Value = txtMONStartTime.Value;
                txtSUNLateInTime.Value = txtMONLateInTime.Value;
                txtSUNBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtSUNBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtSUNBreakEndTime.Value = txtMONBreakEndTime.Value;
                txtSUNBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtSUNHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtSUNEarlyOut.Value = txtMONEarlyOut.Value;
                txtSUNEndTime.Value = txtMONEndTime.Value;
                chkSUNNightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
        }
    }
}