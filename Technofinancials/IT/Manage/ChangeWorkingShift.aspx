﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangeWorkingShift.aspx.cs" Inherits="Technofinancials.IT.Manage.ChangeWorkingShift" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/IT/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>
		.open > .dropdown-menu {
			display: block;
			width: 300px !important
		}

			.open > .dropdown-menu ::-webkit-scrollbar {
				width: 10px;
			}

		input[type="radio"], input[type="checkbox"] {
			display: none !important;
		}

		.dropdown-menu {
			box-shadow: none !important;
		}

		.multiselect-container.dropdown-menu {
			overflow: hidden;
		}

		.dropdown-menu {
			border-radius: 0px;
		}

		@media only screen and (max-width: 1280px) and (min-width: 800px) {
			section.app-content .col-lg-4.col-md-6.col-sm-12 {
				width: 40%;
			}

			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 25px !important;
			}
		}

		@media only screen and (max-width: 1440px) and (min-width: 900px) {

			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 0px !important;
				width: 170px;
			}
		}

		@media only screen and (max-width: 1024px) and (min-width: 993px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 15px !important;
				width: 130px;
			}
		}

		@media only screen and (max-width: 992px) and (min-width: 768px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 50px;
				width: 80px;
			}

			.app-content .form-group h4 {
				font-size: 10px !important;
			}
		}

		@media only screen and (max-width: 1599px) and (min-width: 1201px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 37px;
			}
		}

		.total {
			font-weight: bold;
			font-size: 20px;
			color: #188ae2;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center {
			background-color: #fff;
			border: 1px solid #aaa;
			border-radius: 5px;
			padding-left: 8px;
			padding-right: 8px;
			padding: 3px 37px;
			width: 200px;
		}

		.open > .dropdown-menu {
			padding: 15px;
			max-height: 250px !important;
			border: 1px solid #aaa;
			border-bottom-left-radius: 5px;
			border-bottom-right-radius: 5px;
		}

		multiselect-filter {
			margin-bottom: 15px;
		}

		.multiselect-container.dropdown-menu {
			overflow: overlay !important;
		}

		.multiselect-container .multiselect-all .form-check, .multiselect-container .multiselect-group .form-check, .multiselect-container .multiselect-option .form-check {
			padding: 0px;
		}

		label.form-check-label.font-weight-bold {
			margin: 0px;
		}

		span label {
			font-size: 14px !important;
			margin: 0px;
		}

		@media only screen and (max-width: 1280px) and (min-width: 800px) {
			section.app-content .col-lg-4.col-md-6.col-sm-12 {
				width: 40%;
			}
		}

		@media only screen and (max-width: 1599px) and (min-width: 1201px) {
			.select2-container, span.multiselect-selected-text {
				font-size: 12px !important;
			}
		}

		@media only screen and (max-width: 1024px) and (min-width: 993px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				width: 140px !important;
			}
		}

		@media only screen and (max-width: 992px) and (min-width: 768px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				width: 230px;
			}
		}

		.multiselect-container.dropdown-menu {
			overflow: hidden auto !important;
		}

		.navbar-toolbar > li > .dropdown-menu {
			width: 190px !important;
		}

		.open > .dropdown-menu {
			max-height: 300px !important;
		}

		.dash_User {
			width: 190px !important;
			border-radius: 10px;
			padding: 0 !important;
			border: 1px solid rgba(0,0,0,.15);
		}

		input#btnCopytoAll {
			padding: 3px 24px;
		}

		.navbar-toolbar > li > .dropdown-menu {
			box-shadow: 0 3px 12px rgb(0 0 0 / 18%) !important;
		}
	</style>
</head>


<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>

		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">
			<asp:UpdateProgress ID="updProgress"
				AssociatedUpdatePanelID="upd1"
				runat="server">
				<ProgressTemplate>
					<div class="upd_panel">
						<div class="center">
							<img src="/assets/images/Loading.gif" />
						</div>
					</div>
				</ProgressTemplate>
			</asp:UpdateProgress>

			<div class="wrap">
				<asp:UpdatePanel ID="upd1" runat="server">
					<ContentTemplate>
						<div class="content-header">
							<div class="container-fluid">
								<div class="row mb-2">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<h1 class="m-0 text-dark">Change Shifts</h1>
									</div>
									<!-- /.col -->
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
										<div style="text-align: right;">

											<button class="AD_btn" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
											<button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
											<button class="AD_btn" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
											<asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
											<asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
											<button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
											<button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
											<button class="AD_btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
											<button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClickNew" validationgroup="btnValidate" type="button">Save</button>
											<a class="AD_btn" id="btnBack" runat="server">Back</a>

										</div>
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.container-fluid -->
						</div>
						<!-- Modal -->
						<div class="modal fade M_set" id="notes-modal" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<h1 class="m-0 text-dark">Notes</h1>
										<div class="add_new">
											<button type="button" class="AD_btn" data-dismiss="modal">Save</button>
											<button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
										</div>
									</div>
									<div class="modal-body">
										<p>
											<asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
										</p>
										<p>
											<textarea id="Textarea1" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
										</p>
									</div>
								</div>

							</div>
						</div>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
					</Triggers>
				</asp:UpdatePanel>

				<section class="app-content">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Location<span style="color: red !important;">*</span>
											<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="ddlLocation" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
										<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged"></asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Department<span style="color: red !important;">*</span>
											<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="ddlDepart" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
										<asp:DropDownList ID="ddlDepart" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlDepart_SelectedIndexChanged"></asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6" style="display: none">
									<div class="form-group">
										<h4>Manager<span style="color: red !important;">*</span></h4>
										<asp:DropDownList ID="ddlManager" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged" Enabled="false">
											<asp:ListItem Text="All" Value="0"></asp:ListItem>
										</asp:DropDownList>
									</div>
								</div>

								<div class="col-sm-12" style="display: none">

									<h4>Employee<span style="color: red !important;">*</span>
										<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlEmployee" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
									<asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged"></asp:DropDownList>

								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<h4>Working Hours </h4>
										<input class="form-control" id="txtHours" value="" readonly="true" type="number" runat="server" />
									</div>
								</div>
							
								<div class="col-sm-6">
								</div>


								<div class="col-sm-12" style="display: none;">
									<div class="form-group">
										<h4>Effective Date<span style="color: red !important;">*</span></h4>
										<input type="text" class="form-control datetime-picker" data-date-format="DD-MMM-YYYY" id="txtDate" runat="server" />

									</div>
								</div>
								<div class="col-md-12" style="display: none;">
									<div class="form-group">
										<h4>Notes</h4>
									</div>
									<textarea class="form-control" id="txtNotes" placeholder="Notes" type="text" runat="server" />
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Working Shift<span style="color: red !important;">*</span>
											<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlWorkingShift" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
										<asp:DropDownList ID="ddlWorkingShift" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlWorkingShift_SelectedIndexChanged"></asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Employee<span style="color: red !important;">*</span>
											<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="lstEmployee" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
										<br />
										<asp:ListBox SelectionMode="Multiple" ID="lstEmployee" runat="server"></asp:ListBox>
									</div>

								</div>
									
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-md-12">
									<asp:UpdatePanel ID="UpdatePanel4" runat="server">
										<ContentTemplate>
											<div class="form-group" id="divAlertMsg" runat="server">
												<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
													<span>
														<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
													</span>
													<p id="pAlertMsg" runat="server">
													</p>
												</div>
											</div>
										</ContentTemplate>
									</asp:UpdatePanel>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane active row">

						<div class="col-sm-12" id="shiftDiv" runat="server">
							<div class="row">
								<div class="col-md-4 form-group">
									<div class="row">
									</div>
								</div>
							</div>
							<div class="row" id="shiftDaysDiv" runat="server">
								<div class="col-sm-6">
									

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="allowHolidayWorking" runat="server" name="returnedLockerKeys" />
															<label for="allowHolidayWorking">Allow Holiday Working</label>
														</div>
													
								</div>
								<div class="col-sm-12">
									<div class="clearfix">&nbsp;</div>
									<asp:Button ID="btnCopytoAll" CssClass="AD_btn_inn" runat="server" OnClick="btnCopytoAll_Click" Text="Copy To All" />
									<div class="clearfix">&nbsp;</div>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Day</th>
												<th>Off Day (OT)</th>
												<th>Flexible Shift</th>
												<th>Time In Exemption</th>
												<th>Start Time</th>
												<th>Late In Time</th>
												<th>Break Start Time</th>
												<th>Break End Time</th>
												<th style="display: none;">Half Day Start</th>
												<th>Early Out</th>
												<th>End Time</th>
												<th>Day<span style="color: red">+1</span> Change</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" checked="checked" id="chkMonday" runat="server"  onchange="CheckWeekDayOtSpecial('chkMondayIsSpecial','chkMondayOT')" name="returnedLockerKeys" />
															<label for="chkMonday">Monday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkMondayOT" runat="server" name="OT"   onchange="CheckWeekDayOtSpecial('chkMondayIsSpecial','chkMonday')"/>
															<label for="chkMondayOT"></label>
														</div>
													</div>

												</td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkMondayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkMondayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtMonTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtMONStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtMONLateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtMONBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtMONBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtMONHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtMONEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtMONEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkMonNightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkMonNightShift"></label>
														</div>
													</div>

												</td>

											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" checked="checked" id="chkTuesday"  onchange="CheckWeekDayOtSpecial('chkTuesdayIsSpecial','chkTuesdayOT')"  runat="server" name="returnedLockerKeys" />
															<label for="chkTuesday">Tuesday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkTuesdayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkTuesdayIsSpecial','chkTuesday')" />
															<label for="chkTuesdayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkTuesdayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkTuesdayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtTueTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtTUEStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtTUELateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtTUEBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtTUEBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtTUEHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtTUEEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtTUEEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkTUENightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkTUENightShift"></label>
														</div>
													</div>

												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" checked="checked" id="chkWednesdat" runat="server"   onchange="CheckWeekDayOtSpecial('chkWednesdayIsSpecial','chkWednesdayOT')"  />
															<label for="chkWednesdat">Wednesday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkWednesdayOT" runat="server" name="OT"   onchange="CheckWeekDayOtSpecial('chkWednesdayIsSpecial','chkWednesdat')" />
															<label for="chkWednesdayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkWednesdayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkWednesdayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtWedTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtWEDStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtWEDLateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtWEDBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtWEDBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtWEDHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtWEDEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtWEDEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkWEDNightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkWEDNightShift"></label>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" checked="checked" id="chkThursday" runat="server" onchange="CheckWeekDayOtSpecial('chkThursdayOT','chkThursdayIsSpecial')" />
															<label for="chkThursday">Thursday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkThursdayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkThursdayIsSpecial','chkThursday')" />
															<label for="chkThursdayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkThursdayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkThursdayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtThursTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtTHUStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtTHULateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtTHUBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtTHUBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtTHUHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtTHUEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtTHUEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkTHUNightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkTHUNightShift"></label>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" checked="checked" id="chkFriday" runat="server" onchange="CheckWeekDayOtSpecial('chkFridayOT','chkFridayIsSpecial')" />
															<label for="chkFriday">Friday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkFridayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkFridayIsSpecial','chkFriday')" />
															<label for="chkFridayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkFridayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkFridayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtFriTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtFRIStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtFRILateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtFRIBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="2:30 PM" id="txtFRIBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="3:00 PM" id="txtFRIHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="6:29 PM" id="txtFRIEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="6:30 PM" id="txtFRIEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkFRINightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkFRINightShift"></label>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSaturday" runat="server" onchange="CheckWeekDayOtSpecial('chkSaturdayOT','chkSaturdayIsSpecial')"  />
															<label for="chkSaturday">Saturday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSaturdayOT" runat="server" name="OT"  onchange="CheckWeekDayOtSpecial('chkSaturdayIsSpecial','chkSaturday')"  />
															<label for="chkSaturdayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSaturdayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkSaturdayIsSpecial"></label>
														</div>
													</div>

												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtSatTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtSATStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtSATLateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtSATBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtSATBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtSATHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtSATEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtSATEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSATNightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkSATNightShift"></label>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSunday" runat="server" onchange="CheckWeekDayOtSpecial('chkSundayOT','chkSundayIsSpecial')" />
															<label for="chkSunday">Sunday</label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSundayOT" runat="server" name="OT" onchange="CheckWeekDayOtSpecial('chkSundayIsSpecial','chkSunday')" />
															<label for="chkSundayOT"></label>
														</div>
													</div>
												</td>
												<td>
													<div class="col-sm-2">
														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSundayIsSpecial" runat="server" name="IsSpecial" />
															<label for="chkSundayIsSpecial"></label>
														</div>
													</div>
												</td>
												<td>
													<input class="form-control exemption-timein" value="1" id="txtSunTimeInExemption" placeholder="1 ~ 6" type="number" min="1" max="6" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:00 AM" id="txtSUNStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="9:11 AM" id="txtSUNLateInTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:00 PM" id="txtSUNBreakStartTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="1:30 PM" id="txtSUNBreakEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td style="display: none;">
													<input class="form-control time-picker" value="2:30 PM" id="txtSUNHalfDayStart" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:29 PM" id="txtSUNEarlyOut" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<input class="form-control time-picker" value="5:30 PM" id="txtSUNEndTime" placeholder="00:00" type="text" runat="server" /></td>
												<td>
													<div class="col-sm-2">

														<div class="checkbox checkbox-primary">
															<input type="checkbox" id="chkSUNNightShift" class="NightShift" runat="server" name="Night Shift" />
															<label for="chkSUNNightShift"></label>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>

								</div>
							</div>
						</div>
					</div>
				</section>
			</div>

			<!-- #dash-content -->
			<div class="clearfix">&nbsp;</div>

			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>

		<!-- Modal -->
	</form>
</body>
</html>
<script type="text/javascript">  
	$(document).ready(function () {
		$(".exemption-timein").keypress(function (e) {
			if (e.which < 49 || e.which > 54 || $(this).val() > 0) {
				$("#errmsg").html("Digits Only").show().fadeOut("slow");
				return false;
			}
		});

		$('.exemption-timein').bind('copy paste cut', function (e) {
			e.preventDefault(); //disable cut,copy,paste

		});

		BindEmployees();
	});

	function CheckWeekDayOtSpecial(control1, control2) {
		$('#' + control1).prop("checked", false);
		$('#' + control2).prop("checked", false);
	};
	function BindEmployees() {
		debugger;
		$('[id*=lstEmployee]').multiselect({
			includeSelectAllOption: true,
			buttonContainer: '<div class="btn-group" />',
			includeSelectAllIfMoreThan: 0,
			enableFiltering: true,
			filterPlaceholder: 'Search',
			filterBehavior: 'text',
			includeFilterClearBtn: true,
			enableCaseInsensitiveFiltering: true,
			numberDisplayed: 1,
			maxHeight: true,
			maxHeight: 350,
			templates: {
				button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span></button>',
				//<a href = "https://www.jqueryscript.net/tags.php?/popup/" > popup</a>
				Container: '<div class="multiselect-container dropdown-menu"></div>',
				filter: '<div class="multiselect-filter"><div class="input-group input-group-sm p-1"><div class="input-group-prepend"></div><input class="form-control multiselect-search" type="text" /></div></div>',

				filterClearBtn: '<div class="input-group-append"><button class="multiselect-clear-filter input-group-text" type="button"><i class="fas fa-times"></i></button></div>',

				option: '<button class="multiselect-option dropdown-item"></button>',

				divider: '<div class="dropdown-divider"></div>',

				optionGroup: '<button class="multiselect-group dropdown-item"></button>',

				resetButton: '<div class="multiselect-reset text-center p-2"><button class="btn btn-sm btn-block btn-outline-secondary"></button></div>'
			}
		});
	};
</script>
