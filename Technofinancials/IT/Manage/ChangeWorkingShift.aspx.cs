﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.IT.Manage
{
    public partial class ChangeWorkingShift : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int PrevParentShiftID
        {
            get
            {
                if (ViewState["PrevParentShiftID"] != null)
                {
                    return (int)ViewState["PrevParentShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrevParentShiftID"] = value;
            }
        }
        protected int ChangeWorkingShiftID
        {
            get
            {
                if (ViewState["ChangeWorkingShiftID"] != null)
                {
                    return (int)ViewState["ChangeWorkingShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["ChangeWorkingShiftID"] = value;
            }
        }
        protected int PrevShiftID
        {
            get
            {
                if (ViewState["PrevShiftID"] != null)
                {
                    return (int)ViewState["PrevShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrevShiftID"] = value;
            }
        }
        protected int ShiftID
        {
            get
            {
                if (ViewState["ShiftID"] != null)
                {
                    return (int)ViewState["ShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["ShiftID"] = value;
            }
        }

        protected int EmployeeID
        {
            get
            {
                if (ViewState["EmployeeID"] != null)
                {
                    return (int)ViewState["EmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["EmployeeID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();

                if (!Page.IsPostBack)
                {
                    shiftDaysDiv.Visible = false;
                    ViewState["ChangeWorkingShiftID"] = null;
                    BindWorkingShift();
                    BindDepartment();
                    BindLocation();
                    BindEmployeeDropdown();
                    BindEmployeeList(0, "0");
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/ChangeWorkingShift";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["EmployeeID"] != null)
                    {
                        EmployeeID = Convert.ToInt32(HttpContext.Current.Items["EmployeeID"].ToString());
                        ddlEmployee.SelectedValue = EmployeeID.ToString();
                        lstEmployee.SelectedValue = EmployeeID.ToString();
                        getShiftsByEmployeeID(EmployeeID);

                    }
                    else if (HttpContext.Current.Items["ChangeWorkingShiftID"] != null)
                    {
                        ChangeWorkingShiftID = Convert.ToInt32(HttpContext.Current.Items["ChangeWorkingShiftID"].ToString());
                        getWorkingShiftChangedByID(ChangeWorkingShiftID);
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "WorkingShiftChange";
                objDB.PrimaryColumnnName = "WorkingShiftChangeID";
                objDB.PrimaryColumnValue = ChangeWorkingShiftID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getWorkingShiftChangedByID(int employeeID)
        {
            try
            {
                DataTable dt = new DataTable();

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = employeeID;

                dt = objDB.GetWorkingShiftDetailsById(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtNotes.Value = dt.Rows[0]["Note"].ToString();
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        ddlLocation.SelectedValue = dt.Rows[0]["City"].ToString();
                        ddlWorkingShift.SelectedValue = dt.Rows[0]["WorkingShiftID"].ToString();
                        ddlDepart.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                        getShiftsByShiftID(Convert.ToInt32(dt.Rows[0]["WorkingShiftID"].ToString()));
                    }
                }

                Common.addlog("View", "IT", "Changed Working Shift Viewed", "WorkingShiftChange", objDB.ChangeWorkingShiftID);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("IT", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void clearFields()
        {
            txtDate.Value = "";
            ddlEmployee.SelectedValue = "0";
            ddlWorkingShift.SelectedValue = "0";
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();

            try
            {
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "WorkingShiftChange", "WorkingShiftChangeID", HttpContext.Current.Items["ChangeWorkingShiftID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "IT", "Working Shift of ID\"" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/IT/view/ChangeWorkingShift", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/IT/manage/ChangeWorkingShift/edit-ChangeWorkingShift-" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString(), "AddOn \"" + ChangeWorkingShiftID + "\"", "WorkingShiftChange", "Announcement", Convert.ToInt32(HttpContext.Current.Items["ChangeWorkingShiftID"].ToString()));

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();

            try
            {
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "WorkingShiftChange", "ChangeWorkingShiftID", HttpContext.Current.Items["ChangeWorkingShiftID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "IT", "Working Shift of ID\"" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString() + "\" Status Changed", "WorkingShiftChange", ChangeWorkingShiftID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            CheckSessions();

            try
            {
                objDB.ChangeWorkingShiftID = ChangeWorkingShiftID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteAddOnsExpenseByID();
                Common.addlog("Working Shift Rejected", "IT", "Working Shift of ID\"" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString() + "\" Working Shift Rejected", "WorkingShiftChange", ChangeWorkingShiftID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Working Shift Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindEmployeeDropdown(int DptId)
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DeptID = DptId;
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyIDandDptIDonly(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindEmployeeList(int DptId, string LocationName)
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DeptID = DptId;
                objDB.LocationName = LocationName;
                lstEmployee.DataSource = objDB.GetEmployeesIndex(ref errorMsg);
                lstEmployee.DataTextField = "EmployeeName";
                lstEmployee.DataValueField = "EmployeeID";
                lstEmployee.DataBind();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        private void BindEmployeeDropdown()
        {
            try
            {
                ddlEmployee.DataSource = null;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyIDonly(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindEmployeeDropdownLcoation(int DptId, string Location)
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DeptID = DptId;
                objDB.LocationName = Location;
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyIDandDptID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindEmployeeDropdownLcoationonly(string Location)
        {
            try
            {
                ddlEmployee.DataSource = null;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.LocationName = Location;
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyIDandDptIDLocationOnly(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindDepartDropdown(string Location)
        {
            try
            {
                ddlDepart.DataSource = null;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.LocationName = Location;
                ddlDepart.DataSource = objDB.GetAllApproveDepartmentbyLcoation(ref errorMsg);
                ddlDepart.DataTextField = "DeptName";
                ddlDepart.DataValueField = "DeptID";
                ddlDepart.DataBind();
                ddlDepart.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindWorkingShift()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlWorkingShift.DataSource = objDB.GetApproveAllWorkingShiftsByCompanyID(ref errorMsg);
                ddlWorkingShift.DataTextField = "ShiftName";
                ddlWorkingShift.DataValueField = "ShiftID";
                ddlWorkingShift.DataBind();
                ddlWorkingShift.Items.Insert(0, new ListItem("ALL", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindDepartment()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlDepart.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                ddlDepart.DataTextField = "DeptName";
                ddlDepart.DataValueField = "DeptID";
                ddlDepart.DataBind();
                ddlDepart.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindLocation()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlLocation.DataSource = objDB.GetAllLcoationsByCompanyID(ref errorMsg);
                ddlLocation.DataTextField = "PlaceOfPost";
                ddlLocation.DataValueField = "PlaceOfPost";
                ddlLocation.DataBind();
                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getShiftsByShiftID(int ShiftID)
        {
            try
            {
                shiftDaysDiv.Visible = true;
                DataTable dt, dtday = new DataTable();
                objDB.WorkingShiftID = ShiftID;
                dt = objDB.GetWorkingShiftByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtHours.Value = dt.Rows[0]["Hours"].ToString();
                        chkMonday.Checked = getCheckBoxValue(dt.Rows[0]["Monday"].ToString());
                        chkTuesday.Checked = getCheckBoxValue(dt.Rows[0]["Tuesday"].ToString());
                        chkWednesdat.Checked = getCheckBoxValue(dt.Rows[0]["Wednesday"].ToString());
                        chkThursday.Checked = getCheckBoxValue(dt.Rows[0]["Thursday"].ToString());
                        chkFriday.Checked = getCheckBoxValue(dt.Rows[0]["Friday"].ToString());
                        chkSaturday.Checked = getCheckBoxValue(dt.Rows[0]["Saturday"].ToString());
                        chkSunday.Checked = getCheckBoxValue(dt.Rows[0]["Sunday"].ToString());

                        objDB.DocID = ShiftID;
                        objDB.DocType = "Shifts";
                    }
                }
                dtday = objDB.GetWorkingShiftDaysByShiftID(ref errorMsg);
                if (dtday != null && dtday.Rows.Count > 0)
                {
                    allowHolidayWorking.Checked = dtday.Rows[0]["IsSpecial"].ToString() == "True" ? true : false;
                    for (int i = 0; i < dtday.Rows.Count; i++)
                    {
                        switch (dtday.Rows[i]["DayName"].ToString())
                        {
                            case "Monday":
                                {
                                    chkMondayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkMonNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtMONStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtMONLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtMONBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtMONBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtMONHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtMONEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtMONEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkMondayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }

                            case "Tuesday":
                                {
                                    chkTuesdayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkTUENightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtTUEStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtTUELateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtTUEBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtTUEBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtTUEHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtTUEEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtTUEEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkTuesdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                            case "Wednesday":
                                {
                                    chkWednesdayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkWEDNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtWEDStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtWEDLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtWEDBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtWEDBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtWEDHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtWEDEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtWEDEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkWednesdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                            case "Thursday":
                                {
                                    chkThursdayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkTHUNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtTHUStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtTHULateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtTHUBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtTHUBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtTHUHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtTHUEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtTHUEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkThursdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                            case "Friday":
                                {
                                    chkFridayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkFRINightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtFRIStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtFRILateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtFRIBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtFRIBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtFRIHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtFRIEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtFRIEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkFridayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                            case "Saturday":
                                {
                                    chkSaturdayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkSATNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtSATStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtSATLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtSATBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtSATBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtSATHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtSATEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtSATEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkSaturdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                            case "Sunday":
                                {
                                    chkSundayOT.Checked = getCheckBoxValue(dtday.Rows[i]["AllowOverTime"].ToString());
                                    chkSUNNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                                    txtSUNStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                                    txtSUNLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                                    txtSUNBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                                    txtSUNBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                                    txtSUNHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                                    txtSUNEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                                    txtSUNEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                                    chkSundayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["IsFlexible"].ToString());
                                    break;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getShiftsByEmployeeID(int EmpID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.EmployeeID = EmpID;
                dt = objDB.GetWorkingShiftByEmployeeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        PrevParentShiftID = Convert.ToInt32(dt.Rows[0]["ParentID"].ToString());
                        PrevShiftID = Convert.ToInt32(dt.Rows[0]["ShiftID"].ToString());

                        if (PrevParentShiftID == 0)
                        {
                            PrevParentShiftID = PrevShiftID;
                        }

                        ddlWorkingShift.SelectedValue = PrevParentShiftID.ToString();

                        shiftDaysDiv.Visible = true;
                        getShiftsByShiftID(PrevShiftID);
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            if (ddlEmployee.SelectedItem.Value != "0")
            {
                string Emp_Name = ddlEmployee.SelectedValue;
                if (Emp_Name != "" || Emp_Name != null)
                {
                    getShiftsByEmployeeID(Convert.ToInt32(Emp_Name));
                }
            }
            else
            {
                PrevShiftID = 0;
                PrevParentShiftID = 0;
            }
        }

        private bool getCheckBoxValue(string val)
        {
            return val.ToLower() == "true";
        }

        protected void btnSave_ServerClickNew(object sender, EventArgs e)
        {
            string Response = "";
            divAlertMsg.Visible = false;
            try
            {
                CheckSessions();
                objDB.Hours = txtHours.Value;
                Response = CheckShiftDays();
                if (Response == "Ok")
                {
                    divAlertMsg.Visible = false;
                    string res = "";
                    string lcoation = ddlLocation.SelectedValue;
                    List<string> lstEmpIds = new List<string>();

                    res = CheckWorkingShiftDays();

                    foreach (ListItem item in lstEmployee.Items)
                    {
                        if (item.Selected)
                        {
                            lstEmpIds.Add(item.Value);
                        }
                    }

                    if (res == "New Shift")
                    {
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.WorkingShiftName = ddlWorkingShift.SelectedItem.Text;
                        objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                        objDB.parentShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
                        objDB.Monday = chkMonday.Checked;
                        objDB.Tuesday = chkTuesday.Checked;
                        objDB.Wednesday = chkWednesdat.Checked;
                        objDB.Thursday = chkThursday.Checked;
                        objDB.Friday = chkFriday.Checked;
                        objDB.Saturday = chkSaturday.Checked;
                        objDB.Sunday = chkSunday.Checked;
                        objDB.DeptID = int.Parse(ddlDepart.SelectedValue);
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.LocationName = ddlLocation.SelectedValue == "0" ? "%%" : ddlLocation.SelectedValue;

                        ShiftID = Convert.ToInt32(objDB.AddShiftFromESSNew1(lstEmpIds));
                        objDB.ShiftID = ShiftID;
                        
                        res = "New Shift Added and Assigned";

                        AddShiftDays();
                        objDB.isSpecial = allowHolidayWorking.Checked == true ? true : false;
                        objDB.AllowWorkingHoliday();
                    }
                    else
                    {
                        objDB.Location = ddlLocation.SelectedValue;
                        objDB.DepartmentID = ddlDepart.SelectedValue;
                        objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                        objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
                        ShiftID = Convert.ToInt32(objDB.UpdateShiftForMultipleEmployees(lstEmpIds)); 
                        objDB.ShiftID = ShiftID;
                        objDB.isSpecial = allowHolidayWorking.Checked == true ? true : false;
                        objDB.AllowWorkingHoliday();
                        res = "Shift Assigned";
                    }

                    clearFields();

                    if (res == "New Shift Added and Assigned" || res == "Shift Assigned")
                    {

                        if (ddlEmployee.SelectedValue != "0")
                        {
                            PrevShiftID = ShiftID;
                            PrevParentShiftID = objDB.parentShiftID;
                        }

                        if (res == "New Shift Added and Assigned") { Common.addlog("Add", "IT", "New Shift \"" + objDB.WorkingShiftName + "\" Added", "WorkingShifts"); }
                        if (res == "Shift Assigned") { Common.addlog("Update", "IT", "Shift \"" + objDB.WorkingShiftName + "\" Updated", "WorkingShifts", objDB.ShiftID); }

                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                        pAlertMsg.InnerHtml = res;
                    }
                    else
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = res;
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Working hours did not match";
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnSave_ServerClickNewDepart(object sender, EventArgs e)
        {
            string Response = "";
            divAlertMsg.Visible = false;
            try
            {
                CheckSessions();
                objDB.Hours = txtHours.Value;
                Response = CheckShiftDays();
                if (Response == "Ok")
                {
                    string res = "";
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.WorkingShiftName = ddlWorkingShift.SelectedItem.Text;
                    objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                    objDB.parentShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
                    objDB.Monday = chkMonday.Checked;
                    objDB.Tuesday = chkTuesday.Checked;
                    objDB.Wednesday = chkWednesdat.Checked;
                    objDB.Thursday = chkThursday.Checked;
                    objDB.Friday = chkFriday.Checked;
                    objDB.Saturday = chkSaturday.Checked;
                    objDB.Sunday = chkSunday.Checked;

                    if (PrevParentShiftID == objDB.parentShiftID && PrevParentShiftID != PrevShiftID)
                    {

                        ShiftID = PrevShiftID;
                        objDB.ModifiedBy = Session["UserName"].ToString();
                        objDB.WorkingShiftID = ShiftID;
                        res = objDB.UpdateShift();
                        objDB.DelWorkingShiftDaysByShiftID();
                        AddShiftDays();
                    }
                    else
                    {
                        objDB.CreatedBy = Session["UserName"].ToString();
                        ShiftID = Convert.ToInt32(objDB.AddShiftFromESS());
                        res = "New Shift Added";
                        AddShiftDays();
                        clearFields();
                    }

                    if (res == "New Shift Added" || res == "Shift Data Updated")
                    {
                        PrevShiftID = ShiftID;
                        PrevParentShiftID = objDB.parentShiftID;

                        if (res == "New Shift Added") { Common.addlog("Add", "IT", "New Shift \"" + objDB.WorkingShiftName + "\" Added", "WorkingShifts"); }
                        if (res == "Shift Data Updated") { Common.addlog("Update", "IT", "Shift \"" + objDB.WorkingShiftName + "\" Updated", "WorkingShifts", objDB.ShiftID); }

                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                        pAlertMsg.InnerHtml = res;
                    }
                    else
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = res;
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Working hours did not match";
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private string CheckShiftDays()
        {
            string res = "";
            #region Monday
            objDB.StartTime = txtMONStartTime.Value;
            objDB.LateInTime = txtMONLateInTime.Value;
            objDB.BreakStartTime = txtMONBreakStartTime.Value;
            objDB.BreakEndTime = txtMONBreakEndTime.Value;
            objDB.HalfDayStart = txtMONHalfDayStart.Value;
            objDB.EarlyOut = txtMONEarlyOut.Value;
            objDB.EndTime = txtMONEndTime.Value;
            objDB.isNightShift = chkMonNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Monday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkMonday.Checked;


            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Tuesday

            objDB.StartTime = txtTUEStartTime.Value;
            objDB.LateInTime = txtTUELateInTime.Value;
            objDB.BreakStartTime = txtTUEBreakStartTime.Value;
            objDB.BreakEndTime = txtTUEBreakEndTime.Value;
            objDB.HalfDayStart = txtTUEHalfDayStart.Value;
            objDB.EarlyOut = txtTUEEarlyOut.Value;
            objDB.EndTime = txtTUEEndTime.Value;
            objDB.isNightShift = chkTUENightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Tuesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkTuesday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Wednesday
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.LateInTime = txtWEDLateInTime.Value;
            objDB.BreakStartTime = txtWEDBreakStartTime.Value;
            objDB.BreakEndTime = txtWEDBreakEndTime.Value;
            objDB.HalfDayStart = txtWEDHalfDayStart.Value;
            objDB.EarlyOut = txtWEDEarlyOut.Value;
            objDB.EndTime = txtWEDEndTime.Value;
            objDB.isNightShift = chkWEDNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Wednesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkWednesdat.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Thursday
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.LateInTime = txtTHULateInTime.Value;
            objDB.BreakStartTime = txtTHUBreakStartTime.Value;
            objDB.BreakEndTime = txtTHUBreakEndTime.Value;
            objDB.HalfDayStart = txtTHUHalfDayStart.Value;
            objDB.EarlyOut = txtTHUEarlyOut.Value;
            objDB.EndTime = txtTHUEndTime.Value;
            objDB.isNightShift = chkTHUNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Thursday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkThursday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Friday
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.LateInTime = txtFRILateInTime.Value;
            objDB.BreakStartTime = txtFRIBreakStartTime.Value;
            objDB.BreakEndTime = txtFRIBreakEndTime.Value;
            objDB.HalfDayStart = txtFRIHalfDayStart.Value;
            objDB.EarlyOut = txtFRIEarlyOut.Value;
            objDB.EndTime = txtFRIEndTime.Value;
            objDB.isNightShift = chkFRINightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Friday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkFriday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Saturday
            objDB.StartTime = txtSATStartTime.Value;
            objDB.LateInTime = txtSATLateInTime.Value;
            objDB.BreakStartTime = txtSATBreakStartTime.Value;
            objDB.BreakEndTime = txtSATBreakEndTime.Value;
            objDB.HalfDayStart = txtSATHalfDayStart.Value;
            objDB.EarlyOut = txtSATEarlyOut.Value;
            objDB.EndTime = txtSATEndTime.Value;
            objDB.isNightShift = chkSATNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Saturday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSaturday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Sunday
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.LateInTime = txtSUNLateInTime.Value;
            objDB.BreakStartTime = txtSUNBreakStartTime.Value;
            objDB.BreakEndTime = txtSUNBreakEndTime.Value;
            objDB.HalfDayStart = txtSUNHalfDayStart.Value;
            objDB.EarlyOut = txtSUNEarlyOut.Value;
            objDB.EndTime = txtSUNEndTime.Value;
            objDB.isNightShift = chkSUNNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Sunday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSunday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }

            #endregion

            return "Ok";
        }

        private void AddShiftDays()
        {

            #region Monday
            objDB.isOffDayOT = chkMondayOT.Checked;
            objDB.IsFlexible = chkMondayIsSpecial.Checked;
            objDB.StartTime = txtMONStartTime.Value;
            objDB.LateInTime = txtMONLateInTime.Value;
            objDB.TimeInExemption = txtMonTimeInExemption.Value;
            objDB.BreakStartTime = txtMONBreakStartTime.Value;
            objDB.BreakEndTime = txtMONBreakEndTime.Value;
            objDB.HalfDayStart = txtMONBreakStartTime.Value;
            objDB.EarlyOut = txtMONEarlyOut.Value;
            objDB.EndTime = txtMONEndTime.Value;
            objDB.isNightShift = chkMonNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Monday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkMonday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Tuesday

            objDB.isOffDayOT = chkTuesdayOT.Checked;
            objDB.IsFlexible = chkTuesdayIsSpecial.Checked;
            objDB.StartTime = txtTUEStartTime.Value;
            objDB.LateInTime = txtTUELateInTime.Value;
            objDB.TimeInExemption = txtTueTimeInExemption.Value;
            objDB.BreakStartTime = txtTUEBreakStartTime.Value;
            objDB.HalfDayStart = txtTUEBreakStartTime.Value;
            objDB.BreakEndTime = txtTUEBreakEndTime.Value;
            objDB.EarlyOut = txtTUEEarlyOut.Value;
            objDB.EndTime = txtTUEEndTime.Value;
            objDB.isNightShift = chkTUENightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Tuesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkTuesday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Wednesday
            objDB.isOffDayOT = chkWednesdayOT.Checked;
            objDB.IsFlexible = chkWednesdayIsSpecial.Checked;
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.TimeInExemption = txtWedTimeInExemption.Value;
            objDB.LateInTime = txtWEDLateInTime.Value;
            objDB.BreakStartTime = txtWEDBreakStartTime.Value;
            objDB.HalfDayStart = txtWEDBreakStartTime.Value;
            objDB.BreakEndTime = txtWEDBreakEndTime.Value;
            objDB.EarlyOut = txtWEDEarlyOut.Value;
            objDB.EndTime = txtWEDEndTime.Value;
            objDB.isNightShift = chkWEDNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Wednesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkWednesdat.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Thursday
            objDB.isOffDayOT = chkThursdayOT.Checked;
            objDB.IsFlexible = chkThursdayIsSpecial.Checked;
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.TimeInExemption = txtThursTimeInExemption.Value;
            objDB.LateInTime = txtTHULateInTime.Value;
            objDB.BreakStartTime = txtTHUBreakStartTime.Value;
            objDB.HalfDayStart = txtTHUBreakStartTime.Value;
            objDB.BreakEndTime = txtTHUBreakEndTime.Value;
            objDB.EarlyOut = txtTHUEarlyOut.Value;
            objDB.EndTime = txtTHUEndTime.Value;
            objDB.isNightShift = chkTHUNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Thursday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkThursday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Friday
            objDB.isOffDayOT = chkFridayOT.Checked;
            objDB.IsFlexible = chkFridayIsSpecial.Checked;
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.TimeInExemption = txtFriTimeInExemption.Value;
            objDB.LateInTime = txtFRILateInTime.Value;
            objDB.BreakStartTime = txtFRIBreakStartTime.Value;
            objDB.HalfDayStart = txtFRIBreakStartTime.Value;
            objDB.BreakEndTime = txtFRIBreakEndTime.Value;
            objDB.EarlyOut = txtFRIEarlyOut.Value;
            objDB.EndTime = txtFRIEndTime.Value;
            objDB.isNightShift = chkFRINightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Friday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkFriday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Saturday
            objDB.isOffDayOT = chkSaturdayOT.Checked;
            objDB.IsFlexible = chkSaturdayIsSpecial.Checked;
            objDB.StartTime = txtSATStartTime.Value;
            objDB.LateInTime = txtSATLateInTime.Value;
            objDB.TimeInExemption = txtSatTimeInExemption.Value;
            objDB.BreakStartTime = txtSATBreakStartTime.Value;
            objDB.HalfDayStart = txtSATBreakStartTime.Value;
            objDB.BreakEndTime = txtSATBreakEndTime.Value;
            objDB.EarlyOut = txtSATEarlyOut.Value;
            objDB.EndTime = txtSATEndTime.Value;
            objDB.isNightShift = chkSATNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Saturday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSaturday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Sunday
            objDB.isOffDayOT = chkSundayOT.Checked;
            objDB.IsFlexible = chkSundayIsSpecial.Checked;
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.LateInTime = txtSUNLateInTime.Value;
            objDB.TimeInExemption = txtSunTimeInExemption.Value;
            objDB.BreakStartTime = txtSUNBreakStartTime.Value;
            objDB.HalfDayStart = txtSUNBreakStartTime.Value;
            objDB.BreakEndTime = txtSUNBreakEndTime.Value;
            objDB.EarlyOut = txtSUNEarlyOut.Value;
            objDB.EndTime = txtSUNEndTime.Value;
            objDB.isNightShift = chkSUNNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Sunday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSunday.Checked;

            objDB.AddWorkingShiftsDays2();

            #endregion
        }

        private string CheckWorkingShiftDays()
        {

            #region Monday
            objDB.isOverTime = chkMondayOT.Checked;
            objDB.IsFlexible = chkMondayIsSpecial.Checked;
            objDB.StartTime = txtMONStartTime.Value;
            objDB.TimeInExemption = txtMonTimeInExemption.Value;
            objDB.EndTime = txtMONEndTime.Value;
            objDB.isNightShift = chkMonNightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Monday";
            objDB.isExpectedZero = !chkMonday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Tuesday

            objDB.isOverTime = chkTuesdayOT.Checked;
            objDB.IsFlexible = chkTuesdayIsSpecial.Checked;
            objDB.StartTime = txtTUEStartTime.Value;
            objDB.TimeInExemption = txtTueTimeInExemption.Value;
            objDB.EndTime = txtTUEEndTime.Value;
            objDB.isNightShift = chkTUENightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Tuesday";
            objDB.isExpectedZero = !chkTuesday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Wednesday
            objDB.isOverTime = chkWednesdayOT.Checked;
            objDB.IsFlexible = chkWednesdayIsSpecial.Checked;
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.TimeInExemption = txtWedTimeInExemption.Value;
            objDB.EndTime = txtWEDEndTime.Value;
            objDB.isNightShift = chkWEDNightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Wednesday";
            objDB.isExpectedZero = !chkWednesdat.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Thursday
            objDB.isOverTime = chkThursdayOT.Checked;
            objDB.IsFlexible = chkThursdayIsSpecial.Checked;
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.TimeInExemption = txtThursTimeInExemption.Value;
            objDB.EndTime = txtTHUEndTime.Value;
            objDB.isNightShift = chkTHUNightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Thursday";
            objDB.isExpectedZero = !chkThursday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Friday
            objDB.isOverTime = chkFridayOT.Checked;
            objDB.IsFlexible = chkFridayIsSpecial.Checked;
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.TimeInExemption = txtFriTimeInExemption.Value;
            objDB.EndTime = txtFRIEndTime.Value;
            objDB.isNightShift = chkFRINightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Friday";
            objDB.isExpectedZero = !chkFriday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Saturday
            objDB.isOverTime = chkSaturdayOT.Checked;
            objDB.IsFlexible = chkSaturdayIsSpecial.Checked;
            objDB.StartTime = txtSATStartTime.Value;
            objDB.TimeInExemption = txtSatTimeInExemption.Value;
            objDB.EndTime = txtSATEndTime.Value;
            objDB.isNightShift = chkSATNightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue); ;
            objDB.ShiftDayName = "Saturday";
            objDB.isExpectedZero = !chkSaturday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            #region Sunday
            objDB.isOverTime = chkSundayOT.Checked;
            objDB.IsFlexible = chkSundayIsSpecial.Checked;
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.TimeInExemption = txtSunTimeInExemption.Value;
            objDB.EndTime = txtSUNEndTime.Value;
            objDB.isNightShift = chkSUNNightShift.Checked;
            objDB.WorkingShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            objDB.ShiftDayName = "Sunday";
            objDB.isExpectedZero = !chkSunday.Checked;

            if (objDB.CheckWorkingShiftsDays() == "New Shift")
            {
                return "New Shift";
            }
            #endregion

            return "Existing Shift";
        }

        protected void ddlWorkingShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            int shiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
            getShiftsByShiftID(shiftID);
        }

        protected void ddlDepart_SelectedIndexChanged(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;

            int Dpt_ID = Convert.ToInt32(ddlDepart.SelectedItem.Value);
            string location = ddlLocation.SelectedItem.Value;

            if (location == "0")
                BindEmployeeDropdown(Dpt_ID);
            else
                BindEmployeeDropdownLcoation(Dpt_ID, location);

            BindEmployeeList(Dpt_ID, location);
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            string location = ddlLocation.SelectedItem.Value;
            int dptid = Convert.ToInt32(ddlDepart.SelectedItem.Value.ToString());
            divAlertMsg.Visible = false;
            if (location == "All")
            {
                BindDepartment();
                BindEmployeeDropdown();
            }
            else if (location == "All" && dptid == 0)
            {
                BindEmployeeDropdown();
            }
            else if (location == "All" && dptid != 0)
            {
                BindEmployeeDropdown(dptid);
            }
            else if (location != "All" && dptid != 0)
            {
                BindDepartDropdown(location);
                BindEmployeeDropdownLcoation(dptid, location);
            }
            else if (location != "All" && dptid == 0)
            {
                BindDepartDropdown(location);
                BindEmployeeDropdownLcoationonly(location);
            }

            BindEmployeeList(dptid, ddlLocation.SelectedValue);
        }

        protected void btnCopytoAll_Click(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;
            #region Tuesday
            if (chkTuesday.Checked)
            {
                txtTUEStartTime.Value = txtMONStartTime.Value;
                txtTUELateInTime.Value = txtMONLateInTime.Value;
                txtTueTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtTUEBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtTUEBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtTUEBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtTUEBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtTUEHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtTUEEarlyOut.Value = txtMONEarlyOut.Value;
                txtTUEEndTime.Value = txtMONEndTime.Value;
                chkTUENightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Wednesday
            if (chkWednesdat.Checked)
            {
                txtWEDStartTime.Value = txtMONStartTime.Value;
                txtWEDLateInTime.Value = txtMONLateInTime.Value;
                txtWedTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtWEDBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtWEDBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtWEDBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtWEDBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtWEDHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtWEDEarlyOut.Value = txtMONEarlyOut.Value;
                txtWEDEndTime.Value = txtMONEndTime.Value;
                chkWEDNightShift.Checked = chkMonNightShift.Checked;

            }
            #endregion
            #region Thursday
            if (chkThursday.Checked)
            {
                txtTHUStartTime.Value = txtMONStartTime.Value;
                txtTHULateInTime.Value = txtMONLateInTime.Value;
                txtThursTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtTHUBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtTHUBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtTHUBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtTHUBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtTHUHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtTHUEarlyOut.Value = txtMONEarlyOut.Value;
                txtTHUEndTime.Value = txtMONEndTime.Value;
                chkTHUNightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Friday
            if (chkFriday.Checked)
            {
                txtFRIStartTime.Value = txtMONStartTime.Value;
                txtFRILateInTime.Value = txtMONLateInTime.Value;
                txtFriTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtFRIBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtFRIBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtFRIBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtFRIBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtFRIHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtFRIEarlyOut.Value = txtMONEarlyOut.Value;
                txtFRIEndTime.Value = txtMONEndTime.Value;
                chkFRINightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Saturday
            if (chkSaturday.Checked)
            {
                txtSATStartTime.Value = txtMONStartTime.Value;
                txtSATLateInTime.Value = txtMONLateInTime.Value;
                txtSatTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtSATBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtSATBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtSATBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtSATBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtSATHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtSATEarlyOut.Value = txtMONEarlyOut.Value;
                txtSATEndTime.Value = txtMONEndTime.Value;
                chkSATNightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
            #region Sunday
            if (chkSunday.Checked)
            {
                txtSUNStartTime.Value = txtMONStartTime.Value;
                txtSUNLateInTime.Value = txtMONLateInTime.Value;
                txtSunTimeInExemption.Value = txtMonTimeInExemption.Value;
                //txtSUNBreakEarlyStartTime.Value = txtMONBreakEarlyStartTime.Value;
                txtSUNBreakStartTime.Value = txtMONBreakStartTime.Value;
                txtSUNBreakEndTime.Value = txtMONBreakEndTime.Value;
                //txtSUNBreakLateTime.Value = txtMONBreakLateTime.Value;
                txtSUNHalfDayStart.Value = txtMONHalfDayStart.Value;
                txtSUNEarlyOut.Value = txtMONEarlyOut.Value;
                txtSUNEndTime.Value = txtMONEndTime.Value;
                chkSUNNightShift.Checked = chkMonNightShift.Checked;
            }
            #endregion
        }
    }
}