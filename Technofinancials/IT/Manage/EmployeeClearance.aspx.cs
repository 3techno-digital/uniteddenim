﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

namespace Technofinancials.IT.Manage
{
	public partial class EmployeeClearance : System.Web.UI.Page
	{
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int SeperationId
        {
            get
            {
                if (ViewState["SeperationId"] != null)
                {
                    return (int)ViewState["SeperationId"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["SeperationId"] = value;
            }
        }
        protected string Attachment
        {
            get
            {
                if (ViewState["Attachment"] != null)
                {
                    return (string)ViewState["Attachment"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["Attachment"] = value;
            }
        }
        protected int SelectedEmployee
        {
            get
            {
                if (ViewState["SelectedEmployee"] != null)
                {
                    return (int)ViewState["SelectedEmployee"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["SelectedEmployee"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/employee-clearance";
                    BindDropDowns();
                    btnSave.Visible = true;
                    Attachment = "";
                    divAlertMsg.Visible = false;
                    ViewState["ExpDetailsSrNo"] = null;
                    ViewState["dtExpDetails"] = null;
                    TotalAmount.InnerText = "00.00";
                    btnUpdateDiv.Visible = false;
                    btnAddDiv.Visible = true;

                    dtExpDetails = new DataTable();
                    dtExpDetails = createExpDetails();
                    BindExpDetailsTable();

                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    //txtCode.Value = objDB.GenerateOtherExpenseCode();

                    if (HttpContext.Current.Items["SeperationId"] != null)
                    {
                        GetSeperationDeductionDetails(Convert.ToInt32(HttpContext.Current.Items["SeperationId"].ToString()));
                        //CheckAccess();
                    }
                }

                Page.ClientScript.GetPostBackEventReference(this, string.Empty);
                string ctrlName = Request.Params.Get("__EVENTTARGET");
                string ctrlArgs = Request.Params.Get("__EVENTARGUMENT");

                if (!String.IsNullOrEmpty(ctrlName) && ctrlName == "attachments")
                {
                    if (updLogo != null)
                    {
                        //foreach (HttpPostedFile file in updAttachments.PostedFile)
                        //{

                        HttpPostedFile file = updLogo.PostedFile;
                        Random rand = new Random((int)DateTime.Now.Ticks);
                        int randnum = 0;

                        string fn = "";
                        string exten = "";
                        string destDir = Server.MapPath("~/assets/" + ctrlName + "/SeperationDeductions/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
                        randnum = rand.Next(1, 100000);
                        fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

                        if (!Directory.Exists(destDir))
                        {
                            Directory.CreateDirectory(destDir);
                        }

                        string fname = Path.GetFileName(file.FileName);
                        exten = Path.GetExtension(file.FileName);
                        file.SaveAs(destDir + fn + exten);

                        Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/" + ctrlName + "/SeperationDeductions/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
                        imgLogo.Src = Attachment;
                        //dr[3] = ddlDocType.SelectedValue;

                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "EmployeeSeperationDetails";
                objDB.PrimaryColumnnName = "SeperationDetailsID";
                objDB.PrimaryColumnValue = HttpContext.Current.Items["SeperationId"].ToString();
                objDB.DocName = "EmployeeSeperationDetails";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void GetSeperationDeductionDetails(int seperationId)
        {
            try
            {
                DataTable dt = new DataTable();
                this.SeperationId = seperationId;
                objDB.SeperationDetailsID = seperationId;
                dt = objDB.GetEmployeeSeperationDetailsByID(ref errorMsg);
                double totalamount = 0.0;
                if (dt != null)
                { 
                   
                    if (dt.Rows.Count > 0)
                    {
                        txtDate.Value = Convert.ToDateTime(dt.Rows[0]["SeperationDate"].ToString()).ToString("dd-MMM-yyyy");
                       // ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        txtNotes.Value = dt.Rows[0]["ITComments"].ToString();
                        //lblAmount.InnerText = dt.Rows[0]["DeductionAmount"].ToString();
                        totalamount += Convert.ToDouble(dt.Rows[0]["DeductionAmount"].ToString());
                    }
                }
                TotalAmount.InnerText = totalamount.ToString();
                ddlEmployee.Items.Clear();
                ddlEmployee.DataSource = dt;
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee- --", "0"));
                ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                SelectedEmployee = Convert.ToInt32(ddlEmployee.SelectedValue);

                GetSeperationDeductionItemDetailsByID();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void GetSeperationDeductionItemDetailsByID()
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);

                dt = objDB.GetSeperationDeductionItemsByEmployeeId(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtExpDetails.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["ItemName"],
                            dt.Rows[i]["AssetNumber"],
                            dt.Rows[i]["Remarks"],
                            dt.Rows[i]["Amount"],
                            dt.Rows[i]["FilePath"],
                            dt.Rows[i]["ClearanceItemID"],
                            dt.Rows[i]["DeductionItemDetailsID"]
                        });
                        }
                    }
                }

                BindExpDetailsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private DataTable dtExpDetails
        {
            get
            {
                if (ViewState["dtExpDetails"] != null)
                {
                    return (DataTable)ViewState["dtExpDetails"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtExpDetails"] = value;
            }
        }
        protected int ExpDetailsSrNo
        {
            get
            {
                if (ViewState["ExpDetailsSrNo"] != null)
                {
                    return (int)ViewState["ExpDetailsSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["ExpDetailsSrNo"] = value;
            }
        }


        private DataTable createExpDetails()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("ItemName");
            dt.Columns.Add("AssetNumber");
            dt.Columns.Add("Remarks");
            dt.Columns.Add("Amount");
            dt.Columns.Add("FilePath");
            dt.Columns.Add("ClearanceItemID");
            dt.Columns.Add("DeductionItemDetailsID");
            dt.AcceptChanges();
            return dt;
        }
        protected void BindExpDetailsTable()
        {
            try
            {
                if (ViewState["dtExpDetails"] == null)
                {
                    dtExpDetails = createExpDetails();
                    ViewState["dtExpDetails"] = dtExpDetails;
                }

                gvExpDetails.DataSource = dtExpDetails;
                gvExpDetails.DataBind();

                if (gvExpDetails.Rows.Count > 0)
                {
                    gvExpDetails.UseAccessibleHeader = true;
                    gvExpDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void lnkRemoveFile_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                if (dtExpDetails.Rows[i][0].ToString() == delSr)
                {
                    double.Parse(dtExpDetails.Rows[i]["Amount"].ToString());
                    TotalAmount.InnerText = (double.Parse(TotalAmount.InnerText) - double.Parse(dtExpDetails.Rows[i]["Amount"].ToString())).ToString();

                    dtExpDetails.Rows[i].Delete();
                    dtExpDetails.AcceptChanges();
                }
            }
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                dtExpDetails.Rows[i].SetField(0, i + 1);
                dtExpDetails.AcceptChanges();
            }

            ExpDetailsSrNo = dtExpDetails.Rows.Count + 1;
            BindExpDetailsTable();

            if (btnUpdateDiv.Visible)
            {
                hdnExpenseSrNO.Value = "0";

                txtDescriptionDetail.Value = "";
                txtAmountDetail.Value = "";
                ddlClearanceItems.SelectedValue = "0";
                Attachment = "";

                btnUpdateDiv.Visible = false;
                btnAddDiv.Visible = true;

            }
        }

        private void BindDropDowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());

            //ddlEmployee.Items.Clear();
            //ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
            //ddlEmployee.DataTextField = "EmployeeName";
            //ddlEmployee.DataValueField = "EmployeeID";
            //ddlEmployee.DataBind();
            //ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee- --", "0"));

            ddlClearanceItems.Items.Clear();
            ddlClearanceItems.DataSource = objDB.GetAllClearanceItemsByCopmanyAndDepartmentId(ref errorMsg);
            ddlClearanceItems.DataTextField = "ItemName";
            ddlClearanceItems.DataValueField = "ClearanceItemID";
            ddlClearanceItems.DataBind();
            ddlClearanceItems.Items.Insert(0, new ListItem("--- Select Item ---", "0"));
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("IT", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }


        protected void uploadFile()
        {

            if (updLogo != null)
            {
                Random rand = new Random((int)DateTime.Now.Ticks);
                int randnum = 0;

                string fn = "";
                string exten = "";

                string destDir = Server.MapPath("~/assets/Attachments/SeperationDeductions/");
                randnum = rand.Next(1, 100000);
                fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }

                string fname = Path.GetFileName(updLogo.PostedFile.FileName);
                exten = Path.GetExtension(updLogo.PostedFile.FileName);
                updLogo.PostedFile.SaveAs(destDir + fn + exten);

                Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/Attachments/SeperationDeductions/" + fn + exten;
            }



            if (updLogo != null)
            {
                //foreach (HttpPostedFile file in updAttachments.PostedFile)
                //{

                HttpPostedFile file = updLogo.PostedFile;
                Random rand = new Random((int)DateTime.Now.Ticks);
                int randnum = 0;

                string fn = "";
                string exten = "";
                string destDir = Server.MapPath("~/assets/Attachments/SeperationDeductions/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
                randnum = rand.Next(1, 100000);
                fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }
                string fname = Path.GetFileName(file.FileName);
                exten = Path.GetExtension(file.FileName);
                file.SaveAs(destDir + fn + exten);

                Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/Attachments/SeperationDeductions/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
            }



        }

        protected void btnAddExpDetails_ServerClick(object sender, EventArgs e)
        {
            uploadFile();
            DataRow dr = dtExpDetails.NewRow();
            dr[0] = ExpDetailsSrNo.ToString();
            dr[1] = ddlClearanceItems.SelectedItem.Text;
            dr[2] = txtAssetno.Value;
            dr[3] = txtDescriptionDetail.Value;
            dr[4] = txtAmountDetail.Value;
            dr[5] = Attachment;
            dr[6] = ddlClearanceItems.SelectedValue;



            TotalAmount.InnerText = (double.Parse(TotalAmount.InnerText) + double.Parse(txtAmountDetail.Value)).ToString();
            dtExpDetails.Rows.Add(dr);
            dtExpDetails.AcceptChanges();
            ExpDetailsSrNo += 1;
            BindExpDetailsTable();

            txtDescriptionDetail.Value = "";
            txtAmountDetail.Value = "";
            txtAssetno.Value = "";
            ddlClearanceItems.SelectedValue = "0";
            Attachment = "";
            ddlEmployee.SelectedValue = SelectedEmployee.ToString();
        }

        private bool CheckOtherDetailsItemsCount()
        {
            //bool isValid = true;
            dtExpDetails = (DataTable)ViewState["dtExpDetails"] as DataTable;
            //if (!(dtExpDetails != null && dtExpDetails.Rows.Count > 0))
            //{
            //    isValid = false;
            //}
            return (dtExpDetails != null && dtExpDetails.Rows.Count > 0);
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                if (CheckOtherDetailsItemsCount())
                {
                    dtExpDetails = (DataTable)ViewState["dtExpDetails"] as DataTable;

                    if (dtExpDetails != null)
                    {
                        if (dtExpDetails.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
                            {
                                objDB.FilePath = dtExpDetails.Rows[i]["FilePath"].ToString();
                                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                                objDB.EmployeeID = SelectedEmployee;
                                objDB.ItemID = Convert.ToInt32(dtExpDetails.Rows[i]["ClearanceItemID"].ToString());
                                objDB.Assetno = dtExpDetails.Rows[i]["AssetNumber"].ToString();
                                objDB.NetAmount = float.Parse(dtExpDetails.Rows[i]["Amount"].ToString());
                                objDB.Remarks = dtExpDetails.Rows[i]["Remarks"].ToString();
                                objDB.Department = "IT";
                                //objDB.FilePath = dtItemDetails.Rows[i]["FilePath"].ToString();
                                objDB.CreatedBy = Session["UserName"].ToString();
                                res = objDB.AddSeperationDeductionItem();
                            }
                        }
                    }
                }

                if (res == "New Item Added Successfully" || res == "Item Updated Successfully")
                {

                    objDB.SeperationDetailsID = SeperationId;
                    objDB.ApprovedBy = Session["UserName"].ToString();
                    objDB.Department = "IT";
                    objDB.Comments = txtNotes.Value;
                    objDB.Deductions = float.Parse(TotalAmount.InnerText);

                    objDB.ApproveEmployeeSeperationDetailsByDepartment();
                    ViewState["ExpDetailsSrNo"] = null;
                    ViewState["dtExpDetails"] = null;

                    btnUpdateDiv.Visible = false;
                    btnAddDiv.Visible = true;
                    dtExpDetails = new DataTable();
                    dtExpDetails = createExpDetails();
                    BindExpDetailsTable();
                    Common.addlog("Update", "IT", "Seperation Deduction Item Details ID \"" + SeperationId + "\" Updated", "SeperationDeductionItemDetails");

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


        private void clearFields()
        {
            //txtCode.Value = "";
            txtDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");
            txtDescriptionDetail.Value = txtAmountDetail.Value = txtNotes.Value = "";
            TotalAmount.InnerText = "00.00";
            txtAssetno.Value = "";
            ddlEmployee.SelectedValue = "0";
            //txtCode.Value = objDB.GenerateOtherExpenseCode();
            btnUpdateDiv.Visible = false;
            btnAddDiv.Visible = true;
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "SeperationDeductionItemDetails", "SeperationId", HttpContext.Current.Items["SeperationId"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "IT", "Seperation Deduction Item Details of ID\"" + HttpContext.Current.Items["SeperationId"].ToString() + "\" Status Changed", "SeperationDeductionItemDetails", SeperationId);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void lnkEdit_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                if (dtExpDetails.Rows[i][0].ToString() == delSr)
                {
                    ddlClearanceItems.SelectedValue = dtExpDetails.Rows[i]["ClearanceItemID"].ToString();
                    txtDescriptionDetail.Value = dtExpDetails.Rows[i]["Remarks"].ToString();
                    txtAssetno.Value = dtExpDetails.Rows[i]["AssetNumber"].ToString();
                    txtAmountDetail.Value = dtExpDetails.Rows[i]["Amount"].ToString();
                    imgLogo.Src = dtExpDetails.Rows[i]["FilePath"].ToString();
                    hdnExpenseSrNO.Value = delSr;
                    btnUpdateDiv.Visible = true;
                    btnAddDiv.Visible = false;
                }
            }
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                dtExpDetails.Rows[i].SetField(0, i + 1);
                dtExpDetails.AcceptChanges();
            }

            ExpDetailsSrNo = dtExpDetails.Rows.Count + 1;
            BindExpDetailsTable();
        }


        protected void btnUpdateExpDetails_ServerClick(object sender, EventArgs e)
        {
            TotalAmount.InnerText = "0.0";
            uploadFile();
            string delSr = hdnExpenseSrNO.Value;
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                if (dtExpDetails.Rows[i][0].ToString() == delSr)
                {

                    dtExpDetails.Rows[i]["ItemName"] = ddlClearanceItems.SelectedValue;
                    dtExpDetails.Rows[i]["Remarks"] = txtDescriptionDetail.Value;
                    dtExpDetails.Rows[i]["AssetNumber"] = txtAssetno.Value;
                    dtExpDetails.Rows[i]["Amount"] = txtAmountDetail.Value;
                    dtExpDetails.Rows[i]["FilePath"] = Attachment;
                    dtExpDetails.Rows[i]["ItemName"] = ddlClearanceItems.SelectedItem.Text;
                    dtExpDetails.Rows[i]["ClearanceItemID"] = ddlClearanceItems.SelectedValue;
                    dtExpDetails.AcceptChanges();
                    TotalAmount.InnerText = (double.Parse(TotalAmount.InnerText) + double.Parse(txtAmountDetail.Value)).ToString();

                    hdnExpenseSrNO.Value = "0";

                    txtDescriptionDetail.Value = "";
                    txtAmountDetail.Value = "";
                    ddlClearanceItems.SelectedValue = "0";
                    Attachment = "";

                    btnUpdateDiv.Visible = false;
                    btnAddDiv.Visible = true;
                    BindExpDetailsTable();

                    break;
                }
            }
            for (int i = 0; i < dtExpDetails.Rows.Count; i++)
            {
                dtExpDetails.Rows[i].SetField(0, i + 1);
                dtExpDetails.AcceptChanges();
            }
        }

        protected void btnCancelExpDetails_ServerClick1(object sender, EventArgs e)
        {
            clearFields();
            btnUpdateDiv.Visible = false;
            btnAddDiv.Visible = true;
        }
    }
}