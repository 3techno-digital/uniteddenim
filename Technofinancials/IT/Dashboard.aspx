﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Technofinancials.IT.Dashboard" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/IT/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        button.btn.btn-default.btn-primary{
    padding: 8px 25px;
}
        .widget-body.last div {
    height: 360px;
    overflow-y: scroll;
    width: 100%;
    padding-right: 10px;
}
        .panel {
    border: none;
    box-shadow: none;
    border-radius: 20px;
    box-shadow: 0 2px 3px 0px #ccc;
    background: #e8e8e8;
}

        .to-do-list li .todo-check input[type=checkbox] {
    cursor: pointer;
    position: absolute;
    width: 20px;
    height: 20px;
    top: 5px;
    left: 0px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    border: #ccc 1px solid;
    margin-top: 0px;
}
        .todo-actionlist {
    position: absolute;
    right: 15px;
    top: 13px;
}
        .todo-actionlist span i {
    height: 24px;
    width: 24px;
    display: inline-block;
    text-align: center;
    line-height: 24px;
    color: #ccc;
    cursor: pointer;
    padding-top: 3px;
    padding-left: 14px;
}
                .panel-body p {
                line-height: 2.2;
        }
                .todo-check {
    width: 20px;
    position: relative;
    margin-right: 10px;
    margin-left: 10px;
}
        .to-do-list li {
    background: #f3f3f3;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    position: relative;
    padding: 13px;
    margin-bottom: 5px;
    cursor: pointer;
    list-style: none;
    margin-right: 10px;
    height: 53px;
}
        .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
    background-color: #213e7d;
    border-color: #213e7d;
    color: #FFFFFF;
}
        .btn-todo-select button i, .btn-add-task button i {
    padding-right: 10px;
}
       .widget-body.last div ::-webkit-scrollbar-track {
box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
}
       .widget-body.last div ::-webkit-scrollbar {
    width: 10px;
}
       .widget-body.last div::-webkit-scrollbar-thumb {
    background: #003780;
    border-radius: 5px;
}
      .slimScrollBar ::-webkit-scrollbar-thumb {
    background: #003780;
    border-radius: 5px;
}
      .slimScrollBar  ::-webkit-scrollbar {
    width: 10px;
}
     .slimScrollBar  ::-webkit-scrollbar-track {
box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
}
        div#container {
            height: 395px;
            padding-top: 10px;
        }
         .search{
             padding:0px 5px !important;
         }
        .anychart-credits {
            display: none;
        }
        /*#pieChartAm {
                width: 100%;
                height: 500px;
            }

            #manChartAm4 {
                width: 100%;
                height: 500px;
            }

            #columnChartImages {
                width: 100%;
                height: 500px;
            }

            #solidGuage {
                width: 100%;
                height: 500px;
            }

            #chart3D {
                width: 100%;
                height: 370px;
            }

            #punchCard {
                width: 100%;
                height: 500px;
            }

            #scatterSingleAxis {
                width: 100%;
                height: 500px;
            }*/
        .search {
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -ms-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
    border: 1px solid #fff;
    box-shadow: none;
    border: 1px solid #f6f6f6 !important;
    background: #f6f6f6 !important;
    padding: 0 5px 0 30px;
    color: #e1e1e1;
    border-radius: 100px;
    -webkit-border-radius: 100px;
    height: 35px !important;
}
        .m-l-10 h3 {
            text-align: left;
            margin-top: 0px;
        }


        #chartdiv {
            width: 100%;
            height: 370px;
        }

        span#lblCol3 {
            word-break: break-word;
        }

        .orange {
            background: #fa8564 !important;
        }

        .pink {
            background: #a48ad4 !important;
        }

        .mini-stat .green {
            background: #aec785 !important;
        }

        .mini-stat-icon.yellow {
            background-color: #f9c851;
        }

        .D_NONE {
            display: none !important;
        }

        #backButton {
            position: absolute;
            top: 35px;
            right: 35px;
            cursor: pointer;
        }

        .invisible {
            display: none;
        }

        .form-group h4, label {
            margin: 20px 0 10px;
        }

        #chartContainerNew01 .canvasjs-chart-toolbar,
        #chartContainerBar02 .canvasjs-chart-toolbar {
            display: none;
        }

        .mini-stat-icon i {
            font-size: 25px;
        }

        .gray {
            background: #dee2e6;
            border-radius: 20px;
            margin: 0 auto;
            text-align: center;
            padding: 5px;
            margin-top: 20px;
        }

        .chart_heading {
            text-align: center;
            margin: 0 0 13px;
            padding-top: 20px;
            font-size: 20px;
            font-weight: 700;
            color: #333;
        }

        .removetrial {
            background: #dee2e6;
            height: 20px;
            width: 95px;
            border-radius: 9px;
            position: absolute;
            left: 12px;
            bottom: 16px;
            z-index: 1;
        }

        .removetrial2 {
            background: #dee2e6;
            height: 20px;
            width: 95px;
            border-radius: 9px;
            position: absolute;
            bottom: 5px;
            left: 19px;
            z-index: 1;
        }


        a.canvasjs-chart-credit {
            display: none;
        }

        .table {
            color: black;
            margin-bottom: 0;
        }

        .card {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-clip: border-box;
            border-radius: .25rem;
        }

        .align-self-center {
            -ms-flex-item-align: center !important;
            align-self: center !important;
        }

        .flex-row {
            -ms-flex-direction: row !important;
            flex-direction: row !important;
        }

        .round {
            line-height: 60px;
            color: #5b6be8;
            width: 60px;
            height: 60px;
            font-size: 26px;
            display: inline-block;
            font-weight: 400;
            border: 3px solid #f8f8fe;
            text-align: center;
            border-radius: 50%;
            background: #e1e4fb;
        }

        .mini-stat-icon.purple {
            background-color: #f4b9b9;
        }

        .m-l-10 h5 {
            font-size: 24px;
            color: #767676 !important;
        }

        .text-muted {
            opacity: 1;
            text-align: center;
            margin-bottom: 0px;
            font-size: 11px;
            padding-top: 2px;
            color: #767676 !important;
        }
        .nav-item .user {
    color: #737373 !IMPORTANT;
    border: 2px solid #737373;
    padding: 3px 8px !IMPORTANT;
    border-radius: 38px;
    margin: 6px 5px 0;
    height: 32px !important;
}
        .mini-stat {
            background: #fff;
            padding: 17px;
            box-shadow: 0 2px 3px 0px #ccc;
            margin-bottom: 20px;
                border-radius: 10px;
        }

        .m-l-10 h5, .m-l-10 p {
            text-align: left;
        }

        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1rem;
        }

        .removetrialMale {
            /*background: #e8e8e8;*/
            background: #ffffff;
            height: 40px;
            width: 95px;
            border-radius: 9px;
            position: absolute;
            left: 22px;
            bottom: 34px;
            z-index: 1;
            margin: 4px !important;
        }

        .map_radius .widget-body {
            padding: 0 1rem;
        }

        .table > thead > tr > th {
            /*background-color: #e8e8e8;*/
            background-color: #ffffff;
            border-bottom: 2px solid #000000 !important;
            color: #000;
        }

        .canvasjs-chart-toolbar {
            display: none;
        }
        /*.table tr:nth-child(odd) {
                background: #fff;
            }

            .table tr:nth-child(even) {
                background: #f2f2f2;
            }*/
        .anounceTab th:nth-child(1) {
            width: 30px;
            text-align: center;
        }

        .anounceTab th:nth-child(2) {
            width: 160px;
        }

        .anounceTab th:nth-child(3) {
            width: 200px;
        }

        .anounceTab {
            height: 370px;
            padding-right: 5px;
        }



        #style-1::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        #style-1::-webkit-scrollbar {
            width: 7px;
            background-color: #F5F5F5;
        }

        #style-1::-webkit-scrollbar-thumb {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #003780;
        }

        .slimScrollBar {
box-shadow: inset 0 0 5px grey;
            width: 10px !important;
        }

        .header-fav-tab {
            display: none !important;
        }

        .menubar-scroll-inner.tf-sidebar-menu-items {
            overflow: scroll;
            overflow-x: hidden;
            overflow-y: scroll;
            height: 85vh !important;
        }

        /*body::-webkit-scrollbar {
                width: 1em;
            }

            body::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            }

            body::-webkit-scrollbar-thumb {
                background-color: darkgrey;
                outline: 1px solid slategrey;
            }*/

        .slimScrollDiv {
            height: 85vh !important;
        }

        .table > thead:first-child > tr:first-child > th {
            border-top-style: none !important;
        }

        g[aria-labelledby="id-479-title"] {
            display: none;
        }

        g[aria-labelledby="id-318-title"] {
            display: none;
        }

        g[aria-labelledby="id-68-title"] {
            display: none;
        }

        g[aria-labelledby="id-214-title"] {
            display: none;
        }
        .widget {
    box-shadow: 0 2px 3px 0px #ccc;
    background: #e8e8e8;
    border-radius: 20px;
}
.panel-heading {
    border-bottom: 1px solid #e8e8e8;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
    text-align: center;
    color: #212529 !important;
    margin-top: 0px;
    font-size: 1.25rem;
    padding-top: 20px !important;
    font-weight: 700;
    padding: 15px;
}
        @media only screen and (max-width: 1599px) and (min-width: 1201px) {
            .mini-stat {
                    padding: 10px 11px;
    border-radius: 10px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
                font-size: 9px;
            }

            .mini-stat-icon.purple img {
                width: 50%;
            }

            .mini-stat-icon {
                width: 40px !important;
                height: 40px !important;
                line-height: 33px !important;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .mini-stat {
                padding: 10px 8px;
            }
                        canvas#DepartmentWiseCheckInChart{
                height: 270px;
            }
            .mini-stat-icon.purple img {
                width: 50%;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
                font-size: 10px;
            }

            .mini-stat-icon {
                width: 40px !important;
                height: 40px !important;
                line-height: 33px !important;
            }

                .mini-stat-icon i {
                    font-size: 19px;
                }
        }

        @media only screen and (max-width: 1024px) and (min-width: 768px) {
            .mini-stat {
                padding: 10px 11px;
            }
            canvas#DepartmentWiseCheckInChart{
                height: 225px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            .mini-stat {
                padding: 10px 11px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            .mini-stat {
                padding: 10px 11px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }

        @media only screen and (max-width: 1439px) and (min-width: 1201px) {
            td {
                max-width: 50px !important;
            }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            td {
                max-width: 50px !important;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            td {
                max-width: 50px !important;
            }

            .widget.map_radius.new {
                height: 470px !important;
            }
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            td {
                max-width: 50px !important;
            }
        }
        /*@media only screen and (max-width: 991px) and (min-width: 768px){
                td: before{*/
        /* Now like a table header */
        /*position: absolute;*/
        /* Top/left values mimic padding */
        /*top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
            }
            }*/
        .mini-stat-icon {
            width: 50px;
            height: 50px;
            display: inline-block;
            line-height: 45px;
            text-align: center;
            font-size: 30px;
            background: #eee;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            float: left;
            margin-right: 10px;
            color: #fff;
        }

        .tar {
            background: #1fb5ac !important;
        }

        .mini-stat-info {
            font-size: 11px;
            padding-top: 2px;
            color: #767676 !important;
            font-weight: 700;
        }
        .heading {
    color: #212529 !important;
    margin-top: 0px;
    font-size: 1.25rem;
    padding-top: 0px !important;
    padding-left: 0px !important;
    font-weight: 700;
    padding: 15px;
    text-transform: capitalize;
}
.btn-primary {
    color: #fff;
    background-color: #003780;
    border-color: #003780;
}
        .map_radius {
            border-radius: 0px;
            box-shadow: 0 2px 3px 0px #ccc;
        }

        .panel-heading {

            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
        }

        .pr-0 {
            padding-right: 0px !important;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <h1 class="m-0 text-dark">Rosters Management</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4">
                            <div style="text-align: right;">
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>


            <div class="wrap">
                <section class="app-content">

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <div class="row">
                                <!-- small box -->
                                <div class="col-lg-3 col-md-3 col-sm-6 col-6 pr-0">
                                    <!-- small box -->
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/shifts"); %>">
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon tar">
                                                <img src="/assets/images/shift.png"/>
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="TotShifts">0.00</h3>
                                                        <p class="mb-0 text-muted">No Of Shifts</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                               
                                <div class="col-lg-3 col-md-3 col-sm-6 col-6 pr-0">
                                    <!-- small box -->
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/attendance-sheet-flagwise-Checkin"); %>">
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon pink">
                                                <i class="fa fa-sign-in" aria-hidden="true"></i>
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="TotCheckIn">0.00</h3>
                                                        <p class="mb-0 text-muted">Check In</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-6 pr-0">
                                    <!-- small box -->
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/attendance-sheet-flagwise-Checkout"); %>">
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon orange">
                                                <i class="fa fa-sign-out" aria-hidden="true"></i>
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="TotCheckOut">0.00</h3>
                                                        <p class="mb-0 text-muted">Check Out</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-6 pr-0">
                                    <!-- small box -->
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/view/attendance-sheet-flagwise-Absent"); %>">
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon green">
                                               <i class="fa fa-user-times" aria-hidden="true"></i>
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="TotAbsents">0.00</h3>
                                                        <p class="mb-0 text-muted">Absents</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!-- ./col -->
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="widget">
                                        <header class="panel-heading">
                                            <div class="col-sm-12">
                                                <div class="heading">
                                                    Shift wise Employees
                                                </div>
                                            </div>
                                        </header>
                                        <!-- .widget-header -->
                                        <div class="widget-body">
                                            <canvas id="EmployeeWiseShiftChart" width="500" height="350"></canvas>
                                        </div>

                                        <!-- .widget-body -->
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="widget">
                                        <header class="panel-heading">
                                            <div class="col-sm-12">
                                                <div class="heading">
                                                    Department Wise Check Ins
                                                </div>
                                            </div>
                                        </header>
                                        <!-- .widget-header -->
                                        <div class="widget-body">
                                            <canvas id="DepartmentWiseCheckInChart" width="500" height="350"></canvas>
                                        </div>
                                        <!-- .widget-body -->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-md-6 col-sm-12">
                               <div class="widget">
                                         <header class="panel-heading">
                                            <div class="col-sm-12">
                                                <div class="heading">
                                                    last Change In Shift
                                                </div>
                                            </div>
                                        </header>
                                <!-- .widget-header -->
                                <div class="widget-body last">
                                    <asp:GridView ID="gvLastChangeInShifts" runat="server" CssClass="table table-bordered   dataTable no-footer"  runat="server" AutoGenerateColumns="false">
                                        <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Shift Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("ShiftName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Change By">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("ModifiedBy") + "<br/>" + (Eval("ModifiedDate"))  %>'></asp:Label>
                                          <%--  <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("ModifiedBy") + "<br/>" + ((DateTime)Eval("ModifiedDate")).ToString("dd-MMM-yyyy hh:mm:ss tt")  %>'></asp:Label>
                                        --%>
                                        
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                    </asp:GridView>
                                </div>
                                <!-- .widget-body -->
                            </div>
                                     </div>
                                  <div class="col-md-6 col-sm-12">
                                                        <section class="panel">
                                <header class="panel-heading">
                                    To Do List <span class="tools pull-right"></span>
                                </header>
                                <div class="panel-body">
                                    <div class="slimScrollDiv-on" style="position: relative; overflow: hidden; width: auto; height: 315px;">
                                        <ul class="to-do-list ui-sortable" id="sortable-todo" style="overflow: hidden; height: 300px; width: auto; margin-bottom: 10px; overflow: auto; outline: none;">
                                        </ul>
                                        <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; "></div>
                                        <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                    <div class="todo-action-bar">
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-8 col-xs-8 col-sm-7 todo-search-wrap pr-0">
                                                <input type="text" id="txtTodoDescription" class="form-control search" style="color: #767676;" placeholder=" To Do Task">
                                            </div>
                                            <div class="col-xl-8 col-lg-4 col-xs-4 col-sm-5 btn-add-task" style="text-align:right;">
                                                <button type="button" onclick="AddToDoListItem();" class="btn btn-default btn-primary">Add Task</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                </section>
            </div>
            <!-- #dash-content -->

            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>


        <script>
            sap.ui.controller("myController", {
                onInit: function () {

                    var jsonData = {
                        "cars": [{
                            Model: "Fiat",
                            Value: "500"
                        }, {
                            Model: "Porsche",
                            Value: "911"
                        }, {
                            Model: "Peugeot",
                            Value: "504"
                        }, {
                            Model: "MyCar",
                            Value: "100"
                        }]
                    };
                    var jsonModel = new sap.ui.model.json.JSONModel(jsonData);
                    this.getView().setModel(jsonModel);


                }

            });

            sap.ui.view({
                viewContent: jQuery('#myXml').html(),
                type: sap.ui.core.mvc.ViewType.XML
            }).placeAt("content");

        </script>
        <script>
            anychart.onDocumentReady(function () {

                // set the data
                var data = {
                    header: ["Name", "Death toll"],
                    rows: [
                        ["CEO", 1500],
                        ["HR", 87000],
                        ["Project Manager", 175000],
                        ["Sernior Developer", 10000],
                        ["Tian Shan (1976)", 242000],
                        ["Armenia (1988)", 25000],
                        ["Iran (1990)", 50000],
                        ["Project Manager", 175000],
                        ["Sernior Developer", 10000],
                        ["Tian Shan (1976)", 242000],
                        ["Armenia (1988)", 25000],
                        ["Iran (1990)", 50000]
                    ]
                };

                // create the chart
                var chart = anychart.bar();

                // add data
                chart.data(data);

                // set the chart title
                chart.title("");


                // draw
                chart.container("container");
                chart.draw();
            });
        </script>
        <script>
            am4core.ready(function () {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart instance
                var chart = am4core.create("pieChartAm", am4charts.RadarChart);
                chart.scrollbarX = new am4core.Scrollbar();

                var data = [];

                for (var i = 0; i < 30; i++) {
                    data.push({ category: i, value: Math.round(Math.random() * 100) });
                }

                chart.data = data;
                chart.radius = am4core.percent(100);
                chart.innerRadius = am4core.percent(50);

                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.grid.template.location = 0;
                categoryAxis.renderer.minGridDistance = 30;
                categoryAxis.tooltip.disabled = true;
                categoryAxis.renderer.minHeight = 110;
                categoryAxis.renderer.grid.template.disabled = true;
                //categoryAxis.renderer.labels.template.disabled = true;
                let labelTemplate = categoryAxis.renderer.labels.template;
                labelTemplate.radius = am4core.percent(-60);
                labelTemplate.location = 0.5;
                labelTemplate.relativeRotation = 90;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.grid.template.disabled = true;
                valueAxis.renderer.labels.template.disabled = true;
                valueAxis.tooltip.disabled = true;

                // Create series
                var series = chart.series.push(new am4charts.RadarColumnSeries());
                series.sequencedInterpolation = true;
                series.dataFields.valueY = "value";
                series.dataFields.categoryX = "category";
                series.columns.template.strokeWidth = 0;
                series.tooltipText = "{valueY}";
                series.columns.template.radarColumn.cornerRadius = 10;
                series.columns.template.radarColumn.innerCornerRadius = 0;

                series.tooltip.pointerOrientation = "vertical";

                // on hover, make corner radiuses bigger
                let hoverState = series.columns.template.radarColumn.states.create("hover");
                hoverState.properties.cornerRadius = 0;
                hoverState.properties.fillOpacity = 1;


                series.columns.template.adapter.add("fill", function (fill, target) {
                    return chart.colors.getIndex(target.dataItem.in


x);
                })

                // Cursor
                chart.cursor = new am4charts.RadarCursor();
                chart.cursor.innerRadius = am4core.percent(50);
                chart.cursor.lineY.disabled = true;

            }); // end am4core.ready()
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>
            var data = {
                datasets: [{
                    data: [300, 100],
                    backgroundColor: [
                        "#77a033",
                        "#207cba",
                    ]
                }],
                labels: [
                    "Female",
                    "Male",
                ]
            };

            $(document).ready(
                function () {
                    var canvas = document.getElementById("myChart");
                    var ctx = canvas.getContext("2d");
                    var myNewChart = new Chart(ctx, {
                        type: 'pie',
                        data: data
                    });

                    canvas.onclick = function (evt) {
                        var activePoints = myNewChart.getElementsAtEvent(evt);
                        if (activePoints[0]) {
                            var chartData = activePoints[0]['_chart'].config.data;
                            var idx = activePoints[0]['_index'];

                            var label = chartData.labels[idx];
                            var value = chartData.datasets[0].data[idx];

                            var url = "http://example.com/?label=" + label + "&value=" + value;
                            console.log(url);
                            alert(url);
                        }
                    };
                }
            );
        </script>
        <script>
            function AddToDoListItem() {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/AddToDoListItem',
                    data: '{Description: ' + JSON.stringify($('#txtTodoDescription').val()) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        GetToDoListByUserID();
                        $('#txtTodoDescription').val('');
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }


            function UpdateToDoListItem() {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/UpdateToDoListItem',
                    data: '{ToDoListID: ' + JSON.stringify(id) + ',Description: ' + JSON.stringify(id) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        GetToDoListByUserID();   
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }

            function DeleteToDoListItem(id) {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/DeleteToDoListItem',
                    data: '{ToDoListID: ' + JSON.stringify(id) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        GetToDoListByUserID();
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }

            function UpdateToDoListItemStatus(id) {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/UpdateToDoListItemStatus',
                    data: '{ToDoListID: ' + JSON.stringify(id) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        GetToDoListByUserID();
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }


            function GetToDoListByUserID() {
                $.ajax({
                    type: "POST",
                    url: '/business/services/EssService.asmx/GetToDoListByUserID',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var data = response.d;
                        var lstTodo = $("#sortable-todo");
                        lstTodo.empty();

                        $.each(data, function (key, data) {
                            if (data.Checked == "checked") {
                                lstTodo.append("<li class='clearfix '><div class='todo-check pull-left'><input type='checkbox' value='None' " + data.Checked + " onchange='UpdateToDoListItemStatus(" + data.ToDoListID + ")' id='todo-check" + data.ToDoListID + "'><label for='todo-check2" + data.ToDoListID + "'></label></div><p class='todo-title line-through'>" + data.Description + "</p><div class='todo-actionlist pull-right clearfix'><span id='close' onclick='DeleteToDoListItem(" + data.ToDoListID + ")' class='todo-remove'><i class='fa fa-times' aria-hidden=true></i></span></div></li>");
                            } else {
                                lstTodo.append("<li class='clearfix'><div class='todo-check pull-left'><input type='checkbox' value='None' " + data.Checked + " onchange='UpdateToDoListItemStatus(" + data.ToDoListID + ")' id='todo-check" + data.ToDoListID + "'><label for='todo-check2" + data.ToDoListID + "'></label></div><p class='todo-title'>" + data.Description + "</p><div class='todo-actionlist pull-right clearfix'><span id='close' onclick='DeleteToDoListItem(" + data.ToDoListID + ")' class='todo-remove'><i class='fa fa-times' aria-hidden=true></i></span></div></li>");
                            }
                        })

                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
                });
            }

        </script>
        <asp:Literal ID="ltrDesignationWiseChart" runat="server"></asp:Literal>
                   <asp:Literal ID="ltrEmployeeWiseShift" runat="server"></asp:Literal>
        <asp:Literal ID="ltrDepartmentWiseCheckIn" runat="server"></asp:Literal>
    </form>
</body>
</html>
      