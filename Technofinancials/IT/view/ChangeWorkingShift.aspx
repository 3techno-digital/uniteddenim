﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangeWorkingShift.aspx.cs" Inherits="Technofinancials.IT.view.ChangeWorkingShift" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/IT/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>
		div#gvCurrency_wrapper {
			margin-bottom: 20%;
		}

		button#Button1 {
			margin-top: 13px;
			padding: 4px 14px !important;
		}
	</style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">
			<input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
			<div class="wrap">
				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
								<h1 class="m-0 text-dark">Change Shifts</h1>
							</div>
							<!-- /.col -->
							<div class="col-sm-4">
								<div style="text-align: right;">
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/IT/manage/ChangeWorkingShift/add-new-ChangeWorkingShift"); %>" class="AD_btn">Add</a>
								</div>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.container-fluid -->
				</div>

				<section class="app-content">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Department Name</h4>
										<asp:DropDownList ID="ddlDepartment" runat="server" class="form-control select2" onchange="GetDesignationsByDepartmentIdAndCompanyId()" data-plugin="select2" AutoPostBack="false">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Designation Name</h4>
										<asp:DropDownList ID="ddlDesignation" runat="server" class="form-control select2" onchange="GetEmployeesByDesignationIdAndDepartmentId()" data-plugin="select2" AutoPostBack="false">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>WDID</h4>
										<asp:TextBox ID="txtKTId" runat="server" class="form-control" data-plugin="select2">
										</asp:TextBox>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="clear-fix">&nbsp;</div>
									<div>
										<button class="AD_btn_inn" id="Button1" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
									</div>
								</div>

							</div>
						</div>

						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Employee Name</h4>
										<asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Location</h4>
										<asp:DropDownList ID="ddlLocation" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
										</asp:DropDownList>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row ">
						<div class="col-sm-12">
							<div class="tab-content ">
								<div class="tab-pane active row">
									<div class="col-sm-12 gv-overflow-scrool">
										<asp:GridView ID="gvRoasters" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
											<Columns>
												<asp:TemplateField HeaderText="Sr.No.">
													<ItemTemplate>
														<%#Container.DataItemIndex+1 %>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Employee Code">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol1" Text='<%# Eval("WorkDayID") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>

												<asp:TemplateField HeaderText="Employee">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol1" Text='<%# Eval("EmployeeName") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Cost Center">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("CostCenterName")  %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Designation">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("DesgTitle")  %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Department">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("DeptName")  %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Shift Name">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("ShiftName")  %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="View">
													<ItemTemplate>
														<a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/IT/manage/ChangeWorkingShift/change-shift-by-employee-id-" + Eval("EmployeeID") %>" class="AD_stock">View </a>
													</ItemTemplate>
												</asp:TemplateField>
											</Columns>
										</asp:GridView>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<!-- #dash-content -->
			</div>
			<div class="clear-fix">&nbsp;</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>
		<script src="/business/scripts/ViewDesignations.js"></script>
	</form>
</body>
</html>
