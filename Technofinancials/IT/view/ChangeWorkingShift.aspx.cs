﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.IT.view
{
	public partial class ChangeWorkingShift : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                BindEmployee();
                GetData();
            }
        }

        protected void BindEmployee()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDepartment.DataTextField = "DeptName";
            ddlDepartment.DataValueField = "DeptID";
            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

            ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
            ddlDesignation.DataTextField = "DesgTitle";
            ddlDesignation.DataValueField = "DesgID";
            ddlDesignation.DataBind();
            ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

            ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg, ddlDepartment.SelectedValue, ddlDesignation.SelectedValue);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

            ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
            ddlLocation.DataTextField = "NAME";
            ddlLocation.DataValueField = "NAME";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("ALL", "0"));
        }

        private void GetData(int departmentId = 0, int designationId = 0, int employeeId = 0, string KTId = "")
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.EmployeeID = employeeId;
            objDB.DeptID = departmentId;
            objDB.DesgID = designationId;
            objDB.KTID = KTId;
            objDB.Location = ddlLocation.SelectedValue;

            dt = objDB.GetEmployees_CurrentShifts(ref errorMsg);
            gvRoasters.DataSource = dt;
            gvRoasters.DataBind();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvRoasters.UseAccessibleHeader = true;
                    gvRoasters.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

            Common.addlog("ViewAll", "IT", "All Change Work Shift Viewed", "WorkingShiftChange");
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("IT", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            GetData(Convert.ToInt32(ddlDepartment.SelectedValue), Convert.ToInt32(ddlDesignation.SelectedValue), Convert.ToInt32(ddlEmployee.SelectedValue), txtKTId.Text);
        }
    }
}