﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.IT.view
{
	public partial class EmployeeClearance : System.Web.UI.Page
	{
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                lnkApprove.Visible = true; 
                divAlertMsg.Visible = false;                 
                BindDropDowns();
                GetData();

            }
        }
        private void BindDropDowns()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                ddlDepartment.DataTextField = "DeptName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

                ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
                ddlDesignation.DataTextField = "DesgTitle";
                ddlDesignation.DataValueField = "DesgID";
                ddlDesignation.DataBind();
                ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

                ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

                ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
                ddlLocation.DataTextField = "NAME";
                ddlLocation.DataValueField = "NAME";
                ddlLocation.DataBind();
                ddlLocation.Items.Insert(0, new ListItem("ALL", "0"));

            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void GetData()
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            
            if (string.IsNullOrEmpty(txtFromDate.Text) || string.IsNullOrEmpty(txtFromDate.Text))
            {
                DateTime date = DateTime.Now;
				var firstDay = new DateTime(date.Year, date.Month, 1);
				txtFromDate.Text = (firstDay).ToString();
                txtToDate.Text = (firstDay.AddMonths(1).AddDays(-1)).ToString();
            }
            
            objDB.FromDate = txtFromDate.Text;
            objDB.ToDate = txtToDate.Text;
            objDB.DeptID = Convert.ToInt16(ddlDepartment.SelectedValue);
            objDB.DesgID = Convert.ToInt16(ddlDesignation.SelectedValue);
            objDB.EmployeeID = Convert.ToInt16(ddlEmployee.SelectedValue);
            //objDB.WDID = txtKTId.Text;
            objDB.Location = ddlLocation.SelectedValue;
            objDB.Department = "IT";
            objDB.isApproved = ddlIsApproved.SelectedValue == "1";

            dt = objDB.GetAllEmployeeSeperationDetailsByDepartment(ref errorMsg);
            
            gvEmployees.DataSource = dt;
            gvEmployees.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvEmployees.UseAccessibleHeader = true;
                    gvEmployees.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {

                    divAlertMsg.Visible = true;
                    pAlertMsg.InnerHtml = "No Records Found";
                }
            }
            else
            {
                lnkApprove.Visible = false;

               
                divAlertMsg.Visible = true;
                
                pAlertMsg.InnerHtml = "No Records Found"; 
                

            }


            Common.addlog("View", "IT", "All Seperation Deatils", "EmployeeSeperationDetails");
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("IT", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                divAlertMsg.Visible = false;

                if (string.IsNullOrEmpty(txtFromDate.Text) || string.IsNullOrEmpty(txtToDate.Text))
                {
                    if (DateTime.Parse(txtFromDate.Text) > DateTime.Parse(txtToDate.Text))
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "From Date OR To Date shuold not be empty";
                        return;
                    }
                }

                if (Convert.ToDateTime(txtFromDate.Text) > Convert.ToDateTime(txtToDate.Text))
                {
                    if (DateTime.Parse(txtFromDate.Text) > DateTime.Parse(txtToDate.Text))
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "From Date shuold not be greater than To Date";
                        return;
                    }
                }

                GetData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;

            }
        }

        protected void checkAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chckheader = (CheckBox)gvEmployees.HeaderRow.FindControl("checkAll");

            foreach (GridViewRow row in gvEmployees.Rows)
            {
                if (((Label)row.FindControl("lblFinanceStatus")).Text != "Approved")
                {
                    ((CheckBox)row.FindControl("check")).Checked = chckheader.Checked;
                }
            }
        }

        protected void btnApprove_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();

            if (string.IsNullOrEmpty(txtComments.InnerText))
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "Please Enter CLearance Approval Comments";
                return;
            }

            divAlertMsg.Visible = false;
            lnkApprove.Visible = false;
            string alertMsg = "";
            int recordsCount = 0;

            foreach (GridViewRow gvr in gvEmployees.Rows)
            {
                if (((CheckBox)gvr.FindControl("check")).Checked)
                {
                    string separationID = ((Label)gvr.FindControl("lblSeperationDetailsID")).Text;
                    objDB.SeperationDetailsID = Convert.ToInt32(separationID);
                    objDB.ApprovedBy = Session["UserName"].ToString();
                    objDB.Department = "Finance";
                    objDB.Comments = txtComments.InnerText;
                    //objDB.Deductions = 0;
                    alertMsg = objDB.ApproveEmployeeSeperationDetailsByDepartment();
                    recordsCount++;
                }
            }

            GetData();

            alertMsg = recordsCount > 0 ? "" + recordsCount + " Records Approved Successfully" : "No Records Approved ";

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + alertMsg + "')", true);

            //divAlertMsg.Visible = true;
            //divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            //pAlertMsg.InnerHtml = approveList;
        }
    }
}