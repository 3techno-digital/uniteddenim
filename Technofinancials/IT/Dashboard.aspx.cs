﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.IT
{
    public partial class Dashboard : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
             
                Session["UserAccess"] = Session["OldUserAccess"];
                GetEmployeeWiseShiftsChartData();
                GetLastChangeInShifts();
                GetWidgetsData();
                GetDepartmentWiseCheckInChartData();
               // GetAttendanceDashboard();
            }
        }
        string[] colors = new string[] { "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#2eccc1", "#c45850" };
        private void GetAttendanceDashboard()
        {
            string errorMsg = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = objDB.GetAttendanceDashboard(ref errorMsg);
            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {

                    string str = @"          
                                    <script src='https://www.amcharts.com/lib/4/core.js'></script>
                                <script src='https://www.amcharts.com/lib/4/charts.js'></script>
                                <script src='https://www.amcharts.com/lib/4/themes/animated.js'></script>
                                    <script>
            am4core.ready(function () {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart instance
                var chart = am4core.create(""chartdiv"", am4charts.RadarChart);
                chart.scrollbarX = new am4core.Scrollbar();

               

                        chart.data = [";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i == (dt.Rows.Count - 1))
                        {
                            str += @"{
                                category: """ + dt.Rows[i]["MDay"].ToString() + @""",
                                value: " + dt.Rows[i]["Att"].ToString() + @"
                                }";
                        }
                        else
                        {
                            str += @"{
                                category: """ + dt.Rows[i]["MDay"].ToString() + @""",
                                value: " + dt.Rows[i]["Att"].ToString() + @"
                                },";
                        }
                    }

                    str += @"];

          
                chart.radius = am4core.percent(100);
                chart.innerRadius = am4core.percent(50);

                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = ""category"";
                categoryAxis.renderer.grid.template.location = 0;
                categoryAxis.renderer.minGridDistance = 30;
                categoryAxis.tooltip.disabled = true;
                categoryAxis.renderer.minHeight = 110;
                categoryAxis.renderer.grid.template.disabled = true;
                //categoryAxis.renderer.labels.template.disabled = true;
                let labelTemplate = categoryAxis.renderer.labels.template;
                labelTemplate.radius = am4core.percent(-60);
                labelTemplate.location = 0.5;
                labelTemplate.relativeRotation = 90;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.grid.template.disabled = true;
                valueAxis.renderer.labels.template.disabled = true;
                valueAxis.tooltip.disabled = true;

                // Create series
                var series = chart.series.push(new am4charts.RadarColumnSeries());
                series.sequencedInterpolation = true;
                series.dataFields.valueY = ""value"";
                series.dataFields.categoryX = ""category"";
                series.columns.template.strokeWidth = 0;
                series.tooltipText = ""{valueY}"";
                series.columns.template.radarColumn.cornerRadius = 10;
                series.columns.template.radarColumn.innerCornerRadius = 0;

                series.tooltip.pointerOrientation = ""vertical"";

                // on hover, make corner radiuses bigger
                let hoverState = series.columns.template.radarColumn.states.create(""hover"");
                hoverState.properties.cornerRadius = 0;
                hoverState.properties.fillOpacity = 1;


                series.columns.template.adapter.add(""fill"", function (fill, target) {
                    return chart.colors.getIndex(target.dataItem.index);
                })

                // Cursor
                chart.cursor = new am4charts.RadarCursor();
                chart.cursor.innerRadius = am4core.percent(50);
                chart.cursor.lineY.disabled = true;

            }); // end am4core.ready()
        </script>";

                   // ltrAttendance.Text = str.ToString();
                }

            }

        }
        protected void GetLastChangeInShifts()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            string errorMsg = "";
            DataTable dt = objDB.Get_LastChangeInShifts(10, ref errorMsg);
            gvLastChangeInShifts.DataSource = dt;
            gvLastChangeInShifts.DataBind();
        }

        protected void GetEmployeeWiseShiftsChartData()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            string errorMsg = "";
            DataTable dt = objDB.Get_EmployeeWiseShiftChart(ref errorMsg);

            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    string[] ShiftNames = dt.Rows.Cast<DataRow>().Select(row => "'" + row["ShiftName"].ToString() + "'").ToArray();
                    string[] Totals = dt.Rows.Cast<DataRow>().Select(row => row["TotEmployees"].ToString()).ToArray();
                    string[] Colors = new string[ShiftNames.Length];
                    string strshift = string.Join(",", ShiftNames);
                    string strtotal = string.Join(",", Totals);


                    Random rnd = new Random();
                    for (int i = 0; i < ShiftNames.Length; i++)
                    {
                        Colors[i] = "'" + colors[i >= colors.Length ? 0 : i] + "'";// rnd.Next( "'#" + randomColor.Name + "'";
                    }
                    string color = string.Join(",", Colors);
                    string str2 = @"<script> new Chart(document.getElementById('EmployeeWiseShiftChart'), { type: 'bar', data: { labels:[" + strshift + "], datasets: [ { label: '', backgroundColor:[" + color + "], data:[" + strtotal + "] } ] }, options: { legend: { display: false },scales: {xAxes:[{ticks:{autoSkip: false}}]}, title: { display: true, text: '' } } }); </script> ";
                    ltrEmployeeWiseShift.Text = str2.ToString();

                }
            }
        }

        protected void GetDepartmentWiseCheckInChartData()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            string errorMsg = "";
            DataTable dt = objDB.Get_DepartmentWiseCheckInChart(ref errorMsg);

            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    string[] ShiftNames = dt.Rows.Cast<DataRow>().Select(row => "'" + row["DeptName"].ToString() + "'").ToArray();
                    string[] Totals = dt.Rows.Cast<DataRow>().Select(row => row["TotEmployees"].ToString()).ToArray();
                    string[] Colors = new string[ShiftNames.Length];
                    string strshift = string.Join(",", ShiftNames);
                    string strtotal = string.Join(",", Totals);


                    Random rnd = new Random();
                    for (int i = 0; i < ShiftNames.Length; i++)
                    {
                        Colors[i] = "'" + colors[i >= colors.Length ? 0 : i] + "'";// rnd.Next( "'#" + randomColor.Name + "'";
                    }
                    string color = string.Join(",", Colors);
                    string str2 = @"<script> new Chart(document.getElementById('DepartmentWiseCheckInChart'), { type: 'bar', data: { labels:[" + strshift + "], datasets: [ { label: '', backgroundColor:[" + color + "], data:[" + strtotal + "] } ] }, options: { scaleBeginAtZero: true,scaleStartValue: 0,legend: { display: false },scales: {xAxes:[{ticks:{autoSkip: false}}],yAxes:[{ticks:{beginAtZero:true}}]}, title: { display: true, text: '' } } }); </script> ";
                    ltrDepartmentWiseCheckIn.Text = str2.ToString();

                }
            }
        }
        protected void GetWidgetsData()
        {
            string errorMsg = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = objDB.Get_ITDashboardWidgets(ref errorMsg);
            TotShifts.InnerHtml = dt.Rows[0]["TotShifts"].ToString();
            TotCheckIn.InnerHtml = dt.Rows[0]["TotCheckIn"].ToString();
            TotCheckOut.InnerHtml = dt.Rows[0]["TotCheckOut"].ToString();
            TotAbsents.InnerHtml = dt.Rows[0]["TotAbsents"].ToString();
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("IT", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }
    }
}