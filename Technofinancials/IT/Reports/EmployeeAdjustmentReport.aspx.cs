﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.IT.Reports
{
    public partial class EmployeeAdjustmentReport : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                BindEmployeeDropdown();
                txtStartDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");
                txtEndDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");
                BindGridView();

            }
        }

        private void BindGridView()
        {
           

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);          
            DataTable dt = new DataTable();
            DateTime dtfrom = DateTime.Now, dtto = DateTime.Now;
            int employeeId = 0;
            DateTime.TryParse (txtStartDate.Value,out dtfrom);
            DateTime.TryParse(txtEndDate.Value, out dtto);
             int.TryParse(ddlEmployee.SelectedValue,out employeeId);
            objDB.EmployeeID = employeeId;
            dt = objDB.GetAllEmployeeAttendenceAdjustmentReport(dtfrom,dtto,ref errorMsg);


            if (dt != null && dt.Rows.Count > 0)
            {
                DataView dv = dt.DefaultView;
                dv.Sort = "Date";
                gv.DataSource = dv;
                gv.DataBind();
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("IT", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {




            DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
            DateTime.TryParse(txtStartDate.Value, out Fdate);
            DateTime.TryParse(txtEndDate.Value, out Tdate);
            if (Tdate < Fdate)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = "From Date shoul be less than To Date";


                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('From Date shoul be less than To Date')", true);
                gv.DataSource = null;
                gv.DataBind();
                return;
            }
            if (Fdate > DateTime.Now)
            {

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('From Date shoul be Equal or less than Current Date')", true);
                gv.DataSource = null;
                gv.DataBind();
                return;
            }
            if ((Tdate - Fdate).Days > 31 || (Fdate.Day <= Tdate.Day && (Tdate.Month - Fdate.Month) > 0))
            {

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('date range should be of 1 month')", true);
                gv.DataSource = null;
                gv.DataBind();
                return;
            }
            if (ddlEmployee.SelectedIndex <= 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Employee Name Should be Provided')", true);
                gv.DataSource = null;
                gv.DataBind();
                return;
            }
            else
            {
                divAlertMsg.Visible = false;
                BindGridView();
            }//string where = "";

            //if (ddlEmployee.SelectedValue != null && ddlEmployee.SelectedValue != "")
            //{
            //    if (where != "")
            //    {
            //        where += " And ";
            //    }

            //    where += "Employees.EmployeeID = " + ddlEmployee.SelectedValue.ToString();
            //}

            ////if (txtStartDate.Value != null && txtStartDate.Value != "")
            ////{
            ////    if (where != "")
            ////    {
            ////        where += " And ";
            ////    }

            ////    where += "txtStartDate = " + ddlEmployee.SelectedValue.ToString();
            ////}

            ////if (txtEndDate.Value != null && txtEndDate.Value != "")
            ////{
            ////    if (where != "")
            ////    {
            ////        where += " And ";
            ////    }
            ////    where += "txtEndDate = " + ddlEmployee.SelectedValue.ToString();
            ////}
            //DataTable dt = new DataTable();
            //objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            //objDB.FromDate = txtStartDate.Value;
            //objDB.ToDate = txtEndDate.Value;
            //objDB.WhereClause = where;
            ////dt = objDB.GetAttendanceSummaryKTByEmployeeIDAndDate(ref errorMsg);
            //DataTable dtEmployeSummary = new DataTable();
            //dtEmployeSummary = objDB.GetAllEmployeeAttendenceSummaryByDate(ref errorMsg);
            //gvAttendanceSummary.DataSource = dtEmployeSummary;
            //gvAttendanceSummary.DataBind();
        }
        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyIDonly(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void gvAttendanceSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblCheckInStatus = (e.Row.FindControl("lblCheckInStatus") as Label);
                Label lblCheckOutStatus = (e.Row.FindControl("lblCheckOutStatus") as Label);
                Label lblPresent = (e.Row.FindControl("lblPresent") as Label);

                if (lblCheckInStatus.Text == "Holiday")
                {
                    lblCheckInStatus.Text = "H";
                    lblCheckInStatus.CssClass = "weekend-option";
                }
                else if (lblCheckInStatus.Text == "OverTime")
                {
                    lblCheckInStatus.Text = "O";
                    lblCheckInStatus.CssClass = "overtime-option";
                }
                else if (lblCheckInStatus.Text == "Present" || lblCheckInStatus.Text == "On Time")
                {
                    lblCheckInStatus.Text = "P";
                    lblCheckInStatus.CssClass = "present-option";
                }
                else if (lblCheckInStatus.Text == "Off")
                {
                    lblCheckInStatus.Text = "D";
                    lblCheckInStatus.CssClass = "day-option";
                }
                else if (lblCheckInStatus.Text == "Leave")
                {
                    lblCheckInStatus.Text = "L";
                    lblCheckInStatus.CssClass = "rest-option";
                }
                else if (lblCheckInStatus.Text == "Absent")
                {
                    lblCheckInStatus.Text = "A";
                    lblCheckInStatus.CssClass = "absent-option";
                }
                else if (lblCheckInStatus.Text == "Short Duration" || lblCheckInStatus.Text == "Early" || lblCheckInStatus.Text == "Late")
                {
                    lblCheckInStatus.Text = "S";
                    lblCheckInStatus.CssClass = "short-option";
                    lblPresent.Text = "P";
                    lblPresent.CssClass = "present-option";

                }
                else if (lblCheckInStatus.Text == "Not Marked" || lblCheckInStatus.Text == "M")
                {
                    lblCheckInStatus.Text = "M";
                    lblCheckInStatus.CssClass = "missing-option";
                }



                if (lblCheckOutStatus.Text == "Holiday")
                {
                    lblCheckOutStatus.Text = "H";
                    lblCheckOutStatus.CssClass = "weekend-option";
                }
                else if (lblCheckOutStatus.Text == "OverTime")
                {
                    lblCheckOutStatus.Text = "O";
                    lblCheckOutStatus.CssClass = "overtime-option";
                }
                else if (lblCheckOutStatus.Text == "Present" || lblCheckOutStatus.Text == "On Time")
                {
                    lblCheckOutStatus.Text = "P";
                    lblCheckOutStatus.CssClass = "present-option";
                }
                else if (lblCheckOutStatus.Text == "Off")
                {
                    lblCheckOutStatus.Text = "D";
                    lblCheckOutStatus.CssClass = "day-option";
                }
                else if (lblCheckOutStatus.Text == "Leave")
                {
                    lblCheckOutStatus.Text = "L";
                    lblCheckOutStatus.CssClass = "rest-option";
                }
                else if (lblCheckOutStatus.Text == "Absent")
                {
                    lblCheckOutStatus.Text = "A";
                    lblCheckOutStatus.CssClass = "absent-option";
                }
                else if (lblCheckOutStatus.Text == "Short Duration" || lblCheckOutStatus.Text == "Early" || lblCheckOutStatus.Text == "Late")
                {
                    lblCheckOutStatus.Text = "S";
                    lblCheckOutStatus.CssClass = "short-option";
                }
                else if (lblCheckOutStatus.Text == "Not Marked" || lblCheckOutStatus.Text == "M")
                {
                    lblCheckOutStatus.Text = "M";
                    lblCheckOutStatus.CssClass = "missing-option";
                }


            }
        }
    }
}