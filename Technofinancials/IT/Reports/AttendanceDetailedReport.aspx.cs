﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.IT.Reports
{
    public partial class AttendanceDetailedReport : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                BindDropdowns();
                divAlertMsg.Visible = false;
                gv2.Visible = false;

                txtFromDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
                if (HttpContext.Current.Items["AttendanceFilter"] != null)
                {
                    ddlAttendanceTypes.SelectedValue = HttpContext.Current.Items["AttendanceFilter"].ToString();
                }
                GetData();
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("IT", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void BindDropdowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDepartment.DataTextField = "DeptName";
            ddlDepartment.DataValueField = "DeptID";
            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

            ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
            ddlDesignation.DataTextField = "DesgTitle";
            ddlDesignation.DataValueField = "DesgID";
            ddlDesignation.DataBind();
            ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

            ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

            ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
            ddlLocation.DataTextField = "NAME";
            ddlLocation.DataValueField = "NAME";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("ALL", "0"));
        }
        
        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            GetData();
        }

        internal void GetData()
		{
            try
            {
                string res = "";
                CheckSessions();
                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.FromDate = txtFromDate.Text;
                objDB.ToDate = txtToDate.Text;
                objDB.DeptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                objDB.DesgID = Convert.ToInt32(ddlDesignation.SelectedValue);
                objDB.WDID = Convert.ToInt32(string.IsNullOrEmpty(txtKTId.Text) ? "0" : txtKTId.Text);
                objDB.Location = ddlLocation.SelectedValue;

                DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
                DateTime.TryParse(txtFromDate.Text, out Fdate);
                DateTime.TryParse(txtToDate.Text, out Tdate);
                if (Tdate < Fdate)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be less than To Date";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }
                if (Fdate > DateTime.Now)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be Equal or less than Current Date";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }
                if ((Tdate - Fdate).Days > 31 || (Fdate.Day <= Tdate.Day && (Tdate.Month - Fdate.Month) > 0))
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "date range should be of 1 month";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }

                if (ddlAttendanceTypes.SelectedValue == "3")
                {
                    dt = objDB.GetBreakInBreakout(0, ref errorMsg);
                    gv2.DataSource = dt;
                    gv2.DataBind();
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            gv2.Visible = true;
                            gv.Visible = false;
                            divAlertMsg.Visible = false;
                            gv2.UseAccessibleHeader = true;
                            gv2.HeaderRow.TableSection = TableRowSection.TableHeader;
                        }
                        else
                        {
                            gv.DataSource = null;
                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                            pAlertMsg.InnerHtml = "No Record Found";
                        }
                    }
                    else
                    {
                        gv.DataSource = null;
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                }
                else
                {
                    gv2.Visible = false;
                    objDB.Resultant = ddlAttendanceTypes.SelectedValue;
                    dt = objDB.GetAttendanceSheetFlagWise(ref errorMsg);

                    gv.DataSource = dt;
                    gv.DataBind();
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            divAlertMsg.Visible = false;
                            gv.UseAccessibleHeader = true;
                            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                        }
                        else
                        {
                            gv.DataSource = null;
                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                            pAlertMsg.InnerHtml = "No Record Found";
                        }
                    }
                    else
                    {
                        gv.DataSource = null;
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                    Common.addlog("ViewAll", "IT", "All Employee Attendance Viewed", "EmployeeAttendance");
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}