﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                divAlertMsg.Visible = false;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnResetEmail_ServerClick(object sender, EventArgs e)
        {
            string returnMsg = "";
            try
            {
                DBQueries objDB = new DBQueries();
                string errorMsg = "";
                objDB.Email = Session["UserEmail"].ToString();

                DataTable dt = new DataTable();
                dt = objDB.GetUserDetailsByEmail(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        objDB.UserID = Convert.ToInt32(dt.Rows[0]["UserID"].ToString());
                        objDB.Email = dt.Rows[0]["Email"].ToString();

                        Random rand = new Random((int)DateTime.Now.Ticks);
                        int randnum = 0;
                        randnum = rand.Next(1, 100000);
                        objDB.PasswordKey = randnum.ToString();
                        objDB.AddPasswordKey();

                        string passLink = "https://" + Request.ServerVariables["SERVER_NAME"] + "/reset-password?user-email=" + dt.Rows[0]["Email"].ToString() + "&&access-token=" + Common.Encrypt(randnum.ToString());

                        string file = "";
                        string html = "";
                        file = System.Web.HttpContext.Current.Server.MapPath("/assets/emails/email.html");
                        StreamReader sr = new StreamReader(file);
                        FileInfo fi = new FileInfo(file);

                        if (System.IO.File.Exists(file))
                        {
                            html += sr.ReadToEnd();
                            sr.Close();
                        }

                        html = html.Replace("##NAME##", dt.Rows[0]["UserName"].ToString());
                        html = html.Replace("##PASSWORD##", "Please use the following link to reset password. Link is only useable once. <br /> " + passLink);

                        MailMessage mail = new MailMessage();
                        mail.Subject = "User Account Credentials for Technofinancials";
                        mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString(), "Techno Financials");
                        mail.To.Add(Session["UserEmail"].ToString());
                        mail.Body = html;
                        mail.IsBodyHtml = true;

                        SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SenderSMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SenderPort"].ToString()));
                        smtp.EnableSsl = true;
                        NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["SenderPassword"].ToString());
                        smtp.Credentials = netCre;
                        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                        smtp.Send(mail);

                        returnMsg = "An Email has been sent to your email address, please check your inbox";
                    }
                    else
                    {
                        returnMsg = "Invalid Email Address";
                    }
                }
                else
                {
                    returnMsg = "Invalid Email Address";
                }
            }
            catch (Exception ex)
            {
                returnMsg = ex.Message;
            }

            divAlertMsg.Visible = true;
            pAlertMsg.InnerHtml = returnMsg;
        }
    }
}