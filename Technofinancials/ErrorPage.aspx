﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="Technofinancials.ErrorPage" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScrptMangr" runat="server"></asp:ScriptManager>
        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 error-page">
                        <div>
                            <img src="/assets/images/oops.jpg"  class="img-responsive"/>
                            <h3 class="text-center" style="color:#000;font-weight:bold;">500 - SOMETHING WENT WRONG !!</h3>
                            <p class="error-p">The page you are looking for might have been removed had its name changed or is temporarily unavailable</p>
                            <a class="btn error-btn" href="/login">GO TO HOMEPAGE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    <style>
        body{
            background-image:none!important;
            background-color:#fff!important;
        }
        .error-page img{
            display:block;
            margin:0 auto;
            margin-top:11%!important;
            }
        .error-p{
            color:#000;
            text-align:center;
            width:450px;
            display:block;
            margin:0 auto;
        }
.error-btn {
    border-radius: 100px;
    text-align: center;
    display: block;
    margin: 0 auto;
    margin-top: 15px;
    color: #fff;
    background-color: #021eb6;
    width:200px;
}
.error-btn:hover{
    color: #fff;
}
    </style>
</body>
</html>
