﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials
{
    public partial class login_slider : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                //CheckSessions();
                CheckCookies();
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] != null)
            {
                checkAccessLevel();
            }
        }

        private string checkAccessLevel()
        {
            string url = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/";

            if (Session["UserAccess"].ToString() == "Directors")
            {
                url += "directors/view/company-profile";
            }
            if (Session["UserAccess"].ToString() == "Procrument")
            {
                url += "procrument-management/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Human Resource")
            {
                url += "people-management/view/dashboard";
            }
            else if (Session["UserAccess"].ToString() == "Normal User")
            {
                url += "employee-self-service/view/dashboard";
            }
            return url;
        }

        private void CheckCookies()
        {
            if (Request.Cookies["TFUserName"] != null)
            {
                if (Request.Cookies["TFUserEmail"] != null)
                {
                    txtUserEmail.Value = Request.Cookies["TFUserEmail"].Value;
                    txtUserPassword.Attributes.Add("value", Convert.ToString(Request.Cookies["TFPassword"].Value));
                    txtUserPassword.Text = Convert.ToString(Request.Cookies["TFPassword"].Value);
                    chkRememberMe.Checked = true;
                }
            }
        }

        private void RemeberCredentials()
        {
            if (chkRememberMe.Checked)
            {
                Response.Cookies["TFUserName"].Expires = DateTime.Now.AddDays(30);
                Response.Cookies["TFPassword"].Expires = DateTime.Now.AddDays(30);
                Response.Cookies["TFUserName"].Value = txtUserEmail.Value.Trim();
                Response.Cookies["TFPassword"].Value = txtUserPassword.Text.Trim();
            }
        }

        protected void btnLogin_ServerClick(object sender, EventArgs e)
        {
            if (chkRememberMe.Checked == true)
            {
                RemeberCredentials();
            }

            objDB.Email = txtUserEmail.Value;
            objDB.Password = txtUserPassword.Text;
            DataTable dt = objDB.AuthenticateUser(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "Invalid Email Address" && dt.Rows[0][0].ToString() != "Account Not Approved" && dt.Rows[0][0].ToString() != "Wrong Password!!")
                    {
                        Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                        Session["EmployeeID"] = dt.Rows[0]["EmployeeID"].ToString();
                        Session["UserEmail"] = dt.Rows[0]["Email"].ToString();
                        Session["UserPassword"] = dt.Rows[0]["Password"].ToString();
                        Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                        Session["UserPhoto"] = dt.Rows[0]["Photo"].ToString();
                        Session["CompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                        Session["CompanyName"] = dt.Rows[0]["CompanyName"].ToString();
                        Session["DeptID"] = dt.Rows[0]["DeptID"].ToString();
                        Session["DeptName"] = dt.Rows[0]["DeptName"].ToString();
                        Session["DesgID"] = dt.Rows[0]["DesgID"].ToString();
                        Session["CompanyLogo"] = dt.Rows[0]["CompanyLogo"].ToString();
                        Session["UserAccess"] = dt.Rows[0]["UserAccess"].ToString();
                        Session["CompanyShortName"] = dt.Rows[0]["CompanyShortName"].ToString();
                        Session["ShiftID"] = dt.Rows[0]["ShiftID"].ToString();




                        if (dt.Rows[0]["isNew"].ToString() == "True")
                        {
                            Session["isNewUser"] = "1";
                            Response.Redirect("/update-security-questions");
                        }

                        Response.Redirect(checkAccessLevel());
                    }
                    else
                    {
                        divAlertMsg.Visible = true;
                        pAlertMsg.InnerHtml = dt.Rows[0][0].ToString();
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    pAlertMsg.InnerHtml = "Could not connect with database";
                }
            }
            else
            {
                divAlertMsg.Visible = true;
                pAlertMsg.InnerHtml = "Could not connect with database";
            }
        }

    }
}