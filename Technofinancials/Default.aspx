﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Technofinancials.Default" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />
    <style>
            #login-box {
                            box-sizing: border-box;
                            width: 350px;
                            box-shadow: 1px 1px 3px #DDD;
                            margin: auto;
                            line-height: 20px;
                            display: block;
                            border: 1px solid #EAEAEA;
                            padding: 35px 20px;
                            font-family: 'Open Sans', 'Helvetica', sans-serif;
                            font-size: 14px;
                            border-radius: 5px;
                        }
             #form1 {
                            width: 100%;
                            height: 100vh;
                            display: -webkit-box;
                            display: -webkit-flex;
                            display: -moz-box;
                            display: -ms-flexbox;
                            display: flex;
                            flex-wrap: wrap;
                            justify-content: center;
                            align-items: center;
                            -ms-flex-pack: center;
                            -ms-flex-align: center;
                            padding: 15px;
                            position: relative;
                            z-index: 1;
                            background-color: #fff;
                        }
                        .checkbox label{
                             margin-top:0px !important;
                        }

                        .mb-20 {
                            margin-bottom: 20px;
                        }

                        .mb-15 {
                            margin-bottom: 15px;
                        }

                        .mb-10 {
                            margin-bottom: 10px;
                        }

                        input#userName, input#password {
                            width: 100%;
                        }

                        .privacy_statement_inline_block {
                            width: 100% !important;
                            font-family: 'Open Sans','Helvetica',sans-serif;
                            font-size: 12px;
                            color: #999999;
                        }

                        w100, .row {
                            width: auto;
                        }
                        .descP {
    /* margin-top: 20px; */
    text-align: center;
    font-size: 12px;
    max-width: 300px;
    display: block;
    margin: 10px auto 0;
}
                        .box-header{
                            border:none;
                            padding:0px;
                        }
                        .login_img {
    margin: 20px 0;
    text-align: center;
    display: block;
    margin: 20px auto 30px;
    max-width: 90%;
}
            body {
            }

            #content {
                width: 100%;
                height: 100vh;
                display: -webkit-box;
                display: -webkit-flex;
                display: -moz-box;
                display: -ms-flexbox;
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
                -ms-flex-pack: center;
                -ms-flex-align: center;
                padding: 15px;
                position: relative;
                z-index: 1;
                background-color: #fff;
            }


            .mb-20 {
                margin-bottom: 20px;
            }

            .mb-15 {
                margin-bottom: 15px;
            }

            .mb-10 {
                margin-bottom: 10px;
            }
            input[type="radio"], input[type="checkbox"]{
    display:none !important;
}
            input#userName, input#password {
                width: 100%;
            }

            .privacy_statement_inline_block {
                width: 100% !important;
                font-family: 'Open Sans','Helvetica',sans-serif;
                font-size: 12px;
                color: #999999;
            }

            .fl-left {
                float: left;
            }

            button.login-button {
                width: 100% !important;
                display: block;
                color: #fff;
                background-color: #003780;
                padding: 5px 30px;
                outline: 0;
                border: 0;
                font-size: 12px;
                cursor: pointer;
                color: #fff;
                font-family: Noto-Regular;
                transition: all .2s linear;
                border-radius: 3px;
                border: none;
            }

            .w50 {
                width: 50%;
            }

            .row.check-forgot-section.mb-20.fl-left {
                width: 100%;
                margin: 0px 13px;
            }
            .ForgotPass {
    /* font-size: 10px; */
    /* text-align: right; */
    float: right;
    font-size: 14px;
    font-family: Noto-Regular !important;
    color: #003780 !important;
    padding-bottom: 3px;
    font-weight: 600;
}
            img {
    vertical-align: middle;
    border-style: none;
}
            descP a {
    color: #003780;
    font-family: Noto-Bold;
}
            .ForgotPass:hover{
                    border-bottom: none !important ;
            }

            @media only screen and (max-width: 1280px) and (min-width: 800px) {
                div#UpdPnl .form-col .col-md-6 {
                    width: 60%;
                }
            }
        </style>
</head>
<body class="slider-body">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScrptMangr" runat="server"></asp:ScriptManager>
        <section>
            <div class="container-fluid" style="padding-left: 12px; padding-right: 12px;">
                <div class="row">
                    <div class="box" id="login-box">
                        <div id="login-box-header" class="box-header">
                            <div class="row wrap-image text-center mb-20">
                                <span class="img-pad"></span>
                                <img class=" img-responsive login_img logo-img" src="/assets/images/tf_logo.png" alt="Alternate Text" align="center" id="login-logo">
                            </div>
                        </div>
                        
                        <div method="post" id="login-form" class="row validate-form">
                            <asp:UpdatePanel ID="UpdPnl" runat="server">
                                <ContentTemplate>
                                    <div class="col-sm-12">
                                        <div class="clearfix">&nbsp;</div>

                                        <div class="signup-inner" style="margin-top: 0%;">
                                            <div class="signup-form">
                                                <div class="input-group">
                                                    <asp:RequiredFieldValidator CssClass="abc-test" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtUserEmail" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator><%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" runat="server" ControlToValidate="txtUserEmail" ForeColor="Red" ErrorMessage=" Email Address Not Valid" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="btnValidate"></asp:RegularExpressionValidator>--%>
                                                    <input class="form-control" placeholder="Username" type="text" id="txtUserEmail" runat="server" />
                                                </div>
                                                <div class="input-group">
                                                    <asp:RequiredFieldValidator CssClass="abc-test" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUserPassword" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:TextBox CssClass="form-control" placeholder="Password" TextMode="password" ID="txtUserPassword" runat="server"></asp:TextBox>
                                                </div>
                                                <asp:Button CssClass="btn-sucess btn btn-default btn-login g-recaptcha " ID="btnLogin" Style="width: 100%;" runat="server" ValidationGroup="btnValidate" Text="SIGN IN" OnClick="btnLogin_ServerClick" data-sitekey="6LedGfYZAAAAAMQ7Zldb50TdUOWuHrJuwv46KprY"></asp:Button>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <div class="checkbox checkbox-primary">
                                                                <input type="checkbox" id="chkRememberMe" runat="server">
                                                                <label for="chkRememberMe">Remember Me</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <a href="/forgot-password" class="ForgotPass">Forgot Password</a>
                                                        </div>
                                                        <div class="clearfix">&nbsp;</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
<%--                                        <div class="col-md-6 col-md-offset-3">
                                            <!-- Begin DigiCert site seal HTML and JavaScript -->
                                            <%-- <div id="DigiCertClickID_BkDeAnmk" data-language="en">
                                    </div>                                         <script type="text/javascript">
                                                var __dcid = __dcid || []; __dcid.push(["DigiCertClickID_BkDeAnmk", "15", "s", "black", "BkDeAnmk"]); (function () { var cid = document.createElement("script"); cid.async = true; cid.src = "//seal.digicert.com/seals/cascade/seal.min.js"; var s = document.getElementsByTagName("script"); var ls = s[(s.length - 1)]; ls.parentNode.insertBefore(cid, ls.nextSibling); }());
                                            </script>
                                            <!-- End DigiCert site seal HTML and JavaScript -->
                                        </div>--%>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>

                        <div class="row mb-15 privacy_statement_static privacy_statement_inline_block descP">
                            By clicking on the Sign In button, you understand and agree to our
                                        <a href="/terms-of-use" class="link">Terms of Use</a>,
                                        <a href="/legal" class="link">Legal</a>, <a href="/privacy-policy" class="link">Privacy Policy</a> and <a href="/security" class="link">Security</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        


        <script>
            var slideIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                slideIndex++;
                if (slideIndex > x.length) { slideIndex = 1 }
                x[slideIndex - 1].style.display = "block";
                setTimeout(carousel, 5000); // Change image every 5 seconds
            }
        </script>
        <asp:HiddenField ID="hdnCountryName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCityName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnIP" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnState" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnlatitude" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnlongitude" runat="server" ClientIDMode="Static" />

        <script>
            $(document).ready(function () {
                $.ajax({
                    url: 'https://geoip-db.com/jsonp',
                    jsonpCallback: 'callback',
                    dataType: 'jsonp',
                    success: function (location) {
                        $("#hdnCountryName").val(location.country_name);
                        $("#hdnCityName").val(location.city);
                        $("#hdnIP").val(location.IPv4);
                        $("#hdnState").val(location.state);
                        $("#hdnlatitude").val(location.latitude);
                        $("#hdnlongitude").val(location.longitude);
                        callDBMethod();
                    }
                });
            });

            function callDBMethod() {
                var countryName = $("#hdnCountryName").val();
                var cityName = $("#hdnCityName").val();
                var ip = $("#hdnIP").val();
                var state = $("#hdnState").val();
                var latitude = $("#hdnlatitude").val();
                var longitude = $("#hdnlongitude").val();
                var pageURL = window.location.href;

                $.ajax({
                    type: "POST",
                    url: "<%= ResolveClientUrl("~/business/LogService.asmx/addActivityLog")%>",
                    data: '{countryName: ' + JSON.stringify(countryName) + ', cityName: ' + JSON.stringify(cityName) + ', pageURL:' + JSON.stringify(pageURL) + ', ip:' + JSON.stringify(ip) + ',state:' + JSON.stringify(state) + ',latitude:' + JSON.stringify(latitude) + ',longitude:' + JSON.stringify(longitude) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log("data saved");
                    }
                });
            }
        </script>
    </form>
</body>
</html>
