﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials
{
    public partial class Security : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                divAlertMsg.Visible = false;
                BindSecurityQuestionDropDown();
            }
        }

        private void BindSecurityQuestionDropDown()
        {
            ddlSQ1.DataSource = objDB.GetSecurityQuestionDD1(ref errorMsg);
            ddlSQ1.DataTextField = "Ques";
            ddlSQ1.DataValueField = "QuesID";
            ddlSQ1.DataBind();

            ddlSQ2.DataSource = objDB.GetSecurityQuestionDD2(ref errorMsg);
            ddlSQ2.DataTextField = "Ques";
            ddlSQ2.DataValueField = "QuesID";
            ddlSQ2.DataBind();
        }

        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            if (path == "/update-security-questions")
            {
                string returnMsg = "";
                objDB.UserID = Convert.ToInt32(Session["UserID"]);
                objDB.SQuesID = Convert.ToInt32(ddlSQ1.Value);
                objDB.SQAnswer = answer1.Value;
                returnMsg = objDB.AddUserSecurityAnswers();


                objDB.UserID = Convert.ToInt32(Session["UserID"]);
                objDB.SQuesID = Convert.ToInt32(ddlSQ2.Value);
                objDB.SQAnswer = answer2.Value;
                returnMsg = objDB.AddUserSecurityAnswers();

                if (returnMsg == "Security Answers Added")
                {
                    Response.Redirect("/update-password");
                }
                else
                {
                    divAlertMsg.Visible = true;
                    pAlertMsg.InnerHtml = returnMsg;
                }
            }
            else
            {
                string returnMsg = "";
                objDB.UserID = Convert.ToInt32(Session["UserID"].ToString());
                objDB.Ques1 = ddlSQ1.Value;
                objDB.Ans1 = answer1.Value;
                objDB.Ques2 = ddlSQ2.Value;
                objDB.Ans2 = answer2.Value;
                DataTable dt = objDB.VerifySecurityAnswers(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0][0].ToString() != "Invalid Questions and Answers")
                        {
                            Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                            Session["EmployeeID"] = dt.Rows[0]["EmployeeID"].ToString();
                            Session["UserEmail"] = dt.Rows[0]["Email"].ToString();
                            Session["UserPassword"] = dt.Rows[0]["Password"].ToString();
                            Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                            Session["UserPhoto"] = dt.Rows[0]["Photo"].ToString();
                            Session["CompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            Session["CompanyName"] = dt.Rows[0]["CompanyName"].ToString();
                            Session["DeptID"] = dt.Rows[0]["DeptID"].ToString();
                            Session["DeptName"] = dt.Rows[0]["DeptName"].ToString();
                            Session["DesgID"] = dt.Rows[0]["DesgID"].ToString();
                            Session["CompanyLogo"] = dt.Rows[0]["CompanyLogo"].ToString();

                            if (dt.Rows[0]["ParentCompanyID"] == null)
                            {
                                Session["ParentCompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            }
                            else if (dt.Rows[0]["ParentCompanyID"].ToString() == "")
                            {
                                Session["ParentCompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            }
                            else
                            {
                                Session["ParentCompanyID"] = dt.Rows[0]["ParentCompanyID"].ToString();
                            }

                            if (dt.Rows[0]["UserAccess"].ToString() == "Custom")
                            {
                                Session["isCustom"] = "True";
                            }
                            else
                            {
                                Session["isCustom"] = "False";
                            }

                            returnMsg = "Valid Answsers";
                        }
                        else
                            returnMsg = dt.Rows[0][0].ToString();
                    }
                    else
                    {
                        returnMsg = errorMsg;
                    }
                }
                else
                {
                    returnMsg = errorMsg;
                }

                if (returnMsg == "Valid Answsers")
                {
                    Response.Redirect("/update-password");
                }
                else
                {
                    divAlertMsg.Visible = true;
                    pAlertMsg.InnerHtml = returnMsg;
                }
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }
    }
}