﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewHiring.aspx.cs" Inherits="Technofinancials.People_Management.NewHiring" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/People Management/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <img src="/assets/images/hr-02.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="text-uppercase tf-page-heading-text">New Vacancies</h3>
                            <button class="tf-back-btn pull-right back-button-top-mg" "" data-original-"Back"></button>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <h4><i class="fa fa-male"></i> Select Existing Jd</h4>
                            <select class="form-control">
                                <option>Backend Developer</option>
                                <option>Frontend Developer</option>
                                <option>Operations Manager</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <label>&nbsp;</label>
                            <p class="hiring-or">OR</p>
                        </div>
                        <div class="col-sm-4">
                            <label>&nbsp;</label>
                            <button class="hiring-create-new-btn" type="button" data-toggle="modal" data-target="#myModal"> Create New </button>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-12 ">
                            <p class="tf-mg-top">
                                Position to be filled with in
                                <input name="firstname" type="number" class="tf-num-field" />
                                Days.
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <div id="control-demo-6" class="col-sm-4">
                            <h4><i class="fa fa-graduation-cap"></i>Qualification</h4>
                            <select class="form-control">
                                <option>BS</option>
                                <option>MS</option>
                                <option>CS</option>
                                <option>BBA</option>
                                <option>BA</option>
                            </select>
                        </div>
                        <div id="control-demo-7" class="col-sm-4">
                            <h4><i class="fa fa-calendar-plus-o"></i> Experience</h4>
                            <select class="form-control">
                                <option>1 Year</option>
                                <option>2 Year</option>
                                <option>2.5 Year</option>
                            </select>
                        </div>
                                                <div class="col-sm-4">
                            <div class="form-group">
                                <h4><i class="fa fa-keyboard-o"></i> KeyWords</h4>
                                <input type="text" value="KeyWords" data-plugin="tagsinput" data-role="tagsinput" class="form-control" placeholder="add more.." style="display: none;" />
                            </div>
                        </div>

                    </div>

<%--                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <h4><i class="fa fa-keyboard-o"></i> KeyWords</h4>
                                <input type="text" value="KeyWords" data-plugin="tagsinput" data-role="tagsinput" class="form-control" placeholder="add more.." style="display: none;" />
                            </div>
                        </div>
                    </div>--%>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h4><i class="fa fa-money"></i> Budget</h4>
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h4>Skill Set</h4>
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h4><i class="fa fa-certificate"></i> Certification</h4>
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h4>Training</h4>
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>








                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>



        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Hiring Form</h4>
                    </div>
                    <div class="modal-body">
                    
                            <div class="form-group">
                                <label for="exampleTextInput1" class="col-sm-3 control-label">Name:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="exampleTextInput1" placeholder="Your name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email2" class="col-sm-3 control-label">Email:</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="email2" placeholder="Eamil address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="textarea1" class="col-sm-3 control-label">Comment:</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="textarea1" placeholder="Your comment..."></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="checkbox checkbox-success">
                                        <input type="checkbox" id="checkbox-demo-2">
                                        <label for="checkbox-demo-2">View my email</label>
                                    </div>
                                </div>
                            </div>
                      
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn btn-success" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>



        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script>
            $(document).ready(function () {
                $('.js-example-basic-multiple').select2();
            });
        </script>
    </form>
    <style>
        .modal-dialog {
            width: 450px;
            margin: 30px auto;
        }

        .modal-body div div {
            margin-bottom: 20px;
        }

        section.app-content {
            padding-bottom: 53px;
        }
        .modal-footer {
    border-top: 0px solid #e5e5e5;
}
        .hiring-create-new-btn {
    margin-top: 17px;
    border: none;
    background: none;
    color: #01769a;
}
    </style>

</body>
</html>
