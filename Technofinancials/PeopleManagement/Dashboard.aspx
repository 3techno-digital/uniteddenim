﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Technofinancials.People_Management.Dashboard1" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        div#container {
            height: 395px;
            padding-top: 10px;
        }

        .removetrial {
            background: #e8e8e8;
            height: 20px;
            width: 95px;
            border-radius: 11px;
            position: absolute;
            right: 82%;
            bottom: 24px;
            z-index: 1;
        }

        .anychart-credits {
            display: none;
        }
        /*#pieChartAm {
                width: 100%;
                height: 500px;
            }

            #manChartAm4 {
                width: 100%;
                height: 500px;
            }

            #columnChartImages {
                width: 100%;
                height: 500px;
            }

            #solidGuage {
                width: 100%;
                height: 500px;
            }

            #chart3D {
                width: 100%;
                height: 370px;
            }

            #punchCard {
                width: 100%;
                height: 500px;
            }

            #scatterSingleAxis {
                width: 100%;
                height: 500px;
            }*/
        .m-l-10 h3 {
            text-align: left;
            margin-top: 0px;
        }

        .panel-heading {
            border-color: #eff2f7;
            font-size: 1.25rem;
            padding-top: 20px !important;
            font-weight: 700;
            padding: 15px;
        }

        #chartdiv {
            width: 100%;
            height: 370px;
        }

        span#lblCol3 {
            word-break: break-word;
        }

        .orange {
            background: #fa8564 !important;
        }

        .pink {
            background: #a48ad4 !important;
        }

        .mini-stat .green {
            background: #aec785 !important;
        }

        .mini-stat-icon.yellow {
            background-color: #f9c851;
        }

        .D_NONE {
            display: none !important;
        }

        #backButton {
            position: absolute;
            top: 35px;
            right: 35px;
            cursor: pointer;
        }

        .invisible {
            display: none;
        }

        .form-group h4, label {
            margin: 20px 0 10px;
        }

        #chartContainerNew01 .canvasjs-chart-toolbar,
        #chartContainerBar02 .canvasjs-chart-toolbar {
            display: none;
        }

        .mini-stat-icon i {
            font-size: 25px;
        }

        .gray {
            background: #dee2e6;
            border-radius: 20px;
            margin: 0 auto;
            text-align: center;
            padding: 5px;
            margin-top: 20px;
        }

        .chart_heading {
            text-align: center;
            margin: 0 0 13px;
            padding-top: 20px;
            font-size: 20px;
            font-weight: 700;
            color: #333;
        }



        .removetrial2 {
            background: #dee2e6;
            height: 20px;
            width: 95px;
            border-radius: 9px;
            position: absolute;
            bottom: 5px;
            left: 19px;
            z-index: 1;
        }


        a.canvasjs-chart-credit {
            display: none;
        }

        .table {
            color: black;
            margin-bottom: 0;
        }

        .card {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-clip: border-box;
            border-radius: .25rem;
        }

        .align-self-center {
            -ms-flex-item-align: center !important;
            align-self: center !important;
        }

        .flex-row {
            -ms-flex-direction: row !important;
            flex-direction: row !important;
        }

        .round {
            line-height: 60px;
            color: #5b6be8;
            width: 60px;
            height: 60px;
            font-size: 26px;
            display: inline-block;
            font-weight: 400;
            border: 3px solid #f8f8fe;
            text-align: center;
            border-radius: 50%;
            background: #e1e4fb;
        }

        .mini-stat-icon.purple {
            background-color: #f4b9b9;
        }

        .m-l-10 h5 {
            font-size: 24px;
            color: #767676 !important;
        }

        .m-l-10 h3 {
            font-size: 20px;
        }

        .text-muted {
            opacity: 1;
            text-align: center;
            margin-bottom: 0px;
            font-size: 10px;
            padding-top: 2px;
            color: #767676 !important;
        }

        .mini-stat {
            background: #fff;
            padding: 10px;
            box-shadow: 0 2px 3px 0px #ccc;
            margin-bottom: 20px;
            border-radius: 10px;
            height: 100%;
        }

        .m-l-10 h5, .m-l-10 p {
            text-align: left;
        }

        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1rem;
        }

        .removetrialMale {
            /*background: #e8e8e8;*/
            background: #ffffff;
            height: 40px;
            width: 95px;
            border-radius: 9px;
            position: absolute;
            left: 22px;
            bottom: 34px;
            z-index: 1;
            margin: 4px !important;
            top: 52px;
        }

        .map_radius .widget-body {
            padding: 0 1rem;
        }

        .table > thead > tr > th {
            /*background-color: #e8e8e8;*/
            background-color: #ffffff;
            border-bottom: 2px solid #000000 !important;
            color: #000;
        }

        .canvasjs-chart-toolbar {
            display: none;
        }
        /*.table tr:nth-child(odd) {
                background: #fff;
            }

            .table tr:nth-child(even) {
                background: #f2f2f2;
            }*/
        .anounceTab th:nth-child(1) {
            width: 30px;
            text-align: center;
        }

        .anounceTab th:nth-child(2) {
            width: 160px;
        }

        .anounceTab th:nth-child(3) {
            width: 200px;
        }

        .anounceTab {
            height: 370px;
            padding-right: 5px;
        }



        #style-1::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        #style-1::-webkit-scrollbar {
            width: 7px;
            background-color: #F5F5F5;
        }

        #style-1::-webkit-scrollbar-thumb {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #003780;
        }

        .slimScrollBar {
            background: #fff !important;
            width: 10px !important;
        }

        .header-fav-tab {
            display: none !important;
        }

        .menubar-scroll-inner.tf-sidebar-menu-items {
            overflow: scroll;
            overflow-x: hidden;
            overflow-y: scroll;
            height: 85vh !important;
        }

        /*body::-webkit-scrollbar {
                width: 1em;
            }

            body::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            }

            body::-webkit-scrollbar-thumb {
                background-color: darkgrey;
                outline: 1px solid slategrey;
            }*/

        .slimScrollDiv {
            height: 85vh !important;
        }

        .table > thead:first-child > tr:first-child > th {
            border-top-style: none !important;
        }

        g[aria-labelledby="id-479-title"] {
            display: none;
        }

        g[aria-labelledby="id-318-title"] {
            display: none;
        }

        g[aria-labelledby="id-68-title"] {
            display: none;
        }

        g[aria-labelledby="id-214-title"] {
            display: none;
        }

        @media only screen and (max-width: 1280px) and (min-width:800px) {
            .removetrial {
                right: 78% !important;
            }
             .m-l-10 h3 {
    font-size: 20px;
    margin-bottom: 3px;
}
            .mini-stat-icon {
                width: 40px !important;
                height: 40px !important;
                line-height: 33px !important;
                margin-right: 3px !important;
            }

                .mini-stat-icon i {
                    font-size: 20px;
                }

                .mini-stat-icon img {
                    max-width: 55%;
                }

            .widget.map_radius.announcment_area.extra, .widget.map_radius.new.extra {
                margin-top: 10px;
            }

            .text-muted {
                padding-top: 0px;
                font-size: 9px !important;
            }
        }

        /*        @media screen and (min-width: 1900px) and (min-width: 2080px) {
            .removetrial {
                right: 87px;
            }
        }*/

        @media only screen and (max-width: 1599px) and (min-width: 1281px) {
            .mini-stat {
                padding: 10px 8px;
                border-radius: 10px;
            }

            .removetrial {
                right: 80% !important;
            }
            /*            .removetrial {
                bottom: -22px;
            }*/

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
                font-size: 10px;
            }

            .mini-stat-icon.purple img {
                width: 50%;
            }

            .mini-stat-icon {
                width: 40px !important;
                height: 40px !important;
                line-height: 33px !important;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .mini-stat {
                padding: 10px 8px;
                border-radius: 10px;
            }

            .mini-stat-icon.purple img {
                width: 50%;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
                font-size: 10px;
            }

            .mini-stat-icon {
                width: 40px !important;
                height: 40px !important;
                line-height: 33px !important;
            }

                .mini-stat-icon i {
                    font-size: 19px;
                }
        }

        @media only screen and (max-width: 1024px) and (min-width: 768px) {
            .mini-stat {
                padding: 10px 11px;
                border-radius: 10px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .chart_heading {
                font-size: 19px;
            }

            .text-muted {
                    font-size: 13px !important;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            .mini-stat {
                padding: 10px 11px;
                border-radius: 10px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            .mini-stat {
                padding: 10px 11px;
                border-radius: 10px;
            }
            .removetrial{
                right: 78%;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 25px !important;
                }
        }

        @media only screen and (max-width: 1439px) and (min-width: 1201px) {
            td {
                max-width: 50px !important;
            }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            td {
                max-width: 50px !important;
            }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            td {
                max-width: 50px !important;
            }

            .widget.map_radius.new {
                height: 430px !important;
            }

            .removetrial {
                right: 19em !important;
            }
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            td {
                max-width: 50px !important;
            }
        }
        /*@media only screen and (max-width: 991px) and (min-width: 768px){
                td: before{*/
        /* Now like a table header */
        /*position: absolute;*/
        /* Top/left values mimic padding */
        /*top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
            }
            }*/
        @media only screen and (max-width: 2560px) and (min-width: 1600px) {
            /*            .removetrial {
                left: 83px !important;
            }*/
            canvas#gradewise-chart {
                height: 347px !important;
            }
        }

        @media only screen and (max-width: 767px) and (min-width: 360px) {
            .removetrial {
                right: 28em;
            }
        }

        .mini-stat-icon {
            width: 50px;
            height: 50px;
            display: inline-block;
            line-height: 45px;
            text-align: center;
            font-size: 30px;
            background: #eee;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            float: left;
            margin-right: 10px;
            color: #fff;
        }

        .tar {
            background: #1fb5ac !important;
        }

        .mini-stat-info {
            font-size: 11px;
            padding-top: 2px;
            color: #767676 !important;
            font-weight: 700;
            min-height: 51PX;
        }

        .panel-heading {
            border-bottom: 1px solid #e8e8e8;
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            text-align: center;
            color: #212529 !important;
            margin-top: 0px;
        }

        .map_radius {
            border-radius: 0px;
            box-shadow: 0 2px 3px 0px #ccc;
            background: #e8e8e8;
            border-radius: 20px;
        }

        .pr-0 {
            padding-right: 0px !important;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <h1 class="m-0 text-dark">Human Capital Management</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4">
                            <div style="text-align: right;">
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>

            <div class="wrap">
                <section class="app-content">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <div class="row">
                                <!-- small box -->
                                <div class="col-lg-2 col-md-4 col-sm-6 pr-0">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employees"); %>">
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon orange">
                                                <i class="fa fa-users" aria-hidden="true"></i>
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="hEmployees">00</h3>
                                                        <p class="mb-0 text-muted">Head Count</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-lg-2 col-md-4 col-sm-6 pr-0">
                                    <a id="hrefPendingRequest" runat="server">
                                        <!-- small box -->
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon pink">
                                                <img src="/assets/images/pending-request.png" />
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="hCurrMonthPayroll"></h3>
                                                        <p class="mb-0 text-muted">Pending Requests</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6 pr-0">
                                    <!-- small box -->
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Flag-wise-Employees-Present"); %>">
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon green">
                                                <i class="fa fa-male" aria-hidden="true"></i>
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="presentEmployees"></h3>
                                                        <p class="mb-0 text-muted">Present Employees</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6 pr-0">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Turnover-Report-Department-Wise-" + DateTime.Now.Month); %>">
                                        <!-- small box -->
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon tar">
                                                <img src="/assets/images/turnover-rate.png" />
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" style="display: none;" id="TurnoverRate">0.00%</h3>
                                                        <h3>0.00%</h3>
                                                        <p class="mb-0 text-muted">Turnover Rate</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6 pr-0">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/New-Joiners-" + DateTime.Now.Month); %>">
                                        <!-- small box -->
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon yellow">
                                                <i class="fa fa-user-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="NewRecruits">0</h3>
                                                        <p class="mb-0 text-muted">New Recruits</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Left-Employees-" + DateTime.Now.Month); %>">
                                        <!-- small box -->
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon purple">
                                                <i class="fa fa-user-times" aria-hidden="true"></i>
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="leftEmpCount">0</h3>
                                                        <p class="mb-0 text-muted">Left Employees</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!-- ./col -->
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="widget map_radius">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <h5 class="panel-heading" style="text-align: left; padding-left: 25px;">Attendance For The Month
                                        </h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="right_First_6">

                                            <div class="add_new">

                                                <a href="/keep-truckin/people-management/view/attendance-sheet-CurrentMonth" class="AD_btn">View                                               
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- .widget-header -->
                                <div class="widget-body">
                                    <div id="chartdiv"></div>
                                    <asp:Literal ID="ltrAttendance" runat="server"></asp:Literal>
                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="widget map_radius">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <h5 class="panel-heading" style="text-align: left; padding-left: 25px;">Male Vs Female Ratio
                                        </h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="right_First_6">

                                            <div class="add_new">
                                                <a href="/keep-truckin/people-management/view/MaleFemale-Ratio-Department-Wise" class="AD_btn">View
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- .widget-header -->
                                <div class="widget-body">

                                    <div class="removetrial"></div>
                                    <div id="chartContainer3" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
                                    <asp:Literal ID="ltrGenderChaer" runat="server"></asp:Literal>
                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="widget map_radius announcment_area extra" style="height: 430px;">
                                <header class="panel-heading" style="padding-bottom: 0px;">
                                    Announcements <span class="tools pull-right"></span>
                                </header>
                                <!-- .widget-header -->
                                <div class="card m-b-30">
                                    <div class="card-body" style="padding-top: 0px;">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Title <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtAnnouncementTitle" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input class="form-control" id="txtAnnouncementTitle" placeholder="Title" type="text" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Date <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="text" data-plugin="datetimepicker" class="form-control" data-date-format="DD-MMM-YYYY" id="txtDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Department <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="dpt" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                
                                     <asp:ListBox ID="dpt" SelectionMode="Multiple"  runat="server" class="form-control select2" data-plugin="select2">
                                        </asp:ListBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Slack Channel </h4>
                                            
                                     <asp:ListBox ID="slack" SelectionMode="Multiple"  runat="server" class="form-control select2" data-plugin="select2">
                                        </asp:ListBox>
                                        (if not select then announcement will be sent to #general channel)
                                                </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <h4>Description <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtDescription" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <textarea rows="2" class="form-control" id="txtDescription" placeholder="Description" type="number" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="padding-left: 0px; text-align: center;">
                                            <button style="margin: 25px 5px 5px 0px;" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button" class="AD_btn_inn">Add</button>
                                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Announcement"); %>">
                                                <button type="button" style="margin: 25px 0px 5px 0px;" class="AD_btn_inn">View</button>
                                            </a>
                                        </div>
                                        <div class="col-md-8 col-md-offset-2">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                            <span>
                                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                            </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: none;">
                                    <%--                                       <div class="clearfix">&nbsp;</div>--%>
                                    <asp:GridView ID="gvAnnouncement" runat="server" CssClass="table table-bordered  dataTable no-footer" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="#">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Announcement Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol6" Text='<%# Eval("Date") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Title">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("AnnouncementTitle") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Description")  %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>


                                    <table class="table" style="display: none;">
                                        <tbody>
                                            <tr>
                                                <th>#</th>
                                                <th>Announcement Date</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>30-Aug-2019</td>
                                                <td>Lorem Ipsum</td>
                                                <td>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</td>
                                            </tr>

                                            <tr>
                                                <td>1</td>
                                                <td>30-Aug-2019</td>
                                                <td>Lorem Ipsum</td>
                                                <td>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</td>
                                            </tr>


                                        </tbody>
                                    </table>

                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="widget map_radius new extra" style="height: 434px;">
                                <h5 class="panel-heading">Grade Wise Employees<span class="tools pull-right"></span>
                                </h5>
                                <!-- .widget-header -->
                                <div class="widget-body">
                                    <canvas id="gradewise-chart" width="800" height="450"></canvas>
                                    <!--<div class="removetrialMale"></div>
                                    <div id="chartContainerGrade" style="height: 370px; max-width: 920px; margin: 15px auto; overflow: hidden;"></div>-->
                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>

                        <div class="col-md-6 col-sm-12" style="display: none">
                            <div class="widget map_radius new" style="height: 460px;">
                                <header class="panel-heading">
                                    Grade Wise Employees <span class="tools pull-right"></span>
                                </header>
                                <!-- .widget-header -->
                                <div class="widget-body">
                                    <div class="removetrialMale"></div>
                                    <%--                                    <script src="https://cdn.anychart.com/releases/8.0.0/js/anychart-base.min.js"></script>
                                    <div id="container"></div>--%>
                                    <div id="chartContainer" style="height: 370px; width: 100%; padding-top: 10px;"></div>
                                    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                                    <%-- <asp:Literal ID="ltrGradeWiseChart" runat="server"></asp:Literal>--%>
                                    <!--<div class="removetrialMale"></div>
                                    <div id="chartContainerGrade" style="height: 370px; max-width: 920px; margin: 15px auto; overflow: hidden;"></div>-->
                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>







                    </div>

                    <div class="spacecover"></div>

                    <%--                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                        </div>
                    </div>--%>



                    <div class="row D_NONE ">
                        <div class="col-md-12 col-sm-12">
                            <div class="widget map_radius" style="margin-top: 20px;">
                                <header class="widget-header">
                                    <h4 class="widget-title">Grade Wise Employes</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <div id="chart3D"></div>
                                    <%--<asp:Literal ID="ltrGradeWiseChart" runat="server"></asp:Literal>--%>
                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                        <%--    <div class="col-md-6 col-sm-12">
                            <div class="widget">
                                <header class="widget-header">
                                    <h4 class="widget-title">Department Wise Strength</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <div id="solidGuage"></div>


                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>--%>
                    </div>


                    <div class="row D_NONE">
                        <div class="col-md-12 col-sm-12">
                            <div class="widget map_radius">
                                <header class="widget-header">
                                    <h4 class="widget-title">Male vs Female Ratio</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <div id="manChartAm4"></div>


                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                    </div>

                    <div class="row D_NONE" style="display: none">
                        <div class="col-md-6">
                            <div class="widget">
                                <header class="widget-header">
                                    <h4 class="widget-title">Male vs Female Ratio</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">

                                    <%--     <asp:Literal ID="ltrGenderChaer" runat="server"></asp:Literal>--%>
                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                        <!-- END column -->

                        <div class="col-md-6">
                            <div class="widget">
                                <header class="widget-header">
                                    <h4 class="widget-title">Grade Wise Employees1</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <%--            <asp:Literal ID="ltrGradeWiseChart" runat="server"></asp:Literal>--%>
                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                        <!-- END column -->

                        <div class="col-md-6">
                            <div class="widget">
                                <header class="widget-header">
                                    <h4 class="widget-title">Designation Wise Employees</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <%--     <asp:Literal ID="ltrDesignationWiseChart" runat="server"></asp:Literal>--%>
                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                        <!-- END column -->
                        <div class="col-md-6">
                            <div class="widget">
                                <header class="widget-header">
                                    <h4 class="widget-title">Employee Types</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <asp:Literal ID="ltrEmployeeTypeChart" runat="server"></asp:Literal>

                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                        <!-- END column -->

                    </div>

                    <div class="clearfix">&nbsp;</div>

                    <div class="row D_NONE" style="display: none">
                        <div class="col-md-6 col-sm-12">
                            <div class="widget">
                                <header class="widget-header">
                                    <h4 class="widget-title">Loans</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">

                                    <asp:Literal ID="ltrLoanChart" runat="server"></asp:Literal>

                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                        <div class="col-md-6">
                            <div class="widget">
                                <header class="widget-header">
                                    <h4 class="widget-title">Provident Funds</h4>
                                </header>
                                <!-- .widget-header -->
                                <hr class="widget-separator">
                                <div class="widget-body">
                                    <%-- <div data-plugin='chart' data-options='{tooltip : {trigger: "item",legend: {x : "center",y : "top",data:["Company Investement","Employee Investement","Total Amount","Withdrawn Amount","Remaining Amount"]},calculable : true,series : [{name:"Provient Fund",type:"pie",radius : [20, 110],center : ["50%", 200],roseType : "radius",label: {normal: {show: false},emphasis: {show: true}},lableLine: {normal: {show: false},emphasis: {show: true}},data:[{value:10, name:"rose1"},{value:5, name:"rose2"},{value:15, name:"rose3"},{value:25, name:"rose4"},{value:20, name:"rose5"}]}]}' style='height: 300px;'></div>--%>
                                    <asp:Literal ID="ltrChart" runat="server"></asp:Literal>
                                </div>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>


                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                </section>
            </div>
            <!-- #dash-content -->

            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <%--        <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/echarts.min.js"></script>
        <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts-gl/echarts-gl.min.js"></script>
        <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts-stat/ecStat.min.js"></script>
        <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/extension/dataTool.min.js"></script>
        <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/map/js/china.js"></script>
        <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/map/js/world.js"></script>
        <script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=xfhhaT

l11qYVrqLZii6w8qE5ggnhrY&__ec_v__=20190126"></script>
        <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/extension/bmap.min.js"></script>
        <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/simplex.js"></script>--%>
        <script>
            sap.ui.controller("myController", {
                onInit: function () {

                    var jsonData = {
                        "cars": [{
                            Model: "Fiat",
                            Value: "500"
                        }, {
                            Model: "Porsche",
                            Value: "911"
                        }, {
                            Model: "Peugeot",
                            Value: "504"
                        }, {
                            Model: "MyCar",
                            Value: "100"
                        }]
                    };
                    var jsonModel = new sap.ui.model.json.JSONModel(jsonData);
                    this.getView().setModel(jsonModel);


                }

            });

            sap.ui.view({
                viewContent: jQuery('#myXml').html(),
                type: sap.ui.core.mvc.ViewType.XML
            }).placeAt("content");

        </script>
        <script>
            anychart.onDocumentReady(function () {

                // set the data
                var data = {
                    header: ["Name", "Death toll"],
                    rows: [
                        ["CEO", 1500],
                        ["HR", 87000],
                        ["Project Manager", 175000],
                        ["Sernior Developer", 10000],
                        ["Tian Shan (1976)", 242000],
                        ["Armenia (1988)", 25000],
                        ["Iran (1990)", 50000],
                        ["Project Manager", 175000],
                        ["Sernior Developer", 10000],
                        ["Tian Shan (1976)", 242000],
                        ["Armenia (1988)", 25000],
                        ["Iran (1990)", 50000]
                    ]
                };

                // create the chart
                var chart = anychart.bar();

                // add data
                chart.data(data);

                // set the chart title
                chart.title("");


                // draw
                chart.container("container");
                chart.draw();
            });
        </script>
        <script>
            am4core.ready(function () {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart instance
                var chart = am4core.create("pieChartAm", am4charts.RadarChart);
                chart.scrollbarX = new am4core.Scrollbar();

                var data = [];

                for (var i = 0; i < 30; i++) {
                    data.push({ category: i, value: Math.round(Math.random() * 100) });
                }

                chart.data = data;
                chart.radius = am4core.percent(100);
                chart.innerRadius = am4core.percent(50);

                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.grid.template.location = 0;
                categoryAxis.renderer.minGridDistance = 30;
                categoryAxis.tooltip.disabled = true;
                categoryAxis.renderer.minHeight = 110;
                categoryAxis.renderer.grid.template.disabled = true;
                //categoryAxis.renderer.labels.template.disabled = true;
                let labelTemplate = categoryAxis.renderer.labels.template;
                labelTemplate.radius = am4core.percent(-60);
                labelTemplate.location = 0.5;
                labelTemplate.relativeRotation = 90;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.grid.template.disabled = true;
                valueAxis.renderer.labels.template.disabled = true;
                valueAxis.tooltip.disabled = true;

                // Create series
                var series = chart.series.push(new am4charts.RadarColumnSeries());
                series.sequencedInterpolation = true;
                series.dataFields.valueY = "value";
                series.dataFields.categoryX = "category";
                series.columns.template.strokeWidth = 0;
                series.tooltipText = "{valueY}";
                series.columns.template.radarColumn.cornerRadius = 10;
                series.columns.template.radarColumn.innerCornerRadius = 0;

                series.tooltip.pointerOrientation = "vertical";

                // on hover, make corner radiuses bigger
                let hoverState = series.columns.template.radarColumn.states.create("hover");
                hoverState.properties.cornerRadius = 0;
                hoverState.properties.fillOpacity = 1;


                series.columns.template.adapter.add("fill", function (fill, target) {
                    return chart.colors.getIndex(target.dataItem.in


x);
                })

                // Cursor
                chart.cursor = new am4charts.RadarCursor();
                chart.cursor.innerRadius = am4core.percent(50);
                chart.cursor.lineY.disabled = true;

            }); // end am4core.ready()
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>
            var data = {
                datasets: [{
                    data: [300, 100],
                    backgroundColor: [
                        "#77a033",
                        "#207cba",
                    ]
                }],
                labels: [
                    "Female",
                    "Male",
                ]
            };

            $(document).ready(
                function () {
                    var canvas = document.getElementById("myChart");
                    var ctx = canvas.getContext("2d");
                    var myNewChart = new Chart(ctx, {
                        type: 'pie',
                        data: data
                    });

                    canvas.onclick = function (evt) {
                        var activePoints = myNewChart.getElementsAtEvent(evt);
                        if (activePoints[0]) {
                            var chartData = activePoints[0]['_chart'].config.data;
                            var idx = activePoints[0]['_index'];

                            var label = chartData.labels[idx];
                            var value = chartData.datasets[0].data[idx];

                            var url = "http://example.com/?label=" + label + "&value=" + value;
                            console.log(url);
                            alert(url);
                        }
                    };
                }
            );
        </script>
        <asp:Literal ID="ltrGradeWiseChart" runat="server"></asp:Literal>
        <script>
            new Chart(document.getElementById("bar-chart1"), {
                type: 'bar',
                data: {
                    labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
                    datasets: [
                        {
                            label: "Population (millions)",
                            backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"],
                            data: [2478, 5267, 734, 784, 433]
                        }
                    ]
                },
                options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: ''
                    }
                }
            });
        </script>

        <script type="text/javascript" src="canvasjs.min.js"></script>
        <script>
            var chart = new CanvasJS.Chart("chartContainer31", {
                animationEnabled: true,
                backgroundColor: '',
                borderWidth: 5,
                title: {
                    text: "",
                    horizontalAlign: "center",
                    fontSize: 18,
                    fontWeight: "900",
                    fontFamily: "Roboto",
                    padding: 10,
                },
                data: [{
                    type: "doughnut",
                    startAngle: 60,
                    innerRadius: 100,
                    indexLabelFontSize: 17, col- lg - 2 col - md - 4 col - sm - 6 pr - 0
                    indexLabel: "{label} - #percent%",
                    toolTipContent: "<b>{label}:</b> {y} (#percent%)",
                    dataPoints: [
                    { y: 179.45, label: "Direct" },
                    { y: 7.31, label: "Organic" },
                    { y: 7.06, label: "Paid" }

                ]
                }]
            });
            chart.render();
        </script>

        <asp:Literal ID="ltrDesignationWiseChart" runat="server"></asp:Literal>

    </form>
</body>
</html>
