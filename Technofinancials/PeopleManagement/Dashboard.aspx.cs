﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Collections.Generic;

namespace Technofinancials.People_Management
{
	public partial class Dashboard1 : System.Web.UI.Page
    {
        static int CompanyID = 0;
        string errorMsg = "";
        DBQueries objDB = new DBQueries();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();

            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                Session["UserAccess"] = Session["OldUserAccess"];
                CompanyID = int.Parse(Session["CompanyID"].ToString());
                
                generateMaleFemaleChart();
                generateGradeWiseChart();
                GetAttendanceDashboard();
                GetAnnouncement();
                GetPayrollAmountsAndEmployees();
                LoadSlackChannel();
                LoadDepartments();

                Common.addlog("ViewAll", "HR", "Dashboard Viewed", "");
            }
        }

        private void LoadSlackChannel()
        {
            DataTable dt = new DataTable();

            dt = objDB.GetSlackChannels(ref errorMsg);
            slack.DataSource = dt;
            slack.DataTextField = "Channel";
            slack.DataValueField = "Channel";
            slack.DataBind();

        }
        private void LoadDepartments()
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetDepartments(ref errorMsg);
            dpt.DataSource = dt;
            dpt.DataTextField = "DeptName";
            dpt.DataValueField = "DeptName";

            dpt.DataBind();
            dpt.Items.Insert(0, new ListItem("ALL", "ALL"));

        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }


        private void GetPayrollAmountsAndEmployees()
        {
            CheckSessions();
            string NewRecruit = "";
            string leftEmployeeCount = "";
            string Empturnover = "";
            hrefPendingRequest.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/pending_request_" + DateTime.Now.ToString("MMM-yyyy");

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.FromDate = DateTime.Now.ToString("yyyy-MM-01");
            objDB.ToDate = objDB.ToDate = DateTime.Now.ToString("yyyy-MM-dd");
            objDB.Month = DateTime.Now.ToString();
            hEmployees.InnerHtml = objDB.GetEmployeeCount();
            
            presentEmployees.InnerText = objDB.GetPresentEmployees();

            hCurrMonthPayroll.InnerText = objDB.GetPendingRequestsCount();
            
            NewRecruit = objDB.Get_NewRecruits();
            NewRecruits.InnerHtml = (NewRecruit == "" ? "0.00" : NewRecruit);

            leftEmployeeCount = objDB.Get_LeftEmployeesCount();
            leftEmpCount.InnerHtml = (leftEmployeeCount == "" ? "0.00" : leftEmployeeCount);

            Empturnover = objDB.Get_EmployeeTurnover();
            TurnoverRate.InnerHtml = (Empturnover == "" ? "0.00" : leftEmployeeCount);
        }
        private void GetAnnouncement()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetTop5AnnouncementByCompanyID(ref errorMsg);
            gvAnnouncement.DataSource = dt;
            gvAnnouncement.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvAnnouncement.UseAccessibleHeader = true;
                    gvAnnouncement.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All Announcement Viewed", "Announcement");

        }

        protected void generateLoanChart()
        {
            //string sb = "";

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = objDB.GetLoanSummaryByCompanyID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    string script = "<div data-plugin=\"chart\" data-options=\"{";
                    script += "tooltip:";
                    script += "{";
                    script += "trigger: 'item',";
                    script += "formatter: '{a} <br/>{b}: {c} ({d}%)'";
                    script += "},";
                    script += "legend:";
                    script += "{";
                    script += "orient: 'vertical',";
                    script += "x: 'left',";
                    script += "data:[";
                    script += "'Total Amount', 'Repaid Amount', 'Remaining Amount'";

                    script += "]";
                    script += "},";
                    script += "series: [";
                    script += "{";
                    script += "name: 'Loan Calculations',";
                    script += "type: 'pie',";
                    script += "radius: ['50%', '70%'],";
                    script += "avoidLabelOverlap: false,";
                    script += "label:";
                    script += "{";
                    script += "normal:";
                    script += "{";
                    script += "show: false,";
                    script += "position: 'center'";
                    script += "},";
                    script += "emphasis:";
                    script += "{";
                    script += "show: true,";
                    script += "textStyle:";
                    script += "{";
                    script += "fontSize: '30',";
                    script += "fontWeight: 'bold'";
                    script += "}";
                    script += "}";
                    script += "},";
                    script += "labelLine:";
                    script += "{";
                    script += "normal:";
                    script += "{";
                    script += "show: false";
                    script += "}";
                    script += "},";
                    script += "data:[";
                    script += "{ value: " + dt.Rows[0]["TotalAmount"].ToString() + ", name: 'Total Amount'},";
                    script += "{ value: " + dt.Rows[0]["TotalAmountRepaid"].ToString() + ", name: 'Repaid Amount'},";
                    script += "{ value: " + dt.Rows[0]["RemainingAmount"].ToString() + ", name: 'Remaining Amount'}";

                    script += "]";
                    script += "}";
                    script += "]";
                    script += "}";
                    script += "\" style = \"height: 340px; \"></div>";

                    ltrLoanChart.Text = script.ToString();
                }
            }
            //sb += @"<div data-plugin='chart' data-options='{";
            //sb += @"    tooltip : {";
            //sb += @"trigger: 'item',";
            //sb += @"formatter: '{a} <br/>{b} : {c} ({d}%)'";
            //sb += @"},";
            //sb += @"legend: {";
            //sb += @"x : 'center',";
            //sb += @"y : 'top',";
            //sb += @"data:['rose1','rose2','rose3','rose4','rose5','rose6', 'rose7']";
            //sb += @"},";
            //sb += @"calculable : true,";
            //sb += @"series : [";
            //sb += @"{";
            //sb += @"name:'Rose',";
            //sb += @"type:'pie',";
            //sb += @"radius : [20, 110],";
            //sb += @"center : ['50%', 200],";
            //sb += @"roseType : 'radius',";
            //sb += @"label: {";
            //sb += @"normal: {";
            //sb += @"show: false";
            //sb += @"},";
            //sb += @"emphasis: {";
            //sb += @"show: true";
            //sb += @"}";
            //sb += @"},";
            //sb += @"lableLine: {";
            //sb += @"normal: {";
            //sb += @"show: false";
            //sb += @"},";
            //sb += @"emphasis: {";
            //sb += @"show: true";
            //sb += @"}";
            //sb += @"},";
            //sb += @"data:[";
            //sb += @"{ value:10, name:'rose1'},";
            //sb += @"{ value:5, name:'rose2'},";
            //sb += @"{ value:15, name:'rose3'},";
            //sb += @"{ value:25, name:'rose4'},";
            //sb += @"{ value:20, name:'rose5'},";
            //sb += @"{ value:35, name:'rose6'},";
            //sb += @"{ value:30, name:'rose7'},";
            //sb += @"]";
            //sb += @"}";
            //sb += @"]";
            //sb += @"}' style='height: 300px;'>";

            //sb = @"<div data-plugin=chart; data-options={tooltip:{trigger:&#39;item&#39;,formatter: &#39;{a} <br/>{b} : {c} ({d}%)&#39;},legend: {x : &#39;center&#39;,y : &#39;top&#39;,data:[&#39;rose1&#39;,&#39;rose2&#39;,&#39;rose3&#39;,&#39;rose4&#39;,&#39;rose5&#39;,&#39;rose6&#39;, &#39;rose7&#39;]},calculable : true,series : [{name:&#39;Rose&#39;,type:&#39;pie&#39;,radius : [20, 110],center : [&#39;50%&#39;, 200],roseType : &#39;radius&#39;,label: {normal: {show: false},emphasis: {show: true}},lableLine: {normal: {show: false},emphasis: {show: true}},data:[{value:10, name:&#39;rose1&#39;},{value:5, name:&#39;rose2&#39;},{value:15, name:&#39;rose3&#39;},{value:25, name:&#39;rose4&#39;},{value:20, name:&#39;rose5&#39;},{value:35, name:&#39;rose6&#39;},{value:30, name:&#39;rose7&#39;},]}]};&#39; style=&#39;height: 300px;&#39;>";



            //ltrPFChart.Text = sb.ToString();
        }


        private void generatePFChart()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = objDB.GetProvinentFundSummaryByCompanyID(ref errorMsg);
            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    string str = " <div data-plugin='chart' data-options='{tooltip : {trigger: \"item\"},legend: {x : &quot;center&quot;,y : &quot;top&quot;,data:[&quot;Compamy Amount &quot;,&quot;Employee Amount&quot;,&quot;Total Amount&quot;,&quot;Withdrawn Amount&quot;,&quot;Remaining Amount&quot;]},calculable : true,series : [{name:&quot;Provident Funds&quot;,type:&quot;pie&quot;,radius : [20, 110],center : [&quot;50 % &quot;, 200],roseType : &quot;radius&quot;,label: {normal: {show: false},emphasis: {show: true}},lableLine: {normal: {show: false},emphasis: {show: true}},data:[{value:" + dt.Rows[0]["CompanyAmount"].ToString() + ", name:&quot;Company Amount&quot;},{value:" + dt.Rows[0]["EmployeeAmount"].ToString() + ", name:&quot;Employee Amount&quot;},{value:" + dt.Rows[0]["TotalAmount"].ToString() + ", name:&quot;Total Amount&quot;},{value:" + dt.Rows[0]["WithdrawAmount"].ToString() + ", name:&quot;Withdrawn Amount&quot;},{value:" + dt.Rows[0]["RemaningAmount"].ToString() + ", name:&quot;Remaining Amount&quot;}]}]}' style='height: 340px;'></div>";
                    ltrChart.Text = str.ToString();

                }

            }

        }

        private void generateMaleFemaleChart()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = objDB.GetMaleVsFemaleRatioByCompanyID(ref errorMsg);
            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {

                    //        string str = @"<div data-plugin='chart' data-options='{ tooltip : { trigger: &quot;item&quot;,  formatter: &quot;{a} <br/>{b} : {c} ({d}%)&quot; },  legend: {
                    //        orient: &quot;vertical&quot;,
                    //        x: &quot;left&quot;,
                    //        data: [&quot;" + dt.Rows[0]["Gender"].ToString() + @"&quot;,&quot;"+ dt.Rows[1]["Gender"].ToString() + @"&quot;]	
                    //    },
                    //    series : [
                    //        {
                    //            name: &quot;&quot;,
                    //            type: &quot;pie&quot;,
                    //            radius : &quot;55%&quot;,
                    //            center: [&quot;50%&quot;, &quot;60%&quot;],
                    //            data:[
                    //                {value:"+ dt.Rows[0]["Ratio"].ToString()+", name:&quot;" + dt.Rows[0]["Gender"].ToString() + @"&quot;},
                    //                {value:"+ dt.Rows[1]["Ratio"].ToString() + ", name:&quot;"+ dt.Rows[1]["Gender"].ToString() + @"&quot;},
                    //            ],
                    //            itemStyle: {
                    //                emphasis: {
                    //                    shadowBlur: 10,
                    //                    shadowOffsetX: 0,
                    //                    shadowColor: &quot;rgba(0, 0, 0, 0.5)&quot;
                    //                }
                    //            }
                    //        }
                    //    ]
                    //}' style='height: 300px;'>
                    //</div>";


                    //            string str = @"  <script>
                    //    am4core.ready(function () {

                    //        // Themes begin
                    //        am4core.useTheme(am4themes_animated);
                    //        // Themes end

                    //        var iconPath = ""M53.5,476c0,14,6.833,21,20.5,21s20.5-7,20.5-21V287h21v189c0,14,6.834,21,20.5,21 c13.667,0,20.5-7,20.5-21V154h10v116c0,7.334,2.5,12.667,7.5,16s10.167,3.333,15.5,0s8-8.667,8-16V145c0-13.334-4.5-23.667-13.5-31 s-21.5-11-37.5-11h-82c-15.333,0-27.833,3.333-37.5,10s-14.5,17-14.5,31v133c0,6,2.667,10.333,8,13s10.5,2.667,15.5,0s7.5-7,7.5-13 V154h10V476 M61.5,42.5c0,11.667,4.167,21.667,12.5,30S92.333,85,104,85s21.667-4.167,30-12.5S146.5,54,146.5,42 c0-11.335-4.167-21.168-12.5-29.5C125.667,4.167,115.667,0,104,0S82.333,4.167,74,12.5S61.5,30.833,61.5,42.5z""



                    //        var chart = am4core.create(""manChartAm4"", am4charts.SlicedChart);
                    //        chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect

                    //                chart.data = [";

                    //            str += @"{
                    //                        name: """ + dt.Rows[0]["Gender"].ToString() + @""",
                    //                        value: " + dt.Rows[0]["Ratio"].ToString() + @"
                    //                        },";

                    //            str += @"{
                    //                        name: """ + dt.Rows[1]["Gender"].ToString() + @""",
                    //                        value: " + dt.Rows[1]["Ratio"].ToString() + @"
                    //                        }";


                    //            str += @"];


                    //        var series = chart.series.push(new am4charts.PictorialStackedSeries());
                    //        series.dataFields.value = ""value"";
                    //        series.dataFields.category = ""name"";
                    //        series.alignLabels = true;

                    //        series.maskSprite.path = iconPath;
                    //        series.ticks.template.locationX = 1;
                    //        series.ticks.template.locationY = 0.5;

                    //        series.labelsContainer.width = 200;

                    //        chart.legend = new am4charts.Legend();
                    //        chart.legend.position = ""left"";
                    //        chart.legend.valign = ""bottom"";

                    //    }); // end am4core.ready()
                    //</script>";


                    /*new Chart(document.getElementsByClassName('mychart'), {
                type: 'doughnut',
                data:
                    {
                    labels:['IT', 'IT-Non'],
                    datasets:
                        [
                        {
                        label: 'Population (millions)',
                            backgroundColor:['#f4b9b9', '#aec785'],*/

					string str = @"
    <script src='https://canvasjs.com/assets/script/canvasjs.min.js'></script>

<script>
            CanvasJS.addColorSet('firstshades',
                [

                    '#67b7dc',
                    '#012c7b'


                ]);
            var chart = new CanvasJS.Chart('chartContainer3', {
                animationEnabled: true,
                colorSet: 'firstshades',
                backgroundColor: '',
                borderWidth: 5,
                
                legend: {
                    verticalAlign: 'bottom',
                    horizontalAlign: 'center',
                    
                },
                data: [
                    {
                        
                        indexLabelFontFamily: 'Garamond',
                        indexLabelFontColor: 'darkgrey',
                        indexLabelLineColor: 'darkgrey',
                        indexLabelPlacement: 'outside',
                        type: 'doughnut',
                        startAngle: 0,
                        innerRadius: 100,
                        indexLabelFontSize: 16,
                        showInLegend: true,
                        dataPoints: [";
                    
                    str += @"{ y:" + dt.Rows[0]["Ratio"].ToString() + @", legendText:""" + dt.Rows[0]["Gender"].ToString() + @""", indexLabel: """ + dt.Rows[0]["Ratio"].ToString() + "" + @""" },
                                    { y: " + dt.Rows[1]["Ratio"].ToString() + @", legendText: """ + dt.Rows[1]["Gender"].ToString() + @""", indexLabel: """ + dt.Rows[1]["Ratio"].ToString() + "" + @""" }

                        ]
                    }
                ]
            });

            chart.render();

        </script>";

					ltrGenderChaer.Text = str.ToString();

				}

            }

        }
        string[] colors = new string[] { "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#2eccc1", "#c45850" };

        private void generateGradeWiseChart()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = objDB.GetGradeRatioByCompanyID(ref errorMsg);
            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    string[] GradeNames = dt.Rows.Cast<DataRow>().Select(row => "'" + row["GradeName"].ToString() + "'").ToArray();
                    string[] Ratio = dt.Rows.Cast<DataRow>().Select(row => row["Ratio"].ToString()).ToArray();
                    string[] Colors = new string[GradeNames.Length];
                    string strgrade = string.Join(",", GradeNames);
                    string strratio = string.Join(",", Ratio);


                    Random rnd = new Random();
                    for (int i = 0; i < GradeNames.Length; i++)
                    {
                        Colors[i] = "'" + colors[i >= colors.Length ? 0 : i] + "'";// rnd.Next( "'#" + randomColor.Name + "'";
                    }
                    string color = string.Join(",", Colors);
                    string str = "<script>new Chart(document.getElementById('gradewise-chart'), {type: 'bar',data: {" +
                    $"labels:[{strgrade}]," +
                    "datasets:[{label: 'No Of Employees'," +
                    $"backgroundColor:[{color}],"+
                    $"data:[{strratio}]" +
                        "}]},options:{legend: { display: false },title:{display: true,text: ''}}});</script> ";

//                    string str = @"
//<script>
//             window.onload = function () {

//                 var chart = new CanvasJS.Chart('chartContainer', {
//                     animationEnabled: true,
//                     exportEnabled: true,
//                     theme: 'light1', // 'light1', 'light2', 'dark1', 'dark2'
//                     title: {
//                         text: ''
//                     },
//                     axisX: {
//                         title: ''
//                     },
//                     axisY: {
//                         includeZero: true
//                     },
//                     data: [{
//                         type: 'column', //change type to bar, line, area, pie, etc
//                         //indexLabel: '{y}', //Shows y value on all Data Points
//                         indexLabelFontColor: '#5A5757',
//                         indexLabelFontSize: 16,
//                         indexLabelPlacement: 'outside',
//                         dataPoints: [";
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        if (i == (dt.Rows.Count - 1))
//                        {
//                            str += @"{ label: """ + dt.Rows[i]["GradeName"].ToString() + @""", y: " + dt.Rows[i]["Ratio"].ToString() + @" }";
//                        }
//                        else
//                        {
//                            str += @"{ label: """ + dt.Rows[i]["GradeName"].ToString() + @""", y: " + dt.Rows[i]["Ratio"].ToString() + @" },";
//                        }
//                    }

//                    str += @" ]
//                     }]
//                 });
//                 chart.render();
//                 function toggleDataSeries(e) {
//                     if (typeof (e.dataSeries.visible) === 'undefined' || e.dataSeries.visible) {
//                         e.dataSeries.visible = false;
//                     } else {
//                         e.dataSeries.visible = true;
//                     }
//                     e.chart.render();
//                 }
//             }
//         </script>";

                    ltrGradeWiseChart.Text = str.ToString();
                }

            }

        }
        private void GetAttendanceDashboard()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = objDB.GetAttendanceDashboard(ref errorMsg);
            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {

                    string str = @"          
<script src='https://www.amcharts.com/lib/4/core.js'></script>
        <script src='https://www.amcharts.com/lib/4/charts.js'></script>
        <script src='https://www.amcharts.com/lib/4/themes/animated.js'></script>
<script>
            am4core.ready(function () {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart instance
                var chart = am4core.create(""chartdiv"", am4charts.RadarChart);
                chart.scrollbarX = new am4core.Scrollbar();

               

                        chart.data = [";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i == (dt.Rows.Count - 1))
                        {
                            str += @"{
                                category: """ + dt.Rows[i]["MDay"].ToString() + @""",
                                value: " + dt.Rows[i]["Att"].ToString() + @"
                                }";
                        }
                        else
                        {
                            str += @"{
                                category: """ + dt.Rows[i]["MDay"].ToString() + @""",
                                value: " + dt.Rows[i]["Att"].ToString() + @"
                                },";
                        }
                    }

                    str += @"];

                chart.radius = am4core.percent(100);
                chart.innerRadius = am4core.percent(50);

                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = ""category"";
                categoryAxis.renderer.grid.template.location = 0;
                categoryAxis.renderer.minGridDistance = 30;
                categoryAxis.tooltip.disabled = true;
                categoryAxis.renderer.minHeight = 110;
                categoryAxis.renderer.grid.template.disabled = true;
                //categoryAxis.renderer.labels.template.disabled = true;
                let labelTemplate = categoryAxis.renderer.labels.template;
                labelTemplate.radius = am4core.percent(-60);
                labelTemplate.location = 0.5;
                labelTemplate.relativeRotation = 90;
              
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.grid.template.disabled = true;
                valueAxis.renderer.labels.template.disabled = true;
                valueAxis.tooltip.disabled = true;

                // Create series
                var series = chart.series.push(new am4charts.RadarColumnSeries());
                series.sequencedInterpolation = true;
                series.dataFields.valueY = ""value"";
                series.dataFields.categoryX = ""category"";
                series.columns.template.strokeWidth = 0;
                series.tooltipText = ""{valueY}"";
                series.columns.template.radarColumn.cornerRadius = 10;
                series.columns.template.radarColumn.innerCornerRadius = 0;
    
                series.tooltip.pointerOrientation = ""vertical"";
                

                // on hover, make corner radiuses bigger
                let hoverState = series.columns.template.radarColumn.states.create(""hover"");
                hoverState.properties.cornerRadius = 0;
                hoverState.properties.fillOpacity = 1;
             
                series.columns.template.adapter.add(""fill"", function (fill, target) {
                    return chart.colors.getIndex(target.dataItem.index);
                })

                // Cursor
                chart.cursor = new am4charts.RadarCursor();
                chart.cursor.innerRadius = am4core.percent(50);
                chart.cursor.lineY.disabled = true;

            }); // end am4core.ready()
        </script>";

                    ltrAttendance.Text = str.ToString();
                }

            }

        }





        private void generateDesignationWise()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = objDB.GetGradeDesignationByCompanyID(ref errorMsg);
            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    string str = @"<script>
            am4core.ready(function () {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end



                // Create chart instance
                var chart = am4core.create(""solidGuage"", am4charts.RadarChart);

                // Add data
    chart.data = [";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i == (dt.Rows.Count - 1))
                        {
                            str += @"{
                                category: """ + dt.Rows[i]["DesgTitle"].ToString() + @""",
                                value: """ + dt.Rows[i]["Ratio"].ToString() + @""",
                                full: " + 100 + @"
                                }";
                        }
                        else
                        {
                            str += @"{
                                category: """ + dt.Rows[i]["DesgTitle"].ToString() + @""",
                                value: """ + dt.Rows[i]["Ratio"].ToString() + @""",
                                full: " + 100 + @"
                                },";
                        }
                    }

                    str += @"];


                // Make chart not full circle
                chart.startAngle = -90;
                chart.endAngle = 180;
                chart.innerRadius = am4core.percent(20);

                // Set number format
                chart.numberFormatter.numberFormat = ""#.#'%'"";

                // Create axes
                var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = ""category"";
                categoryAxis.renderer.grid.template.location = 0;
                categoryAxis.renderer.grid.template.strokeOpacity = 0;
                categoryAxis.renderer.labels.template.horizontalCenter = ""right"";
                categoryAxis.renderer.labels.template.fontWeight = 500;
                categoryAxis.renderer.labels.template.adapter.add(""fill"", function (fill, target) {
                    return (target.dataItem.index >= 0) ? chart.colors.getIndex(target.dataItem.index) : fill;
                });
                categoryAxis.renderer.minGridDistance = 10;

                var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.grid.template.strokeOpacity = 0;
                valueAxis.min = 0;
                valueAxis.max = 100;
                valueAxis.strictMinMax = true;

                // Create series
                var series1 = chart.series.push(new am4charts.RadarColumnSeries());
                series1.dataFields.valueX = ""full"";
                series1.dataFields.categoryY = ""category"";
                series1.clustered = false;
                series1.columns.template.fill = new am4core.InterfaceColorSet().getFor(""alternativeBackground"");
                series1.columns.template.fillOpacity = 0.08;
                series1.columns.template.cornerRadiusTopLeft = 20;
                series1.columns.template.strokeWidth = 0;
                series1.columns.template.radarColumn.cornerRadius = 20;

                var series2 = chart.series.push(new am4charts.RadarColumnSeries());
                series2.dataFields.valueX = ""value"";
                series2.dataFields.categoryY = ""category"";
                series2.clustered = false;
                series2.columns.template.strokeWidth = 0;
                series2.columns.template.tooltipText = ""{category}: [bold]{value}[/]"";
                series2.columns.template.radarColumn.cornerRadius = 20;

                series2.columns.template.adapter.add(""fill"", function (fill, target) {
                    return chart.colors.getIndex(target.dataItem.index);
                });

                // Add cursor
                chart.cursor = new am4charts.RadarCursor();

            }); // end am4core.ready()
        </script>";



                    ltrDesignationWiseChart.Text = str.ToString();

                }

            }

        }

        private void generateEmployeeType()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = objDB.GetEmployeeTypeRatioByCompanyID(ref errorMsg);
            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {

                    string str = @"<div data-plugin='chart' data-options='{ tooltip : { trigger: &quot;item&quot;,  formatter: &quot;{a} <br/>{b} : {c} ({d}%)&quot; },  legend: {
                    orient: &quot;vertical&quot;,
                    x: &quot;left&quot;,
                    data: [&quot;" + dt.Rows[0]["EmployementType"].ToString() + @"&quot;,&quot;" + dt.Rows[1]["EmployementType"].ToString() + @"&quot;,&quot;" + dt.Rows[2]["EmployementType"].ToString() + @"&quot;,&quot;" + dt.Rows[3]["EmployementType"].ToString() + @"&quot]	
                },
                series : [
                    {
                        name: &quot;&quot;,
                        type: &quot;pie&quot;,
                        radius : &quot;55%&quot;,
                        center: [&quot;50%&quot;, &quot;60%&quot;],
                        data:[
                            {value:" + dt.Rows[0]["Ratio"].ToString() + ", name:&quot;" + dt.Rows[0]["EmployementType"].ToString() + @"&quot;},
                            {value:" + dt.Rows[1]["Ratio"].ToString() + ", name:&quot;" + dt.Rows[1]["EmployementType"].ToString() + @"&quot;},
                            {value:" + dt.Rows[2]["Ratio"].ToString() + ", name:&quot;" + dt.Rows[2]["EmployementType"].ToString() + @"&quot;},
                            {value:" + dt.Rows[3]["Ratio"].ToString() + ", name:&quot;" + dt.Rows[3]["EmployementType"].ToString() + @"&quot;},
                        ],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: &quot;rgba(0, 0, 0, 0.5)&quot;
                            }
                        }
                    }
                ]
            }' style='height: 300px;'>
            </div>";
                    ltrEmployeeTypeChart.Text = str.ToString();

                }

            }

        }

        protected void clearfields() 
        {
            txtAnnouncementTitle.Value = "";
            txtDate.Value  = DateTime.Now.ToString("dd-MMM-yyyy");
            txtDescription.Value ="";
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.AnnouncementTitle = txtAnnouncementTitle.Value;
                objDB.Date = txtDate.Value;
                objDB.Description = txtDescription.Value;
                int Docid = 0;
                int sendToALL = 0;
                List<string> lstofDptChannels = new List<string>();
                foreach (ListItem item in dpt.Items)
                {
                    if (item.Selected)
                    {
                        if (item.Value == "ALL")
                        {
                            lstofDptChannels.Add(item.Value);
                            sendToALL = 1;

                        }
                        else
                        {
                            lstofDptChannels.Add(item.Value);
                        }

                    }
                }
                foreach (ListItem item in slack.Items)
                {
                    if (item.Selected)
                    {
                        lstofDptChannels.Add(item.Value);
                    }
                }


              
                    objDB.CreatedBy = Session["UserName"].ToString();

                    Docid = Convert.ToInt32(objDB.AddAnnouncement());
                  
                    res = "New Announcement Added";
                
                if (lstofDptChannels.Count > 0)
                {
                    foreach (var item in lstofDptChannels)
                    {
                        objDB.Channel = item;
                        objDB.AnnouncementID = Docid;
                        objDB.AnnouncementDptChannel();
                    }


                }
                objDB.DocType = "Announcement";
                objDB.DocID = Docid;
                objDB.Notes = "Created from HCM Dashboard";
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

                if (res == "New Announcement Added" || res == "Announcement Data Updated")
                {
                    if (res == "New Announcement Added") { Common.addlog("Add", "HR", "New Announcement \"" + objDB.AnnouncementTitle + "\" Added", "Announcement"); }
                    if (res == "Announcement Data Updated") { Common.addlog("Update", "HR", "Announcement \"" + objDB.AnnouncementTitle + "\" Updated", "Announcement", objDB.AnnouncementID); }
                    if (lstofDptChannels.Count > 0)
                    {
                        if (sendToALL == 1)
                        {
                            objDB.Channel = "#general";
                            objDB.SlackBy = Session["UserName"].ToString();
                            objDB.SlackOn = objDB.Date;
                            objDB.SlackIT();

                        }
                        foreach (ListItem item in slack.Items)
                        {
                            if (item.Selected)
                            {
                                if (sendToALL == 1)
                                {

                                    break;
                                }
                                objDB.Channel = item.Value;

                                objDB.SlackBy = Session["UserName"].ToString();
                                objDB.SlackOn = objDB.Date;

                                objDB.SlackIT();

                            }
                        }


                    }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        //protected void btnSave_ServerClick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        CheckSessions();
        //        string res = "";

        //        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //        objDB.AnnouncementTitle = txtAnnouncementTitle.Value;
        //        objDB.Date = txtDate.Value;
        //        objDB.Description = txtDescription.Value;
        //        objDB.CreatedBy = Session["UserName"].ToString();
        //        objDB.AddAnnouncement();
        //        res = "New Announcement Added";
        //        if (res == "New Announcement Added" || res == "Announcement Data Updated")
        //        {

        //            DataTable dt3=objDB.GetAllEmployeesByCompanyID(ref errorMsg);
        //            if (dt3 != null)
        //            {
        //                foreach (DataRow row in dt3.Rows)
        //                {
        //                    sendEmail(row["EmployeeName"].ToString(), row["Email"].ToString(), txtDescription.Value);
        //                }
        //            }
        //            if (res == "New Announcement Added") { Common.addlog("Add", "HR", "New Announcement \"" + objDB.AnnouncementTitle + "\" Added", "Announcement"); }
        //            divAlertMsg.Visible = true;
        //            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
        //            pAlertMsg.InnerHtml = res;
        //            clearfields();
        //        }
        //        else
        //        {
        //            divAlertMsg.Visible = true;
        //            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //            pAlertMsg.InnerHtml = res;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }

        //}

        private void sendEmail(string Name, string UserName, string Annoucemnet)
        {
            string returnMsg = "";
            try
            {
                DBQueries objDB = new DBQueries();
                string errorMsg = "";
                string file = "";
                string html = "";

                file = System.Web.HttpContext.Current.Server.MapPath("/assets/emails/Announcement.html");
                StreamReader sr = new StreamReader(file);
                FileInfo fi = new FileInfo(file);

                if (System.IO.File.Exists(file))
                {
                    html += sr.ReadToEnd();
                    sr.Close();
                }

                //  html = html.Replace("##CREATED_BY##", Session["UserName"].ToString());
                html = html.Replace("##NAME##", Name);
                html = html.Replace("##announcement##", Annoucemnet);

                MailMessage mail = new MailMessage();
                mail.Subject = "New Announcement from Technofinancials";
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), "Techno Financials");
                mail.To.Add(UserName);
                mail.Body = html;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["NoReplySMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NoReplyPort"].ToString()));
                smtp.EnableSsl = true;
                NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["NoReplyPassword"].ToString());
                smtp.Credentials = netCre;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                returnMsg = ex.Message;
            }

        }

    }
}