﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement
{
    public partial class ChangeWorkingShift : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int ChangeWorkingShiftID
        {
            get
            {
                if (ViewState["ChangeWorkingShiftID"] != null)
                {
                    return (int)ViewState["ChangeWorkingShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["ChangeWorkingShiftID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["ChangeWorkingShiftID"] = null;
                    BindEmployeeDropdown();
                    BindWorkingShift();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/AddOnsExpense";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["ChangeWorkingShiftID"] != null)
                    {
                        ChangeWorkingShiftID = Convert.ToInt32(HttpContext.Current.Items["ChangeWorkingShiftID"].ToString());
                       objDB.getWorkingShiftChangedByID(ref errorMsg);
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "AddOns";
                objDB.PrimaryColumnnName = "AddOnsID";
                objDB.PrimaryColumnValue = ChangeWorkingShiftID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getAddOnsExpenseByID(int ChangeWorkingShiftID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.ChangeWorkingShiftID = ChangeWorkingShiftID;
                dt = objDB.GetAddOnsExpenseByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtNotes.Value = dt.Rows[0]["Note"].ToString();
                        txtDate.Value = dt.Rows[0]["EffectiveDate"].ToString();
                        ddlWorkingShift.SelectedValue = dt.Rows[0]["WorkingShiftID"].ToString();
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                    }
                }
                Common.addlog("View", "HR", "Changed Working Shift Viewed", "WorkingShiftChange", objDB.ChangeWorkingShiftID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EffectiveDate = Convert.ToDateTime(txtDate.Value);
                objDB.Notes = txtNotes.Value;
                objDB.ShiftID = Convert.ToInt32(ddlWorkingShift.SelectedValue);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                if (HttpContext.Current.Items["ChangeWorkingShiftID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.ChangeWorkingShiftID = ChangeWorkingShiftID;
                    res = objDB.UpdateWorkingShiftChanged();
                    //res = "AddOn Head Data Updated";
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddWorkingChangedShift();
                    clearFields();
                    //res = "New AddOn Head Added";
                }



                if (res == "New AddOns Added Successfully" || res == "AddOns  Data Updated")
                {
                    if (res == "New AddOns Added Successfully") { Common.addlog("Add", "HR", "New AddOns \"" + objDB.CurrencyName + "\" Added", "AddOns"); }
                    if (res == "AddOns  Data Updated") { Common.addlog("Update", "HR", "AddOns \"" + objDB.CurrencyName + "\" Updated", "AddOns", objDB.ChangeWorkingShiftID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtDate.Value = "";
            ddlEmployee.SelectedValue = "0";
            ddlWorkingShift.SelectedValue = "0";
        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "WorkingShiftChange", "WorkingShiftChangeID", HttpContext.Current.Items["ChangeWorkingShiftID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "Working Shift of ID\"" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/ChangeWorkingShift", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/ChangeWorkingShift/edit-ChangeWorkingShift-" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString(), "AddOn \"" + ChangeWorkingShiftID + "\"", "WorkingShiftChange", "Announcement", Convert.ToInt32(HttpContext.Current.Items["ChangeWorkingShiftID"].ToString()));

                //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                //{
                //    objDB.ChangeWorkingShiftID = ChangeWorkingShiftID;
                //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //    objDB.AddCurrencyToEmployeeAttendance();
                //}

                //Common.addlog("Delete", "HR", "Currency of ID \"" + objDB.ChangeWorkingShiftID + "\" deleted", "Currency", objDB.ChangeWorkingShiftID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "WorkingShiftChange", "ChangeWorkingShiftID", HttpContext.Current.Items["ChangeWorkingShiftID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "Working Shift of ID\"" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString() + "\" Status Changed", "WorkingShiftChange", ChangeWorkingShiftID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.ChangeWorkingShiftID = ChangeWorkingShiftID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteAddOnsExpenseByID();
                Common.addlog("Working Shift Rejected", "HR", "Working Shift of ID\"" + HttpContext.Current.Items["ChangeWorkingShiftID"].ToString() + "\" Working Shift Rejected", "WorkingShiftChange", ChangeWorkingShiftID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Working Shift Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        private void BindWorkingShift()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlWorkingShift.DataSource = objDB.GetApproveAllWorkingShiftsByCompanyID(ref errorMsg);
                ddlWorkingShift.DataTextField = "ShiftName";
                ddlWorkingShift.DataValueField = "ShiftID";
                ddlWorkingShift.DataBind();
                ddlWorkingShift.Items.Insert(0, new ListItem("--- Select Shift  ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

    }
}