﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard2.aspx.cs" Inherits="Technofinancials.PeopleManagement.Dashboard2" %>


<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="https://mannatthemes.com/annex/vertical/assets/plugins/chartist/css/chartist.min.css">
      <style>
        div#container {
    height: 395px;
    padding-top:10px;
}
        .anychart-credits {
            display: none;
        }
            /*#pieChartAm {
                width: 100%;
                height: 500px;
            }

            #manChartAm4 {
                width: 100%;
                height: 500px;
            }

            #columnChartImages {
                width: 100%;
                height: 500px;
            }

            #solidGuage {
                width: 100%;
                height: 500px;
            }

            #chart3D {
                width: 100%;
                height: 370px;
            }

            #punchCard {
                width: 100%;
                height: 500px;
            }

            #scatterSingleAxis {
                width: 100%;
                height: 500px;
            }*/
            .m-l-10 h3 {
    text-align: left;
    margin-top:0px;
}
            .panel-heading {
/*    border-color: #eff2f7;
*/    font-size: 15px;
    font-weight: 700;
/*    background: #fafafa;
*/    text-transform: uppercase;
    padding: 15px;
}
            #chartdiv {
                width: 100%;
                height: 370px;
            }
            span#lblCol3 {
                word-break: break-word;
            }
            .orange {
    background: #fa8564 !important;
}
            .pink {
    background: #a48ad4 !important;
}
            .mini-stat .green {
    background: #aec785 !important;
}
                 .mini-stat-icon.yellow {
    background-color: #f9c851;
}
            .D_NONE {
                display: none !important;
            }

            #backButton {
                position: absolute;
                top: 35px;
                right: 35px;
                cursor: pointer;
            }

            .invisible {
                display: none;
            }
            .form-group h4, label{
                margin: 20px 0 10px;
            }
            #chartContainerNew01 .canvasjs-chart-toolbar,
            #chartContainerBar02 .canvasjs-chart-toolbar {
                display: none;
            }
            .mini-stat-icon i {
    font-size: 25px;
}
            .gray {
                background: #dee2e6;
                border-radius: 20px;
                margin: 0 auto;
                text-align: center;
                padding: 5px;
                margin-top: 20px;
            }

            .chart_heading {
                text-align: center;
                margin: 0 0 13px;
                padding-top: 20px;
                font-size: 20px;
                font-weight: 700;
                color: #333;
            }

            .removetrial {
                background: #dee2e6;
                height: 20px;
                width: 95px;
                border-radius: 9px;
                position: absolute;
                left: 12px;
                bottom: 16px;
                z-index: 1;
            }

            .removetrial2 {
                background: #dee2e6;
                height: 20px;
                width: 95px;
                border-radius: 9px;
                position: absolute;
                bottom: 5px;
                left: 19px;
                z-index: 1;
            }


            a.canvasjs-chart-credit {
                display: none;
            }

            .table {
                color: black;
                margin-bottom: 0;
            }
            .card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-clip: border-box;
    border-radius: .25rem;
}
            .align-self-center {
    -ms-flex-item-align: center!important;
    align-self: center!important;
}
            .flex-row {
    -ms-flex-direction: row!important;
    flex-direction: row!important;
}
            .round {
    line-height: 60px;
    color: #5b6be8;
    width: 60px;
    height: 60px;
    font-size: 26px;
    display: inline-block;
    font-weight: 400;
    border: 3px solid #f8f8fe;
    text-align: center;
    border-radius: 50%;
    background: #e1e4fb;
}
            .mini-stat-icon.purple {
    background-color: #f4b9b9;
}
            .m-l-10 h5 {
    font-size: 24px;
    color: #767676 !important;
}
            .text-muted {
    opacity: 1;
    text-align: center;
    margin-bottom: 0px;
    font-size: 11px;
    padding-top: 2px;
    color: #767676 !important;
}
.mini-stat {
    background: #fff;
    padding: 17px;
    box-shadow: 0 2px 3px 0px #ccc;
    margin-bottom: 20px;
}
            .m-l-10 h5, .m-l-10 p {
    text-align: left;
}
            .card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1rem;
}
            .removetrialMale {
                /*background: #e8e8e8;*/
                background: #ffffff;
                height: 40px;
                width: 95px;
                border-radius: 9px;
                position: absolute;
                left: 22px;
                bottom: 34px;
                z-index: 1;
                margin: 4px !important;
            }

            .map_radius .widget-body {
                padding: 0 1rem;
            }

            .table > thead > tr > th {
                /*background-color: #e8e8e8;*/
                background-color: #ffffff;
                border-bottom: 2px solid #000000 !important;
                color: #000;
            }

            .canvasjs-chart-toolbar {
                display: none;
            }
            /*.table tr:nth-child(odd) {
                background: #fff;
            }

            .table tr:nth-child(even) {
                background: #f2f2f2;
            }*/
            .anounceTab th:nth-child(1) {
                width: 30px;
                text-align: center;
            }

            .anounceTab th:nth-child(2) {
                width: 160px;
            }

            .anounceTab th:nth-child(3) {
                width: 200px;
            }

            .anounceTab {
                height: 370px;
                padding-right: 5px;
            }



            #style-1::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #F5F5F5;
            }

            #style-1::-webkit-scrollbar {
                width: 7px;
                background-color: #F5F5F5;
            }

            #style-1::-webkit-scrollbar-thumb {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
                background-color: #003780;
            }
            .slimScrollBar {
                background: #fff !important;
                width: 10px !important;
            }

            .header-fav-tab {
                display: none !important;
            }

            .menubar-scroll-inner.tf-sidebar-menu-items {
                overflow: scroll;
                overflow-x: hidden;
                overflow-y: scroll;
                height: 85vh !important;
            }

            /*body::-webkit-scrollbar {
                width: 1em;
            }

            body::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            }

            body::-webkit-scrollbar-thumb {
                background-color: darkgrey;
                outline: 1px solid slategrey;
            }*/

            .slimScrollDiv {
                height: 85vh !important;
            }
            .table > thead:first-child > tr:first-child > th{
                border-top-style: none !important;
            }

            g[aria-labelledby="id-479-title"] {
                display: none;
            }

            g[aria-labelledby="id-318-title"] {
                display: none;
            }

            g[aria-labelledby="id-68-title"] {
                display: none;
            }

            g[aria-labelledby="id-214-title"] {
                display: none;
            }
        @media only screen and (max-width: 1599px) and (min-width: 1201px) {
            .mini-stat {
                padding: 10px 8px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
                font-size:9px;
            }
            .mini-stat-icon.purple img {
                width: 50%;
            }
            .mini-stat-icon {
                width: 40px !important;
                height: 40px !important;
                line-height: 33px !important;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }

        @media only screen and (max-width: 1200px) and (min-width: 1025px) {
            .mini-stat {
                padding: 10px 8px;
            }
            .mini-stat-icon.purple img {
    width: 50%;
}
            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
                    font-size: 7px;
            }

            .mini-stat-icon {
                width: 40px !important;
                height: 40px !important;
                line-height: 33px !important;
            }

                .mini-stat-icon i {
                    font-size: 19px;
                }
        }

        @media only screen and (max-width: 1024px) and (min-width: 768px) {
            .mini-stat {
                padding: 10px 11px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }

        @media only screen and (max-width: 1024px) and (min-width: 992px) {
            .mini-stat {
                padding: 10px 11px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }

        @media only screen and (max-width: 991px) and (min-width: 768px) {
            .mini-stat {
                padding: 10px 11px;
            }

            .m-l-10 h3 {
                font-size: 18px;
                margin-bottom: 1px;
            }

            .text-muted {
                padding-top: 0px;
            }

            .mini-stat-icon {
                width: 45px;
                height: 45px;
                line-height: 40px;
            }

                .mini-stat-icon i {
                    font-size: 21px;
                }
        }
            @media only screen and (max-width: 1439px) and (min-width: 1201px) {
                td {
                    max-width: 50px !important;
                }
            }

            @media only screen and (max-width: 1200px) and (min-width: 1025px) {
                td {
                    max-width: 50px !important;
                }
            }

            @media only screen and (max-width: 1024px) and (min-width: 992px) {
                td {
                    max-width: 50px !important;
                }

                .widget.map_radius.new {
                    height: 470px !important;
                }
            }

            @media only screen and (max-width: 991px) and (min-width: 768px) {
                td {
                    max-width: 50px !important;
                }
            }
            /*@media only screen and (max-width: 991px) and (min-width: 768px){
                td: before{*/
                /* Now like a table header */
                /*position: absolute;*/
                /* Top/left values mimic padding */
                /*top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
            }
            }*/
            .mini-stat-icon {
    width: 50px;
    height: 50px;
    display: inline-block;
    line-height: 45px;
    text-align: center;
    font-size: 30px;
    background: #eee;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    float: left;
    margin-right: 10px;
    color: #fff;
}
            .tar {
    background: #1fb5ac !important;
}
            .mini-stat-info {
    font-size: 11px;
    padding-top: 2px;
    color: #767676 !important;
    font-weight: 700;
}
            .panel-heading {
/*    border-color: #eff2f7;*/
    font-size: 15px;
    font-weight: 700;
/*    background: #fafafa;
*/    text-transform: uppercase;
    padding: 15px;
    color:#767676 !important
}

        .map_radius {
            border-radius: 0px;
            box-shadow: 0 2px 3px 0px #ccc;
        }
            .panel-heading {
/*    border-bottom: 1px solid #eff2f7;
*/    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
}
        .pr-0 {
            padding-right: 0px !important;
        }
canvas#myChart {
    width: 400px !important;
    height: 400px !important;
    margin: 0 0 0 15%;
}
        </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <h1 class="m-0 text-dark">Human Capital Management</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4">
                            <div style="text-align: right;">
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <div class="wrap">
                <section class="app-content">
                                        <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                              <div class="row">
                                <!-- small box -->
                                <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employees"); %>">
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-icon orange">
                                                <i class="fa fa-users" aria-hidden="true"></i>
                                            </div>
                                            <div class="mini-stat-info">
                                                <div class="d-flex flex-row">
                                                        <div class="m-l-10">
                                                            <h3 runat="server" id="hEmployees">0</h3>
                                                            <p class="mb-0 text-muted">Employee Heads</p>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                                    <!-- small box -->
                                    <div class="mini-stat clearfix">
                                        <div class="mini-stat-icon pink">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </div>
                                        <div class="mini-stat-info">
                                            <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="hCurrMonthPayroll">0.00</h3>
                                                        <p class="mb-0 text-muted">Current Month Payroll</p>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                                    <!-- small box -->
                                    <div class="mini-stat clearfix">
                                        <div class="mini-stat-icon green">
                                            <i class="fa fa-usd" aria-hidden="true"></i>
                                        </div>
                                        <div class="mini-stat-info">
                                            <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" id="hLastMonthPayroll">0.00</h3>
                                                        <p class="mb-0 text-muted">Last Month Payroll</p>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                                    <!-- small box -->
                                    <div class="mini-stat clearfix">
                                        <div class="mini-stat-icon tar">
                                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="mini-stat-info">
                                            <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server" style="display:none;" id="hAttendanceRate">0.00%</h3>
                                                        <h3>0.00%</h3>
                                                        <p class="mb-0 text-muted">Turnover Rate</p>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                                    <!-- small box -->
                                    <div class="mini-stat clearfix">
                                        <div class="mini-stat-icon yellow">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                        </div>
                                        <div class="mini-stat-info">
                                            <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server">0</h3>
                                                        <p class="mb-0 text-muted">New Joiners</p>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6 col-6 pr-0">
                                    <!-- small box -->
                                    <div class="mini-stat clearfix">
                                        <div class="mini-stat-icon purple">
                                            <img src="/assets/images/leave.png">
                                        </div>
                                        <div class="mini-stat-info">
                                            <div class="d-flex flex-row">
                                                    <div class="m-l-10">
                                                        <h3 runat="server">0</h3>
                                                        <p class="mb-0 text-muted">Leavers</p>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ./col -->
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="widget map_radius announcment_area" style="height: 460px;">
                                <header class="panel-heading">
                                    Announcements <span class="tools pull-right"></span>
                                </header>
                                <!-- .widget-header -->
<%--                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Title <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtAnnouncementTitle" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input class="form-control" id="txtAnnouncementTitle" placeholder="Title" type="text" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Date <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="text" data-plugin="datetimepicker" class="form-control" data-date-format="DD-MMM-YYYY" id="txtDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <h4>Description <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtDescription" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <textarea rows="2" class="form-control" id="txtDescription" placeholder="Description" type="number" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="padding-left: 0px; text-align: center;">
                                            <button style="margin: 25px 5px 5px 0px;" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button" class="AD_btn_inn">Add</button>
                                            <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Announcement"); %>">
                                                <button type="button" style="margin: 25px 0px 5px 0px;" class="AD_btn_inn">View</button>
                                            </a>
                                        </div>
                                        <div class="col-md-8 col-md-offset-2">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                            <span>
                                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                            </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: none;">
                                    <%--                                       <div class="clearfix">&nbsp;</div>
                                    <asp:GridView ID="gvAnnouncement" runat="server" CssClass="table table-bordered  dataTable no-footer" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="#">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Announcement Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol6" Text='<%# Eval("Date") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Title">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("AnnouncementTitle") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Description")  %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>


                                    <table class="table" style="display: none;">
                                        <tbody>
                                            <tr>
                                                <th>#</th>
                                                <th>Announcement Date</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>30-Aug-2019</td>
                                                <td>Lorem Ipsum</td>
                                                <td>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</td>
                                            </tr>

                                            <tr>
                                                <td>1</td>
                                                <td>30-Aug-2019</td>
                                                <td>Lorem Ipsum</td>
                                                <td>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</td>
                                            </tr>


                                        </tbody>
                                    </table>

                                </div>--%>
                                <!-- .widget-body -->
                            </div>
                            <!-- .widget -->
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="widget map_radius announcment_area" style="height: 460px;">
                                <header class="panel-heading">
                                    MALE VS FEMALE RATIO <span class="tools pull-right"></span>
                                </header>
                                <div id="chart">
                                    <canvas id="myChart" width="400" height="400"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .widget -->



                </section>
            </div>
            <!-- #dash-content -->

            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <asp:Literal ID="ltrDesignationWiseChart" runat="server"></asp:Literal>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js"></script>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script>
            var data = {
                datasets: [{
                    data: [
                        11,
                        16,
                        7,
                        3,
                        14
                    ],
                    backgroundColor: [
                        "#FF6384",
                        "#4BC0C0",
                        "#FFCE56",
                        "#E7E9ED",
                        "#36A2EB"
                    ],
                    label: 'My dataset' // for legend
                }],
                labels: [
                    "Red",
                    "Green",
                    "Yellow",
                    "Grey",
                    "Blue"
                ]
            };
            var ctx = $("#myChart");
            new Chart(ctx, {
                data: data,
                type: 'polarArea'
            });
        </script>
    </form>
</body>
</html>
