﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="Technofinancials.People_Management.Reports" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         <img src="/assets/images/hr-09.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="text-uppercase tf-page-heading-text">Reports</h3>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">Promotion Report</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                          <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">New Hiring Report</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                          <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">New Requirment Report</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                          <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">Root Cause Analysis</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                          <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">360&deg; Survey</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">Department Wise Report</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">Trainings Report</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                        
                          <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">Tests Report</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                         
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                         <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">Special KPIs Report</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">Report Name</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                          <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">Report Name</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                        
                         <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <div class="alert alert-success alert-custom alert-dismissible tf-tile tf-border-blue">
								<h4 class="alert-title">Report Name</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam</p>
							</div>
                        </div>
                         
                         
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
</body>
</html>