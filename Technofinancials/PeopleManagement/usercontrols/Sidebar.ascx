﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Sidebar.ascx.cs" Inherits="Technofinancials.People_Management.usercontrols.Sidebar" %>
<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar tf-sidebar">

	<asp:HiddenField ID="currentWDID" Value="" runat="server" />
	<div class="menubar-scroll">
		<div class="menubar-scroll-inner tf-sidebar-menu-items">
			<ul class="app-menu">
				<li class="tf-first-menu">
					<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/dashboard"); %>">
						<img src="/assets/images/dashboard__2.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Dashboard</span>
					</a>
				</li>
				<li>
					<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employees"); %>">
						<img src="/assets/images/hr-04__2.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Personnel Management</span>
					</a>
				</li>
				  <li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/teams"); %>">
                        <img src="/assets/images/users.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Managers</span>
                    </a>
                </li>
				
				<li class="has-submenu">
					<a href="javascript:void(0)" class="submenu-toggle">
						<i class="fa fa-calendar-check-o img-responsive tf-sidebar-icons" aria-hidden="true"></i>
						<span class="menu-text">Attendance</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
					<ul class="submenu">
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Employee-attendance-lock"); %>"><span class="menu-text">Lock Attendance For Payroll</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/attendance-adjustment"); %>"><span class="menu-text">Attendance Adjustment</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/break-adjustment"); %>"><span class="menu-text">Break Adjustments</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/tracking"); %>"><span class="menu-text">Tracking</span></a></li>
					<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/balance-leaves"); %>"><span class="menu-text">Leaves Data</span></a></li>
					</ul>
				</li>
				  <% if(currentWDID.Value == "100865") { %>
				<li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/loansNadvance"); %>">
                        <img src="/assets/images/Assets__03.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Loan & Advance</span>
                    </a>
                </li>
				  <% } %>
				<li class="has-submenu ">
					<a href="javascript:void(0)" class="submenu-toggle">
						<i class="fa fa-handshake-o img-responsive tf-sidebar-icons" aria-hidden="true"></i>
						<%--						<img src="/assets/images/reports-icon-02.png" class="img-responsive tf-sidebar-icons" />--%>
						<span class="menu-text">FnF Settlement</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
					<ul class="submenu">
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employee-clearance"); %>"><span class="menu-text">Employee Clearance Approvals</span></a></li>

					</ul>
				</li>

				<li class="has-submenu ">
					<a href="javascript:void(0)" class="submenu-toggle">
						<img src="/assets/images/reports-icon-02.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Reports</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
						
					<ul class="submenu">
						<li>
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/audit"); %>">
                    
                        <span class="menu-text">Authorization Logs</span>
                    </a>
                </li>
						
						<li class="has-submenu">
							<a href="javascript:void(0)" class="submenu-toggle">
								<%--<img src="/assets/images/hr-04__2.png" class="img-responsive tf-sidebar-icons" />--%>
								<span class="menu-text">Attendance Reports</span>
								<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
							</a>
							<ul class="submenu">
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/attendance-sheet"); %>">
										<%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
										<span class="menu-text">Attendance Sheet</span>
									</a>
								</li>
								<%--<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/attendance-summmary"); %>">
										--%><%--<img src="/assets/images/Attendance__03.png" class="img-responsive tf-sidebar-icons" />--%>
<%--										<span class="menu-text">Attendance Summary</span>
									</a>
								</li>--%>
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Flag-wise-Employees"); %>">
										<span class="menu-text">Flag-Wise Employees Attendance Report</span>
									</a>
								</li>
							</ul>
						</li>
						<li class="has-submenu">
							<a href="javascript:void(0)" class="submenu-toggle">
								<%--<img src="/assets/images/hr-04__2.png" class="img-responsive tf-sidebar-icons" />--%>
								<span class="menu-text">Employees Reports</span>
								<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
							</a>
							<ul class="submenu">
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Employee-Roaster"); %>">
										<span class="menu-text">Shift Wise Employees Report</span>
									</a>
								</li>
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Employee-Leaves"); %>">
										<span class="menu-text">Employee Leaves Report</span>
									</a>
								</li>
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Left-Employees"); %>">
										<span class="menu-text">Left Employees Report</span>
									</a>
								</li>
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Absent-Employees"); %>">
										<span class="menu-text">Absent Employees Report</span>
									</a>
								</li>
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/New-Joiners"); %>">
										<span class="menu-text">New Recruits Report</span>
									</a>
								</li>
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/MaleFemale-Ratio-Department-Wise"); %>">
										<span class="menu-text">Department Wise Male/Female Ratio</span>
									</a>
								</li>
								<li>
									<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Turnover-Report-Department-Wise"); %>">
										<span class="menu-text">Department Wise Turnover</span>
									</a>
								</li>

							</ul>
						</li>
						<li>
							<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/pending-request"); %>">
								<span class="menu-text">Pending Requests for Managers</span>
							</a>
						</li>
					</ul>
				</li>
					<li class="has-submenu">
							<a href="javascript:void(0)" class="submenu-toggle">
				
						<img src="/assets/images/payslip.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Payslip</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>
					<ul class="submenu">
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/generate-pay-slip"); %>"><span class="menu-text">Generate Payslip</span></a></li>
						<%--<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/generate-minipay-slip"); %>"><span class="menu-text">Generate Mini-Payslip</span></a></li>--%>
					</ul>
				</li>
				<li>
					<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Announcement"); %>">
						<img src="/assets/images/white/icon_announcment.png" class="img-responsive tf-sidebar-icons" />

						<span class="menu-text">Announcements</span>
					</a>
				</li>
				<li>
					<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/ChangeWorkingShift"); %>">
						<i class="fa fa-clock-o img-responsive tf-sidebar-icons" aria-hidden="true"></i>
						<span class="menu-text">Change Shifts</span>
					</a>
				</li>

				<li class="has-submenu ">
					<a href="javascript:void(0)" class="submenu-toggle">
						<img src="/assets/images/Admin_setting.png" class="img-responsive tf-sidebar-icons" />
						<span class="menu-text">Settings</span>
						<i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
					</a>

					<ul class="submenu">
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/shifts"); %>"><span class="menu-text">Shifts</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/holidays"); %>"><span class="menu-text">Holidays</span></a></li>
<%--						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/AddOns"); %>"><span class="menu-text">Add-ons Heads</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/deduction-head"); %>"><span class="menu-text">Deduction Heads</span></a></li>--%>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/PolicyProcedures"); %>"><span class="menu-text">Policies & Procedures</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/clearance-items"); %>"><span class="menu-text">Clearance Items</span></a></li>
						<li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/auto-attendance"); %>"><span class="menu-text">Auto Attendance</span></a></li>
					</ul>
				</li>
			</ul>
			<!-- .app-menu -->
		</div>
		<!-- .menubar-scroll-inner -->
	</div>
	<div class="tf-vno">tf-v2.9 <%Response.Write(DateTime.Now.ToString("MM-dd-yyyy")); %></div>
	<!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->
