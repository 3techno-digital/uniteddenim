﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.People_Management.usercontrols
{
 
    public partial class Sidebar : System.Web.UI.UserControl
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            currentWDID.Value = objDB.GetWorkdayID(ref errorMsg).Rows[0][0].ToString();
        }
    }
}