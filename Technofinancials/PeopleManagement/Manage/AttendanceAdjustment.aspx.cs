﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.PeopleManagement.Manage
{
    public partial class AttendanceAdjustment : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int AttendanceAdjustmentID
        {
            get
            {
                if (ViewState["AttendanceAdjustmentID"] != null)
                {
                    return (int)ViewState["AttendanceAdjustmentID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AttendanceAdjustmentID"] = value;
            }
        }

        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return "Saved as Draft";
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["AttendanceAdjustmentID"] = null;
                    BindEmployeeDropdown();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/attendance-adjustment";

                    btnApprove.Visible =
                    btnReview.Visible =
                    btnRevApprove.Visible =
                    lnkReject.Visible =
                    lnkDelete.Visible =
                    btnSubForReview.Visible =
                    btnDisapprove.Visible = false;

                    divAlertMsg.Visible = false;
                    clearFields();
                    txtDescription.Disabled = false;
                    if (HttpContext.Current.Items["AttendanceAdjustmentID"] != null)
                    {
                        AttendanceAdjustmentID = Convert.ToInt32(HttpContext.Current.Items["AttendanceAdjustmentID"].ToString());
                        getAttendanceAdjustmentByID(AttendanceAdjustmentID);
                        CheckAccess();
                        txtDescription.Disabled = true;
                        //ddlEmployee.Enabled = false;
                        //txtStartTime.Disabled =
                        //txtEndTime.Disabled =
                        //txtDate.Disabled =
                        //txtDescription.Disabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = btnApprove.Visible = btnReview.Visible = btnRevApprove.Visible = lnkReject.Visible = 
                lnkDelete.Visible = btnSubForReview.Visible = btnDisapprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "AttendanceAdjustment";
                objDB.PrimaryColumnnName = "AttendanceAdjustmentID";
                objDB.PrimaryColumnValue = AttendanceAdjustmentID.ToString();
                objDB.DocName = "Loans";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = lnkDelete.Visible = btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = btnReview.Visible = lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = btnApprove.Visible = btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = btnRevApprove.Visible = lnkReject.Visible= true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    //btnDisapprove.Visible = true;
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getAttendanceAdjustmentByID(int AttendanceAdjustmentID)
        {
            try
            {

                DataTable dt = new DataTable();
                objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                dt = objDB.GetAttendanceAdjustmentByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        DocStatus = dt.Rows[0]["DocStatus"].ToString();
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        txtDescription.Value = dt.Rows[0]["Resultant"].ToString();
                        txtStartTime.Value = DateTime.Parse(dt.Rows[0]["TimeIn"].ToString()).ToString("hh:mm tt");
                        txtEndTime.Value = DateTime.Parse(dt.Rows[0]["TimeOut"].ToString()).ToString("hh:mm tt");
                        //isNextDay
                        //if (dt.Rows[0]["isNextDay"].ToString() == "True")
                        //{
                        //    isNextDay.Checked = true;
                        //}
                        //else
                        //{
                        //    isNextDay.Checked = false;
                        //}
                        txtDate.Value = DateTime.Parse(dt.Rows[0]["AttendanceDate"].ToString()).ToString("dd-MMM-yyyy"); ;

                    }
                }
                Common.addlog("View", "HR", "AttendanceAdjustment \"" + ddlEmployee.SelectedItem.Text + "\" Viewed", "AttendanceAdjustment", AttendanceAdjustmentID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";

                string today = DateTime.Parse(txtDate.Value).ToShortDateString();
                string tommorow = DateTime.Parse(txtDate.Value).AddDays(1).ToShortDateString();
                DateTime Fromdate = DateTime.Parse(txtStartTime.Value), Todate = DateTime.Parse(txtEndTime.Value);
                DateTime Date = DateTime.Parse(txtDate.Value);
                Fromdate = DateTime.Parse(Date.ToString("dd-MMM-yyyy") + " " + Fromdate.ToString("hh:mm:ss tt"));
                Todate = DateTime.Parse(Date.ToString("dd-MMM-yyyy") + " " + Todate.ToString("hh:mm:ss tt"));
                Todate = Todate < Fromdate ? Todate.AddDays(1) : Todate;

                //if (!(DateTime.Parse(txtStartTime.Value) <= DateTime.Parse(txtEndTime.Value)))
                //{
                //    divAlertMsg.Visible = true;
                //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //    pAlertMsg.InnerHtml = "Invalid Timing";
                //    return;
                //}
                double hours = (Todate - Fromdate).TotalHours;
                if (hours > 16)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Adjustment should not exceed 16 hours";
                    return;
                }
                else
                {
                    objDB.Date = Date.ToString("dd-MMM-yyyy");
                    objDB.StartDate = Fromdate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                    objDB.EndDate = Todate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                    objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                    //string result = objDB.GetEmployeeShiftStartTime();
                    //if (result != "TRUE")
                    //{
                    //    divAlertMsg.Visible = true;
                    //    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    //    pAlertMsg.InnerHtml = result;
                    //    return;
                    //}


                }


                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.TimeIn = Fromdate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                objDB.TimeOUt = Todate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                objDB.BreakStartTime = "";
                objDB.BreakEndTime = "";
                objDB.Resultant = txtDescription.Value;
                objDB.Date = Date.ToString("dd-MMM-yyyy");



                //objDB.isNextDay = isNextDay.Checked;

                if (HttpContext.Current.Items["AttendanceAdjustmentID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                    res = objDB.UpdateAttendanceAdjustment();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddAttendanceAdjustment();


                    clearFields();
                }


                if (res == "New Attendance Adjustment Added" || res == "Attendance Adjustment Data Updated")
                {
                    if (res == "New AttendanceAdjustment Added") { 
                        Common.addlog("Add", "HR", "New AttendanceAdjustment \"" + objDB.Title + "\" Added", "AttendanceAdjustment");
                        
                        
                    }
                    if (res == "AttendanceAdjustment Data Updated") { 
                        Common.addlog("Update", "HR", "AttendanceAdjustment \"" + objDB.Title + "\" Updated", "AttendanceAdjustment", AttendanceAdjustmentID); 
                    }

                    if (DocStatus == "Approved")
                    {
                        objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.ApprovedBy = Session["UserName"].ToString();
                        objDB.ApproveAttendanceAdjustment();
                        res = "Attendance Adjustment Saved and Approved Successfully";
                    }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            ddlEmployee.SelectedIndex = -1;
            txtStartTime.Value =
            txtEndTime.Value =
            txtDate.Value = 
            txtDescription.Value= "";
        }

        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "AttendanceAdjustment", "AttendanceAdjustmentID", HttpContext.Current.Items["AttendanceAdjustmentID"].ToString(), Session["UserName"].ToString());
                int ess_id = Convert.ToInt32(ddlEmployee.SelectedValue.ToString());
                //Common.addlogNew(res, "HR", "AttendanceAdjustment of ID\"" + HttpContext.Current.Items["AttendanceAdjustmentID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/attendance-adjustment", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/attendance-adjustment/edit-attendance-adjustment-" + HttpContext.Current.Items["AttendanceAdjustmentID"].ToString(), "AttendanceAdjustment for Employee \"" + ddlEmployee.SelectedValue + "\"", "AttendanceAdjustment", "Loans", Convert.ToInt32(HttpContext.Current.Items["AttendanceAdjustmentID"].ToString()));
                
                Common.addlogNew(res, "AttendanceAdjustment",
               "Attendance Adjustment \"" + objDB.Title + "\" Status",
               "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + AttendanceAdjustmentID.ToString(),
               "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + AttendanceAdjustmentID.ToString(), "Attendance Adjustment", "AttendanceAdjustment", "ESS", Convert.ToInt32(ess_id));

                if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                {
                    DocStatus = "Approved";
                    objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.ApprovedBy = Session["UserName"].ToString();
                    objDB.ApproveAttendanceAdjustment();

                }
                else if (res == "Rejected & Disapproved Sucessfull" || res == "Disapproved" || res == "Rejected" || res == "Disapproved")
                {
                    objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                    objDB.EnableAttendanceAdjustment();
                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res.Replace("Sucessfull", "Successfully");

                CheckAccess();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "AttendanceAdjustment", "AttendanceAdjustmentID", HttpContext.Current.Items["AttendanceAdjustmentID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "AttendanceAdjustment of ID \"" + HttpContext.Current.Items["AttendanceAdjustmentID"].ToString() + "\" deleted", "AttendanceAdjustment", AttendanceAdjustmentID);
                if (res == "Deleted Succesfully" || res == "Rejected")
                {
                    objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                    objDB.EnableAttendanceAdjustment();
                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res;

                if (string.IsNullOrEmpty(txtAdjustmentNote.Value))
                {
                    res = "Please add a rejection note.";
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Please add a rejection note.";
                    return;
                }

                LinkButton btn = new LinkButton();
                res = Common.addAccessLevels("Reject", "AttendanceAdjustment", "AttendanceAdjustmentID", HttpContext.Current.Items["AttendanceAdjustmentID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "AttendanceAdjustment of ID \"" + HttpContext.Current.Items["AttendanceAdjustmentID"].ToString() + "\" deleted", "AttendanceAdjustment", AttendanceAdjustmentID);

                if (res == "Rejected")
                {
                    objDB.AttendanceAdjustmentID = AttendanceAdjustmentID;
                    objDB.AttendanceAdjustmentRemarks = txtAdjustmentNote.Value;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.RejectAttendanceAdjustment();
                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnDisapprove_ServerClick(object sender, EventArgs e)
        {

        }
    }
}