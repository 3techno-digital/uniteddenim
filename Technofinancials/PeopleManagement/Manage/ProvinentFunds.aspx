﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProvinentFunds.aspx.cs" Inherits="Technofinancials.PeopleManagement.ProvinentFunds" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/hr-02.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Provinent Funds</h3>
                        </div>
                        <div class="col-md-4" style="margin-top: 10px;">
                            <div class="form-group" id="divAlertMsg" runat="server">
                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                    <span>
                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                    </span>
                                    <p id="pAlertMsg" runat="server">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" style="margin-top: 10px;">
                            <div class="pull-right">
                                <button class="tf-save-btn" data-toggle="tooltip" title="Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                <%--   <a class="tf-back-btn" data-toggle="tooltip" title="Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>--%>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>

                 
                  
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h4>Company Ratio</h4>
                                    <input type="text" class="form-control" name="companyRatio" id="txtCompanyRatio" runat="server" />

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h4>Employee Ratio</h4>
                                    <input type="text" class="form-control" name="employeeRatio" id="txtEmployeeRatio" runat="server" />

                                </div>
                            </div>

                        </div>
                       <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>

                      <div class="row">
                                        <div class="col-lg-12">
                                            <h4>Provinent Funnds Summary
                                                
                                                <%--<button class="tf-add-btn pull-right" data-toggle="modal" data-target="#experience-modal" type="button" data-original-title="Add" title="Add"><i class="far fa-plus-circle"></i></button>--%>
                                            </h4>
                                            <asp:GridView ID="gv" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false">
                                                <Columns>
                                                   
                                                    <asp:TemplateField HeaderText="Company Investments">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("CompanyAmount") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Employee Investments">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblOrganization" Text='<%# Eval("EmployeeAmount") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="Total Investments">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblJoiningYear" Text='<%# Eval("TotalAmount") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Withdraw">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblJoiningMonth" Text='<%# Eval("WithdrawAmount") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Remaining">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblYear" Text='<%# Eval("RemaningAmount") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                  
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                    

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
</body>
</html>
