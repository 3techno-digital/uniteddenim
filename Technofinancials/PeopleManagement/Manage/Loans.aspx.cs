﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class Loans : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int loanID
        {
            get
            {
                if (ViewState["loanID"] != null)
                {
                    return (int)ViewState["loanID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["loanID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["loanID"] = null;
                    BindEmployeeDropdown();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/loans";

                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;


                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["LoanID"] != null)
                    {
                        loanID = Convert.ToInt32(HttpContext.Current.Items["LoanID"].ToString());
                        getLoanByID(loanID);
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;


                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "Loans";
                objDB.PrimaryColumnnName = "LoanID";
                objDB.PrimaryColumnValue = loanID.ToString();
                objDB.DocName = "loans";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getLoanByID(int LoanID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.LoanID = LoanID;
                dt = objDB.GetEmployeeLoanByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        txtLoanTitle.Value = dt.Rows[0]["LoanTitle"].ToString();
                        txtLoanAmount.Value = dt.Rows[0]["LoanAmount"].ToString();
                        txtGrantDate.Value = DateTime.Parse(dt.Rows[0]["GrantDate"].ToString()).ToString("dd-MMM-yyyy"); 
                        txtNoOfInstallments.Value = dt.Rows[0]["NoOfInstallments"].ToString();
                        txtNotes.Value = dt.Rows[0]["LoanNotes"].ToString();

                        objDB.DocID = LoanID;
                        objDB.DocType = "loans";
                        ltrNotesTable.Text = objDB.GetDocNotes();
                    }
                }
                Common.addlog("View", "HR", "Loan \"" + txtLoanTitle.Value + "\" Viewed", "Loan", loanID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";

                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedItem.Value);
                objDB.LoanTitle = txtLoanTitle.Value;
                objDB.LoanAmount = float.Parse(txtLoanAmount.Value);
                objDB.LoanGrantDate = txtGrantDate.Value;
                objDB.NoOfInstallments = int.Parse(txtNoOfInstallments.Value);
                objDB.DeductionPerSalary = float.Parse(Math.Round(double.Parse(txtLoanAmount.Value) / double.Parse(txtNoOfInstallments.Value), 0).ToString());
                objDB.LoanNotes = txtNotes.Value;

                if (HttpContext.Current.Items["LoanID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.LoanID = loanID;
                    res = objDB.UpdateEmployeeLoan();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    int tempID = 0;
                    res = objDB.AddEmployeeLoan();
                    if (int.TryParse(res, out tempID))
                    {
                        res = "New Loan Added";
                        clearFields();
                       
                    }
                    loanID = tempID;

                }
                
                if (res == "New Loan Added" || res == "Loan Data Updated")
                {
                    objDB.DocType = "loans";
                    objDB.DocID = loanID;
                    objDB.Notes = txtNotes.Value;
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.AddDocNotes();

                    if (res == "New Loan Added") { Common.addlog("Add", "HR", "New Loan \"" + objDB.LoanTitle + "\" Added", "Loans"); }
                    if (res == "Loan Data Updated") { Common.addlog("Update", "HR", "Loan \"" + objDB.LoanTitle + "\" Updated", "Loans", loanID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            ddlEmployee.SelectedIndex = 0;
            txtLoanTitle.Value = "";
            txtLoanAmount.Value = "";
            txtGrantDate.Value = "";
            txtNoOfInstallments.Value = "";
            txtNotes.Value = "";
        }

        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "Loan of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/loans", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/loans/edit-loan-" + HttpContext.Current.Items["LoanID"].ToString(), "Loan \"" + txtLoanTitle.Value + "\"", "Loans", "loans", Convert.ToInt32(HttpContext.Current.Items["LoanID"].ToString()));

                //Common.addlog(res, "HR", "Loan of ID\"" + HttpContext.Current.Items["LoanID"].ToString() + "\" Status Changed", "Loans", loanID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;

                CheckAccess();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Loans", "LoanID", HttpContext.Current.Items["LoanID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "Loan of ID \"" + HttpContext.Current.Items["LoanID"].ToString() + "\" deleted", "Loans", loanID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

    }
}