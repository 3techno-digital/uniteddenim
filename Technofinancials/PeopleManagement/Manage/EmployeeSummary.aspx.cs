﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Text;
using SelectPdf;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class EmployeeSummary : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        public int empID = 0;
        public static string refNo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            { 
                divAlertMsg.Visible = false;
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employees";
                if (HttpContext.Current.Items["EmployeeID"] != null)
                {
                    empID = Convert.ToInt32(HttpContext.Current.Items["EmployeeID"].ToString());
                    getDocumentDesign(empID);
                }
            }
        }

        private void getDocumentDesign(int EmployeeID)
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DocType = "Employee Summary";
            dt = objDB.GetDocumentDesign(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                    string content = dt.Rows[0]["DocContent"].ToString();
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();

                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.DocType = "PL";
                    objDB.DeptName = "HR";
                    
                    txtContent.Content = addEmployeeDetails(content, EmployeeID);
                }
            }
        }

        private string addEmployeeDetails(string content, int EmployeeID)
        {
            objDB.EmployeeID = EmployeeID;
            DataTable dt = objDB.GetEmployeeByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    content = content.Replace("##EMPLOYECODE##", dt.Rows[0]["EmployeeCode"].ToString());
                    content = content.Replace("##CNIC##", dt.Rows[0]["CNIC"].ToString());
                    content = content.Replace("##DOB##", dt.Rows[0]["DOB"].ToString());
                    content = content.Replace("##FIRSTNAME##", dt.Rows[0]["EmployeeFirstName"].ToString());
                    content = content.Replace("##MIDDLENAME##", dt.Rows[0]["EmployeeMiddleName"].ToString());
                    content = content.Replace("##LASTNAME##", dt.Rows[0]["EmployeeLastName"].ToString());
                    content = content.Replace("##FATHERNAME##", dt.Rows[0]["FatherName"].ToString());
                    content = content.Replace("##GENDER##", dt.Rows[0]["Gender"].ToString());
                    content = content.Replace("##CONTACTNO##", dt.Rows[0]["ContactNo"].ToString());
                    content = content.Replace("##BLOODGROUP##", dt.Rows[0]["BloodGroup"].ToString());
                    content = content.Replace("##EMAIL##", dt.Rows[0]["Email"].ToString());
                    content = content.Replace("##SECEMAIL##", dt.Rows[0]["SecEmail"].ToString());
                    content = content.Replace("##COUNTRY##", dt.Rows[0]["Country"].ToString());
                    content = content.Replace("##STATE##", dt.Rows[0]["State"].ToString());
                    content = content.Replace("##CITY##", dt.Rows[0]["City"].ToString());
                    content = content.Replace("##ADDRESS##", dt.Rows[0]["AddressLine1"].ToString());
                    content = content.Replace("##ZIPCODE##", dt.Rows[0]["ZIPCode"].ToString());
                    content = content.Replace("##EMPLOYMENTTYPE##", dt.Rows[0]["EmploymentType"].ToString());
                    content = content.Replace("##DOJ##", dt.Rows[0]["DateOfJoining"].ToString());
               //     content = content.Replace("##JOBDESCRIPTION##", dt.Rows[0]["CNIC"].ToString());
                //    content = content.Replace("##GRADE##", dt.Rows[0]["ContactNo"].ToString());
               //     content = content.Replace("##DEPARTMRNT##", dt.Rows[0]["Email"].ToString());
               //     content = content.Replace("##DESIGNATION##", dt.Rows[0]["ContactNo"].ToString());
                    content = content.Replace("##DIRECTREPORTTO##", dt.Rows[0]["DirectReportingTo"].ToString());
                    content = content.Replace("##INDIRECTREPORTTO##", dt.Rows[0]["InDirectReportingTo"].ToString());
                    content = content.Replace("##BASICSALARY##", dt.Rows[0]["BasicSalary"].ToString());
               //     content = content.Replace("##SHIFT##", dt.Rows[0]["Email"].ToString());
                }
            }
            return content;
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Common.generatePDF(txtHeader.Content, txtFooter.Content, txtContent.Content, refNo, "A4", "Portrait");
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}