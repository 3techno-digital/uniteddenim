﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class EmployeeBonus : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int EmployeeBonusID
        {
            get
            {
                if (ViewState["EmployeeBonusID"] != null)
                {
                    return (int)ViewState["EmployeeBonusID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["EmployeeBonusID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["EmployeeBonusID"] = null;
                    BindEmployeeDropdown();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employee-bonus";

                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    ViewState["EmployeeBounusDetailSrNo"] = null;
                    ViewState["dtEmployeeBounusDetail"] = null;
                    ViewState["showEmployeeBounusDetailFirstRow"] = null;
                    dtEmployeeBounusDetail = createEmployeeBounusDetail();
                    BindEmployeeBounusDetailTable();

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["EmployeeBonusID"] != null)
                    {
                        EmployeeBonusID = Convert.ToInt32(HttpContext.Current.Items["EmployeeBonusID"].ToString());
                        getEmployeeBonusByID(EmployeeBonusID);
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;


                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "EmployeeBonus";
                objDB.PrimaryColumnnName = "EmployeeBonusID";
                objDB.PrimaryColumnValue = EmployeeBonusID.ToString();
                objDB.DocName = "Loans";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getEmployeeBonusByID(int EmployeeBonusID)
        {
            try
            {

                DataTable dt = new DataTable();
                objDB.EmployeeBonusID = EmployeeBonusID;
                dt = objDB.GetEmployeeBonusByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtTitle.Value = dt.Rows[0]["Title"].ToString();
                        txtGrantDate.Value = DateTime.Parse(dt.Rows[0]["BounusDate"].ToString()).ToString("dd-MMM-yyyy");
                        txtDescription.Value = dt.Rows[0]["Description"].ToString();
                        getTestInvitationsByTestID(EmployeeBonusID);
                        objDB.DocID = EmployeeBonusID;
                        objDB.DocType = "EmployeeBonus";
                        ltrNotesTable.Text = objDB.GetDocNotes();
                    }
                }
                Common.addlog("View", "HR", "EmployeeBonus \"" + txtTitle.Value + "\" Viewed", "EmployeeBonus", EmployeeBonusID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void getTestInvitationsByTestID(int testID)
        {
            try
            {
                dtEmployeeBounusDetail = null;
                dtEmployeeBounusDetail = new DataTable();
                dtEmployeeBounusDetail = createEmployeeBounusDetail();

                DataTable dt = new DataTable();
                objDB.EmployeeBonusID = testID;
                dt = objDB.GetAllEmployeeBonusDetailByEmployeeBonusID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtEmployeeBounusDetail.Rows[0][0].ToString() == "")
                        {
                            dtEmployeeBounusDetail.Rows[0].Delete();
                            dtEmployeeBounusDetail.AcceptChanges();
                            showEmployeeBounusDetailFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtEmployeeBounusDetail.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["EmployeeID"],
                            dt.Rows[i]["EmployeeName"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                        }
                        EmployeeBounusDetailSrNo = Convert.ToInt32(dtEmployeeBounusDetail.Rows[dtEmployeeBounusDetail.Rows.Count - 1][0].ToString()) + 1;
                    }
                }

                BindEmployeeBounusDetailTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                bool isNew = false;

                objDB.Title = txtTitle.Value;
                objDB.BounusDate = txtGrantDate.Value;
                objDB.Description = txtDescription.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                if (HttpContext.Current.Items["EmployeeBonusID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.EmployeeBonusID = EmployeeBonusID;
                    res = objDB.UpdateEmployeeBonus();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    EmployeeBonusID = Convert.ToInt32(objDB.AddEmployeeBonus());
                    res = "New EmployeeBonus Added";
                    isNew = true;

                    clearFields();
                }


                objDB.EmployeeBonusID = EmployeeBonusID;
                objDB.DeleteEmployeeBonusDetailByEmployeeBonusID();

                dtEmployeeBounusDetail = (DataTable)ViewState["dtEmployeeBounusDetail"] as DataTable;
                if (dtEmployeeBounusDetail != null)
                {
                    if (dtEmployeeBounusDetail.Rows.Count > 0)
                    {
                        if (!showEmployeeBounusDetailFirstRow)
                        {
                            dtEmployeeBounusDetail.Rows[0].Delete();
                            dtEmployeeBounusDetail.AcceptChanges();
                        }

                        for (int i = 0; i < dtEmployeeBounusDetail.Rows.Count; i++)
                        {
                            objDB.EmployeeBonusID = EmployeeBonusID;

                            objDB.EmployeeID = Convert.ToInt32 (dtEmployeeBounusDetail.Rows[i]["EmployeeID"].ToString());
                            objDB.Type = dtEmployeeBounusDetail.Rows[i]["Type"].ToString();
                            objDB.Amount = dtEmployeeBounusDetail.Rows[i]["Amount"].ToString();
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddEmployeeBonusDetail();
                        }
                    }
                }

                if (dtEmployeeBounusDetail.Rows.Count == 0 || isNew)
                {
                    
                    ViewState["EmployeeBounusDetailSrNo"] = null;
                    ViewState["showEmployeeBounusDetailFirstRow"] = null;
                    ViewState["dtEmployeeBounusDetail"] = null;
                }


                BindEmployeeBounusDetailTable();



                objDB.DocType = "EmployeeBonus";
                objDB.DocID = EmployeeBonusID;
                objDB.Notes = Textarea1.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();
                if (res == "New EmployeeBonus Added" || res == "EmployeeBonus Data Updated")
                {
                    if (res == "New EmployeeBonus Added") { Common.addlog("Add", "HR", "New EmployeeBonus \"" + objDB.Title + "\" Added", "EmployeeBonus"); }
                    if (res == "EmployeeBonus Data Updated") { Common.addlog("Update", "HR", "EmployeeBonus \"" + objDB.Title + "\" Updated", "EmployeeBonus", EmployeeBonusID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtTitle.Value = "";
            txtGrantDate.Value = "";
            txtDescription.Value = "";
        }

        private void BindEmployeeDropdown()
        {
            try
            {
                //CheckSessions();
                //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                //ddlEmployee.DataTextField = "EmployeeName";
                //ddlEmployee.DataValueField = "EmployeeID";
                //ddlEmployee.DataBind();
                //ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "EmployeeBonus", "EmployeeBonusID", HttpContext.Current.Items["EmployeeBonusID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "EmployeeBonus of ID\"" + HttpContext.Current.Items["EmployeeBonusID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/employee-bonus", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/employee-bonus/edit-employee-bonus-" + HttpContext.Current.Items["EmployeeBonusID"].ToString(), "EmployeeBonus \"" + txtTitle.Value + "\"", "EmployeeBonus", "Loans", Convert.ToInt32(HttpContext.Current.Items["EmployeeBonusID"].ToString()));

                //Common.addlog(res, "HR", "EmployeeBonus of ID\"" + HttpContext.Current.Items["EmployeeBonusID"].ToString() + "\" Status Changed", "EmployeeBonus", EmployeeBonusID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;

                CheckAccess();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "EmployeeBonus", "EmployeeBonusID", HttpContext.Current.Items["EmployeeBonusID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "EmployeeBonus of ID \"" + HttpContext.Current.Items["EmployeeBonusID"].ToString() + "\" deleted", "EmployeeBonus", EmployeeBonusID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        private DataTable dtEmployeeBounusDetail
        {
            get
            {
                if (ViewState["dtEmployeeBounusDetail"] != null)
                {
                    return (DataTable)ViewState["dtEmployeeBounusDetail"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtEmployeeBounusDetail"] = value;
            }
        }
        bool showEmployeeBounusDetailFirstRow
        {
            get
            {
                if (ViewState["showEmployeeBounusDetailFirstRow"] != null)
                {
                    return (bool)ViewState["showEmployeeBounusDetailFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showEmployeeBounusDetailFirstRow"] = value;
            }
        }
        protected int EmployeeBounusDetailSrNo
        {
            get
            {
                if (ViewState["EmployeeBounusDetailSrNo"] != null)
                {
                    return (int)ViewState["EmployeeBounusDetailSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["EmployeeBounusDetailSrNo"] = value;
            }
        }
        private DataTable createEmployeeBounusDetail()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("EmployeeID");
            dt.Columns.Add("EmployeeName");
            dt.Columns.Add("Type");
            dt.Columns.Add("Amount");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindEmployeeBounusDetailTable()
        {
            try
            {

                if (ViewState["dtEmployeeBounusDetail"] == null)
                {
                    dtEmployeeBounusDetail = createEmployeeBounusDetail();
                    ViewState["dtEmployeeBounusDetail"] = dtEmployeeBounusDetail;
                }

                gvEmployeeBounusDetail.DataSource = dtEmployeeBounusDetail;
                gvEmployeeBounusDetail.DataBind();

                if (showEmployeeBounusDetailFirstRow)
                    gvEmployeeBounusDetail.Rows[0].Visible = true;
                else
                    gvEmployeeBounusDetail.Rows[0].Visible = false;

                gvEmployeeBounusDetail.UseAccessibleHeader = true;
                gvEmployeeBounusDetail.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvEmployeeBounusDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                    txtSrNo.Text = EmployeeBounusDetailSrNo.ToString();


                    DropDownList ddList = (DropDownList)e.Row.FindControl("ddlEmployee");
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                    ddList.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                    ddList.DataTextField = "EmployeeName";
                    ddList.DataValueField = "EmployeeID";
                    ddList.DataBind();
                    ddList.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));



                    //CheckSessions();
                    //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    //ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                    //ddlEmployee.DataTextField = "EmployeeName";
                    //ddlEmployee.DataValueField = "EmployeeID";
                    //ddlEmployee.DataBind();
                    //ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }
        protected void gvEmployeeBounusDetail_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                int index = e.RowIndex;

           //     dtEmployeeBounusDetail.Rows[index].SetField(1, ((DropDownList)gvEmployeeBounusDetail.Rows[e.RowIndex].FindControl("ddlEditEmployee")).SelectedValue);
                dtEmployeeBounusDetail.Rows[index].SetField(3, ((DropDownList)gvEmployeeBounusDetail.Rows[e.RowIndex].FindControl("ddlEditType")).SelectedItem.Text);
                dtEmployeeBounusDetail.Rows[index].SetField(4, ((TextBox)gvEmployeeBounusDetail.Rows[e.RowIndex].FindControl("txtEditAmount")).Text);

                dtEmployeeBounusDetail.AcceptChanges();

                gvEmployeeBounusDetail.EditIndex = -1;
                BindEmployeeBounusDetailTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvEmployeeBounusDetail_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvEmployeeBounusDetail.EditIndex = -1;
            BindEmployeeBounusDetailTable();
        }
        protected void gvEmployeeBounusDetail_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvEmployeeBounusDetail.EditIndex = e.NewEditIndex;
            BindEmployeeBounusDetailTable();
        }
        protected void lnkRemoveEmployeeBounusDetail_Command(object sender, CommandEventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender as LinkButton;
                string delSr = lnk.CommandArgument.ToString();
                for (int i = 0; i < dtEmployeeBounusDetail.Rows.Count; i++)
                {
                    if (dtEmployeeBounusDetail.Rows[i][0].ToString() == delSr)
                    {
                        dtEmployeeBounusDetail.Rows[i].Delete();
                        dtEmployeeBounusDetail.AcceptChanges();
                    }
                }
                for (int i = 0; i < dtEmployeeBounusDetail.Rows.Count; i++)
                {
                    dtEmployeeBounusDetail.Rows[i].SetField(0, i + 1);
                    dtEmployeeBounusDetail.AcceptChanges();
                }
                if (dtEmployeeBounusDetail.Rows.Count < 1)
                {
                    DataRow dr = dtEmployeeBounusDetail.NewRow();
                    dtEmployeeBounusDetail.Rows.Add(dr);
                    dtEmployeeBounusDetail.AcceptChanges();
                    showEmployeeBounusDetailFirstRow = false;
                }
                if (showEmployeeBounusDetailFirstRow)
                    EmployeeBounusDetailSrNo = dtEmployeeBounusDetail.Rows.Count + 1;
                else
                    EmployeeBounusDetailSrNo = 1;

                BindEmployeeBounusDetailTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }
        protected void btnAddEmployeeBounusDetail_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow dr = dtEmployeeBounusDetail.NewRow();
                dr[0] = EmployeeBounusDetailSrNo.ToString();
                dr[1] = ((DropDownList)gvEmployeeBounusDetail.FooterRow.FindControl("ddlEmployee")).SelectedItem.Value;
                dr[2] = ((DropDownList)gvEmployeeBounusDetail.FooterRow.FindControl("ddlEmployee")).SelectedItem.Text;
                dr[3] = ((DropDownList)gvEmployeeBounusDetail.FooterRow.FindControl("ddlType")).SelectedItem.Text;
                dr[4] = ((TextBox)gvEmployeeBounusDetail.FooterRow.FindControl("txtAmount")).Text;
                dtEmployeeBounusDetail.Rows.Add(dr);
                dtEmployeeBounusDetail.AcceptChanges();


                if (dtEmployeeBounusDetail.Rows[0][0].ToString() == "")
                {
                    dtEmployeeBounusDetail.Rows[0].Delete();
                    dtEmployeeBounusDetail.AcceptChanges();
                    showEmployeeBounusDetailFirstRow = true;
                }


                EmployeeBounusDetailSrNo += 1;
                BindEmployeeBounusDetailTable();
                ((Label)gvEmployeeBounusDetail.FooterRow.FindControl("txtSrNo")).Text = EmployeeBounusDetailSrNo.ToString();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

    }
}