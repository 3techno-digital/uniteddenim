﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MailingList.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.MailingList" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>

<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">
                <asp:UpdatePanel ID="UpdPnl" runat="server">
                    <ContentTemplate>
                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <h1 class="m-0 text-dark">Mailing List</h1>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                        <div style="text-align: right;">
                                            <button type="button" class="AD_btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note">Note </button>
                                            <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                            <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>




                        <section class="app-content">

                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <h4>Title</h4>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtTitle" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="text" class="form-control" placeholder="Title" name="companyName" id="txtTitle" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-sm-12 form-group">
                                            <h4>Mailing List</h4>
                                            <div class="form-group">
                                                <textarea name="txtCommentsRegardingSalary" id="txtCommentsRegardingSalary" class="form-control" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group" id="divAlertMsg" runat="server">
                                                    <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                        <span>
                                                            <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                        </span>
                                                        <p id="pAlertMsg" runat="server">
                                                        </p>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                            <ContentTemplate>

                                                <!-- Modal -->
                                                <div class="modal fade M_set" id="notes-modal" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h1 class="m-0 text-dark">Notes</h1>
                                                                <div class="add_new">
                                                                    <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                                                    <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                                                </div>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                                </p>
                                                                <p>
                                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                                </p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                            <div class="content-header second_heading">
                                <div class="container-fluid">
                                    <div class="row mb-2">
                                        <div class="col-sm-6">
                                            <h1 class="m-0 text-dark">Select Employees</h1>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.container-fluid -->
                            </div>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:GridView ID="gvEmployees" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvEmployees_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr. No">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="ddlEmployees" runat="server" data-plugin="select2" CssClass="form-control select2"></asp:DropDownList>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveEmployee_Command"></asp:LinkButton>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Button ID="btnAddEmployee" runat="server" CssClass="form-control AD_stock" Text="Add" OnClick="btnAddEmployee_Click"></asp:Button>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>


                        </section>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .table .delete-class:before {
                content: '' !important;
                color: black;
                margin-right: 5px;
            }

            .month-table-show {
                display: none;
                margin-top: 40px;
            }

            .modal-dialog {
                width: 450px;
                margin: 30px auto;
            }

            section.app-content {
                padding-bottom: 53px;
            }

            .modal-footer {
                border-top: 0px solid #e5e5e5;
            }

            .hiring-create-new-btn {
                margin-top: 17px;
                border: none;
                background: none;
                color: #01769a;
            }

            .user-img-div img {
                width: 169px !important;
                display: block;
                margin: 0 auto;
            }

            .user-img-div {
                width: auto;
            }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }
            .modal-content {
                top: 50px;
                width: 450px;
                position: fixed;
                z-index: 1050;
                left: 20%;
                border-radius: 5%;
                height: 100%;
                box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
            }
                .tf-back-btn i {
                    color: #fff !important;
                }

        </style>
        <script>
            $("#updFiles").change(function () {
                __doPostBack('LinkUploadFiles', '');
            });
        </script>
    </form>
</body>


</html>


