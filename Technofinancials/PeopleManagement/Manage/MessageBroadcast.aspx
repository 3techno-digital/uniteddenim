﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MessageBroadcast.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.MessageBroadCast" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
         
        <main id="app-main" class="app-main">
            	 <asp:UpdateProgress ID="updProgress"
		   AssociatedUpdatePanelID="btnUpdPnl"
		   runat="server">
			   <ProgressTemplate>
			   <div class="upd_panel" >
				   <div class="center">
					   <img src="/assets/images/Loading.gif" />
				   </div>
				   
			   
			   </div>
			   </ProgressTemplate>
		   </asp:UpdateProgress>
		   
	   
            <div class="wrap">
     
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/hr-02.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Broadcast Email</h3>
                        </div>


                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                        	   
	       <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                            <ContentTemplate>

										
                        <div class="col-sm-4" >
                            <div class="pull-right flex">
                                <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal"  value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                <button class="tf-save-btn" "Send" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                            </div>
                        </div>
                                                
		<!-- Modal -->
                    <div class="modal fade" id="notes-modal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Notes</h4>
                                </div>
                                <div class="modal-body">
                                                            <p>
                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                        </p>
                                    <p>
                                        <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
			
                                                	 </ContentTemplate>
                                            </asp:UpdatePanel>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">

                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Mailing List</h4>
                                        <asp:ListBox runat="server" ID ="lbMailingList" SelectionMode="Multiple" data-plugin="select2" CssClass="form-controls select2">
                                        </asp:ListBox>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Subject  <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtTitle" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <input type="text" class="form-control" placeholder="Title" name="companyName" id="txtTitle" runat="server" />
                                    </div>
                                </div>
                                
                                <div class="col-sm-12">
                                    
                                        <h4>Body  <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtContent" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                           <textboxio:Textboxio runat="server" ID="txtContent" ScriptSrc="/assets/textboxio/textboxio.js" Width="100%" Height="500px" />
  
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="clearfix">&nbsp;</div>
                 
                    <h3>Add Attachments&nbsp;
                        <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('updFiles').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                    </h3>
                    <asp:FileUpload ID="updFiles" runat="server" ClientIDMode="Static" AllowMultiple="true" Style="display: none;" accept=".pdf,.doc,.docx" />
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:GridView ID="gvFiles" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Attachment">
                                        <ItemTemplate>
                                            <a runat="server" ID="lblTitle" href='<%# Eval("FilePath") %>'><%# Eval("Title") %></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveFile_Command"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
   
                </section>
                
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
      
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .month-table-show {
                display: none;
                margin-top: 40px;
            }

            .modal-dialog {
                width: 450px;
                margin: 30px auto;
            }

            section.app-content {
                padding-bottom: 53px;
            }

            .modal-footer {
                border-top: 0px solid #e5e5e5;
            }

            .hiring-create-new-btn {
                margin-top: 17px;
                border: none;
                background: none;
                color: #01769a;
            }

            .user-img-div img {
                width: 169px !important;
                display: block;
                margin: 0 auto;
            }

            .user-img-div {
                width: auto;
            }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
        </style>
        <script>
           $("#updFiles").change(function () {
                 __doPostBack('LinkUploadFiles', '');
            });
        </script>
    </form>
</body>
</html>

