﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class MessageBroadCast : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int MailingListID
        {
            get
            {
                if (ViewState["MailingListID"] != null)
                {
                    return (int)ViewState["MailingListID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["MailingListID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                ViewState["MailingListID"] = null;
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/mailing-list";

                divAlertMsg.Visible = false;
                clearFields();

                ViewState["FilesSrNo"] = null;
                ViewState["dtFiles"] = null;
                dtFiles = new DataTable();
                dtFiles = createFiles();
                BindFilesTable();

                bindMLListBox();
                
                if (HttpContext.Current.Items["MailingListID"] != null)
                {
                    MailingListID = Convert.ToInt32(HttpContext.Current.Items["MailingListID"].ToString());
                }
            }

            Page.ClientScript.GetPostBackEventReference(this, string.Empty);
            string ctrlName = Request.Params.Get("__EVENTTARGET");
            string ctrlArgs = Request.Params.Get("__EVENTARGUMENT");
            if (!String.IsNullOrEmpty(ctrlName) && ctrlName == "LinkUploadFiles")
            {
                if (updFiles.HasFile || updFiles.HasFiles)
                {
                    foreach (HttpPostedFile file in updFiles.PostedFiles)
                    {
                        Random rand = new Random((int)DateTime.Now.Ticks);
                        int randnum = 0;

                        string fn = "";
                        string exten = "";
                        string destDir = Server.MapPath("~/assets/files/MailingLists/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");

                        randnum = rand.Next(1, 100000);
                        fn = Common.RemoveSpecialCharacter(txtTitle.Value).ToLower().Replace(" ", "-") + "_" + randnum;

                        if (!Directory.Exists(destDir))
                        {
                            Directory.CreateDirectory(destDir);
                        }

                        string fname = Path.GetFileName(file.FileName);
                        exten = Path.GetExtension(file.FileName);
                        file.SaveAs(destDir + fn + exten);

                        DataRow dr = dtFiles.NewRow();
                        dr[0] = FilesSrNo.ToString();
                        dr[1] = fn + exten;
                        dr[2] = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/MailingLists/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
                        dtFiles.Rows.Add(dr);
                        dtFiles.AcceptChanges();

                        FilesSrNo += 1;
                        BindFilesTable();
                    }
                }
            }
        }

        private void bindMLListBox()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            lbMailingList.DataSource = objDB.GetAllMailingListsByCompanyID(ref errorMsg);
            lbMailingList.DataTextField = "MailingListTitle";
            lbMailingList.DataValueField = "MailingListID";
            lbMailingList.DataBind();

            lbMailingList.SelectedIndex = lbMailingList.Items.IndexOf(lbMailingList.Items.FindByValue(HttpContext.Current.Items["MailingListID"].ToString()));

        }

        private void clearFields()
        {
            txtTitle.Value = "";
            txtContent.Content = "";
            lbMailingList.ClearSelection();
            dtFiles = null;
            dtFiles = createFiles();
            FilesSrNo = 1;
            BindFilesTable();
            lst.Clear();
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            string res = "";          
            res  = sendEmails();
            //res = "MailingList Data Updated";

            clearFields();
            BindFilesTable();
        
            if (res == "Message has been delivered successfully")
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }
        private static List<string> lst = new List<string>();

        private void createMailingList()
        {
            foreach (ListItem li in lbMailingList.Items)
            {

                if (li.Selected == true)
                {
                    DataTable dt = new DataTable();
                    objDB.MailingListID = Convert.ToInt32(li.Value) ;
                    dt = objDB.GetAllMailingListEmails(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                lst.Add(dt.Rows[i]["Email"].ToString());
                            }
                        }
                    }
                }
            }
        }

        private string sendEmails()
        {
            string returnMsg = "Message has been delivered successfully";
            try
            {
                createMailingList();
                if (lst != null)
                {
                    if (lst.Count > 0)
                    {
                        string html = "";
                        html = txtContent.Content;
                        objDB.MailingListID = MailingListID;
                        if (dtFiles != null)
                        {
                            if (dtFiles.Rows.Count > 0)
                            {
                                html += "<br />Please Find Attachments<br />";
                                string attachments = "";

                                for (int j = 0; j < dtFiles.Rows.Count; j++)
                                {
                                    attachments += "<a href=" + dtFiles.Rows[j]["FilePath"] + ">" + dtFiles.Rows[j]["Title"] + "</a><br />";
                                }
                                html += attachments;
                            }
                        }

                        html += "<br /><p style='color:red;'>Please DO NOT reply to this email.</p>";


                        MailMessage mail = new MailMessage();
                        mail.Subject = txtTitle.Value;
                        mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), Session["CompanyName"].ToString());
                        mail.To.Add(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString());
                        for (int i = 0; i < lst.Count; i++)
                        {
                            mail.Bcc.Add(lst[i].ToString());
                        }
                        mail.Body = html;
                        mail.IsBodyHtml = true;

                        SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["NoReplySMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NoReplyPort"].ToString()));
                        smtp.EnableSsl = true;
                        NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["NoReplyPassword"].ToString());
                        smtp.Credentials = netCre;
                        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                        smtp.Send(mail);
                    }
                    else
                    {
                        returnMsg = "No reciever selected";
                    }
                }
                else
                {
                    returnMsg = "No reciever selected";
                }
            }
            catch (Exception ex)
            {
                returnMsg = ex.Message;
            }
            return returnMsg;
            
        }

       

        private DataTable dtFiles
        {
            get
            {
                if (ViewState["dtFiles"] != null)
                {
                    return (DataTable)ViewState["dtFiles"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtFiles"] = value;
            }
        }
        protected int FilesSrNo
        {
            get
            {
                if (ViewState["FilesSrNo"] != null)
                {
                    return (int)ViewState["FilesSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["FilesSrNo"] = value;
            }
        }


        private DataTable createFiles()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("FilePath");
            dt.AcceptChanges();

            return dt;
        }
        protected void BindFilesTable()
        {

            if (ViewState["dtFiles"] == null)
            {
                dtFiles = createFiles();
                ViewState["dtFiles"] = dtFiles;
            }

            gvFiles.DataSource = dtFiles;
            gvFiles.DataBind();

            if (gvFiles.Rows.Count > 0)
            {
                gvFiles.UseAccessibleHeader = true;
                gvFiles.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void lnkRemoveFile_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtFiles.Rows.Count; i++)
            {
                if (dtFiles.Rows[i][0].ToString() == delSr)
                {
                    dtFiles.Rows[i].Delete();
                    dtFiles.AcceptChanges();
                }
            }
            for (int i = 0; i < dtFiles.Rows.Count; i++)
            {
                dtFiles.Rows[i].SetField(0, i + 1);
                dtFiles.AcceptChanges();
            }

            FilesSrNo = dtFiles.Rows.Count + 1;
            BindFilesTable();
        }

    }
}