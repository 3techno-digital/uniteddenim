﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeesOld.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.Employees" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <img src="/assets/images/hr-02.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="text-uppercase tf-page-heading-text">Employes List</h3>
                        </div>
                        <div class="col-sm-4">
                            <div class="pull-right">
                                <button class="tf-approved-btn" "Approved"><i class="far fa-thumbs-up"></i></button>
                                <button class="tf-back-btn" "Back"><i class="fas fa-arrow-left"></i></button>
                                <button class="tf-disapproved-btn" "Dispproved"><i class="far fa-thumbs-down"></i></button>
                                <button class="tf-save-btn" "Save"><i class="far fa-save"></i></button>
                                <button class="tf-send-btn" "Send"><i class="fas fa-paper-plane"></i></button>
                                <button class="tf-upload-btn" "Upload"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                <button class="tf-view-btn" "View"><i class="far fa-eye"></i></button>
                                <button class="tf-add-btn" "Add"><i class="far fa-plus-circle"></i></button>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row ">
                        <div id="exTab2" class="">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#1" data-toggle="tab">Basic Information</a>
                                </li>
                                <li>
                                    <a href="#2" data-toggle="tab">Qualification and Experiences</a>
                                </li>
                                <li>
                                    <a href="#3" data-toggle="tab">Employment details</a>
                                </li>
                                <li>
                                    <a href="#4" data-toggle="tab">Pay roll</a>
                                </li>
                                <li>
                                    <a href="#5" data-toggle="tab">Bank Details</a>
                                </li>
                               <%-- <li>
                                    <a href="#6" data-toggle="tab">Assets</a>
                                </li>--%>
                                <li>
                                    <a href="#7" data-toggle="tab">Attendence</a>
                                </li>
                                
                               <%-- <li>
                                    <a href="#8" data-toggle="tab">Sepration</a>
                                </li>--%>
                            </ul>
                            <div class="tab-content ">
                                <div class="tab-pane active row" id="1">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="user-img-div">
                                                    <img src="/assets/images/3techno-Logo.png" class="img-responsive" />
                                                    <asp:FileUpload ID="FileUpload2" runat="server" ClientIDMode="Static" Style="display: none;" />
                                                    <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('FileUpload2').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>Employe No</h4>
                                                <input class="form-control" placeholder="Emp No" type="text" name="employerNo" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>First Name</h4>
                                                <input class="form-control" placeholder="First Name" type="text" name="firstName" />
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>Middle Name</h4>
                                                <input class="form-control" placeholder="Middle Name" type="text" name="middleName" />
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>Last Name</h4>
                                                <input class="form-control" placeholder="Last Name" type="text" name="lastName" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4>Gender</h4>
                                                <select class="form-control" id="gender" name="gender">
                                                    <option value="">Please Select</option>
                                                    <option>Male</option>
                                                    <option>Female</option>
                                                    <option>Others</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>Contact No</h4>
                                                <input class="form-control" placeholder="Contact no" type="text" name="contactNo" />
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>Email address</h4>
                                                <input class="form-control" placeholder="Email address" type="email" name="emailAddress" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4>Country</h4>
                                                <select id="ddl1" class="form-control" data-plugin="select2" name="country">
                                                    <option value="">Please Select</option>
                                                    <option value="option2">Paskitan</option>
                                                    <option value="option3">England</option>
                                                    <option value="option4">China</option>
                                                    <option value="option5">India</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4>City</h4>
                                                <select id="ddl2" class="form-control" data-plugin="select2" name="city">
                                                    <option value="">Please Select</option>
                                                    <option value="option2">Karachi</option>
                                                    <option value="option3">Islamabad</option>
                                                    <option value="option4">Lahore</option>
                                                    <option value="option5">Larkana</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4>Province</h4>
                                                <select id="ddl3" class="form-control" data-plugin="select2" name="province">
                                                    <option value="">Please Select</option>
                                                    <option value="option2">CSS</option>
                                                    <option value="option3">Javascript</option>
                                                    <option value="option4">PHP</option>
                                                    <option value="option5">Bootstrap</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>Address Line 1</h4>
                                                <input class="form-control" placeholder="Address Line 1" type="text" name="addressLine1" />
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>Address Line 2</h4>
                                                <input class="form-control" placeholder="Address Line 2" type="text" name="addressLine2" />
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>Zipcode</h4>
                                                <input class="form-control" placeholder="Zipcode" type="text" name="zipCode" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h3>Emergency Contact</h3>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>Person Name</h4>
                                                <input class="form-control" placeholder="Person Name" type="text" name="personName" />
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <h4>Phone number</h4>
                                                <input class="form-control" placeholder="Phone number" type="text" name="phoneNumber" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="tf-send-btn" "Send" id="button"><i class="fas fa-paper-plane"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane employe-tables" id="2">
                                    <div>
                                        <div>
                                            <h4>Experience
                                                <button class="tf-add-btn pull-right" data-toggle="modal" data-target="#experience-modal" type="button" data-original-"Add" "Add"><i class="far fa-plus-circle"></i></button>
                                            </h4>
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div>
                                            <h4>Qualification
                                                <button class=" tf-add-btn pull-right" data-toggle="modal" data-target="#qualification-modal" type="button" data-original-"Add" "Add"><i class="far fa-plus-circle"></i></button>
                                            </h4>
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div>
                                            <h4>Certification
                                                <button class="tf-add-btn pull-right" data-toggle="modal" data-target="#certification-modal" type="button" data-original-"Add" "Add"><i class="far fa-plus-circle"></i></button>
                                            </h4>
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>


                                        <div>

                                            <h4>Training
                                                <button class="tf-add-btn pull-right" data-toggle="modal" data-target="#training-modal" type="button" data-original-"Add" "Add"><i class="far fa-plus-circle"></i></button>
                                            </h4>
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div>
                                            <h4>Skill Set
                                                <button class="tf-add-btn pull-right" data-toggle="modal" data-target="#skill-modal" type="button" data-original-"Add" "Add"><i class="far fa-plus-circle"></i></button>
                                            </h4>
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="3">
                                    <div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-1" class="control-label">Employment type</h4>
                                                <select id="ddl5" class="form-control" data-plugin="select2" name="employmentType">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">HTML</option>
                                                    <option value="option2">CSS</option>
                                                    <option value="option3">Javascript</option>
                                                    <option value="option4">PHP</option>
                                                    <option value="option5">Bootstrap</option>
                                                </select>
                                            </div>
                                            <!-- END column -->
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-2" class="control-label">Designation</h4>
                                                <select id="ddl6" class="form-control" data-plugin="select2" name="designation">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">HTML</option>
                                                    <option value="option2">CSS</option>
                                                    <option value="option3">Javascript</option>
                                                    <option value="option4">PHP</option>
                                                    <option value="option5">Bootstrap</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-3" class="control-label">Department</h4>
                                                <select id="ddl7" class="form-control" data-plugin="select2" name="department">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">HTML</option>
                                                    <option value="option2">CSS</option>
                                                    <option value="option3">Javascript</option>
                                                    <option value="option4">PHP</option>
                                                    <option value="option5">Bootstrap</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-4" class="control-label">Sub Department</h4>
                                                <select id="ddl8" class="form-control" data-plugin="select2" name="subDepartment">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">HTML</option>
                                                    <option value="option2">CSS</option>
                                                    <option value="option3">Javascript</option>
                                                    <option value="option4">PHP</option>
                                                    <option value="option5">Bootstrap</option>
                                                </select>
                                            </div>
                                            <!-- END column -->
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-5" class="control-label">Grade</h4>
                                                <select id="ddl9" class="form-control" data-plugin="select2" name="grade">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">HTML</option>
                                                    <option value="option2">CSS</option>
                                                    <option value="option3">Javascript</option>
                                                    <option value="option4">PHP</option>
                                                    <option value="option5">Bootstrap</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-6" class="control-label">Direct reporting to</h4>
                                                <select id="ddl10" class="form-control" data-plugin="select2" name="directReportingTo">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">HTML</option>
                                                    <option value="option2">CSS</option>
                                                    <option value="option3">Javascript</option>
                                                    <option value="option4">PHP</option>
                                                    <option value="option5">Bootstrap</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-6" class="control-label">Indirect reporting to</h4>
                                                <select id="ddl11" class="form-control" data-plugin="select2" name="indirectReportingTo">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">HTML</option>
                                                    <option value="option2">CSS</option>
                                                    <option value="option3">Javascript</option>
                                                    <option value="option4">PHP</option>
                                                    <option value="option5">Bootstrap</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <h4>Date of Joining</h4>
                                                    <input type="text" data-plugin="datetimepicker" class="form-control" name="dateOfJoining" />

                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4>Active </h4>
                                                <div class="form-group">
                                                    <div class="checkbox checkbox-primary">
                                                        <input type="checkbox" data-switchery id="checkbox-demo-1" name="active">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="tf-send-btn" "" id="button1" data-original-"Send"><i class="fas fa-paper-plane"></i></button>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div id="exTab3" class="">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#10" data-toggle="tab">Responsibilites</a>
                                                    </li>
                                                    <li><a href="#11" data-toggle="tab">Accountabilities</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content ">
                                                    <div class="tab-pane active" id="10">
                                                        <div>
                                                            <table class="table table-hover">
                                                                <tbody>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>First Name</th>
                                                                        <th>Last Name</th>
                                                                        <th>Username</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td>Mark</td>
                                                                        <td>Otto</td>
                                                                        <td>@mdo</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>2</td>
                                                                        <td>Jacob</td>
                                                                        <td>Thornton</td>
                                                                        <td>@fat</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>3</td>
                                                                        <td>Larry</td>
                                                                        <td>the Bird</td>
                                                                        <td>@twitter</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="11">
                                                        <div>
                                                            <table class="table table-hover">
                                                                <tbody>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>First Name</th>
                                                                        <th>Last Name</th>
                                                                        <th>Username</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td>Mark</td>
                                                                        <td>Otto</td>
                                                                        <td>@mdo</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>2</td>
                                                                        <td>Jacob</td>
                                                                        <td>Thornton</td>
                                                                        <td>@fat</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>3</td>
                                                                        <td>Larry</td>
                                                                        <td>the Bird</td>
                                                                        <td>@twitter</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="4">
                                    <div>
                                        <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <th>#</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Username</th>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="5">
                                    <div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <h4>Bank Name</h4>
                                                    <input type="text" class="form-control" name="bankName" />

                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <h4>Branch Name</h4>
                                                    <input type="text" class="form-control" name="branchName" />

                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <h4>Account No</h4>
                                                    <input type="text" class="form-control" name="accountNo" />

                                                </div>
                                            </div>


                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <h4>Account Title</h4>
                                                    <input type="text" class="form-control" name="accountTitle" />

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="tf-send-btn" "" id="button2" data-original-"Send" aria-describedby="tooltip844001"><i class="fas fa-paper-plane"></i></button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="6">
                                    <div>
                                        <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Asset names</th>
                                                    <th>Status</th>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <div class="col-sm-12">
                                            <div class="pull-right" style="margin-top: 10px;">
                                                <button class="tf-exit-btn" "" data-original-"Exit"></button>
                                                <button class="tf-note-btn" "" data-original-"Note"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="7">
                                    <div>
                                        <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Month name</th>
                                                    <th>Working days</th>
                                                    <th>Present days</th>
                                                    <th>Absent days</th>
                                                    <th>Leaves</th>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td class="month-name-table">Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td class="month-name-table">Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td class="month-name-table">Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-hover month-table-show">
                                            <tbody>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Time in</th>
                                                    <th>Time out</th>
                                                    <th>Total hours</th>
                                                    <th>Late reasons</th>
                                                    <th>Resultant</th>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td class="month-name-table">Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td class="month-name-table">Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td class="month-name-table">Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="8">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-1" class="control-label">Employment ID</h4>
                                                <select id="ddl12" class="form-control" data-plugin="select2" name="employmentId">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">HTML</option>
                                                    <option value="option2">CSS</option>
                                                    <option value="option3">Javascript</option>
                                                    <option value="option4">PHP</option>
                                                    <option value="option5">Bootstrap</option>
                                                </select>
                                            </div>
                                            <!-- END column -->
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-2" class="control-label">Type Of Seperation</h4>
                                                <select id="ddl13" class="form-control" data-plugin="select2" name="typeOfSepration">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">Terminations</option>
                                                    <option value="option2">Lay off</option>
                                                    <option value="option3">Resignation</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-3" class="control-label">Reason For Leaving </h4>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="comment2" name="reasonForLeaving"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-3" class="control-label">Comments Regarding Salary </h4>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="comment3" name="commentsRegardingSalary"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-3" class="control-label">Things Like About Compnay</h4>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="comment4" name="thingsLikeAboutCompany"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4 for="select2-demo-4" class="control-label">Notice Period EndDate</h4>
                                                <div class="form-group">
                                                    <input type="text" id="datetimepicker4" class="form-control" data-plugin="datetimepicker" name="noticePeriodEndDate" />
                                                </div>
                                            </div>
                                            <!-- END column -->
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <h4>Email Or Call Forwarding Period</h4>
                                                    <input type="text" id="datetimepicker5" class="form-control" data-plugin="datetimepicker" name="emailOrCallForwadingPeriod" />
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="tf-send-btn" "" id="sepration" data-original-"Send"><i class="fas fa-paper-plane"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h3>IT Seperation</h3>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4>Email ID Status</h4>
                                                <select id="ddl17" class="form-control" data-plugin="select2" name="emailIdStatus">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">Transfer</option>
                                                    <option value="option2">Revoked</option>
                                                    <option value="option3">Deleted</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4>Domain Access</h4>
                                                <select id="select2-demo-3" class="form-control" data-plugin="select2" name="domainAccess">
                                                    <option value="">Please Select</option>
                                                    <option value="option1">Temp</option>
                                                    <option value="option2">Revoked</option>
                                                    <option value="option3">Deleted</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <h4>&nbsp;</h4>
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="checkbox-demo-4" name="isAccessStatusRevoked" />
                                                            <label for="checkbox-demo-4">is Access Status Revoked</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h4>&nbsp;</h4>

                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox" id="custome-checkbox2" name="companyInfoStoredPersonally" />
                                                            <label for="custome-checkbox2">Company Info Stored Personally</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 for="select2-demo-3" class="control-label">IT Comments</h4>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="comment4" name="itComments"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="tf-send-btn" "" id="it-sepration" data-original-"Send"><i class="fas fa-paper-plane"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h3>Admin Seperation</h3>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" id="custome-checkbox7" name="returnedLockerKeys" />
                                                    <label for="custome-checkbox7">Returned Locker Keys</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" id="custome-checkbox9" name="returnedCar" />
                                                    <label for="custome-checkbox9">Returned Car</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" id="custome-checkbox11" name="returnedStationary" />
                                                    <label for="custome-checkbox11">Returned Stationary</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" id="custome-checkbox8" name="returnedMobilePhone" />
                                                    <label for="custome-checkbox8">Returned Mobile Phone</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" id="custome-checkbox10" name="returnedInternetDevice" />
                                                    <label for="custome-checkbox10">Returned Internet Device</label>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 for="select2-demo-3" class="control-label">Notes</h4>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="comment6" name="notes"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="tf-send-btn" "" id="admin-sepration" data-original-"Send"><i class="fas fa-paper-plane"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h3>HR Seperation</h3>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4>Notice Period Dedudctions</h4>
                                                <input type="number" class="form-control" placeholder="Notice Period Dedudctions" name="noticePeriodDeductions" />
                                            </div>

                                            <div class="col-sm-4">
                                                <h4>Attendance Details</h4>
                                                <input type="text" class="form-control" placeholder="Attendance Details" name="attendanceDetails" />
                                            </div>
                                            <div class="col-sm-4">
                                                <h4>UnAwailed Leave Balance</h4>
                                                <input type="number" class="form-control" placeholder="UnAwailed Leave Balance" name="unawailedLeaveBalance" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4>Medical Refurbishments</h4>
                                                <input type="number" class="form-control" placeholder="Medical Refurbishments" name="medicalRefurbishments" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 for="select2-demo-3" class="control-label">Notes</h4>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="1" cols="10" id="comment6" name="notesmedical"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="tf-send-btn" "" id="hr-sepration" data-original-"Send"><i class="fas fa-paper-plane"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h3>Finance Seperation</h3>
                                            </div>
                                            <div class="col-sm-4">
                                                <h4>Loans and advances</h4>
                                                <input type="number" class="form-control" placeholder="Loans and advances" name="loanAndAdvances" />
                                            </div>

                                            <div class="col-sm-4">
                                                <h4>Company Loan</h4>
                                                <input type="number" class="form-control" placeholder="Company Loan" name="companyLoan" />
                                            </div>
                                            <div class="col-sm-4">
                                                <h4>Any other advance</h4>
                                                <input type="number" class="form-control" placeholder="UnAwailed Leave Balance" name="anyOtherAdvance" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4>Total Dues</h4>
                                                <input type="number" class="form-control" placeholder="Total Dues" name="totalDues" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 for="select2-demo-3" class="control-label">Notes</h4>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="comment8" name="financenotes"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button class="tf-send-btn" "" id="finance-sepration" data-original-"Send"><i class="fas fa-paper-plane"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <!-- Modal -->
        <div id="experience-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Experience</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Job Title</h4>
                                <input class="form-control" placeholder="Job Title" type="text" />
                            </div>
                            <div class="col-sm-12">
                                <h4>Organization</h4>
                                <input class="form-control" placeholder="Organization" type="text" />
                            </div>
                            <div class="col-sm-12">
                                <h4>Country</h4>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Pakistan</option>
                                        <option>Australia</option>
                                        <option>Canada</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h4>State</h4>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Sindh</option>
                                        <option>Punjab</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h4>City</h4>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Karachi</option>
                                        <option>Lahore</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>Start Date</h4>
                                <label>Year</label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>2018</option>
                                        <option>2017</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>&nbsp;</h4>
                                <label>Month</label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>January</option>
                                        <option>February</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>&nbsp;</h4>
                                <label>End Date</label>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>2018</option>
                                        <option>2017</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>&nbsp;</h4>
                                <label>Month</label>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>January</option>
                                        <option>February</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="checkbox checkbox-primary" style="margin-top: 10px;">
                                    <input type="checkbox" id="custome-checkbox2" checked />
                                    <label for="custome-checkbox2">Currently working here</label>
                                </div>
                            </div>
                            <div class="col-sm-12">

                                <div class="form-group">
                                    <label for="comment">Details</label>
                                    <textarea class="form-control" rows="5" id="comment"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-primary">Save</button>
                        <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="qualification-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Qualification</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Degree Level</h4>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>High School</option>
                                        <option>Intermediate</option>
                                        <option>Master's</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>Completion Date</h4>
                                <label>Year</label>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>2018</option>
                                        <option>2017</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>&nbsp;</h4>
                                <label>Month</label>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>January</option>
                                        <option>February</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h4>Grade</h4>
                                <input class="form-control" placeholder="Grade" type="text" />
                            </div>

                            <div class="col-sm-12">
                                <h4>Institution</h4>
                                <input class="form-control" placeholder="Institution" type="text" />
                            </div>
                            <div class="col-sm-12">
                                <h4>Country</h4>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Pakistan</option>
                                        <option>Australia</option>
                                        <option>Canada</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h4>State</h4>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>Sindh</option>
                                        <option>Punjab</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h4>City</h4>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>Karachi</option>
                                        <option>Lahore</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">

                                <div class="form-group">
                                    <label for="comment">Details</label>
                                    <textarea class="form-control" rows="5" id="comment"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-primary">Save</button>
                        <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="certification-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Certification</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Title</h4>
                                <input class="form-control" placeholder="Title" type="text" />
                            </div>
                            <div class="col-sm-6">
                                <h4>Completion Date</h4>
                                <label>Year</label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>2018</option>
                                        <option>2017</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>&nbsp;</h4>
                                <label>Month</label>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>January</option>
                                        <option>February</option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <h4>Score</h4>
                                <input class="form-control" placeholder="Score" type="text" />
                            </div>

                            <div class="col-sm-12">
                                <h4>Institution</h4>
                                <input class="form-control" placeholder="Institution" type="text" />
                            </div>

                            <div class="col-sm-12">
                                <h4>Country</h4>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Pakistan</option>
                                        <option>Australia</option>
                                        <option>Canada</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <h4>State</h4>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>Sindh</option>
                                        <option>Punjab</option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <h4>City</h4>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>Karachi</option>
                                        <option>Lahore</option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12">

                                <div class="form-group">
                                    <label for="comment">Details</label>
                                    <textarea class="form-control" rows="5" id="comment"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-primary">Save</button>
                        <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="training-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Training</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Title</h4>
                                <input class="form-control" placeholder="Title" type="text" />
                            </div>
                            <div class="col-sm-6">
                                <h4>Completion Date</h4>
                                <label>Year</label>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>2018</option>
                                        <option>2017</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>&nbsp;</h4>
                                <label>Month</label>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>January</option>
                                        <option>February</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h4>Score</h4>
                                <input class="form-control" placeholder="Score" type="text" />
                            </div>

                            <div class="col-sm-12">
                                <h4>Institution</h4>
                                <input class="form-control" placeholder="Institution" type="text" />
                            </div>

                            <div class="col-sm-12">
                                <h4>Country</h4>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Pakistan</option>
                                        <option>Australia</option>
                                        <option>Canada</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h4>State</h4>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>Sindh</option>
                                        <option>Punjab</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h4>City</h4>
                                <div class="input-group">

                                    <select class="form-control">
                                        <option>Karachi</option>
                                        <option>Lahore</option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12">

                                <div class="form-group">
                                    <label for="comment">Details</label>
                                    <textarea class="form-control" rows="5" id="comment"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-primary">Save</button>
                        <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="skill-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Skills</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Skill</h4>
                                <input class="form-control" placeholder="Skill" type="text" name="skill" />
                            </div>
                            <div class="col-sm-12">
                                <h4>Skill Level</h4>

                                <div class="input-group">

                                    <select class="form-control">
                                        <option>Beginner</option>
                                        <option>Intermediate</option>
                                        <option>Expert</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-primary" id="#button1">Save</button>
                        <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $(".month-name-table").click(function () {
                    $(".month-table-show").toggle();
                });
            });
        </script>
    </form>
    <style>
        .month-table-show {
            display: none;
            margin-top: 40px;
        }

        .modal-dialog {
            width: 450px;
            margin: 30px auto;
        }

        /*.modal-body div div {
            margin-bottom: 20px;
        }*/

        section.app-content {
            padding-bottom: 53px;
        }

        .modal-footer {
            border-top: 0px solid #e5e5e5;
        }

        .hiring-create-new-btn {
            margin-top: 17px;
            border: none;
            background: none;
            color: #01769a;
        }

        div#experience-modal .input-group {
            width: 100% !important;
        }

        div#qualification-modal .input-group {
            width: 100% !important;
        }

        div#certification-modal .input-group {
            width: 100% !important;
        }

        div#training-modal .input-group {
            width: 100% !important;
        }

        div#skill-modal .input-group {
            width: 100% !important;
        }
    </style>
    <%--  <script>
        $("#button").click(function () {
            $("form1").valid();
        });
    </script>
    <script>
        $("#button1").click(function () {
            $("form1").valid();
        });
    </script>
        <script>
        $("#button2").click(function () {
            $("form1").valid();
        });
    </script>
            <script>
        $("#sepration").click(function () {
            $("form1").valid();
        });
    </script>

                <script>
        $("#it-sepration").click(function () {
            $("form1").valid();
        });
    </script>

                <script>
        $("#admin-sepration").click(function () {
            $("form1").valid();
        });
    </script>
                <script>
        $("#hr-sepration").click(function () {
            $("form1").valid();
        });
    </script>
                <script>
        $("#sepration").click(function () {
            $("finance-sepration").valid();
        });
    </script>
    

    <script>
        $("#form1").validate({
            rules: {
                employerNo: "required",
                firstName: "required",
                lastName: "required",
                contactNo: "required",
                emailAddress: "required",
                addressLine1: "required",
                zipCode: "required",
                personName: "required",
                phoneNumber: "required",
                country: "required",
                city: "required",
                province: "required",
                employmentType: "required",
                designation: "required",
                department: "required",
                subDepartment: "required",
                grade: "required",
                directReportingTo: "required",
                indirectReportingTo: "required",
                dateOfJoining: "required",
                active: "required",
                skill: "required",
                bankName: "required",
                branchName: "required",
                accountNo: "required",
                employmentId: "required",
                typeOfSepration: "required",
                reasonForLeaving: "required",
                commentsRegardingSalary: "required",
                thingsLikeAboutCompany: "required",
                noticePeriodEndDate: "required",
                accountTitle: "required",
                emailOrCallForwadingPeriod: "required",
                emailIdStatus: "required",
                domainAccess: "required",
                itComments: "required",
                notes: "required",
                noticePeriodDeductions: "required",
                attendanceDetails: "required",
                unawailedLeaveBalance: "required",
                medicalRefurbishments: "required",
                notesmedical: "required",
                loanAndAdvances: "required",
                companyLoan: "required",
                anyOtherAdvance: "required",
                totalDues: "required",
                financenotes: "required",

            },
            messages: {
                dd1: {
                    required: "required",
                },
                dd2: {
                    required: "required'",
                },
                dd3: {
                    required: "required",
                },
                gender: {
                    required: "required",
                },
            }
        });
    </script>

    <script>
        jQuery.extend(jQuery.validator.messages, {
            required: "* Required",
            select: "* Required",
        });
    </script>--%>
</body>
</html>
