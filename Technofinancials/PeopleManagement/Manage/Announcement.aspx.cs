﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class Announcement : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int AnnouncementID
        {
            get
            {
                if (ViewState["AnnouncementID"] != null)
                {
                    return (int)ViewState["AnnouncementID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AnnouncementID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["AnnouncementID"] = null;
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/Announcement";

                    divAlertMsg.Visible = false;
                    LoadSlackChannel();
                    LoadDepartments();
                    if (HttpContext.Current.Items["AnnouncementID"] != null)
                    {
                        AnnouncementID = Convert.ToInt32(HttpContext.Current.Items["AnnouncementID"].ToString());
                        getAnnouncementByID(AnnouncementID);
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void LoadSlackChannel()
		{
            DataTable dt = new DataTable();
          
            dt = objDB.GetSlackChannels(ref errorMsg);
            slack.DataSource = dt;
            slack.DataTextField = "Channel";
            slack.DataValueField = "Channel";
            slack.DataBind();

        }
        private void LoadDepartments()
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetDepartments(ref errorMsg);
            dpt.DataSource = dt;
            dpt.DataTextField = "DeptName";
            dpt.DataValueField = "DeptName";
            
            dpt.DataBind();
            dpt.Items.Insert(0, new ListItem("ALL", "ALL"));

        }
        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "Announcement";
                objDB.PrimaryColumnnName = "AnnouncementID";
                objDB.PrimaryColumnValue = AnnouncementID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getAnnouncementByID(int AnnouncementID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.AnnouncementID = AnnouncementID;
                dt = objDB.GetAnnouncementByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtAnnouncementTitle.Value = dt.Rows[0]["AnnouncementTitle"].ToString();
                        txtDate.Value = DateTime.Parse(dt.Rows[0]["Date"].ToString()).ToString("dd-MMM-yyyy");
                        txtDescription.Value = dt.Rows[0]["Description"].ToString();

                        objDB.DocID = AnnouncementID;
                        objDB.DocType = "Announcement";
                        ltrNotesTable.Text = objDB.GetDocNotes();
                    }
                }
                Common.addlog("View", "HR", "Announcement \"" + txtAnnouncementTitle.Value + "\" Viewed", "Announcement", objDB.AnnouncementID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.AnnouncementTitle = txtAnnouncementTitle.Value;
                objDB.Date = txtDate.Value;
                objDB.Description = txtDescription.Value;
                int Docid = 0;
                int sendToALL = 0;
                List<string> lstofDptChannels = new List<string>();
                foreach (ListItem item in dpt.Items)
                {
                    if (item.Selected)
                    {
                        if (item.Value == "ALL")
                        {
                            lstofDptChannels.Add(item.Value);
                            sendToALL = 1;

                        }
						else
						{
                            lstofDptChannels.Add(item.Value);
                        }
                           
                    }
                }
                foreach (ListItem item in slack.Items)
                {
                    if (item.Selected)
                    {
                        lstofDptChannels.Add(item.Value);
                    }
                }

              
                if (HttpContext.Current.Items["AnnouncementID"] != null)
                {
                    txtDate.Disabled = true;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.AnnouncementID = AnnouncementID;
                    Docid = Convert.ToInt32(objDB.UpdateAnnouncement());
                  
                    res = "Announcement Data Updated";
                
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    
                    Docid = Convert.ToInt32(objDB.AddAnnouncement());
                    clearFields();
                    res = "New Announcement Added";
                }
                if (lstofDptChannels.Count > 0)
                {
                    foreach (var item in lstofDptChannels)
                    {
                        objDB.Channel = item;
                        objDB.AnnouncementID = Docid;
                        objDB.AnnouncementDptChannel();
                    }


                }
                objDB.DocType = "Announcement";
                objDB.DocID = Docid;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

                if (res == "New Announcement Added" || res == "Announcement Data Updated")
                {
                    if (res == "New Announcement Added") { Common.addlog("Add", "HR", "New Announcement \"" + objDB.AnnouncementTitle + "\" Added", "Announcement"); }
                    if (res == "Announcement Data Updated") { Common.addlog("Update", "HR", "Announcement \"" + objDB.AnnouncementTitle + "\" Updated", "Announcement", objDB.AnnouncementID); }
                    if (lstofDptChannels.Count > 0)
                    {
                        if (sendToALL == 1)
                        {
                            objDB.Channel = "#general";
                            objDB.SlackBy = Session["UserName"].ToString();
                            objDB.SlackOn = objDB.Date;
                            objDB.SlackIT();
                          
                        }
                        foreach (ListItem item in slack.Items)
                        {
                            if (item.Selected)
                            {
                                if(sendToALL == 1)
								{
                                   
                                    break;
                                }
								objDB.Channel = item.Value;
                              
                                objDB.SlackBy = Session["UserName"].ToString();
                                objDB.SlackOn = objDB.Date;

                                objDB.SlackIT();

                            }
                        }


                    }
                    
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtAnnouncementTitle.Value = "";
            txtDate.Value = "";
            txtDescription.Value = "";
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "Announcement", "AnnouncementID", HttpContext.Current.Items["AnnouncementID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "Announcement of ID\"" + HttpContext.Current.Items["AnnouncementID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/Announcement", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/Announcements/edit-Announcement-" + HttpContext.Current.Items["AnnouncementID"].ToString(), "Announcement \"" + txtAnnouncementTitle.Value + "\"", "Announcement", "Announcement", Convert.ToInt32(HttpContext.Current.Items["AnnouncementID"].ToString()));

                //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                //{
                //    objDB.AnnouncementID = AnnouncementID;
                //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //    objDB.AddAnnouncementToEmployeeAttendance();

                //}

                //Common.addlog("Delete", "HR", "Announcement of ID \"" + objDB.AnnouncementID + "\" deleted", "Announcement", objDB.AnnouncementID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Announcement", "AnnouncementID", HttpContext.Current.Items["AnnouncementID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "Announcement of ID\"" + HttpContext.Current.Items["AnnouncementID"].ToString() + "\" Status Changed", "Announcement", AnnouncementID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.AnnouncementID = AnnouncementID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteAnnouncement();
                Common.addlog("Announcement Rejected", "HR", "Announcement of ID\"" + HttpContext.Current.Items["AnnouncementID"].ToString() + "\" Announcement Rejected", "Announcement", AnnouncementID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Announcement Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


    }
}