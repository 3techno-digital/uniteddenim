﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IT3Approval.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.IT3Approval" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>


<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <div class="modal fade M_set in" id="myModal2" role="dialog" style="display: none;">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                        <h1 class="m-0 text-dark">IT-3 Approvals</h1>
                                        <input name="AttendanceID" type="hidden" id="txtAttendanceID" value="0" />
                                        <input name="It3ID2" id="It3ID2" type="hidden" value="0" />

                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                        <div style="text-align: right;">
                                            <span class="AD_btnn two">Close</span>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>
                        <div class="start-date">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Name</h4>
                                        <br />

                                        <div class="input-group date">
                                            <input class="form-control" name="EmployeeName" style="width: 100%;" type="text" id="EmployeeName" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Payroll Cycle</h4>
                                        <br />
                                        <div class="input-group date">
                                            <input class="form-control" name="PayrollCycle" type="text" id="PayrollCycless" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Total Claim</h4>
                                        <br />
                                        <div class="input-group date">
                                            <input class="form-control" name="TotalCalim" type="text" id="TotalCalim" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Remarks</h4>
                                        <br />
                                        <div class="input-group date">
                                            <input type="text" class="form-control" id="RamarksAdjust" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix">&nbsp;</div>

                        <div class="SaveBTn">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center><a class="AD_btn_inn" onclick="AddAdjustment()" id="modalbtnSave">Approve</a></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">&nbsp;</div>

                    </div>
                </div>

            </div>
    <asp:UpdateProgress ID="updProgress" style="display: none;"
                AssociatedUpdatePanelID="upd1"
                runat="server">
            </asp:UpdateProgress>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            
            
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">IT-3 Approvals</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>
                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                            <span>
                                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                            </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            </div>
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12">
                                        <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv dataTable no-footer" ClientIDMode="Static" AutoGenerateColumns="false">
                                            <Columns>



                                                <asp:TemplateField HeaderText="Sr.No.">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="IT-3 ID">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="IT3ID" Text='<%# Eval("IT3_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="IT3ID" Text='<%# Eval("EmployeeFirstName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Submition Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="SubmissionDate" Text='<%# Eval("Created_Date2") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Total Claim">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="TotalClaim" Text='<%# Eval("Total_Claim") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Payroll Cycle">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="PayrollCycle" Text='<%# Eval("submissionDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="Status" Text='<%# Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="File">
                                                    <ItemTemplate>
                                                        <a href='<%# Eval("File_Path") %>'>Downlaod</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <button id="btnAdjustment<%# Eval("IT3_ID") %>" class="AD_stock btnAdjustment<%# Eval("IT3_ID") %>" onclick="PopUpModal('<%#  Eval("IT3_ID") %>','<%#  Eval("EmployeeFirstName") %>','<%#  Eval("submissionDate") %>','<%# Eval("Total_Claim") %>')">Approve</button>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                        </asp:GridView>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <br />
                                        <br />
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </section>
                    </ContentTemplate>
                    <%--<Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>--%>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
<script>
    function PopUpModal(IT3ID,Name,PayrollCycle,TotalClaim) {

        var modal2 = document.getElementById("myModal2");
        modal2.style.display = "block";
        $("#It3ID2").val(IT3ID);
        $("#EmployeeName").val(Name);
        $("#PayrollCycless").val(PayrollCycle);
        $("#TotalCalim").val(TotalClaim);
               
    }

    function AddAdjustment()
    {
        var IT3_ID = $("#It3ID2").val();      
        var remarks = $("#RamarksAdjust").val();


        $.ajax({
            type: "POST",
            url: '/business/services/EssService.asmx/ApproveIT3',
            data: '{IT3_IT: ' + JSON.stringify(IT3_ID) + ', Remarks: ' + JSON.stringify(remarks) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response)
            {
                var modal2 = document.getElementById("myModal2");
                modal2.style.display = "none";
                alert('Approved Successfully');
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown); }
        });



    }
</script>

<script>

    var modal2 = document.getElementById("myModal2");
    var btn2 = document.getElementById("adjust");
    var span2 = document.getElementsByClassName("AD_btnn two")[0];

    span2.onclick = function () {
        modal2.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal2) {
            modal2.style.display = "none";
        }
    }
</script>
<style>
    .BreakIN, .Time-out {
        padding: 0px 15px;
        text-align: left;
    }

    div#myModal2 {
        background-color: #3b3e4796 !important;
    }

    .modal-content .content-header {
        border-top-left-radius: 11px;
        border-top-right-radius: 11px;
        padding-top: 10px;
    }

    div#myModal2 .modal-content .content-header h1 {
        margin-left: 0px !important;
        font-size: 18px !important;
        padding: 0;
        line-height: 1.3 !important;
    }

    .AD_btnn.two {
        font-size: 14px !important;
        margin: 0 !important;
        padding: 0 !important;
        cursor: pointer;
    }

    .AD_btnn:hover {
        color: #000;
        border-bottom: 2px solid #000 !important;
    }

    .AD_btnn {
        margin: 0 10px;
        font-size: 18px;
        font-weight: 700;
        color: #003780;
        background: none;
        border: 0;
        border-bottom: 2px solid transparent !important;
        border-radius: 0;
        padding: 5px;
    }
</style>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .table_out_data input {
                float: none;
                width: 10% !important;
                margin: 0px !important;
            }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }

            .table_out_data label {
                display: inline-block;
                margin-top: -4px;
            }
        </style>
        <!-- Modal -->
    </form>



</body>


</html>


