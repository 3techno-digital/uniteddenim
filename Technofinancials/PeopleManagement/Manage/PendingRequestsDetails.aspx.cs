﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
	public partial class PendingRequestsDetails : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {   
                divAlertMsg.Visible = false;
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/pending-request";

                if (HttpContext.Current.Items["ManagerId"] != null)
                {
                    objDB.EmployeeID = Convert.ToInt32(HttpContext.Current.Items["ManagerId"].ToString());
                    objDB.PayrollDate = Session["PendingRequestDate"].ToString();
                    DataTable dtt = new DataTable();
                    dtt = objDB.GetPendingRequestsDeatils(ref errorMsg);
                    gv.DataSource = dtt;
                    gv.DataBind();

                    if (dtt != null)
                    {
                        if (dtt.Rows.Count > 0)
                        {
                            gv.UseAccessibleHeader = true;
                            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                        }
                    }
                }
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }
    }
}