﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{

    public partial class BreakAdjustment : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int DirectReportEmployeeID
        {
            get
            {
                if (ViewState["DirectReportEmployeeID"] != null)
                {
                    return (int)ViewState["DirectReportEmployeeID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["DirectReportEmployeeID"] = value;
            }
        }

        protected string DocumentStatus
        {
            get
            {
                if (ViewState["DocumentStatus"] != null)
                {
                    return (string)ViewState["DocumentStatus"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["DocumentStatus"] = value;
            }
        }

        protected int BreakAdjustmentID
        {
            get
            {
                if (ViewState["BreakAdjustmentID"] != null)
                {
                    return (int)ViewState["BreakAdjustmentID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["BreakAdjustmentID"] = value;
            }
        }

        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return "Saved as Draft";
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["BreakAdjustmentID"] = null;
                    BindEmployeeDropdown();
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/break-adjustment";

                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    divAlertMsg.Visible = false;
                    clearFields();

                    if (HttpContext.Current.Items["BreakAdjustmentID"] != null)
                    {
                        BreakAdjustmentID = Convert.ToInt32(HttpContext.Current.Items["BreakAdjustmentID"].ToString());
                        getBreakAdjustmentByID(BreakAdjustmentID);
                        CheckAccess();
                    }
                    else
                    {
                        ddlEmployee.Enabled = true;
                        txtStartTime.Disabled =
                        txtEndTime.Disabled =
                        txtDate.Disabled =
                        txtDescription.Disabled = false;
                        DocumentStatus = "Data Submitted for Review";
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void checkDocStatus()
        {
            CheckSessions();
            if (Session["EmployeeID"].ToString() == ddlEmployee.SelectedValue)
            {
                btnApprove.Visible = false;
                btnDisapprove.Visible = false;
                btnReview.Visible = false;
                lnkReject.Visible = false;
                btnSave.Visible = false;
            }
            else if (Session["EmployeeID"].ToString() == DirectReportEmployeeID.ToString())
            {
                if (DocumentStatus == "Data Submitted for Review")
                {
                    btnReview.Visible = true;
                    lnkReject.Visible = true;
                    btnSave.Visible = true;
                }
                else if (DocumentStatus == "Reviewed")
                {
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                    btnSave.Visible = true;
                }
            }
        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnReview.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;

                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "BreakAdjustment";
                objDB.PrimaryColumnnName = "BreakAdjustmentID";
                objDB.PrimaryColumnValue = BreakAdjustmentID.ToString();
                objDB.DocName = "Loans";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getBreakAdjustmentByID(int BreakAdjustmentID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.BreakAdjustmentID = BreakAdjustmentID;
                dt = objDB.GetBreakAdjustmentByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        DocumentStatus = dt.Rows[0]["DocStatus"].ToString();
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        txtDescription.Value = dt.Rows[0]["Resultant"].ToString();

                        txtStartTime.Value = DateTime.Parse(dt.Rows[0]["BreakTimeIn"].ToString()).ToString("hh:mm tt");
                        txtEndTime.Value = DateTime.Parse(dt.Rows[0]["BreakTimeOut"].ToString()).ToString("hh:mm tt");

                        txtDate.Value = DateTime.Parse(dt.Rows[0]["AttendanceDate"].ToString()).ToString("dd-MMM-yyyy"); ;
                        objDB.EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"].ToString());
                        DataTable dtEmp = objDB.GetEmployeeByID(ref errorMsg);
                        if (dtEmp != null)
                        {
                            string str = dtEmp.Rows[0]["DirectReportingPerson"].ToString();
                            if (dtEmp.Rows.Count > 0 && !string.IsNullOrEmpty(dtEmp.Rows[0]["DirectReportingPerson"].ToString()))
                            {
                                DirectReportEmployeeID = Convert.ToInt32(dtEmp.Rows[0]["DirectReportingPerson"].ToString());
                            }
                        }
                    }
                }

                Common.addlog("View", "HR", "BreakAdjustment \"" + ddlEmployee.SelectedItem.Text + "\" Viewed", "BreakAdjustment", BreakAdjustmentID);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";
                string today = DateTime.Parse(txtDate.Value).ToShortDateString();
                string tommorow = DateTime.Parse(txtDate.Value).AddDays(1).ToShortDateString();
                DateTime Fromdate = DateTime.Parse(txtStartTime.Value), Todate = DateTime.Parse(txtEndTime.Value);
                DateTime Date = DateTime.Parse(txtDate.Value);
                Fromdate = DateTime.Parse(Date.ToString("dd-MMM-yyyy") + " " + Fromdate.ToString("hh:mm:ss tt"));
                Todate = DateTime.Parse(Date.ToString("dd-MMM-yyyy") + " " + Todate.ToString("hh:mm:ss tt"));
                Todate = Todate < Fromdate ? Todate.AddDays(1) : Todate;


                double hours = (Todate - Fromdate).TotalHours;
                if (hours > 16)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Adjustment should not exceed 16 hours";
                    return;
                }
                else
                {

                    objDB.Date = Date.ToString("dd-MMM-yyyy");
                    objDB.StartDate = Fromdate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                    objDB.EndDate = Todate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                    objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);

                    string result = objDB.GetEmployeeShiftStartTime();
                    if (result != "TRUE")
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = result;
                        return;
                    }
                }

                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.BreakStartTime = Fromdate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                objDB.BreakEndTime = Todate.ToString("dd-MMM-yyyy hh:mm:ss tt");
             
                objDB.Resultant = txtDescription.Value;
                objDB.Date = Date.ToString("dd-MMM-yyyy");
                int AdjustmentId = 0;
                if (HttpContext.Current.Items["BreakAdjustmentID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.BreakAdjustmentID = BreakAdjustmentID;
                    res = objDB.UpdateBreakAdjustment();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddBreakAdjustment(ref AdjustmentId);

                    clearFields();
                }

                if (res == "New Break Adjustment Added" || res == "Break Adjustment Data Updated")
                {
                    if (res == "New BreakAdjustment Added")
                    {
                        Common.addlog("Add", "HR", "New BreakAdjustment \"" + objDB.Title + "\" Added", "BreakAdjustment");
                        Common.addlogNew("Data Submitted for Review", "BreakAdjustment",
                  "Add Break Adjustment \"" + objDB.Title + "\" Added",
                  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "employee-self-service/manage/break-adjustment/edit-break-adjustment-" + AdjustmentId.ToString(),
                  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/break-adjustment/edit-break-adjustment-" + AdjustmentId.ToString(), "Break Adjustment", "BreakAdjustment", "Direct Reporting");
                    }

                    if (res == "BreakAdjustment Data Updated")
                    {
                        Common.addlog("Update", "HR", "BreakAdjustment \"" + objDB.Title + "\" Updated", "BreakAdjustment", BreakAdjustmentID);
                    }

                    if (DocStatus == "Approved")
                    {
                        objDB.BreakAdjustmentID = BreakAdjustmentID;
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        objDB.ApproveBreakAdjustment();
                    }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            ddlEmployee.SelectedIndex = -1;
            txtStartTime.Value =
            //txtBreakTime.Value =
            //txtBreakEndTime.Value =
            txtEndTime.Value =
            txtDate.Value =
            txtDescription.Value = "";
        }

        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                ddlEmployee.DataSource = objDB.GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "BreakAdjustment", "BreakAdjustmentID", HttpContext.Current.Items["BreakAdjustmentID"].ToString(), Session["UserName"].ToString());

                if (res == "Reviewed Sucessfull")
                {
                    DocumentStatus = "Reviewed";
                }
                if (btn.ID.ToString() == "btnSubForReview")
                {
                    if (Session["EmployeeID"] != null)
                    {
                        if (ddlEmployee.SelectedValue == Session["EmployeeID"].ToString())
                        {
                            objDB.Status = "Submited By Employee";
                            objDB.BreakAdjustmentID = BreakAdjustmentID;
                            res = objDB.UpdateBreakAdjustmentStatus();

                        }
                        else if (DirectReportEmployeeID == Convert.ToInt32(Session["EmployeeID"].ToString()))
                        {
                            objDB.Status = "Submited By Manager";
                            objDB.BreakAdjustmentID = BreakAdjustmentID;
                            res = objDB.UpdateBreakAdjustmentStatus();
                        }
                    }
                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                CheckAccess();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            checkDocStatus();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "BreakAdjustment", "BreakAdjustmentID", HttpContext.Current.Items["BreakAdjustmentID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "BreakAdjustment of ID \"" + HttpContext.Current.Items["BreakAdjustmentID"].ToString() + "\" deleted", "BreakAdjustment", BreakAdjustmentID);
                if (res == "Deleted Succesfully" || res == "Rejected")
                {
                    objDB.BreakAdjustmentID = BreakAdjustmentID;
                    objDB.EnableBreakAdjustment();
                }
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res;

                if (string.IsNullOrEmpty(txtAdjustmentNote.Value))
                {
                    res = "Please add a rejection note.";
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Please add a rejection note.";
                    return;
                }

                LinkButton btn = new LinkButton();
                res = Common.addAccessLevels("Reject", "BreakAdjustment", "BreakAdjustmentID", HttpContext.Current.Items["BreakAdjustmentID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "BreakAdjustment of ID \"" + HttpContext.Current.Items["BreakAdjustmentID"].ToString() + "\" deleted", "BreakAdjustment", BreakAdjustmentID);

                if (res == "Rejected")
                {
                    objDB.BreakAdjustmentID = BreakAdjustmentID;
                    objDB.BreakAdjustmentRemarks = txtAdjustmentNote.Value;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.RejectBreakAdjustment();
                }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}