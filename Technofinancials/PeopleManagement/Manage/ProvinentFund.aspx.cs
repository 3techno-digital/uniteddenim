﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class ProvinentFund : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false;

                    //btnApprove.Visible = false;
                    //btnReview.Visible = false;
                    //btnRevApprove.Visible = false;
                    //lnkReject.Visible = false;
                    //lnkDelete.Visible = false;
                    //btnSubForReview.Visible = false;
                    //btnDisapprove.Visible = false;


                    // CheckAccess();
                    getProvinentFunsByCompanyId();
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }


        //private void CheckAccess()
        //{
        //    btnSave.Visible = false;
        //    btnApprove.Visible = false;
        //    btnReview.Visible = false;
        //    btnRevApprove.Visible = false;
        //    lnkReject.Visible = false;
        //    lnkDelete.Visible = false;
        //    btnSubForReview.Visible = false;
        //    btnDisapprove.Visible = false;


        //    objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
        //    objDB.TableName = "ProvinentFunds";
        //    objDB.PrimaryColumnnName = "ProvinentFundID";
        //    objDB.PrimaryColumnValue = HttpContext.Current.Items["ProvinentFund"].ToString();
        //    objDB.DocName = "ProvinentFund";

        //    string chkAccessLevel = objDB.CheckDocAccessLevel();
        //    if (chkAccessLevel == "Can Edit")
        //    {
        //        btnSave.Visible = true;
        //        lnkDelete.Visible = true;
        //        btnSubForReview.Visible = true;
        //    }
        //    if (chkAccessLevel == "Can Edit & Review")
        //    {
        //        btnSave.Visible = true;
        //        btnReview.Visible = true;
        //        lnkReject.Visible = true;

        //    }
        //    if (chkAccessLevel == "Can Edit & Approve")
        //    {
        //        btnSave.Visible = true;
        //        btnApprove.Visible = true;
        //        btnDisapprove.Visible = true;
        //    }
        //    if (chkAccessLevel == "Can Edit, Review & Approve")
        //    {
        //        btnSave.Visible = true;
        //        btnRevApprove.Visible = true;
        //        lnkReject.Visible = true;
        //    }

        //}

        


        private void getProvinentFunsByCompanyId()
        {
            try
            {

                objDB.CompanyID = int.Parse(Session["CompanyID"].ToString());
                DataTable dt = objDB.GetProvinentFundByCompanyID(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {

                        txtCompanyRatio.Value = float.Parse(dt.Rows[0]["CompanyRatio"].ToString()).ToString("#.##");
                        txtEmployeeRatio.Value = float.Parse(dt.Rows[0]["EmployeeRatio"].ToString()).ToString("#.##");
                        txtEOBICompanyRatio.Value = float.Parse(dt.Rows[0]["EOBICompanyRatio"].ToString()).ToString("#.##");
                        txtEOBIEmployeeRatio.Value = float.Parse(dt.Rows[0]["EOBIEmployeeRatio"].ToString()).ToString("#.##");
                        txtIESSICompanyRatio.Value = float.Parse(dt.Rows[0]["IESSICompanyRatio"].ToString()).ToString("#.##");
                        txtIESSIEmployeeRatio.Value = float.Parse(dt.Rows[0]["IESSIEmployeeRatio"].ToString()).ToString("#.##");
                        txtOverTimeSimple.Value = float.Parse(dt.Rows[0]["OverTimeSimple"].ToString()).ToString("#.##");
                        txtOverTimeSpecial.Value = float.Parse(dt.Rows[0]["OverTimeSpecial"].ToString()).ToString("#.##");
                        objDB.DocID = int.Parse(Session["CompanyID"].ToString());
                        objDB.DocType = "ProvinentFund";
                        ltrNotesTable.Text = objDB.GetDocNotes();

                    }
                    else
                    {
                        txtCompanyRatio.Value = "0.00";
                        txtEmployeeRatio.Value = "0.00";
                    }
                }
                else
                {
                    txtCompanyRatio.Value = "0.00";
                    txtEmployeeRatio.Value = "0.00";
                }

                getPFSummary();
                Common.addlog("View", "HR", "Provinent Fund Ratio Viewed", "ProvinentFunds");

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void getPFSummary()
        {
            try
            {

                objDB.CompanyID = int.Parse(Session["CompanyID"].ToString());
                DataTable dt = objDB.GetProvinentFundsSummary(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        gv.DataSource = dt;
                        gv.DataBind();

                    }

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.CompanyRatio = float.Parse(txtCompanyRatio.Value);
                objDB.EmployeeRatio = float.Parse(txtEmployeeRatio.Value);

                objDB.EOBICompanyRatio = float.Parse(txtEOBICompanyRatio.Value);
                objDB.EOBIEmployeeRatio = float.Parse(txtEOBIEmployeeRatio.Value);

                objDB.IESSICompanyRatio = float.Parse(txtIESSICompanyRatio.Value);
                objDB.IESSIEmployeeRatio = float.Parse(txtIESSIEmployeeRatio.Value);

                objDB.OverTimeSimple = float.Parse(txtOverTimeSimple.Value);
                objDB.OverTimeSpecial = float.Parse(txtOverTimeSpecial.Value);

                objDB.CreatedBy = Session["UserName"].ToString();

                res = objDB.AddProvinentFund();

                objDB.DocType = "ProvinentFund";
                objDB.DocID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

                if (res == "Payroll Setup Updated" || res == "New Payroll Setup Added")
                {
                    if (res == "New Payroll Setup Added") { Common.addlog("Add", "HR", "New Provinent Fund Ratio Added", "ProvinentFunds"); }
                    if (res == "Payroll Setup Updated") { Common.addlog("Update", "HR", "Provinent Fund Ratio Updated", "ProvinentFunds"); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
    }
}