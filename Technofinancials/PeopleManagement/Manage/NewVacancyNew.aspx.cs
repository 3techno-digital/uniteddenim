﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class NewVacancyNew : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/new-vacancies";
                    //BindCompaniesDropDown();
                    BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
                    clearFields();
                    divAlertMsg.Visible = false;



                    ViewState["assetsSrNo"] = null;
                    ViewState["showAssetsFirstRow"] = null;
                    ViewState["dtJDAssets"] = null;
                    dtJDAssets = createJDAssets();
                    BindAssetsTable();

                    ViewState["AdditionsSrNo"] = null;
                    ViewState["showAdditionsFirstRow"] = null;
                    ViewState["dtJDAdditions"] = null;
                    dtJDAdditions = createJDAdditions();
                    BindAdditionsTable();

                    ViewState["DeductionsSrNo"] = null;
                    ViewState["showDeductionsFirstRow"] = null;
                    ViewState["dtJDDeductions"] = null;
                    dtJDDeductions = createJDAdditions();
                    BindDeductionsTable();

                    ViewState["SkillSetSrNo"] = null;
                    ViewState["showSkillSetFirstRow"] = null;
                    ViewState["dtJDSkillSet"] = null;
                    dtJDSkillSet = createJDAdditions();
                    BindSkillSetTable();

                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;


                    if (HttpContext.Current.Items["VacancyID"] != null)
                    {
                        //vacancyID = Convert.ToInt32(HttpContext.Current.Items["VacancyID"].ToString());
                        getVacancyByIDWithChildTables(Convert.ToInt32(HttpContext.Current.Items["VacancyID"].ToString()));
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }
        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "NewVacancy";
                objDB.PrimaryColumnnName = "VacancyID";
                objDB.PrimaryColumnValue = HttpContext.Current.Items["VacancyID"].ToString();
                objDB.DocName = "newVacancie";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getVacancyByIDWithChildTables(int vacancyID)
        {
            try
            {
                objDB.VacancyID = vacancyID;
                DataSet ds = objDB.GetVacancyByIDWithChildTables(ref errorMsg);
                if (ds != null)
                {

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {

                                ddlDept.SelectedIndex = ddlDept.Items.IndexOf(ddlDept.Items.FindByValue(dt.Rows[0]["DeptID"].ToString()));

                                if (ddlDept.SelectedIndex == -1)
                                    ddlDept.SelectedIndex = 0;

                                BindDesginationsDropDown(Convert.ToInt32(ddlDept.SelectedItem.Value));
                                ddlDesg.SelectedIndex = ddlDesg.Items.IndexOf(ddlDesg.Items.FindByValue(dt.Rows[0]["DesgID"].ToString()));
                                txtBasicSalary.Value = dt.Rows[0]["BasicSalary"].ToString();
                                txtDuration.Value = dt.Rows[0]["PositionToBeFilledIn"].ToString();
                                ddlEducation.SelectedIndex = ddlEducation.Items.IndexOf(ddlEducation.Items.FindByValue(dt.Rows[0]["Education"].ToString()));
                                ddlExperience.SelectedIndex = ddlExperience.Items.IndexOf(ddlExperience.Items.FindByValue(dt.Rows[0]["Experience"].ToString()));

                                getVacancyAssetsByVacancyID(ds.Tables[1]);
                                getVacancyAdditioinsByVacancyID(ds.Tables[2]);
                                getVacancyDeductionsByVacancyID(ds.Tables[3]);
                                getVacancySkillSetByVacancyID(ds.Tables[4]);
                                ltrNotesTable.Text = Common.GetDocNotesInString(ds.Tables[4]);
                                
                            }
                        }
                    }
                }

                Common.addlog("View", "HR", "Vacancy \"" + ddlDesg.SelectedItem.Text + "\" Viewed", "NewVacancy", objDB.VacancyID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getVacancyAssetsByVacancyID(DataTable dt)
        {
            try
            {
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtJDAssets.Rows[0][0].ToString() == "")
                        {
                            dtJDAssets.Rows[0].Delete();
                            dtJDAssets.AcceptChanges();
                            showAssetsFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtJDAssets.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["AssetTitle"],
                            dt.Rows[i]["Quantity"],
                            dt.Rows[i]["AssetDescription"]
                        });
                        }
                        assetsSrNo = Convert.ToInt32(dtJDAssets.Rows[dtJDAssets.Rows.Count - 1][0].ToString()) + 1;
                    }
                }


                BindAssetsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void getVacancyAdditioinsByVacancyID(DataTable dt)
        {
            try
            {
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtJDAdditions.Rows[0][0].ToString() == "")
                        {
                            dtJDAdditions.Rows[0].Delete();
                            dtJDAdditions.AcceptChanges();
                            showAdditionsFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtJDAdditions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                        }
                        AdditionsSrNo = Convert.ToInt32(dtJDAdditions.Rows[dtJDAdditions.Rows.Count - 1][0].ToString()) + 1;
                    }
                }

                BindAdditionsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void getVacancyDeductionsByVacancyID(DataTable dt)
        {
            try
            {
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtJDDeductions.Rows[0][0].ToString() == "")
                        {
                            dtJDDeductions.Rows[0].Delete();
                            dtJDDeductions.AcceptChanges();
                            showDeductionsFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtJDDeductions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                        }
                        DeductionsSrNo = Convert.ToInt32(dtJDDeductions.Rows[dtJDDeductions.Rows.Count - 1][0].ToString()) + 1;
                    }
                }

                BindDeductionsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void getVacancySkillSetByVacancyID(DataTable dt)
        {
            try
            {
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtJDSkillSet.Rows[0][0].ToString() == "")
                        {
                            dtJDSkillSet.Rows[0].Delete();
                            dtJDSkillSet.AcceptChanges();
                            showSkillSetFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtJDSkillSet.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"]
                        });
                        }
                        SkillSetSrNo = Convert.ToInt32(dtJDSkillSet.Rows[dtJDSkillSet.Rows.Count - 1][0].ToString()) + 1;
                    }
                }

                BindSkillSetTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            try
            {

                ddlDept.SelectedIndex = 0;
                BindDesginationsDropDown(Convert.ToInt32(ddlDept.SelectedItem.Value));
                txtBasicSalary.Value = "0";
                ddlEducation.SelectedIndex = 0;
                ddlExperience.SelectedIndex = 0;
                txtDuration.Value = "0";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void BindDepartmentDropDown(int companyID)
        {
            try
            {

                objDB.CompanyID = companyID;
                ddlDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                ddlDept.DataTextField = "DeptName";
                ddlDept.DataValueField = "DeptID";
                ddlDept.DataBind();
                ddlDept.Items.Insert(0, new ListItem("--- None ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckSessions()
        {
            try
            {

                if (Session["UserID"] == null && Session["EmployeeID"] == null)
                    Response.Redirect("/login");
                else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                    Response.Redirect("/login");
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                bool isNew = false;
                int vacancyID = 0;
                CheckSessions();
                objDB.DesgID = Convert.ToInt32(ddlDesg.SelectedItem.Value);
                objDB.PositionToBeFilledIn = Convert.ToInt32(txtDuration.Value);
                objDB.Education = ddlEducation.SelectedItem.Value;
                objDB.Experiencce = ddlExperience.SelectedItem.Value;
                objDB.BasicSalary = float.Parse(txtBasicSalary.Value);

                string res = "";
                if (HttpContext.Current.Items["VacancyID"] != null)
                {
                    vacancyID = Convert.ToInt32(HttpContext.Current.Items["VacancyID"]);
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.VacancyID = vacancyID;
                    res = objDB.UpdateVacancy();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    vacancyID = Convert.ToInt32(objDB.AddVacancy());
                    res = "New Vacancy Added";
                    clearFields();
                    isNew = true;
                }


                objDB.DocType = "newVacancie";
                objDB.DocID = vacancyID;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

                objDB.VacancyID = vacancyID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteNewVacancyAssetsByVacancyID();

                dtJDAssets = (DataTable)ViewState["dtJDAssets"] as DataTable;
                if (dtJDAssets != null)
                {
                    if (dtJDAssets.Rows.Count > 0)
                    {
                        if (!showAssetsFirstRow)
                        {
                            dtJDAssets.Rows[0].Delete();
                            dtJDAssets.AcceptChanges();
                        }

                        for (int i = 0; i < dtJDAssets.Rows.Count; i++)
                        {
                            objDB.VacancyID = Convert.ToInt32(vacancyID);
                            objDB.AssetTitle = dtJDAssets.Rows[i]["Title"].ToString();
                            objDB.Quantity = Convert.ToInt32(dtJDAssets.Rows[i]["Qty"].ToString());
                            objDB.AssetDesc = dtJDAssets.Rows[i]["Desc"].ToString();
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddNewVacancyAssets();
                        }
                    }
                }

                if (dtJDAssets.Rows.Count == 0 || isNew)
                {
                    ViewState["assetsSrNo"] = null;
                    ViewState["showAssetsFirstRow"] = null;
                    ViewState["dtJDAssets"] = null;

                }


                BindAssetsTable();


                objDB.VacancyID = vacancyID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteNewVacancyBudgetByVacancyID();

                dtJDAdditions = (DataTable)ViewState["dtJDAdditions"] as DataTable;
                if (dtJDAdditions != null)
                {
                    if (dtJDAdditions.Rows.Count > 0)
                    {
                        if (!showAdditionsFirstRow)
                        {
                            dtJDAdditions.Rows[0].Delete();
                            dtJDAdditions.AcceptChanges();
                        }

                        for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
                        {
                            objDB.VacancyID = Convert.ToInt32(vacancyID);
                            objDB.BudgetTitle = dtJDAdditions.Rows[i]["Title"].ToString();
                            objDB.BudgetType = dtJDAdditions.Rows[i]["Type"].ToString();
                            objDB.Amount = dtJDAdditions.Rows[i]["Amount"].ToString();
                            objDB.TransictionType = "Additions";
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddNewVacancyBudget();
                        }
                    }
                }

                if (dtJDAdditions.Rows.Count == 0 || isNew)
                {
                    ViewState["AdditionsSrNo"] = null;
                    ViewState["showAdditionsFirstRow"] = null;
                    ViewState["dtJDAdditions"] = null;
                }


                BindAdditionsTable();

                dtJDDeductions = (DataTable)ViewState["dtJDDeductions"] as DataTable;
                if (dtJDDeductions != null)
                {
                    if (dtJDDeductions.Rows.Count > 0)
                    {
                        if (!showDeductionsFirstRow)
                        {
                            dtJDDeductions.Rows[0].Delete();
                            dtJDDeductions.AcceptChanges();
                        }

                        for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
                        {
                            objDB.VacancyID = Convert.ToInt32(vacancyID);
                            objDB.BudgetTitle = dtJDDeductions.Rows[i]["Title"].ToString();
                            objDB.BudgetType = dtJDDeductions.Rows[i]["Type"].ToString();
                            objDB.Amount = dtJDDeductions.Rows[i]["Amount"].ToString();
                            objDB.TransictionType = "Deductions";
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddNewVacancyBudget();
                        }
                    }
                }

                if (dtJDDeductions.Rows.Count == 0 || isNew)
                {
                    ViewState["DeductionsSrNo"] = null;
                    ViewState["showDeductionsFirstRow"] = null;
                    ViewState["dtJDDeductions"] = null;
                }


                BindDeductionsTable();


                objDB.VacancyID = vacancyID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteNewVacancySkillSetByVacancyID();

                dtJDSkillSet = (DataTable)ViewState["dtJDSkillSet"] as DataTable;
                if (dtJDSkillSet != null)
                {
                    if (dtJDSkillSet.Rows.Count > 0)
                    {
                        if (!showSkillSetFirstRow)
                        {
                            dtJDSkillSet.Rows[0].Delete();
                            dtJDSkillSet.AcceptChanges();
                        }

                        for (int i = 0; i < dtJDSkillSet.Rows.Count; i++)
                        {
                            objDB.VacancyID = Convert.ToInt32(vacancyID);
                            objDB.Title = dtJDSkillSet.Rows[i]["Title"].ToString();
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddNewVacancySkillSet();
                        }
                    }
                }

                if (dtJDSkillSet.Rows.Count == 0 || isNew)
                {
                    ViewState["SkillSetSrNo"] = null;
                    ViewState["showSkillSetFirstRow"] = null;
                    ViewState["dtJDSkillSet"] = null;
                }


                BindSkillSetTable();

                if (res == "New Vacancy Added" || res == "Vacancy Updated")
                {
                    if (res == "New Vacancy Added") { Common.addlog("Add", "HR", "New Vacancy \"" + ddlDesg.SelectedItem.Text + "\" Added", "NewVacancy"); }
                    if (res == "Vacancy Updated") { Common.addlog("Update", "HR", "Vacancy \"" + ddlDesg.SelectedItem.Text + "\" Updated", "NewVacancy", objDB.VacancyID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;

                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                ddlDesg.Items.Clear();
                if (ddlDept.SelectedIndex != 0)
                {
                    BindDesginationsDropDown(Convert.ToInt32(ddlDept.SelectedItem.Value));
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void BindDesginationsDropDown(int DeptID)
        {
            try
            {
                objDB.DeptID = DeptID;
                ddlDesg.DataSource = objDB.GetAllDesignationByDepartmentID(ref errorMsg);
                ddlDesg.DataTextField = "DesgTitle";
                ddlDesg.DataValueField = "DesgID";
                ddlDesg.DataBind();
                ddlDesg.Items.Insert(0, new ListItem("--- Select Designation ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private DataTable dtJDAssets
        {
            get
            {
                if (ViewState["dtJDAssets"] != null)
                {
                    return (DataTable)ViewState["dtJDAssets"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtJDAssets"] = value;
            }
        }
        bool showAssetsFirstRow
        {
            get
            {
                if (ViewState["showAssetsFirstRow"] != null)
                {
                    return (bool)ViewState["showAssetsFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showAssetsFirstRow"] = value;
            }
        }
        protected int assetsSrNo
        {
            get
            {
                if (ViewState["assetsSrNo"] != null)
                {
                    return (int)ViewState["assetsSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["assetsSrNo"] = value;
            }
        }
        private DataTable createJDAssets()
        {
            try
            {

                DataTable dt = new DataTable();
                dt.Columns.Add("SrNo");
                dt.Columns.Add("Title");
                dt.Columns.Add("Qty");
                dt.Columns.Add("Desc");
                dt.AcceptChanges();

                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
                dt.AcceptChanges();

                return dt;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                return null;
            }

        }
        protected void BindAssetsTable()
        {
            try
            {

                if (ViewState["dtJDAssets"] == null)
                {
                    dtJDAssets = createJDAssets();
                    ViewState["dtJDAssets"] = dtJDAssets;
                }

                gvAssets.DataSource = dtJDAssets;
                gvAssets.DataBind();

                if (showAssetsFirstRow)
                    gvAssets.Rows[0].Visible = true;
                else
                    gvAssets.Rows[0].Visible = false;

                gvAssets.UseAccessibleHeader = true;
                gvAssets.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvAssets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                    txtSrNo.Text = assetsSrNo.ToString();
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvAssets_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {

                int index = e.RowIndex;

                dtJDAssets.Rows[index].SetField(1, ((TextBox)gvAssets.Rows[e.RowIndex].FindControl("txtEditTitle")).Text);
                dtJDAssets.Rows[index].SetField(2, ((TextBox)gvAssets.Rows[e.RowIndex].FindControl("txtEditQty")).Text);
                dtJDAssets.Rows[index].SetField(3, ((TextBox)gvAssets.Rows[e.RowIndex].FindControl("txtEditDesc")).Text);

                dtJDAssets.AcceptChanges();

                gvAssets.EditIndex = -1;
                BindAssetsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvAssets_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                gvAssets.EditIndex = -1;
                BindAssetsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvAssets_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {

                gvAssets.EditIndex = e.NewEditIndex;
                BindAssetsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {

                LinkButton lnk = (LinkButton)sender as LinkButton;
                string delSr = lnk.CommandArgument.ToString();
                for (int i = 0; i < dtJDAssets.Rows.Count; i++)
                {
                    if (dtJDAssets.Rows[i][0].ToString() == delSr)
                    {
                        dtJDAssets.Rows[i].Delete();
                        dtJDAssets.AcceptChanges();
                    }
                }
                for (int i = 0; i < dtJDAssets.Rows.Count; i++)
                {
                    dtJDAssets.Rows[i].SetField(0, i + 1);
                    dtJDAssets.AcceptChanges();
                }
                if (dtJDAssets.Rows.Count < 1)
                {
                    DataRow dr = dtJDAssets.NewRow();
                    dtJDAssets.Rows.Add(dr);
                    dtJDAssets.AcceptChanges();
                    showAssetsFirstRow = false;
                }
                if (showAssetsFirstRow)
                    assetsSrNo = dtJDAssets.Rows.Count + 1;
                else
                    assetsSrNo = 1;

                BindAssetsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void btnAddAsset_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow dr = dtJDAssets.NewRow();
                dr[0] = assetsSrNo.ToString();
                dr[1] = ((TextBox)gvAssets.FooterRow.FindControl("txtTitle")).Text;
                dr[2] = ((TextBox)gvAssets.FooterRow.FindControl("txtQty")).Text;
                dr[3] = ((TextBox)gvAssets.FooterRow.FindControl("txtDesc")).Text;
                dtJDAssets.Rows.Add(dr);
                dtJDAssets.AcceptChanges();


                if (dtJDAssets.Rows[0][0].ToString() == "")
                {
                    dtJDAssets.Rows[0].Delete();
                    dtJDAssets.AcceptChanges();
                    showAssetsFirstRow = true;
                }


                assetsSrNo += 1;
                BindAssetsTable();
                ((Label)gvAssets.FooterRow.FindControl("txtSrNo")).Text = assetsSrNo.ToString();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private DataTable dtJDAdditions
        {
            get
            {
                if (ViewState["dtJDAdditions"] != null)
                {
                    return (DataTable)ViewState["dtJDAdditions"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtJDAdditions"] = value;
            }
        }
        bool showAdditionsFirstRow
        {
            get
            {
                if (ViewState["showAdditionsFirstRow"] != null)
                {
                    return (bool)ViewState["showAdditionsFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showAdditionsFirstRow"] = value;
            }
        }
        protected int AdditionsSrNo
        {
            get
            {
                if (ViewState["AdditionsSrNo"] != null)
                {
                    return (int)ViewState["AdditionsSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["AdditionsSrNo"] = value;
            }
        }
        private DataTable createJDAdditions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("Type");
            dt.Columns.Add("Amount");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindAdditionsTable()
        {
            try
            {

                if (ViewState["dtJDAdditions"] == null)
                {
                    dtJDAdditions = createJDAdditions();
                    ViewState["dtJDAdditions"] = dtJDAdditions;
                }

                gvAdditions.DataSource = dtJDAdditions;
                gvAdditions.DataBind();

                if (showAdditionsFirstRow)
                    gvAdditions.Rows[0].Visible = true;
                else
                    gvAdditions.Rows[0].Visible = false;

                gvAdditions.UseAccessibleHeader = true;
                gvAdditions.HeaderRow.TableSection = TableRowSection.TableHeader;

                float total = 0;
                if (dtJDAdditions != null)
                {
                    if (dtJDAdditions.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
                        {
                            if (dtJDAdditions.Rows[i][3].ToString() != "")
                            {
                                if (txtBasicSalary.Value != "")
                                {
                                    if (dtJDAdditions.Rows[i][2].ToString() == "Percentage")
                                    {
                                        total += ((float.Parse(dtJDAdditions.Rows[i][3].ToString()) / 100) * float.Parse(txtBasicSalary.Value));
                                    }
                                    else
                                    {
                                        total += float.Parse(dtJDAdditions.Rows[i][3].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            ((Label)gvAdditions.FooterRow.FindControl("lblTotal")).Text = "Total : " + string.Format("{0:f2}", total);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvAdditions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                    txtSrNo.Text = AdditionsSrNo.ToString();
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvAdditions_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                int index = e.RowIndex;

                dtJDAdditions.Rows[index].SetField(1, ((TextBox)gvAdditions.Rows[e.RowIndex].FindControl("txtEditTitle")).Text);
                dtJDAdditions.Rows[index].SetField(2, ((DropDownList)gvAdditions.Rows[e.RowIndex].FindControl("ddlEditType")).SelectedItem.Text);
                dtJDAdditions.Rows[index].SetField(3, ((TextBox)gvAdditions.Rows[e.RowIndex].FindControl("txtEditAmount")).Text);

                dtJDAdditions.AcceptChanges();

                gvAdditions.EditIndex = -1;
                BindAdditionsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvAdditions_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvAdditions.EditIndex = -1;
            BindAdditionsTable();
        }
        protected void gvAdditions_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvAdditions.EditIndex = e.NewEditIndex;
            BindAdditionsTable();
        }
        protected void lnkRemoveAdditions_Command(object sender, CommandEventArgs e)
        {
            try
            {

                LinkButton lnk = (LinkButton)sender as LinkButton;
                string delSr = lnk.CommandArgument.ToString();
                for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
                {
                    if (dtJDAdditions.Rows[i][0].ToString() == delSr)
                    {
                        dtJDAdditions.Rows[i].Delete();
                        dtJDAdditions.AcceptChanges();
                    }
                }
                for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
                {
                    dtJDAdditions.Rows[i].SetField(0, i + 1);
                    dtJDAdditions.AcceptChanges();
                }
                if (dtJDAdditions.Rows.Count < 1)
                {
                    DataRow dr = dtJDAdditions.NewRow();
                    dtJDAdditions.Rows.Add(dr);
                    dtJDAdditions.AcceptChanges();
                    showAdditionsFirstRow = false;
                }
                if (showAdditionsFirstRow)
                    AdditionsSrNo = dtJDAdditions.Rows.Count + 1;
                else
                    AdditionsSrNo = 1;

                BindAdditionsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void btnAddAdditions_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow dr = dtJDAdditions.NewRow();
                dr[0] = AdditionsSrNo.ToString();
                dr[1] = ((TextBox)gvAdditions.FooterRow.FindControl("txtTitle")).Text;
                dr[2] = ((DropDownList)gvAdditions.FooterRow.FindControl("ddlType")).SelectedItem.Text;
                dr[3] = ((TextBox)gvAdditions.FooterRow.FindControl("txtAmount")).Text;
                dtJDAdditions.Rows.Add(dr);
                dtJDAdditions.AcceptChanges();


                if (dtJDAdditions.Rows[0][0].ToString() == "")
                {
                    dtJDAdditions.Rows[0].Delete();
                    dtJDAdditions.AcceptChanges();
                    showAdditionsFirstRow = true;
                }


                AdditionsSrNo += 1;
                BindAdditionsTable();
                ((Label)gvAdditions.FooterRow.FindControl("txtSrNo")).Text = AdditionsSrNo.ToString();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private DataTable dtJDDeductions
        {
            get
            {
                if (ViewState["dtJDDeductions"] != null)
                {
                    return (DataTable)ViewState["dtJDDeductions"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtJDDeductions"] = value;
            }
        }
        bool showDeductionsFirstRow
        {
            get
            {
                if (ViewState["showDeductionsFirstRow"] != null)
                {
                    return (bool)ViewState["showDeductionsFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showDeductionsFirstRow"] = value;
            }
        }
        protected int DeductionsSrNo
        {
            get
            {
                if (ViewState["DeductionsSrNo"] != null)
                {
                    return (int)ViewState["DeductionsSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["DeductionsSrNo"] = value;
            }
        }
        private DataTable createJDDeductions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("Type");
            dt.Columns.Add("Amount");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindDeductionsTable()
        {
            try
            {

                if (ViewState["dtJDDeductions"] == null)
                {
                    dtJDDeductions = createJDDeductions();
                    ViewState["dtJDDeductions"] = dtJDDeductions;
                }

                gvDeductions.DataSource = dtJDDeductions;
                gvDeductions.DataBind();

                if (showDeductionsFirstRow)
                    gvDeductions.Rows[0].Visible = true;
                else
                    gvDeductions.Rows[0].Visible = false;

                gvDeductions.UseAccessibleHeader = true;
                gvDeductions.HeaderRow.TableSection = TableRowSection.TableHeader;

                float total = 0;
                if (dtJDDeductions != null)
                {
                    if (dtJDDeductions.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
                        {
                            if (dtJDDeductions.Rows[i][3].ToString() != "")
                            {
                                if (txtBasicSalary.Value != "")
                                {
                                    if (dtJDDeductions.Rows[i][2].ToString() == "Percentage")
                                    {
                                        total += ((float.Parse(dtJDDeductions.Rows[i][3].ToString()) / 100) * float.Parse(txtBasicSalary.Value));
                                    }
                                    else
                                    {
                                        total += float.Parse(dtJDDeductions.Rows[i][3].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            ((Label)gvDeductions.FooterRow.FindControl("lblTotal")).Text = "Total : " + string.Format("{0:f2}", total);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvDeductions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = DeductionsSrNo.ToString();
            }
        }
        protected void gvDeductions_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            dtJDDeductions.Rows[index].SetField(1, ((TextBox)gvDeductions.Rows[e.RowIndex].FindControl("txtEditTitle")).Text);
            dtJDDeductions.Rows[index].SetField(2, ((DropDownList)gvDeductions.Rows[e.RowIndex].FindControl("ddlEditType")).SelectedItem.Text);
            dtJDDeductions.Rows[index].SetField(3, ((TextBox)gvDeductions.Rows[e.RowIndex].FindControl("txtEditAmount")).Text);

            dtJDDeductions.AcceptChanges();

            gvDeductions.EditIndex = -1;
            BindDeductionsTable();
        }
        protected void gvDeductions_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDeductions.EditIndex = -1;
            BindDeductionsTable();
        }
        protected void gvDeductions_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDeductions.EditIndex = e.NewEditIndex;
            BindDeductionsTable();
        }
        protected void lnkRemoveDeductions_Command(object sender, CommandEventArgs e)
        {
            try
            {

                LinkButton lnk = (LinkButton)sender as LinkButton;
                string delSr = lnk.CommandArgument.ToString();
                for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
                {
                    if (dtJDDeductions.Rows[i][0].ToString() == delSr)
                    {
                        dtJDDeductions.Rows[i].Delete();
                        dtJDDeductions.AcceptChanges();
                    }
                }
                for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
                {
                    dtJDDeductions.Rows[i].SetField(0, i + 1);
                    dtJDDeductions.AcceptChanges();
                }
                if (dtJDDeductions.Rows.Count < 1)
                {
                    DataRow dr = dtJDDeductions.NewRow();
                    dtJDDeductions.Rows.Add(dr);
                    dtJDDeductions.AcceptChanges();
                    showDeductionsFirstRow = false;
                }
                if (showDeductionsFirstRow)
                    DeductionsSrNo = dtJDDeductions.Rows.Count + 1;
                else
                    DeductionsSrNo = 1;

                BindDeductionsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void btnAddDeductions_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow dr = dtJDDeductions.NewRow();
                dr[0] = DeductionsSrNo.ToString();
                dr[1] = ((TextBox)gvDeductions.FooterRow.FindControl("txtTitle")).Text;
                dr[2] = ((DropDownList)gvDeductions.FooterRow.FindControl("ddlType")).SelectedItem.Text;
                dr[3] = ((TextBox)gvDeductions.FooterRow.FindControl("txtAmount")).Text;
                dtJDDeductions.Rows.Add(dr);
                dtJDDeductions.AcceptChanges();


                if (dtJDDeductions.Rows[0][0].ToString() == "")
                {
                    dtJDDeductions.Rows[0].Delete();
                    dtJDDeductions.AcceptChanges();
                    showDeductionsFirstRow = true;
                }


                DeductionsSrNo += 1;
                BindDeductionsTable();
                ((Label)gvDeductions.FooterRow.FindControl("txtSrNo")).Text = DeductionsSrNo.ToString();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void ddlDesg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();

                assetsSrNo = 1;
                showAssetsFirstRow = false;
                dtJDAssets = createJDAssets();
                BindAssetsTable();

                AdditionsSrNo = 1;
                showAdditionsFirstRow = false;
                dtJDAdditions = createJDAdditions();
                BindAdditionsTable();

                DeductionsSrNo = 1;
                showDeductionsFirstRow = false;
                dtJDDeductions = createJDAdditions();
                BindDeductionsTable();

                txtBasicSalary.Value = "0";
                if (ddlDesg.SelectedIndex != 0)
                {
                    getNodeByDesgID(Convert.ToInt32(ddlDesg.SelectedItem.Value));
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void getNodeByDesgID(int desgID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.DesgID = desgID;
                dt = objDB.GetNodeByDesgID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtBasicSalary.Value = dt.Rows[0]["BasicSalary"].ToString();
                        getJDAssetsByNodeID(Convert.ToInt32(dt.Rows[0]["NodeID"].ToString()));
                        getJDAdditionsByNodeID(Convert.ToInt32(dt.Rows[0]["NodeID"].ToString()));
                        getJDDeductionsByNodeID(Convert.ToInt32(dt.Rows[0]["NodeID"].ToString()));

                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void getJDAssetsByNodeID(int nodeID)
        {

            try
            {

                DataTable dt = new DataTable();
                objDB.NodeID = nodeID;
                dt = objDB.GetAllJDSAssetsByNodeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtJDAssets.Rows[0][0].ToString() == "")
                        {
                            dtJDAssets.Rows[0].Delete();
                            dtJDAssets.AcceptChanges();
                            showAssetsFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtJDAssets.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["AssetTitle"],
                            dt.Rows[i]["Quantity"],
                            dt.Rows[i]["AssetDescription"]
                        });
                        }
                        assetsSrNo = Convert.ToInt32(dtJDAssets.Rows[dtJDAssets.Rows.Count - 1][0].ToString()) + 1;
                    }
                }

                BindAssetsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void getJDAdditionsByNodeID(int nodeID)
        {
            try
            {

                DataTable dt = new DataTable();
                objDB.NodeID = nodeID;
                dt = objDB.GetAllJDSBudgetByNodeID(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        dt = Common.filterTable(dt, "TransictionType", "Additions");
                    }
                }

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtJDAdditions.Rows[0][0].ToString() == "")
                        {
                            dtJDAdditions.Rows[0].Delete();
                            dtJDAdditions.AcceptChanges();
                            showAdditionsFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtJDAdditions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                        }
                        AdditionsSrNo = Convert.ToInt32(dtJDAdditions.Rows[dtJDAdditions.Rows.Count - 1][0].ToString()) + 1;
                    }
                }

                BindAdditionsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void getJDDeductionsByNodeID(int nodeID)
        {
            try
            {

                DataTable dt = new DataTable();
                objDB.NodeID = nodeID;
                dt = objDB.GetAllJDSBudgetByNodeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        dt = Common.filterTable(dt, "TransictionType", "Deductions");
                    }
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtJDDeductions.Rows[0][0].ToString() == "")
                        {
                            dtJDDeductions.Rows[0].Delete();
                            dtJDDeductions.AcceptChanges();
                            showDeductionsFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtJDDeductions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                        }
                        DeductionsSrNo = Convert.ToInt32(dtJDDeductions.Rows[dtJDDeductions.Rows.Count - 1][0].ToString()) + 1;
                    }
                }

                BindDeductionsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private DataTable dtJDSkillSet
        {
            get
            {
                if (ViewState["dtJDSkillSet"] != null)
                {
                    return (DataTable)ViewState["dtJDSkillSet"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtJDSkillSet"] = value;
            }
        }
        bool showSkillSetFirstRow
        {
            get
            {
                if (ViewState["showSkillSetFirstRow"] != null)
                {
                    return (bool)ViewState["showSkillSetFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showSkillSetFirstRow"] = value;
            }
        }
        protected int SkillSetSrNo
        {
            get
            {
                if (ViewState["SkillSetSrNo"] != null)
                {
                    return (int)ViewState["SkillSetSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["SkillSetSrNo"] = value;
            }
        }
        private DataTable createJDSkillSet()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindSkillSetTable()
        {
            try
            {
                if (ViewState["dtJDSkillSet"] == null)
                {
                    dtJDSkillSet = createJDSkillSet();
                    ViewState["dtJDSkillSet"] = dtJDSkillSet;
                }

                gvSkillSet.DataSource = dtJDSkillSet;
                gvSkillSet.DataBind();

                if (showSkillSetFirstRow)
                    gvSkillSet.Rows[0].Visible = true;
                else
                    gvSkillSet.Rows[0].Visible = false;

                gvSkillSet.UseAccessibleHeader = true;
                gvSkillSet.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvSkillSet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = SkillSetSrNo.ToString();
            }
        }
        protected void gvSkillSet_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                int index = e.RowIndex;

                dtJDSkillSet.Rows[index].SetField(1, ((TextBox)gvSkillSet.Rows[e.RowIndex].FindControl("txtEditTitle")).Text);

                dtJDSkillSet.AcceptChanges();

                gvSkillSet.EditIndex = -1;
                BindSkillSetTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvSkillSet_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSkillSet.EditIndex = -1;
            BindSkillSetTable();
        }
        protected void gvSkillSet_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSkillSet.EditIndex = e.NewEditIndex;
            BindSkillSetTable();
        }
        protected void lnkRemoveSkillSet_Command(object sender, CommandEventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender as LinkButton;
                string delSr = lnk.CommandArgument.ToString();
                for (int i = 0; i < dtJDSkillSet.Rows.Count; i++)
                {
                    if (dtJDSkillSet.Rows[i][0].ToString() == delSr)
                    {
                        dtJDSkillSet.Rows[i].Delete();
                        dtJDSkillSet.AcceptChanges();
                    }
                }
                for (int i = 0; i < dtJDSkillSet.Rows.Count; i++)
                {
                    dtJDSkillSet.Rows[i].SetField(0, i + 1);
                    dtJDSkillSet.AcceptChanges();
                }
                if (dtJDSkillSet.Rows.Count < 1)
                {
                    DataRow dr = dtJDSkillSet.NewRow();
                    dtJDSkillSet.Rows.Add(dr);
                    dtJDSkillSet.AcceptChanges();
                    showSkillSetFirstRow = false;
                }
                if (showSkillSetFirstRow)
                    SkillSetSrNo = dtJDSkillSet.Rows.Count + 1;
                else
                    SkillSetSrNo = 1;

                BindSkillSetTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void btnAddSkillSet_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dr = dtJDSkillSet.NewRow();
                dr[0] = SkillSetSrNo.ToString();
                dr[1] = ((TextBox)gvSkillSet.FooterRow.FindControl("txtTitle")).Text;
                dtJDSkillSet.Rows.Add(dr);
                dtJDSkillSet.AcceptChanges();


                if (dtJDSkillSet.Rows[0][0].ToString() == "")
                {
                    dtJDSkillSet.Rows[0].Delete();
                    dtJDSkillSet.AcceptChanges();
                    showSkillSetFirstRow = true;
                }


                SkillSetSrNo += 1;
                BindSkillSetTable();
                ((Label)gvSkillSet.FooterRow.FindControl("txtSrNo")).Text = SkillSetSrNo.ToString();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {

                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "NewVacancy", "VacancyID", HttpContext.Current.Items["VacancyID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "Vacancy of ID\"" + HttpContext.Current.Items["VacancyID"].ToString() + "\" Status Changed", "NewVacancy", Convert.ToInt32(HttpContext.Current.Items["VacancyID"].ToString()));

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;

                CheckAccess();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "NewVacancy", "VacancyID", HttpContext.Current.Items["VacancyID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "Vacancy of ID \"" + HttpContext.Current.Items["VacancyID"].ToString() + "\" deleted", "NewVacancy", Convert.ToInt32(HttpContext.Current.Items["VacancyID"].ToString()));

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

    }
}