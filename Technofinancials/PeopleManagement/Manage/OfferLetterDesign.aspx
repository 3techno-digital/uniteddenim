﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OfferLetterDesign.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.OfferLetters" ValidateRequest="false" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
   
        <main id="app-main" class="app-main">
            <div class="wrap">

                      <asp:UpdatePanel ID="UpdPnl" runat="server">
            <ContentTemplate>
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img src="/assets/images/Offer Letter Design.png" class="img-responsive tf-page-heading-img" id="imgHeading" runat="server" />
                                <h1 class="m-0 text-dark" id="pageHeading" runat="server">Offer Letter Design</h1>
                            </div>


                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div style="text-align: right;">
                                    <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                        </div>
                         <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="alert alert-info">
                                    Please use the keywords to bind the database values. For keywords list please 
                                 <a href="#"  data-toggle="modal" data-target="#modalOfferLetter" title="View" id="btnOfferLetter" runat="server">Click Here</a>
                                 <a href="#"  data-toggle="modal" data-target="#modalWarningLetter" title="View" id="btnWarningLetter" runat="server">Click Here</a>
                                 <a href="#" data-toggle="modal" data-target="#modalPayroll" title="View" id="btnPayrollLetter" runat="server">Click Here</a>
                                 <a href="#" data-toggle="modal" data-target="#modalAttendanceSheet" title="View" id="btnAttendanceSheet" runat="server">Click Here</a>
                                 <a href="#"  data-toggle="modal" data-target="#modalPromotionLetter" title="View" id="btnPromotionLetter" runat="server">Click Here</a>
                                 <a href="#"  data-toggle="modal" data-target="#modalEmpSummary" title="View" id="btnEmpSummary" runat="server">Click Here</a>
                                 <a href="#"  data-toggle="modal" data-target="#modalHRTemplate" title="View" id="btnHRReport" runat="server">Click Here</a>
                                 <a href="#"  data-toggle="modal" data-target="#modalPaySlip" title="View" id="btnPaySlip" runat="server">Click Here</a>

                                </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                        </div>
<%--                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>--%>


                    </div>
                    <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark">Header</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="">
                                <textboxio:Textboxio runat="server" ID="txtHeader" ScriptSrc="/assets/textboxio/textboxio.js" Width="100%" Height="300px" />
                            </div>
                        </div>
                        <div class="col-md-4 co-sm-12 hidden"></div>
                    </div>
                    <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark">Content</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="">
                                <textboxio:Textboxio runat="server" ID="txtContent" ScriptSrc="/assets/textboxio/textboxio.js" Width="100%" Height="500px" />
                            </div>
                        </div>
                        <div class="col-md-4 co-sm-12 hidden"></div>
                    </div>
                    <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark">Footer</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="">
                                <textboxio:Textboxio runat="server" ID="txtFooter" ScriptSrc="/assets/textboxio/textboxio.js" Width="100%" Height="200px" />
                            </div>
                        </div>
                        <div class="col-md-4 co-sm-12 hidden"></div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4" style="margin-bottom: 10px;">
                            <div class="form-group" id="divAlertMsg" runat="server">
                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                    <span>
                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                    </span>
                                    <p id="pAlertMsg" runat="server">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                         </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
            </Triggers>
        </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        
        <div class="modal fade M_set" id="modalEmpSummary" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="m-0 text-dark">Keywords</h1>
                        <div class="add_new">
                            <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Keyword</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                    <tr>
                                        <td>Employe Code</td>
                                        <td>##EMPLOYECODE##</td>
                                    </tr>
                                    <tr>
                                        <td>CNIC</td>
                                        <td>##CNIC##</td>
                                    </tr>
                                    <tr>
                                        <td>Date of Birth</td>
                                        <td>##DOB##</td>
                                    </tr>
                                    <tr>
                                        <td>First Name</td>
                                        <td>##FIRSTNAME##</td>
                                    </tr>
                                    <tr>
                                        <td>Middle Name</td>
                                        <td>##MIDDLENAME##</td>
                                    </tr>
                                    <tr>
                                        <td>Last Name</td>
                                        <td>##LASTNAME##</td>
                                    </tr>
                                    <tr>
                                        <td>Father Name</td>
                                        <td>##FATHERNAME##</td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td>##GENDER##</td>
                                    </tr>
                                    <tr>
                                        <td>Contact No</td>
                                        <td>##CONTACTNO##</td>
                                    </tr>
                                    <tr>
                                        <td>Blood Group</td>
                                        <td>##BLOODGROUP##</td>
                                    </tr>
                                    <tr>
                                        <td>Email address</td>
                                        <td>##EMAIL##</td>
                                    </tr>
                                    <tr>
                                        <td>Secondary Email address</td>
                                        <td>##SECEMAIL##</td>
                                    </tr>
                                    <tr>
                                        <td>Country</td>
                                        <td>##COUNTRY##</td>
                                    </tr>
                                    <tr>
                                        <td>State</td>
                                        <td>##STATE##</td>
                                    </tr>
                                    <tr>
                                        <td>City</td>
                                        <td>##CITY##</td>
                                    </tr>
                                <tr>
                                        <td>Address</td>
                                        <td>##ADDRESS##</td>
                                    </tr>
                                    <tr>
                                        <td>Zip code</td>
                                        <td>##ZIPCODE##</td>
                                    </tr>
                                     <tr>
                                        <td>Employment Type</td>
                                        <td>##EMPLOYMENTTYPE##</td>
                                    </tr>
                                     <tr>
                                        <td>Date of Joining</td>
                                        <td>##DOJ##</td>
                                    </tr>
                                    <%-- <tr>
                                        <td>Job Description</td>
                                        <td>##JOBDESCRIPTION##</td>
                                    </tr>
                                     <tr>
                                        <td>Grade</td>
                                        <td>##GRADE##</td>
                                    </tr>
                                     <tr>
                                        <td>Department</td>
                                        <td>##DEPARTMRNT##</td>
                                    </tr>
                                      <tr>
                                        <td>Designation</td>
                                        <td>##DESIGNATION##</td>
                                    </tr>--%>
                                      <tr>
                                        <td>Direct Reporting To</td>
                                        <td>##DIRECTREPORTTO##</td>
                                    </tr>
                                      <tr>
                                        <td>Indirect Reporting To</td>
                                        <td>##INDIRECTREPORTTO##</td>
                                    </tr>
                                         <tr>
                                        <td>Basic Salary</td>
                                        <td>##BASICSALARY##</td>
                                    </tr>
                          <%--               <tr>
                                        <td>Shift</td>
                                        <td>##SHIFT##</td>
                                    </tr>--%>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade M_set" id="modalOfferLetter" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="m-0 text-dark">Keywords</h1>
                        <div class="add_new">
                            <div class="add-new">
                                <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="table-notations">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Value</th>
                                            <th>Keyword</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>REFRENCE NO</td>
                                            <td>##REFNO## </td>
                                        </tr>
                                        <tr>
                                            <td>NAME</td>
                                            <td>##NAME##</td>
                                        </tr>
                                        <tr>
                                            <td>DATE</td>
                                            <td>##DATE##</td>
                                        </tr>
                                        <tr>
                                            <td>ADDRESS</td>
                                            <td>##ADDRESS##</td>
                                        </tr>
                                        <tr>
                                            <td>DESIGNATION</td>
                                            <td>##DESG##</td>
                                        </tr>
                                        <tr>
                                            <td>BASIC SALARY</td>
                                            <td>##BASICSALARY##</td>
                                        </tr>
                                        <tr>
                                            <td>ALLOWANCES</td>
                                            <td>##ALLOWANCES## </td>
                                        </tr>
                                        <tr>
                                            <td>GROSS SALARY</td>
                                            <td>##GROSSSALARY##</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade M_set" id="modalWarningLetter" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="m-0 text-dark">Keywords</h1>
                        <div class="add_new">
                            <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Keyword</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>REFRENCE NO</td>
                                        <td>##REFNO## </td>
                                    </tr>
                                    <tr>
                                        <td>NAME</td>
                                        <td>##NAME##</td>
                                    </tr>
                                    <tr>
                                        <td>DATE</td>
                                        <td>##DATE##</td>
                                    </tr>
                                     <tr>
                                        <td>DEPARTMENT</td>
                                        <td>##DEPT##</td>
                                    </tr>
                                   
                                    <tr>
                                        <td>DESIGNATION</td>
                                        <td>##DESG##</td>
                                    </tr>

                                   
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade M_set" id="modalPayroll" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="m-0 text-dark">Keywords</h1>
                        <div class="add_new">
                            <button type="button" class="AD_btn" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Notations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>REFRENCE NO</td>
                                        <td>##REFNO## </td>
                                    </tr>
                                    <tr>
                                        <td>MONTH</td>
                                        <td>##MONTH##</td>
                                    </tr>
                                    <tr>
                                        <td>YEAR</td>
                                        <td>##YEAR##</td>
                                    </tr>
                                    <tr>
                                        <td>DATE</td>
                                        <td>##DATE##</td>
                                    </tr>
                                     <tr>
                                        <td>SHEET</td>
                                        <td>##TABLE##</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade M_set" id="modalAttendanceSheet" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="m-0 text-dark">Keywords</h1>
                        <div class="add_new">
                            <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Notations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>REFRENCE NO</td>
                                        <td>##REFNO## </td>
                                    </tr>
                                    <tr>
                                        <td>MONTH</td>
                                        <td>##MONTH##</td>
                                    </tr>
                                    <tr>
                                        <td>YEAR</td>
                                        <td>##YEAR##</td>
                                    </tr>
                                    <tr>
                                        <td>SHIFT</td>
                                        <td>##SHIFT##</td>
                                    </tr>
                                    <tr>
                                        <td>DATE</td>
                                        <td>##DATE##</td>
                                    </tr>
                                     <tr>
                                        <td>SHEET</td>
                                        <td>##TABLE##</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade M_set" id="modalPromotionLetter" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="m-0 text-dark">Keywords</h1>
                        <div class="add_new">
                            <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Notations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>REFRENCE NO</td>
                                        <td>##REFNO## </td>
                                    </tr>
                                    <tr>
                                        <td>NAME</td>
                                        <td>##NAME##</td>
                                    </tr>
                                    <tr>
                                        <td>DATE</td>
                                        <td>##DATE##</td>
                                    </tr>
                                    <tr>
                                        <td>JOB DESCRIPTION</td>
                                        <td>##JD##</td>
                                    </tr>
                                    <tr>
                                        <td>GRADE</td>
                                        <td>##GRADE##</td>
                                    </tr>
                                    <tr>
                                        <td>DEPARTMENT</td>
                                        <td>##DEPT##</td>
                                    </tr>
                                    <tr>
                                        <td>DESIGNATION</td>
                                        <td>##DESG## </td>
                                    </tr>
                                    <tr>
                                        <td>DEPARTMENT OF DIRECT REPORTING TO </td>
                                        <td>##DIRECTREPORTINTODEPT##</td>
                                    </tr>
                                     <tr>
                                        <td>DIRECT REPORTING TO </td>
                                        <td>##DIRECTREPORTINTODESG##</td>
                                    </tr>
                                    <tr>
                                        <td>DEPARTMENT OF INDIRECT REPORTING TO </td>
                                        <td>##INDIRECTREPORTINTODEPT##</td>
                                    </tr>
                                     <tr>
                                        <td>INDIRECT REPORTING TO </td>
                                        <td>##INDIRECTREPORTINTODESG##</td>
                                    </tr>
                                    <tr>
                                        <td>BASIC SALARY</td>
                                        <td>##BASICSALARY##</td>
                                    </tr>
                                    <tr>
                                        <td>ALLOWANCES</td>
                                        <td>##ALLOWANCES## </td>
                                    </tr>
                                     <tr>
                                        <td>DEDUCTIONS</td>
                                        <td>##DEDUCTIONS## </td>
                                    </tr>
                                    <tr>
                                        <td>GROSS SALARY</td>
                                        <td>##GROSSSALARY##</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade M_set" id="modalHRTemplate" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="m-0 text-dark">Keywords</h1>
                        <div class="add_new">
                            <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Notations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <tr>
                                        <td>Title</td>
                                        <td>##Title##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>Table</td>
                                        <td>##TABLE##</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>    
        <div class="modal fade M_set in" id="modalPaySlip" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="m-0 text-dark">Keywords</h1>
                        <div class="add_new">
                            <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="table-notations">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>Notations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <tr>
                                        <td>COMPANY NAME</td>
                                        <td>##COMPANYNAME##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>MONTH</td>
                                        <td>##MONTH##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>YEAR</td>
                                        <td>##YEAR##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>EMPLOYEE NAME</td>
                                        <td>##EMPLOYEENAME##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>DESIGNATION</td>
                                        <td>##DESIGNATION##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>DEPARTMENT</td>
                                        <td>##DEPARTMENT##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>BASIC PAY</td>
                                        <td>##BASICPAY##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>ALLOWNCE</td>
                                        <td>##ALLOWNCE##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>P.F.</td>
                                        <td>##PF##</td>
                                    </tr>
                        
                         
                        
                                    <tr>
                                        <td>INCOME TAX</td>
                                        <td>##INCOMETAX##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>OTHERS</td>
                                        <td>##OTHERS##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>TOTAL</td>
                                        <td>##TOTAL##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>DEDUCTIONS</td>
                                        <td>##DEDUCTIONS##</td>
                                    </tr>
                        
                                    <tr>
                                        <td>NET PAY</td>
                                        <td>##NETPAY##</td>
                                    </tr>
                        
                           
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        
        <!--========== END app main -->


        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .month-table-show {
                display: none;
                margin-top: 40px;
            }

            .modal-dialog {
                width: 450px;
                margin: 30px auto;
            }

            section.app-content {
                padding-bottom: 53px;
            }

            .modal-footer {
                border-top: 0px solid #e5e5e5;
            }

            .hiring-create-new-btn {
                margin-top: 17px;
                border: none;
                background: none;
                color: #01769a;
            }

            .user-img-div img {
                width: 169px !important;
                display: block;
                margin: 0 auto;
            }

            .user-img-div {
                width: auto;
            }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }
            .modal-content {
                top: 50px;
                width: 450px;
                position: fixed;
                z-index: 1050;
                left: 20%;
                border-radius: 5%;
                height: 100%;
                box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
        </style>
    </form>
</body>
</html>

