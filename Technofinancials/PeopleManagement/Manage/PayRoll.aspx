﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayRoll.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.PayRoll" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/Payroll-B.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Payrolls</h3>
                        </div>


                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>


                                <div class="col-sm-4">
                                    <div class="pull-right flex">
                                        <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" ><i class="fa fa-sticky-note-o"></i></button>
                                        <button class="tf-pdf-btn"  id="btnPDF" runat="server" onserverclick="btnPDF_ServerClick"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                                        <button class="tf-save-btn"  id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <button class="tf-save-btn"  id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                        <button class="tf-save-btn"  id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class"  CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                        <button class="tf-save-btn"  id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <button class="tf-save-btn"  id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                        <button class="tf-save-btn"  id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>


                                        <button class="tf-save-btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn"  id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Year</h4>
                                        <asp:DropDownList ID="ddlYear" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--Select Year--</asp:ListItem>
                                            <asp:ListItem Value="2019">2019</asp:ListItem>
                                            <asp:ListItem Value="2018">2018</asp:ListItem>
                                            <asp:ListItem Value="2017">2017</asp:ListItem>
                                            <asp:ListItem Value="2016">2016</asp:ListItem>
                                            <asp:ListItem Value="2015">2015</asp:ListItem>
                                            <asp:ListItem Value="2014">2014</asp:ListItem>
                                            <asp:ListItem Value="2013">2013</asp:ListItem>
                                            <asp:ListItem Value="2012">2012</asp:ListItem>
                                            <asp:ListItem Value="2011">2011</asp:ListItem>
                                            <asp:ListItem Value="2010">2010</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Month</h4>
                                        <asp:DropDownList ID="ddlMonth" runat="server" data-plugin="select2" CssClass="select2 form-control js-example-basic-single" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--Select Month--</asp:ListItem>
                                            <asp:ListItem Value="January">January</asp:ListItem>
                                            <asp:ListItem Value="February">February</asp:ListItem>
                                            <asp:ListItem Value="March">March</asp:ListItem>
                                            <asp:ListItem Value="April">April</asp:ListItem>
                                            <asp:ListItem Value="May">May</asp:ListItem>
                                            <asp:ListItem Value="June">June</asp:ListItem>
                                            <asp:ListItem Value="July">July</asp:ListItem>
                                            <asp:ListItem Value="August">August</asp:ListItem>
                                            <asp:ListItem Value="September">September</asp:ListItem>
                                            <asp:ListItem Value="October">October</asp:ListItem>
                                            <asp:ListItem Value="November">November</asp:ListItem>
                                            <asp:ListItem Value="December">December</asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:HiddenField ID="hdnRowNo" runat="server" />
                            <asp:GridView ID="gvSalaries" runat="server" CssClass="table table-bordered" ClientIDMode="Static" ShowFooter="true" AutoGenerateColumns="false" OnRowDataBound="gvSalaries_RowDataBound" OnRowUpdating="gvSalaries_RowUpdating" OnRowCancelingEdit="gvSalaries_RowCancelingEdit" OnRowEditing="gvSalaries_RowEditing">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeCode" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblDeptName" Text='<%# Eval("Department") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblDesgTitle" Text='<%# Eval("Designation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bank Details">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBankDetails" Text='<%# Eval("BankDetails") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Basic Salary <span style='color:red !important;'>*</span>">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBasicSalary" Text='<%# Eval("BasicSalary") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator73" ControlToValidate="txtEditBasicSalary" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />

                                            <asp:TextBox runat="server" TextMode="Number" ID="txtEditBasicSalary" CssClass="form-control" Text='<%# Eval("BasicSalary") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="txtTotalBasicSalaries" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gross Salary (Basic Salary+Home Allowance+Utility)">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblGrossSallary" Text='<%# Eval("GrossAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                        <%-- <EditItemTemplate>
                                                                <span style="color:red !important;">*</span> <span style="color:red !important;">*</span> <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator74" ControlToValidate="txtEditAllowances" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />

                                            <asp:TextBox runat="server" ID="txtEditAllowances" TextMode="Number"  CssClass="form-control" Text='<%# Eval("Allowances") %>'></asp:TextBox>
                                        </EditItemTemplate>--%>
                                        <FooterTemplate>
                                            <asp:Label ID="txtTotalGrossSallay" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Other Allowances    <span style='color:red !important;'>*</span>">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAllowances" Text='<%# Eval("Allowances") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator74" ControlToValidate="txtEditAllowances" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />

                                            <asp:TextBox runat="server" ID="txtEditAllowances" TextMode="Number" CssClass="form-control" Text='<%# Eval("Allowances") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="txtTotalAllowances" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    
                                    <asp:TemplateField HeaderText="Deductions">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblDeductions" Text='<%# Eval("Deductions") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" ID="txtEditDeductions" TextMode="Number" CssClass="form-control" Text='<%# Eval("Deductions") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="txtTotalDeductions" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Provident Fund    <span style='color:red !important;'>*</span>">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblProvidentFund" Text='<%# Eval("PFEmployee") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                           <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator75" ControlToValidate="txtEditProvidentFund" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />

                                            <asp:TextBox runat="server" ID="txtEditProvidentFund" TextMode="Number" CssClass="form-control" Text='<%# Eval("PFEmployee") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="txtTotalProvidentFund" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Taxes   <span style='color:red !important;'>*</span>">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTaxes" Text='<%# Eval("Taxes") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                           <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator76" ControlToValidate="txtEditTaxes" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />

                                            <asp:TextBox runat="server" ID="txtEditTaxes" TextMode="Number" CssClass="form-control" Text='<%# Eval("Taxes") %>' OnTextChanged="txtEditTaxes_TextChanged"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="txtTotalTaxes" CssClass="totalSalaries" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Net Salary  <span style='color:red !important;'>*</span>">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblNetSalary" Text='<%# Eval("NetSalary") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                           <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator77" ControlToValidate="txtEditNetSalary" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />

                                            <asp:TextBox runat="server" ID="txtEditNetSalary" TextMode="Number" CssClass="form-control" Text='<%# Eval("NetSalary") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="txtTotalNetSalaries" CssClass="totalSalaries" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="false">
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" ValidationGroup="btnValidate" Text="Update"></asp:LinkButton>
                                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
               
                </section>


                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }
            .AD_btn_inn{
                padding: 4px 24px;
                margin-top: 10px;
            }
        </style>
    </form>
</body>
</html>

