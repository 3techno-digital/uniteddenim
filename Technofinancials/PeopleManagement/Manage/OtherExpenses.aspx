﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OtherExpenses.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.OtherExpenses" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
            .AD_btn_inn{
                padding:4px 24px;
            }
            input.form-control.input-sm {
                display: none;
            }

            .dataTables_filter {
                margin-bottom: 5px;
                display: none;
            }

            .dt-buttons {
                display: none;
            }

            .amount {
                text-align: center;
                padding-top: 50px;
                color: #003780;
            }

                .amount h5 {
                    font-size: 18px;
                }

                .amount h5 {
                    font-family: Noto-Regular !important;
                }

                .amount span {
                    font-family: Noto-SemiBold !important;
                    display: block;
                    font-size: 30px;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
        </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h1 class="m-0 text-dark">Other Expenses Approval</h1>
                            </div>
                            <!-- /.col -->

                            <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                <ContentTemplate>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div style="text-align: right;">
                                            <button class="AD_btn" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                            <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                            <button class="AD_btn" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                            <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                            <button class="AD_btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button><%--Disapprove--%>
                                            <button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                            <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button><%--Send--%>
                                            <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                            <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>



                <section class="app-content">


                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>


                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <label>Code <span style="color: red !important;">*</span> </label>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtCode" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" disabled="disabled" placeholder="001" id="txtCode" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <label>Data Entry Date <span style="color: red !important;">*</span> </label>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtDate" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="29-May-2020" id="txtDate" disabled="disabled" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group input-group-lg">
                                                <label>
                                                    Title <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="txtTitle" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                <input type="text" class="form-control" id="txtTitle" runat="server" placeholder="Title" />
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group input-group-lg">
                                                <label>
                                                    Employee <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="ddlEmployee" InitialValue="0" ErrorMessage="" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></label>
                                                <asp:DropDownList runat="server" CssClass="form-control select2" ID="ddlEmployee" data-plugin="select2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h4>Notes</h4>
                                                <textarea style="max-width: 430px !important;min-height: 107px !important;" class="form-control" rows="3" name="txtNotes" id="txtNotes" runat="server"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="amount">
                                                <h5>Amount PKR<span id="lblAmount" runat="server">100,000</span></h5>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                            <span>
                                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                            </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                                    </div>
                                </div>

                            </div>



                            <div id="divPay" runat="server">
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="content-header second_heading">
                                <div class="container-fluid">
                                    <div class="row mb-2">
                                        <div class="col-sm-6">
                                            <h1 class="m-0 text-dark">Expense Details</h1>
                                        </div>
                                        <!-- /.col -->

                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.container-fluid -->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="add-table-div">
                                        <div class="card-body table-responsive tbl-cont ">

                                            <asp:GridView ID="gvExpDetails" runat="server" CssClass="table table-bordered no-footer" ClientIDMode="Static" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S.No">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Exp.Type">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblExpType" Text='<%# Eval("ExpType") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Amount">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblAmount" Text='<%# Eval("Amount") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Description") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Attachment">
                                                        <ItemTemplate>
                                                            <a runat="server" target="_blank" class="AD_stock_inn"  visible='<%#(Eval("FilePath")) == "" ? false: true %>'  id="lblDownload" href='<%# Eval("FilePath") %>'> Download <i class="fa fa-download" aria-hidden="true"></i></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Edit" OnCommand="lnkEdit_Command"></asp:LinkButton>
                                                            <asp:LinkButton CssClass="delete-class" ID="lnkRemove" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveFile_Command"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group input-group-lg">
                                                <label>
                                                    Expense Type
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlExpType" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnAddExpDetailsValidate" ForeColor="Red" /></label>
                                                <asp:DropDownList runat="server" CssClass="form-control select2" ID="ddlExpType" data-plugin="select2">
                                                    <asp:ListItem Value="0"> -- select --</asp:ListItem>
                                                    <asp:ListItem Value="Airfare">Airfare</asp:ListItem>
                                                    <asp:ListItem Value="Lodging">Lodging</asp:ListItem>
                                                    <asp:ListItem Value="Ground Transportation">Ground Transportation</asp:ListItem>
                                                    <asp:ListItem Value="Meals & Tips">Meals & Tips</asp:ListItem>
                                                    <asp:ListItem Value="Conferences and Seminars">Conferences and Seminars</asp:ListItem>
                                                    <asp:ListItem Value="Miles">Miles</asp:ListItem>
                                                    <asp:ListItem Value="Mileage Reimbursement<">Mileage Reimbursement</asp:ListItem>
                                                    <asp:ListItem Value="Other">Other</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group input-group-lg">
                                                <label>Amount<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtAmountDetail" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnAddExpDetailsValidate" ForeColor="Red" /></label>
                                                <input type="number" class="form-control" id="txtAmountDetail" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h4>Attachments</h4>
                                            </div>
                                            <div class="file-upload">
                                                <div class="image-upload-wrap">
                                                    <%--<asp:FileUpload ID="updLogo" onchange="readURL(this);" runat="server" ClientIDMode="Static" accept=".png,.jpg,.jpeg,.gif" />--%>
                                                    <input class="file-upload-input" runat="server" type="file" name="uploadFile" id="updLogo" onchange="readURL(this);" accept="image/*" />
                                                    <div class="drag-text">
                                                        <h3>Drag and drop a file or select add Image (MaxSize: 2mb)</h3>
                                                    </div>
                                                </div>
                                                <div class="file-upload-content">
                                                    <img class="file-upload-image" src="/assets/images/3techno-Logo.png" alt="" runat="server" id="imgLogo" />
                                                    <div class="image-title-wrap">
                                                        <button type="button" onclick="removeUpload()" class="remove-image">
                                                            Remove  <span><i class="fa fa-trash" aria-hidden="true"></i></span>

                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                                                                 <div class="col-md-12">
                                            <div class="form-group">
                                                <label>
                                                    Description
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtDescriptionDetail" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnAddExpDetailsValidate" ForeColor="Red" /></label>
                                                <input type="text" class="form-control" id="txtDescriptionDetail" runat="server" />
                                            </div>
                                        </div>

                                        </div>
                                    </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <div runat="server" id="btnAddDiv">
                                                <button type="button" style="margin-top: 2.4rem;text-align: center;" class="AD_btn_inn stock_add" runat="server" onserverclick="btnAddExpDetails_ServerClick" validationgroup="btnAddExpDetailsValidate" id="btnAddExpDetails">Add</button>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <div runat="server" id="btnUpdateDiv" style="text-align: right;">
                                                <asp:HiddenField runat="server" ID="hdnExpenseSrNO" Value="0" />
                                                <button type="button" style="margin-top: 2.4rem;" class="AD_btn_inn stock_add" runat="server" onserverclick="btnUpdateExpDetails_ServerClick" validationgroup="btnAddExpDetailsValidate" id="btnUpdateExpDetails">Update</button>
                                                <button type="button" style="margin-top: 2.4rem;" class="AD_btn_inn stock_add" runat="server" onserverclick="btnCancelExpDetails_ServerClick1" id="btnCancelExpDetails">Cancel</button>
                                            </div>

                                        </div>
                                    </div>
                                    </div>
                                </div>
                            <div class="row">
                            </div>
                        </ContentTemplate>
                           <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAddExpDetails" />
                                    <asp:PostBackTrigger ControlID="btnUpdateExpDetails" />
                                </Triggers>
                    </asp:UpdatePanel>
                    <div class="cleafix">&nbsp;</div>
                    <div class="cleafix">&nbsp;</div>
                    <div class="cleafix">&nbsp;</div>
                    <div class="cleafix">&nbsp;</div>
                     <br />
                    <br />
                    <br />
                    <br />
                </section>
            </div>
            <uc:Footer ID="footer1" runat="server" />
            <!-- .wrap -->
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var ext = input.files[0].name.split('.').pop().toLowerCase();
                    if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
                        removeUpload();
                        alert('invalid extension!');
                    }
                    else {
                        var reader = new FileReader();
                        var myimg = '';
                        reader.onload = function (e) {
                            $('.image-upload-wrap').hide();
                            $('.file-upload-image').attr('src', e.target.result);
                            $('.file-upload-content').show();
                            $('.image-title').html(input.files[0].name);
                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                else {
                    removeUpload();
                }
            }
            function removeUpload() {
                $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                $('.file-upload-content').hide();
                $('.image-upload-wrap').show();
            }
            $('.image-upload-wrap').bind('dragover', function () {
                $('.image-upload-wrap').addClass('image-dropping');
            });
            $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
            });
        </script>



<%--        <script>
            $("#updAttachments").change(function () {
                __doPostBack('attachments', '');
            });

            function readUrlMultiple2(input) {
                if (input.files && input.files[0]) {
                    var fp = $("#updAttachments");
                    var lg = fp[0].files.length; // get length

                    $('.image-title').html(lg + 'Files Uploaded');
                    if (lg > 0) {
                        $('.image-upload-wrap').hide();
                        $('.file-upload-content').show();
                    }

                } else {
                    removeUploadMultiple2();
                }
            }

            function removeUploadMultiple2() {
                $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                $('.file-upload-content').hide();
                $('.image-upload-wrap').show();
            }
            $('.image-upload-wrap').bind('dragover', function () {
                $('.image-upload-wrap').addClass('image-dropping');
            });
            $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
            });

        </script>--%>



        
        <!-- Modal -->
    </form>
</body>
</html>
