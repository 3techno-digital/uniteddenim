﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class JobChange : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        private float EmployeeBasicSalary
        {
            get
            {
                if (ViewState["EmployeeBasicSalary"] != null)
                {
                    return (float)ViewState["EmployeeBasicSalary"];
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["EmployeeBasicSalary"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employee-promotions";
                BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
                bindJDs();
                bindGrades();
                clearFields();
                divAlertMsg.Visible = false;

                BindEmployeesDropDown();

                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                if (HttpContext.Current.Items["PromotionID"] != null)
                {
                    getPrmotionDetailsByID(Convert.ToInt32(HttpContext.Current.Items["PromotionID"].ToString()));
                    CheckAccess();
                }
            }
        }

        private void CheckAccess()
        {
            btnSave.Visible = false;
            btnApprove.Visible = false;
            btnReview.Visible = false;
            btnRevApprove.Visible = false;
            lnkReject.Visible = false;
            lnkDelete.Visible = false;
            btnSubForReview.Visible = false;
            btnDisapprove.Visible = false;
            btnRejDisApprove.Visible = false;


            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.TableName = "Promotions";
            objDB.PrimaryColumnnName = "PromotionID";
            objDB.PrimaryColumnValue = HttpContext.Current.Items["PromotionID"].ToString();
            objDB.DocName = "Succession";



            string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
            if (chkAccessLevel == "Can Edit")
            {
                btnSave.Visible = true;
                lnkDelete.Visible = true;
                btnSubForReview.Visible = true;
            }
            if (chkAccessLevel == "Can Edit & Review")
            {
                btnSave.Visible = true;
                btnReview.Visible = true;
                lnkReject.Visible = true;

            }
            if (chkAccessLevel == "Can Edit & Approve")
            {
                btnSave.Visible = true;
                btnApprove.Visible = true;
                btnDisapprove.Visible = true;
            }
            if (chkAccessLevel == "Can Edit, Review & Approve")
            {
                btnSave.Visible = true;
                btnRevApprove.Visible = true;

                btnRejDisApprove.Visible = true;
            }
            if (chkAccessLevel == "View & Edit")
            {
                btnSave.Visible = true;
            }
        }

       
        private void getPrmotionDetailsByID(int prmotionID)
        {
            DataTable dt = new DataTable();
            objDB.PromotionID = prmotionID;
            dt = objDB.GetEmployeePromotionByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtBasicSalary.Value = dt.Rows[0]["BasicSalary"].ToString();
                    ddlEmployees.SelectedIndex = ddlEmployees.Items.IndexOf(ddlEmployees.Items.FindByValue(dt.Rows[0]["EmployeeID"].ToString()));
                    //ddlEmployees_SelectedIndexChanged(null, null);
                    ddlJD.SelectedIndex = ddlJD.Items.IndexOf(ddlJD.Items.FindByValue(dt.Rows[0]["NodeID"].ToString()));
                    ddlGrades.SelectedIndex = ddlGrades.Items.IndexOf(ddlGrades.Items.FindByValue(dt.Rows[0]["GradeID"].ToString()));
                    txtDate.Value = dt.Rows[0]["PromotionDate"].ToString();
                    //txtBasicSalary.Value = dt.Rows[0]["BasicSalary"].ToString();
                    ddlJD_SelectedIndexChanged(null, null);
                    txtNotes.Value = dt.Rows[0]["Notes"].ToString();

                    txtOverTimeRate.Value = dt.Rows[0]["OverTimeRate"].ToString();
                    txtEOBIAllowance.Value = dt.Rows[0]["EOBI"].ToString();
                    txtMaintenanceAllowance.Value = dt.Rows[0]["MaintenanceAllowance"].ToString();
                    txtFuelAllowance.Value = dt.Rows[0]["FuelAllowance"].ToString();
                    txtNetSalary.Value = dt.Rows[0]["NetSalary"].ToString();

                    ddlDirectReportToDept.SelectedValue = dt.Rows[0]["DirectReportingToDeptID"].ToString();
                    ddlDirectReportToDept_SelectedIndexChanged(null, null);
                    ddlDirectreportTo.SelectedValue = dt.Rows[0]["DirectReportingID"].ToString();

                    ddlInDirectReportToDept.SelectedValue = dt.Rows[0]["InDirectReportingToDeptID"].ToString();
                    ddlInDirectReportToDept_SelectedIndexChanged(null, null);                                      
                    ddlInDirectreportTo.SelectedValue = dt.Rows[0]["InDirectReportingID"].ToString();


                    objDB.DocID = prmotionID;
                    objDB.DocType = "Succession";
                    ltrNotesTable.Text = objDB.GetDocNotes();
                }
            }
            Common.addlog("View", "HR", "Employee Promotion of\"" + ddlEmployees.SelectedItem.Text + "\" Viewed", "Promotions", objDB.PromotionID);

        }

        protected void bindJDs()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlJD.DataSource = objDB.GetAllNodesByCompanyID(ref errorMsg);
            ddlJD.DataTextField = "NodeTitle";
            ddlJD.DataValueField = "NodeID";
            ddlJD.DataBind();
            ddlJD.Items.Insert(0, new ListItem("--- Select ---", "0"));
        }

        protected void bindGrades()
        {

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlGrades.DataSource = objDB.GetAllGradesByCompanyID(ref errorMsg);
            ddlGrades.DataTextField = "GradeName";
            ddlGrades.DataValueField = "GradeID";
            ddlGrades.DataBind();
            ddlGrades.Items.Insert(0, new ListItem("--- Select ---", "0"));
        }

        private void BindEmployeesDropDown()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            ddlEmployees.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
            ddlEmployees.DataTextField = "EmployeeName";
            ddlEmployees.DataValueField = "EmployeeID";
            ddlEmployees.DataBind();
            ddlEmployees.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
        }
        private void clearFields()
        {
            ddlEmployees.SelectedIndex = 0;
            ddlJD.SelectedIndex = 0;
            ddlGrades.SelectedIndex = 0;
            txtDate.Value = "";
            txtBasicSalary.Value = "";
            txtNetSalary.Value = "0";
            txtEOBIAllowance.Value = "0";
            txtFuelAllowance.Value = "0";
            txtMaintenanceAllowance.Value = "0";
            txtOverTimeRate.Value = "0";
            txtNotes.Value = "";
        }

        private void BindDepartmentDropDown(int companyID)
        {
            objDB.CompanyID = companyID;
            ddlDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDept.DataTextField = "DeptName";
            ddlDept.DataValueField = "DeptID";
            ddlDept.DataBind();
            ddlDept.Items.Insert(0, new ListItem("--- None ---", "0"));


            objDB.CompanyID = companyID;
            ddlDirectReportToDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDirectReportToDept.DataTextField = "DeptName";
            ddlDirectReportToDept.DataValueField = "DeptID";
            ddlDirectReportToDept.DataBind();
            ddlDirectReportToDept.Items.Insert(0, new ListItem("--- None ---", "0"));

            objDB.CompanyID = companyID;
            ddlInDirectReportToDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlInDirectReportToDept.DataTextField = "DeptName";
            ddlInDirectReportToDept.DataValueField = "DeptID";
            ddlInDirectReportToDept.DataBind();
            ddlInDirectReportToDept.Items.Insert(0, new ListItem("--- None ---", "0"));
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            bool isNew = false;
            bool isEffective = false;

            CheckSessions();
            int promotionID = 0;
            objDB.EmployeeID = Convert.ToInt32(ddlEmployees.SelectedItem.Value);
            objDB.PromotionDate = txtDate.Value;
            objDB.Notes = txtNotes.Value;
            objDB.DesgID = Convert.ToInt32(ddlDesg.SelectedItem.Value);
            objDB.GradeID = Convert.ToInt32(ddlGrades.SelectedItem.Value);
            objDB.DirectReportingID = Convert.ToInt32(ddlDirectreportTo.SelectedItem.Value);

            if (ddlInDirectreportTo.SelectedItem != null)
                objDB.InDirectReportingID = Convert.ToInt32(ddlInDirectreportTo.SelectedItem.Value);

            objDB.BasicSalary = float.Parse(txtBasicSalary.Value);
            objDB.MaintenanceAllowance = float.Parse(txtMaintenanceAllowance.Value);
            objDB.FuelAllowance = float.Parse(txtFuelAllowance.Value);
            objDB.OverTimeRate = float.Parse(txtOverTimeRate.Value);
            objDB.EOBI = float.Parse(txtEOBIAllowance.Value);
            objDB.NetSalary = float.Parse(txtNetSalary.Value);

            objDB.NodeID = Convert.ToInt32(ddlJD.SelectedItem.Value);

            string res = "";
            if (HttpContext.Current.Items["PromotionID"] != null)
            {
                promotionID = Convert.ToInt32(HttpContext.Current.Items["PromotionID"].ToString());
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.PromotionID = Convert.ToInt32(HttpContext.Current.Items["PromotionID"].ToString());
                res = objDB.UpdateEmployeePromotion();
            }
            else
            {
                objDB.CreatedBy = Session["UserName"].ToString();
                promotionID = Convert.ToInt32(objDB.AddEmployeePromotion());
                res = "New Employee Promotion Added";
                isNew = true;
            }


            objDB.DocType = "Succession";
            objDB.DocID = promotionID;
            objDB.Notes = txtNotes2.Value;
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.AddDocNotes();


            if (res == "New Employee Promotion Added" || res == "Promotion Data Updated")
            {
                if (res == "New Employee Promotion Added") {
                    clearFields();
                    Common.addlog("Add", "HR", "New Employee Promotion of Employee\"" + ddlEmployees.SelectedItem.Text + "\" Added", "Promotions");
                }
                if (res == "Promotion Data Updated") { Common.addlog("Update", "HR", "Employee Promotion of Employee\"" + ddlEmployees.SelectedItem.Text + "\" Updated", "Promotions", objDB.PromotionID); }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }

        protected void BindDesginationsDropDown(int DeptID)
        {
            objDB.DeptID = DeptID;
            ddlDesg.DataSource = objDB.GetAllDesignationByDepartmentID(ref errorMsg);
            ddlDesg.DataTextField = "DesgTitle";
            ddlDesg.DataValueField = "DesgID";
            ddlDesg.DataBind();
            ddlDesg.Items.Insert(0, new ListItem("--- Select Designation ---", "0"));

        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDesg.Items.Clear();
            if (ddlDept.SelectedIndex != 0)
            {
                bindDesignationsDropDown(Convert.ToInt32(ddlDept.SelectedItem.Value));
            }
        }
        protected void bindDesignationsDropDown(int DeptID)
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlDesg.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
            ddlDesg.DataTextField = "DesgTitle";
            ddlDesg.DataValueField = "DesgID";
            ddlDesg.DataBind();
            ddlDesg.Items.Insert(0, new ListItem("--- Select ---", "0"));
        }
        protected void ddlDirectReportToDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDirectreportTo.Items.Clear();
            if (ddlDirectReportToDept.SelectedIndex != 0)
            {
                bindDirectReportingDropDown(Convert.ToInt32(ddlDirectReportToDept.SelectedItem.Value));
            }
        }
        protected void bindDirectReportingDropDown(int DeptID)
        {
            objDB.DeptID = DeptID;
            ddlDirectreportTo.DataSource = objDB.GetAllDesignationByDepartmentID(ref errorMsg);
            ddlDirectreportTo.DataTextField = "DesgTitle";
            ddlDirectreportTo.DataValueField = "DesgID";
            ddlDirectreportTo.DataBind();
            ddlDirectreportTo.Items.Insert(0, new ListItem("--- None ---", "0"));
        }
        protected void ddlInDirectReportToDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlInDirectreportTo.Items.Clear();
            if (ddlInDirectReportToDept.SelectedIndex != 0)
            {
                bindInDirectReportingDropDown(Convert.ToInt32(ddlInDirectReportToDept.SelectedItem.Value));
            }
        }
        protected void bindInDirectReportingDropDown(int DeptID)
        {
            objDB.DeptID = DeptID;
            ddlInDirectreportTo.DataSource = objDB.GetAllDesignationByDepartmentID(ref errorMsg);
            ddlInDirectreportTo.DataTextField = "DesgTitle";
            ddlInDirectreportTo.DataValueField = "DesgID";
            ddlInDirectreportTo.DataBind();
            ddlInDirectreportTo.Items.Insert(0, new ListItem("--- None ---", "0"));
        }

        protected void ddlEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSessions();
            if (ddlEmployees.SelectedIndex != 0)
            {
                getEmployeeByID(Convert.ToInt32(ddlEmployees.SelectedItem.Value));
            }
            else
            {
                clearFields();
            }
        }

        private void getEmployeeByID(int EmpID)
        {
            objDB.EmployeeID = EmpID;
            DataTable dt = objDB.GetEmployeeByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ddlGrades.SelectedValue = dt.Rows[0]["GradeID"].ToString();
                    ddlDept.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                    ddlDept_SelectedIndexChanged(null, null);
                    ddlDesg.SelectedValue = dt.Rows[0]["DesgID"].ToString();
                    ddlJD.SelectedValue = dt.Rows[0]["NodeID"].ToString();
                    txtBasicSalary.Value = dt.Rows[0]["BasicSalary"].ToString();

                    ddlDirectReportToDept.SelectedIndex = ddlDirectReportToDept.Items.IndexOf(ddlDirectReportToDept.Items.FindByValue(dt.Rows[0]["DirectReportingToDeptID"].ToString()));
                    ddlDirectReportToDept_SelectedIndexChanged(null, null);
                    ddlInDirectReportToDept.SelectedIndex = ddlInDirectReportToDept.Items.IndexOf(ddlInDirectReportToDept.Items.FindByValue(dt.Rows[0]["InDirectReportingToDeptID"].ToString()));
                    ddlInDirectReportToDept_SelectedIndexChanged(null, null);

                    ddlDirectreportTo.SelectedIndex = ddlDirectreportTo.Items.IndexOf(ddlDirectreportTo.Items.FindByValue(dt.Rows[0]["DirectReportingTo"].ToString()));
                    ddlInDirectreportTo.SelectedIndex = ddlInDirectreportTo.Items.IndexOf(ddlInDirectreportTo.Items.FindByValue(dt.Rows[0]["InDirectReportingTo"].ToString()));
                    
                }
            }
        }
       

        protected void ddlJD_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (ddlJD.SelectedIndex != 0)
            {

                objDB.NodeID = Convert.ToInt32(ddlJD.SelectedValue);
                DataTable dtJD = objDB.GetNodeByNodeID(ref errorMsg);
                if (dtJD != null && dtJD.Rows.Count > 0)
                {
                    ddlGrades.SelectedValue = dtJD.Rows[0]["Grade"].ToString();

                    bindDesignationsDropDown(0);

                    ddlDesg.SelectedValue = dtJD.Rows[0]["DesgID"].ToString();
                    objDB.DesgID = Convert.ToInt32(ddlDesg.SelectedValue);
                    DataTable dtdesg = objDB.GetDesignationByID(ref errorMsg);
                    if (dtdesg != null && dtdesg.Rows.Count > 0)
                    {
                        ddlDept.SelectedValue = dtdesg.Rows[0]["DeptID"].ToString();
                    }

                }
                
                DataTable dt = new DataTable();
                objDB.NodeID = Convert.ToInt32(ddlJD.SelectedItem.Value);
                dt = objDB.GetNodeByNodeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (HttpContext.Current.Items["PromotionID"] == null)
                        {
                            txtBasicSalary.Value = dt.Rows[0]["BasicSalary"].ToString();
                        }
                    }
                }
            }
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "Promotions", "PromotionID", HttpContext.Current.Items["PromotionID"].ToString(), Session["UserName"].ToString());
            Common.addlogNew(res, "HR", "Promotion of ID\"" + HttpContext.Current.Items["PromotionID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/promotions", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/promotions/edit-promotion-" + HttpContext.Current.Items["PromotionID"].ToString(), "Promotion of Employee \"" + ddlEmployees.SelectedItem.Text + "\"", "Promotions", "Succession", Convert.ToInt32(HttpContext.Current.Items["PromotionID"].ToString()));

            if (res == "Reviewed & Approved Sucessfull")
            {
                objDB.PromotionDate = txtDate.Value;

                if (Convert.ToInt32(DateTime.Parse(objDB.PromotionDate).ToString("yyyyMMdd")) < Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")))
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());

                    objDB.EmployeeID = Convert.ToInt32(ddlEmployees.SelectedItem.Value);
                    objDB.Notes = txtNotes.Value;
                    objDB.DesgID = Convert.ToInt32(ddlDesg.SelectedItem.Value);
                    objDB.GradeID = Convert.ToInt32(ddlGrades.SelectedItem.Value);
                    objDB.DirectReportingID = Convert.ToInt32(ddlDirectreportTo.SelectedItem.Value);

                    if (ddlInDirectreportTo.SelectedItem != null)
                        objDB.InDirectReportingID = Convert.ToInt32(ddlInDirectreportTo.SelectedItem.Value);

                    objDB.BasicSalary = float.Parse(txtBasicSalary.Value);
                    objDB.MaintenanceAllowance = float.Parse(txtMaintenanceAllowance.Value);
                    objDB.FuelAllowance = float.Parse(txtFuelAllowance.Value);
                    objDB.OverTimeRate = float.Parse(txtOverTimeRate.Value);
                    objDB.EOBI = float.Parse(txtEOBIAllowance.Value);
                    objDB.NetSalary = float.Parse(txtNetSalary.Value);

                    objDB.NodeID = Convert.ToInt32(ddlJD.SelectedItem.Value);
                    string rs = objDB.UpdateEmployeeSalary();
                    

                }
            }
            

            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            CheckAccess();

        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            string type = btn.CommandArgument;
            string res = Common.addAccessLevels(type, "Promotions", "PromotionID", HttpContext.Current.Items["PromotionID"].ToString(), Session["UserName"].ToString());
            Common.addlog(res, "HR", "Promotion of ID\"" + HttpContext.Current.Items["PromotionID"].ToString() + "\" Status Changed", "Promotions", Convert.ToInt32(HttpContext.Current.Items["PromotionID"].ToString()));
            divAlertMsg.Visible = true;
            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            pAlertMsg.InnerHtml = res;
            Response.Redirect(btnBack.HRef);

        }



    }

}