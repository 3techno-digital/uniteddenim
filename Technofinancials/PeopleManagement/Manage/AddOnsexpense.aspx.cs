﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class AddOnsexpense : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int AddOnsExpenseID
        {
            get
            {
                if (ViewState["AddOnsExpenseID"] != null)
                {
                    return (int)ViewState["AddOnsExpenseID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["AddOnsExpenseID"] = value;
            }
        }

        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return "Saved as Draft";
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected string AddonType
        {
            get
            {
                if (ViewState["AddonType"] != null)
                {
                    return (string)ViewState["AddonType"];
                }
                else
                {
                    return "Bonus";
                }
            }

            set
            {
                ViewState["AddonType"] = value;
            }
        }
        protected string Attachment
        {
            get
            {
                if (ViewState["Attachment"] != null)
                {
                    return (string)ViewState["Attachment"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["Attachment"] = value;
            }
        }

        protected double EmpGrossSalary
        {
            get
            {
                if (ViewState["EmpGrossSalary"] != null)
                {
                    return Convert.ToDouble(ViewState["EmpGrossSalary"].ToString());
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["EmpGrossSalary"] = value;
            }
        }

        protected double EmpBasicSalary
        {
            get
            {
                if (ViewState["EmpBasicSalary"] != null)
                {
                    return Convert.ToDouble(ViewState["EmpBasicSalary"].ToString());
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["EmpBasicSalary"] = value;
            }
        }

        protected int PayrollStartDate
        {
            get
            {
                if (ViewState["PayrollStartDate"] != null)
                {
                    return Convert.ToInt32(ViewState["PayrollStartDate"].ToString());
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["PayrollStartDate"] = value;
            }
        }

        protected int PayrollEndDate
        {
            get
            {
                if (ViewState["PayrollEndDate"] != null)
                {
                    return Convert.ToInt32(ViewState["PayrollEndDate"].ToString());
                }
                else
                {
                    return 30;
                }
            }

            set
            {
                ViewState["PayrollEndDate"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    CheckSessions();
                    DocStatus = "Saved as Draft";
                    ViewState["AddOnsExpenseID"] = null;
                    BindCurrencyDropdown();

                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    divHolidayHours.Visible = false;
                    divSimpleHours.Visible = false;
                    divDays.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/AddOnsExpense";
                    Attachment = "";
                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["AddOnsExpenseID"] != null)
                    {
                        AddOnsExpenseID = Convert.ToInt32(HttpContext.Current.Items["AddOnsExpenseID"].ToString());
                        getAddOnsExpenseByID(AddOnsExpenseID);
                        CheckAccess();
                    }
                    else
                    {
                        BindEmployeeDropdown();

                        ddlAddOn.Items.Clear();
                        ddlAddOn.DataSource = null;
                        ddlAddOn.DataBind();
                        ddlAddOn.Items.Insert(0, new ListItem("--- Select Employe First ---", "0"));
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "AddOns";
                objDB.PrimaryColumnnName = "AddOnsID";
                objDB.PrimaryColumnValue = AddOnsExpenseID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    //btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getAddOnsExpenseByID(int AddOnsExpenseID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.AddOnsExpenseID = AddOnsExpenseID;
                dt = objDB.GetAddOnsExpenseByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        DocStatus = dt.Rows[0]["DocStatus"].ToString();
                        BindEmployeeDropdown();
                        txtAmount.Text = dt.Rows[0]["Amount"].ToString();
                        txtNotes.Value = dt.Rows[0]["Note"].ToString();
                        txtCurrencyAmount.Text = Convert.ToDouble(dt.Rows[0]["NetAmount"]).ToString("N2");
                        txtRate.Text = dt.Rows[0]["CurrencyRate"].ToString();
                        ddlCurrency.SelectedValue = dt.Rows[0]["CurrencyID"].ToString();
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        BindAddOnsHeadDropdown();
                        ddlAddOn.SelectedValue = dt.Rows[0]["AddOnsHeadID"].ToString();
                        txtCurrencyAmount.Text = Convert.ToDouble(dt.Rows[0]["NetAmount"]).ToString("N2");
                        txtDays.Text = dt.Rows[0]["Days"].ToString();
                        txtSimpleHours.Text = dt.Rows[0]["SimpleHours"].ToString();
                        txtHolidayHours.Text = dt.Rows[0]["HolidayHours"].ToString();
                        txtMonth.Value = dt.Rows[0]["AddonMonth"].ToString();
                        EmpGrossSalary = Convert.ToDouble(dt.Rows[0]["GrossAmount"].ToString());
                        setAddonType(dt.Rows[0]["Types"].ToString());
                        Attachment = dt.Rows[0]["attachmentpath"].ToString();
                        //imgLogo.Src = Attachment;
                    }
                }
                //Common.addlog("View", "HR", "Currency \"" + txtTitle.Value + "\" Viewed", "Currency", objDB.AddOnsExpenseID);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        //protected void uploadFile()
        //{

        //    if (updLogo != null)
        //    {
        //        Random rand = new Random((int)DateTime.Now.Ticks);
        //        int randnum = 0;

        //        string fn = "";
        //        string exten = "";

        //        string destDir = Server.MapPath("~/assets/Attachments/AddOnExpenses/");
        //        randnum = rand.Next(1, 100000);
        //        fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

        //        if (!Directory.Exists(destDir))
        //        {
        //            Directory.CreateDirectory(destDir);
        //        }

        //        string fname = Path.GetFileName(updLogo.PostedFile.FileName);
        //        exten = Path.GetExtension(updLogo.PostedFile.FileName);
        //        updLogo.PostedFile.SaveAs(destDir + fn + exten);

        //        Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/Attachments/AddOnExpenses/" + fn + exten;
        //    }



        //    if (updLogo != null)
        //    {
        //        //foreach (HttpPostedFile file in updAttachments.PostedFile)
        //        //{

        //        HttpPostedFile file = updLogo.PostedFile;
        //        Random rand = new Random((int)DateTime.Now.Ticks);
        //        int randnum = 0;

        //        string fn = "";
        //        string exten = "";
        //        string destDir = Server.MapPath("~/assets/Attachments/AddOnExpenses/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
        //        randnum = rand.Next(1, 100000);
        //        fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

        //        if (!Directory.Exists(destDir))
        //        {
        //            Directory.CreateDirectory(destDir);
        //        }
        //        string fname = Path.GetFileName(file.FileName);
        //        exten = Path.GetExtension(file.FileName);
        //        file.SaveAs(destDir + fn + exten);

        //        Attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/Attachments/AddOnExpenses/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
        //    }



        //}

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                //uploadFile();
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Amount = txtAmount.Text;
                objDB.Notes = txtNotes.Value;
                objDB.CurrencyRate = Convert.ToDouble(txtRate.Text);
                objDB.AmountAfterCurrency = Convert.ToDouble(txtCurrencyAmount.Text);
                objDB.CurrencyID = Convert.ToInt32(ddlCurrency.SelectedValue);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.AddOnsID = Convert.ToInt32(ddlAddOn.SelectedValue);
                objDB.Days = Convert.ToInt32(txtDays.Text);
                objDB.AddonMonth = txtMonth.Value;
                objDB.OverTimeSimple = float.Parse(txtSimpleHours.Text);
                objDB.OverTimeSpecial = float.Parse(txtHolidayHours.Text);

                objDB.IsDeduction = false;

                if (HttpContext.Current.Items["AddOnsExpenseID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.AddOnsExpenseID = AddOnsExpenseID;
                    res = objDB.UpdateAddOnsExpenseNew(Attachment);
                    //res = "Add On Head Data Updated";
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddAddOnsExpenseNew(Attachment);
                    clearFields();
                    //res = "New Add On Head Added";
                }



                if (res == "New AddOns Added Successfully" || res == "AddOns  Data Updated")
                {
                    if (res == "New AddOns Added Successfully") { Common.addlog("Add", "HR", "New AddOns \"" + objDB.CurrencyName + "\" Added", "AddOns"); }
                    if (res == "AddOns  Data Updated") { Common.addlog("Update", "HR", "AddOns \"" + objDB.CurrencyName + "\" Updated", "AddOns", objDB.AddOnsExpenseID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields(string fromMethod = "none")
        {
            if (fromMethod == "Employee")
            {
                ddlCurrency.SelectedValue = "0";
                ddlAddOn.Items.Clear();
                ddlAddOn.DataSource = null;
                ddlAddOn.DataBind();
                ddlAddOn.Items.Insert(0, new ListItem("--- Select Employee First ---", "0"));
            }
            else if (fromMethod == "Addon")
            {
                ddlCurrency.SelectedValue = "0";
            }
            else
            {
                ddlCurrency.SelectedValue = "0";
                ddlEmployee.SelectedValue = "0";
                ddlAddOn.SelectedValue = "0";
            }

            divDays.Visible = false;
            divHolidayHours.Visible = false;
            divSimpleHours.Visible = false;
            txtDays.Text = "0";
            txtHolidayHours.Text = "0";
            txtSimpleHours.Text = "0";
            txtAmount.Text = "0";
            txtRate.Text = "1";
            txtCurrencyAmount.Text = "0";

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "AddOns", "AddOnsID", HttpContext.Current.Items["AddOnsExpenseID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "AddOn of ID\"" + HttpContext.Current.Items["AddOnsExpenseID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/AddOnsexpense", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/AddOnsExpense/edit-AddOnsExpense-" + HttpContext.Current.Items["AddOnsExpenseID"].ToString(), "AddOn \"" + AddOnsExpenseID + "\"", "AddOns", "Announcement", Convert.ToInt32(HttpContext.Current.Items["AddOnsExpenseID"].ToString()));

                //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                //{
                //    objDB.AddOnsExpenseID = AddOnsExpenseID;
                //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //    objDB.AddCurrencyToEmployeeAttendance();
                //}

                //Common.addlog("Delete", "HR", "Currency of ID \"" + objDB.AddOnsExpenseID + "\" deleted", "Currency", objDB.AddOnsExpenseID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "AddOns", "AddOnsExpenseID", HttpContext.Current.Items["AddOnsExpenseID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "AddOns of ID\"" + HttpContext.Current.Items["AddOnsExpenseID"].ToString() + "\" Status Changed", "AddOns", AddOnsExpenseID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.AddOnsExpenseID = AddOnsExpenseID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteAddOnsExpenseByID();
                Common.addlog("AddOns Rejected", "HR", "AddOns of ID\"" + HttpContext.Current.Items["AddOnsExpenseID"].ToString() + "\" AddOns Rejected", "AddOns", AddOnsExpenseID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "AddOns Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);

                if (DocStatus == "Approved" || DocStatus == "Disapproved" || DocStatus == "Rejected" || DocStatus == "Deleted")
                {
                    ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyID(ref errorMsg);
                    ddlEmployee.Enabled = false;
                }
                else
                {
                    ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                    ddlEmployee.Enabled = true;
                }

                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindCurrencyDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlCurrency.DataSource = objDB.GetAllCurrencyByCompanyID(ref errorMsg);
                ddlCurrency.DataTextField = "CurrencyName";
                ddlCurrency.DataValueField = "CurrencyID";
                ddlCurrency.DataBind();
                ddlCurrency.Items.Insert(0, new ListItem("--- Select Currency ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindAddOnsHeadDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                ddlAddOn.Items.Clear();
                if (DocStatus == "Approved" || DocStatus == "Rejected" || DocStatus == "Disapproved" || DocStatus == "Deleted")
                {
                    ddlAddOn.DataSource = objDB.GetAllApprovedAddOnsHeadByCompanyID(ref errorMsg);
                }
                else
                {
                    DataTable dtTemp = objDB.GetAllApprovedAddOnsHeadByCompanyIDandEmployeeID(ref errorMsg);
                    if (dtTemp != null && dtTemp.Rows.Count > 0)
                    {
                        EmpGrossSalary = Convert.ToDouble(dtTemp.Rows[0]["GrossSalary"].ToString());
                        EmpBasicSalary = Convert.ToDouble(dtTemp.Rows[0]["BasicSalary"].ToString());
                        PayrollStartDate = Convert.ToInt32(dtTemp.Rows[0]["StartDate"].ToString());
                        PayrollEndDate = Convert.ToInt32(dtTemp.Rows[0]["EndDate"].ToString());
                    }
                    else
                    {
                        EmpGrossSalary = 0;
                    }
                    ddlAddOn.DataSource = dtTemp;
                }
                ddlAddOn.DataTextField = "Tittle";
                ddlAddOn.DataValueField = "AddOnsID";
                ddlAddOn.DataBind();
                ddlAddOn.Items.Insert(0, new ListItem("--- Select Add Ons Head ---", "0"));


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {

            string errorMsg = "";
            if (ddlAddOn.SelectedValue != "0")
            {
                DataTable dt = new DataTable();
                objDB.CurrencyID = Convert.ToInt32(ddlCurrency.SelectedValue);
                dt = objDB.getCurrencyByID(ref errorMsg);
                if (dt != null && dt.Rows.Count > 0)
                {
                    txtRate.Text = dt.Rows[0]["Rate"].ToString();
                    txtCurrencyAmount.Text = (Convert.ToDouble(txtAmount.Text) * Convert.ToDouble(txtRate.Text)).ToString("N2");
                }
            }
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            txtCurrencyAmount.Text = (Convert.ToDouble(txtAmount.Text) * Convert.ToDouble(txtRate.Text)).ToString("N2");
        }

        protected void setAddonType(string Type)
        {
            AddonType = Type;
            ddlCurrency.Enabled = false;
            txtAmount.Enabled = false;

            if (AddonType == "Leave Encashment" || AddonType == "Attendance Adjustment" || AddonType == "Paid Leaves")
            {
                divDays.Visible = true;
            }
            else if (AddonType == "Overtime")
            {
                divHolidayHours.Visible = true;
                divSimpleHours.Visible = true;
            }
            else
            {
                ddlCurrency.Enabled = true;
                txtAmount.Enabled = true;
            }

        }

        protected void ddlAddOn_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                clearFields("Addon");
                DataTable dt = new DataTable();
                objDB.AddOnsID = Convert.ToInt32(ddlAddOn.SelectedValue);
                dt = objDB.GetAddOnsByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        setAddonType(dt.Rows[0]["Types"].ToString());
                        //else if (AddonType == "Graduity")
                        //{

                        //}

                        ddlCurrency.SelectedValue = dt.Rows[0]["CurrencyID"].ToString();
                        DataTable dt2 = new DataTable();
                        objDB.CurrencyID = Convert.ToInt32(dt.Rows[0]["CurrencyID"].ToString());
                        dt2 = objDB.getCurrencyByID(ref errorMsg);
                        if (dt2 != null && dt2.Rows.Count > 0)
                        {
                            txtRate.Text = dt2.Rows[0]["Rate"].ToString();
                            txtCurrencyAmount.Text = (Convert.ToDouble(txtAmount.Text) * Convert.ToDouble(txtRate.Text)).ToString("N2");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                clearFields("Employee");
                BindAddOnsHeadDropdown();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void txtSimpleHours_TextChanged(object sender, EventArgs e)
        {
            try
            {
                divAlertMsg.Visible = false;
                DateTime dtNow;

                if (txtMonth.Value != "" && DateTime.TryParse("01-" + txtMonth.Value, out dtNow))
                {
                    double days = DateTime.DaysInMonth(DateTime.Parse("01-" + txtMonth.Value).Year, DateTime.Parse("01-" + txtMonth.Value).Month);

                    if (AddonType == "Leave Encashment")
                    {
                        days = 30.0;
                    }

                    if (AddonType == "Leave Encashment" || AddonType == "Attendance Adjustment" || AddonType == "Paid Leaves")
                    {
                        txtAmount.Text = Math.Round(double.Parse(txtDays.Text) * (EmpGrossSalary / days), 2).ToString();
                    }
                    else if (AddonType == "Overtime")
                    {
                        divHolidayHours.Visible = true;
                        divSimpleHours.Visible = true;
                        double SalPerHour = (EmpGrossSalary / days) / 9.0;
                        txtAmount.Text = Math.Round((double.Parse(txtSimpleHours.Text) * SalPerHour * 1.5) + (double.Parse(txtHolidayHours.Text) * SalPerHour * 2), 2).ToString();
                    }
                    else
                    {
                        ddlCurrency.Enabled = true;
                        txtAmount.Enabled = true;
                        txtAmount.Enabled = true;
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Please Select Month Frist";
                    txtDays.Text = "0";
                    txtSimpleHours.Text = "0";
                    txtHolidayHours.Text = "0";
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}