﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class PolicyProcedure : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int PolicyProcedureID
        {
            get
            {
                if (ViewState["PolicyProcedureID"] != null)
                {
                    return (int)ViewState["PolicyProcedureID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PolicyProcedureID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {   
                    ViewState["PolicyProcedureID"] = null;
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;
                    Attachment = "#";

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/PolicyProcedures";

                    divAlertMsg.Visible = false;
                    hdnIsUpdate.Value = "0";
                    if (HttpContext.Current.Items["PolicyProcedureID"] != null)
                    {
                        hdnIsUpdate.Value = "1";
                        PolicyProcedureID = Convert.ToInt32(HttpContext.Current.Items["PolicyProcedureID"].ToString());
                        getPolicyProcedureByID(PolicyProcedureID);
                        CheckAccess();
                    }
                }

                //Page.ClientScript.GetPostBackEventReference(this, string.Empty);
                //string ctrlName = Request.Params.Get("__EVENTTARGET");
                //string ctrlArgs = Request.Params.Get("__EVENTARGUMENT");

                //if (!String.IsNullOrEmpty(ctrlName) && ctrlName == "attachments")
                //{
                //    if (updAttachments.HasFile || updAttachments.HasFiles)
                //    {
                //        foreach (HttpPostedFile file in updAttachments.PostedFiles)
                //        {
                //            Random rand = new Random((int)DateTime.Now.Ticks);
                //            int randnum = 0;

                //            string fn = "";
                //            string exten = "";
                //            string destDir = Server.MapPath("~/assets/" + ctrlName + "/policies/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
                //            randnum = rand.Next(1, 100000);
                //            fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

                //            if (!Directory.Exists(destDir))
                //            {
                //                Directory.CreateDirectory(destDir);
                //            }
                //            string fname = Path.GetFileName(file.FileName);
                //            exten = Path.GetExtension(file.FileName);
                //            file.SaveAs(destDir + fn + exten);

                //            attachment = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/" + ctrlName + "/policies/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
                            
                //        }
                //    }
                //}

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected string Attachment
        {
            get
            {
                if (ViewState["Attachment"] != null)
                {
                    return (string)ViewState["Attachment"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["Attachment"] = value;
            }
        }


        protected void uploadFile()
        {

            if (updLogo != null)
            {
                Random rand = new Random((int)DateTime.Now.Ticks);
                int randnum = 0;

                string fn = "";
                string exten = "";

                string destDir = Server.MapPath("~/assets/Attachments/policies/");
                randnum = rand.Next(1, 100000);
                fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }

                string fname = Path.GetFileName(updLogo.PostedFile.FileName);
                exten = Path.GetExtension(updLogo.PostedFile.FileName);
                updLogo.PostedFile.SaveAs(destDir + fn + exten);

                Attachment =  "/assets/Attachments/policies/" + fn + exten;
            }



        }


        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "PolicyProcedure";
                objDB.PrimaryColumnnName = "PolicyProcedureID";
                objDB.PrimaryColumnValue = PolicyProcedureID.ToString();
                objDB.DocName = "PolicyProcedure";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getPolicyProcedureByID(int PolicyProcedureID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.PolicyProcedureID = PolicyProcedureID;
                dt = objDB.GetPolicyProcedureByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtPolicyProcedureTitle.Value = dt.Rows[0]["PolicyProcedureTitle"].ToString();
                        txtDescription.Value = dt.Rows[0]["Description"].ToString();
                        docPath.HRef = dt.Rows[0]["DocPath"].ToString();
                        docPath.InnerHtml = dt.Rows[0]["PolicyProcedureTitle"].ToString();
                        Attachment = dt.Rows[0]["DocPath"].ToString();
                        objDB.DocID = PolicyProcedureID;
                        objDB.DocType = "PolicyProcedure";
                        ltrNotesTable.Text = objDB.GetDocNotes();
                    }
                }
                Common.addlog("View", "HR", "PolicyProcedure \"" + txtPolicyProcedureTitle.Value + "\" Viewed", "PolicyProcedure", objDB.PolicyProcedureID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";
                uploadFile();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.PolicyProcedureTitle = txtPolicyProcedureTitle.Value;
                objDB.DocPath = Attachment;
                objDB.Description = txtDescription.Value;
                int Docid = 0;
                if (HttpContext.Current.Items["PolicyProcedureID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.PolicyProcedureID = PolicyProcedureID;
                    res = objDB.UpdatePolicyProcedure();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddPolicyProcedure();
                    if (res == "New PolicyProcedure Added")
                    {
                        clearFields();
                    }

                }

                objDB.DocType = "PolicyProcedure";
                objDB.DocID = Docid;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

                if (res == "New PolicyProcedure Added" || res == "PolicyProcedure Data Updated")
                {
                    if (res == "New PolicyProcedure Added") { Common.addlog("Add", "HR", "New PolicyProcedure \"" + objDB.PolicyProcedureTitle + "\" Added", "PolicyProcedure"); }
                    if (res == "PolicyProcedure Data Updated") { Common.addlog("Update", "HR", "PolicyProcedure \"" + objDB.PolicyProcedureTitle + "\" Updated", "PolicyProcedure", objDB.PolicyProcedureID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtPolicyProcedureTitle.Value = "";
            Attachment = "#";
            txtDescription.Value = "";
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "PolicyProcedure", "PolicyProcedureID", HttpContext.Current.Items["PolicyProcedureID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "PolicyProcedure of ID\"" + HttpContext.Current.Items["PolicyProcedureID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/PolicyProcedure", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/PolicyProcedures/edit-PolicyProcedure-" + HttpContext.Current.Items["PolicyProcedureID"].ToString(), "PolicyProcedure \"" + txtPolicyProcedureTitle.Value + "\"", "PolicyProcedure", "PolicyProcedure", Convert.ToInt32(HttpContext.Current.Items["PolicyProcedureID"].ToString()));

                //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                //{
                //    objDB.PolicyProcedureID = PolicyProcedureID;
                //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //    objDB.AddPolicyProcedureToEmployeeAttendance();

                //}

                //Common.addlog("Delete", "HR", "PolicyProcedure of ID \"" + objDB.PolicyProcedureID + "\" deleted", "PolicyProcedure", objDB.PolicyProcedureID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "PolicyProcedure", "PolicyProcedureID", HttpContext.Current.Items["PolicyProcedureID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "PolicyProcedure of ID\"" + HttpContext.Current.Items["PolicyProcedureID"].ToString() + "\" Status Changed", "PolicyProcedure", PolicyProcedureID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                objDB.PolicyProcedureID = PolicyProcedureID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeletePolicyProcedure();
                Common.addlog("PolicyProcedure Rejected", "HR", "PolicyProcedure of ID\"" + HttpContext.Current.Items["PolicyProcedureID"].ToString() + "\" PolicyProcedure Rejected", "PolicyProcedure", PolicyProcedureID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "PolicyProcedure Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


    }
}