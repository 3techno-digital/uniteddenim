﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeSummary.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.EmployeeSummary" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <div class="wrap">
                <%--      <asp:UpdatePanel ID="UpdPnl" runat="server">
            <ContentTemplate>--%>
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">Employee Summary</h1>
                            </div>


                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="pull-right flex">
                                    <%--<button id="btnPDFMake" type="button" onclick="genPDF();" runat="server">PDF</button>--%>
                                    <button class="AD_btn" id="btnPDF" runat="server" onserverclick="btnPDF_ServerClick">PDF</button>
                                    <%--<button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>--%>
                                    <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="row">
                                <div id="printThis">
                                    <div class="content-header second_heading">
                                        <div class="container-fluid">
                                            <div class="row mb-2">
                                                <div class="col-sm-6">
                                                    <h1 class="tf-page-heading-text">Header</h1>
                                                </div>
                                                <!-- /.col -->

                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <!-- /.container-fluid -->
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <textboxio:Textboxio runat="server" ID="txtHeader" ScriptSrc="/assets/textboxio/textboxio.js" Width="900px" Height="300px" />
                                        </div>
                                    </div>
                                    <div class="content-header second_heading">
                                        <div class="container-fluid">
                                            <div class="row mb-2">
                                                <div class="col-sm-6">
                                                    <h1 class="tf-page-heading-text">Content</h1>
                                                </div>
                                                <!-- /.col -->

                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <!-- /.container-fluid -->
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <textboxio:Textboxio runat="server" ID="txtContent" ScriptSrc="/assets/textboxio/textboxio.js" Width="900px" Height="500px" />
                                        </div>
                                    </div>
                                    <div class="content-header second_heading">
                                        <div class="container-fluid">
                                            <div class="row mb-2">
                                                <div class="col-sm-6">
                                                    <h1 class="tf-page-heading-text">Footer</h1>
                                                </div>
                                                <!-- /.col -->

                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <!-- /.container-fluid -->
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <textboxio:Textboxio runat="server" ID="txtFooter" ScriptSrc="/assets/textboxio/textboxio.js" Width="900px" Height="200px" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>

                </section>


                <%-- </ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
				</Triggers>
			</asp:UpdatePanel>--%>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <div id="divPrint"></div>
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
        <script>

            function genPDF() {

                var specialElementHandlers = {
                    '#txtHeader': function (element, renderer) {
                        return true;
                    }
                };

                var margin = {
                    top: 10,
                    left: 10,
                    right: 10,
                    bottom: 10
                };

                var editorHeader = textboxio.replace('#txtHeader');
                var editorContent = textboxio.replace('#txtContent');
                var editorFooter = textboxio.replace('#txtFooter');

                var txtHeader = '<header>' + editorHeader.content.get() + '</header>';
                var txtContent = editorContent.content.get();
                var txtFooter = '<footer>' + editorFooter.content.get() + '</footer>';

                var pdf = new jsPDF('p', 'pt', 'letter');
                pdf.canvas.height = 80 * 11;
                pdf.canvas.width = 80 * 8.5;

                //pdf.fromHTML(txtHeader + txtContent + txtFooter, 0, 0, {
                //    'width': 100,
                //    'elementHandlers': specialElementHandlers
                //},
                //    function (bla) { pdf.save('saveInCallback.pdf'); },
                //    margin);

                //pdf.fromHTML(txtHeader + txtContent + txtFooter);
                //pdf.save('saveInCallback.pdf');
                //$('#divPrint').html(txtHeader + txtContent + txtFooter);
                //pdf.addHTML(document.getElementById('divPrint'), function () {
                //    pdf.save('Test.pdf');
                //});
            }
        </script>
        <style>
            .month-table-show {
                display: none;
                margin-top: 40px;
            }

            .modal-dialog {
                width: 450px;
                margin: 30px auto;
            }

            section.app-content {
                padding-bottom: 53px;
            }

            .modal-footer {
                border-top: 0px solid #e5e5e5;
            }

            .hiring-create-new-btn {
                margin-top: 17px;
                border: none;
                background: none;
                color: #01769a;
            }

            .user-img-div img {
                width: 169px !important;
                display: block;
                margin: 0 auto;
            }

            .user-img-div {
                width: auto;
            }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
        </style>
    </form>
</body>
</html>

