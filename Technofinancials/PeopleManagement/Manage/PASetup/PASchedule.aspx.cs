﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage.PASetup
{
    public partial class PASchedule : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int PAScheduleID
        {
            get
            {
                if (ViewState["PAScheduleID"] != null)
                {
                    return (int)ViewState["PAScheduleID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PAScheduleID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/appraisal-schedule";

                    divAlertMsg.Visible = false;
                    clearFields();

                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["PAScheduleID"] != null)
                    {
                        PAScheduleID = Convert.ToInt32(HttpContext.Current.Items["PAScheduleID"].ToString());
                        getDataByID(PAScheduleID);
                        CheckAccess();

                    }
                    else
                    {

                    }
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.AppraisalScheduleID = PAScheduleID;
            dt = objDB.GetAppraisalScheduleByID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtFromDate.Value = DateTime.Parse(dt.Rows[0]["StartDate"].ToString()).ToString("dd-MMM-yyyy");
                    txtToDate.Value = DateTime.Parse(dt.Rows[0]["EndDate"].ToString()).ToString("dd-MMM-yyyy");
          


                }
            }
            Common.addlog("View", "HR", "AppraisalSchedule \"" + txtFromDate.Value + "\" Viewed", "AppraisalSchedule", objDB.AppraisalScheduleID);

        }

        private void clearFields()
        {
            txtFromDate.Value = "";
            txtToDate.Value = "";
            txtNotes.Value = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.StartDate = txtFromDate.Value;
                objDB.EndDate = txtToDate.Value;
                objDB.Notes = txtNotes.Value;



                if (HttpContext.Current.Items["PAScheduleID"] != null)
                {
                    // update coding
                    objDB.AppraisalScheduleID = PAScheduleID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateAppraisalSchedule();
                }
                else
                {
                    // add coding
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddAppraisalSchedule();
                    clearFields();
                }





                if (res == "Appraisal Schedule added" || res == "Appraisal Schedule Updated")
                {
                    if (res == "Appraisal Schedule added") { Common.addlog("Add", "HR", "New Appraisal Schedule \"" + objDB.StartDate + "\" Added", "AppraisalSchedule"); }
                    if (res == "Appraisal Schedule Updated") { Common.addlog("Update", "HR", "Appraisal Schedule \"" + objDB.StartDate + "\" Updated", "AppraisalSchedule", objDB.AppraisalScheduleID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "AppraisalSchedule";
                objDB.PrimaryColumnnName = "AppraisalScheduleID";
                objDB.PrimaryColumnValue = PAScheduleID.ToString();
                objDB.DocName = "PerformanceAppraisals";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "AppraisalSchedule", "AppraisalScheduleID", HttpContext.Current.Items["PAScheduleID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "AppraisalSchedule of ID \"" + HttpContext.Current.Items["PAScheduleID"].ToString() + "\" Status Changed", "AppraisalSchedule", PAScheduleID/* id from page top   */);/* Button1_ServerClick   */


                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "AppraisalSchedule", "AppraisalScheduleID", HttpContext.Current.Items["PAScheduleID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "AppraisalSchedule of ID \"" + HttpContext.Current.Items["PAScheduleID"].ToString() + "\" deleted", "AppraisalSchedule", PAScheduleID/* id from page top   */);/* lnkDelete_Click   */

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

    }
}