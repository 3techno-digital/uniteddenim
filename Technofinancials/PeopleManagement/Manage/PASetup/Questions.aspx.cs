﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.PeopleManagement.Manage.PASetup
{
    public partial class Questions : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected int COAID
        {
            get
            {
                if (ViewState["COAID"] != null)
                {
                    return (int)ViewState["COAID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["COAID"] = value;
            }
        }
        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                  
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/finance/view/chart-of-account-step-03";

                    BindDepartments();


                    ViewState["showLocationFirstRow"] = null;
                    ViewState["LocationSrNo"] = null;
                    ViewState["dtLocationCode"] = null;
                    dtCatQuestion = createCatQuestionTable();
                    BindCatQuestionTable();



                    divAlertMsg.Visible = false;
                    clearFields();
                    
                    GetData();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    if (HttpContext.Current.Items["COAID"] != null)
                    {
                        COAID = Convert.ToInt32(HttpContext.Current.Items["COAID"].ToString());
                        getDataByID(COAID);
                        CheckAccess();
                    }
                    else
                    {

                    }



                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.TableName = "FAM_COA";
                objDB.PrimaryColumnnName = "COA_ID";
                objDB.PrimaryColumnValue = COAID.ToString();
                objDB.DocName = "ConfigureCOA";
                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void GetData()
        {
            CheckSessions();

            if (ddlDepartment.SelectedValue == "0")
            {
                gvDiv.Visible = false;
            }
            else
            {
                objDB.DeptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                DataTable dt = new DataTable();
                dt = objDB.GetCatQuestionsByDepID(ref errorMsg);

                dtCatQuestion = null;
                dtCatQuestion = new DataTable();
                dtCatQuestion = createCatQuestionTable();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtCatQuestion.Rows[0][0].ToString() == "")
                        {
                            dtCatQuestion.Rows[0].Delete();
                            dtCatQuestion.AcceptChanges();
                            showFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtCatQuestion.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["QuestionID"],
                            dt.Rows[i]["QuestionType"],
                            dt.Rows[i]["Question"],
                            dt.Rows[i]["CategoryID"],
                            dt.Rows[i]["CategoryName"]
                        });
                        }
                        SrNo = Convert.ToInt32(dtCatQuestion.Rows[dtCatQuestion.Rows.Count - 1][0].ToString()) + 1;
                    }
                }
                BindCatQuestionTable();

            }


            // Company Code End


        }

        private void getDataByID(int ID)
        {
            DataTable dt = new DataTable();
            objDB.ChartOfAccountID = COAID;
            dt = objDB.GetCOAByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {




                }
            }

        }

        private void clearFields()
        {
            txtNotes.Value = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.Notes = txtNotes.Value;

                if (HttpContext.Current.Items["COAID"] != null)
                {
                    // update coding
                    objDB.ChartOfAccountID = COAID;
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    res = objDB.UpdateCOA();
                }
                else
                {
                    // add coding
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddCOA();
                    clearFields();
                }

                if (res == "New COA Added" || res == "COA Updated")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "FAM_COA", "COA_ID", HttpContext.Current.Items["COAID"].ToString(), Session["UserName"].ToString());
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private DataTable dtCatQuestion
        {
            get
            {
                if (ViewState["dtCatQuestion"] != null)
                {
                    return (DataTable)ViewState["dtCatQuestion"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtCatQuestion"] = value;
            }
        }
        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }
        protected int SrNo
        {
            get
            {
                if (ViewState["SrNo"] != null)
                {
                    return (int)ViewState["SrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["SrNo"] = value;
            }
        }
        private DataTable createCatQuestionTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("QuestionID");
            dt.Columns.Add("QuestionType");
            dt.Columns.Add("Question");
            dt.Columns.Add("CategoryID");
            dt.Columns.Add("CategoryName");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindCatQuestionTable()
        {
            if (ViewState["dtCatQuestion"] == null)
            {
                dtCatQuestion = createCatQuestionTable();
                ViewState["dtCatQuestion"] = dtCatQuestion;
            }

            gvQuestion.DataSource = dtCatQuestion;
            gvQuestion.DataBind();

            if (showFirstRow)
                gvQuestion.Rows[0].Visible = true;
            else
                gvQuestion.Rows[0].Visible = false;

            gvQuestion.UseAccessibleHeader = true;
            gvQuestion.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gvQuestion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                objDB.DeptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                DropDownList ddlCom = e.Row.FindControl("ddlCategory") as DropDownList;
                ddlCom.DataSource = objDB.GetDepCategoryByDepID(ref errorMsg);
                ddlCom.DataTextField = "Title";
                ddlCom.DataValueField = "DepCategoryID";
                ddlCom.DataBind();
                ddlCom.Items.Insert(0, new ListItem("--- Select Category ---", "0"));


                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = SrNo.ToString();
            }


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    //objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    //DropDownList ddlLocEdit = e.Row.FindControl("ddlEditQuestion") as DropDownList;
                    //ddlLocEdit.DataSource = objDB.GetAllQuestions(ref errorMsg);
                    //ddlLocEdit.DataTextField = "NAME";
                    //ddlLocEdit.DataValueField = "Question_ID";
                    //ddlLocEdit.DataBind();
                    //ddlLocEdit.Items.Insert(0, new ListItem("--- Select Question ---", "0"));


                }
            }
        }
        protected void gvQuestion_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvQuestion.EditIndex = e.NewEditIndex;
            BindCatQuestionTable();
        }
        protected void gvQuestion_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvQuestion.EditIndex = -1;
            BindCatQuestionTable();
        }
        protected void gvQuestion_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;
            string QuestionID = ((HiddenField)gvQuestion.Rows[e.RowIndex].FindControl("hdnQuestionID")).Value;
            objDB.QuestionID = Convert.ToInt32(QuestionID);
            objDB.CategoryID = Convert.ToInt32(dtCatQuestion.Rows[index]["CategoryID"].ToString());
            objDB.Question = ((TextBox)gvQuestion.Rows[e.RowIndex].FindControl("txtEditQuestion")).Text;
            objDB.QuestionType = ((DropDownList)gvQuestion.Rows[e.RowIndex].FindControl("ddlEditType")).SelectedItem.Text;
            objDB.ModifiedBy = Session["UserName"].ToString();
            objDB.UpdateCatQuestion();

            // dtCatQuestion.Rows[index].SetField(1, ((DropDownList)gvQuestion.Rows[e.RowIndex].FindControl("ddlEditQuestion")).SelectedItem.Text);
            dtCatQuestion.Rows[index].SetField(2, ((DropDownList)gvQuestion.Rows[e.RowIndex].FindControl("ddlEditType")).SelectedItem.Text);
            dtCatQuestion.Rows[index].SetField(3, ((TextBox)gvQuestion.Rows[e.RowIndex].FindControl("txtEditQuestion")).Text);
            dtCatQuestion.AcceptChanges();

            gvQuestion.EditIndex = -1;
            BindCatQuestionTable();
        }
        protected void Question_lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtCatQuestion.Rows.Count; i++)
            {
                if (dtCatQuestion.Rows[i][0].ToString() == delSr)
                {
                    string res = Common.addAccessLevels("Delete", "CatQuestions", "QuestionID", dtCatQuestion.Rows[i]["QuestionID"].ToString(), Session["UserName"].ToString());
                    GetData();
                }
            }

        }
        protected void Question_btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                if (dtCatQuestion.Rows[0][0].ToString() == "")
                {
                    dtCatQuestion.Rows[0].Delete();
                    dtCatQuestion.AcceptChanges();
                    showFirstRow = true;
                }

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                objDB.CategoryID = Convert.ToInt32(((DropDownList)gvQuestion.FooterRow.FindControl("ddlCategory")).SelectedItem.Value);
                objDB.QuestionType = ((DropDownList)gvQuestion.FooterRow.FindControl("ddlType")).SelectedItem.Value;
                objDB.Question = ((TextBox)gvQuestion.FooterRow.FindControl("txtQuestion")).Text;
                objDB.CreatedBy = Session["UserName"].ToString();
                string res = objDB.AddCatQuestion();
                if (res == "New Question Added Successfully")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                    GetData();
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;

                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }




        }


        protected void btnsaveNext_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/dashboard");
        }

       

        private void BindDepartments()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                ddlDepartment.DataTextField = "DeptName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("--- Select Department ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlDepartment.SelectedValue == "0")
                {
                    gvDiv.Visible = false;
                }
                else
                {
                    gvDiv.Visible = true;
                    GetData();

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}