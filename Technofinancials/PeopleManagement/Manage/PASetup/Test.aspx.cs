﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage.PASetup
{
    public partial class Test : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            GetStars("9");
        }

        private void GetStars(string DepartmentID)
        {
            objDB.DeptID = int.Parse(DepartmentID);
            DataTable dtstars = objDB.GetCatQuestionsByDepID(ref errorMsg);
            if (dtstars != null)
            {
                if (dtstars.Rows.Count > 0)
                {
                    for (int i = 0; i < dtstars.Rows.Count; i++)
                    {
                        ltrstars.Text += @"<tr>
                            <td>
                                <p class=""rating-txt"">" + dtstars.Rows[i]["Question"].ToString() + @"</p>
                            </td>
                            <td>
                                
                            ";

                        ltrstars.Text += @"<div class=""star-ratings start-ratings-main clearfix"">
                          <div class=""stars stars-example-fontawesome"">
                            <select id=""" + dtstars.Rows[i]["QuestionID"].ToString() + @""" name=""rating"" autocomplete=""off"" class=""example-fontawesome"">
                              <option value=""1"">1</option>
                              <option value=""2"">2</option>
                              <option value=""3"">3</option>
                              <option value=""4"">4</option>
                              <option value=""5"">5</option>
                            </select>
                          </div>
                        </div></td>
                        </tr>";
                    }
                }
            }
        }

    }
}