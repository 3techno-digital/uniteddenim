﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Category.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.PASetup.Category" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/chart-of-account.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Setup Performance Appraisal</h3> 
                        </div>

                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>


                                <div class="col-sm-4" >
                                    <div class="pull-right flex">
                                        <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>
                                        <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                        <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class" "Delete" CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                        <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                        <button class="tf-save-btn" "Reject & Disapproved" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>


                                        <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>

                    <div class="row">
                        <div class="container-fluid">
                            <div class="container-fluid">
                                <div class="stepwizard col-md-offset-3">
                                    <div class="stepwizard-row setup-panel">
                                        
                                        <div class="stepwizard-step">
                                            <a href="#" type="button" class="btn btn-default btn-circle" disabled="disabled">1</a>
                                            <p>Step 1</p>
                                        </div>
                                        
                                        <div class="stepwizard-step">
                                            <a href="#" type="button" class="btn btn-primary btn-circle">2</a>
                                            <p>Step 2</p>
                                        </div>
                                       
                                        <div class="stepwizard-step">
                                            <a href="#" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                            <p>Step 3</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3>Step 02: Categories</h3>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                          
                                                <asp:GridView ID="gvSubCategory" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvSubCategory_RowDataBound" OnRowUpdating="gvSubCategory_RowUpdating" OnRowCancelingEdit="gvSubCategory_RowCancelingEdit" OnRowEditing="gvSubCategory_RowEditing">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. No">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SubCategorySrNo") %>'></asp:Label>
                                                                <asp:HiddenField runat="server" id="hdnSubCatID" Value='<%# Eval("SubCategoryID") %>' />       
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                          <asp:TemplateField HeaderText="Department">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblCategory" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <%--<EditItemTemplate>
                                                                <asp:DropDownList ID="ddlEditCompanies" runat="server" CssClass="form-control js-example-basic-single">
                                                                </asp:DropDownList>
                                                            </EditItemTemplate>--%>
                                                            <FooterTemplate>
                                                                  <asp:RequiredFieldValidator runat="server" ID="reqValddlCategory" InitialValue="0" ControlToValidate="ddlCategory" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="gvAdd" ForeColor="Red" />
                                                                <asp:DropDownList ID="ddlCategory"  data-plugin="select2" runat="server" CssClass="select2 form-control js-example-basic-single" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true" >
                                                                </asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Category">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblSubCategory" Text='<%# Eval("SubCategoryName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                 <asp:RequiredFieldValidator runat="server" ID="RVtxtEditSubCategory" ControlToValidate="txtEditSubCategory" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="gvUpdate" ForeColor="Red" />

                                                                <asp:TextBox runat="server" ID="txtEditSubCategory" CssClass="form-control" Text='<%# Eval("SubCategoryName") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                 <asp:RequiredFieldValidator runat="server" ID="RVtxtSubCategory" ControlToValidate="txtSubCategory" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="gvAdd" ForeColor="Red" />

                                                                <asp:TextBox runat="server" ID="txtSubCategory" CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Score">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblDesc" Text='<%# Eval("SubCategoryCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="reqtxtEditDesc" ControlToValidate="txtEditDesc" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="gvUpdate" ForeColor="Red"  />
                                                                 <asp:RangeValidator ID="RVEditDesc" runat="server" ControlToValidate="txtEditDesc" ErrorMessage="Code must be Integer & b/w 1-100." ForeColor="Red" MaximumValue="100" MinimumValue="1" Type="Integer" Display = "Dynamic" ValidationGroup="gvUpdate"></asp:RangeValidator>  
                                                               
                                                                <asp:TextBox runat="server" ID="txtEditDesc" CssClass="form-control" Text='<%# Eval("SubCategoryCode") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="reqtxtDesc" ControlToValidate="txtDesc" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="gvAdd" ForeColor="Red" style="color:Red !important"/>
                                                                <asp:RangeValidator ID="RVDesc" runat="server" ControlToValidate="txtDesc" ErrorMessage="Code must be Integer & b/w 1-100." ForeColor="Red" MaximumValue="100" MinimumValue="1" Display = "Dynamic" Type=" Integer" ValidationGroup="gvAdd"></asp:RangeValidator>  
                                                                <asp:TextBox runat="server" ID="txtDesc" CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ShowHeader="false">
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" ValidationGroup="gvUpdate" CommandName="Update" Text="Update"></asp:LinkButton>
                                                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="SubCategory_btnAdd" runat="server" CssClass="form-control" Text="Add" ValidationGroup="gvAdd" CausesValidation="true" OnClick="SubCategory_btnAdd_Click"></asp:Button>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="SubCategory_lnkRemove" runat="server" CommandArgument='<%# Eval("SubCategorySrNo")%>' Text="Delete" OnCommand="SubCategory_lnkRemove_Command"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>


                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        
                                         <button class="tf-next-btn btn btn-primary nextBtn btn-lg pull-right" "Save & Next" data-original-"Save & Next" aria-describedby="tooltip32012" type="button" id="btnsaveNext" runat="server" onserverclick="btnsaveNext_ServerClick">Save & Next <i class="fa fa-chevron-right" aria-hidden="true"></i></button>

                                    </div>
                                </div>
                            
                                    
                            
                            </div>
                        </div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
        
                </section>


                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }


            .aspNetDisabled {
                width: 100% !important;
                background: #eeeeee;
                height: 46px;
                padding: 10px 16px;
                font-size: 18px;
                line-height: 1.3333333;
                border-radius: 6px;
                border-color: #ccc;
                outline: none;
                box-shadow: none;
            }
        </style>

        <style>
            .stepwizard-step p {
                margin-top: 10px;
            }

            .stepwizard-row {
                display: table-row;
            }

            .stepwizard {
                display: table;
                width: 50%;
                position: relative;
            }

            .stepwizard-step button[disabled] {
                opacity: 1 !important;
                filter: alpha(opacity=100) !important;
            }

            .stepwizard-row:before {
                top: 14px;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 100%;
                height: 1px;
                background-color: #ccc;
                z-order: 0;
            }

            .stepwizard-step {
                display: table-cell;
                text-align: center;
                position: relative;
            }

            .btn-circle {
                width: 30px;
                height: 30px;
                text-align: center;
                padding: 6px 0;
                font-size: 12px;
                line-height: 1.428571429;
                border-radius: 15px;
            }

            button.btn {
                margin-top: 25px;
                margin-bottom: 48px;
                margin-right: 30px;
            }

            th {
                border-right: 1px solid #fff;
                text-align: center;
            }
        </style>
        <script>
            $("#button").click(function () {
                $("form1").valid();
            });
        </script>

    </form>
</body>
</html>

