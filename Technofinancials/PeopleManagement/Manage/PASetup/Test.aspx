﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.PASetup.Test" %>
<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%@ Register Assembly="TextboxioControl" Namespace="TextboxioControl" TagPrefix="textboxio" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/chart-of-account.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Setup Performance Appraisal</h3>
                        </div>

                        <div class="col-md-4">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>


                               
                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                </p>
                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>

                    <div class="row">
                        <div class="container-fluid">
                            <div class="container-fluid">
                                <div class="stepwizard col-md-offset-3">
                                    <div class="stepwizard-row setup-panel">
                                        <div class="stepwizard-step">
                                            <a href="#" type="button" class="btn btn-primary btn-circle">1</a>
                                            <p>Step 1</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <a href="#" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                            <p>Step 2</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                            <p>Step 3</p>
                                        </div>
                                       
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3>Step 01: Instructions</h3>

                                                <div class="letters-width">
                                                </div>
                                         <asp:Literal ID="ltrstars" runat="server"></asp:Literal>

                                          
                                        
                                        <button class="tf-next-btn btn btn-primary nextBtn btn-lg pull-right" "Next" data-original-"Next" aria-describedby="tooltip32012" type="button" id="btnsaveNext" runat="server">Next <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                                    </div>
                                </div>



                            </div>
                        </div>

                        <div class="clearfix">&nbsp;</div>
                </section>


                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>

        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        
            <style>
            .rate ul {
                margin: 0px;
                padding: 0px;
                list-style: none;
            }

                .rate ul li {
                    display: inline-block;
                    margin: 0px 5px 5px 0px;
                }

                    .rate ul li label {
                        cursor: pointer;
                    }

                        .rate ul li label input[type=radio] {
                            display: none;
                            visibility: hidden;
                        }

                        .rate ul li label span.radio {
                            display: block;
                            width: 32px;
                            height: 32px;
                            background-image: url(http://www.ferhatkatesci.com/assets/images/codepen/rate.png);
                            background-repeat: no-repeat;
                            opacity: 0.25;
                            filter: alpha(opacity=25);
                        }

                            .rate ul li label span.radio:hover {
                                opacity: 0.40;
                                filter: alpha(opacity=40);
                            }

                        .rate ul li label input[type=radio]:checked + span.radio {
                            opacity: 1;
                            filter: alpha(opacity=100);
                        }

                        .rate ul li label.very-bad span.radio {
                            background-position: 0px 0px;
                        }

                        .rate ul li label.bad span.radio {
                            background-position: 0px -32px;
                        }

                        .rate ul li label.normal span.radio {
                            background-position: 0px -64px;
                        }

                        .rate ul li label.good span.radio {
                            background-position: 0px -96px;
                        }

                        .rate ul li label.very-good span.radio {
                            background-position: 0px -128px;
                        }

            .rate-text {
                padding-top: 5px;
                font-style: italic;
            }

            .rate {
                text-align: center;
            }

            .rating-txt-emoji {
                text-align: center;
                font-size: 20px;
                font-weight: 500;
            }
        </style>
        <style>
            .totalSalaries {
                font-weight: bold !important;
                color: #188ae2 !important;
                font-size: 16px !important;
            }


            .aspNetDisabled {
                width: 100% !important;
                background: #eeeeee;
                height: 46px;
                padding: 10px 16px;
                font-size: 18px;
                line-height: 1.3333333;
                border-radius: 6px;
                border-color: #ccc;
                outline: none;
                box-shadow: none;
            }
        </style>

        <style>
            .stepwizard-step p {
                margin-top: 10px;
            }

            .stepwizard-row {
                display: table-row;
            }

            .stepwizard {
                display: table;
                width: 50%;
                position: relative;
            }

            .stepwizard-step button[disabled] {
                opacity: 1 !important;
                filter: alpha(opacity=100) !important;
            }

            .stepwizard-row:before {
                top: 14px;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 100%;
                height: 1px;
                background-color: #ccc;
                z-order: 0;
            }

            .stepwizard-step {
                display: table-cell;
                text-align: center;
                position: relative;
            }

            .btn-circle {
                width: 30px;
                height: 30px;
                text-align: center;
                padding: 6px 0;
                font-size: 12px;
                line-height: 1.428571429;
                border-radius: 15px;
            }

            button.btn {
                margin-top: 25px;
                margin-bottom: 48px;
                margin-right: 30px;
            }

            th {
                border-right: 1px solid #fff;
                text-align: center;
            }
        </style>
       

    </form>
</body>
</html>


