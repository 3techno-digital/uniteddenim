﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class Currency : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int CurrencyID
        {
            get
            {
                if (ViewState["CurrencyID"] != null)
                {
                    return (int)ViewState["CurrencyID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["CurrencyID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {   
                    ViewState["CurrencyID"] = null;
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/currency";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["CurrencyID"] != null)
                    {
                        CurrencyID = Convert.ToInt32(HttpContext.Current.Items["CurrencyID"].ToString());
                        getCurrencyByID(CurrencyID);
                       CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "Currency";
                objDB.PrimaryColumnnName = "CurrencyID";
                objDB.PrimaryColumnValue = CurrencyID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getCurrencyByID(int CurrencyID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.CurrencyID = CurrencyID;
                dt = objDB.getCurrencyByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtCurrencyName.Value = dt.Rows[0]["CurrencyName"].ToString();
                        txtAbbrivation.Value = dt.Rows[0]["Abbreviation"].ToString();
                        txtRate.Value = dt.Rows[0]["Rate"].ToString();
                        txtNotes.Value = dt.Rows[0]["Note"].ToString();

                    }
                }
                Common.addlog("View", "HR", "Currency \"" + txtCurrencyName.Value + "\" Viewed", "Currency", objDB.CurrencyID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                double rate = 0;
                CheckSessions();
                string res = "";
                double.TryParse(txtRate.Value, out rate);
                if (rate != 0 || rate > 0)
                {
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.CurrencyName = txtCurrencyName.Value;
                    objDB.Rate = Convert.ToDouble(txtRate.Value);
                    objDB.Abbrivation = txtAbbrivation.Value;
                    objDB.Notes = txtNotes.Value;
                    if (HttpContext.Current.Items["CurrencyID"] != null)
                    {
                        objDB.CurrencyName = txtCurrencyName.Value;
                        objDB.Abbrivation = txtAbbrivation.Value;
                        objDB.Rate = Convert.ToDouble(txtRate.Value);
                        objDB.ModifiedBy = Session["UserName"].ToString();
                        objDB.CurrencyID = CurrencyID;
                        objDB.UpdateCurrencyByID();
                        res = "Currency Data Updated";
                    }
                    else
                    {
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.AddCurrency();
                        clearFields();
                        res = "New Currency Added";
                    }
                }                


                if (res == "New Currency Added" || res == "Currency Data Updated")
                {
                    if (res == "New Currency Added") { Common.addlog("Add", "HR", "New Currency \"" + objDB.CurrencyName + "\" Added", "Currency"); }
                    if (res == "Currency Data Updated") { Common.addlog("Update", "HR", "Currency \"" + objDB.CurrencyName + "\" Updated", "Currency", objDB.CurrencyID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtCurrencyName.Value = "";
            txtRate.Value = "0";
            txtAbbrivation.Value = "";
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "Currency", "CurrencyID", HttpContext.Current.Items["CurrencyID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "Currency of ID\"" + HttpContext.Current.Items["CurrencyID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/Currency", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/Currency/edit-Currency-" + HttpContext.Current.Items["CurrencyID"].ToString(), "Currency \"" + txtCurrencyName.Value + "\"", "Currency", "Currency", Convert.ToInt32(HttpContext.Current.Items["CurrencyID"].ToString()));

                //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                //{
                //    objDB.CurrencyID = CurrencyID;
                //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //    objDB.AddCurrencyToEmployeeAttendance();

                //}

                //Common.addlog("Delete", "HR", "Currency of ID \"" + objDB.CurrencyID + "\" deleted", "Currency", objDB.CurrencyID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Currency", "CurrencyID", HttpContext.Current.Items["CurrencyID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "Currency of ID\"" + HttpContext.Current.Items["CurrencyID"].ToString() + "\" Status Changed", "Currency", CurrencyID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.CurrencyID = CurrencyID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteCurrency();
                Common.addlog("Currency Rejected", "HR", "Currency of ID\"" + HttpContext.Current.Items["CurrencyID"].ToString() + "\" Currency Rejected", "Currency", CurrencyID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Currency Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


    }
}