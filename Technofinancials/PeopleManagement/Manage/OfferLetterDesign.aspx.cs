﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class OfferLetters : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        public string pageType
        {
            get
            {
                if (ViewState["pageType"] != null)
                {
                    return (string)ViewState["pageType"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["pageType"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {   
                btnOfferLetter.Visible = false;
                btnWarningLetter.Visible = false;
                btnPayrollLetter.Visible = false;
                btnPromotionLetter.Visible = false;
                btnAttendanceSheet.Visible = false;
                btnHRReport.Visible = false;
                btnEmpSummary.Visible = false;
                btnPaySlip.Visible = false;

                divAlertMsg.Visible = false;

                if (HttpContext.Current.Items["PageType"] != null)
                {
                    pageType = HttpContext.Current.Items["PageType"].ToString();
                    pageHeading.InnerHtml = pageType;
                    if (pageType == "Offer Letter")
                    {
                        imgHeading.Src = "/assets/images/Offer Letter Design.png";
                        btnOfferLetter.Visible = true;
                    }
                    else if (pageType == "Warning Letter")
                    {
                        imgHeading.Src = "/assets/images/Warning.png";
                        btnWarningLetter.Visible = true;
                    }
                    else if (pageType == "Payroll")
                    {
                        imgHeading.Src = "/assets/images/Payroll-B.png";
                        btnPayrollLetter.Visible = true;
                    }
                    else if (pageType == "Promotion Letter")
                    {
                        imgHeading.Src = "/assets/images/succession.png";
                        btnPromotionLetter.Visible = true;
                    }
                    else if (pageType == "Attendance Sheet")
                    {
                        imgHeading.Src = "/assets/images/Attendance_Sheet.png";
                        btnAttendanceSheet.Visible = true;
                    }
                    else if (pageType == "HR Reports")
                    {
                        imgHeading.Src = "/assets/images/Attendance_Sheet.png";
                        btnHRReport.Visible = true;

                    }
                    else if (pageType == "Employee Summary")
                    {
                        imgHeading.Src = "/assets/images/Attendance_Sheet.png";
                        btnEmpSummary.Visible = true;

                    }
                    else if (pageType == "Pay Slip")
                    {
                        imgHeading.Src = "/assets/images/Attendance_Sheet.png";
                        btnPaySlip.Visible = true;

                    }
                    getDocumentDesign();
                }
            }
        }

        private void getDocumentDesign()
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DocType = pageType;
            dt = objDB.GetDocumentDesign(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                    txtContent.Content = dt.Rows[0]["DocContent"].ToString();
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                }
            }
            Common.addlog("View", "HR", "Draft Design \"" + objDB.DocType + "\" Viewed", "DocumentDesigns");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DocType = pageType;
            objDB.DocHeader = txtHeader.Content;
            objDB.DocContent = txtContent.Content;
            objDB.DocFooter = txtFooter.Content;
            objDB.CreatedBy = Session["UserName"].ToString();
            string res = objDB.AddDocumentDesgin();

            if (res == "Draft Design Updated")
            {
                Common.addlog("Update", "HR", "Draft Design \"" + objDB.DocType + "\" Updated", "DocumentDesigns");
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }
    }
}