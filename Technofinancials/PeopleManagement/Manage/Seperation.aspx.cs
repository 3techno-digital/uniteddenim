﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.PeopleManagement.Manage
{
    public partial class Seperation : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int EmployeeFullAndFinalID
        {
            get
            {
                if (ViewState["EmployeeFullAndFinalID"] != null)
                {
                    return (int)ViewState["EmployeeFullAndFinalID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["EmployeeFullAndFinalID"] = value;
            }
        }

        protected string DocStatus
        {
            get
            {
                if (ViewState["DocStatus"] != null)
                {
                    return (string)ViewState["DocStatus"];
                }
                else
                {
                    return "Saved as Draft";
                }
            }

            set
            {
                ViewState["DocStatus"] = value;
            }
        }


        protected string AddonType
        {
            get
            {
                if (ViewState["AddonType"] != null)
                {
                    return (string)ViewState["AddonType"];
                }
                else
                {
                    return "Bonus";
                }
            }

            set
            {
                ViewState["AddonType"] = value;
            }
        }



        protected double EmpGrossSalary
        {
            get
            {
                if (ViewState["EmpGrossSalary"] != null)
                {
                    return Convert.ToDouble(ViewState["EmpGrossSalary"].ToString());
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["EmpGrossSalary"] = value;
            }
        }


        protected double EmpBasicSalary
        {
            get
            {
                if (ViewState["EmpBasicSalary"] != null)
                {
                    return Convert.ToDouble(ViewState["EmpBasicSalary"].ToString());
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["EmpBasicSalary"] = value;
            }
        }



        protected int PayrollStartDate
        {
            get
            {
                if (ViewState["PayrollStartDate"] != null)
                {
                    return Convert.ToInt32(ViewState["PayrollStartDate"].ToString());
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["PayrollStartDate"] = value;
            }
        }

        protected int PayrollEndDate
        {
            get
            {
                if (ViewState["PayrollEndDate"] != null)
                {
                    return Convert.ToInt32(ViewState["PayrollEndDate"].ToString());
                }
                else
                {
                    return 30;
                }
            }

            set
            {
                ViewState["PayrollEndDate"] = value;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    DocStatus = "Saved as Draft";
                    ViewState["EmployeeFullAndFinalID"] = null;
                    

                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    //divHolidayHours.Visible = false;
                    //divSimpleHours.Visible = false;
                    //divDays.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employee-seperation";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["EmployeeFullAndFinalID"] != null)
                    {
                        EmployeeFullAndFinalID = Convert.ToInt32(HttpContext.Current.Items["EmployeeFullAndFinalID"].ToString());
                        getAddOnsExpenseByID(EmployeeFullAndFinalID);
                        CheckAccess();
                    }
                    else
                    {
                        BindEmployeeDropdown();
                    }
                  

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "EmployeeFullAndFinal";
                objDB.PrimaryColumnnName = "EmployeeFullAndFinalID";
                objDB.PrimaryColumnValue = EmployeeFullAndFinalID.ToString();
                objDB.DocName = "EmployeeLists";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    //btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getAddOnsExpenseByID(int EmployeeFullAndFinalID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.EmployeeFullAndFinalID = EmployeeFullAndFinalID;
                dt = objDB.GetEmployeeFullAndFinalByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        DocStatus = dt.Rows[0]["DocStatus"].ToString();
                        BindEmployeeDropdown();
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();

                        txtDays.Text = dt.Rows[0]["AttendanceDays"].ToString();
                        txtSimpleHours.Text = dt.Rows[0]["SimpleHours"].ToString();
                        txtHolidayHours.Text = dt.Rows[0]["HolidayHours"].ToString();
                        txtAttendanceTotal.Text = dt.Rows[0]["AttendanceTotal"].ToString();

                        txtLeaveEncashmentDays.Text = dt.Rows[0]["LeaveEncashmentDays"].ToString();
                        txtLeaveEncashment.Text = dt.Rows[0]["LeaveEncashment"].ToString();

                        txtBonus.Text = dt.Rows[0]["Bonus"].ToString();
                        txtCommission.Text = dt.Rows[0]["Commission"].ToString();
                        txtGraduity.Text = dt.Rows[0]["Graduity"].ToString();
                        txtPFCompany.Text = dt.Rows[0]["CompanyPF"].ToString();
                        txtPFEmployee.Text = dt.Rows[0]["EmployeePF"].ToString();
                        txtAmount.Text = dt.Rows[0]["TotalAmount"].ToString();
                        txtNotes.Value = dt.Rows[0]["Note"].ToString();
                        EmpGrossSalary = Convert.ToDouble(dt.Rows[0]["GrossAmount"].ToString());
                    }
                }
                //Common.addlog("View", "HR", "Currency \"" + txtTitle.Value + "\" Viewed", "Currency", objDB.EmployeeFullAndFinalID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.Days = Convert.ToInt32(txtDays.Text);
                objDB.OverTimeSimple = float.Parse(txtSimpleHours.Text);
                objDB.OverTimeSpecial = float.Parse(txtHolidayHours.Text);
                objDB.AttendanceTotal = float.Parse(txtAttendanceTotal.Text);
                objDB.LeaveEncashmentDays = int.Parse(txtLeaveEncashmentDays.Text);
                objDB.LeaveEncashment = float.Parse(txtLeaveEncashment.Text);
                objDB.Bouses = float.Parse(txtBonus.Text);
                objDB.Commissions = float.Parse(txtCommission.Text);
                objDB.Graduity = float.Parse(txtGraduity.Text);
                objDB.EmployeePF = float.Parse(txtPFEmployee.Text);
                objDB.CompanyPF = float.Parse(txtPFCompany.Text);
                objDB.SeverancePay = double.Parse(txtAmount.Text);
                objDB.Notes = txtNotes.Value;
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                //objDB.AddonMonth = txtMonth.Value;

                if (HttpContext.Current.Items["EmployeeFullAndFinalID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.EmployeeFullAndFinalID = EmployeeFullAndFinalID;
                    res = objDB.UpdateEmployeeFullAndFinal();
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddEmployeeFullAndFinal();
                    clearFields();
                }



                if (res == "Employee Seperation Added" || res == "Employee Seperation Updated")
                {
                    if (res == "Employee Seperation Added") { Common.addlog("Add", "HR", "Employee \"" + ddlEmployee.SelectedItem.Text + "\" Seperation Added", "EmployeeFullAndFinal"); }
                    if (res == "Employee Seperation Updated") { Common.addlog("Update", "HR", "Employee \"" + ddlEmployee.SelectedItem.Text + "\" Seperation Added", "EmployeeFullAndFinal", objDB.EmployeeFullAndFinalID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields(string fromMethod = "none")
        {
            if (fromMethod == "none")
            {
                ddlEmployee.SelectedValue = "0";
            }

            txtLeaveEncashmentDays.Text = "0";
            txtLeaveEncashment.Text = "0";
            
            txtDays.Text = "0";
            txtHolidayHours.Text = "0";
            txtSimpleHours.Text = "0";
            txtAttendanceTotal.Text = "0";
            
            
            txtBonus.Text = "0";
            txtCommission.Text = "0";
            txtGraduity.Text = "0";
            
            txtPFCompany.Text = "0";
            txtPFEmployee.Text = "0";
            
            txtAmount.Text = "0";
            txtNotes.Value = "";

            EmpGrossSalary = 0;
            //divDays.Visible = false;
            //divHolidayHours.Visible = false;
            //divSimpleHours.Visible = false;


        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "EmployeeFullAndFinal", "EmployeeFullAndFinalID", HttpContext.Current.Items["EmployeeFullAndFinalID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "Employe Seperation of ID\"" + HttpContext.Current.Items["EmployeeFullAndFinalID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/employee-seperation", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/employee-seperation/edit-employee-seperation-" + HttpContext.Current.Items["EmployeeFullAndFinalID"].ToString(), "Employee Seperation of \"" + ddlEmployee.SelectedItem.Text + "\" " + res, "EmployeeFullAndFinal", "EmployeeLists", Convert.ToInt32(HttpContext.Current.Items["EmployeeFullAndFinalID"].ToString()));

                if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                {

                    objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                    objDB.SeverancePay = double.Parse(txtAmount.Text);
                    objDB.Notes = txtNotes.Value;
                    objDB.EmployeeFullAndFinalID = EmployeeFullAndFinalID;
                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.ApproveEmployeeFullAndFinal();
                }

                //Common.addlog("Delete", "HR", "Currency of ID \"" + objDB.EmployeeFullAndFinalID + "\" deleted", "Currency", objDB.EmployeeFullAndFinalID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "EmployeeFullAndFinal", "EmployeeFullAndFinalID", HttpContext.Current.Items["EmployeeFullAndFinalID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "Employe Seperation of ID\"" + HttpContext.Current.Items["EmployeeFullAndFinalID"].ToString() + "\" Status Changed", "EmployeeFullAndFinal", EmployeeFullAndFinalID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.EmployeeFullAndFinalID = EmployeeFullAndFinalID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteAddOnsExpenseByID();
                Common.addlog("Employee Seperation Rejected", "HR", "Seperation of \"" + ddlEmployee.SelectedItem.Text + "\" Rejected", "EmployeeFullAndFinal", EmployeeFullAndFinalID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Employee Seperation Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }




        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                if (DocStatus == "Approved" || DocStatus == "Disapproved" || DocStatus == "Rejected" || DocStatus == "Deleted")
                {
                    ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyID(ref errorMsg);
                    ddlEmployee.Enabled = false;
                }
                else
                { 
                    ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                    ddlEmployee.Enabled = true;
                }

                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void setValues()
        {


            double check = 0;
            if (!double.TryParse(txtLeaveEncashmentDays.Text,out check))
            {
                txtLeaveEncashmentDays.Text = "0";
            }

            if (!double.TryParse(txtDays.Text, out check))
            {
                txtDays.Text = "0";
            }

            if (!double.TryParse(txtHolidayHours.Text, out check))
            {
                txtHolidayHours.Text = "0";
            }

            if (!double.TryParse(txtSimpleHours.Text, out check))
            {
                txtSimpleHours.Text = "0";
            }

            if (!double.TryParse(txtPFCompany.Text, out check))
            {
                txtPFCompany.Text = "0";
            }

            if (!double.TryParse(txtPFEmployee.Text, out check))
            {
                txtPFEmployee.Text = "0";
            }

            if (!double.TryParse(txtGraduity.Text, out check))
            {
                txtGraduity.Text = "0";
            }

            if (!double.TryParse(txtBonus.Text, out check))
            {
                txtBonus.Text = "0";
            }

            if (!double.TryParse(txtCommission.Text, out check))
            {
                txtCommission.Text = "0";
            }

        }

        protected void txtSimpleHours_TextChanged(object sender, EventArgs e)
        {
            try
            {
                divAlertMsg.Visible = false;
                double SeverancePay = 0;
                double LeaveEncashment = 0;
                double AttendanceTotal = 0;
                double SalPerDay = Math.Round(EmpGrossSalary / DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month), 2);
                double SalPerHour = SalPerDay / 8.0;
                
                if (ddlEmployee.SelectedValue != "0")
                {
                    
                    setValues();

                    LeaveEncashment =  Math.Round(double.Parse(txtLeaveEncashmentDays.Text) * (EmpGrossSalary / 30.0), 2);
                    txtLeaveEncashment.Text = LeaveEncashment.ToString();

                    AttendanceTotal += Math.Round(double.Parse(txtDays.Text) * SalPerDay, 2);
                    AttendanceTotal += Math.Round((double.Parse(txtSimpleHours.Text) * SalPerHour * 1.5) + (double.Parse(txtHolidayHours.Text) * SalPerHour * 2), 2);
                    txtAttendanceTotal.Text = AttendanceTotal.ToString();

                    SeverancePay = AttendanceTotal + LeaveEncashment + double.Parse(txtPFEmployee.Text) + double.Parse(txtPFCompany.Text) + double.Parse(txtCommission.Text) + double.Parse(txtBonus.Text) + double.Parse(txtGraduity.Text);
                    txtAmount.Text = Math.Round(SeverancePay,0).ToString();

                }
                else
                {
                    clearFields("Employee");
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Please Select Employee";
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divAlertMsg.Visible = false;
                clearFields("Employee");
                if (ddlEmployee.SelectedValue !=  "0")
                {
                    CheckSessions();
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                    objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                    DataTable dtEmpSep = objDB.GetSeperationDataByEmployeeID(ref errorMsg);
                    if (dtEmpSep !=  null && dtEmpSep.Rows.Count>0)
                    {
                        EmpGrossSalary = double.Parse(dtEmpSep.Rows[0]["GrossAmount"].ToString());
                        txtDays.Text = dtEmpSep.Rows[0]["WorkingDays"].ToString();
                        //float years =  float.Parse(dtEmpSep.Rows[0]["Years"].ToString());
                        txtPFCompany.Text = dtEmpSep.Rows[0]["PFCompany"].ToString();
                        txtPFEmployee.Text = dtEmpSep.Rows[0]["PFEmployee"].ToString();
                        txtGraduity.Text = dtEmpSep.Rows[0]["Graduity"].ToString();
                        txtSimpleHours.Text = dtEmpSep.Rows[0]["SimpleHours"].ToString();
                        txtHolidayHours.Text = dtEmpSep.Rows[0]["HolidayHours"].ToString();
                    }

                    txtSimpleHours_TextChanged(null,null);

                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}