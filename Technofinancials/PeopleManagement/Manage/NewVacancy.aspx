﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewVacancy.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.NewVacancy" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">


                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">New Vacancy</h1>
                            </div>
                            <!-- /.col -->


                        
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div style="text-align: right;">
                                            <div class="">
                                                <button type="button" class="AD_btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note">Note</button>
                                                <button class="AD_btn" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                                <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                                <button class="AD_btn" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                                <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class"  CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
                                                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                                <button class="AD_btn"  id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                                <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                                <button class="AD_btn"  id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                                <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                                <a class="AD_btn"  id="btnBack" runat="server">Back</a>
                                            </div>
                                        </div>
                                    </div>

                                      
                         
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>



                <section class="app-content">


                    <asp:UpdatePanel ID="upd1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h4>Department
                                                    <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlDept" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            </div>
                                            <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h4>Designation
                                                   <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="ddlDesg" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            </div>
                                            <asp:DropDownList ID="ddlDesg" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlDesg_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h4>Basic Salary
                                                <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtBasicSalary" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            </div>
                                            <input class="form-control" id="txtBasicSalary" placeholder="Basic Salary" type="number" runat="server" />

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h4 for="comment">Qualilfication
                                                        <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="ddlEducation" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <asp:DropDownList ID="ddlEducation" runat="server" CssClass="form-control select2" data-plugin="select2">
                                                    <asp:ListItem Value="0">-- Select Qualilfication--</asp:ListItem>
                                                    <asp:ListItem Value="Below Matriculation">Below Matriculation</asp:ListItem>
                                                    <asp:ListItem Value="Diploma">Diploma</asp:ListItem>
                                                    <asp:ListItem Value="Matriculation / HSc.">Matriculation / HSc.</asp:ListItem>
                                                    <asp:ListItem Value="Intermmidiate / FSc.">Intermmidiate / FSc.</asp:ListItem>
                                                    <asp:ListItem Value="Graduation">Graduation</asp:ListItem>
                                                    <asp:ListItem Value="Masters">Masters</asp:ListItem>
                                                    <asp:ListItem Value="PhD">PhD</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h4 for="comment">Experience
                                                         <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlExperience" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <asp:DropDownList ID="ddlExperience" runat="server" CssClass="form-control select2" data-plugin="select2">
                                                    <asp:ListItem Value="0">-- Select Experience Level--</asp:ListItem>
                                                    <asp:ListItem Value="Fresh">Fresh</asp:ListItem>
                                                    <asp:ListItem Value="1 Year">1 Year</asp:ListItem>
                                                    <asp:ListItem Value="2 Years">2 Years</asp:ListItem>
                                                    <asp:ListItem Value="3 Years">3 Years</asp:ListItem>
                                                    <asp:ListItem Value="4-7 Years">4-7 Years</asp:ListItem>
                                                    <asp:ListItem Value="7-10 Years">7-10 Years</asp:ListItem>
                                                    <asp:ListItem Value="More Than 10 Years">More Than 10 Years</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h4>Position to be filled(Days)
                                                     <span style="color: red !important;">*</span>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtDuration" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            </div>
                                            <input class="form-control" id="txtDuration" placeholder="Position to be filled within (Days)" type="number" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                   <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group" id="divAlertMsg" runat="server">
                                            <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                <span>
                                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                </span>
                                                <p id="pAlertMsg" runat="server">
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                            <ContentTemplate>
                                                <!-- Modal -->
                                                <div class="modal fade M_set" id="notes-modal" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h1 class="m-0 text-dark">Notes</h1>
                                                                <div class="add_new">
                                                                    <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                                                    <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                                                </div>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                                </p>
                                                                <p>
                                                                      <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                                   
                                                                </p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>


                                    </div>
                                </div>

                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                        </Triggers>
                    </asp:UpdatePanel>



                    <div class="row" style="margin-top: 20px;">
                        <div id="exTab2" class="col-sm-12">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                  <a href="#1" data-toggle="tab">Additions</a>
                                </li>
                                <li><a href="#2" data-toggle="tab">Deductions</a>
                                </li>
                                <li><a href="#3" data-toggle="tab">Assets</a>
                                </li>
                                <li><a href="#4" data-toggle="tab">Skill Set</a>
                                </li>
                            </ul>

                            <div class="tab-content">

                                <div class="tab-pane  active" id="1">
                                    <asp:UpdatePanel ID="UpdPnl" runat="server">
                                        <ContentTemplate>

                                            <div class="content-header second_heading">
                                                <div class="container-fluid">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-6">
                                                            <h1 class="m-0 text-dark">Additions</h1>
                                                        </div>
                                                        <!-- /.col -->

                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.container-fluid -->
                                            </div>
                                            <div class="">
                                                <asp:GridView ID="gvAdditions" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvAdditions_RowDataBound" OnRowUpdating="gvAdditions_RowUpdating" OnRowCancelingEdit="gvAdditions_RowCancelingEdit" OnRowEditing="gvAdditions_RowEditing">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. No">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox runat="server" ID="txtEditTitle" CssClass="form-control AphabetOnly" Text='<%# Eval("Title") %>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator31" ControlToValidate="txtEditTitle" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateu" ForeColor="Red" /></h4>

                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox runat="server" ID="txtTitle" CssClass="form-control AphabetOnly" Text='<%# Eval("Title") %>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="txtTitle" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" /></h4>

                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Type <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblType" Text='<%# Eval("Type") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList runat="server" ID="ddlEditType" data-plugin="select2" CssClass="form-control select2" Text='<%# Eval("Type") %>'>
                                                                    <asp:ListItem>Percentage</asp:ListItem>
                                                                    <asp:ListItem>Amount</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator87" ControlToValidate="ddlEditType" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateu" ForeColor="Red" /></h4>

                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="ddlType" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" /></h4>

                                                                <asp:DropDownList runat="server" ID="ddlType" CssClass="form-control select2" data-plugin="select2">
                                                                    <asp:ListItem>Percentage</asp:ListItem>
                                                                    <asp:ListItem>Amount</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Amount <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblAmount" Text='<%# Eval("Amount") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox runat="server" ID="txtEditAmount" CssClass="form-control" TextMode="Number" Text='<%# Eval("Amount") %>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator88" ControlToValidate="txtEditAmount" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateu" ForeColor="Red" /></h4>

                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox runat="server" ID="txtAmount" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" ControlToValidate="txtAmount" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" /></h4>

                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ShowHeader="false" FooterStyle-HorizontalAlign="Center">
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" ValidationGroup="btnValidateu" CommandName="Update" Text="Update"></asp:LinkButton>
                                                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEdit" CssClass="edit-class" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddAdditions" runat="server" CssClass="AD_stock form-control" Text="Add" ValidationGroup="btnValidatef" CausesValidation="true" OnClick="btnAddAdditions_Click"></asp:Button>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField FooterStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveAdditions_Command"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lblTotal" CssClass="total" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="tab-pane " id="2">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                                            <div class="content-header second_heading">
                                                <div class="container-fluid">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-6">
                                                            <h1 class="m-0 text-dark">Deductions</h1>
                                                        </div>
                                                        <!-- /.col -->

                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.container-fluid -->
                                            </div>
                                            <div class="">
                                                <asp:GridView ID="gvDeductions" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvDeductions_RowDataBound" OnRowUpdating="gvDeductions_RowUpdating" OnRowCancelingEdit="gvDeductions_RowCancelingEdit" OnRowEditing="gvDeductions_RowEditing">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. No">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <span style="color: red !important;">*</span> <span style="color: red !important;">*</span>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator40" ControlToValidate="txtEditTitled" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateDU" ForeColor="Red" />
                                                                <asp:TextBox runat="server" ID="txtEditTitled" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator45" ControlToValidate="txtTitled" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateDA" ForeColor="Red" />

                                                                <asp:TextBox runat="server" ID="txtTitled" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Type <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblTyped" Text='<%# Eval("Type") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator41" ControlToValidate="ddlEditTyped" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateDU" ForeColor="Red" />

                                                                <asp:DropDownList runat="server" ID="ddlEditTyped" data-plugin="select2" CssClass="select2 form-control" Text='<%# Eval("Type") %>'>
                                                                    <asp:ListItem>Percentage</asp:ListItem>
                                                                    <asp:ListItem>Amount</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator46" ControlToValidate="ddlTyped" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateDA" ForeColor="Red" />

                                                                <asp:DropDownList runat="server" ID="ddlTyped" data-plugin="select2" CssClass="select2 form-control">
                                                                    <asp:ListItem>Percentage</asp:ListItem>
                                                                    <asp:ListItem>Amount</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Amount <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblAmount" Text='<%# Eval("Amount") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator43" ControlToValidate="txtEditAmountd" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateDU" ForeColor="Red" />

                                                                <asp:TextBox runat="server" ID="txtEditAmountd" TextMode="Number" CssClass="form-control" Text='<%# Eval("Amount") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator47" ControlToValidate="txtAmountd" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateDA" ForeColor="Red" />

                                                                <asp:TextBox runat="server" ID="txtAmountd" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ShowHeader="false" FooterStyle-HorizontalAlign="Center">
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" ValidationGroup="btnValidateDU" CommandName="Update" Text="Update"></asp:LinkButton>
                                                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEdit" CssClass="edit-class" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddAdditions" runat="server" CssClass=" AD_stock form-control" Text="Add" ValidationGroup="btnDedFooter" CausesValidation="true" OnClick="btnAddDeductions_Click"></asp:Button>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField FooterStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveDeductions_Command"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lblTotal" CssClass="total" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>


                                <div class="tab-pane " id="3">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>

                                            <div class="content-header second_heading">
                                                <div class="container-fluid">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-6">
                                                            <h1 class="m-0 text-dark">Assets</h1>
                                                        </div>
                                                        <!-- /.col -->

                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.container-fluid -->
                                            </div>
                                            <div class="">
                                                <asp:GridView ID="gvAssets" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvAssets_RowDataBound" OnRowUpdating="gvAssets_RowUpdating" OnRowCancelingEdit="gvAssets_RowCancelingEdit" OnRowEditing="gvAssets_RowEditing">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. No">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator63" ControlToValidate="txtEditTitlea" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateAU" ForeColor="Red" />
                                                                <asp:TextBox runat="server" ID="txtEditTitlea" CssClass="form-control AphabetOnly" Text='<%# Eval("Title") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator66" ControlToValidate="txtTitlea" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateAF" ForeColor="Red" />
                                                                <asp:TextBox runat="server" ID="txtTitlea" CssClass="form-control AphabetOnly" Text='<%# Eval("Title") %>'></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Quantity <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblQty" Text='<%# Eval("Qty") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator64" ControlToValidate="txtEditQty" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateAU" ForeColor="Red" />
                                                                <asp:TextBox runat="server" ID="txtEditQty" TextMode="Number" CssClass="form-control" Text='<%# Eval("Qty") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator67" ControlToValidate="txtQty" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateAF" ForeColor="Red" />
                                                                <asp:TextBox runat="server" ID="txtQty" TextMode="Number" CssClass="form-control" Text='<%# Eval("Qty") %>'></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblDesc" Text='<%# Eval("Desc") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator65" ControlToValidate="txtEditDesc" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateAU" ForeColor="Red" />
                                                                <asp:TextBox runat="server" ID="txtEditDesc" CssClass="form-control" Text='<%# Eval("Desc") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator68" ControlToValidate="txtDesc" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateAF" ForeColor="Red" />
                                                                <asp:TextBox runat="server" ID="txtDesc" CssClass="form-control"></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ShowHeader="false">
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" ValidationGroup="btnValidateAU" Text="Update"></asp:LinkButton>
                                                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEdit" CssClass="edit-class" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddAsset" runat="server" CssClass=" AD_stock form-control" ValidationGroup="btnValidateAF" Text="Add" OnClick="btnAddAsset_Click"></asp:Button>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemove_Command"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>


                                <div class="tab-pane " id="4">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>

                                            <div class="content-header second_heading">
                                                <div class="container-fluid">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-6">
                                                            <h1 class="m-0 text-dark">Skill Set</h1>
                                                        </div>
                                                        <!-- /.col -->

                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.container-fluid -->
                                            </div>
                                            <div class="">
                                                <asp:GridView ID="gvSkillSet" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvSkillSet_RowDataBound" OnRowUpdating="gvSkillSet_RowUpdating" OnRowCancelingEdit="gvSkillSet_RowCancelingEdit" OnRowEditing="gvSkillSet_RowEditing">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. No">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description <span style='color:red !important;'>*</span>">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("Title") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator53" ControlToValidate="txtEditTitles" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateSSU" ForeColor="Red" />

                                                                <asp:TextBox runat="server" ID="txtEditTitles" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator50" ControlToValidate="txtTitles" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidateSSA" ForeColor="Red" />

                                                                <asp:TextBox runat="server" ID="txtTitles" CssClass="form-control" Text='<%# Eval("Title") %>'></asp:TextBox>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ShowHeader="false">
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" ValidationGroup="btnValidateSSU" Text="Update"></asp:LinkButton>
                                                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEdit" CssClass="edit-class" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>

                                                                <asp:Button ID="btnAddSkillSet" runat="server" CssClass="AD_stock form-control" Text="Add" ValidationGroup="btnValidateSSA" OnClick="btnAddSkillSet_Click"></asp:Button>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkRemove" CssClass="delete-class" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveSkillSet_Command"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>


                            </div>

                        </div>
                    </div>

                    <div class="clear-fix">&nbsp;</div>




                </section>

                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
        </style>
        <!-- Modal -->
    </form>
</body>
</html>
