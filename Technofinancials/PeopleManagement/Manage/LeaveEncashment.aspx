﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeaveEncashment.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.LeaveEncashment" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">

                <section class="app-content">
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/Manage_Leaves.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Leave Encashment</h3>
                        </div>
                        <div class="col-md-4">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                            <ContentTemplate>

                                <div class="col-sm-4">
                                    <div class="pull-right flex">
                                        <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note"><i class="fa fa-sticky-note-o"></i></button>

                                        <button class="tf-save-btn" "Review & Approve" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <button class="tf-save-btn" "Review" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                        <button class="tf-save-btn" "Approve" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                        <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" "Reject" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class" "Delete" CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                        <button class="tf-save-btn" "Dis Approve" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                        <button class="tf-save-btn" "Submit for Review" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                        <button class="tf-save-btn" "Reject & Disapprove" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>

                                        <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                        <a class="tf-back-btn" "Back" id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>

                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade" id="notes-modal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Notes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>

                    <div>
                        <%--  <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>--%>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h4>Select Employee
                                         <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlEmployees" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <asp:DropDownList ID="ddlEmployees" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h4>Select Leaves
                                         <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlLeaves" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <asp:DropDownList ID="ddlLeaves" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlLeaves_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                              <div class="col-sm-4">
                                <div class="form-group">
                                    <h4>No of Leaves
                                        <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtNoOfLeaves" ErrorMessage="*Required" Display="Dynamic"  ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input type="text" class="form-control"  id="txtNoOfLeaves" disabled runat="server" />

                                </div>
                            </div>
                        </div>
                        <%-- </ContentTemplate>
                   
                </asp:UpdatePanel>--%>
                        <div class="row">
                          

                            <div class="col-sm-4">
                              <div class="form-group">
                                    <h4>Basic Salary
                                        <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtBasicSalary" ErrorMessage="*Required" Display="Dynamic"  ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input type="text" class="form-control"  id="txtBasicSalary" disabled runat="server" />

                                </div>
                            </div>

                            <div class="col-sm-4">
                              <div class="form-group">
                                    <h4>Per Day Salary
                                        <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator31" ControlToValidate="txtPerDaySalary" ErrorMessage="*Required" Display="Dynamic"  ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input type="text" class="form-control"  id="txtPerDaySalary" disabled runat="server" />

                                </div>
                            </div>

                            <div class="col-sm-4">
                              <div class="form-group">
                                    <h4>Total Cash
                                        <span style="color: red !important;">*</span>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator32" ControlToValidate="txtTotalCash" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input type="text" class="form-control"  id="txtTotalCash" disabled runat="server" />

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>


                </section>

                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
</body>
</html>
