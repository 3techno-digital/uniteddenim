﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProvinentFund.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.ProvinentFund" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>

    
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="UpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Payroll Setup</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                    <button type="button" class="AD_btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note">Note </button>
                                    <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <asp:UpdatePanel ID="UpdPnl" runat="server">
                    <ContentTemplate>
                        <section class="app-content">
                            <div class="row">

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12 col-xs-12 form-group pl-0">
                                            <h4><strong>Provident Fund Ratio</strong></h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Company Ratio <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtCompanyRatio" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="number" class="form-control" name="companyRatio" id="txtCompanyRatio" runat="server" />

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Employee Ratio<span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtEmployeeRatio" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="number" class="form-control" name="employeeRatio" id="txtEmployeeRatio" runat="server" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12 col-xs-12 form-group pl-0">
                                            <h4><strong>IESSI Ratio</strong></h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Company Ratio <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtIESSICompanyRatio" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="number" class="form-control" name="companyRatio" id="txtIESSICompanyRatio" runat="server" />

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Max Salary<span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtIESSIEmployeeRatio" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="number" class="form-control" name="employeeRatio" id="txtIESSIEmployeeRatio" runat="server" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12 col-xs-12 form-group pl-0">
                                            <h4><strong>Over Time Ratio</strong></h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Working / Holidays <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtOverTimeSimple" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="number" class="form-control" name="companyRatio" id="txtOverTimeSimple" runat="server" />

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Working Holiday<span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtOverTimeSpecial" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="number" class="form-control" name="employeeRatio" id="txtOverTimeSpecial" runat="server" />

                                            </div>
                                        </div>
                                      </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-12 col-xs-12 form-group pl-0">
                                            <h4><strong>E.O.B.I Contribution</strong></h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Company Contribution <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtEOBICompanyRatio" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="number" class="form-control" name="companyRatio" id="txtEOBICompanyRatio" runat="server" />

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Employee Contribution<span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtEOBIEmployeeRatio" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="number" class="form-control" name="employeeRatio" id="txtEOBIEmployeeRatio" runat="server" />

                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                            <span>
                                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                            </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
		                            <div class="row">

                                        <!-- Modal -->
                                        <div class="modal fade M_set" id="notes-modal" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="m-0 text-dark">Notes</h1>
                                                        <div class="add_new">
                                                            <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                                            <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                                        </div>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>
                                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                        </p>
                                                        <p>
                                                            <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                        </p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
		                            </div>
	                            </div>
	                            
                                    
                                    </div>
                            
                            <div class="clearfix">&nbsp;</div>
                            <div class="clearfix">&nbsp;</div>

                            <div class="row">
                                <div class="col-lg-12">

                                   
                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv D_NONE" ClientIDMode="Static" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Company Investments">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCompanyAmount" Text='<%# Eval("CompanyAmount").ToString() == "" ? "0" : float.Parse(Eval("CompanyAmount").ToString()).ToString("#.###") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee Investments">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblEmployeeAmount" Text='<%# Eval("EmployeeAmount").ToString() == "" ? "0" : float.Parse(Eval("EmployeeAmount").ToString()).ToString("#.###") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Investments">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lbltotalAmount" Text='<%# Eval("TotalAmount").ToString() == "" ? "0" : float.Parse(Eval("TotalAmount").ToString()).ToString("#.###") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Withdraw">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblWithdraw" Text='<%# Eval("WithdrawAmount").ToString() == "" ? "0" : float.Parse(Eval("WithdrawAmount").ToString()).ToString("#.###") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remaining">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblRemaning" Text='<%# Eval("RemaningAmount").ToString() == "" ? "0" : float.Parse(Eval("RemaningAmount").ToString()).ToString("#.###") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>


                        </section>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->

        <%--</ContentTemplate>--%>
        <%--        <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
            </Triggers>--%>
        <%--</asp:UpdatePanel>--%>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    <!--========== check if this page is working -->
<style>
    @media only screen and (max-width: 1280px) and (min-width: 800px) {
        .form-group h4, label {
            font-size: 13px !important;
        }
    }
</style>
</body>


</html>