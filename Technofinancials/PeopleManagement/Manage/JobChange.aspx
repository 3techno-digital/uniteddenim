﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobChange.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.JobChange" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="upd1"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">
                <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>
                        <div class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <h1 class="m-0 text-dark">Employee Promotion</h1>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div style="text-align: right;">

                                            <%--<button type="button" class="AD_btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note">Note </button>--%>
                                            <button class="AD_btn" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                            <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                            <button class="AD_btn" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                            <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                            <button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                            <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                            <button class="AD_btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                            <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                            <a class="AD_btn" id="btnBack" runat="server">Back</a>

                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>
                        <!-- Modal -->
                        <div class="modal fade M_set" id="notes-modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="m-0 text-dark">Notes</h1>
                                        <div class="add_new">
                                            <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                            <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                        </p>
                                        <p>
                                            <textarea id="txtNotes2" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>
                </asp:UpdatePanel>

                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="col-sm-6">
                                            <h4>Employee<%--<span style="color: red !important;">*</span>--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlEmployees" InitialValue="0" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                            <asp:DropDownList ID="ddlEmployees" runat="server" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployees_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6">
                                                <h4>Promotion Date<%--<span style="color: red !important;">*</span>--%>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input type="text" class="form-control datetime-picker" placeholder="Date"  data-date-format="DD-MMM-YYYY" id="txtDate" runat="server" />
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 for="select2-demo-5" class="control-label">Job Description <%--<span style="color: red !important;">*</span>--%>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlJD" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            <asp:DropDownList ID="ddlJD" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlJD_SelectedIndexChanged">
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-6">
                                            <h4 for="select2-demo-5" class="control-label">Grade  <%--<span style="color: red !important;">*</span>--%>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="ddlGrades" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                            <asp:DropDownList ID="ddlGrades" runat="server" class="form-control select2" disabled="disabled" >
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4>Department<%--<span style="color: red !important;">*</span>--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDept" InitialValue="0" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                            <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control select2" disabled="disabled" ClientIDMode="Static" ></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4>Designation<%--<span style="color: red !important;">*</span>--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlDesg" InitialValue="0" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                            <asp:DropDownList ID="ddlDesg" runat="server" class="form-control select2" disabled="disabled">
                                            </asp:DropDownList>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <div class="col-sm-6">
                                    <h4>Basic Salary
                                    <%--<span style="color: red !important;">*</span>--%>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtBasicSalary" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                    <input class="form-control" id="txtBasicSalary" placeholder="Basic Salary" type="number" runat="server" onkeyup="calcSalaries();" />
                                </div>

                                <div class="col-sm-6">
                                    <h4>
                                        Maintenance Allowance
                                    </h4>
                                    <input class="form-control" placeholder="Maintenance Allowance" type="number" value="0" id="txtMaintenanceAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                                </div>
                                <div class="col-sm-6">
                                    <h4>
                                        Fuel Allowance</h4>
                                    <input class="form-control" placeholder="Fuel Allowance" type="number" value="0" id="txtFuelAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                                </div>

                                <div class="col-sm-6">
                                    <h4>
                                        E.O.B.I</h4>
                                    <input class="form-control" placeholder="E.O.B.I" type="number" value="0" id="txtEOBIAllowance" runat="server" clientidmode="static" onkeyup="calcSalaries();" />
                                </div>

                                <div class="col-sm-6">
                                    <h4>
                                        Over Time Rate
                                    </h4>
                                    <input class="form-control" placeholder="0" runat="server" type="number" id="txtOverTimeRate" value="0" />
                                </div>

                                <div class="col-sm-6" >
                                    <h4>
                                        Net Salary</h4>
                                    <input class="form-control" placeholder="Net Salary" disabled="disabled" type="number" id="txtNetSalary" value="0" runat="server" clientidmode="static" />
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>

                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-sm-12">
                                            <h4>Direct Reporting To</h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4>Department<%--<span style="color: red !important;">*</span>--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlDirectReportToDept" InitialValue="0" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                            <asp:DropDownList ID="ddlDirectReportToDept" runat="server" ClientIDMode="Static" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlDirectReportToDept_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4>Designation <%--<span style="color: red !important;">*</span>--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlDirectreportTo" InitialValue="0" ErrorMessage=" *" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator></h4>
                                            <asp:DropDownList ID="ddlDirectreportTo" runat="server" ClientIDMode="Static" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-sm-12">
                                            <h4>Indirect Reporting To</h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4>Department</h4>
                                            <asp:DropDownList ID="ddlInDirectReportToDept" runat="server" ClientIDMode="Static" CssClass="form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlInDirectReportToDept_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4>Designation</h4>
                                            <asp:DropDownList ID="ddlInDirectreportTo" runat="server" ClientIDMode="Static" CssClass="form-control select2" data-plugin="select2"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-12">
                                            <h4>Notes</h4>
                                            <textarea class="form-control" rows="5" id="txtNotes" runat="server" name="notes"></textarea>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->

        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>

            function calcSalaries() {
                $("#txtNetSalary").val('0');
                var MaintenanceAllowance = $("#txtMaintenanceAllowance").val() != "" ? $("#txtMaintenanceAllowance").val() : "0";
                var fuelAllowance = $("#txtFuelAllowance").val() != "" ? $("#txtFuelAllowance").val() : "0";
                var basicSalry = $("#txtBasicSalary").val() != "" ? $("#txtBasicSalary").val() : "0";
                var EOBIAllowance = $("#txtEOBIAllowance").val() != "" ? $("#txtEOBIAllowance").val() : "0";

                var netSalary = parseFloat(fuelAllowance) + parseFloat(basicSalry) + parseFloat(MaintenanceAllowance) - parseFloat(EOBIAllowance);
                $("#txtNetSalary").val(netSalary);

            }

        </script>

        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }

            div#exTab2 {
                padding: 0px 10px 0px 20px;
            }
        </style>
        <!-- Modal -->
    </form>
</body>
</html>
