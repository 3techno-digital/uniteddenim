﻿using QRCoder;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class Employee : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected string empPhoto
        {
            get
            {
                if (ViewState["empPhoto"] != null)
                {
                    return (string)ViewState["empPhoto"];
                }
                else
                {
                    return "";
                }
            }

            set
            {
                ViewState["empPhoto"] = value;
            }
        }
        protected int PrevParentShiftID
        {
            get
            {
                if (ViewState["PrevParentShiftID"] != null)
                {
                    return (int)ViewState["PrevParentShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrevParentShiftID"] = value;
            }
        }
        protected int PrevShiftID
        {
            get
            {
                if (ViewState["PrevShiftID"] != null)
                {
                    return (int)ViewState["PrevShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["PrevShiftID"] = value;
            }
        }
        protected int ShiftID
        {
            get
            {
                if (ViewState["ShiftID"] != null)
                {
                    return (int)ViewState["ShiftID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["ShiftID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    if (Session["ParentCompanyID"].ToString() == "28" || Session["CompanyID"].ToString() == "28")
                    {
                        divHouseRentAllowance.Attributes.Add("style", "display:block");
                        divUtility.Attributes.Add("style", "display:block");
                        divMobileAllowance.Attributes.Add("style", "display:block");
                        divCarAllowance.Attributes.Add("style", "display:block");
                        divTravelAllowance.Attributes.Add("style", "display:block");
                        //divOtherAllowance.Attributes.Add("style", "display:block");
                        divMedicalAllowance.Attributes.Add("style", "display:block");
                        divNetSalary.Attributes.Add("style", "display:block");
                        divNightFoodAllowance.Attributes.Add("style", "display:block");
                        divDeductPF.Attributes.Add("style", "display:block");

                        btnCalculator.Visible = true;
                    }
                    else
                    {
                        divMobileAllowance.Attributes.Add("style", "display:none");
                        divCarAllowance.Attributes.Add("style", "display:none");
                        divTravelAllowance.Attributes.Add("style", "display:none");
                        divNightFoodAllowance.Attributes.Add("style", "display:none");

                        btnCalculator.Visible = true;
                    }

                    shiftDaysDiv.Visible = false;
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    BindCOA();
                    BindCountriesDropDown();
                    BindStatesDropDown(166);
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employees";
                    bindJDs();
                    BindDepartmentDropDown(Convert.ToInt32(Session["CompanyID"]));
                    bindInDirectReportingPersonDropDown();
                    bindDirectReportingPersonDropDown();
                    bindDesignationsDropDown();

                    divAlertMsg.Visible = false;
                    ViewState["empPhoto"] = null;
                    
                    ViewState["dtSkillSet"] = null;
                    ViewState["showSkillSetFirstRow"] = null;
                    ViewState["skillSetSrNo"] = null;
                    dtSkillSet = createSkillSetTable();
                    BindSkillSetTable();

                    ViewState["dtCertification"] = null;
                    ViewState["showCertificationFirstRow"] = null;
                    ViewState["CertificationSrNo"] = null;
                    dtCertification = createCertificationTable();
                    BindCertificationTable();

                    ViewState["dtQualification"] = null;
                    ViewState["showQualificationFirstRow"] = null;
                    ViewState["QualificationSrNo"] = null;
                    dtQualification = createQualificationTable();
                    BindQualificationTable();

                    ViewState["dtEmployeeCareerTracking"] = null;
                    ViewState["showEmployeeCareerTrackingFirstRow"] = null;
                    ViewState["EmployeeCareerTrackingSrNo"] = null;

                    ViewState["dtExperience"] = null;
                    ViewState["showExperienceFirstRow"] = null;
                    ViewState["ExperienceSrNo"] = null;
                    dtExperience = createExperienceTable();
                    BindExperienceTable();

                    ViewState["dtGradeLeaves"] = null;
                    ViewState["showGLFirstRow"] = null;
                    ViewState["GLsrNo"] = null;

                    dtGradeLeaves = null;
                    dtGradeLeaves = createGreadLeavesTable();
                    showGLFirstRow = false;
                   
                    ViewState["dtGeneralKPIS"] = null;
                    ViewState["showFirstRow"] = null;
                    ViewState["srNo"] = null;

                    ViewState["dtJDAssets"] = null;
                    ViewState["showAssetsFirstRow"] = null;
                    ViewState["assetsSrNo"] = null;

                    ViewState["dtJDAdditions"] = null;
                    ViewState["showAdditionsFirstRow"] = null;
                    ViewState["AdditionsSrNo"] = null;

                    ViewState["dtJDDeductions"] = null;
                    ViewState["showDeductionsFirstRow"] = null;
                    ViewState["DeductionsSrNo"] = null;

                    BindShiftsDropDown(int.Parse(Session["CompanyID"].ToString()));

                    ViewState["FilesSrNo"] = null;
                    ViewState["dtFiles"] = null;

                    dtFiles = new DataTable();
                    dtFiles = createFiles();
                    BindFilesTable();
                    dtJDAdditions = createJDAdditions();
                    BindAdditionsTable();

                    dtJDDeductions = createJDDeductions();
                    BindDeductionsTable();

                    txtEmployeeCode.Disabled = true;
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    txtEmployeeCode.Value = objDB.GenerateEmployeeCode();
                    BindCitiesDropDown(0);
                    if (HttpContext.Current.Items["EmployeeID"] != null)
                    {
                        getEmployeeByID(Convert.ToInt32(HttpContext.Current.Items["EmployeeID"].ToString()));
                        CheckAccess();
                    }

                    ddlDesg.Enabled = false;
                    ddlCostCenter.Enabled = false;
                    ddlDept.Enabled = false;
                }

                //Page.ClientScript.GetPostBackEventReference(this, string.Empty);
                string ctrlName = Request.Params.Get("__EVENTTARGET");
                string ctrlArgs = Request.Params.Get("__EVENTARGUMENT");

                if (!String.IsNullOrEmpty(ctrlName) && ctrlName == "attachments")
                {
                    if (updAttachments.HasFile || updAttachments.HasFiles)
                    {
                        foreach (HttpPostedFile file in updAttachments.PostedFiles)
                        {
                            Random rand = new Random((int)DateTime.Now.Ticks);
                            int randnum = 0;

                            string fn = "";
                            string exten = "";
                            string destDir = Server.MapPath("~/assets/" + ctrlName + "/employees/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
                            randnum = rand.Next(1, 100000);
                            fn = Common.RemoveSpecialCharacter(ddlDocType.SelectedItem.Value).ToLower().Replace(" ", "-") + "_" + randnum;

                            if (!Directory.Exists(destDir))
                            {
                                Directory.CreateDirectory(destDir);
                            }
                            string fname = Path.GetFileName(file.FileName);
                            exten = Path.GetExtension(file.FileName);
                            file.SaveAs(destDir + fn + exten);
                            DataRow dr = dtFiles.NewRow();
                            dr[0] = FilesSrNo.ToString();
                            dr[1] = ddlDocType.SelectedValue;
                            dr[2] = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/" + ctrlName + "/employees/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;

                            dtFiles.Rows.Add(dr);
                            dtFiles.AcceptChanges();

                            FilesSrNo += 1;
                            BindFilesTable();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected string GeneratedQRcodeofEmployee(string BarCode)
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            int randnum = 0;
            randnum = rand.Next(1, 100000);

            string fn = Common.RemoveSpecialCharacter(BarCode).Replace(" ", "").ToLower() + "_" + randnum;
            string exten = ".jpg";
            string Path = "";
            string destDir = HttpContext.Current.Server.MapPath("~/assets/images/employees/QRcodes/");

            if (!Directory.Exists(destDir))
            {
                Directory.CreateDirectory(destDir);
            }

            Path = destDir + fn + exten;

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(BarCode, QRCodeGenerator.ECCLevel.Q);
            System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
            imgBarCode.Height = 300;
            imgBarCode.Width = 400;

            using (Bitmap bitMap = qrCode.GetGraphic(20))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();
                    using (MemoryStream ms1 = new MemoryStream(Convert.FromBase64String(Convert.ToBase64String(byteImage))))
                    {
                        using (Bitmap bm2 = new Bitmap(ms1))
                        {
                            bm2.Save(Path);
                        }
                    }
                }
            }

            return Path;
        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = true;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "Employees";
                objDB.PrimaryColumnnName = "EmployeeID";
                objDB.PrimaryColumnValue = HttpContext.Current.Items["EmployeeID"].ToString();
                objDB.DocName = "EmployeeLists";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;
                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindDepartmentDropDown(int companyID)
        {
            try
            {
                objDB.CompanyID = companyID;
                ddlDept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                ddlDept.DataTextField = "DeptName";
                ddlDept.DataValueField = "DeptID";
                ddlDept.DataBind();
                ddlDept.Items.Insert(0, new ListItem("--- Select ---", "0"));

                ddlCostCenter.DataSource = objDB.GetAllCostCentersByCompanyID(ref errorMsg);
                ddlCostCenter.DataTextField = "CostCenterName";
                ddlCostCenter.DataValueField = "CostCenterID";
                ddlCostCenter.DataBind();
                ddlCostCenter.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void getGradeLeavesByGradeID(int gradeID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.GradeID = gradeID;
                dt = objDB.GetAllLeavesByGradeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtGradeLeaves.Rows[0][0].ToString() == "")
                        {
                            dtGradeLeaves.Rows[0].Delete();
                            dtGradeLeaves.AcceptChanges();
                            showGLFirstRow = true;
                        }
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtGradeLeaves.Rows.Add(new object[] {
                            dt.Rows[i]["SrNo"],
                            dt.Rows[i]["LeaveTitle"],
                            dt.Rows[i]["NoOfLeaves"],
                            "0"

                        });
                        }
                        showGLFirstRow = true;
                        GLsrNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                    }
                    else
                    {
                        GLsrNo = 1;
                        showGLFirstRow = false;
                        dtGradeLeaves = null;
                    }
                }
                else
                {
                    GLsrNo = 1;
                    showGLFirstRow = false;
                    dtGradeLeaves = null;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
        private void BindCountriesDropDown()
        {
            try
            {
                CheckSessions();
                ddlCountries.DataSource = objDB.GetAllCounties(ref errorMsg);
                ddlCountries.DataTextField = "Name";
                ddlCountries.DataValueField = "ID";
                ddlCountries.DataBind();
                ddlCountries.Items.Insert(0, new ListItem("--- Select Country ---", "0"));
                ddlCountries.SelectedValue = "166";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindStatesDropDown(int CountryID)
        {
            try
            {
                objDB.CountryID = CountryID;
                ddlStates.DataSource = objDB.GetAllStatesByCountryID(ref errorMsg);
                ddlStates.DataTextField = "Name";
                ddlStates.DataValueField = "ID";
                ddlStates.DataBind();
                ddlStates.Items.Insert(0, new ListItem("--- Select State ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindCitiesDropDown(int StateID)
        {
            try
            {
                ddlCities.DataSource = new DataTable();
                
                if (StateID > 0)
                {
                    objDB.StateID = StateID;
                    ddlCities.DataSource = objDB.GetAllCitiesByStateID(ref errorMsg);
                    ddlCities.DataTextField = "Name";
                    ddlCities.DataValueField = "Name";
                }

                ddlCities.DataBind();
                ddlCities.Items.Insert(0, new ListItem("--- Select City ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                BindCitiesDropDown(Convert.ToInt32(ddlStates.SelectedItem.Value));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                BindStatesDropDown(Convert.ToInt32(ddlCountries.SelectedItem.Value));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void clearFields()
        {
            CheckSessions();

            txtWDID.Value = "";
            txtEOBIAllowance.Value = "0";
            txtTax.Value = "0";
            txtFoodAllowance.Value = "0";
            txtNightFoodAllowance.Value = "0";
            ddlExpCOA.SelectedValue = "0";
            ddlCOA.SelectedValue = "0";
            ddlEmployeeCOA.SelectedValue = "0";
            txtOverTimeRate.Value = "1.5";
            txtMaintenanceAllowance.Value = "0";
            txtFuelAllowance.Value = "0";
            txtEmployeeCode.Value = "";
            txtCNIC.Value = "";
            txtFirstName.Value = "";
            txtMiddleName.Value = "";
            txtLastName.Value = "";
            ddlGender.SelectedValue = "0";
            txtContactNo.Value = "";
            txtEmail.Text = "";
            ddlEmploymentType.SelectedValue = "0";
            txtDOJ.Value = "";
            ddlDept.SelectedValue = "0";
            ddlDesg.SelectedValue = "0";
            ddlJD.SelectedValue = "0";
            txtBasicSalary.Value = "0";
            txtFatherName.Value = "";
            txtDOB.Value = "";
            txtSecEmail.Value = "";
            txtPlaceOfPosting.Value = "";
            txtAddressLine1.Value = "";
            txtAddressLine2.Value = "";
            txtZIPCode.Value = "";
            txtEmergencyPerson.Value = "";
            txtEmergencyPhone.Value = "";
            txtBankName.Value = "";
            txtBranchName.Value = "";
            txtAccountNo.Value = "";
            txtAccountTitle.Value = "";
            ddlAnnualCalendar.SelectedValue = "PK";
            autoattendance.Checked = false;
            chkDeductPF.Checked = false;
            chkIsOverTime.Checked = false;

            bindDirectReportingPersonDropDown();
            bindInDirectReportingPersonDropDown();

            ddlDirectReportingPerson.SelectedValue = "0";
            ddlInDirectReportingPerson.SelectedValue = "0";
            txtHouseRentAllowance.Value = "0";
            txtUtility.Value = "0";
            txtMobileAllowance.Value = "0";
            txtCarAllowance.Value = "0";
            txtTravelAllowance.Value = "0";
            txtOtherAllowance.Value = "0";
            txtMedicalAllowance.Value = "0";
            txtNetSalary.Value = "0";
            ViewState["empPhoto"] = null;
            ViewState["dtSkillSet"] = null;
            ViewState["showSkillSetFirstRow"] = null;
            ViewState["skillSetSrNo"] = null;

            dtSkillSet = createSkillSetTable();

            BindSkillSetTable();

            ViewState["dtCertification"] = null;
            ViewState["showCertificationFirstRow"] = null;
            ViewState["CertificationSrNo"] = null;
            dtCertification = createCertificationTable();
            BindCertificationTable();

            ViewState["dtQualification"] = null;
            ViewState["showQualificationFirstRow"] = null;
            ViewState["QualificationSrNo"] = null;
            dtQualification = createQualificationTable();
            BindQualificationTable();

            ViewState["dtEmployeeCareerTracking"] = null;
            ViewState["showEmployeeCareerTrackingFirstRow"] = null;
            ViewState["EmployeeCareerTrackingSrNo"] = null;

            ViewState["dtExperience"] = null;
            ViewState["showExperienceFirstRow"] = null;
            ViewState["ExperienceSrNo"] = null;
            dtExperience = createExperienceTable();
            BindExperienceTable();

            ViewState["dtGradeLeaves"] = null;
            ViewState["showGLFirstRow"] = null;
            ViewState["GLsrNo"] = null;
            dtGradeLeaves = createGreadLeavesTable();
            
            ViewState["dtGeneralKPIS"] = null;
            ViewState["showFirstRow"] = null;
            ViewState["srNo"] = null;
            dtGeneralKPIS = createKPIsTable();
            BindKPIsTable();

            ViewState["dtJDAdditions"] = null;
            ViewState["showAdditionsFirstRow"] = null;
            ViewState["AdditionsSrNo"] = null;
            dtJDAdditions = createJDAdditions();
            BindAdditionsTable();

            ViewState["dtJDDeductions"] = null;
            ViewState["showDeductionsFirstRow"] = null;
            ViewState["DeductionsSrNo"] = null;
            dtJDDeductions = createJDDeductions();
            BindDeductionsTable();

            BindShiftsDropDown(int.Parse(Session["CompanyID"].ToString()));

            ViewState["FilesSrNo"] = null;
            ViewState["dtFiles"] = null;

            dtFiles = new DataTable();
            dtFiles = createFiles();
            BindFilesTable();

            txtEmployeeCode.Disabled = true;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            txtEmployeeCode.Value = objDB.GenerateEmployeeCode();
        }

        private void getEmployeeByID(int EmpID)
        {
            try
            {
                objDB.EmployeeID = EmpID;
                DataTable dt = objDB.GetEmployeeDetailsByID(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtTotalDeduction.Value = (double.Parse(dt.Rows[0]["EOBI"].ToString()) + double.Parse(dt.Rows[0]["IESSI"].ToString())).ToString();
                        txtIESSI.Value = dt.Rows[0]["IESSI"].ToString();
                        txtWDID.Value = dt.Rows[0]["WDID"].ToString();
                        txtDivision.Value = dt.Rows[0]["Division"].ToString();
                        txtTeam.Value = dt.Rows[0]["Team"].ToString();
                        empPhoto = dt.Rows[0]["EmployeePhoto"].ToString();
                        imgPhoto.Src = empPhoto;
                        txtEmployeeCode.Value = dt.Rows[0]["EmployeeCode"].ToString();
                        txtCNIC.Value = dt.Rows[0]["CNIC"].ToString();
                        txtFirstName.Value = dt.Rows[0]["EmployeeFirstName"].ToString();
                        txtMiddleName.Value = dt.Rows[0]["EmployeeMiddleName"].ToString();
                        txtLastName.Value = dt.Rows[0]["EmployeeLastName"].ToString();
                        ddlGender.SelectedValue = dt.Rows[0]["Gender"].ToString();
                        txtContactNo.Value = dt.Rows[0]["ContactNo"].ToString();
                        txtEmail.Text = dt.Rows[0]["Email"].ToString();
                        ddlEmploymentType.SelectedValue = dt.Rows[0]["EmploymentType"].ToString();
                        txtDOJ.Value = (dt.Rows[0]["DateOfJoining"].ToString() == "") ? "" : DateTime.Parse(dt.Rows[0]["DateOfJoining"].ToString()).ToString("dd-MMM-yyyy");
                        ddlDept.SelectedValue = dt.Rows[0]["DeptID"].ToString();
                        ddlDesg.SelectedValue = dt.Rows[0]["DesgID"].ToString();
                        ddlJD.SelectedValue = dt.Rows[0]["TitleID"].ToString();
                        txtBasicSalary.Value = dt.Rows[0]["BasicSalary"].ToString();
                        txtFatherName.Value = dt.Rows[0]["FatherName"].ToString();
                        txtDOB.Value = Convert.ToDateTime(dt.Rows[0]["DOB"]).ToString("dd-MMM-yyyy");
                        txtSecEmail.Value = dt.Rows[0]["SecEmail"].ToString();
                        txtPlaceOfPosting.Value = dt.Rows[0]["PlaceOfPost"].ToString();
                        ddlCostCenter.SelectedValue = dt.Rows[0]["CostDeductor"].ToString();
                        txtFuelAllowance.Value = dt.Rows[0]["FuelAllowance"].ToString();
                        txtMobileAllowance.Value = dt.Rows[0]["MobileAllowance"].ToString();
                        txtCarAllowance.Value = dt.Rows[0]["CarAllowance"].ToString();
                        txtTravelAllowance.Value = dt.Rows[0]["TravelAllowance"].ToString();
                        txtFoodAllowance.Value = dt.Rows[0]["FoodAllowance"].ToString();
                        txtOtherAllowance.Value = dt.Rows[0]["OtherAllowance"].ToString();
                        txtMedicalAllowance.Value = dt.Rows[0]["MedicalAllowance"].ToString();
                        txtGrossSalary.Value = dt.Rows[0]["GrossAmount"].ToString();
                        txtNetSalary.Value = dt.Rows[0]["NetSalary"].ToString();
                        txtHouseRentAllowance.Value = dt.Rows[0]["HouseAllownace"].ToString();
                        txtUtility.Value = dt.Rows[0]["UtitlityAllowance"].ToString();
                        ddlJD.SelectedValue = dt.Rows[0]["CostDeductor"].ToString();
                        ddlDesg.SelectedValue = dt.Rows[0]["CostDeductor"].ToString();
                        ddlDept.SelectedValue = dt.Rows[0]["CostDeductor"].ToString();

                        txtNightFoodAllowance.Value = dt.Rows[0]["NightFoodAllowance"].ToString();
                        txtOverTimeRate.Value = dt.Rows[0]["OverTimeRate"].ToString();
                        txtEOBIAllowance.Value = dt.Rows[0]["EOBI"].ToString();
                        txtTax.Value = dt.Rows[0]["EmployeeTax"].ToString();
                        txtMaintenanceAllowance.Value = dt.Rows[0]["MaintenanceAllowance"].ToString();
                        ddlCOA.SelectedValue = dt.Rows[0]["COA_ID"].ToString();
                        ddlCOA_SelectedIndexChanged(null, null);

                        ddlExpCOA.SelectedValue = dt.Rows[0]["PayrolllExpCOA"].ToString();
                        ddlExpCOA_SelectedIndexChanged(null, null);

                        ddlEmployeeCOA.SelectedValue = dt.Rows[0]["EmployeeCOA"].ToString();
                        ddlEmployeeCOA_SelectedIndexChanged(null, null);

                        ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));
                        int countriesIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));
                        if (countriesIndex == -1)
                            countriesIndex = 0;
                        BindStatesDropDown(Convert.ToInt32(ddlCountries.Items[countriesIndex].Value));

                        ddlStates.SelectedIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["State"].ToString()));
                        int statesIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["State"].ToString()));
                        if (statesIndex == -1)
                            statesIndex = 0;

                        BindCitiesDropDown(Convert.ToInt32(ddlStates.Items[statesIndex].Value));

                        ddlCities.SelectedIndex = ddlCities.Items.IndexOf(ddlCities.Items.FindByText(dt.Rows[0]["City"].ToString()));
                        txtAddressLine1.Value = dt.Rows[0]["AddressLine1"].ToString();
                        txtAddressLine2.Value = dt.Rows[0]["AddressLine2"].ToString();
                        txtZIPCode.Value = dt.Rows[0]["ZIPCode"].ToString();

                        txtEmergencyPerson.Value = dt.Rows[0]["EmergencyContactPerson"].ToString();
                        txtEmergencyPhone.Value = dt.Rows[0]["EmergencyContactNo"].ToString();

                        txtBankName.Value = dt.Rows[0]["BankName"].ToString();
                        txtBranchName.Value = dt.Rows[0]["BranchName"].ToString();
                        txtAccountNo.Value = dt.Rows[0]["AccountNo"].ToString();
                        txtAccountTitle.Value = dt.Rows[0]["AccountTitle"].ToString();

                        bindInDirectReportingPersonDropDown();
                        bindDirectReportingPersonDropDown();
                        chkDeductPF.Checked = getCheckBoxValue(dt.Rows[0]["DeductPF"].ToString());
                        chkIsOverTime.Checked = getCheckBoxValue(dt.Rows[0]["isOverTime"].ToString());

                        ddlDirectReportingPerson.SelectedIndex = ddlDirectReportingPerson.Items.IndexOf(ddlDirectReportingPerson.Items.FindByValue(dt.Rows[0]["DirectReportingPerson"].ToString()));
                        ddlInDirectReportingPerson.SelectedIndex = ddlInDirectReportingPerson.Items.IndexOf(ddlInDirectReportingPerson.Items.FindByValue(dt.Rows[0]["DirectReportingPerson"].ToString()));

                        ddlAnnualCalendar.SelectedValue = dt.Rows[0]["AnnualCalendar"].ToString();
                        autoattendance.Checked = dt.Rows[0]["autoattendance"].ToString() == "True" ? true : false;
                        AdditionsSrNo = 1;
                        showAdditionsFirstRow = false;
                        dtJDAdditions = createJDAdditions();
                        BindAdditionsTable();

                        DeductionsSrNo = 1;
                        showDeductionsFirstRow = false;
                        dtJDDeductions = createJDAdditions();

                        getGeneralKPISByEmployeeID(EmpID);
                        getExperienceByEmployeeID(EmpID);
                        getQualificationByEmployeeID(EmpID);
                        getSkillSetByEmployeeID(EmpID);
                        getCertificationByEmployeeID(EmpID);
                        getEmployeeAdditionsByEmployeeID(EmpID);
                        getEmployeeDeductionsByEmployeeID(EmpID);
                        getAttachementsByEmployeeID(EmpID);
                        getShiftsByEmployeeID(EmpID);
                        getEmployeeLeavesByEmployeeID(EmpID);
                       
                        objDB.DocID = EmpID;
                        objDB.DocType = "EmployeeLists";
                        ltrNotesTable.Text = objDB.GetDocNotes();
                    }
                }

                Common.addlog("View", "HR", "Employee \"" + txtFirstName.Value + " " + txtMiddleName.Value + " " + txtLastName.Value + "\" Viewed", "Employees", objDB.EmployeeID);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getAttachementsByEmployeeID(int employeeID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.EmployeeID = employeeID;
                dt = objDB.GetAllEmployeeAttachmentByEmployeeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtFiles.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["AttachmentTitle"],
                            dt.Rows[i]["AttachmentPath"]
                        });
                        }
                    }
                }

                BindFilesTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private bool getCheckBoxValue(string val)
        {
            if (val == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void AddShiftDays()
        {
            #region Monday
            objDB.isOffDayOT = chkMondayOT.Checked;
            objDB.isSpecial = chkMondayIsSpecial.Checked;
            objDB.StartTime = txtMONStartTime.Value;
            objDB.LateInTime = txtMONLateInTime.Value;
            objDB.TimeInExemption = txtMonTimeInExemption.Value;
            objDB.BreakStartTime = txtMONBreakStartTime.Value;
            objDB.BreakEndTime = txtMONBreakEndTime.Value;
            objDB.HalfDayStart = txtMONBreakStartTime.Value;
            objDB.EarlyOut = txtMONEarlyOut.Value;
            objDB.EndTime = txtMONEndTime.Value;
            objDB.isNightShift = chkMonNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Monday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkMonday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Tuesday

            objDB.isOffDayOT = chkTuesdayOT.Checked;
            objDB.isSpecial = chkTuesdayIsSpecial.Checked;
            objDB.StartTime = txtTUEStartTime.Value;
            objDB.LateInTime = txtTUELateInTime.Value;
            objDB.TimeInExemption = txtTueTimeInExemption.Value;
            objDB.BreakStartTime = txtTUEBreakStartTime.Value;
            objDB.HalfDayStart = txtTUEBreakStartTime.Value;
            objDB.BreakEndTime = txtTUEBreakEndTime.Value;
            objDB.EarlyOut = txtTUEEarlyOut.Value;
            objDB.EndTime = txtTUEEndTime.Value;
            objDB.isNightShift = chkTUENightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Tuesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkTuesday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Wednesday
            objDB.isOffDayOT = chkWednesdayOT.Checked;
            objDB.isSpecial = chkWednesdayIsSpecial.Checked;
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.TimeInExemption = txtWedTimeInExemption.Value;
            objDB.LateInTime = txtWEDLateInTime.Value;
            objDB.BreakStartTime = txtWEDBreakStartTime.Value;
            objDB.HalfDayStart = txtWEDBreakStartTime.Value;
            objDB.BreakEndTime = txtWEDBreakEndTime.Value;
            objDB.EarlyOut = txtWEDEarlyOut.Value;
            objDB.EndTime = txtWEDEndTime.Value;
            objDB.isNightShift = chkWEDNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Wednesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkWednesdat.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Thursday
            objDB.isOffDayOT = chkThursdayOT.Checked;
            objDB.isSpecial = chkThursdayIsSpecial.Checked;
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.TimeInExemption = txtThursTimeInExemption.Value;
            objDB.LateInTime = txtTHULateInTime.Value;
            objDB.BreakStartTime = txtTHUBreakStartTime.Value;
            objDB.HalfDayStart = txtTHUBreakStartTime.Value;
            objDB.BreakEndTime = txtTHUBreakEndTime.Value;
            objDB.EarlyOut = txtTHUEarlyOut.Value;
            objDB.EndTime = txtTHUEndTime.Value;
            objDB.isNightShift = chkTHUNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Thursday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkThursday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Friday
            objDB.isOffDayOT = chkFridayOT.Checked;
            objDB.isSpecial = chkFridayIsSpecial.Checked;
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.TimeInExemption = txtFriTimeInExemption.Value;
            objDB.LateInTime = txtFRILateInTime.Value;
            objDB.BreakStartTime = txtFRIBreakStartTime.Value;
            objDB.HalfDayStart = txtFRIBreakStartTime.Value;
            objDB.BreakEndTime = txtFRIBreakEndTime.Value;
            objDB.EarlyOut = txtFRIEarlyOut.Value;
            objDB.EndTime = txtFRIEndTime.Value;
            objDB.isNightShift = chkFRINightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Friday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkFriday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Saturday
            objDB.isOffDayOT = chkSaturdayOT.Checked;
            objDB.isSpecial = chkSaturdayIsSpecial.Checked;
            objDB.StartTime = txtSATStartTime.Value;
            objDB.LateInTime = txtSATLateInTime.Value;
            objDB.TimeInExemption = txtSatTimeInExemption.Value;
            objDB.BreakStartTime = txtSATBreakStartTime.Value;
            objDB.HalfDayStart = txtSATBreakStartTime.Value;
            objDB.BreakEndTime = txtSATBreakEndTime.Value;
            objDB.EarlyOut = txtSATEarlyOut.Value;
            objDB.EndTime = txtSATEndTime.Value;
            objDB.isNightShift = chkSATNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Saturday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSaturday.Checked;

            objDB.AddWorkingShiftsDays2();
            #endregion

            #region Sunday
            objDB.isOffDayOT = chkSundayOT.Checked;
            objDB.isSpecial = chkSundayIsSpecial.Checked;
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.LateInTime = txtSUNLateInTime.Value;
            objDB.TimeInExemption = txtSunTimeInExemption.Value;
            objDB.BreakStartTime = txtSUNBreakStartTime.Value;
            objDB.HalfDayStart = txtSUNBreakStartTime.Value;
            objDB.BreakEndTime = txtSUNBreakEndTime.Value;
            objDB.EarlyOut = txtSUNEarlyOut.Value;
            objDB.EndTime = txtSUNEndTime.Value;
            objDB.isNightShift = chkSUNNightShift.Checked;
            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Sunday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSunday.Checked;

            objDB.AddWorkingShiftsDays2();

            #endregion

        }

        private string CheckShiftDays()
        {
            string res = "";
            #region Monday
            objDB.StartTime = txtMONStartTime.Value;
            objDB.LateInTime = txtMONLateInTime.Value;
            objDB.BreakStartTime = txtMONBreakStartTime.Value;
            objDB.BreakEndTime = txtMONBreakEndTime.Value;
            objDB.HalfDayStart = txtMONHalfDayStart.Value;
            objDB.EarlyOut = txtMONEarlyOut.Value;
            objDB.EndTime = txtMONEndTime.Value;

            objDB.isNightShift = chkMonNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Monday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkMonday.Checked;


            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Tuesday

            objDB.StartTime = txtTUEStartTime.Value;
            objDB.LateInTime = txtTUELateInTime.Value;
            objDB.BreakStartTime = txtTUEBreakStartTime.Value;
            objDB.BreakEndTime = txtTUEBreakEndTime.Value;
            objDB.HalfDayStart = txtTUEHalfDayStart.Value;
            objDB.EarlyOut = txtTUEEarlyOut.Value;
            objDB.EndTime = txtTUEEndTime.Value;

            objDB.isNightShift = chkTUENightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Tuesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkTuesday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Wednesday
            objDB.StartTime = txtWEDStartTime.Value;
            objDB.LateInTime = txtWEDLateInTime.Value;
            objDB.BreakStartTime = txtWEDBreakStartTime.Value;
            objDB.BreakEndTime = txtWEDBreakEndTime.Value;
            objDB.HalfDayStart = txtWEDHalfDayStart.Value;
            objDB.EarlyOut = txtWEDEarlyOut.Value;
            objDB.EndTime = txtWEDEndTime.Value;

            objDB.isNightShift = chkWEDNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Wednesday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkWednesdat.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Thursday
            objDB.StartTime = txtTHUStartTime.Value;
            objDB.LateInTime = txtTHULateInTime.Value;
            objDB.BreakStartTime = txtTHUBreakStartTime.Value;
            objDB.BreakEndTime = txtTHUBreakEndTime.Value;
            objDB.HalfDayStart = txtTHUHalfDayStart.Value;
            objDB.EarlyOut = txtTHUEarlyOut.Value;
            objDB.EndTime = txtTHUEndTime.Value;

            objDB.isNightShift = chkTHUNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Thursday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkThursday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Friday
            objDB.StartTime = txtFRIStartTime.Value;
            objDB.LateInTime = txtFRILateInTime.Value;
            objDB.BreakStartTime = txtFRIBreakStartTime.Value;
            objDB.BreakEndTime = txtFRIBreakEndTime.Value;
            objDB.HalfDayStart = txtFRIHalfDayStart.Value;
            objDB.EarlyOut = txtFRIEarlyOut.Value;
            objDB.EndTime = txtFRIEndTime.Value;

            objDB.isNightShift = chkFRINightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Friday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkFriday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Saturday
            objDB.StartTime = txtSATStartTime.Value;
            objDB.LateInTime = txtSATLateInTime.Value;
            objDB.BreakStartTime = txtSATBreakStartTime.Value;
            objDB.BreakEndTime = txtSATBreakEndTime.Value;
            objDB.HalfDayStart = txtSATHalfDayStart.Value;
            objDB.EarlyOut = txtSATEarlyOut.Value;
            objDB.EndTime = txtSATEndTime.Value;

            objDB.isNightShift = chkSATNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Saturday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSaturday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }
            #endregion

            #region Sunday
            objDB.StartTime = txtSUNStartTime.Value;
            objDB.LateInTime = txtSUNLateInTime.Value;
            objDB.BreakStartTime = txtSUNBreakStartTime.Value;
            objDB.BreakEndTime = txtSUNBreakEndTime.Value;
            objDB.HalfDayStart = txtSUNHalfDayStart.Value;
            objDB.EarlyOut = txtSUNEarlyOut.Value;
            objDB.EndTime = txtSUNEndTime.Value;

            objDB.isNightShift = chkSUNNightShift.Checked;

            objDB.WorkingShiftID = ShiftID;
            objDB.ShiftDayName = "Sunday";
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.isExpectedZero = !chkSunday.Checked;

            res = objDB.CheckWorkingShiftsHours();

            if (res != "Ok")
            {
                return res;
            }

            #endregion

            return "Ok";
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                int employeeID = 0;
                string res = "";

                objDB.Hours = txtHours.Value;
                res = CheckShiftDays();

                if (res == "Ok")
                {
                    res = "";
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Working hours did not match";
                    return;
                }

                if (updPhoto.HasFile)
                {
                    Random rand = new Random((int)DateTime.Now.Ticks);
                    int randnum = 0;

                    string fn = "";
                    string exten = "";

                    string destDir = Server.MapPath("~/assets/images/employees/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
                    randnum = rand.Next(1, 100000);
                    fn = Common.RemoveSpecialCharacter(txtCNIC.Value).ToLower().Replace(" ", "-") + "_" + randnum;

                    if (!Directory.Exists(destDir))
                    {
                        Directory.CreateDirectory(destDir);
                    }

                    string fname = Path.GetFileName(updPhoto.PostedFile.FileName);
                    exten = Path.GetExtension(updPhoto.PostedFile.FileName);
                    updPhoto.PostedFile.SaveAs(destDir + fn + exten);

                    empPhoto = "https://" + Request.ServerVariables["SERVER_NAME"] + "/assets/images/employees/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
                }

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.AnnualCalendar = ddlAnnualCalendar.SelectedValue;
                objDB.autoattendance = autoattendance.Checked == true ? true : false;
                objDB.EmployeeCode = txtEmployeeCode.Value;
                objDB.NodeID = Convert.ToInt32(ddlJD.SelectedItem.Value);
                objDB.EmployeeFName = txtFirstName.Value;
                objDB.EmployeeMName = txtMiddleName.Value;
                objDB.EmployeeLName = txtLastName.Value;
                objDB.EmployeePhoto = empPhoto;
                objDB.CNIC = txtCNIC.Value;
                objDB.Gender = ddlGender.SelectedValue;
                objDB.DateOfJoining = txtDOJ.Value;
                objDB.ContactNo = txtContactNo.Value;
                objDB.Email = txtEmail.Text;
                objDB.CountryName = ddlCountries.SelectedItem.Text;
                objDB.StateName = ddlStates.SelectedItem.Text;
                objDB.CityName = ddlCities.SelectedItem.Text;
                objDB.AddressLine1 = txtAddressLine1.Value;
                objDB.AddressLine2 = txtAddressLine2.Value;
                objDB.ZIPCode = txtZIPCode.Value;
                objDB.BankName = txtBankName.Value;
                objDB.BranchName = txtBranchName.Value;
                objDB.AccountTitle = txtAccountTitle.Value;
                objDB.AccountNo = txtAccountNo.Value;
                objDB.EmploymentType = ddlEmploymentType.SelectedItem.Value;
                objDB.FatherName = txtFatherName.Value;
                objDB.DOB = txtDOB.Value;
                objDB.SecEmail = txtSecEmail.Value;
                objDB.PlaceOfPosting = txtPlaceOfPosting.Value;
                objDB.CostDeductor = ddlCostCenter.SelectedValue;
                objDB.FuelAllowance = float.Parse((txtFuelAllowance.Value == "") ? "0" : txtFuelAllowance.Value);
                objDB.MobileAllowance = float.Parse((txtMobileAllowance.Value == "") ? "0" : txtMobileAllowance.Value);
                objDB.CarAllowance = float.Parse((txtCarAllowance.Value == "") ? "0" : txtCarAllowance.Value);
                objDB.TravelAllowance = float.Parse((txtTravelAllowance.Value == "") ? "0" : txtTravelAllowance.Value);
                objDB.FoodAllowance = float.Parse((txtFoodAllowance.Value == "") ? "0" : txtFoodAllowance.Value);
                objDB.OtherAllowance = float.Parse((txtOtherAllowance.Value == "") ? "0" : txtOtherAllowance.Value);
                objDB.MedicalAllowance = float.Parse((txtMedicalAllowance.Value == "") ? "0" : txtMedicalAllowance.Value);
                objDB.GrossAmount = float.Parse((txtGrossSalary.Value == "") ? "0" : txtGrossSalary.Value);
                objDB.NetSalary = float.Parse((txtNetSalary.Value == "") ? "0" : txtNetSalary.Value);
                objDB.HouseAllownace = float.Parse((txtHouseRentAllowance.Value == "") ? "0" : txtHouseRentAllowance.Value);
                objDB.UtitlityAllowance = float.Parse((txtUtility.Value == "") ? "0" : txtUtility.Value);
                objDB.MaintenanceAllowance = float.Parse((txtMaintenanceAllowance.Value == "") ? "0" : txtMaintenanceAllowance.Value);
                objDB.OverTimeRate = float.Parse((txtOverTimeRate.Value == "") ? "0" : txtOverTimeRate.Value);
                objDB.NightFoodAllowance = float.Parse((txtNightFoodAllowance.Value == "") ? "0" : txtNightFoodAllowance.Value);
                objDB.EOBI = float.Parse((txtEOBIAllowance.Value == "") ? "0" : txtEOBIAllowance.Value);
                objDB.EmployeeTax = float.Parse((txtTax.Value == "") ? "0" : txtTax.Value);
                
                if (ddlDirectReportingPerson.SelectedItem != null)
                {
                    objDB.DirectReportTo = Convert.ToInt32(ddlDirectReportingPerson.SelectedItem.Value);
                    objDB.DirectReportingPerson = Convert.ToInt32(ddlDirectReportingPerson.SelectedItem.Value);
                }

                if (ddlInDirectReportingPerson.SelectedItem != null)
                {
                    objDB.InDirectReportingPerson = Convert.ToInt32(ddlInDirectReportingPerson.SelectedItem.Value);
                    objDB.IndirectReportTo = Convert.ToInt32(ddlInDirectReportingPerson.SelectedItem.Value);
                }
                if (ddlCOA.SelectedItem != null)
                    objDB.PayrolllExpCOA = Convert.ToInt32(ddlExpCOA.SelectedItem.Value);
                if (ddlEmployeeCOA.SelectedItem != null)
                {
                    objDB.EmployeeCOA = Convert.ToInt32(ddlEmployeeCOA.SelectedItem.Value);
                    objDB.COA_ID = Convert.ToInt32(ddlEmployeeCOA.SelectedItem.Value);
                }

                // Employee QrCode
                objDB.QRcodePath = GeneratedQRcodeofEmployee(objDB.CNIC);

                objDB.EmergencyContactNo = txtEmergencyPhone.Value;
                objDB.EmergencyContactPerson = txtEmergencyPerson.Value;
                objDB.WDID = Convert.ToInt32(txtWDID.Value);
                if (txtBasicSalary.Value != "")
                    objDB.BasicSalary = float.Parse(txtBasicSalary.Value);
                objDB.DeductPF = chkDeductPF.Checked;
                objDB.isOverTime = chkIsOverTime.Checked;
                objDB.ShiftID = Convert.ToInt32(ddlShifts.SelectedItem.Value);
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DesgID = Convert.ToInt32(ddlDesg.SelectedItem.Value);
                objDB.DeptID = Convert.ToInt32(ddlDept.SelectedItem.Value);
                objDB.JDID = Convert.ToInt32(ddlJD.SelectedItem.Value);

                if (HttpContext.Current.Items["EmployeeID"] != null)
                {
                    employeeID = Convert.ToInt32(HttpContext.Current.Items["EmployeeID"]);
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.EmployeeID = employeeID;
                    if (ShiftID != 0)
                    {
                        objDB.ShiftID = ShiftID;
                    }
                    res = objDB.UpdateNewEmployee();
                }
                else
                {
                    objDB.EmployeeCode = objDB.GenerateEmployeeCode();
                    objDB.CreatedBy = Session["UserName"].ToString();

                    res = objDB.AddNewEmployeesNew();

                    if (!(int.TryParse(res, out employeeID)))
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";

                        pAlertMsg.InnerHtml = res;
                        return;
                    }

                    objDB.CreatedBy = Session["UserName"].ToString();
                    objDB.EmployeeID = employeeID;
                    objDB.Email = txtEmail.Text;
                    objDB.AccessLevel = "Normal User";
                    objDB.AddUser();

                    sendEmail(txtFirstName.Value + " " + txtLastName.Value, objDB.Email, objDB.AccessLevel);

                    res = "New Employee Added";
                }

                objDB.WorkingShiftName = ddlShifts.SelectedItem.Text;
                objDB.EmployeeID = employeeID;
                objDB.parentShiftID = Convert.ToInt32(ddlShifts.SelectedValue);
                objDB.Monday = chkMonday.Checked;
                objDB.Tuesday = chkTuesday.Checked;
                objDB.Wednesday = chkWednesdat.Checked;
                objDB.Thursday = chkThursday.Checked;
                objDB.Friday = chkFriday.Checked;
                objDB.Saturday = chkSaturday.Checked;
                objDB.Sunday = chkSunday.Checked;
                objDB.DeptID = 0;

                string resShift = objDB.AddShiftFromESS();
                int TempShiftID = 0;
                if (int.TryParse(resShift, out TempShiftID))
                {
                    ShiftID = TempShiftID;
                    AddShiftDays();

                    if (HttpContext.Current.Items["EmployeeID"] != null)
                    {
                        PrevShiftID = ShiftID;
                        PrevParentShiftID = objDB.parentShiftID;
                    }
                    else
                    {
                        PrevShiftID = 0;
                        PrevParentShiftID = 0;
                        txtHours.Value = "";
                        shiftDaysDiv.Visible = false;
                    }
                }

                objDB.DocType = "EmployeeLists";
                objDB.DocID = employeeID;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

                objDB.EmployeeID = employeeID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteEmployeeExperienceByEmployeeID();

                dtExperience = (DataTable)ViewState["dtExperience"] as DataTable;
                if (dtExperience != null)
                {
                    if (dtExperience.Rows.Count > 0)
                    {
                        if (!showExperienceFirstRow)
                        {
                            dtExperience.Rows[0].Delete();
                            dtExperience.AcceptChanges();
                        }

                        for (int i = 0; i < dtExperience.Rows.Count; i++)
                        {
                            objDB.EmployeeID = Convert.ToInt32(employeeID);
                            objDB.Jobtitle = dtExperience.Rows[i]["Title"].ToString();
                            objDB.Organization = dtExperience.Rows[i]["Organization"].ToString();
                            objDB.StartYear = dtExperience.Rows[i]["StartYear"].ToString();
                            objDB.StartMonth = dtExperience.Rows[i]["StartMonth"].ToString();
                            objDB.EndYear = dtExperience.Rows[i]["EndYear"].ToString();
                            objDB.EndMonth = dtExperience.Rows[i]["EndMonth"].ToString();
                            objDB.Remarks = dtExperience.Rows[i]["Remarks"].ToString();
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddEmployeeExperience();
                        }
                    }
                }

                if (dtExperience.Rows.Count == 0)
                {
                    ViewState["dtExperience"] = null;
                    ViewState["showExperienceFirstRow"] = null;
                    ViewState["ExperienceSrNo"] = null;
                }

                objDB.EmployeeID = employeeID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteEmployeeQualificationByEmployeeID();

                dtQualification = (DataTable)ViewState["dtQualification"] as DataTable;
                if (dtQualification != null)
                {
                    if (dtQualification.Rows.Count > 0)
                    {
                        if (!showQualificationFirstRow)
                        {
                            dtQualification.Rows[0].Delete();
                            dtQualification.AcceptChanges();
                        }

                        for (int i = 0; i < dtQualification.Rows.Count; i++)
                        {
                            objDB.EmployeeID = Convert.ToInt32(employeeID);
                            objDB.DegreeTitle = dtQualification.Rows[i]["Degree"].ToString();
                            objDB.DegreeMonth = dtQualification.Rows[i]["Month"].ToString();
                            objDB.DegreeYear = dtQualification.Rows[i]["Year"].ToString();

                            objDB.Grade = dtQualification.Rows[i]["Grade"].ToString();
                            objDB.DegreeInst = dtQualification.Rows[i]["Institution"].ToString();
                            objDB.CityName = dtQualification.Rows[i]["City"].ToString();
                            objDB.CountryName = dtQualification.Rows[i]["PecNo"].ToString();
                            objDB.StateName = dtQualification.Rows[i]["PecExp"].ToString();
                            objDB.Remarks = dtQualification.Rows[i]["Remarks"].ToString();

                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddEmployeeQualification();
                        }
                    }
                }

                if (dtQualification.Rows.Count == 0)
                {
                    ViewState["dtQualification"] = null;
                    ViewState["showQualificationFirstRow"] = null;
                    ViewState["QualificationSrNo"] = null;
                }

                objDB.EmployeeID = employeeID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteEmployeeCareerTracking();
                                
                objDB.EmployeeID = employeeID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteEmployeeSkillSetByEmployeeID();

                dtSkillSet = (DataTable)ViewState["dtSkillSet"] as DataTable;
                if (dtSkillSet != null)
                {
                    if (dtSkillSet.Rows.Count > 0)
                    {
                        if (!showSkillSetFirstRow)
                        {
                            dtSkillSet.Rows[0].Delete();
                            dtSkillSet.AcceptChanges();
                        }

                        for (int i = 0; i < dtSkillSet.Rows.Count; i++)
                        {
                            objDB.EmployeeID = Convert.ToInt32(employeeID);
                            objDB.Skilltitle = dtSkillSet.Rows[i]["Skill"].ToString();
                            objDB.SkillLevel = dtSkillSet.Rows[i]["Level"].ToString();

                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddEmployeeSkillSet();
                        }
                    }
                }

                if (dtSkillSet.Rows.Count == 0)
                {
                    ViewState["dtSkillSet"] = null;
                    ViewState["showSkillSetFirstRow"] = null;
                    ViewState["skillSetSrNo"] = null;
                }

                objDB.EmployeeID = employeeID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteEmployeeCertAndTraByEmployeeID();

                dtCertification = (DataTable)ViewState["dtCertification"] as DataTable;
                if (dtCertification != null)
                {
                    if (dtCertification.Rows.Count > 0)
                    {
                        if (!showCertificationFirstRow)
                        {
                            dtCertification.Rows[0].Delete();
                            dtCertification.AcceptChanges();
                        }

                        for (int i = 0; i < dtCertification.Rows.Count; i++)
                        {
                            objDB.EmployeeID = Convert.ToInt32(employeeID);
                            objDB.Title = dtCertification.Rows[i]["Title"].ToString();
                            objDB.Year = dtCertification.Rows[i]["Year"].ToString();
                            objDB.Month = dtCertification.Rows[i]["Month"].ToString();
                            objDB.Institution = dtCertification.Rows[i]["Institution"].ToString();
                            objDB.Details = dtCertification.Rows[i]["Remarks"].ToString();
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.Score = dtCertification.Rows[i]["Score"].ToString();
                            objDB.AddEmployeeCertAndTra();
                        }
                    }
                }

                if (dtCertification.Rows.Count == 0)
                {
                    ViewState["dtCertification"] = null;
                    ViewState["showCertificationFirstRow"] = null;
                    ViewState["CertificationSrNo"] = null;
                }

                objDB.EmployeeID = employeeID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteEmployeeTableByEmployeeID();

                dtGeneralKPIS = (DataTable)ViewState["dtGeneralKPIS"] as DataTable;
                if (dtGeneralKPIS != null)
                {
                    if (dtGeneralKPIS.Rows.Count > 0)
                    {
                        if (!showFirstRow)
                        {
                            dtGeneralKPIS.Rows[0].Delete();
                            dtGeneralKPIS.AcceptChanges();
                        }

                        for (int i = 0; i < dtGeneralKPIS.Rows.Count; i++)
                        {
                            objDB.EmployeeID = employeeID;
                            objDB.Title = dtGeneralKPIS.Rows[i]["KPIDesc"].ToString();
                            objDB.Type = dtGeneralKPIS.Rows[i]["KPIType"].ToString();
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddEmployeeTable();
                        }
                    }
                }

                if (dtGeneralKPIS.Rows.Count == 0)
                {
                    ViewState["dtGeneralKPIS"] = null;
                    ViewState["showFirstRow"] = null;
                    ViewState["srNo"] = null;
                }

                objDB.EmployeeID = employeeID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteEmployeeAssetByEmployeeID();

                dtJDAssets = (DataTable)ViewState["dtJDAssets"] as DataTable;
                if (dtJDAssets != null)
                {
                    if (dtJDAssets.Rows.Count > 0)
                    {
                        if (dtJDAssets.Rows[0][0].ToString() == "")
                        {
                            dtJDAssets.Rows[0].Delete();
                            dtJDAssets.AcceptChanges();
                        }

                        for (int i = 0; i < dtJDAssets.Rows.Count; i++)
                        {
                            objDB.EmployeeID = Convert.ToInt32(employeeID);
                            objDB.AssetTitle = dtJDAssets.Rows[i]["Title"].ToString();
                            objDB.Quantity = int.Parse(dtJDAssets.Rows[i]["Qty"].ToString());
                            objDB.AssetDesc = dtJDAssets.Rows[i]["Desc"].ToString();
                            objDB.AssetStatus = "In Use";
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddEmployeeAsset();
                        }
                    }
                }

                if (dtJDAssets.Rows.Count == 0)
                {
                    ViewState["dtJDAssets"] = null;
                    ViewState["showAssetsFirstRow"] = null;
                    ViewState["assetsSrNo"] = null;
                }

                objDB.EmployeeID = employeeID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteEmployeeBudgetByEmployeeID();

                dtJDAdditions = (DataTable)ViewState["dtJDAdditions"] as DataTable;
                if (dtJDAdditions != null)
                {
                    if (dtJDAdditions.Rows.Count > 0)
                    {
                        if (dtJDAdditions.Rows[0][0].ToString() == "")
                        {
                            dtJDAdditions.Rows[0].Delete();
                            dtJDAdditions.AcceptChanges();
                        }

                        for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
                        {
                            objDB.EmployeeID = employeeID;
                            objDB.Title = dtJDAdditions.Rows[i]["Title"].ToString();
                            objDB.Type = dtJDAdditions.Rows[i]["Type"].ToString();
                            objDB.Amount = dtJDAdditions.Rows[i]["Amount"].ToString();
                            objDB.TransictionType = "Additions";
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddEmployeeBudget();
                        }
                    }
                }

                if (dtJDAdditions.Rows.Count == 0)
                {
                    ViewState["dtJDAdditions"] = null;
                    ViewState["showAdditionsFirstRow"] = null;
                    ViewState["AdditionsSrNo"] = null;
                }

                BindAdditionsTable();

                dtJDDeductions = (DataTable)ViewState["dtJDDeductions"] as DataTable;
                if (dtJDDeductions != null)
                {
                    if (dtJDDeductions.Rows.Count > 0)
                    {
                        if (dtJDDeductions.Rows[0][0].ToString() == "")
                        {
                            dtJDDeductions.Rows[0].Delete();
                            dtJDDeductions.AcceptChanges();
                        }

                        for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
                        {
                            objDB.EmployeeID = employeeID;
                            objDB.Title = dtJDDeductions.Rows[i]["Title"].ToString();
                            objDB.Type = dtJDDeductions.Rows[i]["Type"].ToString();
                            objDB.Amount = dtJDDeductions.Rows[i]["Amount"].ToString();
                            objDB.TransictionType = "Deductions";
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddEmployeeBudget();
                        }
                    }
                }

                if (dtJDDeductions.Rows.Count == 0)
                {
                    ViewState["dtJDDeductions"] = null;
                    ViewState["showDeductionsFirstRow"] = null;
                    ViewState["DeductionsSrNo"] = null;
                }

                BindDeductionsTable();

                objDB.EmployeeID = employeeID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteLeavesByEmployeeID();

                dtGradeLeaves = (DataTable)ViewState["dtGradeLeaves"] as DataTable;
                if (dtGradeLeaves != null)
                {
                    if (dtGradeLeaves.Rows.Count > 0)
                    {
                        if (dtGradeLeaves.Rows[0][0].ToString() == "")
                        {
                            dtGradeLeaves.Rows[0].Delete();
                            dtGradeLeaves.AcceptChanges();
                        }

                        for (int i = 0; i < dtGradeLeaves.Rows.Count; i++)
                        {
                            objDB.EmployeeID = Convert.ToInt32(employeeID);
                            objDB.LeaveTitle = dtGradeLeaves.Rows[i]["LeaveTitle"].ToString();
                            objDB.NoOfLeaves = int.Parse(dtGradeLeaves.Rows[i]["NoOfLeaves"].ToString());
                            objDB.LeavesConsumed = int.Parse(dtGradeLeaves.Rows[i]["LeavesConsumed"].ToString());
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddEmployeeLeaves();
                        }
                    }
                }
                if (dtGradeLeaves.Rows.Count == 0)
                {
                    ViewState["dtGradeLeaves"] = null;
                    ViewState["showGLFirstRow"] = null;
                    ViewState["GLsrNo"] = null;
                }
                

                objDB.EmployeeID = employeeID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteEmployeeAttachmentByEmployeeID();

                dtFiles = (DataTable)ViewState["dtFiles"] as DataTable;
                if (dtFiles != null)
                {
                    if (dtFiles.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtFiles.Rows.Count; i++)
                        {
                            objDB.EmployeeID = Convert.ToInt32(employeeID);
                            objDB.AttachmentTitle = dtFiles.Rows[i]["Title"].ToString();
                            objDB.AttachmentPath = dtFiles.Rows[i]["FilePath"].ToString();
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddEmployeeAttachment();
                        }
                    }
                }

                imgPhoto.Src = empPhoto;

                if (res == "New Employee Added" || res == "Employee Data Updated")
                {
                    if (res == "New Employee Added")
                    {
                        Common.addlog("Add", "HR", "New Employee \"" + objDB.EmployeeFName + " " + objDB.EmployeeMName + " " + objDB.EmployeeLName + "\" Added", "Employees");
                        clearFields();
                    }
                    if (res == "Employee Data Updated") { Common.addlog("Update", "HR", "Employee \"" + objDB.EmployeeFName + " " + objDB.EmployeeMName + " " + objDB.EmployeeLName + "\" Updated", "Employees", objDB.EmployeeID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getEmployeeAdditionsByEmployeeID(int employeeID)
        {
            try
            {
                CheckSessions();
                DataTable dt = new DataTable();
                objDB.EmployeeID = employeeID;
                dt = objDB.GetAllEmployeeBudgetByEmployeeID(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        dt = Common.filterTable(dt, "TransictionType", "Additions");
                    }
                }

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtJDAdditions.Rows[0][0].ToString() == "")
                        {
                            dtJDAdditions.Rows[0].Delete();
                            dtJDAdditions.AcceptChanges();
                            showAdditionsFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtJDAdditions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                        }

                        AdditionsSrNo = Convert.ToInt32(dtJDAdditions.Rows[dtJDAdditions.Rows.Count - 1][0].ToString()) + 1;
                    }
                }

                BindAdditionsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getEmployeeDeductionsByEmployeeID(int employeeID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.EmployeeID = employeeID;
                dt = objDB.GetAllEmployeeBudgetByEmployeeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        dt = Common.filterTable(dt, "TransictionType", "Deductions");
                    }
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtJDDeductions.Rows[0][0].ToString() == "")
                        {
                            dtJDDeductions.Rows[0].Delete();
                            dtJDDeductions.AcceptChanges();
                            showDeductionsFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtJDDeductions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                        }
                        DeductionsSrNo = Convert.ToInt32(dtJDDeductions.Rows[dtJDDeductions.Rows.Count - 1][0].ToString()) + 1;
                    }
                }
                BindDeductionsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void bindJDs()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                ddlJD.DataSource = objDB.GetAllJobDescriptions(ref errorMsg);
                ddlJD.DataTextField = "Title";
                ddlJD.DataValueField = "TitleID";
                ddlJD.DataBind();
                ddlJD.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


        protected void bindDesignationsDropDown()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlDesg.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
                ddlDesg.DataTextField = "DesgTitle";
                ddlDesg.DataValueField = "DesgID";
                ddlDesg.DataBind();
                ddlDesg.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlGrades_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                dtGeneralKPIS = null;
                dtGeneralKPIS = createKPIsTable();

                dtGradeLeaves = null;
                dtGradeLeaves = createGreadLeavesTable();

                
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private DataTable dtGeneralKPIS
        {
            get
            {
                if (ViewState["dtGeneralKPIS"] != null)
                {
                    return (DataTable)ViewState["dtGeneralKPIS"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtGeneralKPIS"] = value;
            }
        }

        bool showFirstRow
        {
            get
            {
                if (ViewState["showFirstRow"] != null)
                {
                    return (bool)ViewState["showFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showFirstRow"] = value;
            }
        }

        protected int srNo
        {
            get
            {
                if (ViewState["srNo"] != null)
                {
                    return (int)ViewState["srNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["srNo"] = value;
            }
        }

        private DataTable createKPIsTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("KPIType");
            dt.Columns.Add("KPIDesc");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }

        protected void BindKPIsTable()
        {
            try
            {
                if (ViewState["dtGeneralKPIS"] == null)
                {
                    dtGeneralKPIS = createKPIsTable();
                    ViewState["dtGeneralKPIS"] = dtGeneralKPIS;
                }

                gv.DataSource = dtGeneralKPIS;
                gv.DataBind();

                if (showFirstRow)
                    gv.Rows[0].Visible = true;
                else
                    gv.Rows[0].Visible = false;

                gv.UseAccessibleHeader = true;
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = srNo.ToString();
            }
        }

        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindKPIsTable();
        }

        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindKPIsTable();
        }

        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                int index = e.RowIndex;

                dtGeneralKPIS.Rows[index].SetField(1, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditKPIType")).SelectedItem.Text);
                dtGeneralKPIS.Rows[index].SetField(2, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditDesc")).Text);
                dtGeneralKPIS.AcceptChanges();

                gv.EditIndex = -1;
                BindKPIsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender as LinkButton;
                string delSr = lnk.CommandArgument.ToString();
                for (int i = 0; i < dtGeneralKPIS.Rows.Count; i++)
                {
                    if (dtGeneralKPIS.Rows[i][0].ToString() == delSr)
                    {
                        dtGeneralKPIS.Rows[i].Delete();
                        dtGeneralKPIS.AcceptChanges();
                    }
                }
                for (int i = 0; i < dtGeneralKPIS.Rows.Count; i++)
                {
                    dtGeneralKPIS.Rows[i].SetField(0, i + 1);
                    dtGeneralKPIS.AcceptChanges();
                }
                if (dtGeneralKPIS.Rows.Count < 1)
                {
                    DataRow dr = dtGeneralKPIS.NewRow();
                    dtGeneralKPIS.Rows.Add(dr);
                    dtGeneralKPIS.AcceptChanges();
                    showFirstRow = false;
                }
                if (showFirstRow)
                    srNo = dtGeneralKPIS.Rows.Count + 1;
                else
                    srNo = 1;

                BindKPIsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dr = dtGeneralKPIS.NewRow();
                dr[0] = srNo.ToString();
                dr[1] = ((DropDownList)gv.FooterRow.FindControl("ddlKPIType")).SelectedItem.Value;
                dr[2] = ((TextBox)gv.FooterRow.FindControl("txtDesc")).Text;
                dtGeneralKPIS.Rows.Add(dr);
                dtGeneralKPIS.AcceptChanges();
                srNo += 1;
                BindKPIsTable();
                ((Label)gv.FooterRow.FindControl("txtSrNo")).Text = srNo.ToString();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getGeneralKPISByEmployeeID(int employeeID)
        {
            try
            {
                dtGeneralKPIS = null;
                dtGeneralKPIS = new DataTable();
                dtGeneralKPIS = createKPIsTable();

                DataTable dt = new DataTable();
                objDB.EmployeeID = employeeID;
                //  dt = objDB.GetAllSurveyInvitations(ref errorMsg);
                dt = objDB.GetAllEmployeeTableByEmployeeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dtGeneralKPIS.Rows[0][0].ToString() == "")
                        {
                            dtGeneralKPIS.Rows[0].Delete();
                            dtGeneralKPIS.AcceptChanges();
                            showFirstRow = true;
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dtGeneralKPIS.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Title"]
                        });
                        }
                        srNo = Convert.ToInt32(dtGeneralKPIS.Rows[dtGeneralKPIS.Rows.Count - 1][0].ToString()) + 1;
                    }
                }

                BindKPIsTable();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private DataTable dtJDAssets
        {
            get
            {
                if (ViewState["dtJDAssets"] != null)
                {
                    return (DataTable)ViewState["dtJDAssets"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtJDAssets"] = value;
            }
        }
        bool showAssetsFirstRow
        {
            get
            {
                if (ViewState["showAssetsFirstRow"] != null)
                {
                    return (bool)ViewState["showAssetsFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showAssetsFirstRow"] = value;
            }
        }
        protected int assetsSrNo
        {
            get
            {
                if (ViewState["assetsSrNo"] != null)
                {
                    return (int)ViewState["assetsSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["assetsSrNo"] = value;
            }
        }

        protected void ddlJD_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtJDAssets = null;

            if (ddlJD.SelectedIndex != 0)
            {
                objDB.NodeID = Convert.ToInt32(ddlJD.SelectedValue);
                DataTable dtJD = objDB.GetNodeByNodeID(ref errorMsg);
                if (dtJD != null && dtJD.Rows.Count > 0)
                {
                    ddlCostCenter.SelectedValue = dtJD.Rows[0]["CostCenterID"].ToString();
                    ddlDesg.SelectedValue = dtJD.Rows[0]["DesgID"].ToString();
                    objDB.DesgID = Convert.ToInt32(ddlDesg.SelectedValue);
                    DataTable dtdesg = objDB.GetDesignationByID(ref errorMsg);
                    if (dtdesg != null && dtdesg.Rows.Count > 0)
                    {
                        ddlDept.SelectedValue = dtdesg.Rows[0]["DeptID"].ToString();
                    }
                }

                AdditionsSrNo = 1;
                showAdditionsFirstRow = false;
                dtJDAdditions = createJDAdditions();
                BindAdditionsTable();

                DeductionsSrNo = 1;
                showDeductionsFirstRow = false;
                dtJDDeductions = createJDAdditions();
                BindDeductionsTable();
            }
        }
                
        private DataTable dtSkillSet
        {
            get
            {
                if (ViewState["dtSkillSet"] != null)
                {
                    return (DataTable)ViewState["dtSkillSet"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtSkillSet"] = value;
            }
        }
        bool showSkillSetFirstRow
        {
            get
            {
                if (ViewState["showSkillSetFirstRow"] != null)
                {
                    return (bool)ViewState["showSkillSetFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showSkillSetFirstRow"] = value;
            }
        }
        protected int skillSetSrNo
        {
            get
            {
                if (ViewState["skillSetSrNo"] != null)
                {
                    return (int)ViewState["skillSetSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["skillSetSrNo"] = value;
            }
        }
        private DataTable createSkillSetTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Skill");
            dt.Columns.Add("Level");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindSkillSetTable()
        {
            try
            {
                if (ViewState["dtSkillSet"] == null)
                {
                    dtSkillSet = createSkillSetTable();
                    ViewState["dtSkillSet"] = dtSkillSet;
                }

                gvSkillSet.DataSource = dtSkillSet;
                gvSkillSet.DataBind();

                if (showSkillSetFirstRow)
                    gvSkillSet.Rows[0].Visible = true;
                else
                    gvSkillSet.Rows[0].Visible = false;

                gvSkillSet.UseAccessibleHeader = true;
                gvSkillSet.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        
        protected void gvSkillSet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = skillSetSrNo.ToString();
            }
        }
        
        protected void gvSkillSet_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv.EditIndex = e.NewEditIndex;
            BindSkillSetTable();
        }
        
        protected void gvSkillSet_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv.EditIndex = -1;
            BindSkillSetTable();
        }
        
        protected void gvSkill_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            dtSkillSet.Rows[index].SetField(2, ((DropDownList)gv.Rows[e.RowIndex].FindControl("ddlEditLevel")).SelectedItem.Text);
            dtSkillSet.Rows[index].SetField(1, ((TextBox)gv.Rows[e.RowIndex].FindControl("txtEditSkill")).Text);
            dtSkillSet.AcceptChanges();

            gv.EditIndex = -1;
            BindSkillSetTable();
        }
        
        protected void lnkSkillSetRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtSkillSet.Rows.Count; i++)
            {
                if (dtSkillSet.Rows[i][0].ToString() == delSr)
                {
                    dtSkillSet.Rows[i].Delete();
                    dtSkillSet.AcceptChanges();
                }
            }
            for (int i = 0; i < dtSkillSet.Rows.Count; i++)
            {
                dtSkillSet.Rows[i].SetField(0, i + 1);
                dtSkillSet.AcceptChanges();
            }
            if (dtSkillSet.Rows.Count < 1)
            {
                DataRow dr = dtSkillSet.NewRow();
                dtSkillSet.Rows.Add(dr);
                dtSkillSet.AcceptChanges();
                showSkillSetFirstRow = false;
            }
            if (showSkillSetFirstRow)
                srNo = dtSkillSet.Rows.Count + 1;
            else
                srNo = 1;

            BindSkillSetTable();
        }
        
        protected void btnAddSkillSet_Click(object sender, EventArgs e)
        {
            DataRow dr = dtSkillSet.NewRow();
            dr[0] = skillSetSrNo.ToString();
            dr[2] = ((DropDownList)gvSkillSet.FooterRow.FindControl("ddlLevel")).SelectedItem.Value;
            dr[1] = ((TextBox)gvSkillSet.FooterRow.FindControl("txtSkill")).Text;
            dtSkillSet.Rows.Add(dr);
            dtSkillSet.AcceptChanges();
            skillSetSrNo += 1;
            BindSkillSetTable();
            ((Label)gvSkillSet.FooterRow.FindControl("txtSrNo")).Text = skillSetSrNo.ToString();
        }
        
        private void getSkillSetByEmployeeID(int EmpID)
        {
            dtSkillSet = null;
            dtSkillSet = new DataTable();
            dtSkillSet = createSkillSetTable();

            DataTable dt = new DataTable();
            objDB.EmployeeID = EmpID;
            //  dt = objDB.GetAllSurveyInvitations(ref errorMsg);
            dt = objDB.GetAllEmployeeSkillSetByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtSkillSet.Rows[0][0].ToString() == "")
                    {
                        dtSkillSet.Rows[0].Delete();
                        dtSkillSet.AcceptChanges();
                        showSkillSetFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtSkillSet.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["SkillTitle"],
                            dt.Rows[i]["SkillLevel"]

                    });
                    }
                    skillSetSrNo = Convert.ToInt32(dtSkillSet.Rows[dtSkillSet.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindSkillSetTable();
        }

        private DataTable dtCertification
        {
            get
            {
                if (ViewState["dtCertification"] != null)
                {
                    return (DataTable)ViewState["dtCertification"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtCertification"] = value;
            }
        }
        bool showCertificationFirstRow
        {
            get
            {
                if (ViewState["showCertificationFirstRow"] != null)
                {
                    return (bool)ViewState["showCertificationFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showCertificationFirstRow"] = value;
            }
        }
        protected int CertificationSrNo
        {
            get
            {
                if (ViewState["CertificationSrNo"] != null)
                {
                    return (int)ViewState["CertificationSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["CertificationSrNo"] = value;
            }
        }
        private DataTable createCertificationTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("Month");
            dt.Columns.Add("Year");
            dt.Columns.Add("Score");
            dt.Columns.Add("Institution");
            dt.Columns.Add("Remarks");

            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindCertificationTable()
        {
            try
            {
                CheckSessions();
                if (ViewState["dtCertification"] == null)
                {
                    dtCertification = createCertificationTable();
                    ViewState["dtCertification"] = dtCertification;
                }

                gvCertification.DataSource = dtCertification;
                gvCertification.DataBind();

                if (showCertificationFirstRow)
                    gvCertification.Rows[0].Visible = true;
                else
                    gvCertification.Rows[0].Visible = false;

                gvCertification.UseAccessibleHeader = true;
                gvCertification.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        
        protected void gvCertification_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = CertificationSrNo.ToString();
            }
        }
        
        protected void gvCertification_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvCertification.EditIndex = e.NewEditIndex;
            BindCertificationTable();
        }
        
        protected void gvCertification_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvCertification.EditIndex = -1;
            BindCertificationTable();
        }
        
        protected void gvCertification_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            dtCertification.Rows[index].SetField(1, ((TextBox)gvCertification.Rows[e.RowIndex].FindControl("txtEditTilte")).Text);
            dtCertification.Rows[index].SetField(2, ((TextBox)gvCertification.Rows[e.RowIndex].FindControl("txtEditCity")).Text);
            dtCertification.Rows[index].SetField(3, ((TextBox)gvCertification.Rows[e.RowIndex].FindControl("txtEditDate")).Text);
            dtCertification.Rows[index].SetField(4, ((TextBox)gvCertification.Rows[e.RowIndex].FindControl("txtEditScore")).Text);
            dtCertification.Rows[index].SetField(5, ((TextBox)gvCertification.Rows[e.RowIndex].FindControl("txtEditInstitution")).Text);
            dtCertification.Rows[index].SetField(6, ((TextBox)gvCertification.Rows[e.RowIndex].FindControl("txtEditRemarks")).Text);

            dtCertification.AcceptChanges();

            gvCertification.EditIndex = -1;
            BindCertificationTable();
        }
        
        protected void lnkCertificationRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtCertification.Rows.Count; i++)
            {
                if (dtCertification.Rows[i][0].ToString() == delSr)
                {
                    dtCertification.Rows[i].Delete();
                    dtCertification.AcceptChanges();
                }
            }
            for (int i = 0; i < dtCertification.Rows.Count; i++)
            {
                dtCertification.Rows[i].SetField(0, i + 1);
                dtCertification.AcceptChanges();
            }
            if (dtCertification.Rows.Count < 1)
            {
                DataRow dr = dtCertification.NewRow();
                dtCertification.Rows.Add(dr);
                dtCertification.AcceptChanges();
                showCertificationFirstRow = false;
            }
            if (showCertificationFirstRow)
                CertificationSrNo = dtCertification.Rows.Count + 1;
            else
                CertificationSrNo = 1;

            BindCertificationTable();
        }
        
        protected void btnAddCertification_Click(object sender, EventArgs e)
        {
            if (dtCertification.Rows[0][0].ToString() == "")
            {
                dtCertification.Rows[0].Delete();
                dtCertification.AcceptChanges();
                showCertificationFirstRow = true;
            }
            DataRow dr = dtCertification.NewRow();

            dr[0] = CertificationSrNo.ToString();
            dr[1] = ((TextBox)gvCertification.FooterRow.FindControl("txtTitle")).Text;
            dr[2] = ((TextBox)gvCertification.FooterRow.FindControl("txtCity")).Text;
            dr[3] = ((TextBox)gvCertification.FooterRow.FindControl("txtDate")).Text;
            dr[4] = ((TextBox)gvCertification.FooterRow.FindControl("txtScore")).Text;
            dr[5] = ((TextBox)gvCertification.FooterRow.FindControl("txtInstitution")).Text;
            dr[6] = ((TextBox)gvCertification.FooterRow.FindControl("txtRemarks")).Text;

            dtCertification.Rows.Add(dr);
            dtCertification.AcceptChanges();
            CertificationSrNo += 1;
            BindCertificationTable();
            ((Label)gvCertification.FooterRow.FindControl("txtSrNo")).Text = CertificationSrNo.ToString();
        }
        
        private void getCertificationByEmployeeID(int EmpID)
        {
            dtCertification = null;
            dtCertification = new DataTable();
            dtCertification = createCertificationTable();

            DataTable dt = new DataTable();
            objDB.EmployeeID = EmpID;
            //  dt = objDB.GetAllSurveyInvitations(ref errorMsg);
            dt = objDB.GetAllEmployeeCertAndTraByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtCertification.Rows[0][0].ToString() == "")
                    {
                        dtCertification.Rows[0].Delete();
                        dtCertification.AcceptChanges();
                        showCertificationFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtCertification.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                             dt.Rows[i]["Month"],
                            dt.Rows[i]["Year"],
                            dt.Rows[i]["Score"],
                            dt.Rows[i]["Inst"],
                            dt.Rows[i]["Remark"]

                    });
                    }
                    CertificationSrNo = Convert.ToInt32(dtCertification.Rows[dtCertification.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindCertificationTable();
        }

        private DataTable dtQualification
        {
            get
            {
                if (ViewState["dtQualification"] != null)
                {
                    return (DataTable)ViewState["dtQualification"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtQualification"] = value;
            }
        }
        bool showQualificationFirstRow
        {
            get
            {
                if (ViewState["showQualificationFirstRow"] != null)
                {
                    return (bool)ViewState["showQualificationFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showQualificationFirstRow"] = value;
            }
        }
        protected int QualificationSrNo
        {
            get
            {
                if (ViewState["QualificationSrNo"] != null)
                {
                    return (int)ViewState["QualificationSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["QualificationSrNo"] = value;
            }
        }
        private DataTable createQualificationTable()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("SrNo");
            dt.Columns.Add("Degree");
            dt.Columns.Add("Month");
            dt.Columns.Add("Year");
            dt.Columns.Add("Grade");
            dt.Columns.Add("Institution");
            dt.Columns.Add("PecNo");
            dt.Columns.Add("PecExp");
            dt.Columns.Add("City");
            dt.Columns.Add("Remarks");

            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindQualificationTable()
        {
            try
            {
                CheckSessions();
                if (ViewState["dtQualification"] == null)
                {
                    dtQualification = createQualificationTable();
                    ViewState["dtQualification"] = dtQualification;
                }

                gvQualification.DataSource = dtQualification;
                gvQualification.DataBind();

                if (showQualificationFirstRow)
                    gvQualification.Rows[0].Visible = true;
                else
                    gvQualification.Rows[0].Visible = false;

                gvQualification.UseAccessibleHeader = true;
                gvQualification.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvQualification_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = QualificationSrNo.ToString();
            }
        }
        
        protected void gvQualification_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvQualification.EditIndex = e.NewEditIndex;
            BindQualificationTable();
        }
        
        protected void gvQualification_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvQualification.EditIndex = -1;
            BindQualificationTable();
        }
        
        protected void gvQualification_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            dtQualification.Rows[index].SetField(1, ((DropDownList)gvQualification.Rows[e.RowIndex].FindControl("ddlEditDegree")).Text);
            dtQualification.Rows[index].SetField(2, ((TextBox)gvQualification.Rows[e.RowIndex].FindControl("txtEditTitle")).Text);
            dtQualification.Rows[index].SetField(3, ((TextBox)gvQualification.Rows[e.RowIndex].FindControl("txtEditDate")).Text);
            dtQualification.Rows[index].SetField(4, ((TextBox)gvQualification.Rows[e.RowIndex].FindControl("txtEditGrade")).Text);
            dtQualification.Rows[index].SetField(5, ((TextBox)gvQualification.Rows[e.RowIndex].FindControl("txtEditInstitution")).Text);
            dtQualification.Rows[index].SetField(6, ((TextBox)gvQualification.Rows[e.RowIndex].FindControl("txtEditPecNo")).Text);
            dtQualification.Rows[index].SetField(7, ((TextBox)gvQualification.Rows[e.RowIndex].FindControl("txtEditPecExp")).Text);
            dtQualification.Rows[index].SetField(8, ((TextBox)gvQualification.Rows[e.RowIndex].FindControl("txtEditCity")).Text);
            dtQualification.Rows[index].SetField(9, ((TextBox)gvQualification.Rows[e.RowIndex].FindControl("txtEditRemarks")).Text);

            dtQualification.AcceptChanges();

            gvQualification.EditIndex = -1;
            BindQualificationTable();
        }
        
        protected void lnkQualificationRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtQualification.Rows.Count; i++)
            {
                if (dtQualification.Rows[i][0].ToString() == delSr)
                {
                    dtQualification.Rows[i].Delete();
                    dtQualification.AcceptChanges();
                }
            }
            for (int i = 0; i < dtQualification.Rows.Count; i++)
            {
                dtQualification.Rows[i].SetField(0, i + 1);
                dtQualification.AcceptChanges();
            }
            if (dtQualification.Rows.Count < 1)
            {
                DataRow dr = dtQualification.NewRow();
                dtQualification.Rows.Add(dr);
                dtQualification.AcceptChanges();
                showQualificationFirstRow = false;
            }
            if (showQualificationFirstRow)
                QualificationSrNo = dtQualification.Rows.Count + 1;
            else
                QualificationSrNo = 1;

            BindQualificationTable();
        }
        
        protected void btnAddQualification_Click(object sender, EventArgs e)
        {
            if (dtQualification.Rows[0][0].ToString() == "")
            {
                dtQualification.Rows[0].Delete();
                dtQualification.AcceptChanges();
                showQualificationFirstRow = true;
            }
            DataRow dr = dtQualification.NewRow();

            dr[0] = QualificationSrNo.ToString();

            dr[1] = ((DropDownList)gvQualification.FooterRow.FindControl("ddlDegree")).Text;
            dr[2] = ((TextBox)gvQualification.FooterRow.FindControl("txtTitle")).Text;
            dr[3] = ((TextBox)gvQualification.FooterRow.FindControl("txtDate")).Text;
            dr[4] = ((TextBox)gvQualification.FooterRow.FindControl("txtGrade")).Text;
            dr[5] = ((TextBox)gvQualification.FooterRow.FindControl("txtInstitution")).Text;
            dr[6] = ((TextBox)gvQualification.FooterRow.FindControl("txtPecNo")).Text;
            dr[7] = ((TextBox)gvQualification.FooterRow.FindControl("txtPecExp")).Text;
            dr[8] = ((TextBox)gvQualification.FooterRow.FindControl("txtCity")).Text;
            dr[9] = ((TextBox)gvQualification.FooterRow.FindControl("txtRemarks")).Text;

            dtQualification.Rows.Add(dr);
            dtQualification.AcceptChanges();
            QualificationSrNo += 1;
            BindQualificationTable();
            ((Label)gvQualification.FooterRow.FindControl("txtSrNo")).Text = QualificationSrNo.ToString();
        }
        
        private void getQualificationByEmployeeID(int EmpID)
        {
            dtQualification = null;
            dtQualification = new DataTable();
            dtQualification = createQualificationTable();

            DataTable dt = new DataTable();
            objDB.EmployeeID = EmpID;
            //  dt = objDB.GetAllSurveyInvitations(ref errorMsg);
            dt = objDB.GetAllEmployeeQualificationByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtQualification.Rows[0][0].ToString() == "")
                    {
                        dtQualification.Rows[0].Delete();
                        dtQualification.AcceptChanges();
                        showQualificationFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtQualification.Rows.Add(new object[] {
                            i+1,

                            dt.Rows[i]["DegreeTitle"],
                            dt.Rows[i]["DegreeMonth"],
                             dt.Rows[i]["DegreeYear"],
                            dt.Rows[i]["Grade"],
                            dt.Rows[i]["DegreeInst"],
                               dt.Rows[i]["Country"],
                            dt.Rows[i]["State"],
                            dt.Rows[i]["City"],
                            dt.Rows[i]["Remark"]

                    });
                    }
                    QualificationSrNo = Convert.ToInt32(dtQualification.Rows[dtQualification.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindQualificationTable();
        }

        bool showEmployeeCareerTrackingFirstRow
        {
            get
            {
                if (ViewState["showEmployeeCareerTrackingFirstRow"] != null)
                {
                    return (bool)ViewState["showEmployeeCareerTrackingFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showEmployeeCareerTrackingFirstRow"] = value;
            }
        }        
        private DataTable dtExperience
        {
            get
            {
                if (ViewState["dtExperience"] != null)
                {
                    return (DataTable)ViewState["dtExperience"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtExperience"] = value;
            }
        }
        bool showExperienceFirstRow
        {
            get
            {
                if (ViewState["showExperienceFirstRow"] != null)
                {
                    return (bool)ViewState["showExperienceFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showExperienceFirstRow"] = value;
            }
        }
        protected int ExperienceSrNo
        {
            get
            {
                if (ViewState["ExperienceSrNo"] != null)
                {
                    return (int)ViewState["ExperienceSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["ExperienceSrNo"] = value;
            }
        }
        private DataTable createExperienceTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("Organization");
            dt.Columns.Add("StartYear");
            dt.Columns.Add("StartMonth");
            dt.Columns.Add("EndYear");
            dt.Columns.Add("EndMonth");
            dt.Columns.Add("Remarks");

            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindExperienceTable()
        {
            try
            {

                if (ViewState["dtExperience"] == null)
                {
                    dtExperience = createExperienceTable();
                    ViewState["dtExperience"] = dtExperience;
                }

                gvExperience.DataSource = dtExperience;
                gvExperience.DataBind();

                if (showExperienceFirstRow)
                    gvExperience.Rows[0].Visible = true;
                else
                    gvExperience.Rows[0].Visible = false;

                gvExperience.UseAccessibleHeader = true;
                gvExperience.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        
        protected void gvExperience_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = ExperienceSrNo.ToString();
            }
        }
        
        protected void gvExperience_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvExperience.EditIndex = e.NewEditIndex;
            BindExperienceTable();
        }
        
        protected void gvExperience_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvExperience.EditIndex = -1;
            BindExperienceTable();
        }
        
        protected void gvExperience_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            dtExperience.Rows[index].SetField(1, ((TextBox)gvExperience.Rows[e.RowIndex].FindControl("txtEditTitle")).Text);
            dtExperience.Rows[index].SetField(2, ((TextBox)gvExperience.Rows[e.RowIndex].FindControl("txtEditOrganization")).Text);
            dtExperience.Rows[index].SetField(3, ((TextBox)gvExperience.Rows[e.RowIndex].FindControl("txtEditStartDate")).Text);
            dtExperience.Rows[index].SetField(4, ((TextBox)gvExperience.Rows[e.RowIndex].FindControl("txtEditStartDate")).Text);
            dtExperience.Rows[index].SetField(5, ((TextBox)gvExperience.Rows[e.RowIndex].FindControl("txtEditLeavingDate")).Text);
            if (((CheckBox)gvExperience.FooterRow.FindControl("txtCurrentDate")) != null)
            {
                if (((CheckBox)gvExperience.Rows[e.RowIndex].FindControl("txtEditCurrentDate")).Checked)
                {
                    ((CheckBox)gvExperience.Rows[e.RowIndex].FindControl("txtEditCurrentDate")).Checked = true;
                    dtExperience.Rows[index].SetField(6, "Yes");
                }
                else
                {
                    dtExperience.Rows[index].SetField(6, "No");
                }
            }

            dtExperience.Rows[index].SetField(7, ((TextBox)gvExperience.Rows[e.RowIndex].FindControl("txtEditRemarks")).Text);

            dtExperience.AcceptChanges();

            gvExperience.EditIndex = -1;
            BindExperienceTable();
        }
        
        protected void lnkExperienceRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtExperience.Rows.Count; i++)
            {
                if (dtExperience.Rows[i][0].ToString() == delSr)
                {
                    dtExperience.Rows[i].Delete();
                    dtExperience.AcceptChanges();
                }
            }
            for (int i = 0; i < dtExperience.Rows.Count; i++)
            {
                dtExperience.Rows[i].SetField(0, i + 1);
                dtExperience.AcceptChanges();
            }
            if (dtExperience.Rows.Count < 1)
            {
                DataRow dr = dtExperience.NewRow();
                dtExperience.Rows.Add(dr);
                dtExperience.AcceptChanges();
                showExperienceFirstRow = false;
            }
            if (showExperienceFirstRow)
                ExperienceSrNo = dtExperience.Rows.Count + 1;
            else
                ExperienceSrNo = 1;

            BindExperienceTable();
        }
        
        protected void btnAddExperience_Click(object sender, EventArgs e)
        {
            if (dtExperience.Rows[0][0].ToString() == "")
            {
                dtExperience.Rows[0].Delete();
                dtExperience.AcceptChanges();
                showExperienceFirstRow = true;
            }
            DataRow dr = dtExperience.NewRow();

            dr[0] = ExperienceSrNo.ToString();
            dr[1] = ((TextBox)gvExperience.FooterRow.FindControl("txtTitle")).Text;
            dr[2] = ((TextBox)gvExperience.FooterRow.FindControl("txtOrganization")).Text;
            dr[3] = ((TextBox)gvExperience.FooterRow.FindControl("txtStartDate")).Text;
            dr[4] = ((TextBox)gvExperience.FooterRow.FindControl("txtStartDate")).Text;
            //dr[3] = ((DropDownList)gvExperience.FooterRow.FindControl("ddlJoiningYear")).SelectedItem.Text;
            //dr[4] = ((DropDownList)gvExperience.FooterRow.FindControl("ddlJoiningMonth")).SelectedItem.Text;
            dr[5] = ((TextBox)gvExperience.FooterRow.FindControl("txtLeavingDate")).Text;
            if (((CheckBox)gvExperience.FooterRow.FindControl("txtCurrentDate")).Checked)
            {
                dr[6] = "Yes";
            }
            else
            {
                dr[6] = "No";
            }
            dr[7] = ((TextBox)gvExperience.FooterRow.FindControl("txtRemarks")).Text;

            dtExperience.Rows.Add(dr);
            dtExperience.AcceptChanges();
            ExperienceSrNo += 1;
            BindExperienceTable();
            ((Label)gvExperience.FooterRow.FindControl("txtSrNo")).Text = ExperienceSrNo.ToString();
        }
        
        private void getExperienceByEmployeeID(int EmpID)
        {
            dtExperience = null;
            dtExperience = new DataTable();
            dtExperience = createExperienceTable();

            DataTable dt = new DataTable();
            objDB.EmployeeID = EmpID;
            //  dt = objDB.GetAllSurveyInvitations(ref errorMsg);
            dt = objDB.GetAllEmployeeExperienceByEmployeeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtExperience.Rows[0][0].ToString() == "")
                    {
                        dtExperience.Rows[0].Delete();
                        dtExperience.AcceptChanges();
                        showExperienceFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtExperience.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["JobTitle"],
                            dt.Rows[i]["Organization"],
                            dt.Rows[i]["StartYear"],
                             dt.Rows[i]["StartMonth"],
                            dt.Rows[i]["EndYear"],
                               dt.Rows[i]["EndMonth"],

                            dt.Rows[i]["Remark"]

                    });
                    }
                    ExperienceSrNo = Convert.ToInt32(dtExperience.Rows[dtExperience.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindExperienceTable();
        }

        private void getJDAdditionsByNodeID(int nodeID)
        {
            DataTable dt = new DataTable();
            objDB.NodeID = nodeID;
            dt = objDB.GetAllJDSBudgetByNodeID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dt = Common.filterTable(dt, "TransictionType", "Additions");
                }
            }

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtJDAdditions.Rows[0][0].ToString() == "")
                    {
                        dtJDAdditions.Rows[0].Delete();
                        dtJDAdditions.AcceptChanges();
                        showAdditionsFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtJDAdditions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                    }
                    AdditionsSrNo = Convert.ToInt32(dtJDAdditions.Rows[dtJDAdditions.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindAdditionsTable();
        }
        private DataTable dtJDAdditions
        {
            get
            {
                if (ViewState["dtJDAdditions"] != null)
                {
                    return (DataTable)ViewState["dtJDAdditions"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtJDAdditions"] = value;
            }
        }
        bool showAdditionsFirstRow
        {
            get
            {
                if (ViewState["showAdditionsFirstRow"] != null)
                {
                    return (bool)ViewState["showAdditionsFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showAdditionsFirstRow"] = value;
            }
        }
        protected int AdditionsSrNo
        {
            get
            {
                if (ViewState["AdditionsSrNo"] != null)
                {
                    return (int)ViewState["AdditionsSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["AdditionsSrNo"] = value;
            }
        }
        private DataTable createJDAdditions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("Type");
            dt.Columns.Add("Amount");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindAdditionsTable()
        {
            try
            {

                if (ViewState["dtJDAdditions"] == null)
                {
                    dtJDAdditions = createJDAdditions();
                    ViewState["dtJDAdditions"] = dtJDAdditions;
                }

                gvAdditions.DataSource = dtJDAdditions;
                gvAdditions.DataBind();

                if (showAdditionsFirstRow)
                    gvAdditions.Rows[0].Visible = true;
                else
                    gvAdditions.Rows[0].Visible = false;

                gvAdditions.UseAccessibleHeader = true;
                gvAdditions.HeaderRow.TableSection = TableRowSection.TableHeader;

                float total = 0;
                if (dtJDAdditions != null)
                {
                    if (dtJDAdditions.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
                        {
                            if (dtJDAdditions.Rows[i][3].ToString() != "")
                            {
                                if (txtBasicSalary.Value != "")
                                {
                                    if (dtJDAdditions.Rows[i][2].ToString() == "Percentage")
                                    {
                                        total += ((float.Parse(dtJDAdditions.Rows[i][3].ToString()) / 100) * float.Parse(txtBasicSalary.Value));
                                    }
                                    else
                                    {
                                        total += float.Parse(dtJDAdditions.Rows[i][3].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            ((Label)gvAdditions.FooterRow.FindControl("lblTotal")).Text = "Total : " + string.Format("{0:f2}", total);
                txtOtherAllowance.Value = total.ToString();
                CalcullateSalaries();

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        
        protected void gvAdditions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = AdditionsSrNo.ToString();
            }
        }
        
        protected void gvAdditions_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            dtJDAdditions.Rows[index].SetField(1, ((TextBox)gvAdditions.Rows[e.RowIndex].FindControl("txtEditTitle")).Text);
            dtJDAdditions.Rows[index].SetField(2, ((DropDownList)gvAdditions.Rows[e.RowIndex].FindControl("ddlEditType")).SelectedItem.Text);
            dtJDAdditions.Rows[index].SetField(3, ((TextBox)gvAdditions.Rows[e.RowIndex].FindControl("txtEditAmount")).Text);

            dtJDAdditions.AcceptChanges();

            gvAdditions.EditIndex = -1;
            BindAdditionsTable();
        }
        
        protected void gvAdditions_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvAdditions.EditIndex = -1;
            BindAdditionsTable();
        }
        
        protected void gvAdditions_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvAdditions.EditIndex = e.NewEditIndex;
            BindAdditionsTable();
        }
        
        protected void lnkRemoveAdditions_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
            {
                if (dtJDAdditions.Rows[i][0].ToString() == delSr)
                {
                    dtJDAdditions.Rows[i].Delete();
                    dtJDAdditions.AcceptChanges();
                }
            }
            for (int i = 0; i < dtJDAdditions.Rows.Count; i++)
            {
                dtJDAdditions.Rows[i].SetField(0, i + 1);
                dtJDAdditions.AcceptChanges();
            }
            if (dtJDAdditions.Rows.Count < 1)
            {
                DataRow dr = dtJDAdditions.NewRow();
                dtJDAdditions.Rows.Add(dr);
                dtJDAdditions.AcceptChanges();
                showAdditionsFirstRow = false;
            }
            if (showAdditionsFirstRow)
                AdditionsSrNo = dtJDAdditions.Rows.Count + 1;
            else
                AdditionsSrNo = 1;

            BindAdditionsTable();
        }
        
        protected void btnAddAdditions_Click(object sender, EventArgs e)
        {
            DataRow dr = dtJDAdditions.NewRow();
            dr[0] = AdditionsSrNo.ToString();
            dr[1] = ((TextBox)gvAdditions.FooterRow.FindControl("txtTitle")).Text;
            dr[2] = ((DropDownList)gvAdditions.FooterRow.FindControl("ddlType")).SelectedItem.Text;
            dr[3] = ((TextBox)gvAdditions.FooterRow.FindControl("txtAmount")).Text;
            dtJDAdditions.Rows.Add(dr);
            dtJDAdditions.AcceptChanges();


            if (dtJDAdditions.Rows[0][0].ToString() == "")
            {
                dtJDAdditions.Rows[0].Delete();
                dtJDAdditions.AcceptChanges();
                showAdditionsFirstRow = true;
            }


            AdditionsSrNo += 1;
            BindAdditionsTable();
            ((Label)gvAdditions.FooterRow.FindControl("txtSrNo")).Text = AdditionsSrNo.ToString();
        }

        private void getJDDeductionsByNodeID(int nodeID)
        {
            DataTable dt = new DataTable();
            objDB.NodeID = nodeID;
            dt = objDB.GetAllJDSBudgetByNodeID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dt = Common.filterTable(dt, "TransictionType", "Deductions");
                }
            }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtJDDeductions.Rows[0][0].ToString() == "")
                    {
                        dtJDDeductions.Rows[0].Delete();
                        dtJDDeductions.AcceptChanges();
                        showDeductionsFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtJDDeductions.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["Title"],
                            dt.Rows[i]["Type"],
                            dt.Rows[i]["Amount"]
                        });
                    }
                    DeductionsSrNo = Convert.ToInt32(dtJDDeductions.Rows[dtJDDeductions.Rows.Count - 1][0].ToString()) + 1;
                }
            }

            BindDeductionsTable();
        }
        private DataTable dtJDDeductions
        {
            get
            {
                if (ViewState["dtJDDeductions"] != null)
                {
                    return (DataTable)ViewState["dtJDDeductions"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtJDDeductions"] = value;
            }
        }
        bool showDeductionsFirstRow
        {
            get
            {
                if (ViewState["showDeductionsFirstRow"] != null)
                {
                    return (bool)ViewState["showDeductionsFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showDeductionsFirstRow"] = value;
            }
        }
        protected int DeductionsSrNo
        {
            get
            {
                if (ViewState["DeductionsSrNo"] != null)
                {
                    return (int)ViewState["DeductionsSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["DeductionsSrNo"] = value;
            }
        }
        private DataTable createJDDeductions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("Type");
            dt.Columns.Add("Amount");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindDeductionsTable()
        {
            try
            {
                if (ViewState["dtJDDeductions"] == null)
                {
                    dtJDDeductions = createJDDeductions();
                    ViewState["dtJDDeductions"] = dtJDDeductions;
                }

                gvDeductions.DataSource = dtJDDeductions;
                gvDeductions.DataBind();

                if (showDeductionsFirstRow)
                    gvDeductions.Rows[0].Visible = true;
                else
                    gvDeductions.Rows[0].Visible = false;

                gvDeductions.UseAccessibleHeader = true;
                gvDeductions.HeaderRow.TableSection = TableRowSection.TableHeader;

                float total = 0;
                if (dtJDDeductions != null)
                {
                    if (dtJDDeductions.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
                        {
                            if (dtJDDeductions.Rows[i][3].ToString() != "")
                            {
                                if (txtBasicSalary.Value != "")
                                {
                                    if (dtJDDeductions.Rows[i][2].ToString() == "Percentage")
                                    {
                                        total += ((float.Parse(dtJDDeductions.Rows[i][3].ToString()) / 100) * float.Parse(txtBasicSalary.Value));
                                    }
                                    else
                                    {
                                        total += float.Parse(dtJDDeductions.Rows[i][3].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            ((Label)gvDeductions.FooterRow.FindControl("lblTotal")).Text = "Total : " + string.Format("{0:f2}", total);
                txtOtherDeductions.Value = total.ToString();
                CalcullateSalaries();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvDeductions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = DeductionsSrNo.ToString();
            }
        }
       
        protected void gvDeductions_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = e.RowIndex;

            dtJDDeductions.Rows[index].SetField(1, ((TextBox)gvDeductions.Rows[e.RowIndex].FindControl("txtEditTitle")).Text);
            dtJDDeductions.Rows[index].SetField(2, ((DropDownList)gvDeductions.Rows[e.RowIndex].FindControl("ddlEditType")).SelectedItem.Text);
            dtJDDeductions.Rows[index].SetField(3, ((TextBox)gvDeductions.Rows[e.RowIndex].FindControl("txtEditAmount")).Text);

            dtJDDeductions.AcceptChanges();

            gvDeductions.EditIndex = -1;
            BindDeductionsTable();
        }
        
        protected void gvDeductions_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDeductions.EditIndex = -1;
            BindDeductionsTable();
        }
        
        protected void gvDeductions_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDeductions.EditIndex = e.NewEditIndex;
            BindDeductionsTable();
        }
        
        protected void lnkRemoveDeductions_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
            {
                if (dtJDDeductions.Rows[i][0].ToString() == delSr)
                {
                    dtJDDeductions.Rows[i].Delete();
                    dtJDDeductions.AcceptChanges();
                }
            }
            for (int i = 0; i < dtJDDeductions.Rows.Count; i++)
            {
                dtJDDeductions.Rows[i].SetField(0, i + 1);
                dtJDDeductions.AcceptChanges();
            }
            if (dtJDDeductions.Rows.Count < 1)
            {
                DataRow dr = dtJDDeductions.NewRow();
                dtJDDeductions.Rows.Add(dr);
                dtJDDeductions.AcceptChanges();
                showDeductionsFirstRow = false;
            }
            if (showDeductionsFirstRow)
                DeductionsSrNo = dtJDDeductions.Rows.Count + 1;
            else
                DeductionsSrNo = 1;

            BindDeductionsTable();
        }
        
        protected void btnAddDeductions_Click(object sender, EventArgs e)
        {
            DataRow dr = dtJDDeductions.NewRow();
            dr[0] = DeductionsSrNo.ToString();
            dr[1] = ((TextBox)gvDeductions.FooterRow.FindControl("txtTitle")).Text;
            dr[2] = ((DropDownList)gvDeductions.FooterRow.FindControl("ddlType")).SelectedItem.Text;
            dr[3] = ((TextBox)gvDeductions.FooterRow.FindControl("txtAmount")).Text;
            dtJDDeductions.Rows.Add(dr);
            dtJDDeductions.AcceptChanges();


            if (dtJDDeductions.Rows[0][0].ToString() == "")
            {
                dtJDDeductions.Rows[0].Delete();
                dtJDDeductions.AcceptChanges();
                showDeductionsFirstRow = true;
            }


            DeductionsSrNo += 1;
            BindDeductionsTable();
            ((Label)gvDeductions.FooterRow.FindControl("txtSrNo")).Text = DeductionsSrNo.ToString();
        }

        private DataTable dtFiles
        {
            get
            {
                if (ViewState["dtFiles"] != null)
                {
                    return (DataTable)ViewState["dtFiles"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtFiles"] = value;
            }
        }
        
        protected int FilesSrNo
        {
            get
            {
                if (ViewState["FilesSrNo"] != null)
                {
                    return (int)ViewState["FilesSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["FilesSrNo"] = value;
            }
        }

        private DataTable createFiles()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("Title");
            dt.Columns.Add("FilePath");
            //dt.Columns.Add("FileType");
            dt.AcceptChanges();

            return dt;
        }
        
        protected void BindFilesTable()
        {
            try
            {
                if (ViewState["dtFiles"] == null)
                {
                    dtFiles = createFiles();
                    ViewState["dtFiles"] = dtFiles;
                }

                gvFiles.DataSource = dtFiles;
                gvFiles.DataBind();

                if (gvFiles.Rows.Count > 0)
                {
                    gvFiles.UseAccessibleHeader = true;
                    gvFiles.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        
        protected void lnkRemoveFile_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtFiles.Rows.Count; i++)
            {
                if (dtFiles.Rows[i][0].ToString() == delSr)
                {
                    dtFiles.Rows[i].Delete();
                    dtFiles.AcceptChanges();
                }
            }
            for (int i = 0; i < dtFiles.Rows.Count; i++)
            {
                dtFiles.Rows[i].SetField(0, i + 1);
                dtFiles.AcceptChanges();
            }

            FilesSrNo = dtFiles.Rows.Count + 1;
            BindFilesTable();
        }

        private DataTable dtGradeLeaves
        {
            get
            {
                if (ViewState["dtGradeLeaves"] != null)
                {
                    return (DataTable)ViewState["dtGradeLeaves"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtGradeLeaves"] = value;
            }
        }
        
        bool showGLFirstRow
        {
            get
            {
                if (ViewState["showGLFirstRow"] != null)
                {
                    return (bool)ViewState["showGLFirstRow"];
                }
                else
                {
                    return false;
                }
            }

            set
            {
                ViewState["showGLFirstRow"] = value;
            }
        }
        
        protected int GLsrNo
        {
            get
            {
                if (ViewState["GLsrNo"] != null)
                {
                    return (int)ViewState["GLsrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["GLsrNo"] = value;
            }
        }

        protected int TotalNoOfLeaves
        {
            get
            {
                if (ViewState["TotalNoOfLeaves"] != null)
                {
                    return (int)ViewState["TotalNoOfLeaves"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["TotalNoOfLeaves"] = value;
            }
        }
        
        private DataTable createGreadLeavesTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("LeaveTitle");
            dt.Columns.Add("NoOfLeaves");
            dt.Columns.Add("LeavesConsumed");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        
        private void getEmployeeLeavesByEmployeeID(int EmpID)
        {
            DataTable dt = new DataTable();
            objDB.EmployeeID = EmpID;
            dt = objDB.GetAllLeavesByEmployeeID(ref errorMsg);
            dtGradeLeaves = dt;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    showGLFirstRow = true;
                    GLsrNo = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString()) + 1;
                }
            }
        }

        private void getShiftsByEmployeeID(int EmpID)
        {

            try
            {
                DataTable dt = new DataTable();
                objDB.EmployeeID = EmpID;
                dt = objDB.GetWorkingShiftByEmployeeID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {


                        PrevParentShiftID = Convert.ToInt32(dt.Rows[0]["ParentID"].ToString());
                        PrevShiftID = Convert.ToInt32(dt.Rows[0]["ShiftID"].ToString());

                        if (PrevParentShiftID == 0)
                        {
                            PrevParentShiftID = PrevShiftID;
                        }

                        ddlShifts.SelectedValue = PrevParentShiftID.ToString();

                        shiftDaysDiv.Visible = true;
                        getShiftsByShiftID(PrevShiftID);

                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getShiftsByShiftID(int ShiftID)
        {
            try
            {
                shiftDaysDiv.Visible = true;
                DataTable dt, dtday = new DataTable();
                objDB.WorkingShiftID = ShiftID;
                dt = objDB.GetWorkingShiftByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        //txtShiftTitle.Value = dt.Rows[0]["ShiftName"].ToString();
                        txtHours.Value = dt.Rows[0]["Hours"].ToString();
                        chkMonday.Checked = getCheckBoxValue(dt.Rows[0]["Monday"].ToString());
                        chkTuesday.Checked = getCheckBoxValue(dt.Rows[0]["Tuesday"].ToString());
                        chkWednesdat.Checked = getCheckBoxValue(dt.Rows[0]["Wednesday"].ToString());
                        chkThursday.Checked = getCheckBoxValue(dt.Rows[0]["Thursday"].ToString());
                        chkFriday.Checked = getCheckBoxValue(dt.Rows[0]["Friday"].ToString());
                        chkSaturday.Checked = getCheckBoxValue(dt.Rows[0]["Saturday"].ToString());
                        chkSunday.Checked = getCheckBoxValue(dt.Rows[0]["Sunday"].ToString());

                        objDB.DocID = ShiftID;
                        objDB.DocType = "Shifts";
                    }
                }
                dtday = objDB.GetWorkingShiftDaysByShiftID(ref errorMsg);
                if (dtday != null && dtday.Rows.Count > 0)
                {
                    for (int i = 0; i < dtday.Rows.Count; i++)
                    {
                        if (dtday.Rows[i]["DayName"].ToString() == "Monday")
                        {
                            chkMonNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtMONStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtMONLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtMONBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtMONBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtMONHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtMONEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtMONEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkMondayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["isSpecial"].ToString());
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Tuesday")
                        {
                            chkTUENightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtTUEStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtTUELateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtTUEBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtTUEBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtTUEHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtTUEEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtTUEEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkTuesdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["isSpecial"].ToString());
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Wednesday")
                        {
                            chkWEDNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtWEDStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtWEDLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtWEDBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtWEDBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtWEDHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtWEDEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtWEDEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkWednesdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["isSpecial"].ToString());
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Thursday")
                        {
                            chkTHUNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtTHUStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtTHULateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtTHUBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtTHUBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtTHUHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtTHUEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtTHUEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkThursdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["isSpecial"].ToString());
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Friday")
                        {
                            chkFRINightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtFRIStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtFRILateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtFRIBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtFRIBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtFRIHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtFRIEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtFRIEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkFridayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["isSpecial"].ToString());
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Saturday")
                        {
                            chkSATNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtSATStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtSATLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtSATBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtSATBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtSATHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtSATEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtSATEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkSaturdayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["isSpecial"].ToString());
                        }
                        if (dtday.Rows[i]["DayName"].ToString() == "Sunday")
                        {
                            chkSUNNightShift.Checked = getCheckBoxValue(dtday.Rows[i]["isNightShift"].ToString());
                            txtSUNStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["StartTime"]).ToString("hh:mm tt");
                            txtSUNLateInTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["LateInTime"]).ToString("hh:mm tt");
                            txtSUNBreakStartTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakStartTime"]).ToString("hh:mm tt");
                            txtSUNBreakEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["BreakEndTime"]).ToString("hh:mm tt");
                            txtSUNHalfDayStart.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["HalfDayStart"]).ToString("hh:mm tt");
                            txtSUNEarlyOut.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EarlyOut"]).ToString("hh:mm tt");
                            txtSUNEndTime.Value = (DateTime.Today + (TimeSpan)dtday.Rows[i]["EndTime"]).ToString("hh:mm tt");
                            chkSundayIsSpecial.Checked = getCheckBoxValue(dtday.Rows[i]["isSpecial"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void BindShiftsDropDown(int companyID)
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = companyID;
                ddlShifts.DataSource = objDB.GetApproveAllWorkingShiftsByCompanyID(ref errorMsg);
                ddlShifts.DataTextField = "ShiftName";
                ddlShifts.DataValueField = "ShiftID";
                ddlShifts.DataBind();
                ddlShifts.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnWithdraw_ServerClick(object sender, EventArgs e)
        {

        }

        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "Employees", "EmployeeID", HttpContext.Current.Items["EmployeeID"].ToString(), Session["UserName"].ToString());
                //Common.addlog(res, "HR", "Employee of ID\"" + HttpContext.Current.Items["EmployeeID"].ToString() + "\" Status Changed", "Candidates", objDB.EmployeeID);
                Common.addlogNew(res, "HR", "Employee of ID\"" + HttpContext.Current.Items["EmployeeID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/employees", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/employees/edit-employee-" + HttpContext.Current.Items["EmployeeID"].ToString(), "Employee \"" + txtFirstName.Value + " " + txtMiddleName.Value + " " + txtLastName.Value + "\"", "Employees", "EmployeeLists", Convert.ToInt32(HttpContext.Current.Items["EmployeeID"].ToString()));
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;

                CheckAccess();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Employees", "EmployeeID", HttpContext.Current.Items["EmployeeID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "Employee of ID \"" + HttpContext.Current.Items["EmployeeID"].ToString() + "\" deleted", "Employees", Convert.ToInt32(HttpContext.Current.Items["EmployeeID"].ToString()));

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }



        }

        protected void ddlShifts_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlShifts.SelectedValue != "0")
                {
                    shiftDaysDiv.Visible = true;
                    getShiftsByShiftID(int.Parse(ddlShifts.SelectedItem.Value));
                }
                else
                {
                    shiftDaysDiv.Visible = false;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void bindDirectReportingPersonDropDown()
        {
            try
            {
                objDB.CompanyID = int.Parse(Session["CompanyID"].ToString());
                ddlDirectReportingPerson.DataSource = objDB.GetAllEmployeesByCompanyID(ref errorMsg);
                ddlDirectReportingPerson.DataTextField = "EmployeeName";
                ddlDirectReportingPerson.DataValueField = "EmployeeID";
                ddlDirectReportingPerson.DataBind();
                ddlDirectReportingPerson.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void bindInDirectReportingPersonDropDown()
        {
            try
            {
                objDB.CompanyID = int.Parse(Session["CompanyID"].ToString());
                ddlInDirectReportingPerson.DataSource = objDB.GetAllEmployeesByCompanyID(ref errorMsg);
                ddlInDirectReportingPerson.DataTextField = "EmployeeName";
                ddlInDirectReportingPerson.DataValueField = "EmployeeID";
                ddlInDirectReportingPerson.DataBind();
                ddlInDirectReportingPerson.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void txtEditCurrentDate_CheckedChanged(object sender, EventArgs e)
        {
            int index = gvExperience.EditIndex;

            if (((CheckBox)gvExperience.Rows[index].FindControl("txtEditCurrentDate")).Checked)
            {
                ((TextBox)gvExperience.Rows[index].FindControl("txtEditLeavingDate")).Enabled = false;
            }
            else
            {
                ((TextBox)gvExperience.Rows[index].FindControl("txtEditLeavingDate")).Enabled = true;
            }
        }
        
        protected void txtCurrentDate_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)gvExperience.FooterRow.FindControl("txtCurrentDate")).Checked)
            {
                ((TextBox)gvExperience.FooterRow.FindControl("txtLeavingDate")).Enabled = false;
            }
            else
            {
                ((TextBox)gvExperience.FooterRow.FindControl("txtLeavingDate")).Enabled = true;
            }
        }

        protected void txtEmail_ServerChange(object sender, EventArgs e)
        {
            objDB.Email = txtEmail.Text;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            DataTable dtemail = objDB.CheckEmail(ref errorMsg);
            lblEmailError.Text = "";
            if (dtemail != null && dtemail.Rows.Count > 0)
            {
                string status = dtemail.Rows[0]["Column1"].ToString();
                if (status == "Email Already Exists")
                {
                    //txtEmail.Text = "";
                    spnemail.Visible = true;
                    lblEmailError.Text = "Email Already Exists";

                }
                else
                {
                    spnemail.Visible = false;

                }
            }

        }
        
        private void BindCOA()
        {
            objDB.CompanyID = Convert.ToInt32(Session["ParentCompanyID"].ToString());
            ddlCOA.Items.Clear();
            ddlCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlCOA.DataTextField = "CodeTitle";
            ddlCOA.DataValueField = "COA_ID";
            ddlCOA.DataBind();
            ddlCOA.Items.Insert(0, new ListItem("-- Select COA --", "0"));

            //Expense COA

            ddlExpCOA.Items.Clear();
            ddlExpCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlExpCOA.DataTextField = "CodeTitle";
            ddlExpCOA.DataValueField = "COA_ID";
            ddlExpCOA.DataBind();
            ddlExpCOA.Items.Insert(0, new ListItem("-- Select COA --", "0"));

            //Employee COA

            ddlEmployeeCOA.Items.Clear();
            ddlEmployeeCOA.DataSource = objDB.GetAllApprovedCOAs(ref errorMsg);
            ddlEmployeeCOA.DataTextField = "CodeTitle";
            ddlEmployeeCOA.DataValueField = "COA_ID";
            ddlEmployeeCOA.DataBind();
            ddlEmployeeCOA.Items.Insert(0, new ListItem("-- Select COA --", "0"));
        }

        protected void ddlCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlEmployeeCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblEmployeeCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlEmployeeCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblEmployeeCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void ddlExpCOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblExpCOABal.Text = "Balance: 0";
            try
            {
                objDB.COA_ID = Convert.ToInt32(ddlExpCOA.SelectedValue);
                DataTable dtCOABalance = objDB.GetCOABalance(ref errorMsg);
                if (dtCOABalance != null)
                {
                    if (dtCOABalance.Rows.Count > 0)
                    {
                        lblExpCOABal.Text = "Balance: " + string.Format("{0:0,0}", dtCOABalance.Rows[0]["COABalance"]);
                    }
                }

            }
            catch (Exception ex)
            {

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void sendEmail(string Name, string UserName, string AccessLevel)
        {
            string returnMsg = "";
            try
            {
                DBQueries objDB = new DBQueries();
                string file = "";
                string html = "";

                file = System.Web.HttpContext.Current.Server.MapPath("/assets/emails/NewUser.html");
                StreamReader sr = new StreamReader(file);
                FileInfo fi = new FileInfo(file);

                if (System.IO.File.Exists(file))
                {
                    html += sr.ReadToEnd();
                    sr.Close();
                }

                html = html.Replace("##CREATED_BY##", Session["UserName"].ToString());
                html = html.Replace("##NAME##", Name);
                html = html.Replace("##USER_NAME##", UserName);
                html = html.Replace("##ROLE##", AccessLevel);

                MailMessage mail = new MailMessage();
                mail.Subject = "User Account Credentials for Technofinancials";
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), "Techno Financials");
                mail.To.Add(UserName);
                mail.Body = html;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["NoReplySMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NoReplyPort"].ToString()));
                smtp.EnableSsl = true;
                NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["NoReplyPassword"].ToString());
                smtp.Credentials = netCre;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                returnMsg = ex.Message;
            }
        }

        private void CalcullateSalaries()
        {
            try
            {
                float medicalAllowance = txtMedicalAllowance.Value != "" ? float.Parse(txtMedicalAllowance.Value) : 0;
                float otherAllowance = txtOtherAllowance.Value != "" ? float.Parse(txtOtherAllowance.Value) : 0;
                float MaintenanceAllowance = txtMaintenanceAllowance.Value != "" ? float.Parse(txtMaintenanceAllowance.Value) : 0;
                float foodAllowance = txtFoodAllowance.Value != "" ? float.Parse(txtFoodAllowance.Value) : 0;
                float travelAllowance = txtTravelAllowance.Value != "" ? float.Parse(txtTravelAllowance.Value) : 0;
                float carAllowance = txtCarAllowance.Value != "" ? float.Parse(txtCarAllowance.Value) : 0;
                float mobileAllowance = txtMobileAllowance.Value != "" ? float.Parse(txtMobileAllowance.Value) : 0;
                float fuelAllowance = txtFuelAllowance.Value != "" ? float.Parse(txtFuelAllowance.Value) : 0;
                float utility = txtUtility.Value != "" ? float.Parse(txtUtility.Value) : 0;
                float houseRentAllowance = txtHouseRentAllowance.Value != "" ? float.Parse(txtHouseRentAllowance.Value) : 0;
                float basicSalry = txtBasicSalary.Value != "" ? float.Parse(txtBasicSalary.Value) : 0;
                float EOBI = txtEOBIAllowance.Value != "" ? float.Parse(txtEOBIAllowance.Value) : 0;
                float IESSI = txtIESSI.Value != "" ? float.Parse(txtIESSI.Value) : 0;
                float OtherDeductions = txtOtherDeductions.Value != "" ? float.Parse(txtOtherDeductions.Value) : 0;

                float netSalary = medicalAllowance + otherAllowance + foodAllowance + travelAllowance + carAllowance + mobileAllowance + MaintenanceAllowance + fuelAllowance + utility + houseRentAllowance + basicSalry - EOBI - OtherDeductions - IESSI;
                txtNetSalary.Value = netSalary.ToString();

                float grossSalary = medicalAllowance + otherAllowance + foodAllowance + travelAllowance + carAllowance + mobileAllowance + MaintenanceAllowance + fuelAllowance + utility + houseRentAllowance + basicSalry;
                float totDed = EOBI + IESSI + OtherDeductions;

                txtTotalDeduction.Value = totDed.ToString();
                txtGrossSalary.Value = grossSalary.ToString();
            }
            catch (Exception)
            {
            }
        }

		protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
		{
            ddlCostCenter.SelectedItem.Text = ddlDept.SelectedItem.Text;
		}
	}
}
