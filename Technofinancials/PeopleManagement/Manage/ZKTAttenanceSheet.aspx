﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ZKTAttenanceSheet.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.ZKTAttenanceSheet" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
           <%-- <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />

            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/excelupload.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Upload Attendance Sheet</h3>
                        </div>


                        	<div class="col-md-4">
                                               <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg"  runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme"  runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg"  runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>



                        <div class="col-sm-4" style="margin-top: 20px;">
                            <div style="text-align: right;">
                            <a href="/assets/SampleAttendanceSheet.xlsx" target="_blank">Download Sample Sheet</a>
                                </div>
                           <%-- <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                <ContentTemplate>
                                    <div style="text-align: right;">
                                        <button class="tf-save-btn" "Save" id="btnSave" runat="server"  validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>
                            </asp:UpdatePanel>--%>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                     <div class="col-sm-12">
                             <div class="tab-content ">
                            <div class="tab-pane active row">
                                <div class="cleafix">&nbsp;</div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group add-atr-file-upload">
                                            <asp:FileUpload ID="updSheet" class="btn btn-primary" ClientIDMode="Static" runat="server" AllowMultiple="false" Style="display: none;" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"></asp:FileUpload>
                                            <button class="tf-upload-btn" "" data-original-"Upload" type="button" aria-describedby="tooltip260525" onclick="document.getElementById('updSheet').click();"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                            Select File
                                        </div>
                                    </div>
                                </div>
                                <div class="cleafix">&nbsp;</div>
                                <div class="cleafix">&nbsp;</div>
                                <asp:UpdatePanel ID="UpdPnl" runat="server">
                                    <ContentTemplate>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                     </div>
                    </div>

            
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
         <script>
            //function readURL(input) {
            //    if (input.files && input.files[0]) {
            //        var reader = new FileReader();

            //        reader.onload = function (e) {
            //            $('#imgLogo').attr('src', e.target.result);
            //        }

            //        reader.readAsDataURL(input.files[0]);
            //    }
            //}

            $("#updSheet").change(function () {
                __doPostBack('btnUploadSheet', '');
            });
        </script>

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757 !important;
                border-radius: 0px !important;
                box-shadow: 0 8px 6px -5px #cacaca !important;
                height: 38px !important;
                width: 37px !important;
                display: inline-block !important;
                padding: 5px !important;
                position: relative !important;
                text-align: center !important;
            }
        </style>
    </form>
</body>
</html>
