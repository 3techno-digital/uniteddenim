﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class OvertimeRatio : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int OvertimeRatioID
        {
            get
            {
                if (ViewState["OvertimeRatioID"] != null)
                {
                    return (int)ViewState["OvertimeRatioID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["OvertimeRatioID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["OvertimeRatioID"] = null;
                    BindJDDropdown();
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/OvertimeRatio";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["OvertimeRatioID"] != null)
                    {
                        OvertimeRatioID = Convert.ToInt32(HttpContext.Current.Items["OvertimeRatioID"].ToString());
                        getOvertimeRatioByID(OvertimeRatioID);
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "OvertimeRatio";
                objDB.PrimaryColumnnName = "OvertimeRatioID";
                objDB.PrimaryColumnValue = OvertimeRatioID.ToString();
                objDB.DocName = "Announcement";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getOvertimeRatioByID(int OvertimeRatioID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.OvertimeRatioID = OvertimeRatioID;
                dt = objDB.GetAllOvertimeRatioByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtWorkingDays.Value = dt.Rows[0]["WorkingDaysRatio"].ToString();
                        txtWeeakndRatio.Value = dt.Rows[0]["WeakendsRatio"].ToString();
                        txtNotes.Value = dt.Rows[0]["Note"].ToString();
                        chkAllow.Checked = Convert.ToBoolean(dt.Rows[0]["isActive"].ToString());
                        ddlJd.SelectedValue = dt.Rows[0]["JDID"].ToString();

                    }
                }
                Common.addlog("View", "HR", "Overtime Ratio Viewed", "OvertimeRatio", objDB.OvertimeRatioID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                int isAllow = 0;
                string res = "";

                if (chkAllow.Checked == true)
                {
                    isAllow = 1;
                }

                if (Convert.ToInt32(txtWeeakndRatio.Value) < 0 && Convert.ToInt32(txtWeeakndRatio.Value) > 0  )
                {
                    res = "Invalid Weakend Ratio";    
                }
                else if (Convert.ToInt32(txtWorkingDays.Value) < 0 && Convert.ToInt32(txtWorkingDays.Value) > 0)
                {
                    res = "Invalid Work Days Ratio";
                }
                else
                {
                    objDB.IsAllow = isAllow;
                    CheckSessions();

                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.WorkingDaysRatio = Convert.ToInt32(txtWeeakndRatio.Value);
                    objDB.WeakendsRatio = Convert.ToInt32(txtWeeakndRatio.Value);
                    objDB.Active = Convert.ToInt32(chkAllow.Checked);
                    objDB.JDID = Convert.ToInt32(ddlJd.SelectedValue);
                    objDB.Notes = txtNotes.Value;
                    if (HttpContext.Current.Items["OvertimeRatioID"] != null)
                    {
                        objDB.ModifiedBy = Session["UserName"].ToString();
                        objDB.OvertimeRatioID = OvertimeRatioID;
                        res = objDB.UpdateOvertimeRatio();
                        //res = "AddOn Head Data Updated";
                    }
                    else
                    {
                        objDB.CreatedBy = Session["UserName"].ToString();
                        res = objDB.AddOvertimeRatio();
                        clearFields();
                        //res = "New AddOn Head Added";
                    }
                }




                if (res == "Succesfully Inserted" || res == "Succesfully Updated")
                {
                    if (res == "Succesfully Inserted") { Common.addlog("Add", "HR", "New AddOns \"" + objDB.Title + "\" Added", "OvertimeRatio"); }
                    if (res == "Succesfully Updated") { Common.addlog("Update", "HR", "AddOns \"" + objDB.Title + "\" Updated", "OvertimeRatio", objDB.OvertimeRatioID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtWeeakndRatio.Value = "0";
            txtWorkingDays.Value = "0";
            ddlJd.SelectedValue = "0";
            chkAllow.Checked = false;
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "OvertimeRatio", "OvertimeRatioID", HttpContext.Current.Items["OvertimeRatioID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "OvertimeRatio of ID\"" + HttpContext.Current.Items["OvertimeRatioID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/AddOns", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/AddOns/edit-AddOns-" + HttpContext.Current.Items["OvertimeRatioID"].ToString(), "OvertimeRatio for costcenter \"" + ddlJd.SelectedItem.Text + "\" has been changed", "OvertimeRatio", "Announcement", Convert.ToInt32(HttpContext.Current.Items["OvertimeRatioID"].ToString()));

                //if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                //{
                //    objDB.OvertimeRatioID = OvertimeRatioID;
                //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                //    objDB.AddCurrencyToEmployeeAttendance();

                //}

                //Common.addlog("Delete", "HR", "OvertimeRatio of ID \"" + objDB.OvertimeRatioID + "\" deleted", "OvertimeRatio", objDB.OvertimeRatioID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "OvertimeRatio", "OvertimeRatioID", HttpContext.Current.Items["OvertimeRatioID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "Overtime Ratio of ID\"" + HttpContext.Current.Items["OvertimeRatioID"].ToString() + "\" Status Changed", "OvertimeRatio", OvertimeRatioID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.OvertimeRatioID = OvertimeRatioID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteOvertimeRatiobyID();
                Common.addlog("OvertimeRatio Rejected", "HR", "OvertimeRatio of ID\"" + HttpContext.Current.Items["OvertimeRatioID"].ToString() + "\" OvertimeRatio Rejected", "OvertimeRatio", OvertimeRatioID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "OvertimeRatio Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void BindJDDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlJd.DataSource = objDB.GetAllNodesByCompanyID(ref errorMsg);
                ddlJd.DataTextField = "NodeTitle";
                ddlJd.DataValueField = "NodeID";
                ddlJd.DataBind();
                ddlJd.Items.Insert(0, new ListItem("--- Select Job Discription ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

    }
}