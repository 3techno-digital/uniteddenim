﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyProcedure.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.PolicyProcedure" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
          <%--  <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="upd1"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>


            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h1 class="m-0 text-dark">Policies & Procedures</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div style="text-align: right;">
                                    <button type="button" class="AD_btn" data-toggle="modal" data-target="#notes-modal" value="Add Note" "Note">Note </button>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Delete' OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                    <button class="AD_btn" id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                    <button class="AD_btn" id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                    <button class="AD_btn" id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                    <button class="AD_btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Disapprove</button>
                                    <button class="AD_btn" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                    <a class="AD_btn" id="btnBack" runat="server">Back</a>
                                    <button class="AD_btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Submit</button>
                                    <button class="AD_btn" id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button">Approve</button>
                                    <asp:LinkButton ID="lnkReject" runat="server" CssClass="AD_btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click">Reject</asp:LinkButton>

                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>



                <section class="app-content">
                                    <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>

                        <div class="row">

                            <!-- Modal -->
                            <div class="modal fade M_set" id="notes-modal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="m-0 text-dark">Notes</h1>
                                            <div class="add_new">
                                                <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                                <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                            </div>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                            </p>
                                            <p>
                                                <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               <%-- <hr />--%>

                            </div>
                        </div>


                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSave" />
                    </Triggers>
                </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <h4>Title <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtPolicyProcedureTitle" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <input class="form-control" id="txtPolicyProcedureTitle" placeholder="Title" type="text" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <h4>Description <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtDescription" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <textarea rows="2" class="form-control" id="txtDescription" placeholder="Description" type="number" runat="server" />
                                            </div>
                                        </div>
                                            <asp:HiddenField id="hdnIsUpdate" Value="0"  runat="server" />
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h4>Attachments</h4>
                                            </div>
                                            <div class="file-upload">
                                                <div class="image-upload-wrap">
                                                    <%--<asp:FileUpload CssClass="file-upload-input" ID="updAttachments" runat="server" AllowMultiple="true" ClientIDMode="Static" onchange="readUrlMultiple2(this);" accept="image/*"></asp:FileUpload>--%>
                                                    <%--<input class="file-upload-input" type="file" name="updAttachments" id="updAttachments" runat="server" onchange="readUrlMultiple2(this);" accept="image/*">--%>
                                                    <input class="file-upload-input" runat="server" type="file" name="uploadFile" id="updLogo" onchange="readURL(this);" />
                                                    <div class="drag-text">
                                                        <h3>Drag and drop a file or select add Image (MaxSize: 2mb)</h3>
                                                    </div>
                                                </div>
                                                <div class="file-upload-content">
                                                    <%--<img class="file-upload-image" src="#" alt="Supplier image">--%>
                                                    <a target="_blank" id="docPath" runat="server" class="file-upload-image" href="#"> <span class="image-title">No file</span> </a>


                                                    <div class="image-title-wrap">
                                                        <button type="button" onclick="removeUpload()" class="remove-image">
                                                            Remove  <span><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">

                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                                                                <div class="col-md-12">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group" id="divAlertMsg" runat="server">
                                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                                                                           <span>
                                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                </span>
                                                            <p id="pAlertMsg" runat="server">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        
        <script>



            $(document).ready(function () {

                if ($('#hdnIsUpdate').val() == "1") {
                    $('.image-upload-wrap').hide();
                    $('.file-upload-content').show();
                }
                
            })

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var fileName = input.files[0].name;
                    var reader = new FileReader();
                    var myimg = '';
                    reader.onload = function (e) {
                        $('.image-upload-wrap').hide();
                        $('.file-upload-image').attr('href', e.target.result);
                        $('.file-upload-content').show();
                        $('.image-title').html(input.files[0].name);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                else {
                    removeUpload();
                }
            }
            function removeUpload() {
                $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                $('.file-upload-content').hide();
                $('.image-upload-wrap').show();
            }
            $('.image-upload-wrap').bind('dragover', function () {
                $('.image-upload-wrap').addClass('image-dropping');
            });
            $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
            });
        </script>


        <script>
            //function readUrlMultiple2(input) {
            //    if (input.files && input.files[0]) {
            //        var fp = $("#updAttachments");
            //        var lg = fp[0].files.length; // get length

            //        $('.image-title two').html(lg + 'Files Uploaded');
            //        if (lg > 0) {




            //            $('.image-upload-wrap two').hide();
            //            $('.file-upload-content two').show();
            //        }

                   


            //    } else {
            //        removeUploadMultiple2();
            //    }
            //}

            //function removeUploadMultiple2() {
            //    $('.file-upload-input two').replaceWith($('.file-upload-input two').clone());
            //    $('.file-upload-content two').hide();
            //    $('.image-upload-wrap two').show();
            //}
            //$('.image-upload-wrap two').bind('dragover', function () {
            //    $('.image-upload-wrap two').addClass('image-dropping two');
            //});
            //$('.image-upload-wrap two').bind('dragleave', function () {
            //    $('.image-upload-wrap two').removeClass('image-dropping two');
            //});

            //$("#updAttachments").change(function () {
            //    __doPostBack('attachments', '');
            //});

        </script>

        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
        </style>
        <!-- Modal -->
    </form>
</body>
</html>
