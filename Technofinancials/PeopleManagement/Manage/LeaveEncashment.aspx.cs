﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class LeaveEncashment : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int LeaveEncashmentID
        {
            get
            {
                if (ViewState["LeaveEncashmentID"] != null)
                {
                    return (int)ViewState["LeaveEncashmentID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["LeaveEncashmentID"] = value;
            }
        }
        // string AttendanceDate
        //{
        //    get
        //    {
        //        if (ViewState["AttendanceDate"] != null)
        //        {
        //            return ViewState["AttendanceDate"].ToString();
        //        }
        //        else
        //        {
        //            return "";
        //        }
        //    }

        //    set
        //    {
        //        ViewState["AttendanceDate"] = value;
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    //txtFromDate.Attributes.Add("min", DateTime.Now.ToString("yyyy-MM-dd"));
                    //txtToDate.Attributes.Add("min", DateTime.Now.ToString("yyyy-MM-dd"));
                    ViewState["LeaveEncashmentID"] = null;
                    //  ViewState["AttendanceDate"] = null;
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;
                    BindEmployeesDropDown();

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/leave-encashment";

                    divAlertMsg.Visible = false;

                    ddlLeaves.Enabled = false;

                    if (HttpContext.Current.Items["LeaveEncashmentID"] != null)
                    {
                        LeaveEncashmentID = Convert.ToInt32(HttpContext.Current.Items["LeaveEncashmentID"].ToString());

                        getLeaveByID(LeaveEncashmentID);

                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "LeaveEnCashment";
                objDB.PrimaryColumnnName = "LeaveEnCashmentID";
                objDB.PrimaryColumnValue = LeaveEncashmentID.ToString();
                objDB.DocName = "Holidays";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    //btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getLeaveByID(int LeaveEncashmentID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.LeaveEnCashmentID = LeaveEncashmentID;
                dt = objDB.GetLeaveEnCashmentByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        //txtFromDate.Value = DateTime.Parse(dt.Rows[0]["StartDate"].ToString()).ToString("dd-MMM-yyyy");
                        //txtToDate.Value = DateTime.Parse(dt.Rows[0]["EndDate"].ToString()).ToString("dd-MMM-yyyy");
                        ddlEmployees.SelectedValue = dt.Rows[0]["EmployeeID"].ToString();
                        ddlEmployee_SelectedIndexChanged(null, null);
              
    
                        ddlLeaves.SelectedValue = dt.Rows[0]["LeaveID"].ToString();
                        ddlLeaves_SelectedIndexChanged(null, null);
                        objDB.DocID = LeaveEncashmentID;
                        objDB.DocType = "Leaves";
                        txtNotes.Value = objDB.GetDocNotes();
                    }
                }
                Common.addlog("View", "HR", "Leaves \"" + ddlEmployees.SelectedItem.Text + "\" Viewed", "LeaveEnCashment", objDB.LeaveEnCashmentID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }
        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (CheckLeavesAvailability())
                {
                    System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                    string res = Common.addAccessLevels(btn.ID.ToString(), "LeaveEnCashment", "LeaveEnCashmentID", HttpContext.Current.Items["LeaveEncashmentID"].ToString(), Session["UserName"].ToString());
                    Common.addlogNew(res, "HR", "Leave of ID\"" + HttpContext.Current.Items["LeaveEnCashmentID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/leave-encashment", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/LeaveEnCashments/edit-leave-encashment-" + HttpContext.Current.Items["LeaveEnCashmentID"].ToString(), "Leave for Employee \"" + ddlEmployees.SelectedItem.Text + "\"", "LeaveEnCashment", "Holidays", Convert.ToInt32(HttpContext.Current.Items["LeaveEnCashmentID"].ToString()));

                    if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                    {
                        objDB.LeaveEnCashmentID = LeaveEncashmentID;
                        objDB.UpdateEncashedLeaves();

                    }


                    //Common.addlog(res, "HR", "Leave of ID\"" + HttpContext.Current.Items["LeaveEncashmentID"].ToString() + "\" Status Changed", "LeaveEnCashment", LeaveEncashmentID);

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;

                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Can't Proceed because selected no of leaves is not avalible to this employee";

                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        bool CheckLeavesAvailability()
        {
            string s;
            objDB.LeaveID = ddlLeaves.SelectedValue;
            //objDB.StartDate = txtFromDate.Value;
            //objDB.EndDate = txtToDate.Value;
            objDB.LeaveEnCashQty = Convert.ToInt32(txtNoOfLeaves.Value);
            s = objDB.CheckLeavesAvailabilityForEnCashment();
            if (s == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
          
                objDB.EmployeeID = Convert.ToInt32(ddlEmployees.SelectedValue);
                objDB.LeaveEnCashQty = Convert.ToInt32(txtNoOfLeaves.Value);
                objDB.BasicSalary = float.Parse (txtBasicSalary.Value);
                objDB.TotalAmount = float.Parse(txtTotalCash.Value);
                objDB.PerDaySalary = float.Parse(txtPerDaySalary.Value);
                objDB.LeaveID = ddlLeaves.SelectedValue;
                int Docid = 0;
                if (HttpContext.Current.Items["LeaveEncashmentID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.LeaveEnCashmentID = LeaveEncashmentID;
                    Docid = Convert.ToInt32(objDB.UpdateLeaveEnCashment());
                    res = "Leave Data Updated";
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    res = objDB.AddLeaveEnCashment();

                }

                objDB.DocType = "Leaves";
                objDB.DocID = Docid;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

                if (res == "New Leave Added" || res == "Leave Data Updated")
                {
                    if (res == "New Leave Added")
                    {
                        Common.addlog("Add", "HR", "New Leave \"" + (objDB.LeaveEnCashmentID).ToString() + "\" Added", "LeaveEnCashment");
                        clearFields();
                    }
                    if (res == "Leave Data Updated") { Common.addlog("Update", "HR", "Leave \"" + (objDB.LeaveEnCashmentID).ToString() + "\" Updated", "LeaveEnCashment", objDB.LeaveEnCashmentID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void BindEmployeesDropDown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                ddlEmployees.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddlEmployees.DataTextField = "EmployeeName";
                ddlEmployees.DataValueField = "EmployeeID";
                ddlEmployees.DataBind();
                ddlEmployees.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void clearFields()
        {
            txtNoOfLeaves.Value = "0";
            txtBasicSalary.Value = "0";
            txtPerDaySalary.Value = "0";
            txtTotalCash.Value = "0";
            ddlLeaves.Items.Clear();
            ddlLeaves.Enabled = false;
            ddlEmployees.SelectedIndex = 0;
        }

        private void BindEmployeesLeaves()
        {
            try
            {
                CheckSessions();
                ddlLeaves.Items.Clear();
                objDB.EmployeeID = Convert.ToInt32(ddlEmployees.SelectedValue);
                ddlLeaves.DataSource = objDB.GetAllLeavesByEmployeeIDForDropDown(ref errorMsg);
                ddlLeaves.DataTextField = "LeavesDetail";
                ddlLeaves.DataValueField = "LeaveID";
                ddlLeaves.DataBind();
                ddlLeaves.Items.Insert(0, new ListItem("--- Select Leave Type ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }


        private void getEmployeeAttendanceByEmployeeID(int EmpID)
        {

        }

        //protected void btnSave_ServerClick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        CheckSessions();
        //        string res = "";
        //        string consumedLeav = ddlLeaves.SelectedItem.Text;
        //        consumedLeav = consumedLeav.Substring(consumedLeav.IndexOf("- ") + 1);
        //        consumedLeav = consumedLeav.Replace(" ", "");
        //        DateTime startDate = DateTime.Parse(txtFromDate.Value);
        //        DateTime endDate = DateTime.Parse(txtToDate.Value);
        //        int reqLeaves = Convert.ToInt32((endDate.Date - startDate.Date).TotalDays) + 1;
        //        if (int.Parse(consumedLeav) >= reqLeaves)
        //        {

        //            int shiftID = 0;
        //            objDB.EmployeeID = Convert.ToInt32(ddlEmployees.SelectedValue);
        //            DataTable dt = objDB.GetWorkingShiftByEmployeeID(ref errorMsg);
        //            if (dt != null)
        //            {
        //                if (dt.Rows.Count > 0)
        //                {
        //                    if (dt.Rows[0]["ShiftID"].ToString() != "")
        //                        shiftID = Convert.ToInt32(dt.Rows[0]["ShiftID"].ToString());

        //                    if (shiftID != 0)
        //                    {
        //                        objDB.LeaveID = ddlLeaves.SelectedItem.Value;
        //                        objDB.NoOfLeaves = reqLeaves;
        //                        res = objDB.DeductEmployyeLeaves();


        //                        for (DateTime i = startDate; i <= endDate; i = i.AddDays(1))
        //                        {
        //                            objDB.ShiftID = shiftID;
        //                            objDB.EmployeeID = Convert.ToInt32(ddlEmployees.SelectedValue);
        //                            objDB.TimeIn = i.ToString("dd-MMM-yyyy");

        //                            objDB.AddEmployeeLeavesInAttendanceTable();
        //                        }

        //                        if (res == "Leaves Deducted")
        //                        {
        //                            Common.addlog("Leaves Deducted", "HR", "Leaves Deducted of Employ\"" + ddlEmployees.SelectedItem.Text, "EmployeeAttendance", objDB.EmployeeID);
        //                            divAlertMsg.Visible = true;
        //                            divAlertTheme.Attributes["class"] = "alert tf-alert-success";
        //                            pAlertMsg.InnerHtml = res;
        //                            clearFields();
        //                        }
        //                        else
        //                        {
        //                            divAlertMsg.Visible = true;
        //                            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //                            pAlertMsg.InnerHtml = res;
        //                        }

        //                    }
        //                    else
        //                    {
        //                        divAlertMsg.Visible = true;
        //                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //                        pAlertMsg.InnerHtml = "No Working Shift Found! Please assign working shift to employee first";
        //                    }


        //                }
        //            }
        //            else
        //            {
        //                divAlertMsg.Visible = true;
        //                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //                pAlertMsg.InnerHtml = "No Working Shift Found! Please assign working shift to employee first";
        //            }


        //        }



        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }

        //}

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlEmployees.SelectedIndex != 0)
                {
                    DataTable dte = new DataTable();
                    objDB.EmployeeID = Convert.ToInt32 (ddlEmployees.SelectedValue);

                    dte = objDB.GetEmployeeByID(ref errorMsg);
                    if (dte != null && dte.Rows.Count > 0)
                    {
                        txtBasicSalary.Value = dte.Rows[0]["BasicSalary"].ToString();
                        txtPerDaySalary.Value = ((float.Parse(dte.Rows[0]["BasicSalary"].ToString())*12)/ 365.25).ToString("0.##");
                        txtNoOfLeaves.Value = "0";
                        txtTotalCash.Value = "0";

                    }


                }

                ddlLeaves.Enabled = true;
                BindEmployeesLeaves();

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {


        }

        protected void ddlLeaves_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlLeaves.SelectedIndex != 0)
                {
                    int tottleaves;
                    int ConLeaves;
                    DataTable dtL = new DataTable();
                    objDB.LeaveID = ddlLeaves.SelectedValue;
                    dtL = objDB.GetEmployeeLeavesByID(ref errorMsg);
                    if (dtL != null && dtL.Rows.Count > 0)
                    {
                        tottleaves = Convert.ToInt32(dtL.Rows[0]["NoOfLeaves"].ToString()); 
                        ConLeaves = Convert.ToInt32(dtL.Rows[0]["LeavesConsumed"].ToString());
                        txtNoOfLeaves.Value = (tottleaves-ConLeaves).ToString();
                        txtTotalCash.Value = ((tottleaves - ConLeaves) * float.Parse(txtPerDaySalary.Value)).ToString();


                    }


                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }



        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "LeaveEnCashment", "LeaveEnCashmentID", HttpContext.Current.Items["LeaveEncashmentID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "Leave of ID \"" + objDB.LeaveEnCashmentID + "\" deleted", "LeaveEnCashment", objDB.LeaveEnCashmentID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.LeaveEnCashmentID = LeaveEncashmentID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteHoliday();
                Common.addlog("Leave Rejected", "HR", "Leave of ID\"" + HttpContext.Current.Items["LeaveEncashmentID"].ToString() + "\" Leave Rejected", "LeaveEnCashment", LeaveEncashmentID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Holiday Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }
    }
}