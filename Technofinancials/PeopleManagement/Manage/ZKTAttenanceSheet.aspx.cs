﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class ZKTAttenanceSheet : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
           
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
            }

            Page.ClientScript.GetPostBackEventReference(this, string.Empty);
            string ctrlName = Request.Params.Get("__EVENTTARGET");
            string ctrlArgs = Request.Params.Get("__EVENTARGUMENT");
            if (!String.IsNullOrEmpty(ctrlName) && ctrlName == "btnUploadSheet")
            {
                if (updSheet.HasFile)
                {
                    string FileName = Path.GetFileName(updSheet.PostedFile.FileName);
                    string Extension = Path.GetExtension(updSheet.PostedFile.FileName);
                    string FolderPath = "/assets/temp_sheets";
                    string FilePath = Server.MapPath(FolderPath + FileName);
                    updSheet.SaveAs(FilePath);
                    //SaveSheet(FileName, Extension, FolderPath);

                    DataTable dt = Common.excelToDataTable(FilePath);
                    if (!Common.isDataTableNullOrRowsZero(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                            objDB.EmployeeCode = dt.Rows[i][0].ToString();
                            objDB.mergedDateTime = dt.Rows[i][1].ToString();
                            objDB.CreatedBy = Session["UserName"].ToString();

                            objDB.AddEmployeeAttendanceFromSheet();
                        }
                        
                    }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = "Attendance Sheet Updated!!";
                }
            }
        }

        //private void SaveSheet(string FileName, string Extension,string FolderPath)
        //{
        //    string CommandText = "";
        //    switch (Extension)
        //    {
        //        case ".xls":
        //            CommandText = "spx_ImportFromExcel03";
        //            break;
        //        case ".xlsx":
        //            CommandText = "spx_ImportFromExcel07";
        //            break;
        //    }


        //    string strConnString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        //    SqlConnection con = new SqlConnection(strConnString);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.CommandText = CommandText;
        //    cmd.Parameters.Add("@SheetName", SqlDbType.VarChar).Value = ddlSheets.SelectedItem.Text;
        //    cmd.Parameters.Add("@FilePath", SqlDbType.VarChar).Value = FolderPath + FileName;
        //    cmd.Parameters.Add("@HDR", SqlDbType.VarChar).Value = rbHDR.SelectedItem.Text;
        //    cmd.Parameters.Add("@TableName", SqlDbType.VarChar).Value = txtTable.Text;

        //    cmd.Connection = con;

        //    try
        //    {
        //        con.Open();
        //        object count = cmd.ExecuteNonQuery();

        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
        //        pAlertMsg.InnerHtml = count.ToString() + " records inserted.";
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }
        //    finally
        //    {

        //        con.Close();
        //        con.Dispose();
        //    }
        //}

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

    }
}