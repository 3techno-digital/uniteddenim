﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class Holidays : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int HolidayID
        {
            get
            {
                if (ViewState["HolidayID"] != null)
                {
                    return (int)ViewState["HolidayID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["HolidayID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    ViewState["HolidayID"] = null;
                    btnApprove.Visible = false;
                    btnReview.Visible = false;
                    btnRevApprove.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;

                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/holidays";

                    divAlertMsg.Visible = false;

                    if (HttpContext.Current.Items["HolidayID"] != null)
                    {
                        HolidayID = Convert.ToInt32(HttpContext.Current.Items["HolidayID"].ToString());
                        getHolidayByID(HolidayID);
                        CheckAccess();
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {

                btnSave.Visible = false;
                btnApprove.Visible = false;
                btnReview.Visible = false;
                btnRevApprove.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;

                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "Holidays";
                objDB.PrimaryColumnnName = "HolidayID";
                objDB.PrimaryColumnValue = HolidayID.ToString();
                objDB.DocName = "Holidays";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));

                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApprove.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApprove.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getHolidayByID(int HolidayID)
        {
            try
            {
                DataTable dt = new DataTable();
                objDB.HolidayID = HolidayID;
                dt = objDB.GetHolidayByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtHolidayTitle.Value = dt.Rows[0]["HolidayTitle"].ToString();
                        ddlAnnualCalendar.SelectedValue = dt.Rows[0]["HolidayType"].ToString();
                        txtStartDate.Value = DateTime.Parse(dt.Rows[0]["StartDate"].ToString()).ToString("dd-MMM-yyyy");
                        txtEndDate.Value = DateTime.Parse(dt.Rows[0]["EndDate"].ToString()).ToString("dd-MMM-yyyy");
                        chkSpecialHoliday.Checked = (dt.Rows[0]["isSpecial"].ToString() == "True") ? true : false;
                        objDB.DocID = HolidayID;
                        objDB.DocType = "Holidays";
                        ltrNotesTable.Text = objDB.GetDocNotes();
                    }
                }
                Common.addlog("View", "HR", "Holiday \"" + txtHolidayTitle.Value + "\" Viewed", "Holidays", objDB.HolidayID);

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.HolidayTitle = txtHolidayTitle.Value;
                objDB.HolidayType = ddlAnnualCalendar.SelectedValue;
                objDB.StartDate = txtStartDate.Value;
                objDB.EndDate = txtEndDate.Value;
                objDB.AnnualCalendar = "PK";
                objDB.isSpecial = chkSpecialHoliday.Checked;
                string Today = DateTime.Now.ToString("yyyyMMdd");
                string StartDate = objDB.StartDate;
                string EndDate = objDB.EndDate;
                //Today = trimString(String.Format("{0:M/d/yyyy}", Today));
                StartDate = Convert.ToDateTime(StartDate).ToString("yyyyMMdd");
                EndDate = Convert.ToDateTime(EndDate).ToString("yyyyMMdd");
                //if (Convert.ToInt32(StartDate) >= Convert.ToInt32(Today) && Convert.ToInt32(EndDate) >= Convert.ToInt32(Today) && Convert.ToInt32(StartDate) >= Convert.ToInt32(EndDate))
                if (true)
                {
                    int Docid = 0;
                    if (HttpContext.Current.Items["HolidayID"] != null)
                    {
                        objDB.ModifiedBy = Session["UserName"].ToString();
                        objDB.HolidayID = HolidayID;
                        Docid = Convert.ToInt32(objDB.UpdateHoliday());
                        res = "Holiday Data Updated";
                    }
                    else
                    {
                        objDB.CreatedBy = Session["UserName"].ToString();
                        res = objDB.AddHoliday();
                        if (int.TryParse(res, out Docid))
                        {
                            clearFields();
                            res = "New Holiday Added";
                        }
                    }


                    if (res == "New Holiday Added" || res == "Holiday Data Updated")
                    {

                        objDB.DocType = "Holidays";
                        objDB.DocID = Docid;
                        objDB.Notes = txtNotes.Value;
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.AddDocNotes();

                        if (res == "New Holiday Added") { Common.addlog("Add", "HR", "New Holiday \"" + objDB.HolidayTitle + "\" Added", "Holidays"); }
                        if (res == "Holiday Data Updated") { Common.addlog("Update", "HR", "Holiday \"" + objDB.HolidayTitle + "\" Updated", "Holidays", objDB.HolidayID); }
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                        pAlertMsg.InnerHtml = res;
                    }
                    else
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = res;
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "Date can not be less than today's";
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private string trimString(string date)
        {
            string trimDate = "";

            for (int i = 0; i < date.Length; i++)
            {
                if (date[i] != '/')
                {
                    trimDate += date[i];
                }
            }
            return trimDate;
        }

        private void clearFields()
        {
            txtHolidayTitle.Value = "";
            txtStartDate.Value = "";
            txtEndDate.Value = "";
            chkSpecialHoliday.Checked = false;
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "Holidays", "HolidayID", HttpContext.Current.Items["HolidayID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "Holiday of ID\"" + HttpContext.Current.Items["HolidayID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/holidays", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/holidays/edit-holiday-" + HttpContext.Current.Items["HolidayID"].ToString(), "Holiday \"" + txtHolidayTitle.Value + "\"", "Holidays", "Holidays", Convert.ToInt32(HttpContext.Current.Items["HolidayID"].ToString()));

                if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
                {
                    objDB.HolidayID = HolidayID;
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    string ss = objDB.AddHolidaysToEmployeeAttendanceKT();
                }
                //Common.addlog("Delete", "HR", "Holiday of ID \"" + objDB.HolidayID + "\" deleted", "Holidays", objDB.HolidayID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

            CheckAccess();
        }


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "Holidays", "HolidayID", HttpContext.Current.Items["HolidayID"].ToString(), Session["UserName"].ToString());
                Common.addlog(res, "HR", "Holiday of ID\"" + HttpContext.Current.Items["HolidayID"].ToString() + "\" Status Changed", "Holidays", HolidayID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        protected void btnReject_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                objDB.HolidayID = HolidayID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteHoliday();
                Common.addlog("Holiday Rejected", "HR", "Holiday of ID\"" + HttpContext.Current.Items["HolidayID"].ToString() + "\" Holiday Rejected", "Holidays", HolidayID);

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = "Holiday Rejected";
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

    }
}