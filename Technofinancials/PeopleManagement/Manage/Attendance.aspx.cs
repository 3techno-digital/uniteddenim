﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class Attendance : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        string AttendanceDate = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    //btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/manage/promotions";

                    divAlertMsg.Visible = false;

                    if (Session["EmployeeID"] != null)
                    {
                        BindEmployeesDropDown();
                        getEmployeeAttendanceByEmployeeID(Convert.ToInt32(Session["EmployeeID"].ToString()));

                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void BindEmployeesDropDown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                ddlEmployees.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddlEmployees.DataTextField = "EmployeeName";
                ddlEmployees.DataValueField = "EmployeeID";
                ddlEmployees.DataBind();
                ddlEmployees.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void getEmployeeAttendanceByEmployeeID(int EmpID)
        {
            
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";
                objDB.EmployeeID = int.Parse(ddlEmployees.SelectedItem.Value);
                if (AttendanceDate == "")
                {
                    AttendanceDate = DateTime.Parse(txtDate.Value).ToShortDateString();
                }
                objDB.TimeIn = AttendanceDate + " " + txtTimeIn.Value;
                objDB.TimeOUt = AttendanceDate + " " + txtTimeOut.Value;
                objDB.LateReason = txtLateReasion.Value;
                objDB.Resultant = txtResultant.Value;
              
                int attendanceID = 0;
               DataTable dt = objDB.UpdateEmployeeAttendance(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count>0)
                    {
                        res = dt.Rows[0]["Message"].ToString();
                        attendanceID = Convert.ToInt32(dt.Rows[0]["AttendanceID"].ToString());
                    }
                }


                objDB.DocType = "ManageAttendance";
                objDB.DocID = attendanceID;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();

  
                if (res == "Attendance Data Updated" || res == "Attendance Added")
                {
                    if (res == "Attendance Added") { Common.addlog("Add", "HR", "Attendance Added of Employ\"" + ddlEmployees.SelectedItem.Text, "EmployeeAttendance"); }
                    if (res == "Attendance Data Updated") { Common.addlog("Update", "HR", "Attendance Data Updated of Employ \"" + ddlEmployees.SelectedItem.Text, "EmployeeAttendance", attendanceID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            getEmployeeAttendanceByEmployeeID(Convert.ToInt32(Session["EmployeeID"].ToString()));
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {

                objDB.EmployeeID = int.Parse(ddlEmployees.SelectedItem.Value);
                objDB.date = txtDate.Value;
                DataTable dt = objDB.GetEmployeeAttendanceByDate(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["TimeIn"].ToString() != "")
                            txtTimeIn.Value = DateTime.Parse(dt.Rows[0]["TimeIn"].ToString()).ToString("HH:mm tt");
                        if (dt.Rows[0]["TimeOut"].ToString() != "")
                            txtTimeOut.Value = DateTime.Parse(dt.Rows[0]["TimeOut"].ToString()).ToString("HH:mm tt");
                        AttendanceDate = DateTime.Parse(dt.Rows[0]["TimeIn"].ToString()).ToString("dd-MMM-yyyy");
                        txtLateReasion.Value = dt.Rows[0]["LateReason"].ToString();
                        txtResultant.Value = dt.Rows[0]["Resultant"].ToString();
                    }
                    else
                    {
                        clearFields();
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Data Found";

                    }
                    Common.addlog("View", "HR", "Attendance of \"" + ddlEmployees.SelectedItem.Text + "\" Viewed by date :" + objDB.date, "EmployeeAttendance");

                }

                else
                {
                    clearFields();
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Data Found";

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void clearFields()
        {
            txtTimeIn.Value = "";
            txtTimeOut.Value = "";
            txtLateReasion.Value = "";
            txtResultant.Value = "";
            AttendanceDate = "";
        }

    }
}