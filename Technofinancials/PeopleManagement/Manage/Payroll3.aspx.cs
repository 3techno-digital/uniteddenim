﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class Payroll3 : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected int NewPayrollID
        {
            get
            {
                if (ViewState["NewPayrollID"] != null)
                {
                    return (int)ViewState["NewPayrollID"];
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                ViewState["NewPayrollID"] = value;
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/payrolls";

                    divAlertMsg.Visible = false;
                    clearFields();

                    ViewState["dtEmployeeSalaries"] = null;

                    btnApproveByHR.Visible = false;
                    btnReview.Visible = false;
                    btnRevApproveByHR.Visible = false;
                    lnkReject.Visible = false;
                    lnkDelete.Visible = false;
                    btnSubForReview.Visible = false;
                    btnDisapprove.Visible = false;
                    btnRejDisApprove.Visible = false;




                    if (HttpContext.Current.Items["PayrollID"] != null)
                    {
                        NewPayrollID = Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString());
                        getNewPayrollByID(NewPayrollID);
                        txtPayrollDate.Enabled = false;
                        btnView.Visible = false;
                        CheckAccess();
                    }
                    else
                    {
                        //SalarySrNo = 1;
                        //calculateEmployeeSalaries();
                        //CheckAccess();
                    }
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckAccess()
        {
            try
            {
                btnSave.Visible = false;
                btnApproveByHR.Visible = false;
                btnReview.Visible = false;
                btnRevApproveByHR.Visible = false;
                lnkReject.Visible = false;
                lnkDelete.Visible = false;
                btnSubForReview.Visible = false;
                btnDisapprove.Visible = false;
                btnRejDisApprove.Visible = false;


                objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
                objDB.TableName = "NewPayroll";
                objDB.PrimaryColumnnName = "NewPayrollID";
                objDB.PrimaryColumnValue = NewPayrollID.ToString();
                objDB.DocName = "Payroll";

                string chkAccessLevel = objDB.CheckDocAccessLevel(Convert.ToInt32(Session["UserID"].ToString()));
                if (chkAccessLevel == "Can Edit")
                {
                    btnSave.Visible = true;
                    lnkDelete.Visible = true;
                    btnSubForReview.Visible = true;
                }
                if (chkAccessLevel == "Can Edit & Review")
                {
                    btnSave.Visible = true;
                    btnReview.Visible = true;
                    lnkReject.Visible = true;

                }
                if (chkAccessLevel == "Can Edit & Approve")
                {
                    btnSave.Visible = true;
                    btnApproveByHR.Visible = true;
                    btnDisapprove.Visible = true;
                }
                if (chkAccessLevel == "Can Edit, Review & Approve")
                {
                    btnSave.Visible = true;
                    btnRevApproveByHR.Visible = true;

                    btnRejDisApprove.Visible = true;
                }
                if (chkAccessLevel == "View & Edit")
                {
                    btnSave.Visible = true;
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private DataTable dtEmployeeSalaries
        {
            get
            {
                if (ViewState["dtEmployeeSalaries"] != null)
                {
                    return (DataTable)ViewState["dtEmployeeSalaries"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtEmployeeSalaries"] = value;
            }
        }

        private DataTable createPayrollTable()
        {
            objDB.CompanyID = 0;
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.PayrollDate = "01-Jan-2020";
            return objDB.GeneratePayroll(ref errorMsg);

        }

        private void getNewPayrollByID(int NewPayrollID)
        {
            DataTable dt = new DataTable();
            objDB.NewPayrollID = NewPayrollID;
            dt = objDB.GetNewPayrollByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    txtPayrollDate.Text = DateTime.Parse(dt.Rows[0]["NewPayrollDate"].ToString()).ToString("MMM-yyyy");

                    //objDB.DocID = NewPayrollID;
                    //objDB.DocType = "NewPayroll";
                    //ltrNotesTable.Text = objDB.GetDocNotes();

                    getNewPayrollDetailsByParollID(Convert.ToInt32(dt.Rows[0]["NewPayrollID"].ToString()));
                }
            }
            Common.addlog("View", "HR", "NewPayroll \"" + txtPayrollDate.Text + "\" Viewed", "NewPayroll", objDB.NewPayrollID);

        }
        private void getNewPayrollDetailsByParollID(int NewPayrollID)
        {
            DataTable dt = new DataTable();
            objDB.NewPayrollID = NewPayrollID;
            dt = objDB.GetNewPayrollDetailsByNewPayrollID(ref errorMsg);
            dtEmployeeSalaries = dt;

            BindData();

            //gvSalaries.DataSource = dt;
            //gvSalaries.DataBind();
            //if (dt != null)
            //{
            //    if (dt.Rows.Count > 0)
            //    {
            //        gvSalaries.UseAccessibleHeader = true;
            //        gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
            //    }
            //}
        }

        private void clearFields()
        {
            txtPayrollDate.Text = "";
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {

                CheckSessions();
                string res = "";
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.PayrollDate = "01-" + txtPayrollDate.Text;


                if (HttpContext.Current.Items["PayrollID"] != null)
                {
                    objDB.ModifiedBy = Session["UserName"].ToString();
                    objDB.NewPayrollID = NewPayrollID;

                    objDB.UpdateNewPayroll();
                    res = "NewPayroll Data Updated";
                }
                else
                {
                    objDB.CreatedBy = Session["UserName"].ToString();
                    NewPayrollID = Convert.ToInt32(objDB.AddNewPayroll());
                    res = "New NewPayroll Added";
                }


                objDB.DocType = "NewPayroll";
                objDB.DocID = NewPayrollID;
                objDB.Notes = txtNotes.Value;
                objDB.CreatedBy = Session["UserName"].ToString();
                objDB.AddDocNotes();
                objDB.NewPayrollID = NewPayrollID;
                objDB.DeletedBy = Session["UserName"].ToString();
                objDB.DeleteNewPayrollDetails();

                dtEmployeeSalaries = (DataTable)ViewState["dtEmployeeSalaries"] as DataTable;
                if (dtEmployeeSalaries != null)
                {
                    if (dtEmployeeSalaries.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                        {

                            objDB.NewPayrollID = Convert.ToInt32(NewPayrollID);
                            objDB.EmployeeID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["EmployeeID"].ToString());
                            objDB.EmployeeCode = (dtEmployeeSalaries.Rows[i]["EmployeeCode"].ToString());
                            objDB.BasicSalary = float.Parse(dtEmployeeSalaries.Rows[i]["BasicSalary"].ToString());
                            objDB.EmployeeName = (dtEmployeeSalaries.Rows[i]["EmployeeName"].ToString());
                            objDB.OverTime = float.Parse(dtEmployeeSalaries.Rows[i]["OverTime"].ToString());
                            objDB.OverTimeAmount = float.Parse(dtEmployeeSalaries.Rows[i]["OverTimeAmount"].ToString());
                            objDB.Maintenance = float.Parse(dtEmployeeSalaries.Rows[i]["Maintenance"].ToString());
                            objDB.FoodAllowence = float.Parse(dtEmployeeSalaries.Rows[i]["FoodAllowence"].ToString());
                            objDB.Maintenance = float.Parse(dtEmployeeSalaries.Rows[i]["Maintenance"].ToString());
                            objDB.Fuel = float.Parse(dtEmployeeSalaries.Rows[i]["Fuel"].ToString());
                            objDB.AbsentAmount = float.Parse(dtEmployeeSalaries.Rows[i]["AbsentAmount"].ToString());
                            objDB.GrossSalary = float.Parse(dtEmployeeSalaries.Rows[i]["GrossSalary"].ToString());
                            objDB.Tax = float.Parse(dtEmployeeSalaries.Rows[i]["Tax"].ToString());
                            objDB.GrossSalaryWithOT = float.Parse(dtEmployeeSalaries.Rows[i]["GrossSalaryWithOT"].ToString());
                            objDB.EOBI = float.Parse(dtEmployeeSalaries.Rows[i]["EOBI"].ToString());
                            objDB.Advance = float.Parse(dtEmployeeSalaries.Rows[i]["Advance"].ToString());
                            objDB.TotalSalry = float.Parse(dtEmployeeSalaries.Rows[i]["TotalSalry"].ToString());
                            objDB.TotalSalryWithOT = float.Parse(dtEmployeeSalaries.Rows[i]["TotalSalryWithOT"].ToString());
                            objDB.COA_ID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["COA_ID"].ToString());
                            objDB.PayrolllExpCOA = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PayrolllExpCOA"].ToString());
                            objDB.EMP_COA = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["EMP_COA"].ToString());
                            objDB.TotalSalry2 = float.Parse(dtEmployeeSalaries.Rows[i]["TotalSalry2"].ToString());
                            objDB.OverTimeTotal = float.Parse(dtEmployeeSalaries.Rows[i]["OverTimeTotal"].ToString());
                            objDB.GrossSalary2 = float.Parse(dtEmployeeSalaries.Rows[i]["GrossSalary2"].ToString());
                            objDB.AddOnsAmount = float.Parse(dtEmployeeSalaries.Rows[i]["AddOns"].ToString());

                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddNewPayrollDetailsKT();
                        }
                    }
                }


                if (res == "New NewPayroll Added" || res == "NewPayroll Data Updated")
                {
                    if (res == "New NewPayroll Added") { Common.addlog("Add", "HR", "New NewPayroll \"" + objDB.PayrollDate + "\" Added", "NewPayroll"); }
                    if (res == "NewPayroll Data Updated") { Common.addlog("Update", "HR", "NewPayroll \"" + objDB.PayrollDate + "\" Updated", "NewPayroll", objDB.NewPayrollID); }

                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnPDF_ServerClickOld(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "NewPayroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##DATE##", DateTime.Now.ToString("dd-MMM-yyyy"));
                //content = content.Replace("##MONTH##", ddlMonth.SelectedItem.Text);
                //content = content.Replace("##YEAR##", ddlYear.SelectedItem.Text);

                StringBuilder sbTable = new StringBuilder();
                sbTable.Append("<table style='width:100%;'>");
                sbTable.Append("<thead><tr><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Sr. No</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Employee Code</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Employee</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Department</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Designation</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Bank Details</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Basic Salary</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Allowances</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Deductions</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Net Salary</td></tr></thead>");
                if (dtEmployeeSalaries != null)
                {
                    if (dtEmployeeSalaries.Rows.Count > 0)
                    {
                        float totalBasicSalaries = 0;
                        float totalAllowances = 0;
                        float totalDeductions = 0;
                        float totalNetSalaries = 0;
                        sbTable.Append("<tbody>");
                        for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                        {
                            sbTable.Append("<tr><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + (i + 1) + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["EmployeeCode"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["EmployeeName"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Department"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Designation"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["BankDetails"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["BasicSalary"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Allowances"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["Deductions"] + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #fdfdfd;color: #000;text-align: left;'>" + dtEmployeeSalaries.Rows[i]["NetSalary"] + "</td></tr>");
                            totalBasicSalaries += float.Parse(dtEmployeeSalaries.Rows[i][5].ToString());
                            totalAllowances += float.Parse(dtEmployeeSalaries.Rows[i][6].ToString());
                            totalDeductions += float.Parse(dtEmployeeSalaries.Rows[i][7].ToString());
                            totalNetSalaries += float.Parse(dtEmployeeSalaries.Rows[i][8].ToString());
                        }
                        sbTable.Append("</tbody>");
                        sbTable.Append("<tfoot>");
                        sbTable.Append("<tr><td colspan='6' style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>Net Calculations:</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalBasicSalaries + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalAllowances + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left;'>" + totalDeductions + "</td><td style='border: 0px solid #ddd !important;border-bottom: 1px solid #f6f9ff !important;border-right: 1px solid #f6f9ff !important;padding: 8px;line-height: 1.428571429;    vertical-align: top;    background-color: #188ae2;color: #fff;text-align: left; font-weight:bold;'>" + totalNetSalaries + "</td></tr>");
                        sbTable.Append("</tfoot>");
                    }
                }
                sbTable.Append("</table>");
                content = content.Replace("##TABLE##", sbTable.ToString());

                //IronPdf.HtmlToPdf Renderer = new IronPdf.HtmlToPdf();
                //Renderer.PrintOptions.Header = new HtmlHeaderFooter()
                //{
                //    HtmlFragment = header
                //};
                //Renderer.PrintOptions.Footer = new HtmlHeaderFooter()
                //{
                //    HtmlFragment = footer
                //};
                //Renderer.PrintOptions.MarginTop = 50;
                //Renderer.PrintOptions.MarginLeft = 20;
                //Renderer.PrintOptions.MarginRight = 20;
                //Renderer.PrintOptions.MarginBottom = 20;

                //Renderer.PrintOptions.FitToPaperWidth = true;

                //Renderer.RenderHtmlAsPdf(content).Print();
                Common.addlog("Report", "HR", "NewPayroll Report Generated", "NewPayroll");

                Common.generatePDF(header, footer, content, "NewPayroll Sheet (" + txtPayrollDate.Text + ")", "A4", "Portrait");

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Payroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##MONTH##", txtPayrollDate.Text);

                string sbTable = Common.GetTemplate(gvSalaries);

                content = content.Replace("##TABLE##", sbTable.ToString());

                Common.addlog("Report", "HR", "NewPayroll Report Generated", "NewPayroll");

                Common.generatePDF(header, footer, content, "NewPayroll Sheet", "A4", "Landscape");


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnPDFNormalSal_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Payroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##MONTH##", txtPayrollDate.Text);

                string sbTable = Common.GetTemplate(gvNormalSal);

                content = content.Replace("##TABLE##", sbTable.ToString());

                Common.addlog("Report", "HR", "NewPayroll Report Generated", "NewPayroll");

                Common.generatePDF(header, footer, content, "NewPayroll Sheet", "A4", "Landscape");


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void btnPDFOverTime_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Payroll";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##MONTH##", txtPayrollDate.Text);

                string sbTable = Common.GetTemplate(gvOverTime);

                content = content.Replace("##TABLE##", sbTable.ToString());

                Common.addlog("Report", "HR", "NewPayroll Report Generated", "NewPayroll");

                Common.generatePDF(header, footer, content, "NewPayroll Sheet", "A4", "Landscape");


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }


        protected void Button1_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
                string res = Common.addAccessLevels(btn.ID.ToString(), "NewPayroll", "NewPayrollID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlogNew(res, "HR", "Payroll of ID\"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/payrolls", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/Payrolls/edit-spotlit-payroll-" + HttpContext.Current.Items["PayrollID"].ToString(), "Payroll of Date \"" + txtPayrollDate.Text + "\"", "NewPayroll", "Payroll", Convert.ToInt32(HttpContext.Current.Items["PayrollID"].ToString()));

                //Common.addlog(res, "HR", "NewPayroll of ID\"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" Status Changed", "NewPayroll", NewPayrollID);
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;

                if (btn.ID.ToString() == "btnApproveByHR" || btn.ID.ToString() == "btnRevApproveByHR")
                {
                    //if (dtEmployeeSalaries != null)
                    //{
                    //    for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                    //    {

                    //        objDB.PFID = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PFID"].ToString());
                    //        objDB.PFCompanyAmount = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PFCompany"].ToString());
                    //        objDB.PFEmpoyeeAmount = Convert.ToInt32(dtEmployeeSalaries.Rows[i]["PFEmployee"].ToString());
                    //        objDB.PFYear = ddlYear.SelectedValue;
                    //        objDB.PFMonth = ddlMonth.SelectedValue;
                    //        objDB.CreatedBy = Session["UserName"].ToString();
                    //        objDB.AddPFDeduction();
                    //    }

                    //}
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }




            CheckAccess();
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                LinkButton btn = (LinkButton)sender as LinkButton;
                string type = btn.CommandArgument;
                string res = Common.addAccessLevels(type, "NewPayroll", "NewPayrollID", HttpContext.Current.Items["PayrollID"].ToString(), Session["UserName"].ToString());
                Common.addlog("Delete", "HR", "NewPayroll of ID \"" + HttpContext.Current.Items["PayrollID"].ToString() + "\" deleted", "NewPayroll", NewPayrollID);

                string ss = objDB.UpdateAddOnsStatusByPayrollID();

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
                Response.Redirect(btnBack.HRef);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }



        }

        protected void txtPayrollDate_ServerChange(object sender, EventArgs e)
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"]);
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.PayrollDate = ("01-" + txtPayrollDate.Text);
            dt = objDB.GeneratePayroll(ref errorMsg);
            dtEmployeeSalaries = dt;

            BindData();

            //gvSalaries.DataSource = dt;
            //gvSalaries.DataBind();
            //if (dt != null)
            //{
            //    if (dt.Rows.Count > 0)
            //    {
            //        gvSalaries.UseAccessibleHeader = true;
            //        gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
            //    }
            //}
        }



        protected void gvSalaries_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {

                int index = e.RowIndex;

                dtEmployeeSalaries.Rows[index].SetField("BasicSalary", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditBasicSalary")).Text);
                dtEmployeeSalaries.Rows[index].SetField("OverTime", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditOverTime")).Text);
                dtEmployeeSalaries.Rows[index].SetField("OverTimeAmount", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditOverTimeAmount")).Text);
                dtEmployeeSalaries.Rows[index].SetField("FoodAllowence", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditFoodAllowence")).Text);
                dtEmployeeSalaries.Rows[index].SetField("Maintenance", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditMaintenance")).Text);
                dtEmployeeSalaries.Rows[index].SetField("Fuel", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditFuel")).Text);
                dtEmployeeSalaries.Rows[index].SetField("AbsentAmount", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditAbsentAmount")).Text);
                dtEmployeeSalaries.Rows[index].SetField("Tax", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditTax")).Text);
                dtEmployeeSalaries.Rows[index].SetField("EOBI", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditEOBI")).Text);
                dtEmployeeSalaries.Rows[index].SetField("Advance", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditAdvance")).Text);
                dtEmployeeSalaries.Rows[index].SetField("AddOns", ((TextBox)gvSalaries.Rows[e.RowIndex].FindControl("txtEditAddOns")).Text);
                dtEmployeeSalaries.AcceptChanges();



                dtEmployeeSalaries.Rows[index]["GrossSalary"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["BasicSalary"].ToString()) + Convert.ToDouble(dtEmployeeSalaries.Rows[index]["AddOns"].ToString()) + Convert.ToDouble(dtEmployeeSalaries.Rows[index]["FoodAllowence"].ToString()) + Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Maintenance"].ToString()) + Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Fuel"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["AbsentAmount"].ToString());
                dtEmployeeSalaries.AcceptChanges();


                dtEmployeeSalaries.Rows[index]["GrossSalary2"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["BasicSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["AbsentAmount"].ToString());
                dtEmployeeSalaries.AcceptChanges();

                dtEmployeeSalaries.Rows[index]["OverTimeTotal"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["OverTimeAmount"].ToString()) + Convert.ToDouble(dtEmployeeSalaries.Rows[index]["FoodAllowence"].ToString()) + Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Maintenance"].ToString()) + Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Fuel"].ToString());
                dtEmployeeSalaries.AcceptChanges();

                dtEmployeeSalaries.Rows[index]["GrossSalaryWithOT"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) + Convert.ToDouble(dtEmployeeSalaries.Rows[index]["OverTimeAmount"].ToString());
                dtEmployeeSalaries.AcceptChanges();


                dtEmployeeSalaries.Rows[index]["TotalSalry"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString());
                dtEmployeeSalaries.AcceptChanges();

                dtEmployeeSalaries.Rows[index]["TotalSalry2"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalary2"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString());
                dtEmployeeSalaries.AcceptChanges();


                dtEmployeeSalaries.Rows[index]["TotalSalryWithOT"] = Convert.ToDouble(dtEmployeeSalaries.Rows[index]["GrossSalaryWithOT"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["EOBI"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Advance"].ToString()) - Convert.ToDouble(dtEmployeeSalaries.Rows[index]["Tax"].ToString());
                dtEmployeeSalaries.AcceptChanges();

                gvSalaries.EditIndex = -1;
                BindData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void gvSalaries_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSalaries.EditIndex = -1;
            BindData();
        }
        protected void gvSalaries_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSalaries.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void gvSalaries_lnkRemove_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();

            if (dtEmployeeSalaries != null)
            {
                for (int i = 0; i < dtEmployeeSalaries.Rows.Count; i++)
                {
                    if (dtEmployeeSalaries.Rows[i]["EmployeeID"].ToString() == delSr)
                    {
                        dtEmployeeSalaries.Rows[i].Delete();
                        dtEmployeeSalaries.AcceptChanges();
                        break;
                    }
                }

            }

            BindData();

        }

        protected void BindData()
        {
            gvSalaries.DataSource = dtEmployeeSalaries;
            gvSalaries.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    gvSalaries.UseAccessibleHeader = true;
                    gvSalaries.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }


            gvNormalSal.DataSource = dtEmployeeSalaries;
            gvNormalSal.DataBind();
            if (dtEmployeeSalaries != null)
            {
                if (dtEmployeeSalaries.Rows.Count > 0)
                {
                    gvNormalSal.UseAccessibleHeader = true;
                    gvNormalSal.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }




            DataTable filteredTable = dtEmployeeSalaries.Clone();
            var rows = dtEmployeeSalaries.AsEnumerable()
                .Where(r => r.Field<double>("OverTimeAmount") != 0);
            if (rows.Any())
                filteredTable = rows.CopyToDataTable();

            gvOverTime.DataSource = filteredTable;
            gvOverTime.DataBind();
            if (filteredTable != null)
            {
                if (filteredTable.Rows.Count > 0)
                {
                    gvOverTime.UseAccessibleHeader = true;
                    gvOverTime.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

        }


    }
}