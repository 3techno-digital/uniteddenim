﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Attendance.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.Attendance" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>

    <body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="UpdatePanel1"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Manage Attendance</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                  
                                    <button type="button" class="AD_btn" data-toggle="modal" data-target="#notes-modal" value="Add Note">Note </button>
                                    <button class="AD_btn" id="Button1" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button">Save</button>
                                    <%--<a class="AD_btn" id="btnBack" runat="server">Back</a>--%>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>


                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <asp:UpdatePanel ID="UpdPnl" runat="server">
                                    <ContentTemplate>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <h4>Employee
                                                     <span style="color: red !important;">*</span>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlEmployees" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                <asp:DropDownList ID="ddlEmployees" runat="server" class=" form-control select2" data-plugin="select2" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Date
                                             <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <input type="text" data-date-format="DD-MMM-YYYY" class="form-control datetime-picker" id="txtDate" runat="server" />

                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Time In
                                             <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtTimeIn" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <input type="text" class="form-control" data-plugin="timepicker" id="txtTimeIn" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Time Out <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtTimeOut" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />--%></h4>
                                        <input type="text" class="form-control" data-plugin="timepicker" id="txtTimeOut" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Late Reason <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtLateReasion" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />--%></h4>
                                        <input type="text" class="form-control" name="accountTitle" id="txtLateReasion" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Resultant <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtResultant" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />--%></h4>
                                        <input type="text" class="form-control" name="accountTitle" id="txtResultant" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4></h4><br />
                                    <button class=" AD_btn_inn"  id="btnView" runat="server" onserverclick="btnView_ServerClick" type="button">View</button>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group" id="divAlertMsg" runat="server">
                                            <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                <span>
                                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                </span>
                                                <p id="pAlertMsg" runat="server">
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>


                                            <!-- Modal -->
                                            <div class="modal fade M_set" id="notes-modal" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h1 class="m-0 text-dark">Notes</h1>
                                                            <div class="add_new">
                                                                <button type="button" class="AD_btn" data-dismiss="modal">Save</button>
                                                                <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>
                                                                <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                            </p>
                                                            <p>
                                                                <textarea id="txtNotes" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                            </p>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">

                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                
                            </div>
                        </div>
                    </div>

                </section>

                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    <style>
        .tf-alert-danger {
            padding: 26px;
        }

        .tf-alert-success {
            padding: 26px;
        }

        #divAlertTheme {
            margin-top: 26px;
        }

            #divAlertTheme span {
                position: absolute !important;
                left: 30px !important;
                top: 40px !important;
                padding: 0px !important;
            }

        .tf-alert-danger p {
            display: inline-block;
            margin-top: -7px;
            position: absolute;
            margin-left: 30px;
        }

        .tf-alert-success p {
            display: inline-block;
            margin-top: -7px;
            position: absolute;
            margin-left: 30px;
        }
    </style>
</body>

</html>
