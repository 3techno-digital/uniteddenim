﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeBonus.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.EmployeeBonus" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="btnUpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="wrap">
                <asp:UpdatePanel ID="upd1" runat="server">

                    <ContentTemplate>
                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <img src="/assets/images/loan.png" class="img-responsive tf-page-heading-img" />
                                    <h3 class="tf-page-heading-text">Employee Bonus</h3>
                                </div>

                                <div class="col-md-4">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>


                                <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                    <ContentTemplate>


                                        <div class="col-sm-4">
                                            <div class="pull-right flex">
                                                <button type="button" class="tf-save-btn" data-toggle="modal" data-target="#notes-modal" value="Note"><i class="fa fa-sticky-note-o"></i></button>
                                                <button class="tf-save-btn"  id="btnRevApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                                <button class="tf-save-btn" id="btnReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-check-square"></i></button>
                                                <button class="tf-save-btn"  id="btnApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-up"></i></button>
                                                <asp:LinkButton ID="lnkReject" runat="server" CssClass="tf-save-btn tf-del delete-class" CommandArgument='Reject' OnClick="lnkDelete_Click"><i class="far fa-times"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="tf-save-btn tf-del delete-class"  CommandArgument='Delete' OnClick="lnkDelete_Click"><i class="far fa-trash"></i></asp:LinkButton>
                                                <button class="tf-save-btn"  id="btnDisapprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                                <button class="tf-save-btn"  id="btnSubForReview" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-paper-plane"></i></button>
                                                <button class="tf-save-btn" id="btnRejDisApprove" runat="server" onserverclick="Button1_ServerClick" validationgroup="btnValidate" type="button"><i class="fas fa-thumbs-down"></i></button>
                                                <button class="tf-save-btn"  id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                                <a class="tf-back-btn"  id="btnBack" runat="server"><i class="fas fa-arrow-left"></i></a>
                                            </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="notes-modal" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Notes</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>
                                                            <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                                        </p>
                                                        <p>
                                                            <textarea id="Textarea1" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                                        </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Save & Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr />
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-sm-12">
                                    <div class="tab-content ">
                                        <div class="tab-pane active row">
                                            <div class="col-sm-12">
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <h4>Title <span style="color: red !important;">*</span>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtTitle" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                        <input class="form-control" id="txtTitle" placeholder="Title" type="text" runat="server" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h4>Grant Date  <span style="color: red !important;">*</span>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtGrantDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                        <input class="form-control" id="txtGrantDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" placeholder="Grant Date" type="text" runat="server" />
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <h4>Description</h4>
                                                        <textarea class="form-control" id="txtDescription" placeholder="Description" type="text" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="clearfix">&nbsp;</div>
                                                <h3>Select Employees</h3>

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:GridView ID="gvEmployeeBounusDetail" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvEmployeeBounusDetail_RowDataBound" OnRowUpdating="gvEmployeeBounusDetail_RowUpdating" OnRowCancelingEdit="gvEmployeeBounusDetail_RowCancelingEdit" OnRowEditing="gvEmployeeBounusDetail_RowEditing">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Sr. No">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblSrNo" Text='<%# Eval("SrNo") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="txtSrNo" runat="server"></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Employee">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                                    </ItemTemplate>

                                                                    <FooterTemplate>
                                                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator31" ControlToValidate="ddlEmployee" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                        <asp:DropDownList ID="ddlEmployee" runat="server" data-plugin="select2" CssClass="form-control select2"></asp:DropDownList>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Type">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblType" Text='<%# Eval("Type") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:DropDownList runat="server" ID="ddlEditType" CssClass="form-control select2" data-plugin="select2" Text='<%# Eval("Type") %>'>
                                                                            <asp:ListItem>Percentage</asp:ListItem>
                                                                            <asp:ListItem>Amount</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </EditItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:DropDownList runat="server" ID="ddlType" CssClass="form-control select2" data-plugin="select2">
                                                                            <asp:ListItem>Percentage</asp:ListItem>
                                                                            <asp:ListItem>Amount</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Amount">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblAmount" Text='<%# Eval("Amount") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator32" ControlToValidate="txtEditAmount" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidateE" ForeColor="Red" />
                                                                        <asp:TextBox runat="server" ID="txtEditAmount" CssClass="form-control" TextMode="Number" Text='<%# Eval("Amount") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator33" ControlToValidate="txtAmount" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidatef" ForeColor="Red" />
                                                                        <asp:TextBox runat="server" ID="txtAmount" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ShowHeader="false">
                                                                    <EditItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" ValidationGroup="btnValidateE"   Text="Update"></asp:LinkButton>
                                                                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                   <FooterTemplate>
                                                                        <asp:Button ID="btnAddEmployeeBounusDetail" runat="server" CssClass="form-control" Text="Add" ValidationGroup="btnValidatef" CausesValidation="true" OnClick="btnAddEmployeeBounusDetail_Click"></asp:Button>
                                                                    </FooterTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField >

                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%# Eval("SrNo")%>' Text="Delete" OnCommand="lnkRemoveEmployeeBounusDetail_Command"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                              
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="clearfix">&nbsp;</div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .total {
                font-weight: bold;
                font-size: 20px;
                color: #188ae2;
            }
        </style>
        <!-- Modal -->
    </form>
</body>
</html>
