﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PendingRequestsDetails.aspx.cs" Inherits="Technofinancials.PeopleManagement.Manage.PendingRequestsDetails" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>
		.AD_btn_inn {
			padding: 3px 24px;
		}

		.tf-note-btn {
			background-color: #575757;
			padding: 10px 8px 7px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-note-btn i {
				color: #fff !important;
			}

		.tf-disapproved-btn {
			background-color: #575757;
			padding: 10px 8px 7px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-disapproved-btn i {
				color: #fff !important;
			}


		.tf-add-btn {
			background-color: #575757;
			padding: 4px 9px 4px 10px !important;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-add-btn i {
				color: #fff !important;
			}

		.tf-add-btn {
			background-color: #575757;
			padding: 12px 10px 8px 10px !important;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-add-btn i {
				color: #fff !important;
			}

		.tf-back-btn {
			background-color: #575757;
			padding: 10px 10px 10px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
			padding: 5px !important;
			height: 38px !important;
			width: 37px !important;
			display: inline-block !important;
			text-align: center !important;
		}

			.tf-back-btn i {
				color: #fff !important;
			}


		.table > thead:first-child > tr:first-child > th {
			font-size: 11px;
		}

		table#gv {
			width: 3000px;
		}

		div div#gv_wrapper::-webkit-scrollbar-thumb {
			background-color: #003780;
			border: 2px solid #003780;
			border-radius: 10px;
		}

		div div#gv_wrapper::-webkit-scrollbar {
			height: 10px;
			background-color: #ffffff;
		}

		div div#gv_wrapper::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
			background-color: #FFF;
		}

		@media only screen and (max-width: 992px) and (min-width: 768px) {
			div div#gv_wrapper {
				overflow-x: scroll;
				overflow-y: hidden;
				padding-bottom: 5rem;
			}
		}

		@media only screen and (max-width: 1024px) and (min-width: 993px) {
			div div#gv_wrapper {
				overflow-x: scroll;
				overflow-y: hidden;
				padding-bottom: 5rem;
			}
		}

		@media only screen and (max-width: 1280px) and (min-width: 800px) {
			div div#gv_wrapper {
				overflow-x: scroll;
				overflow-y: hidden;
				padding-bottom: 5rem;
			}
		}
	</style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">

			<input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
			<div class="wrap">

				<asp:UpdatePanel runat="server">
					<ContentTemplate>
						<div class="content-header">
							<div class="container-fluid">
								<div class="row mb-2">
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
										<h1 class="m-0 text-dark">Pending Request Details</h1>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
										<div style="text-align: right;">
											<a class="AD_btn" id="btnBack" runat="server">Back</a>
										</div>
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.container-fluid -->
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
				<section class="app-content">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<asp:UpdatePanel ID="UpdatePanel4" runat="server">
								<ContentTemplate>
									<div class="form-group" id="divAlertMsg" runat="server">
										<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
											<span>
												<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
											</span>
											<p id="pAlertMsg" runat="server">
											</p>
										</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>
					</div>
					<div class="clear-fix">&nbsp;</div>
					<div class="row ">
						<div class="col-sm-12">
							<div class="tab-content ">
								<div class="tab-pane active row">
									<div class="col-sm-12 gv-overflow-scrool">

										<asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
											<Columns>
												<asp:TemplateField HeaderText="Sr.No.">
													<ItemTemplate>
														<%#Container.DataItemIndex+1 %>
													</ItemTemplate>
												</asp:TemplateField>

												<asp:TemplateField HeaderText="Employee ID" Visible="false">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("EmployeeID") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>


												<asp:TemplateField HeaderText="Employee Code">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol1" Text='<%#  Eval("WDID") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>

												<asp:TemplateField HeaderText="Employee">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("EmployeeName") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>

												<asp:TemplateField HeaderText="Attendance Adjustments">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("AttAdjustmentCount") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Break Adjustments">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("BreakAdjustment") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Addons">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("AddOnsCount") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Expenses">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("OtherExpenseCount") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
											</Columns>
										</asp:GridView>

									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
			<div class="clear-fix">&nbsp;</div>
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>
		<script src="/business/scripts/ViewDesignations.js"></script>
	</form>
</body>
</html>

