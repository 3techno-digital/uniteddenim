﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class MailingList : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/mailing-list";

                divAlertMsg.Visible = false;
                clearFields();



                ViewState["EmployeesSrNo"] = null;
                ViewState["dtEmployees"] = null;
                ViewState["showEmployeesFirstRow"] = null;
                dtEmployees = createEmployees();
                BindEmployeesTable();

                if (HttpContext.Current.Items["MailingListID"] != null)
                {
                    //MailingListID = Convert.ToInt32(HttpContext.Current.Items["MailingListID"].ToString());
                    getMailingListByID(Convert.ToInt32(HttpContext.Current.Items["MailingListID"].ToString()));
                }
            }

            Page.ClientScript.GetPostBackEventReference(this, string.Empty);
            string ctrlName = Request.Params.Get("__EVENTTARGET");
            string ctrlArgs = Request.Params.Get("__EVENTARGUMENT");

        }

        private void getMailingListByID(int MailingListID)
        {
            DataTable dt = new DataTable();
            objDB.MailingListID = MailingListID;
            dt = objDB.GetMailingListByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtTitle.Value = dt.Rows[0]["MailingListTitle"].ToString();

                    objDB.DocID = MailingListID;
                    objDB.DocType = "MailingList";
                    ltrNotesTable.Text = objDB.GetDocNotes();

                    getMailingListEmailsByMailingListID(MailingListID);




                }
            }
            Common.addlog("View", "HR", "Mailing List \"" + txtTitle.Value + "\" Viewed", "MailingLists", objDB.MailingListID);
        }
        private void getMailingListEmailsByMailingListID(int MailingListID)
        {
            dtEmployees = null;
            dtEmployees = new DataTable();
            dtEmployees = createEmployees();

            DataTable dt = new DataTable();
            objDB.MailingListID = MailingListID;
            dt = objDB.GetAllMailingListEmails(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dtEmployees.Rows[0][0].ToString() == "")
                    {
                        dtEmployees.Rows[0].Delete();
                        dtEmployees.AcceptChanges();
                        showEmployeesFirstRow = true;
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtEmployees.Rows.Add(new object[] {
                            i+1,
                            dt.Rows[i]["EmployeeID"],
                            dt.Rows[i]["EmployeeName"]
                        });
                    }
                    EmployeesSrNo = Convert.ToInt32(dtEmployees.Rows[dtEmployees.Rows.Count - 1][0].ToString()) + 1;
                }
            }
            Common.addlog("View", "HR", "Mailing List Emails \"" + txtTitle.Value + "\" Viewed", "MailingListEmails", objDB.MailingListID);
            BindEmployeesTable();
        }

        private void clearFields()
        {
            txtTitle.Value = "";

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            string res = "";
            bool isNew = false;
            int MailingListID = 0;


            dtEmployees = (DataTable)ViewState["dtEmployees"] as DataTable;

            if (dtEmployees != null)
            {
                if ((showEmployeesFirstRow == true && dtEmployees.Rows.Count > 0) || (showEmployeesFirstRow == false && dtEmployees.Rows.Count > 1))
                {
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objDB.MailingListTitle = txtTitle.Value;

                    if (HttpContext.Current.Items["MailingListID"] != null)
                    {
                        MailingListID = Convert.ToInt32(HttpContext.Current.Items["MailingListID"].ToString());
                        objDB.ModifiedBy = Session["UserName"].ToString();
                        objDB.MailingListID = MailingListID;
                        res = objDB.UpdateMailingList();
                    }
                    else
                    {
                        objDB.CreatedBy = Session["UserName"].ToString();

                        MailingListID = 0;
                        res = objDB.AddMailingList();
                        if (int.TryParse(res, out MailingListID))
                        {
                            res = "New MailingList Added";
                            isNew = true;
                        }

                    }


                    if (res == "New MailingList Added" || res == "MailingList Data Updated")
                    {


                        objDB.DocType = "MailingList";
                        objDB.DocID = MailingListID;
                        objDB.Notes = txtNotes.Value;
                        objDB.CreatedBy = Session["UserName"].ToString();
                        objDB.AddDocNotes();

                        objDB.MailingListID = MailingListID;
                        objDB.DeletedBy = Session["UserName"].ToString();
                        objDB.DeleteMailingListEmails();



                        if (!showEmployeesFirstRow)
                        {
                            dtEmployees.Rows[0].Delete();
                            dtEmployees.AcceptChanges();
                        }

                        for (int i = 0; i < dtEmployees.Rows.Count; i++)
                        {
                            objDB.MailingListID = Convert.ToInt32(MailingListID);
                            objDB.EmployeeID = Convert.ToInt32(dtEmployees.Rows[i]["EmployeeID"].ToString());
                            objDB.CreatedBy = Session["UserName"].ToString();
                            objDB.AddMailingListEmail();
                        }

                    }

                }
                else
                {
                    res = "Employee List Is Empty";
                }
            }
            else
            {
                res = "Employee List Is Empty";
            }

            //objDB.MailingListID = MailingListID;
            //objDB.DeletedBy = Session["UserName"].ToString();


            if (isNew)
            {
                ViewState["EmployeesSrNo"] = null;
                ViewState["dtEmployees"] = null;
                ViewState["showEmployeesFirstRow"] = null;
                dtEmployees = createEmployees();
                clearFields();
            }

            BindEmployeesTable();


            if (res == "New MailingList Added" || res == "MailingList Data Updated")
            {
                if (res == "New MailingList Added") { Common.addlog("Add", "HR", "New Mailing List \"" + objDB.MailingListTitle + "\" Added", "MailingLists"); }
                if (res == "MailingList Data Updated") { Common.addlog("Update", "HR", "Mailing List \"" + objDB.MailingListTitle + "\" Updated", "MailingLists", objDB.MailingListID); }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }

        private DataTable dtEmployees
        {
            get
            {
                if (ViewState["dtEmployees"] != null)
                {
                    return (DataTable)ViewState["dtEmployees"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtEmployees"] = value;
            }
        }
        bool showEmployeesFirstRow
        {
            get
            {
                if (ViewState["showEmployeesFirstRow"] != null)
                {
                    return (bool)ViewState["showEmployeesFirstRow"];
                }
                else
                {
                    return false;
                }
            }
            set
            {
                ViewState["showEmployeesFirstRow"] = value;
            }
        }
        protected int EmployeesSrNo
        {
            get
            {
                if (ViewState["EmployeesSrNo"] != null)
                {
                    return (int)ViewState["EmployeesSrNo"];
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                ViewState["EmployeesSrNo"] = value;
            }
        }
        private DataTable createEmployees()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrNo");
            dt.Columns.Add("EmployeeID");
            dt.Columns.Add("EmployeeName");
            dt.AcceptChanges();

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            dt.AcceptChanges();

            return dt;
        }
        protected void BindEmployeesTable()
        {
            if (ViewState["dtEmployees"] == null)
            {
                dtEmployees = createEmployees();
                ViewState["dtEmployees"] = dtEmployees;
            }

            gvEmployees.DataSource = dtEmployees;
            gvEmployees.DataBind();

            if (showEmployeesFirstRow)
                gvEmployees.Rows[0].Visible = true;
            else
                gvEmployees.Rows[0].Visible = false;

            gvEmployees.UseAccessibleHeader = true;
            gvEmployees.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void gvEmployees_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label txtSrNo = e.Row.FindControl("txtSrNo") as Label;
                txtSrNo.Text = EmployeesSrNo.ToString();

                DropDownList ddList = (DropDownList)e.Row.FindControl("ddlEmployees");
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddList.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddList.DataTextField = "EmployeeName";
                ddList.DataValueField = "EmployeeID";
                ddList.DataBind();
            }
        }
        protected void lnkRemoveEmployee_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender as LinkButton;
            string delSr = lnk.CommandArgument.ToString();
            for (int i = 0; i < dtEmployees.Rows.Count; i++)
            {
                if (dtEmployees.Rows[i][0].ToString() == delSr)
                {
                    dtEmployees.Rows[i].Delete();
                    dtEmployees.AcceptChanges();
                }
            }
            for (int i = 0; i < dtEmployees.Rows.Count; i++)
            {
                dtEmployees.Rows[i].SetField(0, i + 1);
                dtEmployees.AcceptChanges();
            }
            if (dtEmployees.Rows.Count < 1)
            {
                DataRow dr = dtEmployees.NewRow();
                dtEmployees.Rows.Add(dr);
                dtEmployees.AcceptChanges();
                showEmployeesFirstRow = false;
            }
            if (showEmployeesFirstRow)
                EmployeesSrNo = dtEmployees.Rows.Count + 1;
            else
                EmployeesSrNo = 1;

            BindEmployeesTable();
        }
        protected void btnAddEmployee_Click(object sender, EventArgs e)
        {
            divAlertMsg.Visible = false;

            string EmployeeId = ((DropDownList)gvEmployees.FooterRow.FindControl("ddlEmployees")).SelectedItem.Value;
            bool flag = true;
            for (int i = 0; i < dtEmployees.Rows.Count; i++)
            {
                if (EmployeeId == dtEmployees.Rows[i]["EmployeeID"].ToString())
                {
                    flag = false;
                    break;
                }
            }
            if (flag)
            {
                DataRow dr = dtEmployees.NewRow();
                dr[0] = EmployeesSrNo.ToString();
                dr[1] = EmployeeId;
                dr[2] = ((DropDownList)gvEmployees.FooterRow.FindControl("ddlEmployees")).SelectedItem.Text;
                dtEmployees.Rows.Add(dr);
                dtEmployees.AcceptChanges();


                if (dtEmployees.Rows[0][0].ToString() == "")
                {
                    dtEmployees.Rows[0].Delete();
                    dtEmployees.AcceptChanges();
                    showEmployeesFirstRow = true;
                }


                EmployeesSrNo += 1;
               

            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "Duplicate employee is not allowed";

            }

            BindEmployeesTable();
            ((Label)gvEmployees.FooterRow.FindControl("txtSrNo")).Text = EmployeesSrNo.ToString();
        }
    }
}