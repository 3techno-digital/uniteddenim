﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Manage
{
    public partial class Employees : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected static int candidateID = 0;
        protected static string resume = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                //BindCountriesDropDown();
                //BindStatesDropDown(166);
                //btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/candidates";
                //BindVacanciesDropDown();
                //divAlertMsg.Visible = false;

                //if (HttpContext.Current.Items["CandidateID"] != null)
                //{
                //    candidateID = Convert.ToInt32(HttpContext.Current.Items["CandidateID"].ToString());
                //    getCandidateByID(candidateID);
                //}
            }
        }

        //private void getCandidateByID(int departmentID)
        //{
        //    DataTable dt = new DataTable();
        //    objDB.CandidateID = candidateID;
        //    dt = objDB.GetCandidateByID(ref errorMsg);
        //    if (dt != null)
        //    {
        //        if (dt.Rows.Count > 0)
        //        {
        //            ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));
        //            int countriesIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText(dt.Rows[0]["Country"].ToString()));
        //            if (countriesIndex == -1)
        //                countriesIndex = 0;
        //            BindStatesDropDown(Convert.ToInt32(ddlCountries.Items[countriesIndex].Value));

        //            ddlStates.SelectedIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["State"].ToString()));
        //            int statesIndex = ddlStates.Items.IndexOf(ddlStates.Items.FindByText(dt.Rows[0]["State"].ToString()));
        //            if (statesIndex == -1)
        //                statesIndex = 0;

        //            BindCitiesDropDown(Convert.ToInt32(ddlStates.Items[statesIndex].Value));

        //            ddlCities.SelectedIndex = ddlCities.Items.IndexOf(ddlCities.Items.FindByText(dt.Rows[0]["City"].ToString()));
        //            txtAddressLine1.Value = dt.Rows[0]["AddressLine1"].ToString();
        //            txtAddressLine2.Value = dt.Rows[0]["AddressLine2"].ToString();
        //            txtZIPCode.Value = dt.Rows[0]["ZIPCode"].ToString();

        //            resume = dt.Rows[0]["CV"].ToString();
        //            ddlDesg.SelectedIndex = ddlDesg.Items.IndexOf(ddlDesg.Items.FindByValue(dt.Rows[0]["VacancyID"].ToString()));
        //            txtFirstName.Value = dt.Rows[0]["CandidateFirstName"].ToString();
        //            txtMiddleName.Value = dt.Rows[0]["CandidateMiddleName"].ToString();
        //            txtLastName.Value = dt.Rows[0]["CandidateLastName"].ToString();
        //            txtCNIC.Value = dt.Rows[0]["CNIC"].ToString();
        //            ddlEducation.SelectedIndex = ddlEducation.Items.IndexOf(ddlEducation.Items.FindByText(dt.Rows[0]["Qualification"].ToString()));
        //            ddlExperience.SelectedIndex = ddlExperience.Items.IndexOf(ddlExperience.Items.FindByText(dt.Rows[0]["Experience"].ToString()));
        //            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(dt.Rows[0]["Gender"].ToString()));
        //            txtContactNo.Value = dt.Rows[0]["ContactNo"].ToString();
        //            txtEmail.Value = dt.Rows[0]["Email"].ToString();
        //        }
        //    }

        //}

        //private void BindVacanciesDropDown()
        //{
        //    CheckSessions();
        //    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //    ddlDesg.DataSource = objDB.GetAllVacancyByCompanyID(ref errorMsg);
        //    ddlDesg.DataTextField = "DesgTitle";
        //    ddlDesg.DataValueField = "VacancyID";
        //    ddlDesg.DataBind();
        //    ddlDesg.Items.Insert(0, new ListItem("--- Select Opening ---", "0"));
        //}

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        //protected void btnSave_ServerClick(object sender, EventArgs e)
        //{
        //    CheckSessions();
        //    string res = "";

        //    if (updResume.HasFile)
        //    {
        //        Random rand = new Random((int)DateTime.Now.Ticks);
        //        int randnum = 0;

        //        string fn = "";
        //        string exten = "";

        //        string destDir = Server.MapPath("~/assets/files/resumes/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
        //        randnum = rand.Next(1, 100000);
        //        fn = Common.RemoveSpecialCharacter(txtCNIC.Value).ToLower().Replace(" ", "-") + "_" + randnum;

        //        if (!Directory.Exists(destDir))
        //        {
        //            Directory.CreateDirectory(destDir);
        //        }

        //        string fname = Path.GetFileName(updResume.PostedFile.FileName);
        //        exten = Path.GetExtension(updResume.PostedFile.FileName);
        //        updResume.PostedFile.SaveAs(destDir + fn + exten);

        //        resume = "http://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/resumes/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
        //    }

        //    objDB.VacancyID = Convert.ToInt32(ddlDesg.SelectedItem.Value);
        //    objDB.CandidateFirstName = txtFirstName.Value;
        //    objDB.CandidateMiddleName = txtMiddleName.Value;
        //    objDB.CandidateLastName = txtLastName.Value;
        //    objDB.CNIC = txtCNIC.Value;
        //    objDB.Qualification = ddlEducation.SelectedItem.Value;
        //    objDB.Experience = ddlExperience.SelectedItem.Value;
        //    objDB.Gender = ddlGender.SelectedItem.Value;
        //    objDB.ContactNo = txtContactNo.Value;
        //    objDB.Email = txtEmail.Value;
        //    objDB.CountryName = ddlCountries.SelectedItem.Text;
        //    objDB.StateName = ddlStates.SelectedItem.Text;
        //    objDB.CityName = ddlCities.SelectedItem.Text;
        //    objDB.AddressLine1 = txtAddressLine1.Value;
        //    objDB.AddressLine2 = txtAddressLine2.Value;
        //    objDB.ZIPCode = txtZIPCode.Value;
        //    objDB.CV = resume;

        //    if (HttpContext.Current.Items["CandidateID"] != null)
        //    {
        //        objDB.ModifiedBy = Session["UserName"].ToString();
        //        objDB.CandidateID = candidateID;
        //        res = objDB.UpdateCandidate();
        //    }
        //    else
        //    {
        //        objDB.CreatedBy = Session["UserName"].ToString();
        //        res = objDB.AddCandidate();
        //        clearFields();
        //    }

        //    if (res == "New Candidate Added" || res == "Candidate Data Updated")
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-success";
        //        pAlertMsg.InnerHtml = res;
        //    }
        //    else
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = res;
        //    }
        //}

        //private void clearFields()
        //{
        //    ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText("Pakistan"));
        //    int countriesIndex = ddlCountries.Items.IndexOf(ddlCountries.Items.FindByText("Pakistan"));
        //    if (countriesIndex == -1)
        //        countriesIndex = 0;
        //    BindStatesDropDown(Convert.ToInt32(ddlCountries.Items[countriesIndex].Value));

        //    ddlCities.Items.Clear();

        //    txtAddressLine1.Value = "";
        //    txtAddressLine2.Value = "";
        //    txtZIPCode.Value = "";

        //    resume = "";
        //    ddlDesg.SelectedIndex = 0;
        //    txtFirstName.Value = "";
        //    txtMiddleName.Value = "";
        //    txtLastName.Value = "";
        //    txtCNIC.Value = "";
        //    ddlEducation.SelectedIndex = 0;
        //    ddlExperience.SelectedIndex = 0;
        //    ddlGender.SelectedIndex = 0;
        //    txtContactNo.Value = "";
        //    txtEmail.Value = "";
        //}

        //private void BindCountriesDropDown()
        //{
        //    ddlCountries.DataSource = objDB.GetAllCounties(ref errorMsg);
        //    ddlCountries.DataTextField = "Name";
        //    ddlCountries.DataValueField = "ID";
        //    ddlCountries.DataBind();
        //    ddlCountries.Items.Insert(0, new ListItem("--- Select Country ---", "0"));
        //    ddlCountries.SelectedValue = "166";
        //}

        //private void BindStatesDropDown(int CountryID)
        //{
        //    objDB.CountryID = CountryID;
        //    ddlStates.DataSource = objDB.GetAllStatesByCountryID(ref errorMsg);
        //    ddlStates.DataTextField = "Name";
        //    ddlStates.DataValueField = "ID";
        //    ddlStates.DataBind();
        //    ddlStates.Items.Insert(0, new ListItem("--- Select State ---", "0"));
        //}

        //private void BindCitiesDropDown(int StateID)
        //{
        //    objDB.StateID = StateID;
        //    ddlCities.DataSource = objDB.GetAllCitiesByStateID(ref errorMsg);
        //    ddlCities.DataTextField = "Name";
        //    ddlCities.DataValueField = "Name";
        //    ddlCities.DataBind();
        //    ddlCities.Items.Insert(0, new ListItem("--- Select City ---", "0"));
        //}

        //protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    CheckSessions();
        //    BindCitiesDropDown(Convert.ToInt32(ddlStates.SelectedItem.Value));

        //}

        //protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    CheckSessions();
        //    BindStatesDropDown(Convert.ToInt32(ddlCountries.SelectedItem.Value));

        //}
    }
}