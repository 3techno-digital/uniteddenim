﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement
{
    public partial class ProvinentFunds : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                //      btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/employee-self-service/view/dashboard";
                divAlertMsg.Visible = false;
                getProvinentFunsByCompanyId();
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }


        private void getProvinentFunsByCompanyId()
        {
            objDB.CompanyID = int.Parse(Session["CompanyID"].ToString());
            DataTable dt = objDB.GetProvinentFundByCompanyID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    txtCompanyRatio.Value = dt.Rows[0]["CompanyRatio"].ToString();
                    txtEmployeeRatio.Value = dt.Rows[0]["EmployeeRatio"].ToString();
                    
                }
                else
                {
                    txtCompanyRatio.Value = "0.00";
                    txtEmployeeRatio.Value = "0.00";
                }
            }
            else
            {
                txtCompanyRatio.Value = "0.00";
                txtEmployeeRatio.Value = "0.00";
            }

            getPFSummary();
        }
        private void getPFSummary() {

            objDB.CompanyID = int.Parse(Session["CompanyID"].ToString());
            DataTable dt = objDB.GetProvinentFundsSummary(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.DataSource = dt;
                    gv.DataBind();

                }
               
            }
          
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            string res = "";

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.CompanyRatio = float.Parse(txtCompanyRatio.Value);
            objDB.EmployeeRatio = float.Parse(txtEmployeeRatio.Value);

            objDB.CreatedBy = Session["UserName"].ToString();

            res = objDB.AddProvinentFund();

            if (res == "New Provinent Fund Ratio Added" || res == "Provinent Fund Ration Updated")
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }




    }
}