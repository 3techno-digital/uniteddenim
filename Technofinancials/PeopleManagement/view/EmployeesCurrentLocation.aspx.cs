﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
    public partial class EmployeesCurrentLocation : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
            }

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }
        
        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();

                DataTable dt = new DataTable();
                objDB.Date = txtDate.Value;
                dt = objDB.GetAllUsersLatLng(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        string Script = "<script>";
                        Script += "var map;";
                        Script += "var json = [";
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Script += "{'label':'" + dt.Rows[i]["EmployeeName"].ToString() + "','longitude':'" + dt.Rows[i]["Lng"].ToString() + "','latitude':'" + dt.Rows[i]["Lat"].ToString() + "'},";
                        }
                        Script = Script.Remove(Script.Length - 1);
                        Script += "];";
                        Script += "function initialize()";
                        Script += "{";
                        Script += "var mapOptions = {";
                        Script += "zoom: 11,";
                        Script += "center: new google.maps.LatLng(" + dt.Rows[0]["Lat"].ToString() + ", " + dt.Rows[0]["Lng"].ToString() + ")";
                        Script += "};";
                        Script += "map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);";
                        Script += "for (var i = 0; i < json.length; i++)";
                        Script += "{";
                        Script += "var obj = json[i];";
                        Script += "var marker = new google.maps.Marker({";
                        Script += "position: new google.maps.LatLng(obj.latitude, obj.longitude),";
                        Script += "map: map,";
                        Script += "content: obj.label,";
                        Script += "icon:'http://ethlonmedia.com/assets/images/rikshaw.png'";
                        Script += "});";

                        Script += "var clicker = addClicker(marker, obj.label);";
                        Script += "}";
                        Script += "function addClicker(marker, content)";
                        Script += "{";
                        Script += "google.maps.event.addListener(marker, 'click', function()";
                        Script += "{";
                        //Script += "if (infowindow) { infowindow.close(); }";
                        Script += "infowindow = new google.maps.InfoWindow({ content: content });";
                        Script += "infowindow.open(map, marker);";
                        Script += "});";
                        Script += "}";
                        Script += "}";
                        Script += "google.maps.event.addDomListener(window, 'load', initialize);";
                        Script += "</script>";
                        ltrMap.Text = Script;
                    }
                    else
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}