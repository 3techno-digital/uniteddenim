﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeClearance.aspx.cs" Inherits="Technofinancials.PeopleManagement.view.EmployeeClearance" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        .tf-note-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-note-btn i {
                color: #fff !important;
            }

        .tf-disapproved-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-disapproved-btn i {
                color: #fff !important;
            }

        div#gvCurrency_wrapper {
            margin-bottom: 15%;
        }

        .tf-add-btn {
            background-color: #575757;
            padding: 4px 9px 4px 10px !important;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-add-btn i {
                color: #fff !important;
            }

        .tf-add-btn {
            background-color: #575757;
            padding: 12px 10px 8px 10px !important;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-add-btn i {
                color: #fff !important;
            }

        .tf-back-btn {
            background-color: #575757;
            padding: 10px 10px 10px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-back-btn i {
                color: #fff !important;
            }

        .tf-back-btn {
            background-color: #575757 !important;
            border-radius: 0px !important;
            box-shadow: 0 8px 6px -5px #cacaca !important;
            height: 38px !important;
            width: 37px !important;
            display: inline-block !important;
            padding: 5px !important;
            position: relative !important;
            text-align: center !important;
        }

        button#btnView {
            padding: 3px 24px;
            margin-top: 14px;
        }
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">

                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Employee Clearance</h1>
                            </div>

                            <!-- /.col -->
                            <div class="col-md-4 col-sm-6">
                                <div style="text-align: right;">
                                    <asp:LinkButton ID="lnkApprove" runat="server" CssClass="AD_btn" CommandArgument='Approve' data-toggle="modal" data-target="#notes-modal" value="Add Note"><%--<i class="fa fa-thumbs-down"></i>--%> Approve</asp:LinkButton>

                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>


                <!-- Modal -->
                <div class="modal fade M_set" id="notes-modal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="m-0 text-dark">Clearance Approval Comments</h1>
                                <div class="add_new">
                                    <button type="button" class="AD_btn" data-dismiss="modal">Close</button>
                                    <button type="button" class="AD_btn" id="Reject" runat="server" onserverclick="btnApprove_ServerClick" data-dismiss="modal">Save</button>
                                </div>
                            </div>
                            <div class="modal-body">
                                <p>
                                    <asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
                                </p>
                                <p>
                                    <textarea id="txtComments" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <section class="app-content">

                    <div class="row ">
                        <div class="col-lg-4 col-md-6 col-sm-12">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>From Date
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtFromDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>To Date
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtToDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee Name</h4>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Location</h4>
                                        <asp:DropDownList ID="ddlLocation" runat="server" class="form-control select2 ">
                                        </asp:DropDownList>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Departments<span style="color: red !important;">*</span>
                                        </h4>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" onchange="GetDesignationsByDepartmentIdAndCompanyId()" class="form-control select2" data-plugin="select2" AutoPostBack="false"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Designation</h4>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" onchange="GetEmployeesByDesignationIdAndDepartmentId()" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Filter</h4>
                                        <asp:DropDownList ID="ddlIsApproved" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                            <%--<asp:ListItem Value="-1">ALL</asp:ListItem>--%>
                                            <asp:ListItem Value="0">Pending</asp:ListItem>
                                            <asp:ListItem Value="1">Approved</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="clear-fix">&nbsp;</div>
                                    <div>
                                        <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="col-sm-12">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group" id="divAlertMsg" runat="server">
                                            <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                <span>
                                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                </span>
                                                <p id="pAlertMsg" runat="server">
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div class="clear-fix">&nbsp;</div>
                    <div class="row ">
                        <div class="col-sm-12">
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12 gv-overflow-scrool">
                                        <asp:GridView ID="gvEmployees" runat="server" CssClass="table table-bordered gv" EmptyDataText="No Records Found" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No.">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="checkAll_CheckedChanged" ID="checkAll" ToolTip="Click To Select All"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" Enabled='<%# Eval("HRStatus").ToString()=="Approved"? false : true %>' ID="check"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Code">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblWDID" Text='<%# Eval("WDID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Department Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblDepartmentName" Text='<%# Eval("DeptName")  %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Approval Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblHRStatus" Text='<%# Eval("HRStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Approved By">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblApprovedBy" Text='<%# Eval("HRClearedBy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Deduction Amount">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblDeductionAmount" Text='<%# Eval("DeductionAmount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Separation Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblSeperationDate" Text='<%# Eval("EmployeeSeperationDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View">

                                                    <ItemTemplate>
                                                        <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/people-management/manage/employee-clearance/edit-seperation-item-" + Eval("SeperationDetailsID") %>" class="AD_stock">View </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblSeperationDetailsID" Text='<%# Eval("SeperationDetailsID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
                    <div class="clear-fix">&nbsp;</div>
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }

            div#gvCurrency_wrapper {
                margin-bottom: 15%;
            }

            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757 !important;
                border-radius: 0px !important;
                box-shadow: 0 8px 6px -5px #cacaca !important;
                height: 38px !important;
                width: 37px !important;
                display: inline-block !important;
                padding: 5px !important;
                position: relative !important;
                text-align: center !important;
            }

            button#btnView {
                padding: 3px 24px;
                margin-top: 14px;
            }
        </style>
    </form>
</body>
</html>


