﻿using IronPdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.PeopleManagement.view
{
    public partial class AttendanceSummary : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false; 
                    BindShiftsDropDown();
                    BindDropDown();

                    Common.addlog("ViewAll", "HR", "All Attendance Report Viewed", "EmployeeAttendance");


                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void BindDropDown()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg); ;
                ddlDepartment.DataTextField = "DeptName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

                ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg); ;
                ddlDesignation.DataTextField = "DesgTitle";
                ddlDesignation.DataValueField = "DesgID";
                ddlDesignation.DataBind();
                ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

                ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
                ddlLocation.DataTextField = "NAME";
                ddlLocation.DataValueField = "NAME";
                ddlLocation.DataBind();
                ddlLocation.Items.Insert(0, new ListItem("ALL", "0"));

                ddlGrades.DataSource = objDB.GetAllGradesByCompanyID(ref errorMsg);
                ddlGrades.DataTextField = "GradeName";
                ddlGrades.DataValueField = "GradeID";
                ddlGrades.DataBind();
                ddlGrades.Items.Insert(0, new ListItem("ALL", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void BindShiftsDropDown()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlShifts.DataSource = objDB.GetApproveAllWorkingShiftsByCompanyID(ref errorMsg);
                ddlShifts.DataTextField = "ShiftName";
                ddlShifts.DataValueField = "ShiftID";
                ddlShifts.DataBind();
                ddlShifts.Items.Insert(0, new ListItem("ALL", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void GetData()
        {
            try
            {
                string res = "";
                CheckSessions();

                DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
                DateTime.TryParse(txtFromDate.Value, out Fdate);
                DateTime.TryParse(txtToDate.Value, out Tdate);
                if (Tdate < Fdate)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be less than To Date";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }
                if (Fdate > DateTime.Now)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be Equal or less than Current Date";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }
                if ((Tdate - Fdate).Days > 31 || (Fdate.Day <= Tdate.Day && (Tdate.Month - Fdate.Month) > 0))
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "date range should be of 1 month";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.GradeID = Convert.ToInt32(ddlGrades.SelectedValue);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.FromDate = txtFromDate.Value;
                objDB.ToDate = txtToDate.Value;

               
                
                
                    objDB.WhereClause = "Employees.DocStatus = 'Approved' And Employees.CompanyID = " + objDB.CompanyID;

                    if (ddlEmployee.SelectedValue != "0" && ddlEmployee.SelectedValue != "")
                    {
                        objDB.WhereClause += " and Employees.EmployeeID = " + ddlEmployee.SelectedValue;
                    }
                    if (ddlGrades.SelectedValue != "0" && ddlGrades.SelectedValue != "")
                    {
                        objDB.WhereClause += " and Employees.GradeID = " + ddlGrades.SelectedValue;
                    }
                    if (txtFromDate.Value != "")
                    {
                        objDB.WhereClause += " and '" + DateTime.Parse(objDB.FromDate).ToString("yyyyMMdd") + "' <= " + "Format(Cast(EmployeeAttendance.TimeIn2 as date),'yyyyMMdd')";
                    }
                    if (txtToDate.Value != "")
                    {
                        objDB.WhereClause += " and '" + DateTime.Parse(objDB.ToDate).ToString("yyyyMMdd") + "' >= " + "Format(Cast(EmployeeAttendance.TimeIn2 as date),'yyyyMMdd')";
                    }
                    if (ddlDepartment.SelectedValue != "0" && ddlDepartment.SelectedValue != "")
                    {
                        objDB.WhereClause += " and Departments.DeptID = " + ddlDepartment.SelectedValue;
                    }
                    if (ddlDesignation.SelectedValue != "0" && ddlDesignation.SelectedValue != "")
                    {
                        objDB.WhereClause += " and Designations.DesgID = " + ddlDesignation.SelectedValue;
                    }
                    if (txtKTId.Text != "" && !string.IsNullOrEmpty(txtKTId.Text))
                    {
                        objDB.WhereClause += " and Employees.WDID = " + txtKTId.Text;
                    }
                    if (ddlLocation.SelectedValue != "0" && ddlLocation.SelectedValue != "")
                    {
                        objDB.WhereClause += " and Employees.PlaceOfPost = '" + ddlLocation.SelectedValue + "'";
                    }

                    dt = objDB.GetAttendanceSummaryKT(ref errorMsg);
                    gv.DataSource = dt;
                    gv.DataBind();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        divAlertMsg.Visible = false;
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gv.DataSource = null;
                        gv.DataBind();
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                }
                else
                {
                    gv.DataSource = null;
                    gv.DataBind();
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                }
                    Common.addlog("ViewAll", "HR", "All Employee Attendance Viewed", "EmployeeAttendance");
                


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    foreach (DataControlFieldCell cell in e.Row.Cells)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Attendance Sheet";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##DATE##", DateTime.Now.ToString("dd-MMM-yyyy"));
                content = content.Replace("##MONTH##", DateTime.Parse(txtFromDate.Value).ToString("MMM"));
                content = content.Replace("##YEAR##", DateTime.Parse(txtFromDate.Value).ToString("yyyy"));
                content = content.Replace("##SHIFT##", "");
                content = content.Replace("##TABLE##", GetTemplate(gv));
                Common.addlog("Report", "HR", "Attendance Sheet Report Generated", "EmployeeAttendance");

                Common.generatePDF(header, footer, content, "Attendance Sheet - " + " " + " (" + txtFromDate.Value + ")", "A4", "Portrait");


            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private string GetTemplate(GridView gd)
        {
            try
            {

                StringBuilder sheetBody = new StringBuilder();
                StringWriter sw = new StringWriter(sheetBody);
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                gd.RenderControl(hw);
                return sheetBody.ToString();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                return "";
            }

        }
    }
}