﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
    public partial class NewHiringReport : System.Web.UI.Page
    {


        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                try
                {
                    divAlertMsg.Visible = false;
                }
                catch (Exception ex)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = ex.Message;
                }
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void GetData()
        {
            try
            {
                CheckSessions();
                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.FromDate = txtFromDate.Value;
                objDB.ToDate = txtToDate.Value;
                dt = objDB.GetNewHiringReport(ref errorMsg);
                gv.DataSource = dt;
                gv.DataBind();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        divAlertMsg.Visible = false;
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {

                        gv.DataSource = null;
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                }
                else
                {

                    gv.DataSource = null;
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
            Common.addlog("ViewAll", "HR", "All Hiring Reports Viewed", "Employees");

        }


        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnReport_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Session["ReportTitle"] = "New Hiring";
                //Session["ReportDataTable"] = GetTemplate(UpdPnl);
                Session["ReportDataTable"] = Common.GetTemplate(gv);
                Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/generate-hr-report");
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }


        //private string GetTemplate(GridView gd)
        //{
        //    try
        //    {
        //        StringBuilder sheetBody = new StringBuilder();
        //        StringWriter sw = new StringWriter(sheetBody);
        //        HtmlTextWriter hw = new HtmlTextWriter(sw);
        //        gd.RenderControl(hw);
        //        return sheetBody.ToString();
        //    }
        //    catch (Exception ex)
        //    {

        //        return "";
        //    }

        //}

    }
}