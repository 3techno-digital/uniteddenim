﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
	public partial class AttendanceLock : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                btnLockForward.Visible = btnPendingRequest.Visible=
                divAlertMsg.Visible = false;
            }
        }

        private void GetData()
        {
            //objDB.Month = Convert.ToDateTime($"01-{txtPayrollMonth.Text}").ToString("MM-dd-yyyy"); 
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.PayrollDate = Convert.ToDateTime($"01-{txtPayrollMonth.Text}").ToString("MM-dd-yyyy");
            DataTable dt = new DataTable();
            //dt = objDB.GetEmployeeMonthlyAttendanceDetails(ref errorMsg);
            dt =  objDB.GetPendingRequestsList(ref errorMsg);
            gv.DataSource = null;
            gv.DataBind();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    btnLockForward.Visible = false;
                    gv.DataSource = dt;
                    gv.DataBind();
                   btnPendingRequest.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/people-management/view/pending_request_" + txtPayrollMonth.Text;

                    divAlertMsg.Visible = btnPendingRequest.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-info";
                    pAlertMsg.InnerHtml = "Attendance can not be forward to finance unless all the adjustments/pendings are setteled";
                }
                else
                {
                    btnLockForward.Visible = true;
                }
            }
			

            Common.addlog("ViewAll", "HR", "All employee-attendance Viewed", "EmployeeAttendance");
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnApprove_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            btnPendingRequest.Visible = divAlertMsg.Visible = btnLockForward.Visible = false;
            //objDB.PayrollDate = txtPayrollMonth.Text;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.PayrollDate = Convert.ToDateTime($"01-{txtPayrollMonth.Text}").ToString("MM-dd-yyyy");
            if (objDB.GetPendingRequestsList(ref errorMsg).Rows.Count > 0)
            {
                //btnPendingRequest.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/people-management/view/pending_request_" + txtPayrollMonth.Text;

                divAlertMsg.Visible = btnPendingRequest.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "Attendance can not be forward to finance unless all the adjustments/pendings are approved";
            }
            else
            {
                List<string> lstEmpIds = new List<string>();
                string attendanceMonth = "";

                //foreach (GridViewRow gvr in gv.Rows)
                //{
                //    attendanceMonth = ((Label)gvr.FindControl("lblPayrollMonth")).Text;
                //    lstEmpIds.Add(((Label)gvr.FindControl("lblEmployeeID")).Text);
                //}
                objDB.Month = Convert.ToDateTime($"01-{txtPayrollMonth.Text}").ToString("MM-dd-yyyy");
                string errMsg = objDB.LockAttendanceForPayroll();
                GetData();

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = errMsg;
            }
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            divAlertMsg.Visible = btnLockForward.Visible = btnPendingRequest.Visible = false;
            CheckSessions();
            GetData();
        }
    }
}

