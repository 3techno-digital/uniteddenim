﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
    public partial class MailingList : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            if (Request.QueryString["DateFilter"] != null)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["DateFilter"].ToString()))
                {
                    objDB.MailingListDate = Request.QueryString["DateFilter"].ToString();
                    dt = objDB.GetAllMailingListsByCompanyIDAndMailingListDate(ref errorMsg);
                }
                else
                {
                    dt = objDB.GetAllMailingListsByCompanyID(ref errorMsg);
                }
            }
            else
            {
                dt = objDB.GetAllMailingListsByCompanyID(ref errorMsg);
            }

            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All Mailing Lists Viewed", "MailingLists");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            int ID = Convert.ToInt32(btn.CommandArgument);
            objDB.MailingListID = ID;
            objDB.DeletedBy = Session["UserName"].ToString();

            objDB.DeleteMailingList();
            GetData();
        }
    }
}