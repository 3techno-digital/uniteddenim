﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
    public partial class AutoAttendance : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false;
                    BindDropdown();
                    
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        void BindDropdown()
        {


            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            ddldept.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddldept.DataTextField = "DeptName";
            ddldept.DataValueField = "DeptID";
            ddldept.DataBind();
            ddldept.Items.Insert(0, new ListItem("ALL", "0"));

        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
           
        }





        protected void chckchanged(object sender, EventArgs e)

        {
            CheckBox chckheader = (CheckBox)gv.HeaderRow.FindControl("checkAll");

            foreach (GridViewRow row in gv.Rows)
            {
                
                    ((CheckBox)row.FindControl("check")).Checked = chckheader.Checked;
                
            }
        }


        //     private void GetData()
        //     {
        //         CheckSessions();
        //         btnApprove.Visible = btnDisapprove.Visible = true;
        //         DataTable dt = new DataTable();
        //         //objDB.Status = ddlAdjustmentStatus.SelectedValue;
        //         List<string> lstEmpIds = new List<string>();

        //foreach (ListItem item in lstEmployee.Items)
        //{
        //	if (item.Selected)
        //	{
        //		lstEmpIds.Add(item.Value);
        //	}
        //}

        //dt = objDB.GetAllIT3Forms(lstEmpIds, ref errorMsg);
        //         gv.DataSource = dt;
        //         gv.DataBind();
        //         if (dt != null)
        //         {
        //             if (dt.Rows.Count > 0)
        //             {
        //                 gv.UseAccessibleHeader = true;
        //                 gv.HeaderRow.TableSection = TableRowSection.TableHeader;
        //             }
        //         }
        //     }
        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                divAlertMsg.Visible = false;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DeptID = Convert.ToInt32(ddldept.SelectedValue);




                DataTable dr = objDB.GetAllAutoAttendanceEmployeesByDeptID(ref errorMsg);


                if (dr != null)
                {
                    if (dr.Rows.Count > 0)
                    {
                        gv.DataSource = dr;
                        gv.DataBind();
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;


                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

     
        protected void btnApprove_ServerClick(object sender, EventArgs e)
        {
            btnApprove.Visible = true;
            CheckSessions();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            string alertMsg = "";
            int recordsCount = 0;
            List<string> MarkedEmp = new List<string>();
            List<string> UnmarkedEmp = new List<string>();
         
            string employeeID = "";
            foreach (GridViewRow gvr in gv.Rows)
            {
                if (((CheckBox)gvr.FindControl("check")).Checked)
                {

                     employeeID = ((Label)gvr.FindControl("EmployeeID")).Text;


                    MarkedEmp.Add(employeeID);
                    recordsCount++;
                    Common.addlog("AutoAttendance", "HR", "Department of ID \"" + ddldept.SelectedItem.Text + "\" updated - marked  By user: -" + Session["Username"].ToString() + "", "Employees", Convert.ToInt32(employeeID));


                }
				else
				{
                     employeeID = ((Label)gvr.FindControl("EmployeeID")).Text;


                    UnmarkedEmp.Add(employeeID);
                    Common.addlog("AutoAttendance", "HR", "Department of ID \"" + ddldept.SelectedItem.Text + "\" updated - unmarked  By user: -" + Session["Username"].ToString() + "", "Employees", Convert.ToInt32(employeeID));

                }
            }
          

            string res = objDB.AutoAttendanceMark(MarkedEmp, UnmarkedEmp);
            if (res == "Auto Attendance")
            {
                alertMsg = recordsCount > 0 ? "" + recordsCount + " Records Updated Successfully" : "No Records Updated ";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + alertMsg + "')", true);


            }
			else
			{
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }

        }

        protected void btnDisApprove_ServerClick(object sender, EventArgs e)
        {
            btnApprove.Visible = true;
            CheckSessions();
            string alertMsg = "";
            int recordsCount = 0;
            List<string> IT3IDs = new List<string>();
            foreach (GridViewRow gvr in gv.Rows)
            {
                if (((CheckBox)gvr.FindControl("check")).Checked)
                {
                    string employeeID = ((Label)gvr.FindControl("lblEmployeeID")).Text;
                    string IT3ID = Convert.ToString(((Label)gvr.FindControl("IT3ID")).Text);

                    IT3IDs.Add(IT3ID);
                    recordsCount++;
                }
            }
            objDB.ApprovedBy = Session["UserName"].ToString();
            objDB.Status = "Rejected";
            objDB.UpdateIT3Status(IT3IDs, ref errorMsg);
           
            alertMsg = recordsCount > 0 ? "" + recordsCount + " Records Rejected Successfully" : "No Records Updated ";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + alertMsg + "')", true);
        }
    }
}