﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
    public partial class AttendanceAdjustment : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {  
                btnApprove.Visible = lnkReject.Visible = false;
                BindDropdowns();

                DateTime date = DateTime.Now;
                var firstDay = new DateTime(date.Year, date.Month, 1);
                //txtFromDate.Text = (firstDay).ToString("dd-MMM-yyyy");
                //txtToDate.Text = (firstDay.AddMonths(1).AddDays(-1)).ToString("dd-MMM-yyyy");
                GetData();

            }
        }

        void BindDropdowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDepartment.DataTextField = "DeptName";
            ddlDepartment.DataValueField = "DeptID";
            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

            ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
            ddlDesignation.DataTextField = "DesgTitle";
            ddlDesignation.DataValueField = "DesgID";
            ddlDesignation.DataBind();
            ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

            ddlEmployee.DataSource = null;
            ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();

            ddlAdjustmentStatus.DataSource = objDB.GetAttendanceAdjustmentStatus(ref errorMsg);
            ddlAdjustmentStatus.DataTextField = "AdjustmentStatus";
            ddlAdjustmentStatus.DataValueField = "AdjustmentStatus";
            ddlAdjustmentStatus.DataBind();
            ddlAdjustmentStatus.Items.Insert(0, new ListItem("ALL", "0"));

            lstEmployees.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg);
            lstEmployees.DataTextField = "EmployeeName";
            lstEmployees.DataValueField = "EmployeeID";
            lstEmployees.DataBind();
        }
        
        private void GetData()
        {
            divAlertMsg.Visible = true;
            pAlertMsg.InnerHtml = "No Records Found";

            CheckSessions();
            DataTable dt = new DataTable();
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objDB.FromDate = txtFromDate.Text;
            objDB.ToDate = txtToDate.Text;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.KTID = txtKTId.Text;

            List<string> lstEmpIds = new List<string>();
            foreach (ListItem item in lstEmployees.Items)
            {
                if (item.Selected)
                {
                    lstEmpIds.Add(item.Value);
                }
            }

            dt = objDB.GetAllAttendanceAdjustmentByCompanyID(lstEmpIds.Count == 0||lstEmpIds.Count == lstEmployees.Items.Count ? "0" : string.Join(",", lstEmpIds), ddlDepartment.SelectedValue, ddlDesignation.SelectedValue, ddlAdjustmentStatus.SelectedValue, ref errorMsg);
            gv.DataSource = null;
            gv.DataBind();
            
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    divAlertMsg.Visible = false;
                    pAlertMsg.InnerHtml = "";
                    gv.DataSource = dt;
                    gv.DataBind();
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;

                    btnApprove.Visible = lnkReject.Visible = true;
                }
                else
                {

                    divAlertMsg.Visible = true;
                    pAlertMsg.InnerHtml = "No Records Found";
                }
            }

            Common.addlog("ViewAll", "HR", "All employee-adjustments Viewed", "AttendanceAdjustment");
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void chckchanged(object sender, EventArgs e)
        {
            CheckBox chckheader = (CheckBox)gv.HeaderRow.FindControl("checkAll");

            foreach (GridViewRow row in gv.Rows)
            {
                if (((Label)row.FindControl("lblDocStatus")).Text == "Data Submitted for Review" || ((Label)row.FindControl("lblDocStatus")).Text == "Saved as Draft")
                {
                    ((CheckBox)row.FindControl("check")).Checked = chckheader.Checked;
                }
            }
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                DateTime fdate = DateTime.Parse(txtFromDate.Text);
                DateTime tdate = DateTime.Parse(txtToDate.Text);
                if (fdate > tdate)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('From Date should not be greater than To Date')", true);
                    return;
                }

                btnApprove.Visible = lnkReject.Visible = false;
                GetData();
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnApprove_ServerClick(object sender, EventArgs e)
        {
            btnApprove.Visible = lnkReject.Visible = false;
            CheckSessions();
            string alertMsg = "";
            int recordsCount = 0;
            foreach (GridViewRow gvr in gv.Rows)
            {
                if (((CheckBox)gvr.FindControl("check")).Checked)
                {
                    string employeeID = ((Label)gvr.FindControl("lblEmployeeID")).Text;
                    int attendanceAdjustmentId = Convert.ToInt32(((Label)gvr.FindControl("lblAttendanceAdjustmentID")).Text);
                    string employeeName = ((Label)gvr.FindControl("lblName")).Text;
                    string attendanceDate = ((Label)gvr.FindControl("lblAttendanceDate")).Text;
                    string docStatus = ((Label)gvr.FindControl("lblDocStatus")).Text;
                    //string timeIn = ((Label)gvr.FindControl("lblTimeIn")).Text;
                    //string timeOut = ((Label)gvr.FindControl("lblTimeOut")).Text;
                    //string breakStart = ((Label)gvr.FindControl("lblBreakStart")).Text;
                    //string breakEnd = ((Label)gvr.FindControl("lblBreakEnd")).Text;
                    //string resultant = ((Label)gvr.FindControl("lblResultant")).Text;

                    alertMsg = ApproveAttendanceAdjustmentRequest(attendanceAdjustmentId, sender, employeeID);
                    //msg += $"{employeeName} request status for date {attendanceDate} is {requestStatus}";
                    recordsCount++;
                }
            }

            GetData();

            alertMsg = recordsCount > 0 ? "" + recordsCount + " Records Approved Successfully" : "No Records Approved ";

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + alertMsg + "')", true);

            //divAlertMsg.Visible = true;
            //divAlertTheme.Attributes["class"] = "alert tf-alert-success";
            //pAlertMsg.InnerHtml = approveList;
        }

        private string ApproveAttendanceAdjustmentRequest(int attendanceAdjustmentId, object sender, string employeeID)
        {
            System.Web.UI.HtmlControls.HtmlButton btn = (System.Web.UI.HtmlControls.HtmlButton)sender as System.Web.UI.HtmlControls.HtmlButton;
            string res = Common.addAccessLevels(btn.ID.ToString(), "AttendanceAdjustment", "AttendanceAdjustmentID", attendanceAdjustmentId.ToString(), Session["UserName"].ToString());
            //Common.addlogNew(res, "ESS", "AttendanceAdjustment of ID\"" + attendanceAdjustmentId.ToString() + "\" Status Changed", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/view/attendance-adjustment", "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/people-management/manage/attendance-adjustment/edit-attendance-adjustment-" + attendanceAdjustmentId.ToString(), "AttendanceAdjustment for Employee \"" + employeeID + "\"", "AttendanceAdjustment", "Loans", attendanceAdjustmentId);
            //
            Common.addlogNew(res, "AttendanceAdjustment",
              "Attendance Adjustment \"" + objDB.Title + "\" Status",
              "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + attendanceAdjustmentId.ToString(),
              "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + attendanceAdjustmentId.ToString(), "Attendance Adjustment", "AttendanceAdjustment", "ESS", Convert.ToInt32(employeeID));

            if (res == "Reviewed & Approved Sucessfull" || res == "Approved Sucessfull")
            {
                objDB.AttendanceAdjustmentID = attendanceAdjustmentId;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.ApprovedBy = Session["UserName"].ToString();
                objDB.ApproveAttendanceAdjustment();

            }
            else if (res == "Rejected & Disapproved Sucessfull" || res == "Disapproved" || res == "Rejected" || res == "Disapproved")
            {

                if (string.IsNullOrEmpty(txtAdjustmentNote.Value))
                {
                    return "Please add a rejection note."; ;
                }

                objDB.AttendanceAdjustmentID = attendanceAdjustmentId;
                objDB.AttendanceAdjustmentRemarks = txtAdjustmentNote.Value;
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.RejectAttendanceAdjustment();
            }

            return res;
        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            gv.DataSource = null;
            gv.DataBind();
            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            BindEmployees();
        }

        private void BindEmployees()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            lstEmployees.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg, ddlDepartment.SelectedValue, ddlDesignation.SelectedValue);
            lstEmployees.DataTextField = "EmployeeName";
            lstEmployees.DataValueField = "EmployeeID";
            lstEmployees.DataBind();
        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlEmployee.SelectedItem.Value != "0")
            {
            }
        }
    }
}

