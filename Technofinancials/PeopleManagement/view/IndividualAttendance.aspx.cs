﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
    public partial class IndividualAttendance : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        private static bool chkMonday = true;
        private static bool chkTuesday = true;
        private static bool chkWednesday = true;
        private static bool chkThursday = true;
        private static bool chkFriday = true;
        private static bool chkSaturday = true;
        private static bool chkSunday = true;

        private void getWorkingDaysByShiftID(int shiftID)
        {
            try
            {
                DataTable dtShifts = new DataTable();
                objDB.WorkingShiftID = shiftID;
                dtShifts = objDB.GetWorkingShiftByID(ref errorMsg);

                if (dtShifts != null)
                {
                    if (dtShifts.Rows.Count > 0)
                    {
                        chkMonday = getCheckBoxValue(dtShifts.Rows[0]["Monday"].ToString());
                        chkTuesday = getCheckBoxValue(dtShifts.Rows[0]["Tuesday"].ToString());
                        chkWednesday = getCheckBoxValue(dtShifts.Rows[0]["Wednesday"].ToString());
                        chkThursday = getCheckBoxValue(dtShifts.Rows[0]["Thursday"].ToString());
                        chkFriday = getCheckBoxValue(dtShifts.Rows[0]["Friday"].ToString());
                        chkSaturday = getCheckBoxValue(dtShifts.Rows[0]["Saturday"].ToString());
                        chkSunday = getCheckBoxValue(dtShifts.Rows[0]["Sunday"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private bool getCheckBoxValue(string val)
        {
            if (val == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static DataTable dtHolidays;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {  
                    BindEmployeesDropDown();
                    //BindYearDropDown();
                    divAlertMsg.Visible = false;

                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void getHolidaysByCompanyID()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dtHolidays = objDB.GetAllHolidaysByCompanyID(ref errorMsg);
        }

        private void BindEmployeesDropDown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        //private void BindYearDropDown()
        //{
        //    try
        //    {
        //        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
        //        ddlYear.DataSource = null;
        //        int loc = 1;
        //        int Curyear = DateTime.Now.Year;
        //        ddlYear.Items.Insert(0, new ListItem("--- Select ---", "0"));
        //        while (Curyear > 2009)
        //        {
        //            ddlYear.Items.Insert(loc, new ListItem(Curyear.ToString(), Curyear.ToString()));
        //            loc++;
        //            Curyear--;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }

        //}


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private static DataTable dtAttendanceSheet;
        private DataTable createAttendanceTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Date");
            dt.Columns.Add("Login Time");
            dt.Columns.Add("Logout Time");
            dt.Columns.Add("Late / Over Time Reason");
            dt.Columns.Add("On Leave");
            dt.Columns.Add("Resultant");

            dt.AcceptChanges();

            return dt;
        }
        //protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        dtAttendanceSheet = null;
        //        gv.DataSource = null;
        //        gv.DataBind();

        //        int shiftID = 0;

        //        if (ddlMonth.SelectedIndex != 0 && ddlYear.SelectedIndex != 0 && ddlEmployee.SelectedIndex != 0)
        //        {
        //            if (dtAttendanceSheet == null)
        //            {
        //                dtAttendanceSheet = createAttendanceTable();
        //            }

        //            int month = Convert.ToInt32(ddlMonth.SelectedValue);
        //            int year = Convert.ToInt32(ddlYear.SelectedValue);

        //            int days = DateTime.DaysInMonth(year, month);
        //            for (int day = 1; day <= days; day++)
        //            {
        //                string date = "";
        //                string logintime = "";
        //                string logouttime = "";
        //                string latereason = "";
        //                string onleave = "";
        //                string resultant = "";

        //                date = new DateTime(year, month, day).ToString("dd-MMM-yyyy");

        //                objDB.date = new DateTime(year, month, day).ToString("dd-MMM-yyyy");
        //                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);

        //                DataTable dtTemp = objDB.GetEmployeeAttendanceByDate(ref errorMsg);
        //                if (dtTemp != null)
        //                {
        //                    if (dtTemp.Rows.Count > 0)
        //                    {
        //                        shiftID = Convert.ToInt32(dtTemp.Rows[0]["ShiftID"].ToString());

        //                        if (dtTemp.Rows[0]["TimeIn"].ToString() != "")
        //                            logintime = DateTime.Parse(dtTemp.Rows[0]["TimeIn"].ToString()).ToString("hh:mm:ss tt");

        //                        if (dtTemp.Rows[0]["TimeOut"].ToString() != "")
        //                            logouttime = DateTime.Parse(dtTemp.Rows[0]["TimeOut"].ToString()).ToString("hh:mm:ss tt");

        //                        if (dtTemp.Rows[0]["LateReason"].ToString() != "")
        //                            latereason = dtTemp.Rows[0]["LateReason"].ToString();

        //                        if (dtTemp.Rows[0]["Resultant"].ToString() != "")
        //                            resultant = dtTemp.Rows[0]["Resultant"].ToString();

        //                        if (dtTemp.Rows[0]["isOnLeave"].ToString() == "True")
        //                            onleave = "Yes";
        //                        else
        //                            onleave = "No";
        //                    }
        //                }

        //                DataRow dr = dtAttendanceSheet.NewRow();
        //                dr[0] = new DateTime(year, month, day).ToString("dd-MMM-yyyy");
        //                dr[1] = logintime;
        //                dr[2] = logouttime;
        //                dr[3] = latereason;
        //                dr[4] = onleave;
        //                dr[5] = resultant;

        //                dtAttendanceSheet.Rows.Add(dr);
        //                dtAttendanceSheet.AcceptChanges();


        //            }

        //            getHolidaysByCompanyID();
        //            getWorkingDaysByShiftID(shiftID);

        //            gv.DataSource = dtAttendanceSheet;
        //            gv.DataBind();
        //            Common.addlog("ViewAll", "HR", "Employee \"" + ddlEmployee.SelectedItem.Text + "\"Attendance Viewed", "EmployeeAttendance");

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        divAlertMsg.Visible = true;
        //        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
        //        pAlertMsg.InnerHtml = ex.Message;
        //    }

        //}

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int rowIndex = e.Row.RowIndex;
                    DateTime currDate = DateTime.Parse(e.Row.Cells[0].Text);

                    string dayName = currDate.DayOfWeek.ToString();
                    if (dayName == "Monday" && chkMonday == false)
                        e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                    else if (dayName == "Monday" && chkMonday == false)
                        e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                    else if (dayName == "Tuesday" && chkTuesday == false)
                        e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                    else if (dayName == "Wednesday" && chkWednesday == false)
                        e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                    else if (dayName == "Thursday" && chkThursday == false)
                        e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                    else if (dayName == "Friday" && chkFriday == false)
                        e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                    else if (dayName == "Saturday" && chkSaturday == false)
                        e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                    else if (dayName == "Sunday" && chkSunday == false)
                        e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);

                    if (dtHolidays != null)
                    {
                        if (dtHolidays.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtHolidays.Rows.Count; i++)
                            {
                                DateTime holidayStartDate = DateTime.Parse(dtHolidays.Rows[i]["StartDate"].ToString());
                                DateTime holidayEndDate = DateTime.Parse(dtHolidays.Rows[i]["EndDate"].ToString());

                                if (currDate >= holidayStartDate && currDate <= holidayEndDate)
                                    e.Row.BackColor = System.Drawing.Color.FromArgb(1, 188, 188, 188);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (ddlEmployee.SelectedValue != null && ddlEmployee.SelectedValue.ToString() != "0" && txtPayrollDate.Value != "")
            {
                bindRepeater();
            }
        }

        protected void rpAttendance_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblTimeIn = e.Item.FindControl("lblTimeIn") as Label;
                Label lblTimeOut = e.Item.FindControl("lblTimeOut") as Label;
                Label lblAttendance = e.Item.FindControl("lblAttendance") as Label;
                Label lblReason = e.Item.FindControl("lblReason") as Label;
                TextBox txtReason = e.Item.FindControl("txtReason") as TextBox;
                LinkButton btnAddReason = e.Item.FindControl("btnAddReason") as LinkButton;
                if (lblAttendance.Text == "Absent")
                {
                    lblAttendance.CssClass = "late";
                }
                else if (lblAttendance.Text == "Casual Leave")
                {
                    lblAttendance.CssClass = "casual";
                }
                else if (lblAttendance.Text == "Sick Leave")
                {
                    lblAttendance.CssClass = "sick";
                }
                else if (lblAttendance.Text == "Off")
                {
                    lblAttendance.CssClass = "offf";
                }
                else if (lblAttendance.Text == "Present")
                {
                    lblAttendance.CssClass = "pres";
                }
                else
                {
                    lblAttendance.CssClass = "not-marekd";
                }


                DataRowView drv = e.Item.DataItem as DataRowView;
                if (drv != null)
                {
                    string IsLate = drv["LateIn"].ToString();

                    if (lblReason.Text == "" && IsLate == "1")
                    {
                        txtReason.Visible = true;
                        btnAddReason.Visible = true;
                        lblReason.Visible = false;
                    }
                    else
                    {
                        txtReason.Visible = false;
                        btnAddReason.Visible = false;
                        lblReason.Visible = true;
                    }
                    if (lblTimeIn.Text != "")
                    {
                        if (Convert.ToDateTime(lblTimeIn.Text) > Convert.ToDateTime("10:15 AM"))
                        {
                            lblTimeIn.CssClass = "late";
                        }
                    }


                    if (lblAttendance.Text == "Off")
                    {
                        lblTimeIn.Text = "";
                        lblTimeOut.Text = "";
                    }

                }
            }
        }

        protected void rpAttendance_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "UpdateReason")
            {
                string errorMsg = "";
                TextBox txtReason = e.Item.FindControl("txtReason") as TextBox;
                string id = e.CommandArgument.ToString();
                objDB.AttendanceID = Convert.ToInt32(id);
                objDB.LateReason = txtReason.Text;
                errorMsg = objDB.UpdateEmployeeAttendanceReason();
            }
        }

        private void bindRepeater()
        {
            DataTable dt = new DataTable();
            objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            objDB.date = txtPayrollDate.Value;

            dt = objDB.GetEmployeeAttendanceByMonth(ref errorMsg);
            if (dt != null)
            {
                rpAttendance.DataSource = dt;
            }
            else
            {
                rpAttendance.DataSource = "";
            }
            rpAttendance.DataBind();

        }

        protected void btnAddReason_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            foreach (RepeaterItem ri in rpAttendance.Items)
            {
                Label lblTimeIn = ri.FindControl("lblTimeIn") as Label;
                Label lblTimeOut = ri.FindControl("lblTimeOut") as Label;
                Label lblAttendance = ri.FindControl("lblAttendance") as Label;
                Label lblReason = ri.FindControl("lblReason") as Label;
                TextBox txtReason = ri.FindControl("txtReason") as TextBox;
                LinkButton btnAddReason = ri.FindControl("btnAddReason") as LinkButton;

                if (btnAddReason.ClientID == btn.ClientID)
                {

                    lblReason.Text = txtReason.Text;
                    lblReason.Visible = true;
                    btnAddReason.Visible = false;
                    txtReason.Visible = false;
                    btn.Focus();
                    break;
                }

                //string quantityText = quantityLabel.Text;
                //string partNumberText = partNumberLabel.Text;
            }
        }
    }
}