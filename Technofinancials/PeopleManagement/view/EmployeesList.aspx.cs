﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
    public partial class EmployeesList : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        string filePath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            hdnIsRedirect.Value = "0";
            if (!Page.IsPostBack)
            {
                //GetData(10, 0);
                GetData();
                divAlertMsg.Visible = false;
            }
            
        }

     
        private DataTable FilterData(DataTable dt)
        {
           if (dt == null)
            {
                return dt;
            }
            DataTable dtFilter = new DataTable();

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.DocName = "EmployeeLists";

            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");

                    if (dtTemp != null)
                        dtFilter.Merge(dtTemp);

                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }

                }
            }

            return dtFilter;
        }

        private void GetData()
        {
            CheckSessions();

            DataTable ds = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            ds = objDB.GetAllEmployeesByCompanyID(ref errorMsg);
         
            gv.DataSource = ds;// FilterData(dt);
            gv.DataBind();
            if (ds != null)
            {
                if (ds.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }


            Common.addlog("ViewAll", "HR", "All Employees Viewed", "Employees");

        }

        private void GetData(int numberOfRecords, int from)
        {
            CheckSessions();

            DataSet ds = new DataSet();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            ds = objDB.GetAllEmployeesByCompanyID(ref errorMsg, numberOfRecords,from);
            var dt = ds.Tables[0];
            gv.VirtualItemCount = Convert.ToInt32(ds.Tables[2].Rows[0]["column1"]);
            gv.DataSource = dt;// FilterData(dt);
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

           
            Common.addlog("ViewAll", "HR", "All Employees Viewed", "Employees");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            CheckSessions();
            LinkButton btn = (LinkButton)sender as LinkButton;
            int ID = Convert.ToInt32(btn.CommandArgument);
            objDB.CandidateID = ID;
            objDB.DeletedBy = Session["UserName"].ToString();
            objDB.DeleteEmployeeByID();
            GetData(10, 0);

        }

        protected void btnToggleBlackListing_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();

            string attachment = "";

            if (updLogo != null)
            {

                Random rand = new Random((int)DateTime.Now.Ticks);
                int randnum = 0;

                string fn = "";
                string exten = "";

                string destDir = Server.MapPath("~/assets/files/blacking_listing_attachments/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
                //string destDir = Server.MapPath("~/assets/files/companies_logos/");
                randnum = rand.Next(1, 100000);
                fn = "blacking_listing_attachment_" + randnum;

                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }

                string fname = Path.GetFileName(updLogo.PostedFile.FileName);
                exten = Path.GetExtension(updLogo.PostedFile.FileName);
                updLogo.PostedFile.SaveAs(destDir + fn + exten);

                attachment = "http://" + Request.ServerVariables["SERVER_NAME"] + "/assets/files/blacking_listing_attachments/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
            }

            objDB.EmployeeID = Convert.ToInt32(hdnEmployeeID.Value);
            objDB.BlackListedBy = Session["UserName"].ToString();
            objDB.BlackListedAttachment = attachment;
            objDB.BlackListedReason = txtReason.Value;
            objDB.ToggleEmployeeBlackListing();
            Common.addlog("ViewAll", "HR", "Employee of ID" + objDB.EmployeeID + "BlackList Status Changed ", "Employees", objDB.EmployeeID);

            GetData(10,0);
        }

        private DataTable createTempTable()
        {
            DataTable dt = new DataTable();
            dt.TableName = "EmployeeTable";
            dt.Columns.Add("EmployeeID");
            dt.Columns.Add("EmployeeCode");
            dt.Columns.Add("EmployeeName");

            if (Session["ParentCompanyID"].ToString() == "28" || Session["CompanyID"].ToString() == "28")
            {
                dt.Columns.Add("BasicSalary");
                dt.Columns.Add("FuelAllowance");
                dt.Columns.Add("MobileAllowance");
                dt.Columns.Add("CarAllowance");
                dt.Columns.Add("FoodAllowance");
                dt.Columns.Add("TravelAllowance");
                dt.Columns.Add("OtherAllowance");
                dt.Columns.Add("MedicalAllowance");
                dt.Columns.Add("GrossAmount");
                dt.Columns.Add("NetSalary");
                dt.Columns.Add("HouseAllownace");
                dt.Columns.Add("UtitlityAllowance");
                dt.Columns.Add("EOBI");
                dt.Columns.Add("NightFoodAllowance");
                dt.Columns.Add("MaintenanceAllowance");
                dt.Columns.Add("OverTimeRate");
                dt.Columns.Add("EmployeeTax");
            }
            else
            {
                dt.Columns.Add("BasicSalary");
                dt.Columns.Add("FuelAllowance");
                dt.Columns.Add("GrossAmount");
                dt.Columns.Add("EOBI");
                dt.Columns.Add("MaintenanceAllowance");
                dt.Columns.Add("OverTimeRate");
                dt.Columns.Add("EmployeeTax");
            }
            
            dt.AcceptChanges();
            return dt;
        }

        protected DataTable dtEmployees
        {
            get
            {
                if (ViewState["dtEmployees"] != null)
                {
                    return (DataTable)ViewState["dtEmployees"];
                }
                else
                {
                    return new DataTable();
                }
            }

            set
            {
                ViewState["dtEmployees"] = value;
            }
        }
      
       
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                string res = "";

                if (dtEmployees != null)
                {
                    if (dtEmployees.Rows.Count > 0)
                    {

                        objDB.ModifiedBy = Session["UserName"].ToString();
                        objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                        for (int i = 0; i < dtEmployees.Rows.Count; i++)
                        {
                            objDB.EmployeeID = Convert.ToInt32(dtEmployees.Rows[i]["EmployeeID"].ToString());
                            if (Session["ParentCompanyID"].ToString() == "28" || Session["CompanyID"].ToString() == "28" || Session["ParentCompanyID"].ToString() == "56" || Session["CompanyID"].ToString() == "56")
                            {
                                objDB.EmployeeFName = dtEmployees.Rows[i]["EmployeeFirstName"].ToString();
                                objDB.EmployeeMName = dtEmployees.Rows[i]["EmployeeMiddleName"].ToString();
                                objDB.EmployeeLName = dtEmployees.Rows[i]["EmployeeLastName"].ToString();
                                objDB.EmployeeCode = dtEmployees.Rows[i]["EmployeeCode"].ToString();
                                objDB.CNIC = dtEmployees.Rows[i]["CNIC"].ToString();
                                objDB.Gender = dtEmployees.Rows[i]["Gender"].ToString();
                                objDB.DateOfJoining = dtEmployees.Rows[i]["DateOfJoining"].ToString();
                                objDB.ContactNo = dtEmployees.Rows[i]["ContactNo"].ToString();
                                objDB.Email = dtEmployees.Rows[i]["Email"].ToString();
                                objDB.CountryName = dtEmployees.Rows[i]["Country"].ToString();
                                objDB.StateName = dtEmployees.Rows[i]["State"].ToString();
                                objDB.CityName = dtEmployees.Rows[i]["City"].ToString();
                                objDB.AddressLine1 = dtEmployees.Rows[i]["AddressLine1"].ToString();
                                objDB.AddressLine2 = dtEmployees.Rows[i]["AddressLine2"].ToString();
                                objDB.ZIPCode = dtEmployees.Rows[i]["ZIPCode"].ToString();

                                objDB.BankName = dtEmployees.Rows[i]["BankName"].ToString();
                                objDB.BranchName = dtEmployees.Rows[i]["BranchName"].ToString();
                                objDB.AccountTitle = dtEmployees.Rows[i]["AccountTitle"].ToString();
                                objDB.AccountNo = dtEmployees.Rows[i]["AccountNo"].ToString();

                                objDB.BasicSalary = float.Parse(dtEmployees.Rows[i]["BasicSalary"].ToString());
                                objDB.FuelAllowance = float.Parse(dtEmployees.Rows[i]["FuelAllowance"].ToString());
                                objDB.MobileAllowance = float.Parse(dtEmployees.Rows[i]["MobileAllowance"].ToString());
                                objDB.CarAllowance = float.Parse(dtEmployees.Rows[i]["CarAllowance"].ToString());
                                objDB.TravelAllowance = float.Parse(dtEmployees.Rows[i]["TravelAllowance"].ToString());
                                objDB.FoodAllowance = float.Parse(dtEmployees.Rows[i]["FoodAllowance"].ToString());
                                objDB.OtherAllowance = float.Parse(dtEmployees.Rows[i]["OtherAllowance"].ToString());
                                objDB.MedicalAllowance = float.Parse(dtEmployees.Rows[i]["MedicalAllowance"].ToString());
                                objDB.GrossAmount = float.Parse(dtEmployees.Rows[i]["GrossAmount"].ToString());
                                objDB.NetSalary = float.Parse(dtEmployees.Rows[i]["NetSalary"].ToString());
                                objDB.HouseAllownace = float.Parse(dtEmployees.Rows[i]["HouseAllownace"].ToString());
                                objDB.UtitlityAllowance = float.Parse(dtEmployees.Rows[i]["UtitlityAllowance"].ToString());
                                objDB.EOBI = float.Parse(dtEmployees.Rows[i]["EOBI"].ToString());
                                objDB.NightFoodAllowance = float.Parse(dtEmployees.Rows[i]["NightFoodAllowance"].ToString());
                                objDB.MaintenanceAllowance = float.Parse(dtEmployees.Rows[i]["MaintenanceAllowance"].ToString());
                                objDB.OverTimeRate = float.Parse(dtEmployees.Rows[i]["OverTimeRate"].ToString());
                                objDB.EmployeeTax = float.Parse(dtEmployees.Rows[i]["EmployeeTax"].ToString());
                                res = objDB.UpdateNewEmployeeUpload();
                            }
                            else
                            {
                                objDB.BasicSalary = float.Parse(dtEmployees.Rows[i]["BasicSalary"].ToString());
                                objDB.FuelAllowance = float.Parse(dtEmployees.Rows[i]["FuelAllowance"].ToString());
                                objDB.GrossAmount = float.Parse(dtEmployees.Rows[i]["GrossAmount"].ToString());
                                objDB.EOBI = float.Parse(dtEmployees.Rows[i]["EOBI"].ToString());
                                objDB.MaintenanceAllowance = float.Parse(dtEmployees.Rows[i]["MaintenanceAllowance"].ToString());
                                objDB.OverTimeRate = float.Parse(dtEmployees.Rows[i]["OverTimeRate"].ToString());
                                objDB.EmployeeTax = float.Parse(dtEmployees.Rows[i]["EmployeeTax"].ToString());
                                res = objDB.UpdateNewEmployeeUpload2();
                            }
                        
                        }
                        Response.Redirect("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employees");
                    }
                    else
                    {
                        res = "File has no data";
                    }
                }
                else
                {
                    res = "File not Selected";
                }
                if (res == "Employee Data Updated")
                {
                    if (res == "Employee Data Updated") { Common.addlog("Update", "HR", "Employees  Updated", "Employee", objDB.EmployeeID); }
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                    pAlertMsg.InnerHtml = res;
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv.PageIndex = e.NewPageIndex;
            GetData(10, e.NewPageIndex);

        }
    }
}