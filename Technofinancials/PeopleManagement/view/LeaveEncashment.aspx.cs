﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
    public partial class LeaveEnchashment : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                GetData();

            }
        }

        private void GetData()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            // dt = FilterData(objDB.GetAllEmployeesLeavesByCompanyID(ref errorMsg));
            dt = objDB.GetAllLeaveEnCashmentByCompanyID(ref errorMsg);
            gvLeaves.DataSource = dt;
            gvLeaves.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvLeaves.UseAccessibleHeader = true;
                    gvLeaves.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All Employee Leaves Viewed", "EmployeeLeaves");

        }


        private DataTable FilterData(DataTable dt)
        {
            DataTable dtFilter = new DataTable();

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.DocName = "Leaves";

            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");
                    if (dtFilter != null)
                    {
                        if (dtFilter.Rows.Count > 0)
                        {
                            if (dtTemp != null)
                            {
                                if (dtTemp.Rows.Count > 0)
                                {
                                    dtFilter.Merge(dtTemp);
                                }
                            }
                        }
                    }


                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }
                }
            }

            return dtFilter;
        }


        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }




    }
}