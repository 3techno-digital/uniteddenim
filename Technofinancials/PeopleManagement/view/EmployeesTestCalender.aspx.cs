﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
    public partial class EmployeesTest : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                BindData();
                Common.addlog("ViewAll", "HR", "All Employee Test Calender Viewed", "EmployeeTests");

            }
        }

        private DataTable FilterData(DataTable dt)
        {
            if (dt == null)
            {
                return dt;
            }
            DataTable dtFilter = new DataTable();

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.DocName = "Tests";

            DataTable dtAccessLevel = objDB.GetUserAccessByUserIDandDocName(ref errorMsg);
            if (dtAccessLevel != null)
            {
                if (dtAccessLevel.Rows.Count > 0)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = Common.filterTable(dt, "DocStatus", "Saved as Draft");
                    dtTemp = Common.filterTable(dtTemp, "PreparedBy", Session["UserName"].ToString());

                    dtFilter = Common.ReversefilterTable(dt, "DocStatus", "Saved as Draft");

                    if (dtTemp != null)
                        dtFilter.Merge(dtTemp);

                    if (dtAccessLevel.Rows[0]["isApprover"].ToString() == "True" && dtAccessLevel.Rows[0]["isReviewer"].ToString() == "False")
                    {
                        dtFilter = Common.ReversefilterTable(dtFilter, "DocStatus", "Data Submitted for Review");

                    }

                }
            }

            return dtFilter;
        }

        private void BindData()
        {
            string ltr = @"<script>
            $('.external-event').draggable({
                revert: true,
                revertDuration: 0
            });

            var cal = $('#fullcalendar').fullCalendar({
                theme: true,
                height: 650,
                editable: true,
                droppable: true,";
            ltr += "defaultDate: '" + DateTime.Now.ToString("yyyy-MM-dd") + "',";
            ltr += @"header: {
                    left: 'prev, today, next',
                    center: 'title',
                    right: 'month, agendaWeek, agendaDay'
                },

                buttonText: {
                    prev: '',
                    next: '',
                    today: 'Today',
                    month: 'Month',
                    agendaWeek: 'Week',
                    agendaDay: 'Day'
                },
                events: [";

            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = FilterData(objDB.GetAllEmployeeTestsByCompanyID(ref errorMsg));
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ltr += @"{'category': 'warning','title': '" + dt.Rows[i]["TestTitle"] + "','start': '" + dt.Rows[i]["TestDate"] + "','end': '" + dt.Rows[i]["TestDate"] + "'},";
                    }
                }
            }

            ltr += @"],
                drop: function(date)
                    {
                        if ($('#drop-remove').is (':checked')) {
                        $(this).remove();
                        }
                    },
                dayClick: function(date, jsEvent, view)
                    {";

            ltr += "window.location = '" + "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employees-test-list?DateFilter=' + date.format();";
            ltr += @"}
                });

            $.fn.windowCheck = function()
                {
                    var ww = $(window).width();
                    if (ww > 768)
                    {
                        cal.fullCalendar('changeView', 'month');
                    }
                    else if (ww < 768 && ww > 540)
                    {
                        cal.fullCalendar('changeView', 'basicWeek');
                    }
                    else
                    {
                        cal.fullCalendar('changeView', 'basicDay');
                    }
                };

            $(window).on('resize', function (e) {
                $().windowCheck();
                });";

            ltr += "$('.fc-prev-button').append('<i class=\"fa fa-chevron-left\"><i>');$('.fc-next-button').append('<i class=\"fa fa-chevron-right\"><i>');";

            ltr += @"function addEvent()
                {
                    var title = $('#event_title').val();
                    var category = $('#event_category').val();
                    var start = $('#event_start').val();

                    cal.fullCalendar('addEventSource', [
                        {
                        title: title,
                        start: start,
                        className: [category]
                    }
                ])
                $('#new_event_modal').modal('hide');

                }
            </script>";
            ltrCalender.Text = ltr;
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }
    }
}