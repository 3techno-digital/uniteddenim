﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayrollReport.aspx.cs" Inherits="Technofinancials.PeopleManagement.view.PayrollReport" %>


<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />

            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Payroll Report</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                    <button class="AD_btn" id="btnReport" runat="server" onserverclick="btnReport_ServerClick" type="button">PDF</button>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">

                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>&nbsp;</h4>
                                    <input name="txtPayrollDate" type="text" id="txtPayrollDate" class="form-control" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
                                </div>
                                <div class="col-md-6">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-6">
                                            <h4>&nbsp;</h4>
                                            <button class="AD_btn"  id="Button1" onclick="showPayroll();" type="button">View</button>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="row D_NONE">
                                <div class="col-sm-6 D_NONE">
                                    
                                    <div class="form-group D_NONE" >
                                        <h4>Year</h4>
                                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control js-example-basic-single ">
                                            <asp:ListItem Value="">--Select Year--</asp:ListItem>
                                            <asp:ListItem Value="2018">2018</asp:ListItem>
                                            <asp:ListItem Value="2017">2017</asp:ListItem>
                                            <asp:ListItem Value="2016">2016</asp:ListItem>
                                            <asp:ListItem Value="2015">2015</asp:ListItem>
                                            <asp:ListItem Value="2014">2014</asp:ListItem>
                                            <asp:ListItem Value="2013">2013</asp:ListItem>
                                            <asp:ListItem Value="2012">2012</asp:ListItem>
                                            <asp:ListItem Value="2011">2011</asp:ListItem>
                                            <asp:ListItem Value="2010">2010</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                    
                                </div>

                                <asp:UpdatePanel ID="btnUpdPnl" runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-6">
                                            <h4>&nbsp;</h4>
                                            <button class="AD_btn eye"  id="btnView" runat="server" onserverclick="btnView_ServerClick" type="button">View</button>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group" id="divAlertMsg" runat="server">
                                            <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                <span>
                                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                </span>
                                                <p id="pAlertMsg" runat="server">
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div class="cleafix">&nbsp;</div>
                    <div class="cleafix">&nbsp;</div>



                    <div class="row">
                        <asp:UpdatePanel ID="UpdPnl" runat="server">
                            <ContentTemplate>
                                <div class="col-sm-12 gv-overflow-scrool">
                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Year">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("PayrollYear") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total BasicSalary">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("BasicSalary") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Allowances">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Allowances") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Deductions">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("Deductions") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Net Amount">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol5" Text='<%# Eval("NetAmount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <div class="row">
                        <div class="card-body table-responsive tbl-cont">
                            <table class="table table-hover tbl-pagenation table-sm gv" style="display:none;" id="PayrolTable">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Emp Code</th>
                                        <th>Emp Name</th>
                                        <th>Designation </th>
                                        <th>Grade</th>
                                        <th>Salary</th>

                                        <th>Present Days</th>
                                        <th>Leaves Days</th>
                                        <th>Overtime Hours</th>
                                        <th>Overtime Amount</th>
                                         <th>Absent Days</th>
                                         <th>Absent Amount</th>
                                        <th>E.O.B.I</th>
                                        <th>Additions</th>
                                        <th>Deductions</th>
                                        <th>Advance</th>
                                         <th>Emp PF</th>
                                        <th>Gross Salary</th>
                                        <th>Tax</th>
                                        <th>Net Salary</th>
                                        <th>Signature</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>1</td>
                                        <td>Beta 2.0_00001</td>
                                        <td>Muhammad Ahmed Shaikh</td>
                                        <td>CEO</td>
                                        <td>Executive</td>
                                        <td>1000,000</td>

                                        <td>18</td>
                                        <td>4</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>300</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>100,000</td>
                                        <td>899,700</td>
                                        <td>170,342</td>
                                        <td>729,358</td>
                                        <td> </td>
                                    </tr>
                             

                                    <tr>
                                        <td>2</td>
                                        <td>Beta 2.0_00002</td>
                                        <td>Emma Smith	</td>
                                        <td>COO</td>
                                        <td>Executive</td>
                                        <td>700,000</td>
                                        <td>20</td>
                                        <td>2</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>300</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>70,000</td>
                                        <td>629,700</td>
                                        <td>103,766</td>
                                        <td>525,934 </td>
                                        <td> </td>
                                    </tr>

                                    
                                    <tr>
                                        <td>3</td>
                                        <td>Beta 2.0_00003</td>
                                        <td>Sophia WILLIAMS	 </td>
                                        <td> Receptionist</td>
                                         <td> Non-Executive</td>
                                        <td> 500,000</td>
                                        <td>19</td>
                                        <td>3</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>300</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>50,000</td>
                                        <td>449,700</td>
                                        <td>63,266</td>
                                        <td>386,434</td>
                                        <td></td>
                                    </tr>



                                    <tr>
                                        <td>4</td>
                                        <td>Beta 2.0_00004</td>
                                        <td>James Smith	</td>
                                        <td>Project Manager</td>
                                        <td>Non-Executive</td>
                                        <td>250,000</td>
                                        <td>21</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>8,333.333</td>
                                        <td>300</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>25,000</td>
                                        <td>216,366.6667</td>
                                        <td>17,656</td>
                                        <td>188,710.7 </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            5
                                        </td>
                                        <td>
                                            	Beta 2.0_00005
                                        </td>
                                        <td>
                                           Thomas Lee	
                                        </td>
                                         <td>
                                            		Head of Sales
                                        </td>
                                         <td>
                                            Executive
                                        </td>
                                        <td>
                                            500,000
                                        </td>
                                        
                                          <td>22</td>
                                        <td>0</td>
                                        <td>16</td>
                                        <td>3,333.33</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>300</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>5,000</td>
                                        <td>48,033.33</td>
                                        <td>0</td>
                                        <td>48,033.33 </td>
                                        <td></td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                                        <div class="clear-fix">&nbsp;</div>
                    <div class="clear-fix">&nbsp;</div>
                    <div class="clear-fix">&nbsp;</div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>


        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757 !important;
                border-radius: 0px !important;
                box-shadow: 0 8px 6px -5px #cacaca !important;
                height: 38px !important;
                width: 37px !important;
                display: inline-block !important;
                padding: 5px !important;
                position: relative !important;
                text-align: center !important;
            }
        </style>

        <script>
            function showPayroll() {
                $("#PayrolTable").css("display", "block");
            }

        </script>
    </form>
</body>
</html>
