﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Technofinancials.PeopleManagement.view
{
    public partial class ETS : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                BindEmployeeDropdown();
            }

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void BindEmployeeDropdown()
        {
            try
            {
                CheckSessions();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--- Select Employee ---", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                
                DataTable dt = new DataTable();
                int lastLoc = 0;
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.Date = txtDate.Value;
                dt = objDB.GetUserLatLng(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        string Script = "<script>" +
                                    "var map;" +
                                    "function initialize()" +
                                    "{" +
                                    "var mapOptions = {" +
                                    "center: new google.maps.LatLng(24.8607,67.0011)," +
                                    "zoom: 11," +
                                    "mapTypeId: google.maps.MapTypeId.ROADMAP" +
                                    "};" +
                                    "map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);" +
                                    "var myTrip = new Array();";
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        Script += "myTrip.push(new google.maps.LatLng("+ dt.Rows[i]["Lat"] + ","+ dt.Rows[i]["Lng"] + "));";
                                        lastLoc = i;
                                    }
                                     Script += "var flightPath = new google.maps.Polyline({" +
                                    "path: myTrip,"+
                                    "strokeColor: '#0000FF',"+
                                    "strokeOpacity: 0.8,"+
                                    "strokeWeight: 2"+
                                    "});"+
                                    "flightPath.setMap(map);"+
                                    "var infowindowS = new google.maps.InfoWindow({" +
                                    "content: 'Starting Point'" +
                                    "});" +
                                    "var infowindowE = new google.maps.InfoWindow({" +
                                    "content: 'Ending Point'" +
                                    "});" +
                                    "var markerS = new google.maps.Marker({" +
                                    "position: new google.maps.LatLng(" + dt.Rows[0]["Lat"] + "," + dt.Rows[0]["Lng"] + ")," +
                                    "map: map," +
                                    "title: 'Starting Point'," +
                                    "});" +
                                    "var markerE = new google.maps.Marker({" +
                                    "position: new google.maps.LatLng(" + dt.Rows[lastLoc]["Lat"] + "," + dt.Rows[lastLoc]["Lng"] + ")," +
                                    "map: map," +
                                    "title: 'Ending Point'," +
                                    "icon:'http://ethlonmedia.com/assets/images/rikshaw.png'"+
                                    "});" +
                                    "markerS.addListener('click', function() {" +
                                    "infowindowS.open(map, markerS);" +
                                    "});"+
                                    "markerE.addListener('click', function() {" +
                                    "infowindowE.open(map, markerE);" +
                                    "});" +
                                    "}" +
                                    "initialize();"+
                                    "</script>";
                                    ltrMap.Text = Script;
                        
                    }
                    else
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Recode Found";
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Recode Found";
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message; 
            }
        }
    }
}