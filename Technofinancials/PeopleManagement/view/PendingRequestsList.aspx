﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PendingRequestsList.aspx.cs" Inherits="Technofinancials.PeopleManagement.view.PendingRequestsList" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>
		.form-group button#btnView {
			padding: 3px 24px !important;
			margin-top: 33px !important;
		}

		.tf-note-btn {
			background-color: #575757;
			padding: 10px 8px 7px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-note-btn i {
				color: #fff !important;
			}

		.tf-disapproved-btn {
			background-color: #575757;
			padding: 10px 8px 7px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-disapproved-btn i {
				color: #fff !important;
			}


		.tf-add-btn {
			background-color: #575757;
			padding: 4px 9px 4px 10px !important;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-add-btn i {
				color: #fff !important;
			}

		.tf-add-btn {
			background-color: #575757;
			padding: 12px 10px 8px 10px !important;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-add-btn i {
				color: #fff !important;
			}

		.tf-back-btn {
			background-color: #575757;
			padding: 10px 10px 10px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
			padding: 5px !important;
			height: 38px !important;
			width: 37px !important;
			display: inline-block !important;
			text-align: center !important;
		}

			.tf-back-btn i {
				color: #fff !important;
			}

		.table > thead:first-child > tr:first-child > th {
			font-size: 11px;
		}
	</style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">

			<input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
			<div class="wrap">

				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
								<h1 class="m-0 text-dark">Pending Request For Managers</h1>
							</div>
							<!-- /.col -->
							<!-- /.col -->
						</div>
						<!-- /.row -->

					</div>
					<!-- /.container-fluid -->
				</div>
				<section class="app-content">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">

								<div class="col-sm-6">
									<div class="form-group">
										<h4>Month
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtPendingDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
										<asp:TextBox runat="server" CssClass="form-control" ID="txtPendingDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
									</div>
								</div>
							</div>

						</div>
						<div class="col-lg-4 col-md-6 col-sm-12"></div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<asp:UpdatePanel ID="UpdatePanel4" runat="server">
								<ContentTemplate>
									<div class="form-group" id="divAlertMsg" runat="server">
										<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
											<span>
												<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
											</span>
											<p id="pAlertMsg" runat="server">
											</p>
										</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>
					</div>
					<div class="clear-fix">&nbsp;</div>
					<div class="row ">
						<div class="col-sm-12">
							<div class="tab-content ">
								<div class="tab-pane active row">
									<div class="col-sm-12 gv-overflow-scrool">

										<asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
											<Columns>
												<asp:TemplateField HeaderText="Sr.No.">
													<ItemTemplate>
														<%#Container.DataItemIndex+1 %>
													</ItemTemplate>
												</asp:TemplateField>

												<asp:TemplateField HeaderText="Employee Code">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblWDID" Text='<%#  Eval("MWDID") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>

												<asp:TemplateField HeaderText="Manager Name">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Manager") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Department">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblCol3" Text='<%# Eval("deptname") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>


												<asp:TemplateField HeaderText="Attendance Adjustments">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblAttAdjustmentCount" Text='<%# Eval("AttAdjustmentCount") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Break Adjustments">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblBreakAdjustment" Text='<%# Eval("BreakAdjustment") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Addons">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblAddOnsCount" Text='<%# Eval("AddOnsCount") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Expenses">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblOtherExpenseCount" Text='<%# Eval("OtherExpenseCount") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Total Requests">
													<ItemTemplate>
														<asp:Label runat="server" ID="lblTotalRequests" Text='<%# Eval("TotalRequests") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>

												<asp:TemplateField HeaderText="View">
													<ItemTemplate>
														<a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/people-management/manage/pending-request/view-pending-request-" + Eval("ManagerId") %>" class="AD_stock">View </a>
														<%--<asp:LinkButton runat="server" CssClass="edit-classs" CommandArgument='<%# Eval("AddOnsID") %>' OnClick="lnkCommand_Click"> View</asp:LinkButton>--%>
													</ItemTemplate>
												</asp:TemplateField>

											</Columns>
										</asp:GridView>

									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
			
			<div class="clear-fix">&nbsp;</div>
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>
		<script src="/business/scripts/ViewDesignations.js"></script>
	</form>
</body>
</html>

