﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeHistory.aspx.cs" Inherits="Technofinancials.PeopleManagement.view.EmployeeHistory" %>
          

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
     <%--       <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />--%>

            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h3 class="tf-page-heading-text">Employee Profile</h3>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                    <%--<button class="AD_btn" "Generate Report" id="btnReport" runat="server" onserverclick="btnReport_ServerClick" type="button">PDF</button>--%>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>

                <section class="app-content">


                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Select Employee</label>
                                        <select class="form-control select2" data-plugin="select2">
                                            <option>Muhammad Ahmed Shaikh</option>
                                            <option>Emma Smith</option>
                                            <option>Sophia WILLIAMS</option>
                                            <option>James Smith</option>
                                            <option>Thomas Lee</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-6 col-sm-12">
                           
                        </div>
                    </div>

                    <div class="content-header second_heading">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-sm-6">
                                        <h1 class="m-0 text-dark">Basic Information</h1>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Employe Code </h4>
                                        <input class="form-control" placeholder="Beta 2.0_00001" disabled="disabled" type="text" name="employerNo" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>CNIC</h4>
                                        <input class="form-control cnic-val" placeholder="xxxxx-xxxxxxx-x" disabled="disabled" maxlength="15" type="text" />
                                    </div>
                                </div>

                                <div class="col-lg-4 form-group">
                                    <h4>First Name</h4>
                                    <input class="form-control AphabetOnly" placeholder="Muhammad" type="text" disabled="disabled" id="txtFirstName" runat="server" name="firstName" />
                                </div>
                                <div class="col-lg-4 form-group">
                                    <h4>Middle Name</h4>
                                    <input class="form-control AphabetOnly" placeholder="Ahmed" type="text" disabled="disabled" id="txtMiddleName" runat="server" name="middleName" />
                                </div>
                                <div class="col-lg-4 form-group">
                                    <h4>Last Name</h4>
                                    <input class="form-control AphabetOnly" placeholder="Shaikh" type="text" disabled="disabled" id="txtLastName" runat="server" name="lastName" />
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                    </div>



                    <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark">Current Employee Details</h1>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                        <!-- /.container-fluid -->
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Employment Type</h4>
                                        <input class="form-control " disabled="disabled" placeholder="Permanent" type="text" id="" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Date of Joining</h4>
                                        <input class="form-control " disabled="disabled" placeholder="10-Jul-2020" type="text" id="" />
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Job Description</h4>
                                        <input class="form-control " disabled="disabled" placeholder="CEO" type="text" id="" />
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Grade</h4>
                                        <input class="form-control " disabled="disabled" placeholder="Executive" type="text" id="" />
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Department</h4>
                                        <input class="form-control "disabled="disabled" placeholder="Directors" type="text" id="" />
                                    </div>
                                     </div>
                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Designation</h4>
                                        <input class="form-control " disabled="disabled" placeholder="CEO" type="text" id="" />
                                    </div>
                                </div>
                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Place of posting</h4>
                                        <input class="form-control " disabled="disabled" placeholder="Karachi" type="text" id="" />
                                    </div>
                                </div>
                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Cost Centre</h4>
                                        <input class="form-control " disabled="disabled" placeholder="Centre" type="text" id="" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                    </div>

                    <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark">2020 - Present</h1>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                        <!-- /.container-fluid -->
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                               
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Grade</h4>
                                        <input class="form-control " disabled="disabled" placeholder="Executive" type="text" id="" />
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Department</h4>
                                        <input class="form-control "disabled="disabled" placeholder="Directors" type="text" id="" />
                                    </div>
                                     </div>
                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Designation</h4>
                                        <input class="form-control " disabled="disabled" placeholder="CEO" type="text" id="" />
                                    </div>
                                </div>
                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Place of posting</h4>
                                        <input class="form-control " disabled="disabled" placeholder="Karachi" type="text" id="" />
                                    </div>
                                </div>
                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Salary</h4>
                                        <input class="form-control " disabled="disabled" placeholder="1,000,000" type="text" id="" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                    </div>



                    <div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark">2019 - 2020</h1>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                        <!-- /.container-fluid -->
                    </div>



                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                               
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Grade</h4>
                                        <input class="form-control " disabled="disabled" placeholder="Executive" type="text" id="" />
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Department</h4>
                                        <input class="form-control "disabled="disabled" placeholder="Directors" type="text" id="" />
                                    </div>
                                     </div>
                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Designation</h4>
                                        <input class="form-control " disabled="disabled" placeholder="CEO" type="text" id="" />
                                    </div>
                                </div>
                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Place of posting</h4>
                                        <input class="form-control " disabled="disabled" placeholder="Karachi" type="text" id="" />
                                    </div>
                                </div>
                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <h4>Salary</h4>
                                        <input class="form-control " disabled="disabled" placeholder="700,000" type="text" id="" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                    </div>


                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
         


    </form>
</body>
</html>