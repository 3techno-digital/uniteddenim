﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="payroll_PF_Detail.aspx.cs" Inherits="Technofinancials.PeopleManagement.view.payroll_PF_Detail" %>
    
<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Finance/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
            <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }
            button#btnView {
    padding: 3px 24px;
    margin-top: 7px;
}
                .tf-note-btn i {
                    color: #fff !important;
                }


            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
                    padding: 5px!important;
    height: 38px!important;
    width: 37px!important;
    display: inline-block!important;
    text-align: center!important;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

        </style>

</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">

                  <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Provident Fund Detail</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">

                                     <% 
                                                              string SpotlitURL = "";
                                                              Session["SpotlitURL"] = ""; 
                                                              if (Session["CompanyID"] != null)
                                                              {
                                                                  if (Session["ParentCompanyID"].ToString() == "53" || Session["CompanyID"].ToString() == "53")
                                                                  {

                                                                      SpotlitURL = "spotlit-";
                                                                  }
                                                              } %>

                                  
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>


                <section class="app-content">
                               <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <h4>Payroll Date  <span style="color: red !important;">*</span>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPayrollDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                        <%--<input class="form-control" id="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" placeholder="Grant Date" type="text" runat="server"   onserverchange="txtPayrollDate_ServerChange"  />--%>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <h4>&nbsp;</h4>
                                    <button class="AD_btn_inn" title= "View" id="btnView" runat="server" onserverclick="btnView_ServerClick" type="button">View</button>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="clear-fix">&nbsp;</div>
                    <div class="row ">
                      <div class="col-sm-12">
                            <div class="tab-content ">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                     
                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Payroll Month">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%#  DateTime.Parse(Eval("NewPayrollDate").ToString()).ToString("MMM-yyyy")  %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Employee Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("ktid") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="clear-fix">&nbsp;</div>
                    <div class="clear-fix">&nbsp;</div>
                    <div class="clear-fix">&nbsp;</div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

    </form>
</body>
</html>
