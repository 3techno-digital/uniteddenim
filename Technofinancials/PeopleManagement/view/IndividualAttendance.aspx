﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IndividualAttendance.aspx.cs" Inherits="Technofinancials.PeopleManagement.view.IndividualAttendance" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Time In-Out Report</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h4>Date </h4>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtPayrollDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red" />
                                                <input name="txtPayrollDate" type="text" id="txtPayrollDate" class="form-control datetime-picker" data-date-format="MMM-YYYY" runat="server" />

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h4>Employee</h4>
                                                <%--                                        <select class="form-control select2" data-plugin="select2" runat="server" id="ddlEmployeess">
                                            <option>-- Select --</option>
                                            <option value="Employee1">Muhammad Ahmed Shaikh</option>
                                            <option value="Employee2">Emma Smith</option>
                                            <option value="Employee1">Sophia WILLIAMS</option>
                                            <option value="Employee1">James Smith</option>
                                            <option value="Employee1">Thomas Lee</option>
                                           
                                        </select>--%>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator26" ControlToValidate="ddlEmployee" InitialValue="0" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red" />
                                                <asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2" data-plugin="select2">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4>&nbsp;</h4>
                                            <asp:Button ID="btnView" ValidationGroup="btnView" runat="server" Text="View" class="AD_btn_inn"  data-original-title="" OnClick="btnView_Click" />

                                            <%--<button class="AD_btn" "" type="button" runat="server" onclick="ShowTable();" data-original-"">View</button>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>

                            </div>


                            <div class="row" style="display: none;">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="row">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">

                                <div class="col-md-12">
                                </div>

                                <div class="col-sm-12 gv-overflow-scrool">
                                    <%--              <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" ShowHeaderWhenEmpty="true" OnRowDataBound="gv_RowDataBound">
                                    </asp:GridView>--%>
                                    <table class="table table-bordered" id="tbl">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Attendance</th>
                                                <th>Time In</th>
                                                <th>Time Out</th>
                                              <%--  <th>Reason</th>
                                                <th style="display:none;">Comments</th>
                                                <th style="display:none;">Action</th>--%>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rpAttendance" runat="server" OnItemDataBound="rpAttendance_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%#Eval("Date") %></td>
                                                        <td>
                                                            <asp:Label ID="lblAttendance" runat="server" Text='<%# Eval("Resultant") %>'></asp:Label>
                                                        </td>
                                                        <%--<td class="late">11:00 AM</td>--%>
                                                        <td>
                                                            <asp:Label ID="lblTimeIn" runat="server" Text='<%#Eval("Login Time") %>'></asp:Label></td>
                                                        <td>
                                                            <asp:Label ID="lblTimeOut" runat="server" Text='<%#Eval("Logout Time") %>'></asp:Label>
                                                        </td>
                                                       <%-- <td>
                                                            <asp:TextBox ID="txtReason" runat="server" class="form-control" Visible="true"></asp:TextBox>
                                                            <asp:Label ID="lblReason" runat="server" Text='<%#Eval("Late / Over Time Reason") %>'></asp:Label>
                                                        </td>
                                                        <td style="display:none;">
                                                            <asp:LinkButton ID="btnAddReason" runat="server" class="AD_stock form-control" Visible="false" CommandName="UpdateReason" CommandArgument='<%#Eval("AttendanceID") %>' OnClick="btnAddReason_Click">Add</asp:LinkButton>
                                                        </td>
                                                        --%>

                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </ContentTemplate>
                </asp:UpdatePanel>


                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }
            input#btnView {
                padding: 3px 14px !important;
                margin-top: 11px !important;
            }
            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757 !important;
                border-radius: 0px !important;
                box-shadow: 0 8px 6px -5px #cacaca !important;
                height: 38px !important;
                width: 37px !important;
                display: inline-block !important;
                padding: 5px !important;
                position: relative !important;
                text-align: center !important;
            }
        </style>

        <script>
            function AddReason() {
                $('#lblReason').css('display', 'block');
                $('#lblReason').text($('#txtReason').val());
                $('#lblReasonShow').text($('#txtReason').val());
                $('#txtReason').css('display', 'none');
                $('#lblReasonShow').css('display', 'block');

            }

            function AddComment() {
                $('#lblComment').css('display', 'block');
                $('#lblComment').text($('#txtComment').val());
                $('#lblCommentApprove-disApprove').text('DisApproved');
                //$('#txtComment').val($('#txtComment').val());
                $('#lblCommentApprove-disApprove').css('display', 'block');
                $('#txtComment').css('display', 'none');
                $('#btnCommentDisapprove').css('display', 'none');
                $('#btnCommentApprove').css('display', 'none');

            }

            function AddCommentApprove() {
                $('#lblComment').css('display', 'block');
                $('#lblComment').text($('#txtComment').val());
                $('#lblCommentApprove-disApprove').text('Approved');
                $('#lblCommentApprove-disApprove').css('display', 'block');
                //$('#txtComment').val($('#txtComment').val());
                $('#txtComment').css('display', 'none');
                $('#btnCommentDisapprove').css('display', 'none');
                $('#btnCommentApprove').css('display', 'none');

            }
            function ShowTable() {
                if ($('#ddlEmployeess').val() == 'Employee1') {
                    $('#tblEmployee1').css('display', 'block');
                    $('#tblEmployee2').css('display', 'none');
                } else if ($('#ddlEmployeess').val() == 'Employee2') {
                    $('#tblEmployee1').css('display', 'none');
                    $('#tblEmployee2').css('display', 'block');
                }
            }

            var table = document.getElementById('tbl');
            var tbody = table.getElementsByTagName('tbody')[0];
            var cells = tbody.getElementsByTagName('td');

            for (var i = 0, len = cells.length; i < len; i++) {
                if (cells[i].innerHTML.includes('Absent')) {
                    cells[i].className = 'late';
                }
                else if (cells[i].innerHTML == 'Casual Leave') {
                    cells[i].className = 'casual';
                }
                else if (cells[i].innerHTML == 'Sick Leave') {
                    cells[i].className = 'sick';
                }
                else if (cells[i].innerHTML == 'Off') {
                    cells[i].className = 'offf';
                }
                else if (cells[i].innerHTML == 'Present') {
                    cells[i].className = 'pres';
                }               
                else {
                    cells[i].className = 'not-marekd';
                }

            }

        </script>
    </form>
</body>
</html>
