﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeesList.aspx.cs" Inherits="Technofinancials.PeopleManagement.view.EmployeesList" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Employees</h1>
                            </div>  
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                    <a class="AD_btn" href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/manage/employees/add-new-employee"); %>">Add</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-md-4">
                        </div>

                        <div class="col-md-4">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="form-group" id="divAlertMsg" runat="server">
                                        <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                            <span>
                                                <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                            </span>
                                            <p id="pAlertMsg" runat="server">
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </div>

                     <div class="row ">
                        <asp:HiddenField ID="hdnIsRedirect" runat="server" />
                        <asp:HiddenField ID="hdnLinkRedirect" runat="server" />
                        <div class="col-sm-12">
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12 gv-overflow-scrool" style="padding-bottom:60px;">
                                        <asp:GridView  ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Image">
                                                    <ItemTemplate>
                                                        <asp:Image runat="server" class="tableUserImage" ID="imgEmployee" ImageUrl='<%# Eval("EmployeePhoto") %>'></asp:Image>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Employee Code">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblWDID" Text='<%# Eval("WorkDayID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Cost Center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCostCenter" Text='<%# Eval("CostCenterName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Department">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Designation">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol11" Text='<%# Eval("DesgTitle")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                   <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol11" Text='<%# Eval("PlaceOfPost")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Net Salary">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol20" Text='<%# (Eval("NetSalary").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("NetSalary").ToString())):Eval("NetSalary").ToString() %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="View">
                                                    <ItemTemplate>
                                                        <a  href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/people-management/manage/employees/edit-employee-" + Eval("EmployeeID") %>" class="AD_stock">View </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                    
                                        </asp:GridView>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                  <%--  Pagination by Mutahar Oct 2021 --%>
                   <%-- <div class="row ">
                        <asp:HiddenField ID="hdnIsRedirect" runat="server" />
                        <asp:HiddenField ID="hdnLinkRedirect" runat="server" />
                        <div class="col-sm-12">
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12 gv-overflow-scrool" style="padding-bottom:60px;">
                                        <asp:GridView  ID="_gv" runat="server" CssClass="table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" AllowCustomPaging="True" AllowPaging="True" OnPageIndexChanging="gv_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Image">
                                                    <ItemTemplate>
                                                        <asp:Image runat="server" class="tableUserImage" ID="imgEmployee" ImageUrl='<%# Eval("EmployeePhoto") %>'></asp:Image>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Employee Code">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblWDID" Text='<%# Eval("WorkDayID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEmployeeName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Cost Center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCostCenter" Text='<%# Eval("CostCenterName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Designation">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("DesgTitle") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol11" Text='<%# Eval("GradeName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Net Salary">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol20" Text='<%# (Eval("NetSalary").ToString() != "")? string.Format("{0:n2}", Convert.ToDouble(Eval("NetSalary").ToString())):Eval("NetSalary").ToString() %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="View">
                                                    <ItemTemplate>
                                                        <a  href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/people-management/manage/employees/edit-employee-" + Eval("EmployeeID") %>" class="AD_stock">View </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                        </asp:GridView>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>--%>

                    <%--<div class="content-header second_heading">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="tf-page-heading-text">Separated Employees</h1>
                                </div>
                                <!-- /.col -->

                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <div class="row ">
                        <div class="col-sm-12">
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12 gv-overflow-scrool">
                                        <asp:GridView ID="gvSeparated" runat="server" CssClass="gv table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No.">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Code">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol5" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("DocStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Separation Type">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblTypeOfSeperation" Text='<%# Eval("TypeOfSeperation") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol11" Text='<%# Eval("EmpStatus")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Prepared Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("PreparedDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Summary">
                                                    <ItemTemplate>
                                                        <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/people-management/manage/emp-summary/generate-emp-summary-" + Eval("EmployeeID") %>">Generate</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Warning Letter">
                                                    <ItemTemplate>
                                                        <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/people-management/manage/warning-letter/generate-warning-letter-" + Eval("EmployeeID") %>">Generate</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Black List">
                                                    <ItemTemplate>
                                                        <a onclick="showModal('<%# Eval("EmployeeID") %>')" data-toggle="modal" data-target="#myModal" class="AD_stock">Black List</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View">
                                                    <ItemTemplate>
                                                        <a href="<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/people-management/manage/employees/edit-employee-" + Eval("EmployeeID") %>" class="AD_stock" >View </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>

                <%--    <div class="col-sm-12 gv-overflow-scrool" style="display: none;">
                        <asp:GridView ID="gvDownload" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static">
                        </asp:GridView>
                    </div>--%>

<%--
                    <div class="modal fade" id="UploaderGvModel" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <asp:UpdatePanel ID="upModal" runat="server">
                                <ContentTemplate>
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" style="float: right;" class="btn btn-default" runat="server" validationgroup="btnSaveAdnUpload" data-dismiss="modal" onserverclick="btnSave_ServerClick">Save & Close</button>
                                            <h4 class="modal-title">Uploaded Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active row" style="overflow-x: auto; overflow-y: auto; height: 600px;">
                                                            <div class="col-sm-12 ">
                                                                <asp:GridView ID="gvUploader" runat="server" CssClass="table table-bordered">
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
--%>


                    <div class="modal fade M_set" id="myModal" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="m-0 text-dark">Reason</h1>
                                    <div class="add_new">
                                        <button type="button" class="AD_btn" id="btnToggleBlackListing" runat="server" onserverclick="btnToggleBlackListing_ServerClick">Black List</button>
                                        <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <asp:HiddenField ID="hdnEmployeeID" runat="server" ClientIDMode="Static" />
                                        <textarea id="txtReason" runat="server" rows="5" placeholder="Reason (max 100 characters).." maxlength="100" class="form-control"></textarea>
                                    </p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h4>Attachments</h4>
                                            </div>
                                            <div class="file-upload">
                                                <div class="image-upload-wrap">
                                                    <input class="file-upload-input" type="file" name="uploadFile" runat="server" id="updLogo" onchange="readURL(this);" accept="image/*">
                                                    <div class="drag-text">
                                                        <h3>Drag and drop a file or select add Image (MaxSize: 2mb)</h3>
                                                    </div>
                                                </div>
                                                <div class="file-upload-content">
                                                    <img class="file-upload-image" src="#" alt="Supplier image">
                                                    <div class="image-title-wrap">
                                                        <button type="button" onclick="removeUpload()" class="remove-image">
                                                            Remove  <span><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>
            $("#FileUpload1").change(function () {
                __doPostBack('LinkUploadFiles', '');
            });



        </script>
        <script type="text/javascript">
            function showModalEmployees() {
                $("#UploaderGvModel").modal('show');
            }

            function closeModel() {
                $("#UploaderGvModel").modal('hide');
            }


            $("#btnDownload").on("click", function () {
                var table = $('#gvDownload').DataTable();
                table.button('.buttons-excel').trigger();

            });

        </script>

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var ext = input.files[0].name.split('.').pop().toLowerCase();
                    if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
                        removeUpload();
                        alert('invalid extension!');
                    }
                    else {
                        var reader = new FileReader();
                        var myimg = '';
                        reader.onload = function (e) {
                            $('.image-upload-wrap').hide();
                            $('.file-upload-image').attr('src', e.target.result);
                            $('.file-upload-content').show();
                            $('.image-title').html(input.files[0].name);
                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                else {
                    removeUpload();
                }
            }
            function removeUpload() {
                $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                $('.file-upload-content').hide();
                $('.image-upload-wrap').show();
            }
            $('.image-upload-wrap').bind('dragover', function () {
                $('.image-upload-wrap').addClass('image-dropping');
            });
            $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
            });



            //$("#_gv").DataTable({
            //    dom: 'Bfrtip',
            //    retrieve: true,
            //    searching: true,
            //    lengthChange: true,
            //    bPaginate: false,
            //    buttons: [
            //        'print', 'pdf', 'excel'
            //    ],
            //    pageLength: 10,
            //    fnDrawCallback: function (settings) {

            //        console.log(settings._iDisplayStart);
            //        console.log(settings);
            //    }
            //});

            //$('.buttons-excel span').html('<span "Excel Export"><i class="fa fa-file-excel-o icn2" aria-hidden="true"></i></span>');
            //$('.buttons-pdf span').html('<span "PDF Export"><i class="fa fa-file-pdf-o icn2" aria-hidden="true"></i></span>');
            //$('.buttons-print span').html('<span "Print"><i class="fa fa-print icn2" aria-hidden="true"></i></span>');



        </script>


        <style>
            a.AD_stock {
                cursor: pointer !important;
            }
            .barcode {
                width: 120px;
                height: 80px;
            }

            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }
                        div#gv_wrapper{
                margin-bottom:15%;
            }
                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }
                /*gridview*/
.table table  tbody  tr  td a ,
.table table  tbody  tr  td  span {
position: relative;
float: left;
padding: 6px 12px;
margin-left: -1px;
line-height: 1.42857143;
color: #337ab7;
text-decoration: none;
background-color: #fff;
border: 1px solid #ddd;

}

.table table > tbody > tr > td > span {
z-index: 3;
color: #fff;
cursor: default;
background-color: #337ab7;
border-color: #337ab7;
}

.table table > tbody > tr > td:first-child > a,
.table table > tbody > tr > td:first-child > span {
margin-left: 0;
border-top-left-radius: 4px;
border-bottom-left-radius: 4px;
}

.table table > tbody > tr > td:last-child > a,
.table table > tbody > tr > td:last-child > span {
border-top-right-radius: 4px;
border-bottom-right-radius: 4px;
}

.table table > tbody > tr > td > a:hover,
.table   table > tbody > tr > td > span:hover,
.table table > tbody > tr > td > a:focus,
.table table > tbody > tr > td > span:focus {
z-index: 2;
color: #23527c;
background-color: #eee;
border-color: #ddd;
}
            table {
        width: 30%;
            }
        </style>
        <script>
            function showModal(id) {
                $('#hdnEmployeeID').val(id);
                //$('#myModal').show();
            }
        </script>
    </form>
</body>
</html>

