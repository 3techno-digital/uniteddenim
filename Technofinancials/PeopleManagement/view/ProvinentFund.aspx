﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProvinentFund.aspx.cs" Inherits="Technofinancials.PeopleManagement.view.ProvinentFund" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->

        <main id="app-main" class="app-main">
            <asp:UpdateProgress ID="updProgress"
                AssociatedUpdatePanelID="UpdPnl"
                runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Provident Fund Summary</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div style="text-align: right;">
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <asp:UpdatePanel ID="UpdPnl" runat="server">
                    <ContentTemplate>
                        <section class="app-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                </div>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="clearfix">&nbsp;</div>


                            <div class="row">
                                <div class="col-lg-12">

                                    <table class="table table-bordered gv">
                                        <thead>
                                            <tr>
                                                <th>Employer Ratio</th>
                                                <th>Employee Ratio</th>
                                                <th>Total</th>
                                                <th>Withdraw</th>
                                                <th>Balance</th>
                                                <th>Detail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>25,00,000</td>
                                                <td>25,00,000</td>
                                                <td>50,00,000</td>
                                                <td>0</td>
                                                <td>50,00,000</td>
                                                <td>
                                                    <button class="AD_stock" data-toggle="modal" data-target="#notes-modal" "View Detail" type="button" onclick="">Detail</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="modal fade M_set" id="notes-modal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="m-0 text-dark">Provident Fund Detail</h1>
                                            <div class="add_new">

                                                <button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
                                            </div>
                                        </div>
                                        <div class="modal-body" style="padding-bottom: 50px;">
                                            <table class="table table-bordered gv">
                                                <thead>
                                                    <tr>
                                                        <th>Sr. No</th>
                                                        <th>Employee</th>
                                                        <th>Employer Ratio</th>
                                                        <th>Employee Ratio</th>
                                                        <th>Total</th>
                                                        <th>Withdraw</th>
                                                        <th>Balance</th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Muhammad Ahmed Shaikh</td>
                                                        <td>100,000</td>
                                                        <td>100,000</td>
                                                        <td>200,000</td>
                                                        <td>0</td>
                                                        <td>200,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Emma Smith</td>
                                                        <td>70,000</td>
                                                        <td>70,000</td>
                                                        <td>140,000</td>
                                                        <td>0</td>
                                                        <td>140,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Sophia WILLIAMS</td>
                                                        <td>50,000</td>
                                                        <td>50,000</td>
                                                        <td>100,000</td>
                                                        <td>0</td>
                                                        <td>100,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>James Smith</td>
                                                       <td>25,000</td>
                                                        <td>25,000</td>
                                                        <td>50,000</td>
                                                        <td>0</td>
                                                        <td>50,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>Thomas Lee</td>
                                                        <td>5,000</td>
                                                        <td>5,000</td>
                                                        <td>10,000</td>
                                                        <td>0</td>
                                                        <td>10,000</td>
                                                    </tr>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </section>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->

        <%--</ContentTemplate>--%>
        <%--        <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="serverclick" />
            </Triggers>--%>
        <%--</asp:UpdatePanel>--%>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
    <!--========== check if this page is working -->
</body>
</html>
