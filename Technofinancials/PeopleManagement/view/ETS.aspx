﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ETS.aspx.cs" Inherits="Technofinancials.PeopleManagement.view.ETS" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="/assets/images/Candidates.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Employees Tracking</h3>
                        </div>

                         <div class="col-md-4">
                                    
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        
                                </div>

                        <div class="col-sm-4" style="margin-top: 10px;">
                            <div class="pull-right">
                                <%--<a class="tf-add-btn" "Add" href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/manage/candidates/add-new-candidate"); %>"><i class="far fa-plus-circle"></i></a>--%>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-12">
                            <div class="tab-content ">
                                <div class="tab-pane active row">


                                    <div class="row">

                                        <div class="col-md-3">
                                            <h4>Date <span style="color: red !important;">*</span></h4>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                              <input class="form-control" id="txtDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" placeholder="Date" type="text" runat="server" />
                                        </div>


                                        <div class="col-md-3">
                                            <h4>Employee <span style="color: red !important;">*</span></h4>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator31" ControlToValidate="ddlEmployee" InitialValue="0" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" />
                                            <asp:DropDownList ID="ddlEmployee" runat="server" data-plugin="select2" CssClass="form-control select2"></asp:DropDownList>
                                        </div>


                                        <div class="col-md-3">
                                            <h4>&nbsp;</h4>

                                            <button class="tf-view-btn" "View" id="btnView" type="button" runat="server" ValidationGroup="btnValidate" onserverclick="btnView_ServerClick" ><i class="far fa-eye"></i></button>
                                        </div>

                                    </div>
                                                               <br/>
                                                               <br/>
 <%--<div id="googleMap" style="width:100%;height:400px;"></div>                                 
<script>
function myMap() {
var mapProp= {
  center:new google.maps.LatLng(24.8607,67.0011),
  zoom:13,
};
var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3RwBupXy123Fdul5XtIAWjDsF9f8ogyLam4&callback=myMap"></script>
--%>

                                     <div id="map-canvas" style="width: 100%; height: 70vh; margin-bottom:20px;"></div>

                                      


                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>


         <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8S6XyTsgQOMAej0zmYjV6OcqcFXhZ8Hc&callback=initialize"></script>

         <asp:Literal ID="ltrMap" runat="server"></asp:Literal>

        <style>
            .add-table-div {
                margin-bottom: 70px;
            }

            .tf-add-btn {
                padding: 12px 10px 8px 10px !important;
            }
        </style>
    </form>
</body>
</html>
