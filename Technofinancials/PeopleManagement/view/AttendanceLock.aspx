﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttendanceLock.aspx.cs" Inherits="Technofinancials.PeopleManagement.view.AttendanceLock" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
	<style>
		.tf-note-btn {
			background-color: #575757;
			padding: 10px 8px 7px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-note-btn i {
				color: #fff !important;
			}

		.tf-disapproved-btn {
			background-color: #575757;
			padding: 10px 8px 7px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-disapproved-btn i {
				color: #fff !important;
			}

		div#gv_wrapper {
			margin-bottom: 20%;
		}

		.tf-add-btn {
			background-color: #575757;
			padding: 4px 9px 4px 10px !important;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-add-btn i {
				color: #fff !important;
			}

		.tf-add-btn {
			background-color: #575757;
			padding: 12px 10px 8px 10px !important;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center span.multiselect-selected-text {
			white-space: nowrap;
		}

		.tf-add-btn i {
			color: #fff !important;
		}

		.tf-back-btn {
			background-color: #575757;
			padding: 10px 10px 10px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

			.tf-back-btn i {
				color: #fff !important;
			}

		.tf-back-btn {
			background-color: #575757 !important;
			border-radius: 0px !important;
			box-shadow: 0 8px 6px -5px #cacaca !important;
			height: 38px !important;
			width: 37px !important;
			display: inline-block !important;
			padding: 5px !important;
			position: relative !important;
			text-align: center !important;
		}

		.tf-back-btn {
			background-color: #575757;
			padding: 10px 10px 10px 10px;
			border-radius: 100px;
			border: none !important;
			color: #fff;
		}

		.open > .dropdown-menu {
			display: block;
			width: 300px !important
		}

			.open > .dropdown-menu ::-webkit-scrollbar {
				width: 10px;
			}

		.dropdown-menu {
			box-shadow: none !important;
		}

		.multiselect-container.dropdown-menu {
			overflow: hidden;
		}

		.tf-back-btn i {
			color: #fff !important;
		}

		.dropdown-menu {
			border-radius: 0px;
		}

		.total {
			font-weight: bold;
			font-size: 20px;
			color: #188ae2;
		}

		button#btnView {
			padding: 4px 24px;
			margin-top: 15px;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center {
			width: 200px;
		}

		button.multiselect.dropdown-toggle.custom-select.text-center {
			background-color: #fff;
			border: 1px solid #aaa;
			border-radius: 5px;
			padding-left: 8px;
			padding-right: 8px;
			padding: 3px 37px;
		}

		.open > .dropdown-menu {
			padding: 15px;
			max-height: 250px !important;
			border: 1px solid #aaa;
			border-bottom-left-radius: 5px;
			border-bottom-right-radius: 5px;
		}

		multiselect-filter {
			margin-bottom: 15px;
		}

		.multiselect-container.dropdown-menu {
			overflow: overlay !important;
		}

		.multiselect-container .multiselect-all .form-check, .multiselect-container .multiselect-group .form-check, .multiselect-container .multiselect-option .form-check {
			padding: 0px;
		}

		label.form-check-label.font-weight-bold {
			margin: 0px;
		}

		span label {
			font-size: 14px !important;
			margin: 0px;
		}

		@media only screen and (max-width: 1280px) and (min-width: 800px) {
			section.app-content .col-lg-4.col-md-6.col-sm-12 {
				width: 40%;
			}

			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 25px !important;
			}
		}

		@media only screen and (max-width: 1440px) and (min-width: 900px) {

			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 0px !important;
				width: 170px;
			}
		}

		@media only screen and (max-width: 1024px) and (min-width: 993px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 15px !important;
				width: 130px;
			}
		}

		@media only screen and (max-width: 992px) and (min-width: 768px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 50px;
				width: 225px;
			}
		}

		@media only screen and (max-width: 1599px) and (min-width: 1201px) {
			button.multiselect.dropdown-toggle.custom-select.text-center {
				padding: 3px 37px;
			}
		}

		.multiselect-container.dropdown-menu {
			overflow: hidden auto !important;
		}

		.navbar-toolbar > li > .dropdown-menu {
			width: 190px !important;
		}

		.open > .dropdown-menu {
			max-height: 300px !important;
		}

		.dash_User {
			width: 190px !important;
			border-radius: 10px;
			padding: 0 !important;
			border: 1px solid rgba(0,0,0,.15);
		}

		/*        .content-header .AD_btn {
            font-size: 15px;
        }*/

		a#lnkReject {
			font-weight: 700 !important;
		}

		.navbar-toolbar > li > .dropdown-menu {
			box-shadow: 0 3px 12px rgb(0 0 0 / 18%) !important;
		}

		@media only screen and (max-width: 992px) and (min-width: 768px) {
			h1.m-0.text-dark {
				font-size: 17px !important;
			}
		}
	</style>
</head>

<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->
		<main id="app-main" class="app-main">
			<input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
			<div class="wrap">
				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
								<h1 class="m-0 text-dark">Lock Attendance For Payroll</h1>
							</div>

							<!-- /.col -->
							<div class="col-md-4 col-sm-6">
								<div style="text-align: right;">
									<a class="AD_btn" id="btnPendingRequest" runat="server">View Pending Requests</a>
									<button class="AD_btn" id="btnLockForward" runat="server" onserverclick="btnApprove_ServerClick" validationgroup="btnValidate" type="button">Lock & Forward to Finance</button>
								</div>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.container-fluid -->
				</div>

				<section class="app-content">

					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Payroll Month
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtPayrollMonth" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
										<asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollMonth" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
									</div>
								</div>
								<div class="col-sm-6">
									<div class="clear-fix">&nbsp;</div>
									<div>
										<button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12 col-lg-offset-4">
							<div class="row">
								<div class="col-md-12">
									<asp:UpdatePanel ID="UpdatePanel4" runat="server">
										<ContentTemplate>
											<div class="form-group" id="divAlertMsg" runat="server">
												<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
													<span>
														<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
													</span>
													<p id="pAlertMsg" runat="server">
													</p>
												</div>
											</div>
										</ContentTemplate>
									</asp:UpdatePanel>
								</div>
							</div>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-12">
							<div class="tab-content ">
								<div class="tab-pane active">
									<asp:GridView ID="gv" runat="server" CssClass="table table-bordered table-resposive gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found">
										<Columns>

											<asp:TemplateField HeaderText="Sr.No.">
												<ItemTemplate>
													<%#Container.DataItemIndex+1 %>
												</ItemTemplate>
											</asp:TemplateField>

											
											<asp:TemplateField HeaderText="WDID">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblGeneralOvertime" Text='<%# Eval("MWDID") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Manager">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblHolidayOvertime" Text='<%# Eval("Manager") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Attendance Adjustments">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblAbsentDays" Text='<%# Eval("AttAdjustmentCount") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Break Adjustments">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblAbsentDays" Text='<%# Eval("BreakAdjustment") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Add Ons">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblMonthDays" Text='<%# Eval("AddOnsCount") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Other Expenses">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblWorkingDays" Text='<%# Eval("OtherExpenseCount") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
												<asp:TemplateField HeaderText="Total">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblWorkingDays" Text='<%# Eval("Totalrequests") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
										
										






















										<%--	<asp:TemplateField HeaderText="Manager ID Code">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblWDID" Text='<%# Eval("WDID") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Employee">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblName" Text='<%# Eval("EmployeeName") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>--%>

<%--											<asp:TemplateField HeaderText="Month">
												<ItemTemplate>
												<	<asp:Label runat="server" ID="lblPayrollMonth" Text='<%# Convert.ToDateTime( Eval("AttendanceMonth")).ToString("MMM-yyyy") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>--%>

<%--											<asp:TemplateField HeaderText="General Over Time">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblGeneralOvertime" Text='<%# Eval("OvertimeGeneral") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Holiday Over Time">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblHolidayOvertime" Text='<%# Eval("OvertimeHoliday") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Absent Days">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblAbsentDays" Text='<%# Eval("AbsentDays") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Month Days">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblMonthDays" Text='<%# Eval("TotalDays") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="Working Days">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblWorkingDays" Text='<%# Eval("[WorkingDays]") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField Visible="false">
												<ItemTemplate>
													<asp:Label runat="server" ID="lblEmployeeID" Text='<%#  Eval("EmployeeID") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
										--%>
										</Columns>
									</asp:GridView>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- #dash-content -->
			</div>
			<div class="clear-fix">&nbsp;</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>
		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>
		<script src="/business/scripts/ViewDesignations.js"></script>
	</form>
</body>
</html>
