﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Employees.aspx.cs" Inherits="Technofinancials.People_Management.view.Employees" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Administrator/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <img src="/assets/images/Designations.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Designations</h3>
                        </div>

                        <div class="col-sm-4" style="margin-top: 10px;">
                            <div class="pull-right">
                                <%--<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/directors/manage/designations/add-new-designation"); %>" class="tf-add-btn" "Add"><i class="far fa-plus-circle"></i></a>--%>
                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row ">
                   <div class="col-sm-12">
                            <div class="tab-content ">
                            <div class="tab-pane active row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                    <asp:GridView ID="gv" runat="server" CssClass="gv table table-bordered" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>

                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("EmployeeFirstName")+" "+Eval("EmployeeMiddleName")+" "+Eval("EmployeeLastName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                             <asp:TemplateField HeaderText="CNIC">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("CNIC") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Number">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("ContactNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Email") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Separation Type">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol5" Text='<%# Eval("TypeOfSeperation") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                               <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <%--<a href= "<%#  "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ","-") + "/directors/manage/designations/edit-designation-" + Eval("EmployeeID") %>" class="tf-note-btn" ><i class="fa fa-pencil" aria-hidden="true"></i></a>--%>
                                                     <asp:LinkButton ID="lnkDelete" runat="server" CssClass="delete-class"CommandArgument='<%# Eval("EmployeeID") %>' OnClick="lnkDelete_Click"> Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                   </div>
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

        <style>
                        @media only screen and (max-width: 992px) and (min-width: 768px) {
                div#gv_wrapper {
                    overflow-x: scroll;
                    padding-bottom: 35px;
                }
                    /* Track */
                   div#gv_wrapper ::-webkit-scrollbar-track {
                        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                        background-color: #FFF;
                    }

                    div#gv_wrapper ::-webkit-scrollbar {
                        height: 10px;
                        background-color: #ffffff;
                    }

                  div#gv_wrapper ::-webkit-scrollbar-thumb {
                        background-color: #003780;
                        border: 2px solid #003780;
                        border-radius: 10px;
                    }

            }            
                                        .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }
            
            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

        </style>
    </form>
</body>
</html>