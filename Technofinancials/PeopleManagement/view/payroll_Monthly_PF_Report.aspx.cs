﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
    public partial class payroll_Monthly_PF_Report : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                string errormsg = "";
                ddlFiscalYear.DataSource = objDB.GetFiscalYear(ref errormsg);
                ddlFiscalYear.DataTextField = "FYName";
                ddlFiscalYear.DataValueField = "FYName";
                ddlFiscalYear.DataBind();   
               
            }
        }


        private void GetData(string FiscalYear)
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            dt = objDB.Get_Monthly_PF_report(FiscalYear,ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            Common.addlog("ViewAll", "HR", "All NewPayroll Viewed", "NewPayroll");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            GetData(ddlFiscalYear.SelectedValue);
        }
    }
}