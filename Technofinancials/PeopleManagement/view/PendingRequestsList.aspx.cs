﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.view
{
	public partial class PendingRequestsList : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;
                if (HttpContext.Current.Items["PendingDate"] != null)
                {
                    txtPendingDate.Text = HttpContext.Current.Items["PendingDate"].ToString();
                    GetData();
                }
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void GetData()
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.PayrollDate = Convert.ToDateTime($"01-{txtPendingDate.Text}").ToString("MM-dd-yyyy");
            
            Session["PendingRequestDate"] = txtPendingDate.Text;
            dt = objDB.GetPendingRequestsList(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.DataSource = dt;
                    gv.DataBind();
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }

            Common.addlog("ViewAll", "HR", "View All Managers' Pending Requests", "MultipleTables");
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            GetData();
        }
	}
}