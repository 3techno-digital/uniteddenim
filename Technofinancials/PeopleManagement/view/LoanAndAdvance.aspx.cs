﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.View
{
    public partial class LoanAndAdvance : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected string PersonName
        {
            get
            {
                if (ViewState["PersonName"].ToString() != "")
                {
                    return ViewState["PersonName"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["PersonName"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["PersonName"] = "";
                CheckSessions();
                GetData();
            }
        }
       
        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
			try
			{
                if (e.CommandName == "DownloadPFApplication")
                {
                    string content = "";
                    string header = "";
                    string footer = "";
                    int LoanID = Convert.ToInt32(e.CommandArgument);
                    DataTable design = new DataTable();
                    DataSet ds = new DataSet();
                    objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                    objDB.DocType = "PF Withdrawal";

                    design = objDB.GetDocumentDesign(ref errorMsg);

                    if (design != null)
                    {
                        if (design.Rows.Count > 0)
                        {

                            content = design.Rows[0]["DocContent"].ToString();

                        }
                    }
                    DataTable dt = new DataTable();
                    objDB.LoanID = LoanID;
                    ds = objDB.GetLoanAndAdvanceDetailsByLoanID(ref errorMsg);
                    dt = ds.Tables[0];

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        content = content.Replace("##Grant_Date##", Convert.ToDateTime(dt.Rows[0]["RequestDate"].ToString()).ToString("dd/MM/yyyy"));
                        content = content.Replace("##Loan_Amount##", dt.Rows[0]["Amount"].ToString());
                        content = content.Replace("##No_Of_Installment##", dt.Rows[0]["NoOfInstallment"].ToString());
                        content = content.Replace("##is_Perm_Applied##", dt.Rows[0]["iSPermApplied"].ToString());
                        content = content.Replace("##is_Temp_Applied##", dt.Rows[0]["iSTempApplied"].ToString());
                        content = content.Replace("##EmployeeName##", dt.Rows[0]["Employeename"].ToString());
                        PersonName = dt.Rows[0]["wdid"].ToString();
                        content = content.Replace("##WDID##", dt.Rows[0]["wdid"].ToString());
                        content = content.Replace("##CNIC##", dt.Rows[0]["CNIC"].ToString());
                        content = content.Replace("##Designation##", dt.Rows[0]["DesgTitle"].ToString());
                        content = content.Replace("##DOB##", Convert.ToDateTime(dt.Rows[0]["DOB"].ToString()).ToString("dd/MM/yyyy"));
                        content = content.Replace("##Location##", dt.Rows[0]["Placeofpost"].ToString());


                        content = content.Replace("##HRNAME##", dt.Rows[0]["HRDept"].ToString());
                        content = content.Replace("##HRApprovaldate##", dt.Rows[0]["HRClearedon"].ToString() =="" ? "" :Convert.ToDateTime(dt.Rows[0]["HRClearedon"].ToString()).ToString("dd/MM/yyyy"));

                        content = content.Replace("##FinanceName##", dt.Rows[0]["FinanceDept"].ToString());
                        content = content.Replace("##FinanceApprovaldate##", dt.Rows[0]["FinanceClearedon"].ToString() == "" ? "" : Convert.ToDateTime(dt.Rows[0]["FinanceClearedon"].ToString()).ToString("dd/MM/yyyy"));
           
                        
                        content = content.Replace("##Zakatyes##", dt.Rows[0]["isZakatDeduct"].ToString()=="True" ? "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />" : "");
                        content = content.Replace("##Zakatno##", dt.Rows[0]["isZakatDeduct"].ToString()=="False" ? "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />" : "");


                        for (int i = 1; i <= Convert.ToInt32(ds.Tables[1].Rows[0][0].ToString()); i++)
                        {
                            if (i.ToString() == dt.Rows[0]["mainoptionid"].ToString())
                            {
                                content = content.Replace("##Option" + i + "##", "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />");
                            }
                            if (i.ToString() == dt.Rows[0]["subpurposeid"].ToString())
                            {

                                content = content.Replace("##SubOption" + i + "##", "<img src=https://www.kt.technofinancials.com/assets/images/tick.png />");
                            }
                            else
                            {
                                content = content.Replace("##Option" + i + "##", "");
                                content = content.Replace("##SubOption" + i + "##", "");
                            }

                        }


                    }


                    Common.generatePDF(header, footer, content, "PF_Withdrwawal-" + PersonName + "", "A4", "Portrait", 30, 30);


                }

            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }
        }
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
              
            }
        }
        private void GetData()
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dt = objDB.GetAllLoanAndAdvance(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
			if (dt != null)
			{
				if (dt.Rows.Count > 0)
				{
					gv.UseAccessibleHeader = true;
					gv.HeaderRow.TableSection = TableRowSection.TableHeader;
				}
			}
			Common.addlog("ViewAll", "Finance", "Loans Viewed", "");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

       
    }
}