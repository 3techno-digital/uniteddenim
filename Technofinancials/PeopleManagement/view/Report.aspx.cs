﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Text;
//using IronPdf;
//using iTextSharp.text.pdf;
//using iTextSharp.text;
//using iTextSharp.text.html.simpleparser;
using SelectPdf;

namespace Technofinancials.PeopleManagement.view
{
    public partial class Report : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        public static int candidateID = 0;
        public static string refNo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            { 
                divAlertMsg.Visible = false;
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/dashboard";
               
                if (Session["ReportTitle"] != null && Session["ReportDataTable"] != null)
                {
                    getReport(Session["ReportTitle"].ToString(), Session["ReportDataTable"].ToString());

                    if (Session["ReportTitle"].ToString() == "New Hiring")
                    {
                        reportsPageHeading.InnerHtml = "New Hiring Report";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/new-hiring-report";
                    }
                    else if (Session["ReportTitle"].ToString() == "New Vacancies")
                    {
                        reportsPageHeading.InnerHtml = "New Vacancies Report";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/new-vacancies-report";
                    }
                    else if (Session["ReportTitle"].ToString() == "Employees Blacklist")
                    {
                        reportsPageHeading.InnerHtml = "Employees Blacklist Report";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employee-black-list-report";
                    }
                    else if (Session["ReportTitle"].ToString() == "Payroll Report")
                    {
                        reportsPageHeading.InnerHtml = "Payroll Report";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/payroll-report";
                    }
                    else if (Session["ReportTitle"].ToString() == "Attendance Report")
                    {
                        reportsPageHeading.InnerHtml = "Attendance Report";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/attendance-report";
                    }
                    else if (Session["ReportTitle"].ToString() == "Surveys Report")
                    {
                        reportsPageHeading.InnerHtml = "Surveys Report";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/surveys-report";
                    }
                    else if (Session["ReportTitle"].ToString() == "Trainings Report")
                    {
                        reportsPageHeading.InnerHtml = "Trainings Report";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/trainings-report";
                    }
                    else if (Session["ReportTitle"].ToString() == "Daily Attendance Sheet")
                    {
                        reportsPageHeading.InnerHtml = "Daily Attendance Sheet";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/daily-attendance-sheet";
                    }
                    else if (Session["ReportTitle"].ToString() == "Employee Attendance Monthly")
                    {
                        reportsPageHeading.InnerHtml = "Employee Attendance Monthly";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/employee-attendance-monthly";
                    }
                    else if (Session["ReportTitle"].ToString() == "Late Comers Sheet")
                    {
                        reportsPageHeading.InnerHtml = "Late Comers Sheet";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/late-comers-sheet";
                    }

                    else if (Session["ReportTitle"].ToString() == "Leave Consumption Report")
                    {
                        reportsPageHeading.InnerHtml = "Leave Consumption Report";
                        btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/leave-consumed-report";
                    }
                }
            }
        }

        private void getReport(string ReportTitle, string ReportDT)
        {
            DataTable dt = new DataTable();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.DocType = "HR Reports";
            dt = objDB.GetDocumentDesign(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtHeader.Content = dt.Rows[0]["DocHeader"].ToString();
                    string content = dt.Rows[0]["DocContent"].ToString();
                    
                    content = content.Replace("##Title##", ReportTitle);
                    content = content.Replace("##TABLE##", ReportDT);
                    txtContent.Content = content;
                    txtFooter.Content = dt.Rows[0]["DocFooter"].ToString();
                    refNo = ReportTitle.Replace(" ","-");
                }
            }
        }

  
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Common.generatePDF(txtHeader.Content, txtFooter.Content, txtContent.Content, refNo, "A4", "Portrait");
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

    }
}