﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneratePayroll.aspx.cs" Inherits="Technofinancials.PeopleManagement.Reports.GeneratePayroll" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
		<uc:Header ID="header1" runat="server"></uc:Header>
		<uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
		<!-- APP MAIN ==========-->

		<main id="app-main" class="app-main">
			<asp:UpdateProgress ID="updProgress"
				AssociatedUpdatePanelID="btnUpdPnl"
				runat="server">
				<ProgressTemplate>
					<div class="upd_panel">
						<div class="center">
							<img src="/assets/images/Loading.gif" />
						</div>


					</div>
				</ProgressTemplate>
			</asp:UpdateProgress>

			<div class="wrap">

				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
								<h1 class="m-0 text-dark">Generate Payslip</h1>
							</div>
							<!-- /.col -->
							<div class="col-sm-4">
								<div style="text-align: right;">
									<button class="AD_btn" visible="false" id="btnReport" validationgroup="btnView" type="button" runat="server" onserverclick="btnPDF_ServerClick" >PDF</button>
									<button class="AD_btn" visible="false" id="PrintBtn" validationgroup="btnView" type="button" onclick="Print()" style="display:none;" >PDF</button>
								</div>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.container-fluid -->
				</div>


				<section class="app-content">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Employee Name</h4>
										<asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2" data-plugin="select2">
										</asp:DropDownList>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<h4>Month
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtPayrollDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>

										<%-- <input name="txtPayrollDate" type="text" id="txtPayrollDate" class="form-control" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />--%>
										<asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" />
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-sm-6">
									<%--<button class="AD_btn" "" type="button" runat="server" onclick="ShowReport();" data-original-"">View</button>--%>
									<button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnPDF_ServerClick" type="button">PDF</button>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
							<asp:UpdatePanel ID="UpdatePanel4" runat="server">
								<ContentTemplate>
									<div class="form-group" id="divAlertMsg" runat="server">
										<div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
											<span>
												<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
											</span>
											<p id="pAlertMsg" runat="server">
											</p>
										</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>
					</div>
					<div class="row">



						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="row">

								<asp:UpdatePanel ID="btnUpdPnl" runat="server">
									<ContentTemplate>
										<!-- Modal -->

										<!-- Modal -->
										<div class="modal fade M_set" id="notes-modal" role="dialog">
											<div class="modal-dialog">
												<!-- Modal content-->
												<div class="modal-content">

													<div class="modal-header">
														<h1 class="m-0 text-dark">Notes</h1>
														<div class="add_new">
															<button type="button" class="AD_btn" data-dismiss="modal">Save</button>
															<button data-dismiss="modal" aria-hidden="true" class="AD_btn">Close</button>
														</div>
													</div>

													<div class="modal-body">
														<p>
															<asp:Literal ID="ltrNotesTable" runat="server"></asp:Literal>
														</p>
														<p>
															<textarea id="Textarea1" runat="server" rows="5" placeholder="Notes.." class="form-control"></textarea>
														</p>
													</div>

												</div>
											</div>
										</div>

										<!-- Modal -->

									</ContentTemplate>
								</asp:UpdatePanel>
							</div>
						</div>

						<div class="col-md-6 col-lg-4 col-sm-12">
						</div>

					</div>
					<div class="clearfix">&nbsp;</div>
					<%-- Print --%>
					<div style="display: none">

						<div id="imgReport">

							<div style="text-align: center">
								<img src="../assets/images/a4print.png" style="max-width: 100%" />
							</div>

						</div>


					</div>
					<div class="container-fuild">
						<div>
							<div style="display: none;" class="B_class" id="ReportPayrolls">
								<img src="https://www.test.technofinancials.com/assets/files/companies_logos/admin-payroll_81024.png" alt="Company Logo" style="width: 200px; position: absolute;" />
								<div class="AddressSide">
									<h1>ABS - LABS (PVT.) LIMITED</h1>
									<h3>Payslip</h3>
									<h4>For the Month Of
										<asp:Label ID="Label1" runat="server"></asp:Label>
									</h4>
								</div>
								<div style="width: 100%; display: block;">
									<div class="row">
										<div class="Box01 col-md-6 col-lg-6 col-sm-6">
											<div class="OneLine">
												<h3>Employee Code :</h3>
												<asp:Label ID="Label2" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Location :</h3>
												<asp:Label ID="Label3" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Designation :</h3>
												<asp:Label ID="Label4" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Date of Joining :</h3>
												<asp:Label ID="Label5" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Date of Birth :</h3>
												<asp:Label ID="Label6" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Basic Salary :</h3>
												<asp:Label ID="Label7" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Account :</h3>
												<asp:Label ID="Label8" runat="server"></asp:Label>
											</div>
										</div>
										<div class="Box02 col-md-6 col-lg-6 col-sm-6">
											<div class="OneLine">
												<h3>Name :</h3>

												<asp:Label ID="Label9" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Department :</h3>
												<asp:Label ID="Label10" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Employee Type :</h3>
												<asp:Label ID="Label11" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Date Of Confirmation :</h3>
												<asp:Label ID="Label12" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>CNIC :</h3>
												<asp:Label ID="Label13" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Bank :</h3>
												<asp:Label ID="Label14" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>&nbsp;</h3>
												<p class="">&nbsp;</p>
											</div>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="row">
										<div class="Box03 col-md-6 col-lg-6 col-sm-6">
											<div class="table-1">
												<table>
													<tr>
														<th>Allowance</th>
														<th>Amount</th>
														<th>YTD</th>
													</tr>
													<tr>
														<td>Basic Salary</td>
														<td>
															<asp:Label ID="Label15" runat="server"></asp:Label>
														</td>
														<td>
															<asp:Label ID="Label16" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Sales Commission</td>
														<td>
															<asp:Label ID="Label17" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label18" runat="server"></asp:Label></td>

													</tr>
													<tr>
														<td>House Rent</td>
														<td>
															<asp:Label ID="Label19" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label20" runat="server"></asp:Label></td>

													</tr>

													<tr>
														<td>Medical Allowance</td>
														<td>
															<asp:Label ID="Label21" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label22" runat="server"></asp:Label></td>

													</tr>
													<tr>
														<td>Utility Allowance</td>
														<td>
															<asp:Label ID="Label23" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label24" runat="server"></asp:Label></td>

													</tr>
													<tr>
														<td>Expense Reimbursement</td>
														<td>
															<asp:Label ID="Label25" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label26" runat="server"></asp:Label></td>
													</tr>

													<tr>
														<td>Eid Bouns</td>
														<td>
															<asp:Label ID="Label27" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label28" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Spiffs</td>
														<td>
															<asp:Label ID="Label29" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label30" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Overtime Hours - General</td>
														<td>
															<asp:Label ID="Label31" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label32" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Overtime Holidays</td>
														<td>
															<asp:Label ID="Label33" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label34" runat="server"></asp:Label></td>
													</tr>
												</table>
											</div>
										</div>
										<div class="Box04 col-md-6 col-lg-6 col-sm-6">
											<div class="table-1">
												<table>
													<tr>
														<th>Deduction</th>
														<th>Amount</th>
														<th>YTD</th>
													</tr>
													<tr>
														<td>Income Tax</td>
														<td>
															<asp:Label ID="Label35" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label36" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Addition Income Tax</td>
														<td>
															<asp:Label ID="Label37" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label38" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>EOBI Deduction</td>
														<td>
															<asp:Label ID="Label39" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="Label40" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
												</table>

											</div>
										</div>
									</div>
									<div class="row">
										<div class="Box05 col-md-6 col-lg-6 col-sm-6">
											<div class="table-1">
												<table>
													<tbody>
														<tr>
															<td style="width: 66%;">Total</td>
															<td>
																<asp:Label ID="Label41" runat="server"></asp:Label></td>
															<%--             <td><asp:Label ID="Total2" runat="server"></asp:Label></td>--%>
															<td>
																<label>0</label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="Box06 col-md-6 col-lg-6 col-sm-6" style="margin-top: 23.5px;">
											<div class="table-1">
												<table>
													<tbody>
														<tr>
															<td style="width: 66%;">Total</td>
															<td>
																<asp:Label ID="Label42" runat="server"></asp:Label></td>
															<%--             <td><asp:Label ID="Total4" runat="server"></asp:Label></td>--%>
															<td>
																<label>0</label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="Box09 col-md-6 col-lg-6 col-sm-6 main">
											<div class="table-1">
												<table>
													<tbody>
														<tr>
															<td style="font-weight: bold;">Net Amount:</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="Box06 col-md-6 col-lg-6 col-sm-6 extra">
											<div class="table-1">
												<table>
													<tbody>
														<tr>
															<td></td>
															<td></td>
															<td></td>
															<td style="font-weight: bold; text-align: right;">
																<asp:Label ID="Label43" runat="server"></asp:Label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="Box07 col-md-12 col-lg-12 col-sm-12">
											<div class="table-1">
												<table>
													<tbody>
														<tr>
															<td style="font-weight: bold;">Amount of Words</td>
															<td>
																<asp:Label ID="Label44" runat="server"></asp:Label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="row">
										<div class="Box08 col-md-12 col-lg-12 col-sm-12">
											<div class="table-1">
												<table style="background-color: #c0c0c0;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Income Tax Details:</td>
														</tr>
													</tbody>
												</table>
												<table style="margin-top: 0px; border-top: none;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Annual Taxable Income</td>
															<td></td>
															<td>0</td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Total Income Tax Liability</td>
															<td></td>
															<td>0</td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Income Tax Paid</td>
															<td></td>
															<td>0</td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Advance Tax Adjustment</td>
															<td></td>
															<td>--</td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Tax Credit </td>
															<td></td>
															<td>--</td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Remaining Income Tax Payable</td>
															<td></td>
															<td>0</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div>
										<p style="text-align: center;">Techno Financials - This is auto generated pay slip and needs no signature.</p>
									</div>
								</div>
							</div>
							<style type="text/css">
								.Box03.col-md-6.col-lg-6.col-sm-6 {
									border-bottom
								}

								.table-1.B_class table {
									border: 0px !important;
									margin: 0px !important;
								}

								.table-1.B_class {
									border: 0px !important;
									padding: 0px !important;
									border-bottom: 0px !important;
								}

									.table-1.B_class td:nth-child(4) {
										text-align: right;
									}

									.table-1.B_class td:nth-child(2) {
										text-align: left;
									}

								@media only screen and (max-width: 768px) {
									/*        .Box04.col-md-6.col-lg-6.col-sm-6 tr:nth-child(7) {
            height: 8.9rem !important;
        }*/
									.Box04.col-md-6 tr:nth-child(7) {
										height: 30px !important;
									}

									div#ReportPayroll img {
										width: 149px !important;
									}

									.OneLine h3 {
										font-size: 12px !important;
										font-weight: 600 !important;
									}

									.Box01 {
										padding-right: 0px !important;
									}

									.Box02 {
										padding-right: 0px !important;
										padding-left: 10px !important;
									}

									.OneLine span {
										font-size: 12px;
										line-height: 1.2;
									}

									.Box03 tbody {
										display: inherit !important;
										max-width: 150px !important;
										overflow: hidden !important;
									}

									.Box03 table {
										display: inherit !important;
										overflow: hidden !important;
									}
								}

								@media only screen and (max-width: 800px) {
									/*        .Box04.col-md-6.col-lg-6.col-sm-6 tr:nth-child(7) {
            height: 10rem !important;
        }*/
									div#ReportPayroll img {
										width: 149px !important;
									}

									.OneLine h3 {
										font-size: 12px !important;
										font-weight: 600 !important;
									}

									.Box01 {
										padding-right: 0px !important;
									}

									.Box02 {
										padding-right: 0px !important;
										padding-left: 10px !important;
									}

									.OneLine span {
										font-size: 12px;
										line-height: 1.2;
									}

									.Box03 tbody {
										display: inherit !important;
										max-width: 150px !important;
										overflow: hidden !important;
									}

									.Box03 table {
										display: inherit !important;
										overflow: hidden !important;
									}
								}

								@media only screen and (max-width: 875px) {
									/*        .Box04.col-md-6.col-lg-6.col-sm-6 tr:nth-child(7) {
            height: 11.2rem !important;
        }*/
									div#ReportPayroll img {
										width: 149px !important;
									}

									.OneLine h3 {
										font-size: 12px !important;
										font-weight: 600 !important;
									}

									.Box01 {
										padding-right: 0px !important;
									}

									.Box02 {
										padding-right: 0px !important;
										padding-left: 10px !important;
									}

									.OneLine span {
										font-size: 12px;
										line-height: 1.2;
									}

									.Box03 tbody, .Box04 tbody {
										display: inherit !important;
										max-width: 150px !important;
										overflow: hidden !important;
									}

									.Box03 table, .Box04 table {
										display: inherit !important;
										overflow: hidden !important;
									}
								}

								@media only screen and (max-width: 990px) and (min-width: 1000px) {
									.Box04.col-md-6.col-lg-6.col-sm-6 tr:nth-child(7) {
										height: 11.2rem !important;
									}

									div#ReportPayroll img {
										width: 149px !important;
									}

									.OneLine h3 {
										font-size: 12px !important;
										font-weight: 600 !important;
									}

									.Box01 {
										padding-right: 0px !important;
									}

									.Box02 {
										padding-right: 0px !important;
										padding-left: 10px !important;
									}

									.OneLine span {
										font-size: 12px;
										line-height: 1.2;
									}

									.Box03 tbody, .Box04 tbody {
										display: inherit !important;
										max-width: 150px !important;
										overflow: hidden !important;
									}

									.Box03 table {
										display: inherit !important;
										overflow: hidden !important;
									}
								}


								@media only screen and (max-width: 992px) {
									.Box04.col-md-6 tr:nth-child(7) {
										height: 106px !important;
									}

									.Box06 {
										margin-top: 23px !important;
									}

									.Box06 {
										margin-left: -0.2%;
									}
								}

								@media only screen and (max-width: 1020px) and (min-width: 1000px) {
									.Box04.col-md-6 tr:nth-child(7) {
										height: 35px !important;
									}

									.Box06 {
										margin-top: 23px !important;
									}

									.Box06 {
										margin-left: -0.2%;
									}
								}

								@media only screen and (max-width: 1200px) and (min-width: 1025px) {
									.Box04.col-md-6 tr:nth-child(6) {
										height: 28px !important;
									}

									.Box06 {
										margin-top: 23px !important;
									}
								}

								@media only screen and (max-width: 1599px) and (min-width: 1201px) {
									.Box04.col-md-6 tr:nth-child(6) {
										height: 28px !important;
									}

									.Box06 {
										margin-top: 24px !important;
									}
								}

								@media only screen and (max-width: 1024px) {
									.Box04.col-md-6 tr:nth-child(7) {
										height: 30px;
									}
								}
							</style>
							<style type="text/css">
								.B_class {
									padding: 20px;
									/*width: 800px;
         margin: 0 auto;
         display: block;*/
									border: 2px solid #000;
									background: #fff;
								}

									.B_class h1,
									.B_class h2,
									.B_class h3,
									.B_class h4,
									.B_class p,
									.B_class td {
										margin: 0;
										font-family: lato;
										color: #000;
									}

									.B_class table {
										border: 2px solid;
										margin-top: -25px;
										width: 100%;
										text-align: left;
									}

										.B_class table td,
										.B_class table th {
											padding: 6.4px;
										}

									.B_class .slip-table &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									thead &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									tr &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									th {
										background-color: #ffffff;
										border-bottom: 2px solid #000000 !important;
										border-top: 2px solid #000000 !important;
										color: #000;
										padding: 0 10px;
									}

								.slip-table-bordered &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								thead &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								th,
								.slip-table-bordered &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								thead &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								td,
								.slip-table-bordered &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tbody &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								th,
								.slip-table-bordered &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tbody &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								td {
									border-top: 2px solid transparent !important;
									border: 0;
									vertical-align: middle;
									padding: .1rem;
									font-size: 14px;
								}

								.slip-table &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								thead &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								th,
								.slip-table &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								thead &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								td,
								.slip-table &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tbody &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								th,
								.slip-table &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tbody &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								td,
								.slip-table &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tfoot &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								th,
								.slip-table &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tfoot &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								td {
									height: 32px;
									line-height: 1.428571429;
									vertical-align: middle;
									border-top: 2px solid #ddd;
									padding: 0 10px;
								}

								.slip-table {
									width: 100%;
									max-width: 100%;
									background-color: transparent;
									border-collapse: collapse;
									border-spacing: 0;
								}

								.slip-table-bordered &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tfoot &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								th,
								.slip-table-bordered &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tfoot &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								td {
									border-top: 2px solid #000 !important;
									border: 0;
									vertical-align: middle;
									padding: .1rem;
									font-size: 14px;
									padding: 0 10px;
								}

								.B_class .slip-table &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								thead &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								th {
									text-align: right;
								}

									.B_class .slip-table &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									thead &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									tr &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									th:nth-child(1) {
										text-align: left;
									}

								.B_class .slip-table &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tbody &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								td {
									text-align: right;
									height: 28px;
								}

									.B_class .slip-table &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									tbody &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									tr &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									td:nth-child(1) {
										text-align: left;
									}

								.B_class .slip-table &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tfoot &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								tr &amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								amp;
								gt;
								td {
									text-align: right;
									height: 34px;
								}

									.B_class .slip-table &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									tfoot &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									tr &amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									amp;
									gt;
									td:nth-child(1) {
										text-align: left;
										width: 200px !important;
									}
								/*.B_class .slip-table &amp;amp;amp;amp;amp;amp;amp;gt; tfoot &amp;amp;amp;amp;amp;amp;amp;gt; tr &amp;amp;amp;amp;amp;amp;amp;gt; td:nth-child(1) {
                                    text-align: left;
                                        width: 150px !important;
                                }*/

								.B_class hr {
									margin-top: 20px;
									margin-bottom: 10px;
									border: 0;
									border-top: 2px solid #000000;
								}
								/*.B_class tbody tr td {
                padding: 15px 10px !important;
            }*/

								.tableP {
									height: 34px;
									padding: 6.4px;
									border: 2px solid;
									margin-top: -6px !important;
									width: 99.9%;
								}

									.tableP span {
										margin-left: 40px;
										text-transform: capitalize;
									}

								.AddressSide {
									height: 90px;
									color: #000;
									text-align: center;
								}

									.AddressSide h1 {
										font-size: 18px;
										text-transform: capitalize;
										font-weight: 700;
										margin: 5px 0;
										color: #000;
									}

									.AddressSide h2 {
										font-size: 18px;
										text-align: right;
										font-weight: 400;
										color: #000;
										margin: 5px 0;
									}

									.AddressSide h3 {
										font-size: 18px;
										text-transform: capitalize;
										font-weight: 700;
										margin: 5px 0;
										color: #000;
									}

									.AddressSide h4 {
										font-size: 18px;
										text-transform: capitalize;
										font-weight: 700;
										margin-bottom: 15px;
										color: #000;
										margin: 5px 0;
									}

									.AddressSide p {
										font-size: 16px;
										font-weight: 500;
										max-width: 350px;
										text-transform: capitalize;
										color: #000;
									}

								.bigBox {
									width: 100%;
									border: 2px solid #000;
									height: 400px;
									margin: 50px 0 20px;
								}

								.Box01 {
									border: 2px solid #000;
									height: 220px;
									display: inline-block;
									padding: 10px;
									/* position: relative; */
									color: #000;
									border-right: 0;
								}

								.Box02 {
									border: 2px solid #000;
									height: 220px;
									display: inline-block;
									padding: 10px 20px;
									/* margin-right: -10px; */
									margin-left: -5px;
									/* position: relative; */
									color: #000;
									/* top: -34px; */
									border-left: 0;
								}

								.Box03 {
									display: inline-block;
								}

								.Box04 {
									margin-left: -0.1%;
									display: inline-block;
									padding-right: 0.1rem;
									padding-left: 0px;
								}

								.Box05 {
									margin-top: 24px;
									padding-left: 0px;
									padding-right: 0px;
								}

								.Box06 {
									margin-top: 23px;
									margin-left: -0.1%;
									display: inline-block;
									padding-right: 0.1rem;
									padding-left: 0px;
								}

								.OneLine {
									width: 100%;
									height: 28px;
								}

									.OneLine h3 {
										display: inline-block;
										font-size: 16px;
										font-weight: 700;
										margin: 3px 0;
										color: #000;
									}

									.OneLine p {
										display: inline-block;
										font-size: 16px;
										width: 60%;
										margin: 3px 0;
										color: #000;
									}

								.totalSalaries {
									font-weight: bold !important;
									color: #188ae2 !important;
									font-size: 16px !important;
								}

								.Box08, .Box11 {
									padding-left: 0px;
									padding-right: 0.1rem;
								}

								.Box09 {
									padding-left: 0px;
									padding-right: 0px;
									margin-top: 23px;
								}

								.Box07 {
									margin-top: 23px;
									padding-left: 0px;
									padding-right: 0.1rem;
									width: 99.9%;
								}

								.Box04.col-md-6 tr:nth-child(6) {
									height: 28px;
								}

								.line-four {
									width: 99.9%;
									display: block;
								}

								.table-1 th:nth-child(2), .table-1 td:nth-child(2), .table-1 td:nth-child(3), .table-1 th:nth-child(3) {
									text-align: right;
								}

								.Box07 .table-1 td:nth-child(2) {
									text-align: left;
								}
							</style>



							<div class="B_class" id="ReportPayroll" runat="server">
					
							<asp:Image ID="hdnCompanyLogo"  runat="server" ImageUrl=""  Width="46px"/> 
				
								
								<div class="AddressSide">
								<h1 runat="server" id="hdnCompanyName"></h1>
									<h3>Payslip</h3>
									<h4>For the Month of 
										<asp:Label ID="Month" runat="server"></asp:Label>
									</h4>
								</div>
								<div style="width: 100%; display: block;">
									<div class="row">
										<div class="Box01 col-md-6 col-lg-6 col-sm-6">
											<div class="OneLine">
												<h3>Employee Code :</h3>
												<asp:Label ID="empCode" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Location :</h3>
												<asp:Label ID="empLocation" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Designation :</h3>
												<asp:Label ID="empDesg" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Date of Joining :</h3>
												<asp:Label ID="DateofJoin" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Date of Birth :</h3>
												<asp:Label ID="DOB" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Account :</h3>
												<asp:Label ID="empAccount" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Total Days :</h3>
												<asp:Label ID="Label45" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Gross Salary :</h3>
												<asp:Label ID="BasicSalary" runat="server"></asp:Label>
											</div>
										</div>
										<div class="Box02 col-md-6 col-lg-6 col-sm-6">
											<div class="OneLine">
												<h3>Name :</h3>

												<asp:Label ID="empname" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Department :</h3>
												<asp:Label ID="empDpt" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Employee Type :</h3>
												<asp:Label ID="empType" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Date Of Confirmation :</h3>
												<asp:Label ID="DateofConf" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>CNIC :</h3>
												<asp:Label ID="CNIC" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Bank :</h3>
												<asp:Label ID="BankName" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3>Present Days :</h3>
												<asp:Label ID="Label46" runat="server"></asp:Label>
											</div>
											<div class="OneLine">
												<h3></h3>
											</div>

											<div class="OneLine">
												<h3>&nbsp;</h3>
												<p class="">&nbsp;</p>
											</div>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="clearfix">&nbsp;</div>
									<div class="row">
										<div class="Box03 col-md-6 col-lg-6 col-sm-6">
											<div class="table-1">
												<table>
													<tr style="border-bottom: 2px solid #000;">
														<th>Allowance</th>
														<th>Amount</th>
														<th>YTD</th>
													</tr>
													<tr>
														<td>Basic Salary</td>
														<td>
															<asp:Label ID="basicSalry" runat="server"></asp:Label>
														</td>
														<td>
															<asp:Label ID="basicSalryYTD" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>House Rent</td>
														<td>
															<asp:Label ID="houseRent" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="houseRentYTD" runat="server"></asp:Label></td>

													</tr>

													<tr>
														<td>Medical Allowance</td>
														<td>
															<asp:Label ID="mdecialAllow" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="mdecialAllowYTD" runat="server"></asp:Label></td>

													</tr>
													<tr>
														<td>Utility Allowance</td>
														<td>
															<asp:Label ID="UtilityAllow" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="UtilityAllowYTD" runat="server"></asp:Label></td>

													</tr>
													<tr>
														<th>
															<b>Total</b>
														</th>
														<th>
															<asp:Label ID="TotalGross" CssClass="payslip-totalcol-bar" runat="server"></asp:Label>
														</th>
														<th>
															<asp:Label ID="TotalGrossYTD" CssClass="payslip-totalcol-bar" runat="server"></asp:Label>
														</th>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td>Sales Commission</td>
														<td>
															<asp:Label ID="SalesComission" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="SalesComissionYTD" runat="server"></asp:Label></td>

													</tr>
													<tr>
														<td>Other Allowance</td>
														<td>
															<asp:Label ID="OtherAllowance" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="OtherAllowanceYTD" runat="server"></asp:Label></td>

													</tr>
													<tr>
														<td>Expense Reimbursement</td>
														<td>
															<asp:Label ID="ExpenseReim" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="ExpenseReimYTD" runat="server"></asp:Label></td>
													</tr>

													<tr>
														<td>Eid Bouns</td>
														<td>
															<asp:Label ID="EidBonus" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="EidBonusYTD" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Spiffs</td>
														<td>
															<asp:Label ID="Spiffs" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="SpiffsYTD" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Overtime Hours - General</td>
														<td>
															<asp:Label ID="Overtime" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="OvertimeYTD" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Overtime Holidays</td>
														<td>
															<asp:Label ID="OvertimeHolidays" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="OvertimeHolidaysYTD" runat="server"></asp:Label></td>
													</tr>
												</table>
											</div>
										</div>
										<div class="Box04 col-md-6 col-lg-6 col-sm-6">
											<div class="table-1">
												<table>
													<tr style="border-bottom: 2px solid #000;">
														<th>Deduction</th>
														<th>Amount</th>
														<th>YTD</th>
													</tr>
													<tr>
														<td>Income Tax</td>
														<td>
															<asp:Label ID="IncomeTax" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="IncomeTaxYTD" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Addition Income Tax</td>
														<td>
															<asp:Label ID="AddIncomeTax" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="AddIncomeTaxYTD" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>EOBI Deduction</td>
														<td>
															<asp:Label ID="EOBI" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="EOBIYTD" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Provident Fund</td>
														<td>
															<asp:Label ID="PFtxt" runat="server"></asp:Label>
														</td>
														<td>
															<asp:Label ID="PFYTD" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td>Other Deductions</td>
														<td>
															<asp:Label ID="OtherDeductiontxt" runat="server"></asp:Label></td>
														<td>
															<asp:Label ID="OtherDeductiontxtYTD" runat="server"></asp:Label></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
													</tr>
												</table>

											</div>
										</div>
									</div>
									<div class="row">
										<div class="Box05 col-md-6 col-lg-6 col-sm-6">
											<div class="table-1">
												<table>
													<tbody>
														<tr>
															<td style="width: 58%;">Total</td>
															<td style="text-align: left;">
																<asp:Label ID="Total1" runat="server"></asp:Label></td>
															<td>
																<asp:Label ID="Total2" runat="server"></asp:Label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="Box06 col-md-6 col-lg-6 col-sm-6" style="margin-top: 23.5px;">
											<div class="table-1">
												<table>
													<tbody>
														<tr>
															<td style="width: 66%;">Total</td>
															<td>
																<asp:Label ID="Total3" runat="server"></asp:Label></td>
															<td>
																<asp:Label ID="Total4" runat="server"></asp:Label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="Box09 col-md-6 col-lg-6 col-sm-6 main">
											<div class="table-1">
												<table>
													<tbody>
														<tr>
															<td style="font-weight: bold;">Net Amount:</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="Box06 col-md-6 col-lg-6 col-sm-6 extra">
											<div class="table-1">
												<table>
													<tbody>
														<tr>
															<td></td>
															<td></td>
															<td></td>
															<td style="font-weight: bold; text-align: right;">
																<asp:Label ID="NetAmnt" runat="server"></asp:Label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="Box07 col-md-12 col-lg-12 col-sm-12">
											<div class="table-1">
												<table>
													<tbody>
														<tr>
															<td style="font-weight: bold; width: 25%;">Amount of Words</td>
															<td>
																<asp:Label ID="AmountWrds" runat="server"></asp:Label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="row">
										<div class="Box08 col-md-12 col-lg-12 col-sm-12">
											<div class="table-1">
												<table style="background-color: #c0c0c0;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Income Tax Details:</td>
														</tr>
													</tbody>
												</table>
												<table style="margin-top: 0px; border-top: none;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Annual Taxable Income</td>
															<td></td>
															<td>
																<asp:Label ID="AnnualTaxInc" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Total Income Tax Liability</td>
															<td></td>
															<td>
																<asp:Label ID="TotalIncomeTaxLia" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Income Tax Paid</td>
															<td></td>
															<td>
																<asp:Label ID="IncomeTaxPaid" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Advance Tax Adjustment</td>
															<td></td>
															<td>-</td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Tax Credit </td>
															<td></td>
															<td>-</td>
														</tr>
														<tr>
															<td style="font-weight: bold;">Remaining Income Tax Payable</td>
															<td></td>
															<td>
																<asp:Label ID="RemIncomeTaxPayable" runat="server"></asp:Label></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="row">
										<div class="Box11 col-md-12 col-lg-12 col-sm-12">
											<div class="table-1">
												<table style="background-color: #c0c0c0;">
													<tbody>
														<tr>
															<td style="font-weight: bold;">Provident Fund Deatils:</td>
														</tr>
													</tbody>
												</table>
												<table style="margin-top: 0px; border-top: none;">
													<tfoot>
														<tr>
															<td style="font-weight: bold; border: 2px solid; border-left: none; border-top: none;"></td>
															<td style="text-align: center; font-weight: bold; border: 2px solid; border-top: none;">Opening</td>
															<td style="text-align: center; font-weight: bold; border: 2px solid; border-top: none;">Current Month</td>
															<td style="text-align: center; font-weight: bold; border-right: none; border: 2px solid; border-top: none;">Closing</td>
														</tr>
														<tr>
															<td style="font-weight: bold; border: 2px solid; border-left: none; border-top: none;">Employee Contribution</td>
															<td style="border: 2px solid; border-left: none; border-top: none;">
																<asp:Label ID="EmployeePF" runat="server"></asp:Label></td>
															<td style="border: 2px solid; border-left: none; border-top: none;">
																<asp:Label ID="Label47" runat="server"></asp:Label></td>
															<td style="border: 2px solid; border-left: none; border-top: none; text-align: right;">
																<asp:Label ID="Label48" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold; border: 2px solid; border-left: none; border-top: none;">Employer Contribution</td>
															<td style="border: 2px solid; border-left: none; border-top: none;">
																<asp:Label ID="EmployeerPF" runat="server"></asp:Label></td>
															<td style="border: 2px solid; border-left: none; border-top: none;">
																<asp:Label ID="Label49" runat="server"></asp:Label></td>
															<td style="border: 2px solid; border-left: none; border-top: none; text-align: right;">
																<asp:Label ID="Label50" runat="server"></asp:Label></td>
														</tr>
														<tr>
															<td style="font-weight: bold; border: 2px solid; border-left: none; border-top: none;">Total</td>
															<td style="border: 2px solid; border-left: none; border-top: none;">
																<asp:Label ID="TotalPF" runat="server"></asp:Label></td>
															<td style="border: 2px solid; border-left: none; border-top: none;">
																<asp:Label ID="Label51" runat="server"></asp:Label></td>
															<td style="border: 2px solid; border-left: none; border-top: none; text-align: right;">
																<asp:Label ID="Label52" runat="server"></asp:Label></td>
														</tr>
													</tfoot>
												</table>

											</div>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div>
										<p style="text-align: center;">Techno Financials - This is auto generated pay slip and needs no signature.</p>
									</div>
								</div>
							</div>
							<style type="text/css">
								.Box09.col-md-6.col-lg-6.col-sm-6.main table {
									border-top: 1px solid #fff !important;
								}

								.table-1.B_class table {
									border: 0px !important;
									margin: 0px !important;
								}

								.table-1.B_class {
									border: 0px !important;
									padding: 0px !important;
									border-bottom: 0px !important;
								}

									.table-1.B_class td:nth-child(1) {
										width: 50%;
									}

								.Box09 table {
									border-top-color: #fff !important;
									border-bottom-color: #fff !important;
								}

								.Box03 table {
									border-bottom-color: #fff !important;
								}

								.extra table, .Box04 table {
									border-bottom-color: #fff !important;
								}

								.Box06 table {
									border-bottom-color: #fff !important;
								}

								.table-1.B_class td:nth-child(4) {
									text-align: right;
								}

								.table-1.B_class td:nth-child(2) {
									text-align: left;
								}

								.Box04 tr:nth-child(7) td {
									height: 17px;
								}
								/*    @media only screen and (max-width: 1980px){
    .Box03.col-md-6.col-lg-6.col-sm-6 tr:nth-child(9){
         height:45px;
    }
    .Box04.col-md-6 tr:nth-child(6) {
    height: 38px;
}
    }
*/
							</style>
							<div class="table-1 B_class" id="ReportPayrollFotter" runat="server">
								<table>
									<tbody>
										<tr>
											<td><asp:Label runat="server" ID="hdnCompanyNamefooter"> </asp:Label> - Human Capital Management</td>
											<td>© <asp:Label runat="server" ID="hdnCompanyNamecopyrights"> </asp:Label></td>
											
											<td>
												<asp:Label ID="Datetime" runat="server"></asp:Label></td>
											<td style="display: none;">USER:
												<asp:Label ID="username" runat="server"></asp:Label></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<%-- Main Div For Print The PAy SLip --%>
					<asp:UpdatePanel ID="UpdatePanel1" runat="server">
						<ContentTemplate>
							<div runat="server" id="divPayrSlip" visible="false">
								<asp:Literal ID="ltrShowPaySlip" runat="server"></asp:Literal>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
					<div id="divPrint" style="display: none;">
						<%-- Start --%>

						<div class="B_class" id="ReportPayrollasa">
							<img src="<%--/assets/images/01-01.png--%>https://www.technofinancials.com/assets/files/companies_logos/sigma-distributors_75849.png" alt="Alternate Text" style="width: 50px; position: absolute;" />
							<div class="AddressSide">
								<h1>Beta 2.0 LLC</h1>
								<h3>Payslip</h3>
								<h4>For the Month of June 2020</h4>
							</div>
							<div style="width: 100%; display: block;">
								<div class="Box01 col-md-6">

									<div class="OneLine">
										<h3>Employee Code:</h3>
										<p class="">Beta 2.0_00001</p>
									</div>
									<div class="OneLine">
										<h3>Location</h3>
										<p class="">Karachi</p>
									</div>
									<div class="OneLine">
										<h3>Designation:</h3>
										<p class="">CEO </p>
									</div>
									<div class="OneLine">
										<h3>Date of Joining</h3>
										<p class="">10-Jul-2020 </p>
									</div>
									<div class="OneLine">
										<h3>Date of Birth</h3>
										<p class="">10-Jul-1985</p>
									</div>
									<div class="OneLine">
										<h3>Basic Salary</h3>
										<p class="">700,000</p>
									</div>
									<div class="OneLine">
										<h3>Account</h3>
										<p class="">015345667893</p>
									</div>
								</div>


								<div class="Box02 col-md-6">

									<div class="OneLine">
										<h3>Name :</h3>
										<p class="">Muhammad Ahmed Shaikh</p>
									</div>
									<div class="OneLine">
										<h3>Department :</h3>
										<p class="">Directors</p>
									</div>
									<div class="OneLine">
										<h3>Employee Type :</h3>
										<p class="">Permanent</p>
									</div>
									<div class="OneLine">
										<h3>Date:</h3>
										<p class="">10-Jul-2020</p>
									</div>
									<div class="OneLine">
										<h3>CNIC :</h3>
										<p class="">45100-1256486-8</p>
									</div>
									<div class="OneLine">
										<h3>Bank</h3>
										<p class="">Meezan Bank Limited</p>
									</div>
									<div class="OneLine">
										<h3>&nbsp;</h3>
										<p class="">&nbsp;</p>
									</div>
								</div>
							</div>
							<div class="clearfix">&nbsp;</div>
							<div class="clearfix">&nbsp;</div>

							<div class="Box03 col-md-6">
								<div class="">
									<table class="slip-table slip-table-bordered ">
										<thead>
											<tr>
												<th scope="col">Allowance</th>
												<th scope="col">Amount</th>
												<th scope="col">YTD</th>
											</tr>
										</thead>
										<tbody>
											<asp:Literal ID="ltrAllowanceTable" runat="server"></asp:Literal>
										</tbody>
										<tfoot>
											<tr>
												<td><span id="">Total</span></td>
												<td><span id="">1,000,000</span></td>
												<td><span id="">1,000,000</span></td>
											</tr>
											<tr>
												<td><span id=""><b>Amount in Words:</b></span></td>
												<td colspan="2" style="text-align: left; margin-left: 20px;"><span id=""></span>One million only</td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>



							<div class="Box04">
								<div class="">
									<table class="slip-table slip-table-bordered ">
										<thead>
											<tr>
												<th scope="col">Deductions</th>
												<th scope="col">Amount</th>
												<th scope="col">YTD</th>
											</tr>
										</thead>
										<tbody>
											<asp:Literal ID="ltrDeductionTable" runat="server"></asp:Literal>
										</tbody>
										<tfoot>
											<tr>
												<td><span id="">Total</span></td>
												<td><span id="">170,642</span></td>
												<td><span id="">170,642</span></td>
											</tr>
											<tr>
												<td><span id=""><b>Amount in Words:</b></span></td>
												<td colspan="2" style="text-align: left; margin-left: 20px;"><span id=""></span></td>
											</tr>
										</tfoot>
									</table>
								</div>

							</div>

							<div class="clearfix">&nbsp;</div>

							<div class="Box05">
								<table class="slip-table slip-table-bordered ">
									<tbody>
										<tr>
											<td><span><b>Amount in Words:</b>Pak Rupees Five Thousand Eight Hundred Rupees Only.</span></td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="clearfix">&nbsp;</div>

							<div class="Box03">
								<div class="">
									<table class="slip-table slip-table-bordered ">
										<thead>
											<tr>
												<th scope="col">Income Tax Details</th>
												<th scope="col">Amount</th>
											</tr>
										</thead>
										<tbody>
											<asp:Literal ID="ltrTaxTable" runat="server"></asp:Literal>

										</tbody>
										<tfoot>
											<tr>
												<td><span id="">Balance</span></td>
												<td><span id="">0</span></td>
											</tr>

										</tfoot>
									</table>
								</div>
							</div>



							<div class="Box04">
								<div class="">
									<table class="slip-table slip-table-bordered ">
										<thead>
											<tr>
												<th scope="col">PF Details</th>
												<th scope="col">Amount</th>
											</tr>
										</thead>
										<tbody>
											<asp:Literal ID="ltrPFTable" runat="server"></asp:Literal>

										</tbody>
										<tfoot>
											<tr>
												<td><span id="">Net Balance</span></td>
												<td><span id="">200,000</span></td>
												<td><span id="">200,000</span></td>
											</tr>

										</tfoot>
									</table>
								</div>

							</div>
						</div>

						<style>
							.B_class {
								padding: 20px;
								/*width: 800px;
         margin: 0 auto;
         display: block;*/
								border: 2px solid #000;
								background: #fff;
							}

								.B_class h1,
								.B_class h2,
								.B_class h3,
								.B_class h4,
								.B_class p,
								.B_class td {
									margin: 0;
									font-family: lato;
									color: #000;
								}

								/*                                .B_class table {
                                    border: 2px solid;
                                    margin-top: -25px;
                                }*/

								.B_class table td, .B_class table th {
									padding: 6.4px;
								}

								.B_class .slip-table > thead > tr > th {
									background-color: #ffffff;
									border-bottom: 2px solid #000000 !important;
									border-top: 2px solid #000000 !important;
									color: #000;
									padding: 0 10px;
								}

							.slip-table-bordered > thead > tr > th,
							.slip-table-bordered > thead > tr > td,
							.slip-table-bordered > tbody > tr > th,
							.slip-table-bordered > tbody > tr > td {
								border-top: 2px solid transparent !important;
								border: 0;
								vertical-align: middle;
								padding: .1rem;
								font-size: 14px;
							}

							.slip-table > thead > tr > th,
							.slip-table > thead > tr > td,
							.slip-table > tbody > tr > th,
							.slip-table > tbody > tr > td,
							.slip-table > tfoot > tr > th,
							.slip-table > tfoot > tr > td {
								height: 32px;
								line-height: 1.428571429;
								vertical-align: middle;
								border-top: 2px solid #ddd;
								padding: 0 10px;
							}

							.slip-table {
								width: 100%;
								max-width: 100%;
								background-color: transparent;
								border-collapse: collapse;
								border-spacing: 0;
							}

							.slip-table-bordered > tfoot > tr > th,
							.slip-table-bordered > tfoot > tr > td {
								border-top: 2px solid #000 !important;
								border: 0;
								vertical-align: middle;
								padding: .1rem;
								font-size: 14px;
								padding: 0 10px;
							}

							.B_class .slip-table > thead > tr > th {
								text-align: right;
							}

								.B_class .slip-table > thead > tr > th:nth-child(1) {
									text-align: left;
								}

							.B_class .slip-table > tbody > tr > td {
								text-align: right !important;
								height: 28px;
							}

								.B_class .slip-table > tbody > tr > td:nth-child(1) {
									text-align: left !important;
								}

							.B_class .slip-table > tfoot > tr > td {
								text-align: right !important;
								height: 43px;
							}

							.totalSalaries {
								font-weight: bold !important;
								color: #188ae2 !important;
								font-size: 16px !important;
							}

							@media only screen and (max-width: 1280px) and (min-width: 800px) {
								.tf-alert-danger p {
									display: inline-block;
									margin-top: 0px;
									position: absolute;
									margin-left: 7px;
									font-size: 13px;
									color: #fff;
								}
							}

							.B_class .slip-table > tfoot > tr > td:nth-child(1) {
								text-align: left !important;
								width: 200px !important;
							}
							/*.B_class .slip-table > tfoot > tr > td:nth-child(1) {
                                    text-align: left;
                                        width: 150px !important;
                                }*/

							.B_class hr {
								margin-top: 20px;
								margin-bottom: 10px;
								border: 0;
								border-top: 2px solid #000000;
							}

							/*.B_class tbody tr td {
                padding: 15px 10px !important;
            }*/
							button#btnView {
								padding: 4px 24px !important;
								margin-top: 20px !important;
							}

							.tableP {
								height: 34px;
								padding: 6.4px;
								border: 2px solid;
								margin-top: -6px !important;
								width: 99.9%;
							}

								.tableP span {
									margin-left: 40px;
									text-transform: capitalize;
								}

							.AddressSide {
								height: 90px;
								color: #000;
								text-align: center;
							}

								.AddressSide h1 {
									font-size: 18px;
									text-transform: capitalize;
									font-weight: 700;
									margin: 5px 0;
									color: #000;
								}

								.AddressSide h2 {
									font-size: 18px;
									text-align: right;
									font-weight: 400;
									color: #000;
									margin: 5px 0;
								}

								.AddressSide h3 {
									font-size: 18px;
									text-transform: capitalize;
									font-weight: 700;
									margin: 5px 0;
									color: #000;
								}

								.AddressSide h4 {
									font-size: 18px;
									font-weight: 700;
									text-transform: initial;
									margin-bottom: 15px;
									color: #000;
									margin: 5px 0;
								}

							span#Month {
								padding-left: 5px;
							}

							.AddressSide p {
								font-size: 16px;
								font-weight: 500;
								max-width: 350px;
								text-transform: capitalize;
								color: #000;
							}

							.bigBox {
								width: 100%;
								border: 2px solid #000;
								height: 400px;
								margin: 50px 0 20px;
							}

							.Box01 {
								border: 2px solid #000;
								height: 240px;
								display: inline-block;
								padding: 10px;
								/* position: relative; */
								color: #000;
								border-right: 0;
							}

							.Box02 {
								border: px solid #000;
								height: 240px;
								display: inline-block;
								padding: 8px 20px;
								margin-right: -10px;
								margin-left: -5px;
								position: relative;
								color: #000;
								border-left: 0;
							}

							.Box03 {
								display: inline-block;
								padding-left: 0px;
								padding-right: 0px;
							}

							.OneLine {
								width: 100%;
								height: 28px;
							}

							.form-group h4, label {
								margin: 0px;
							}

							.OneLine h3 {
								display: inline-block;
								font-size: 16px;
								font-weight: 700;
								margin: 3px 0;
								color: #000;
							}

							.OneLine p {
								display: inline-block;
								font-size: 16px;
								width: 60%;
								margin: 3px 0;
								color: #000;
							}

							.totalSalaries {
								font-weight: bold !important;
								color: #188ae2 !important;
								font-size: 16px !important;
							}

							.line-four {
								width: 99.9%;
								display: block;
							}

							.payslip-totalcol-bar {
								border-top: 2px solid #000;
								border-bottom: 2px solid #000;
							}
						</style>



						<%-- End --%>
					</div>
					<div class="clearfix">&nbsp;</div>

				</section>


				<!-- #dash-content -->
			</div>
			<!-- .wrap -->
			<uc:Footer ID="footer1" runat="server"></uc:Footer>
		</main>

		<!--========== END app main -->
		<uc:Scripts ID="script1" runat="server"></uc:Scripts>
		<script src="/assets/js/printThis.js"></script>
		<script>
			function PrintThis() {
				$('#divPrint').printThis({
					importCSS: false,
					importStyle: false,
					header: "",
					footer: "",
					loadCSS: ""
				});
			}
		</script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
		<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

		<script>
			function Print() {
				debugger;
				var HTML_Width = $("#ReportPayroll").width();
				var HTML_Height = $("#ReportPayroll").height();
				var top_left_margin = 15;
				var PDF_Width = HTML_Width + (top_left_margin * 2);
				var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
				var canvas_image_width = HTML_Width;
				var canvas_image_height = HTML_Height;

				var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

				html2canvas($("#ReportPayroll")[0]).then(function (canvas) {
					var imgData = canvas.toDataURL("image/jpeg", 1.0);
					var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
					// var pdf = new jsPDF("p", "pt", "a4");
					pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
					for (var i = 1; i <= totalPDFPages; i++) {
						pdf.addPage(PDF_Width, PDF_Height);
						pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height * i) + (top_left_margin * 4), canvas_image_width, canvas_image_height);
					}
					pdf.save("PaySlip.pdf");
					$(".html-content").hide();
				});
			}
		</script>
	</form>
</body>
</html>

