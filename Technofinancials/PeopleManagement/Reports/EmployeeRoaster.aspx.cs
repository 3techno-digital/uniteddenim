﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Reports
{
    public partial class EmployeeRoaster : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!IsPostBack)
                { 
                    BindDropdowns();
                    BindGridView();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void BindDropdowns()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                ddlShifts.DataSource = objDB.GetApproveAllWorkingShiftsByCompanyID(ref errorMsg);
                ddlShifts.DataTextField = "ShiftName";
                ddlShifts.DataValueField = "ShiftID";
                ddlShifts.DataBind();
                ddlShifts.Items.Insert(0, new ListItem("ALL", "0"));

                ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                ddlDepartment.DataTextField = "DeptName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

                ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
                ddlDesignation.DataTextField = "DesgTitle";
                ddlDesignation.DataValueField = "DesgID";
                ddlDesignation.DataBind();
                ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

                ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

                ddlLocation.DataSource = objDB.GetAllLocationsNew(ref errorMsg);
                ddlLocation.DataTextField = "NAME";
                ddlLocation.DataValueField = "NAME";
                ddlLocation.DataBind();
                ddlLocation.Items.Insert(0, new ListItem("ALL", "0"));
            }
            catch (Exception ex)
            {
            }
        }

        protected void BindGridView()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.ShiftID = Convert.ToInt16(ddlShifts.SelectedValue);
            objDB.DeptID = Convert.ToInt16(ddlDepartment.SelectedValue);
            objDB.DesgID = Convert.ToInt16(ddlDesignation.SelectedValue);
            objDB.Location = ddlLocation.SelectedValue;
            objDB.EmployeeID = Convert.ToInt16(ddlEmployee.SelectedValue);
            objDB.KTID = txtKTId.Text;

            DataTable dt = objDB.Get_EmployeeWiseShiftReport(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                BindGridView();
            }
            catch (Exception ex)
            {
            }
        }
    }
}