﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Reports
{
    public partial class LeftEmployee : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false;
                    BindDropdowns();
                    if (HttpContext.Current.Items["CurrentMonth"] != null)
                    {

                        txtFromDate.Text = DateTime.Now.ToString("01-MMM-yyyy");
                        txtToDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");

                        objDB.FromDate = txtFromDate.Text;
                        objDB.ToDate = txtToDate.Text;
                        objDB.CompanyID = Convert.ToInt16(Session["CompanyID"]);
                        objDB.DeptID = 0;
                        objDB.DesgID = 0;
                        objDB.EmployeeID = 0;
                        objDB.KTID = "";
                        objDB.Location = "";

                        objDB.KTID = "";
                        objDB.Location = "";



                        DataTable dt = objDB.Get_LeftEmployeeReport(ref errorMsg);
                        gv.DataSource = dt;
                        gv.DataBind();
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                gv.UseAccessibleHeader = true;
                                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                            }
                        }

                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");

        }

        protected void BindGridView(DateTime fdate, DateTime tdate, string Department, string Designation, string EmployeeId, string KTId)
        {
            divAlertMsg.Visible = false;
            DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
            DateTime.TryParse(txtFromDate.Text, out Fdate);
            DateTime.TryParse(txtToDate.Text, out Tdate);

            if (Tdate < Fdate)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "From Date shoul be less than To Date";
                gv.DataSource = "";
                gv.DataBind();
                return;
            }
            if (Fdate > DateTime.Now)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = "From Date shoul be Equal or less than Current Date";
                gv.DataSource = "";
                gv.DataBind();
                return;
            }
             
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.FromDate = txtFromDate.Text;
            objDB.ToDate = txtToDate.Text;
            objDB.DeptID = Convert.ToInt16(ddlDepartment.SelectedValue);
            objDB.DesgID = Convert.ToInt16(ddlDesignation.SelectedValue);
            objDB.EmployeeID = Convert.ToInt16(ddlEmployee.SelectedValue);
            objDB.KTID = txtKTId.Text;
            objDB.Location = ddlLocation.SelectedValue;

            DataTable dt = objDB.Get_LeftEmployeeReport(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void BindDropdowns()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            DataTable dt = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDepartment.DataSource = dt;
            ddlDepartment.DataTextField = "DeptName";
            ddlDepartment.DataValueField = "DeptID";
            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

            ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
            ddlDesignation.DataTextField = "DesgTitle";
            ddlDesignation.DataValueField = "DesgID";
            ddlDesignation.DataBind();
            ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

            ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

            ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
            ddlLocation.DataTextField = "NAME";
            ddlLocation.DataValueField = "NAME";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("ALL", "0"));
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                DateTime fdate = DateTime.Parse(txtFromDate.Text);
                DateTime tdate = DateTime.Parse(txtToDate.Text);
                BindGridView(fdate, tdate, ddlDepartment.SelectedValue, ddlDesignation.SelectedValue, ddlEmployee.SelectedValue, txtKTId.Text);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;

            }
        }
    }
}