﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Reports
{
    public partial class EmployeeLeaveReport : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    divAlertMsg.Visible = false;                     
                    BindDropdown();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        protected void BindDropdown()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

            ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
            ddlDepartment.DataTextField = "DeptName";
            ddlDepartment.DataValueField = "DeptID";
            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

            ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg);
            ddlDesignation.DataTextField = "DesgTitle";
            ddlDesignation.DataValueField = "DesgID";
            ddlDesignation.DataBind();
            ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

            ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

            ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
            ddlLocation.DataTextField = "NAME";
            ddlLocation.DataValueField = "NAME";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("ALL", "0"));
        }

        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
                DateTime.TryParse(txtFromDate.Text, out Fdate);
                DateTime.TryParse(txtToDate.Text, out Tdate);
                if (Tdate < Fdate)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be less than To Date";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }
                if (Fdate > DateTime.Now)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be Equal or less than Current Date";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }
                if ((Tdate - Fdate).Days > 31 || (Fdate.Day <= Tdate.Day && (Tdate.Month - Fdate.Month) > 0))
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "date range should be of 1 month";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }

                divAlertMsg.Visible = false;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.FromDate = txtFromDate.Text;
                objDB.ToDate = txtToDate.Text;
                objDB.DeptID = Convert.ToInt16(ddlDepartment.SelectedValue);
                objDB.DesgID = Convert.ToInt16(ddlDesignation.SelectedValue);
                objDB.EmployeeID = Convert.ToInt16(ddlEmployee.SelectedValue);
                objDB.KTID = txtKTId.Text;
                objDB.Location = ddlLocation.SelectedValue;

                DataTable dt = objDB.Get_EmployeeLeaveReport(ref errorMsg);
                gv.DataSource = dt;
                gv.DataBind();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        gv.UseAccessibleHeader = true;
                        gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                }
                else
                {
                    gv.DataSource = null;
                    gv.DataBind();
                }
            }

            catch (Exception ex)
            {
                gv.DataSource = null;
                gv.DataBind();
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }
    }
}