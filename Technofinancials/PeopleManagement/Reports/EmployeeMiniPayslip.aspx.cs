﻿using System;
using System.Data;
using System.Web.UI;

namespace Technofinancials.PeopleManagement.Reports
{
	public partial class EmployeeMiniPayslip : System.Web.UI.Page
	{
        DBQueries objDB = new DBQueries();
        string errorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (!Page.IsPostBack)
                {
                    BindDropDown();
                    ReportPayroll.Visible = false;
                    ReportPayrollFotter.Visible = false;
                    divAlertMsg.Visible = false;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        void BindDropDown()
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            ddlEmployee.DataSource = objDB.GetAllEmployeesByCompanyID(ref errorMsg);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " Million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string content = "";
                string header = "";
                string footer = "";
                content = printPaySlip();

                Common.generatePDF(header, footer, content, "Pay Slip-" + Session["UserName"].ToString() + " (" + txtPayrollDate.Text + ")", "A4", "Portrait");

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private string printPaySlip()
        {
            string header = "";
            string content = "";
            string footer = "";

            try
            {
                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);//Convert.ToInt32(Session["EmployeeID"].ToString());
                objDB.PayrollDate = txtPayrollDate.Text;
                objDB.DocType = "Pay Slip";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                DataTable dtEmp = new DataTable();
                dtEmp = objDB.GetEmployeeByID(ref errorMsg);

                if (dtEmp != null && dtEmp.Rows.Count > 0)
                {
                    string logo = Session["CompanyLogo"].ToString();
                    content = content.Replace("##COMPANY_NAME##", Session["CompanyName"].ToString());
                    content = content.Replace("##COMPANY_LOGO##", Session["CompanyLogo"].ToString());
                    content = content.Replace("##PAYROLL_MONTH##", txtPayrollDate.Text);
                    content = content.Replace("##LOCATION##", "");

                    content = content.Replace("##EMPLOYEE_CODE##", dtEmp.Rows[0]["EmployeeCode"].ToString());
                    content = content.Replace("##DESIGNATION##", dtEmp.Rows[0]["DesgTitle"].ToString());
                    content = content.Replace("##DATE_OF_JOINING##", dtEmp.Rows[0]["DateOfJoining"].ToString());
                    content = content.Replace("##DATE_OF_BIRTH##", dtEmp.Rows[0]["DOB"].ToString());
                    content = content.Replace("##BASIC_SALARY##", dtEmp.Rows[0]["BasicSalary"].ToString());
                    content = content.Replace("##ACCOUNT_NO##", dtEmp.Rows[0]["AccountNo"].ToString());
                    content = content.Replace("##EMPLOYEE_NAME##", dtEmp.Rows[0]["EmployeeName"].ToString());
                    content = content.Replace("##DEPARTMENT##", dtEmp.Rows[0]["DeptName"].ToString());
                    content = content.Replace("##EMPLOYEE_TYPE##", dtEmp.Rows[0]["EmploymentType"].ToString());
                    content = content.Replace("##DATE##", dtEmp.Rows[0]["EmployeeCode"].ToString());
                    content = content.Replace("##CNIC##", dtEmp.Rows[0]["CNIC"].ToString());
                    content = content.Replace("##BANK_NAME##", dtEmp.Rows[0]["BankName"].ToString());

                    string st = "";

                    DataTable dtAllowance = new DataTable();
                    dtAllowance = objDB.GetNewPayrollDetailsByEmployeeID(ref errorMsg);
                    if (dtAllowance != null && dtAllowance.Rows.Count > 0)
                    {

                        // Allowance Section
                        double total = 0;
                        double deductions = 0;

                        total = double.Parse(dtAllowance.Rows[0]["BasicSalary"].ToString()) + double.Parse(dtAllowance.Rows[0]["OverTimeAmount"].ToString()) + double.Parse(dtAllowance.Rows[0]["Additions"].ToString()) + double.Parse(dtAllowance.Rows[0]["Maintenance"].ToString()) + double.Parse(dtAllowance.Rows[0]["Fuel"].ToString()) + double.Parse(dtAllowance.Rows[0]["FoodAllowence"].ToString()) + double.Parse(dtAllowance.Rows[0]["AddOns"].ToString());
                        deductions = (double.Parse(dtAllowance.Rows[0]["Tax"].ToString()) + double.Parse(dtAllowance.Rows[0]["EOBI"].ToString()) + double.Parse(dtAllowance.Rows[0]["AbsentAmount"].ToString()) + double.Parse(dtAllowance.Rows[0]["EmpDeductions"].ToString()) + double.Parse(dtAllowance.Rows[0]["EmployeePF"].ToString()) + double.Parse(dtAllowance.Rows[0]["Advance"].ToString()));

                        st += @"  <table class='slip-table slip-table-bordered '>
          <thead>
            <tr>
              <th scope='col'>Allowance</th>
              <th scope='col'>Amount</th>
              <th scope='col'>YTD</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><span id=''>" + "BasicSalary" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["BasicSalary"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["BasicSalary"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "OverTimeAmount" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["OverTimeAmount"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["OverTimeAmount"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Additions" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Additions"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Additions"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "AddOns" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["AddOns"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["AddOns"].ToString() + @"</span></td>
            </tr>";


                        st += @"<tr>
              <td><span id=''>" + "Maintenance" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Maintenance"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Maintenance"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Fuel Allowance" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Fuel"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Fuel"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Food Allowance" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["FoodAllowence"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["FoodAllowence"].ToString() + @"</span></td>
            </tr>
            
</tbody>
          <tfoot>
            <tr>
              <td><span id=''>Total</span></td>
              <td><span id=''>" + total.ToString() + @"</span></td>
              <td><span id=''>" + total.ToString() + @"</span></td>
            </tr>
            <tr>
              <td><span id=''><b>Amount in Words:</b></span></td>
              <td colspan='2' style='text-align: left; margin-left: 20px;'><span id=''></span>" + Common.NumberToWords(total) + @"</td>
            </tr>
          </tfoot>
        </table>
";

                        content = content.Replace("##ALLOWANCE_TABLE##", st);

                        // Deduction Section

                        st = "";
                        st += @"
<table class='slip-table slip-table-bordered '>
          <thead>
            <tr>
              <th scope='col'>Deductions</th>
              <th scope='col'>Amount</th>
              <th scope='col'>YTD</th>
            </tr>
          </thead>
          <tbody>
<tr>
              <td><span id=''>" + "Income Tax" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Tax"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Tax"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "EOBI" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["EOBI"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["EOBI"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Advance" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Advance"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Advance"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Employee PF" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["EmployeePF"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["EmployeePF"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Employee Deductions" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["EmpDeductions"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["EmpDeductions"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Absent Amount" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["AbsentAmount"].ToString() + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["AbsentAmount"].ToString() + @"</span></td>
            </tr>
</tbody>
          <tfoot>
            <tr>
              <td><span id=''>Total</span></td>
              <td><span id=''>" + deductions.ToString() + @"</span></td>
              <td><span id=''>" + deductions.ToString() + @"</span></td>
            </tr>
            <tr>
              <td><span id=''><b>Amount in Words:</b></span></td>
              <td colspan='2' style='text-align: left; margin-left: 20px;'><span id=''>" + Common.NumberToWords(deductions) + @"</span></td>
            </tr>
          </tfoot>
        </table>";

                        content = content.Replace("##DEDUCTION_TABLE##", st);
                        content = content.Replace("##TOTAL_AMOUNTS##", (total - deductions).ToString());
                        content = content.Replace("##TOTAL_AMOUNT_IN_WORDS##", Common.NumberToWords((total - deductions)));

                        // Tax Section

                        st = "";
                        st += @"
<table class='slip-table slip-table-bordered '>
          <thead>
            <tr>
              <th scope='col'>Income Tax Detail</th>
              <th scope='col'>Amount</th>
            </tr>
          </thead>
          <tbody>
<tr>
              <td><span id=''>" + "Total Income Tax Liability" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Tax"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Income Tax Paid" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["Tax"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Tax Adjustment" + @"</span></td>
              <td><span id=''>" + "-" + @"</span></td>
            </tr>
</tbody>
          <tfoot>
            <tr>
              <td><span id=''>Balance</span></td>
              <td><span id=''>" + "0" + @"</span></td>
            </tr>

          </tfoot>
        </table>
";

                        content = content.Replace("##TAX_TABLE##", st);

                        // PF Section

                        st = "";
                        total = 0;
                        st += @"
 <table class='slip-table slip-table-bordered '>
          <thead>
            <tr>
              <th scope='col'>PF Details</th>
              <th scope='col'>Amount</th>
            </tr>
          </thead>
          <tbody>
<tr>
              <td><span id=''>" + "Employee Share" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["EmployeePF"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Employer Share" + @"</span></td>
              <td><span id=''>" + dtAllowance.Rows[0]["CompanyPF"].ToString() + @"</span></td>
            </tr>";

                        st += @"<tr>
              <td><span id=''>" + "Withdrawals" + @"</span></td>
              <td><span id=''>" + "-" + @"</span></td>
            </tr>
</tbody>
          <tfoot>
            <tr>
              <td><span id=''>Net Balance</span></td>
              <td><span id=''>" + (double.Parse(dtAllowance.Rows[0]["EmployeePF"].ToString()) + double.Parse(dtAllowance.Rows[0]["CompanyPF"].ToString())).ToString() + @"</span></td>
            </tr>

          </tfoot>
        </table>
";

                        content = content.Replace("##PF_TABLE##", st);
                    }

                    Common.addlog("Report", "HR", "Pay Slip", "Employees");
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
            return content;
        }

		protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                decimal totalMedicalYTD = 0;
                decimal totalYTDminus = 0;
                decimal sumTaxableAmount2 = 0;
                decimal AnnualTaxableIncome2 = 0;
                decimal minusValue = 0;
                decimal CalSum = 0;
                decimal sumYTDTAX = 0;
                decimal sumYTDAddTax = 0;

                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);// Convert.ToInt32(Session["EmployeeID"]);
                objDB.PayrollDate = txtPayrollDate.Text;
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                DataTable dt = new DataTable();
                dt = objDB.GetEmployeeByID(ref errorMsg);

                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        Month.Text = txtPayrollDate.Text;
                        empname.Text = dt.Rows[0]["EmployeeName"].ToString();
                        empDpt.Text = dt.Rows[0]["DeptName"].ToString();
                        DateofConf.Text = dt.Rows[0]["DateOfJoining"].ToString();
                        CNIC.Text = dt.Rows[0]["CNIC"].ToString();
                        BankName.Text = dt.Rows[0]["BankName"].ToString();
                        empCode.Text = dt.Rows[0]["WDID"].ToString();
                        empLocation.Text = dt.Rows[0]["PlaceOFPost"].ToString();
                        empDesg.Text = dt.Rows[0]["DesgTitle"].ToString();
                        DateofJoin.Text = dt.Rows[0]["DateOfJoining"].ToString();
                        DOB.Text = dt.Rows[0]["DOB"].ToString();
                        double bs = double.Parse(dt.Rows[0]["GrossAmount"].ToString());
                        double gs = double.Parse(dt.Rows[0]["BasicSalary"].ToString());
                        BasicSalary.Text = bs.ToString("N2");
                        empAccount.Text = dt.Rows[0]["AccountNo"].ToString();
                        empType.Text = dt.Rows[0]["EmploymentType"].ToString();
                        decimal houseRent2 = decimal.Parse(dt.Rows[0]["HouseAllownace"].ToString());
                        decimal utilityallowance2 = decimal.Parse(dt.Rows[0]["UtitlityAllowance"].ToString());
                        sumTaxableAmount2 = houseRent2 + utilityallowance2 + decimal.Parse(gs.ToString());
                    }
                    catch (Exception ex)
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = ex.Message;
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                    ReportPayroll.Visible = false;
                    ReportPayrollFotter.Visible = false;
                }

                DataTable dt2 = objDB.GetPayMiniSlipKTByEmployeeID(ref errorMsg);
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    try
                    {
                        double total, total2, NetAmount;

                        username.Text = dt.Rows[0]["EmployeeName"].ToString();
                        Datetime.Text = DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss");
                        string basicSalary = dt2.Rows[0]["BasicSalary"].ToString();
                        string SalesComissions = dt2.Rows[0]["Commission"].ToString();
                        string houseRents = dt2.Rows[0]["HouseRent"].ToString();
                        string UtilityAllows = dt2.Rows[0]["UtilityAllowence"].ToString();
                        string mdeicalAllowance = dt2.Rows[0]["MedicalAllowence"].ToString();
                        string ExpenseReims = dt2.Rows[0]["OtherExpenses"].ToString();
                        string EidBonuss = dt2.Rows[0]["EidBonus"].ToString();
                        string Spiffss = dt2.Rows[0]["Spiffs"].ToString();
                        string Overtimes = dt2.Rows[0]["OverTimeGeneralAmount"].ToString();
                        string OvertimeHolidayss = dt2.Rows[0]["OvertimeHolidayAmount2"].ToString();
                        string IncomeTaxs = dt2.Rows[0]["Tax"].ToString();
                        string AddIncomeTaxs = dt2.Rows[0]["AdditionalTax"].ToString();
                        string EOBIs = dt2.Rows[0]["EOBI"].ToString();
                        string OtherAllowances = dt2.Rows[0]["Other_Allowances"].ToString();
                        string PFNew = dt2.Rows[0]["PF"].ToString();
                        string OtherDeduction = dt2.Rows[0]["OtherDeductions"].ToString();
                        string TotSal2 = dt2.Rows[0]["NetSalry2"].ToString();
                        double totalsumm = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss) + double.Parse(EidBonuss);
                        TotSal2 = totalsumm.ToString();
                        double totalgs = double.Parse(basicSalary) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows);
                        decimal tts = decimal.Parse(totalgs.ToString());
                        tts = decimal.Round(tts, 2, MidpointRounding.AwayFromZero);
                        sumTaxableAmount2 = decimal.Parse(basicSalary) + decimal.Parse(houseRents) + decimal.Parse(UtilityAllows);
                        total = double.Parse(basicSalary) + double.Parse(OtherAllowances) + double.Parse(SalesComissions) + double.Parse(houseRents) + double.Parse(mdeicalAllowance) + double.Parse(UtilityAllows) + double.Parse(ExpenseReims) + double.Parse(Spiffss) + double.Parse(Overtimes) + double.Parse(OvertimeHolidayss);
                        total2 = double.Parse(IncomeTaxs) + double.Parse(EOBIs) + double.Parse(PFNew) + double.Parse(OtherDeduction) + double.Parse(AddIncomeTaxs);
                        NetAmount = total - total2;

                        decimal bs = decimal.Parse(basicSalary.ToString());
                        bs = decimal.Round(bs, 2, MidpointRounding.AwayFromZero);


                        decimal ttt = decimal.Parse(TotSal2.ToString());
                        ttt = decimal.Round(ttt, 2, MidpointRounding.AwayFromZero);

                        decimal sc = decimal.Parse(SalesComissions.ToString());
                        sc = decimal.Round(sc, 2, MidpointRounding.AwayFromZero);

                        decimal hr = decimal.Parse(houseRents.ToString());
                        hr = decimal.Round(hr, 2, MidpointRounding.AwayFromZero);

                        decimal ua = decimal.Parse(UtilityAllows.ToString());
                        ua = decimal.Round(ua, 2, MidpointRounding.AwayFromZero);

                        decimal er = decimal.Parse(ExpenseReims.ToString());
                        er = decimal.Round(er, 2, MidpointRounding.AwayFromZero);

                        decimal eb = decimal.Parse(EidBonuss.ToString());
                        eb = decimal.Round(eb, 2, MidpointRounding.AwayFromZero);

                        decimal sp = decimal.Parse(Spiffss.ToString());
                        sp = decimal.Round(sp, 2, MidpointRounding.AwayFromZero);

                        decimal ot = decimal.Parse(Overtimes.ToString());
                        ot = decimal.Round(ot, 2, MidpointRounding.AwayFromZero);

                        decimal oth = decimal.Parse(OvertimeHolidayss.ToString());
                        oth = decimal.Round(oth, 2, MidpointRounding.AwayFromZero);

                        decimal it = decimal.Parse(IncomeTaxs.ToString());
                        it = decimal.Round(it, 2, MidpointRounding.AwayFromZero);

                        decimal ait = decimal.Parse(AddIncomeTaxs.ToString());
                        ait = decimal.Round(ait, 2, MidpointRounding.AwayFromZero);

                        decimal eobi = decimal.Parse(EOBIs.ToString());
                        eobi = decimal.Round(eobi, 2, MidpointRounding.AwayFromZero);

                        decimal ma = decimal.Parse(mdeicalAllowance.ToString());
                        ma = decimal.Round(ma, 2, MidpointRounding.AwayFromZero);

                        decimal ollow = decimal.Parse(OtherAllowances.ToString());
                        ollow = decimal.Round(ollow, 2, MidpointRounding.AwayFromZero);

                        decimal pf = decimal.Parse(PFNew.ToString());
                        pf = decimal.Round(pf, 2, MidpointRounding.AwayFromZero);

                        decimal odnew = decimal.Parse(OtherDeduction.ToString());
                        odnew = decimal.Round(odnew, 2, MidpointRounding.AwayFromZero);

                        OtherDeductiontxt.Text = odnew.ToString("N2");
                        PFtxt.Text = pf.ToString("N2");
                        basicSalry.Text = bs.ToString("N2");
                        SalesComission.Text = sc.ToString("N2");
                        houseRent.Text = hr.ToString("N2");
                        UtilityAllow.Text = ua.ToString("N2");
                        ExpenseReim.Text = er.ToString("N2");
                        EidBonus.Text = eb.ToString("N2");
                        Spiffs.Text = sp.ToString("N2");
                        Overtime.Text = ot.ToString("N2");
                        OvertimeHolidays.Text = oth.ToString("N2");
                        IncomeTax.Text = it.ToString("N2");
                        AddIncomeTax.Text = ait.ToString("N2");
                        //AddIncomeTax.Text = "-";
                        EOBI.Text = eobi.ToString("N2");
                        mdecialAllow.Text = ma.ToString("N2");
                        // Total1.Text = total.ToString("N2");
                        Total1.Text = ttt.ToString("N2");
                        Total3.Text = total2.ToString("N2");
                        NetAmnt.Text = NetAmount.ToString("N2");
                        TotalGross.Text = tts.ToString("N2");
                        AmountWrds.Text = NumberToWords(Convert.ToInt32(NetAmount)) + " only-";
                        OtherAllowance.Text = ollow.ToString("N2");
                        if (SalesComission.Text == "0" || SalesComission.Text == "0.00")
                        {
                            SalesComission.Text = "-";
                        }

                        if (OtherAllowance.Text == "0" || OtherAllowance.Text == "0.00")
                        {
                            OtherAllowance.Text = "-";
                        }
                        if (houseRent.Text == "0" || houseRent.Text == "0.00")
                        {
                            houseRent.Text = "-";
                        }
                        if (UtilityAllow.Text == "0" || UtilityAllow.Text == "0.00")
                        {
                            UtilityAllow.Text = "-";
                        }
                        if (ExpenseReim.Text == "0" || ExpenseReim.Text == "0.00")
                        {
                            ExpenseReim.Text = "-";
                        }
                        if (EidBonus.Text == "0" || EidBonus.Text == "0.00")
                        {
                            EidBonus.Text = "-";
                        }
                        if (Spiffs.Text == "0" || Spiffs.Text == "0.00")
                        {
                            Spiffs.Text = "-";
                        }
                        if (Overtime.Text == "0" || Overtime.Text == "0.00")
                        {
                            Overtime.Text = "-";
                        }
                        if (OvertimeHolidays.Text == "0" || OvertimeHolidays.Text == "0.00")
                        {
                            OvertimeHolidays.Text = "-";
                        }
                        if (IncomeTax.Text == "0" || IncomeTax.Text == "0.00")
                        {
                            IncomeTax.Text = "-";
                        }
                        if (AddIncomeTax.Text == "0" || AddIncomeTax.Text == "0.00")
                        {
                            AddIncomeTax.Text = "-";
                        }
                        if (EOBI.Text == "0" || EOBI.Text == "0.00")
                        {
                            EOBI.Text = "-";
                        }
                    }
                    catch (Exception ex)
                    {
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = ex.Message;
                    }
                }
                else
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "No Record Found";
                    return;
                    ReportPayroll.Visible = false;
                    ReportPayrollFotter.Visible = false;
                }

                string date = "01-" + txtPayrollDate.Text;
                objDB.PayrollDate = date;
                DateTime dateTime12 = Convert.ToDateTime(date);
                string months = dateTime12.ToString("MM");
                int monthss = int.Parse(months);
                int RemainingMonths = 0;
                double TotalSalary2 = 0;

                if (months != null)
                {
                    if (monthss == 07)
                    {
                        RemainingMonths = 11;
                    }
                    else if (monthss == 08)
                    {
                        RemainingMonths = 10;
                    }
                    else if (monthss == 09)
                    {
                        RemainingMonths = 09;
                    }
                    else if (monthss == 10)
                    {
                        RemainingMonths = 08;
                    }
                    else if (monthss == 11)
                    {
                        RemainingMonths = 07;
                    }
                    else if (monthss == 12)
                    {
                        RemainingMonths = 06;
                    }
                    else if (monthss == 01)
                    {
                        RemainingMonths = 05;
                    }
                    else if (monthss == 02)
                    {
                        RemainingMonths = 04;
                    }
                    else if (monthss == 03)
                    {
                        RemainingMonths = 03;
                    }
                    else if (monthss == 04)
                    {
                        RemainingMonths = 02;
                    }
                    else if (monthss == 05)
                    {
                        RemainingMonths = 01;
                    }
                    else if (monthss == 06)
                    {
                        RemainingMonths = 0;
                    }
                }
                if (RemainingMonths != 0)
                {
                    double subtract = double.Parse(dt.Rows[0]["GrossAmount"].ToString()) - double.Parse(dt.Rows[0]["MedicalAllowance"].ToString());
                    TotalSalary2 = RemainingMonths * subtract;
                }
                
                //objDB.PayAmount = float.Parse(AnnualTaxableIncome.ToString());
                //objDB.PayAmount = float.Parse(AnnualTaxableIncome2.ToString());
                objDB.PayrollDate = date;
                DataTable dt4 = objDB.GetAnnualTaxableIncomePayslips(ref errorMsg);
                if (dt4 != null && dt4.Rows.Count > 0)
                {
                    double TITL = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
                    TotalIncomeTaxLia.Text = TITL.ToString("N2");
                    sumYTDAddTax = decimal.Parse(TITL.ToString()) - sumYTDTAX;
                }

                double incometaxLia = double.Parse(dt4.Rows[0]["Total_Income_Tax_Lia"].ToString());
                double reamainingIncomeTax = incometaxLia;

                RemIncomeTaxPayable.Text = sumYTDAddTax.ToString("N2");

                divAlertMsg.Visible = false;
                ReportPayroll.Visible = true;
                ReportPayrollFotter.Visible = true;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                ReportPayroll.Visible = false;
                ReportPayrollFotter.Visible = false;
            }
        }
    }
}