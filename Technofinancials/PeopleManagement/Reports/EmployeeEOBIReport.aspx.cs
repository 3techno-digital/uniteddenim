﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Reports
{
	public partial class EmployeeEOBIReport : System.Web.UI.Page
	{
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessions();
            if (!Page.IsPostBack)
            {
                divAlertMsg.Visible = false;

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
                ddlLocation.DataTextField = "NAME";
                ddlLocation.DataValueField = "Location_ID";
                ddlLocation.DataBind();
                ddlLocation.Items.Insert(0, new ListItem("Select", "0"));
            }
        }
        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }



        protected void GetDatabyLocationandMonth(object sender, EventArgs e)
        {
            CheckSessions();
            DataTable dt = new DataTable();
            objDB.PayrollDate = txtEOBIDate.Text;
            objDB.PlaceOfPosting = ddlLocation.SelectedItem.Text;
            dt = objDB.GetEmployeesEOBIReport(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            //    Common.addlog("ViewAll", "HR", "All NewPayroll Viewed", "NewPayroll");
        }




    }
}