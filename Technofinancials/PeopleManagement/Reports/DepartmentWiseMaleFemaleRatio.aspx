﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DepartmentWiseMaleFemaleRatio.aspx.cs" Inherits="Technofinancials.PeopleManagement.Reports.DepartmentWiseMaleFemaleRatio" %>
  
<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
  <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Department Wise Male Female Ratio</h1>
                            </div>
                            <!-- /.col -->
                            
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                        
                    
                    <div class="row">
                             <div class="col-sm-12 gv-overflow-scrool">
                            <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" >
                                    <Columns>
                                    <asp:TemplateField HeaderText="Sr. No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Department Name">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("DeptName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Employees">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("totemployees") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Male">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("totmale") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Total Female">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("totFemale") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Male %">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("MalePerc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Female %">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("FemalePerc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                </Columns>                               
                            </asp:GridView>
                        </div>
                    </div>
                            </div>
                        </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>

        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>

        <style>
            .sick {
                color: #a200ff;
                font-weight: bold;
            }
            .mater {
                font-weight: bold;
                color: #0090ff;
            }
            .year {
                font-weight: bold;
                color: black;
            }   
            .casual {
                color: #0039f1;
                font-weight: bold;
            }
            .abs{
                color:red;
                font-weight: bold;
            }
            .pres{
                color: green;
                font-weight: bold;
            }
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }
            div#gv{
                margin-bottom:20%;
            }
                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }



            th.sorting {
                font-size: 12px !important;
                padding-left: 5px !important;
                padding-right: 5px !important;
            }
                        .content-header.second_heading .container-fluid {
    padding-left: 0px;
}
            .content-header.second_heading h1 {
    margin-left: 0px !important;
}
            /*td{
                text-align: center;
            }*/
        </style>
    </form>
</body>
</html>
