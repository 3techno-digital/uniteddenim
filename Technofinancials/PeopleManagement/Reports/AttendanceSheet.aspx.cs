﻿using IronPdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.PeopleManagement.Reports
{
    public partial class AttendanceSheet : System.Web.UI.Page
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                divAlertMsg.Visible = false;
                if (!Page.IsPostBack)
                {
                    BindShiftsDropDown();
                    //BindYearDropDown();
                    BindDropDown();
                    Common.addlog("ViewAll", "HR", "All Attendance Report Viewed", "EmployeeAttendance");

                    if (HttpContext.Current.Items["CurrentMonth"] != null)
                    {
                        if(HttpContext.Current.Items["CurrentMonth"].ToString()== "CurrentMonth")
						{
							txtToDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");
							txtFromDate.Value = DateTime.Now.ToString("dd-MMM-yyyy");
						}
						else
						{
                            string sMonth = DateTime.Now.ToString("MMM");
                            string syear = DateTime.Now.ToString("yyyy");
                            string sdate = HttpContext.Current.Items["CurrentMonth"].ToString() + "-" + sMonth + "-" + syear;
                            txtToDate.Value = sdate;
                            txtFromDate.Value = sdate;
                        }
                 
                        
                        GetData();
                    }


                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private void BindDropDown()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);

                ddlDepartment.DataSource = objDB.GetAllDepartmentsByCompanyID(ref errorMsg); ;
                ddlDepartment.DataTextField = "DeptName";
                ddlDepartment.DataValueField = "DeptID";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("ALL", "0"));

                ddlDesignation.DataSource = objDB.GetAllDesignationByCompanyID(ref errorMsg); ;
                ddlDesignation.DataTextField = "DesgTitle";
                ddlDesignation.DataValueField = "DesgID";
                ddlDesignation.DataBind();
                ddlDesignation.Items.Insert(0, new ListItem("ALL", "0"));

                ddlEmployee.DataSource = objDB.GetAllApproveEmployeesByCompanyID(ref errorMsg);
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("ALL", "0"));

                ddlGrades.DataSource = objDB.GetAllGradesByCompanyID(ref errorMsg);
                ddlGrades.DataTextField = "GradeName";
                ddlGrades.DataValueField = "GradeID";
                ddlGrades.DataBind();
                ddlGrades.Items.Insert(0, new ListItem("ALL", "0"));

                ddlLocation.DataSource = objDB.GetAllLocations(ref errorMsg);
                ddlLocation.DataTextField = "NAME";
                ddlLocation.DataValueField = "NAME";
                ddlLocation.DataBind();
                ddlLocation.Items.Insert(0, new ListItem("ALL", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private void BindShiftsDropDown()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                ddlShifts.DataSource = objDB.GetApproveAllWorkingShiftsByCompanyID(ref errorMsg);
                ddlShifts.DataTextField = "ShiftName";
                ddlShifts.DataValueField = "ShiftID";
                ddlShifts.DataBind();
                ddlShifts.Items.Insert(0, new ListItem("ALL", "0"));
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        private void GetData()
        {
            try
            {
                string res = "";
                CheckSessions();
                DataTable dt = new DataTable();

                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.GradeID = Convert.ToInt32(ddlGrades.SelectedValue);
                objDB.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDB.FromDate = txtFromDate.Value;
                objDB.ToDate = txtToDate.Value;

                DateTime Fdate = DateTime.Now, Tdate = DateTime.Now;
                DateTime.TryParse(txtFromDate.Value, out Fdate);
                DateTime.TryParse(txtToDate.Value, out Tdate);
                if (Tdate < Fdate)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be less than To Date";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }
                if (Fdate > DateTime.Now)
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "From Date shoul be Equal or less than Current Date";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }
                if ((Tdate - Fdate).Days > 31 || (Fdate.Day <= Tdate.Day && (Tdate.Month - Fdate.Month) > 0))
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = "date range should be of 1 month";
                    gv.DataSource = "";
                    gv.DataBind();
                    return;
                }
                else
                {
                    objDB.WhereClause = "Employees.DocStatus = 'Approved' And Employees.CompanyID = " + objDB.CompanyID;

                    if (ddlEmployee.SelectedValue != "0" && ddlEmployee.SelectedValue != "")
                    {
                        objDB.WhereClause += " and Employees.EmployeeID = " + ddlEmployee.SelectedValue;
                    }
                    if (ddlGrades.SelectedValue != "0" && ddlGrades.SelectedValue != "")
                    {
                        objDB.WhereClause += " and Employees.GradeID = " + ddlGrades.SelectedValue;
                    }
                    if (txtFromDate.Value != "")
                    {
                        objDB.WhereClause += " and '" + DateTime.Parse(objDB.FromDate).ToString("yyyyMMdd") + "' <= " + "Format(Cast(EmployeeAttendance.AttendanceDate as date),'yyyyMMdd')";
                    }
                    if (txtToDate.Value != "")
                    {
                        objDB.WhereClause += " and '" + DateTime.Parse(objDB.ToDate).ToString("yyyyMMdd") + "' >= " + "Format(Cast(EmployeeAttendance.AttendanceDate as date),'yyyyMMdd')";
                    }
                    if (ddlDepartment.SelectedValue != "0" && ddlDepartment.SelectedValue != "")
                    {
                        objDB.WhereClause += " and Departments.DeptID = " + ddlDepartment.SelectedValue;
                    }
                    if (ddlDesignation.SelectedValue != "0" && ddlDesignation.SelectedValue != "")
                    {
                        objDB.WhereClause += " and Designations.DesgID = " + ddlDesignation.SelectedValue;
                    }
                    if (txtKTId.Text != "" && !string.IsNullOrEmpty(txtKTId.Text))
                    {
                        objDB.WhereClause += " and Employees.WDID = " + txtKTId.Text;
                    }
                    if (ddlLocation.SelectedValue != "0" && ddlLocation.SelectedValue != "")
                    {
                        objDB.WhereClause += " and Employees.PlaceOfPost = '" + ddlLocation.SelectedValue + "'";
                    }

                    dt = objDB.GetAttendanceSheetKT(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            DataView dv = new DataView(dt);
                            dv.Sort = "Employee ASC";
                            gv.DataSource = dv;
                            gv.DataBind();
                    
                            divAlertMsg.Visible = false;
                            gv.UseAccessibleHeader = true;
                            gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                        }
                        else
                        {
                            gv.DataSource = null;
                            gv.DataBind();
                            divAlertMsg.Visible = true;
                            divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                            pAlertMsg.InnerHtml = "No Record Found";
                        }
                    }
                    else
                    {
                        gv.DataSource = null;
                        gv.DataBind();
                        divAlertMsg.Visible = true;
                        divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                        pAlertMsg.InnerHtml = "No Record Found";
                    }
                    Common.addlog("ViewAll", "HR", "All Employee Attendance Viewed", "EmployeeAttendance");
                }


                if (res != "")
                {
                    divAlertMsg.Visible = true;
                    divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                    pAlertMsg.InnerHtml = res;
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }


        protected void btnView_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetData();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private void getHolidaysByCompanyID()
        {
            try
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                dtHolidays = objDB.GetAllHolidaysByCompanyID(ref errorMsg);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }


        }

        private void getEmployeesByShiftID(int ShiftID)
        {
            try
            {
                objDB.ShiftID = Convert.ToInt32(ddlShifts.SelectedValue);
                dtEmployees = objDB.GetAllEmployeesByShiftID(ref errorMsg);
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private static int month = 0;
        private static int year = 0;
        private static int shiftID = 0;

        private static bool chkMonday = true;
        private static bool chkTuesday = true;
        private static bool chkWednesday = true;
        private static bool chkThursday = true;
        private static bool chkFriday = true;
        private static bool chkSaturday = true;
        private static bool chkSunday = true;

        private static DataTable dtHolidays;
        private static DataTable dtEmployees;
        private static DataTable dtMonthlyAttendanceSheet;


        private void getWorkingDaysByShiftID(int shiftID)
        {
            try
            {
                DataTable dtShifts = new DataTable();
                objDB.WorkingShiftID = shiftID;
                dtShifts = objDB.GetWorkingShiftByID(ref errorMsg);

                if (dtShifts != null)
                {
                    if (dtShifts.Rows.Count > 0)
                    {
                        chkMonday = getCheckBoxValue(dtShifts.Rows[0]["Monday"].ToString());
                        chkTuesday = getCheckBoxValue(dtShifts.Rows[0]["Tuesday"].ToString());
                        chkWednesday = getCheckBoxValue(dtShifts.Rows[0]["Wednesday"].ToString());
                        chkThursday = getCheckBoxValue(dtShifts.Rows[0]["Thursday"].ToString());
                        chkFriday = getCheckBoxValue(dtShifts.Rows[0]["Friday"].ToString());
                        chkSaturday = getCheckBoxValue(dtShifts.Rows[0]["Saturday"].ToString());
                        chkSunday = getCheckBoxValue(dtShifts.Rows[0]["Sunday"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }
        private DataTable createMonthSheet(int month, int year, int shiftID)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Sr. No");
                dt.Columns.Add("Employee Name");
                dt.Columns.Add("Present Days");
                dt.Columns.Add("Absent Days");

                int days = DateTime.DaysInMonth(year, month);
                for (int day = 1; day <= days; day++)
                {
                    dt.Columns.Add(day.ToString());
                }


                dt.AcceptChanges();

                dt.AcceptChanges();

                return dt;
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                return null;
            }


        }

        protected void BindCalenderTable(DataTable dt)
        {
            try
            {
                gv.DataSource = dt;
                gv.DataBind();

                if (gv.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        private bool getCheckBoxValue(string val)
        {
            if (val == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
            else if (!objDB.CheckModuleAccessLevel("Human Resource", Convert.ToInt32(Session["UserID"])))
                Response.Redirect("/login");
        }

        private void addEmployeeAttendanceRows()
        {
            try
            {
                if (dtEmployees != null)
                {
                    if (dtEmployees.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEmployees.Rows.Count; i++)
                        {
                            DataRow dr = dtMonthlyAttendanceSheet.NewRow();
                            dr[0] = i + 1;
                            dr[1] = dtEmployees.Rows[i]["EmployeeName"].ToString();
                            int days = DateTime.DaysInMonth(year, month);
                            int presentCount = 0;
                            int absentCount = 0;

                            for (int day = 1; day <= days; day++)
                            {
                                DateTime cellHeaderDate = new DateTime(year, month, day);
                                DateTime today = DateTime.Now;

                                if (cellHeaderDate < today)
                                {
                                    objDB.EmployeeID = Convert.ToInt32(dtEmployees.Rows[i]["EmployeeID"].ToString());
                                    objDB.date = new DateTime(year, month, day).ToString("dd-MMM-yyyy");
                                    string chk = objDB.CheckEmployeeAttendanceByDate();

                                    if (chk == "P")
                                    {
                                        presentCount++;
                                        dr[day + 3] = "P";
                                    }
                                    else if (chk == "A")
                                    {
                                        bool isHoliday = false;
                                        bool isNonWorkingDay = false;

                                        if (dtHolidays != null)
                                        {
                                            if (dtHolidays.Rows.Count > 0)
                                            {
                                                for (int j = 0; j < dtHolidays.Rows.Count; j++)
                                                {
                                                    DateTime currDate = new DateTime(year, month, day);
                                                    DateTime holidayStartDate = DateTime.Parse(dtHolidays.Rows[j]["StartDate"].ToString());
                                                    DateTime holidayEndDate = DateTime.Parse(dtHolidays.Rows[j]["EndDate"].ToString());

                                                    if (currDate >= holidayStartDate && currDate <= holidayEndDate)
                                                    {
                                                        isHoliday = true;
                                                    }
                                                }
                                            }
                                        }

                                        string dayName = new DateTime(year, month, day).DayOfWeek.ToString();
                                        if (dayName == "Monday" && chkMonday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Monday" && chkMonday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Tuesday" && chkTuesday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Wednesday" && chkWednesday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Thursday" && chkThursday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Friday" && chkFriday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Saturday" && chkSaturday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Sunday" && chkSunday == false)
                                            isNonWorkingDay = true;

                                        if (!isHoliday && !isNonWorkingDay)
                                        {
                                            absentCount++;
                                            dr[day + 3] = "A";
                                        }
                                    }
                                    else if (chk == "L")
                                    {
                                        bool isHoliday = false;
                                        bool isNonWorkingDay = false;

                                        if (dtHolidays != null)
                                        {
                                            if (dtHolidays.Rows.Count > 0)
                                            {
                                                for (int j = 0; j < dtHolidays.Rows.Count; j++)
                                                {
                                                    DateTime currDate = new DateTime(year, month, day);
                                                    DateTime holidayStartDate = DateTime.Parse(dtHolidays.Rows[j]["StartDate"].ToString());
                                                    DateTime holidayEndDate = DateTime.Parse(dtHolidays.Rows[j]["EndDate"].ToString());

                                                    if (currDate >= holidayStartDate && currDate <= holidayEndDate)
                                                    {
                                                        isHoliday = true;
                                                    }
                                                }
                                            }
                                        }

                                        string dayName = new DateTime(year, month, day).DayOfWeek.ToString();
                                        if (dayName == "Monday" && chkMonday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Monday" && chkMonday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Tuesday" && chkTuesday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Wednesday" && chkWednesday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Thursday" && chkThursday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Friday" && chkFriday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Saturday" && chkSaturday == false)
                                            isNonWorkingDay = true;
                                        else if (dayName == "Sunday" && chkSunday == false)
                                            isNonWorkingDay = true;

                                        if (!isHoliday && !isNonWorkingDay)
                                        {
                                            dr[day + 3] = "L";
                                        }
                                    }
                                }
                            }

                            dr[2] = presentCount;
                            dr[3] = absentCount;

                            dtMonthlyAttendanceSheet.Rows.Add(dr);
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    foreach (DataControlFieldCell cell in e.Row.Cells)
                    {
                        if (cell.ContainingField is BoundField)
                        {
                            int chk;
                            if (int.TryParse(((BoundField)cell.ContainingField).HeaderText, out chk))
                            {
                                DateTime cellHeaderDate = new DateTime(year, month, Convert.ToInt32(((BoundField)cell.ContainingField).HeaderText));
                                DateTime today = DateTime.Now;

                                if (cellHeaderDate > today)
                                    setColBackColorbyName(e.Row, ((BoundField)cell.ContainingField).HeaderText, System.Drawing.Color.FromArgb(1, 231, 234, 237));

                                string dayName = new DateTime(year, month, Convert.ToInt32(((BoundField)cell.ContainingField).HeaderText)).DayOfWeek.ToString();
                                if (dayName == "Monday" && chkMonday == false)
                                    setColBackColorbyName(e.Row, ((BoundField)cell.ContainingField).HeaderText, System.Drawing.Color.FromArgb(1, 188, 188, 188));
                                else if (dayName == "Tuesday" && chkTuesday == false)
                                    setColBackColorbyName(e.Row, ((BoundField)cell.ContainingField).HeaderText, System.Drawing.Color.FromArgb(1, 188, 188, 188));
                                else if (dayName == "Wednesday" && chkWednesday == false)
                                    setColBackColorbyName(e.Row, ((BoundField)cell.ContainingField).HeaderText, System.Drawing.Color.FromArgb(1, 188, 188, 188));
                                else if (dayName == "Thursday" && chkThursday == false)
                                    setColBackColorbyName(e.Row, ((BoundField)cell.ContainingField).HeaderText, System.Drawing.Color.FromArgb(1, 188, 188, 188));
                                else if (dayName == "Friday" && chkFriday == false)
                                    setColBackColorbyName(e.Row, ((BoundField)cell.ContainingField).HeaderText, System.Drawing.Color.FromArgb(1, 188, 188, 188));
                                else if (dayName == "Saturday" && chkSaturday == false)
                                    setColBackColorbyName(e.Row, ((BoundField)cell.ContainingField).HeaderText, System.Drawing.Color.FromArgb(1, 188, 188, 188));
                                else if (dayName == "Sunday" && chkSunday == false)
                                    setColBackColorbyName(e.Row, ((BoundField)cell.ContainingField).HeaderText, System.Drawing.Color.FromArgb(1, 188, 188, 188));

                                if (dtHolidays != null)
                                {
                                    if (dtHolidays.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtHolidays.Rows.Count; i++)
                                        {
                                            DateTime currDate = new DateTime(year, month, Convert.ToInt32(((BoundField)cell.ContainingField).HeaderText));
                                            DateTime holidayStartDate = DateTime.Parse(dtHolidays.Rows[i]["StartDate"].ToString());
                                            DateTime holidayEndDate = DateTime.Parse(dtHolidays.Rows[i]["EndDate"].ToString());

                                            if (currDate >= holidayStartDate && currDate <= holidayEndDate)
                                                setColBackColorbyName(e.Row, ((BoundField)cell.ContainingField).HeaderText, System.Drawing.Color.FromArgb(1, 188, 188, 188));
                                        }
                                    }
                                }

                                if (cell.Text == "P")
                                {
                                    cell.ForeColor = System.Drawing.Color.White;
                                    cell.BackColor = System.Drawing.Color.FromArgb(1, 32, 113, 39);
                                }
                                else if (cell.Text == "A")
                                {
                                    cell.ForeColor = System.Drawing.Color.White;
                                    cell.BackColor = System.Drawing.Color.FromArgb(1, 148, 29, 19);
                                }
                                else if (cell.Text == "L")
                                {
                                    cell.ForeColor = System.Drawing.Color.White;
                                    cell.BackColor = System.Drawing.Color.FromArgb(1, 244, 164, 6);
                                }

                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        void setColBackColorbyName(GridViewRow row, string columnName, System.Drawing.Color color)
        {
            try
            {

                int columnIndex = 0;
                foreach (DataControlFieldCell cell in row.Cells)
                {
                    if (cell.ContainingField is BoundField)
                        if (((BoundField)cell.ContainingField).HeaderText.Equals(columnName))
                        {
                            cell.ContainingField.HeaderStyle.BackColor = color;
                            cell.ContainingField.ItemStyle.BackColor = color;
                        }
                    columnIndex++;
                }
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckSessions();
                if (ddlMonth.SelectedIndex != 0 && ddlYear.SelectedIndex != 0 && ddlShifts.SelectedIndex != 0)
                {
                    month = Convert.ToInt32(ddlMonth.SelectedValue);
                    year = Convert.ToInt32(ddlYear.SelectedValue);
                    shiftID = Convert.ToInt32(ddlShifts.SelectedValue);

                    getEmployeesByShiftID(shiftID);
                    getHolidaysByCompanyID();
                    getWorkingDaysByShiftID(shiftID);
                    dtMonthlyAttendanceSheet = null;
                    dtMonthlyAttendanceSheet = createMonthSheet(month, year, shiftID);
                    addEmployeeAttendanceRows();
                    BindCalenderTable(dtMonthlyAttendanceSheet);
                }

            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
            }

        }

        protected void btnPDF_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string header = "";
                string content = "";
                string footer = "";

                DataTable dt = new DataTable();
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DocType = "Attendance Sheet";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        header = dt.Rows[0]["DocHeader"].ToString();
                        content = dt.Rows[0]["DocContent"].ToString();
                        footer = dt.Rows[0]["DocFooter"].ToString();
                    }
                }

                content = content.Replace("##DATE##", DateTime.Now.ToString("dd-MMM-yyyy"));
                content = content.Replace("##MONTH##", DateTime.Parse(txtFromDate.Value).ToString("MMM"));
                content = content.Replace("##YEAR##", DateTime.Parse(txtFromDate.Value).ToString("yyyy"));
                content = content.Replace("##SHIFT##", "");
                content = content.Replace("##TABLE##", GetTemplate(gv));
                Common.addlog("Report", "HR", "Attendance Sheet Report Generated", "EmployeeAttendance");

                Common.generatePDF(header, footer, content, "Attendance Sheet - " + " " + " (" + txtFromDate.Value + ")", "A4", "Portrait");


            }
            catch (Exception ex)
            {
                //divAlertMsg.Visible = true;
                //divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                //pAlertMsg.InnerHtml = ex.Message;
            }
        }

        private string GetTemplate(GridView gd)
        {
            try
            {

                StringBuilder sheetBody = new StringBuilder();
                StringWriter sw = new StringWriter(sheetBody);
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                gd.RenderControl(hw);
                return sheetBody.ToString();
            }
            catch (Exception ex)
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = ex.Message;
                return "";
            }

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
}