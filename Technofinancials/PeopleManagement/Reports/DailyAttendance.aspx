﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DailyAttendance.aspx.cs" Inherits="Technofinancials.PeopleManagement.Reports.DailyAttendance" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            	<%-- <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="btnUpdPnl" runat="server">
			   <ProgressTemplate>
			   <div class="upd_panel" >
				   <div class="center">
					   <img src="/assets/images/Loading.gif" />
				   </div>
				   
			   
			   </div>
			   </ProgressTemplate>
		   </asp:UpdateProgress>--%>
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
           
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <img src="/assets/images/Attendance_inner__03.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Daily Attendance Sheet</h3>
                        </div>

                        <div class="col-sm-4" style="margin-top: 20px;">
                            <div style="text-align: right;">
                                   <button class="tf-pdf-btn" "Generate Report" id="btnReport" runat="server" onserverclick="btnReport_ServerClick" type="button"><i class="fa fa-file-pdf-o"></i></button>

                                <%--<a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/manage/attendance/leaves"); %>" class="tf-add-btn" "Add"><i class="fa fa-plus"></i></a>--%>
                                <%--<a class="tf-back-btn" "Back" href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/holidays"); %>"><i class="fas fa-arrow-left"></i></a>--%>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                        <div class="tab-content ">
                            <div class="tab-pane active row">

                                <div class="col-sm-12">
                                   <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                
                                <div class="col-sm-4">
                                    <div class="form-group">
                                
                                                        <h4>Date  <span style="color: red !important;">*</span>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtPayrollDate" ErrorMessage="*Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" /></h4>
                                                        <%--<input class="form-control" id="txtPayrollDate" data-plugin="datetimepicker" data-date-format="MMM-YYYY" placeholder="Grant Date" type="text" runat="server"   onserverchange="txtPayrollDate_ServerChange"  />--%>
                                                         <asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY"    />

                                    </div>
                                </div>
                          
                                          <div class="col-md-3">
                                                <h4>&nbsp;</h4>
                                                <button class="tf-view-btn" "View" id="btnView" runat="server" onserverclick="btnView_ServerClick" type="button"><i class="far fa-eye"></i></button>
                                    </div>
                                 
                                    </div>
                    
                        </div>
                    </div>
                                </div>
                                
                    <div class="cleafix">&nbsp;</div>
                    <div class="cleafix">&nbsp;</div>
                                 <asp:UpdatePanel ID="UpdPnl" runat="server">
                                <ContentTemplate>
                                <div class="col-sm-12 gv-overflow-scrool">

                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                           <asp:TemplateField HeaderText="Employee Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("EmployeeCode") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Employee Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                 
                                             <asp:TemplateField HeaderText="Department Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol2" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Attendance">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol3" Text='<%# Eval("Resultant") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Late In">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol4" Text='<%# Eval("LateIn") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Half Day">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol5" Text='<%# Eval("HalfDay") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Early Out">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol6" Text='<%# Eval("EarlyOut") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Over Time">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol7" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="On Leave">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol8" Text='<%# Eval("OnLeave") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            
                                             
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>

                            </div>
                        </div>
                            </div>
                    </div>

                      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="form-group" id="divAlertMsg" runat="server">
                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                    <span>
                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                    </span>
                                    <p id="pAlertMsg" runat="server">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                                    </ContentTemplate>
                             </asp:UpdatePanel>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
         

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757 !important;
                border-radius: 0px !important;
                box-shadow: 0 8px 6px -5px #cacaca !important;
                height: 38px !important;
                width: 37px !important;
                display: inline-block !important;
                padding: 5px !important;
                position: relative !important;
                text-align: center !important;
            }
        </style>
    </form>
</body>
</html>
