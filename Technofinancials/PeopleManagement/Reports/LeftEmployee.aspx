﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeftEmployee.aspx.cs" Inherits="Technofinancials.PeopleManagement.Reports.LeftEmployee" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <style>
        .sick {
            color: #a200ff;
            font-weight: bold;
        }

        button#btnView {
            padding: 3px 24px;
            margin-top: 14px;
        }

        .mater {
            font-weight: bold;
            color: #0090ff;
        }

        .year {
            font-weight: bold;
            color: black;
        }

        .casual {
            color: #0039f1;
            font-weight: bold;
        }

        .abs {
            color: red;
            font-weight: bold;
        }

        .pres {
            color: green;
            font-weight: bold;
        }

        .tf-note-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

        div#gv {
            margin-bottom: 20%;
        }

        .tf-note-btn i {
            color: #fff !important;
        }

        .tf-disapproved-btn {
            background-color: #575757;
            padding: 10px 8px 7px 10px;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-disapproved-btn i {
                color: #fff !important;
            }

        .tf-add-btn {
            background-color: #575757;
            padding: 12px 10px 8px 10px !important;
            border-radius: 100px;
            border: none !important;
            color: #fff;
        }

            .tf-add-btn i {
                color: #fff !important;
            }



        th.sorting {
            font-size: 12px !important;
            padding-left: 5px !important;
            padding-right: 5px !important;
        }

        .content-header.second_heading .container-fluid {
            padding-left: 0px;
        }

        .content-header.second_heading h1 {
            margin-left: 0px !important;
        }
        /*td{
                text-align: center;
            }*/
    </style>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <h1 class="m-0 text-dark">Left Employees</h1>
                            </div>
                            <!-- /.col -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>From Date
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtFromDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtFromDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>To Date
                                 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtToDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnView" ForeColor="Red">*</asp:RequiredFieldValidator></h4>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtToDate" data-plugin="datetimepicker" data-date-format="DD-MMM-YYYY" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee Name</h4>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Employee Code</h4>
                                        <asp:TextBox ID="txtKTId" runat="server" class="form-control">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Department Name</h4>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" onchange="GetDesignationsByDepartmentIdAndCompanyId()" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Designation Name</h4>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" onchange="GetEmployeesByDesignationIdAndDepartmentId()" class="form-control select2" data-plugin="select2" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <h4>Location</h4>
                                        <asp:DropDownList ID="ddlLocation" runat="server" class="form-control select2 ">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="clear-fix">&nbsp;</div>
                                    <div>
                                        <button class="AD_btn_inn" id="btnView" runat="server" validationgroup="btnView" onserverclick="btnView_ServerClick" type="button">View</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="col-sm-12">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group" id="divAlertMsg" runat="server">
                                            <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                <span>
                                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                </span>
                                                <p id="pAlertMsg" runat="server">
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="clear-fix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">


                            <div class="row">
                                <div class="col-sm-12 gv-overflow-scrool">
                                    <asp:GridView ID="gv" runat="server" CssClass="table table-bordered gv" EmptyDataText="No Records Found" ClientIDMode="Static" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr. No">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                            <asp:TemplateField HeaderText="Employee Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("WorkDayID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date Of Separation">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# ((DateTime)Eval("SeperationDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cost Center">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("CostCenterName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Designation">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("DesgTitle") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Department">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCol1" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear-fix">&nbsp;</div>
                    <div class="clear-fix">&nbsp;</div>
                    <div class="clear-fix">&nbsp;</div>
                    <div class="clear-fix">&nbsp;</div>

                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>


    </form>
</body>
</html>
