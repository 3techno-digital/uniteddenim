﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PFReport.aspx.cs" Inherits="Technofinancials.PeopleManagement.Reports.PFReport" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>
<a href="Loans.aspx">Loans.aspx</a>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <input type="hidden" id="hdnCompanyName" value="<% Response.Write(Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower()); %>" />
            <div class="wrap">
                <section class="app-content">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <img src="/assets/images/loan.png" class="img-responsive tf-page-heading-img" />
                            <h3 class="tf-page-heading-text">Provident Fund</h3>
                        </div>

                        <div class="col-sm-4" style="margin-top: 20px;">
                            <div style="text-align: right;">
                                <a href="#" class="tf-add-btn" "Add"><i class="fa fa-file-pdf-o"></i></a>
                                
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-12">
                            <div class="tab-content ">
                                <div class="tab-pane active row">
                                    <div class="col-sm-12 gv-overflow-scrool">


                                        <table class="table table-bordered gv">
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>Employee Name</th>
                                                <th>Employee PF</th>
                                                <th>Company Ratio</th>
                                                <th>Total</th>
                                            </tr>

                                            <tr>
                                                <td>1</td>
                                                <td>Imranm Khan</td>
                                                <td>99,600‬</td>
                                                <td>99,600‬</td>
                                                <td>199,200</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Sadaqt Ali</td>
                                                <td>29,880‬‬</td>
                                                <td>29,880‬‬</td>
                                                <td>59,760‬</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Mukesh Kumar</td>
                                                <td>19,920‬</td>
                                                <td>19,920‬‬‬</td>
                                                <td>39,840‬‬</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Kawish</td>
                                                <td>67,230‬</td>
                                                <td>67,230‬</td>
                                                <td>134,460‬</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>Qasim</td>
                                                <td>52,290‬</td>
                                                <td>52,290‬‬</td>
                                                <td>104,580</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>Raheel</td>
                                                <td>6,225‬</td>
                                                <td>6,225‬‬</td>
                                                <td>12,450‬</td>

                                            </tr>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <script src="/business/scripts/ViewDesignations.js"></script>

        <style>
            .tf-note-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-note-btn i {
                    color: #fff !important;
                }

            .tf-disapproved-btn {
                background-color: #575757;
                padding: 10px 8px 7px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-disapproved-btn i {
                    color: #fff !important;
                }


            .tf-add-btn {
                background-color: #575757;
                padding: 4px 9px 4px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-add-btn {
                background-color: #575757;
                padding: 12px 10px 8px 10px !important;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-add-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }

            .tf-back-btn {
                background-color: #575757 !important;
                border-radius: 0px !important;
                box-shadow: 0 8px 6px -5px #cacaca !important;
                height: 38px !important;
                width: 37px !important;
                display: inline-block !important;
                padding: 5px !important;
                position: relative !important;
                text-align: center !important;
            }
        </style>
    </form>
</body>
</html>
