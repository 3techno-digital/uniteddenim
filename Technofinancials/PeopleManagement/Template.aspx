﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Template.aspx.cs" Inherits="Technofinancials.People_Management.Dashboard" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/PeopleManagement/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">
            <div class="wrap">
                <section class="app-content">
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button class="tf-approved-btn" "Approved"></button>
                            <button class="tf-back-btn" "Back"></button>
                            <button class="tf-disapproved-btn" "Dispproved"></button>
                            <button class="tf-excel-btn" "Excel"></button>
                            <button class="tf-exit-btn" "Exit"></button>
                            <button class="tf-note-btn" "Note"></button>
                            <button class="tf-pdf-btn" "PDF"></button>
                            <button class="tf-print-btn" "Print"></button>
                            <button class="tf-review-btn" "Review"></button>
                            <button class="tf-save-btn" "Save"></button>
                            <button class="tf-send-btn" "Send"></button>
                            <button class="tf-upload-btn" "Approved"></button>
                            <button class="tf-view-btn" "Approved"></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <button class="tf-back-btn" "Back"><i class="fas fa-arrow-left"></i></button>
                            <button class="tf-disapproved-btn" "Dispproved"><i class="far fa-thumbs-down"></i></button>
                           <%-- <button class="tf-excel-btn" "Excel"></button>--%>
                            <button class="tf-note-btn" "Note"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            <button class="tf-pdf-btn" "PDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                            <button class="tf-print-btn" "Print"><i class="fa fa-print" aria-hidden="true"></i></button>
                            <button class="tf-save-btn" "Save"><i class="far fa-save"></i></button>
                            <button class="tf-send-btn" "Send"><i class="fas fa-paper-plane"></i></button>
                            <button class="tf-upload-btn" "Upload"><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                            <button class="tf-view-btn" "View"><i class="far fa-eye"></i></button>
                            <button class="tf-add-btn" "Add"><i class="far fa-plus-circle"></i></button>
                            <button class="tf-download-btn" "Download"><i class="fa fa-cloud-download" aria-hidden="true"></i></button>
                            <button class="tf-next-btn" "Download"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                            <button class="tf-exit-btn" "Exit"><i class="fa fa-sign-out" aria-hidden="true"></i></button>
                            

                            <button class="tf-approved-btn" "Approved"><i class="far fa-thumbs-up"></i></button>
                             <button class="tf-review-btn" "Review"><i class="fas fa-check-square"></i></button>
                         
                            
                            
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <input class="form-control" placeholder="First Name" type="text">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <input class="form-control" placeholder="Last Name" type="text">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <select class="form-control">
                                <option>sarfraz@3techno.com</option>
                                <option>sarfraz@3techno.com</option>
                                <option>sarfraz@3techno.com</option>
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <select class="form-control">
                                <option>sarfraz@3techno.com</option>
                                <option>sarfraz@3techno.com</option>
                                <option>sarfraz@3techno.com</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="alert tf-alert-danger">
                                <span>
                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                </span>
                                <p>
                                    Techno Financial applies only one rule per transaction and you prioritise the rules
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="alert tf-alert-success">
                                <span>
                                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                </span>
                                <p>
                                    Techno Financial applies only one rule per transaction and you prioritise the rules
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="radio radio-success">
                                <input type="radio" name="chk">
                                <label>Yes</label>
                            </div>
                            <div class="radio radio-danger">
                                <input id="radio-danger" name="chk" type="radio">
                                <label>No</label>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
</body>
</html>
