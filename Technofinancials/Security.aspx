﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Security.aspx.cs" Inherits="Technofinancials.Security1" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%--<%@ Register Src="~/usercontrols/Footer2.ascx" TagName="Footer" TagPrefix="uc" %>--%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />

</head>
<body class="slider-body">
    <form id="form1" runat="server">

                       <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="header-bg-div">
                        <div class="header-name-div">
                            <h2 class="header-name-slide">Security Policies &amp; Infrastructure</h2>
                            <h4 class="header-name-slide" style="font-size:14px; padding-top:0;">Last Updated: March 12, 2019</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="terms-of-use-main-div">
                        <br />

                        <h3>Security Policies and Infrastructure</h3>
                        <p>3Techno Digital Private Limited (hereinafter referred to as ‘3Techno’) professional staff and automated tools monitor service performance for problems 24 hours a day, 7 days a week. </p>
                        <p>We rely on advanced, industry-recognized security safeguards to keep all of your financial data private and protected. Techno Financials is a DigiCert Secured product. You can find the seal and validity of our license by clicking on the verification and validation seal on our website. DigiCert® is the leading secure sockets layer (SSL) Certificate Authority. With password-protected login, firewall protected servers and the same encryption technology used by the world's top organizations. 3techno Digital Pvt Ltd. provides for the security of their users by enabling the encryption of data transmitted between 3techno Digital Pvt Ltd. and your browser during an SSL/TLS encrypted session (look for the padlock).</p>
                        <p>We provide automatic offsite storage without extra cost at secured, industry-recognized cloud hosting and at physical locations. The data is completely encrypted and secured as we have a defined Web Application Firewall (WAF) and Content Delivery Network (CDN). This also comes with monitoring which will check Techno Financials website twice a day (every 12 hours) for malicious content such as backdoor scripts on your website, trojans, etc. And should the unexpected or an anomaly ever happen to your system, all of your data will still be instantly accessible to you from any computer connected to the internet. </p>
                        <p>As per our governance structure, we do not sell, rent or share your information with third parties for their promotional use. For full disclosure of our privacy practices, please read our Privacy Policy. </p>
                        <p>Techno Financials has been available more than 99% of the time since its inception in 2017. This is because we have a cloud-based services arrangement with renowned cloud-based service providers. So even if one server becomes impacted or unavailable, your service will probably not be affected. This means that you can access your data online no matter where you are or what you are doing. </p>
                        <p>Our 3-layered control (3LC) mechanism helps you to invite new users in Techno Financials where they must create a unique password with 2-Factor Authentication along with secret questions. Further, our 3LC offers multiple permission levels that let you limit the access privileges of each user.</p>
                        <p>Techno Financials offers unique Audit Trail features, which cannot be turned off by a user. They record every login to the service and any changes made to every financial transaction. </p>
                        <span class="spanHeading">Other Securities</span>
                        <ul>
                            <li>Our 3rd party centers that host your data are guarded seven days a week, 24 hours a day, each and every day of the year by private security guards. Each data center is monitored 7x24x365 with night vision cameras. 3Techno servers are located inside generic-looking, undisclosed locations that make them less likely to be a target of an attack. Audits are regularly performed and the whole process is reviewed by management.</li>
                            <li>Accessing data center information as well as customer data is done on an as-needed only basis, and only when approved by the customer (i.e. as part of a support incident), or by senior security management to provide support and maintenance.</li>
                            <li>Customer data is mirrored in a separate geographic location for Disaster Recovery and Business Continuity purposes. We have a 3 layered data backup for DRP and BCP. For more details please contact us at techsupport@3techno.com.</li>
                        </ul>

<%--                        <div class="col-md-6 col-md-offset-3">
                            <!-- Begin DigiCert site seal HTML and JavaScript -->
                            <div id="DigiCertClickID_BkDeAnmk" data-language="en">
                            </div>
                            <script type="text/javascript">
                                var __dcid = __dcid || []; __dcid.push(["DigiCertClickID_BkDeAnmk", "15", "s", "black", "BkDeAnmk"]); (function () { var cid = document.createElement("script"); cid.async = true; cid.src = "//seal.digicert.com/seals/cascade/seal.min.js"; var s = document.getElementsByTagName("script"); var ls = s[(s.length - 1)]; ls.parentNode.insertBefore(cid, ls.nextSibling); }());
                            </script>
                            <!-- End DigiCert site seal HTML and JavaScript -->
                        </div>--%>

                    </div>
                </div>
            </div>
        </div>
                                                     <div class="clearfix">&nbsp;</div>
          <div class="clearfix">&nbsp;</div>
          <div class="clearfix">&nbsp;</div>
<br />
                <br />
                           <br />
    </section>

    <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-5">
                    <p>© 2018-2020 TechnoFinancials. All rights reserved.</p>
                </div>
                <div class="col-sm-4 d-flex justify-content-center p-0">
                    <ul class="<%--list-group list-group-horizontal fotor_ul--%> fotor_ul list-inline">
                        <li><a href="/privacy-policy">Policy</a></li>
                        <li><a href="/security">Security</a></li>
                        <li><a href="/terms-of-use">Terms of Service</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
<%--                    <div class="copyright pull-right">
                        <a href="http://3techno.com" target="_blank" class="tf-footer-name tf-font-times-new-roman">A Product of
                    <img src="/assets/images/footer_logo.png" class="tf-footer-logo">
                            3techno</a>
                    </div>--%>
                </div>
            </div>
        </div>
    </footer>




     <%--   <section>
            <div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">
                <div class="row" style="margin-left: 0px; margin-right: 0px;">
                    <div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
                        <div class="header-bg-div">
                            <div class="header-name-div">
                                <h2 class="header-name-slide">Security Policies &amp; Infrastructure</h2>
                                <h4  class="header-name-slide" style="font-size:15px;">Last Updated: March 12, 2019</h4>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row" style="margin-left: 0px; margin-right: 0px;">
                    <div class="col-sm-12">
                        <div class="terms-of-use-main-div">

                            <h3>Security Policies and Infrastructure</h3>
                            <p>3Techno Digital Private Limited (hereinafter referred to as ‘3Techno’) professional staff and automated tools monitor service performance for problems 24 hours a day, 7 days a week. </p>
                            <p>We rely on advanced, industry-recognized security safeguards to keep all of your financial data private and protected. Techno Financials is a DigiCert Secured product. You can find the seal and validity of our license by clicking on the verification and validation seal on our website. DigiCert® is the leading secure sockets layer (SSL) Certificate Authority. With password-protected login, firewall protected servers and the same encryption technology used by the world's top organizations. 3techno Digital Pvt Ltd. provides for the security of their users by enabling the encryption of data transmitted between 3techno Digital Pvt Ltd. and your browser during an SSL/TLS encrypted session (look for the padlock).</p>
                            <p>We provide automatic offsite storage without extra cost at secured, industry-recognized cloud hosting and at physical locations. The data is completely encrypted and secured as we have a defined Web Application Firewall (WAF) and Content Delivery Network (CDN). This also comes with monitoring which will check Techno Financials website twice a day (every 12 hours) for malicious content such as backdoor scripts on your website, trojans, etc. And should the unexpected or an anomaly ever happen to your system, all of your data will still be instantly accessible to you from any computer connected to the internet. </p>
                            <p>As per our governance structure, we do not sell, rent or share your information with third parties for their promotional use. For full disclosure of our privacy practices, please read our Privacy Policy. </p>
                            <p>Techno Financials has been available more than 99% of the time since its inception in 2017. This is because we have a cloud-based services arrangement with renowned cloud-based service providers. So even if one server becomes impacted or unavailable, your service will probably not be affected. This means that you can access your data online no matter where you are or what you are doing. </p>
                            <p>Our 3-layered control (3LC) mechanism helps you to invite new users in Techno Financials where they must create a unique password with 2-Factor Authentication along with secret questions. Further, our 3LC offers multiple permission levels that let you limit the access privileges of each user.</p>
                            <p>Techno Financials offers unique Audit Trail features, which cannot be turned off by a user. They record every login to the service and any changes made to every financial transaction. </p>
                            <span class="spanHeading">Other Securities</span>
                            <ul>
                                <li>Our 3rd party centers that host your data are guarded seven days a week, 24 hours a day, each and every day of the year by private security guards. Each data center is monitored 7x24x365 with night vision cameras. 3Techno servers are located inside generic-looking, undisclosed locations that make them less likely to be a target of an attack. Audits are regularly performed and the whole process is reviewed by management.</li>
                                <li>Accessing data center information as well as customer data is done on an as-needed only basis, and only when approved by the customer (i.e. as part of a support incident), or by senior security management to provide support and maintenance.</li>
                                <li>Customer data is mirrored in a separate geographic location for Disaster Recovery and Business Continuity purposes. We have a 3 layered data backup for DRP and BCP. For more details please contact us at techsupport@3techno.com.</li>
                            </ul>

                            <div class="col-md-6 col-md-offset-3">
                               

                                <div id="DigiCertClickID_BkDeAnmk" data-language="en">
                                </div>
                                <script type="text/javascript">
                                    var __dcid = __dcid || []; __dcid.push(["DigiCertClickID_BkDeAnmk", "15", "s", "black", "BkDeAnmk"]); (function () { var cid = document.createElement("script"); cid.async = true; cid.src = "//seal.digicert.com/seals/cascade/seal.min.js"; var s = document.getElementsByTagName("script"); var ls = s[(s.length - 1)]; ls.parentNode.insertBefore(cid, ls.nextSibling); }());
                                </script>
                               

                            </div>

                        </div>
                    </div>
                </div>


            </div>

        </section>--%>
         <%--<uc:Footer ID="Footer1" runat="server"></uc:Footer>--%>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>

    <style>

        ul{
            list-style-type: disc !important;
            padding: 0px 35px !important;
            margin-top:0px !important;
            margin-bottom:10px !important;
        }
          .terms-of-use-main-div p {
    font-size: 14px;
    text-align: justify;
}
        .spanHeading {
            /*font-weight: bold;
            color: #6a6c6f;
            font-size: 21px;*/
            font-weight: bold;
    color: rgb(0, 55, 128);
    font-size: 18px;
    padding: 5px 0px;
        }
        .terms-of-use-main-div{
    margin:auto 60px !important;
    padding-right:20px;
}
        .container-fluid{
            padding-left:0px !important; 
        }
 .main-footer {
    color: #000 !important;
    font-size: 14px;
    padding: 10px 30px;
    box-shadow: inset 0px 0 9px 0px #c3c1c1;
    border-top: 1px solid;
    position: fixed;
    width: 100%;
    bottom: 0;
    background: #fff;
    right: 0;
    transition: all 0.3s;
    z-index: 1;
}
        .main-footer ul {
    list-style-type: none;
    margin-bottom: 0px !important;
}
        .main-footer ul li {
    padding: 0 10px;
}
        .main-footer ul li a {
    font-family: Noto-Regular;
    color: inherit;
    text-decoration: none;
    transition: all 0.3s;
    outline: none !important;
}
    </style>
</body>
</html>
