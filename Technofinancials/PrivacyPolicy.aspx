﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.cs" Inherits="Technofinancials.PrivacyPolicy" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<%--<%@ Register Src="~/usercontrols/Footer2.ascx" TagName="Footer" TagPrefix="uc" %>--%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />

</head>
<body class="slider-body">
    <form id="form1" runat="server">
        <section>
            <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="header-bg-div">
                        <div class="header-name-div">
                            <h2 class="header-name-slide">Privacy Policy</h2>
                            <h4 class="header-name-slide" style="font-size:14px; padding-top:0;">Last Updated: March 12, 2019</h4>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="terms-of-use-main-div">
                        <br>
                        <h3>INTRODUCTION</h3>
                        <p>
                            This 3Techno Digital Private Limited’s (hereinafter referred to as ‘3Techno’) Privacy Policy delivers info use, collection; processing and sharing of personal information in connection with your utilization of 3Techno social media pages, websites and mobile applications that link to this Privacy Policy, in the context of other offline sales and marketing activities, and your interactions with 3Techno during in-person meetings or at 3Techno events.
                        </p>
                        <p>The most up-to-date version of the Privacy Policy can be found on 3Techno website. This Privacy Policy can change over time, for example to comply with legal requirements or to meet changing business needs. We will inform you in case there is a change in the Privacy Policy via notice or statement of changes on our website.</p>
                        <p>
                            ‘Personal Information’ or ‘personal data’ includes your name, address, email address, business contact details, or information gathered through your interactions with us via our websites or at events. This information relates to an identified individual or to an identifiable individual.
                        </p>
                        <h3>SCOPE</h3>

                        This Privacy Policy applies to the processing of personal information by 3Techno of:
                        <ul>
                            <li>Our social media pages that link to this Privacy Policy, (collectively referred to as the sites) users and visitors of the various 3Techno sites, including our websites on www.3techno.com or www.technofinancials.com or computer or mobile software applications;</li>
                            <li>Customers and prospective customers and their representatives;</li>
                            <li>Suppliers and business partners and their representatives.</li>
                        </ul>
                        <p></p>
                        <p>
                            While utilizing our websites, you also have the ability to link with non-3Techno, social networks, websites, social networks, other features or applications. This will lead to other parties than 3Techno processing information about you. 3Techno does not have any control over these features of other parties. We encourage you to review the privacy policies of these parties before using these features.
                        </p>
                        <h3>RESPONSIBLITY OF YOUR PERSONAL INFORMATION</h3>

                        3Techno Private Limited is responsible for processing your personal information described in this Privacy Policy. <br>

                        <h3> TYPES OF PERSONAL INFORMATION WE DO PROCESS AND FROM WHICH SOURCES</h3>
                        <p>3Techno can process information about you collected both offline and online.</p><p>


                        </p><p> <span class="spanHeading"> Online information </span> about you originates from the use of cookies and similar technologies (for example, pixel tags and device identifiers) on our sites or sites of third parties. Information about you may also be provided by third party sources, such as data aggregators who may not have a relationship with you. </p>
                        <p>Online information about you may also from your activities on our sites, for example, in relation with your 3Techno accounts, (pre-) sales inquiries or from your interactions with 3Techno via electronic communication tools such as email or telephone.</p>
                        <p> <span class="spadHeading"> Offline information </span> about you originates from our interactions with you during in-person meetings or at 3Techno events, conferences, workshops or gatherings. </p>
                        <p>Information about you that 3Techno may collect and process includes:</p>

                        <ul>
                            <li>company data such as the name, size and location of the company you work for and your role within the company;</li>
                            <li>name and physical address, email addresses, and telephone numbers;</li>
                            <li>demographic attributes, when tied to personal information that identifies you;</li>
                            <li>transactional data, including products and services ordered, financial details and payment methods;</li>
                            <li>unique IDs such as your mobile device identifier or cookie ID on your browser;</li>
                            <li>IP address and information that may be derived from IP address, such as geographic location;</li>
                        </ul>

                        <p>Please note that you should carefully consider whether you wish to submit personal information to 3Techno Communities forums or social networks; in some cases, such content may be publicly available on the Internet. 3Techno does not control the content that you may post to 3Techno Communities forums or social networks.</p>


                        <h3>HOW AND WHY DO 3Techno USE YOUR PERSONAL INFORMATION?</h3>

                        <span class="spanHeading">1-To communicate and respond to your requests and inquiries to 3Techno</span>
                        <p>We process information about you to communicate with you and to respond to your requests or other inquiries, if you get in touch with us. We can also process personal information to interact with you on third party social networks.</p>
                        <span class="spanHeading">2-To deliver functionality on our sites and for their technical and functional management</span>
                        <p>We need to process the personal information provided by you so that we can create and manage a personal account for you, as you choose to register with us. Upon creating your account, we will send you your personal login information. This personal information enables us to administer your account, for example by changing your password for you.</p>
                        <span class="spanHeading">3-To market our products and services or related products and services and to tailor marketing and sales activities</span>
                        <p> 3Techno may use personal information to advertise 3Techno's products and services or related products and services. 3Techno may use information about you to notify you about new product releases and events, service developments, updates, alerts, prices, terms, special offers and promotions and associated campaigns. </p>
                        <p> We may also process your personal information to post testimonials on our sites, but will first obtain your consent to use your name and testimonial.</p>
                        <span class="spanHeading">4-To engage in transactions with customers, suppliers and business partners and to process purchases of our products and services</span>
                        <p> If you provide services to 3Techno, our customers, employees or business partner, or partners as a supplier 3Techno processes information about you to engage in and administer your order, administer the relevant transactions, and help you get started and adopt our products and services (e.g., by contacting you to activate your TF credits). If you download products or services from our sites, 3Techno uses information about you to confirm certain information about your order.</p>
                        <span class="spanHeading">5-To analyze, develop, improve and optimize the use, function and performance of our sites and products and services</span>
                        <p> 3Techno may process personal information when moderating activities, in case the sites permit you to participate in interactive discussions, create a profile, post comments, opportunities or other content, or communicate directly with another user or otherwise engage in networking activities. We may process personal information in order to analyze, develop, improve and optimize the use, function and performance of our sites and products and services, as well as marketing and sales campaigns. </p>
                        <span class="spanHeading">6-To manage the security of our sites, networks and systems</span>
                        <p> We may collect site use data for security and to help keep our sites, networks and systems secure, or to investigate and prevent potential fraud, including ad fraud and cyber-attacks and to detect bots.</p>
                        <span class="spanHeading">7-To comply with applicable laws and regulations and to operate our business</span>
                        <p> In some cases, we have to process personal information to comply with applicable laws and regulations. We may also process personal information in the performance and operation of our business, such as finance and accounting or to conduct internal audits and investigations and insurance purposes and archiving.</p>


                        <h3>RETENTION OF PERSONAL INFORMATION</h3>

                        <p> 3Techno maintains personal information for the following retention periods:</p>

                        <ul>
                            <li>Information about you we collect to process purchases of our products and services will be retained for the duration of the transaction or services period, or longer as necessary for record retention and legal compliance purpose, or to engage in transactions with our customers, suppliers and business partners.</li>
                            <li>Your account information and account will be deleted if you do not log in for 18 consecutive months. 3Techno retains records of that deletion for 90 days. If you have registered for a 3Techno account, your account information will be retained for as long as you maintain an active account.</li>
                            <li>If you have registered for our newsletters and blogs, your subscription data will be retained for as long as you are subscribed to our distribution lists. 3Techno retains records of that deletion for 30 days.</li>
                            <li>If you have reached out to us via 3Techno Sales chat, we will delete all chat transcripts 90 days after the chat has concluded.</li>
                            <li>Personal information needed to retain your opt-out preferences are retained for 5 years (or longer as necessary to comply with applicable law)</li>
                        </ul>

                        <h3>WHEN AND HOW CAN WE SHARE YOUR PERSONAL INFORMATION</h3>
                        <span class="spanHeading">Sharing within 3Techno </span>
                        <p>3Techno employees are authorized to access personal information only to the extent necessary to serve the applicable purpose(s) and to perform their job functions.</p>
                        <span class="spanHeading">Sharing with third parties</span>
                        <p>We may share personal information with the following third parties:</p>
                        <p>Third-party service providers (for example, order fulfillment, website management, information technology and related e-mail delivery, infrastructure provision, customer service, auditing, and other similar service providers) in order for those service providers to perform business functions on behalf of 3Techno.</p>

                        <p>Specific partners that offer complementary products and services or with third parties to facilitate interest-based advertising. </p>

                        <p> As required by law, such as to comply with legal process, when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to government requests, including public and government authorities outside your country of residence, for national security and/or law enforcement purposes.</p>

                        <p> When third parties are given access to personal information, we will take appropriate contractual, technical and organizational measures designed to ensure that personal information is processed only to the extent that such processing is necessary, consistent with this Privacy Policy, and in accordance with applicable law.</p>


                        <h3>PERSONAL INFORMATION HANDLED BY 3TECHNO</h3>

                        <p>3Techno is a Pakistan based registered organization with operations happening worldwide. 3Techno will take measures designed to adequately protect information about you, if personal information is transferred to a 3Techno recipient in a country that does not provide an adequate level of protection for personal information, such as ensuring that such transfers are subject to the terms of the personal data protection of Pakistan. </p>
                        <h3> HOW IS YOUR PERSONAL INFORMATION SECURED? </h3>
                        <p> 3Techno has implemented appropriate physical, technical organizational measures designed to protect personal information against alteration or unlawful destruction, damage unauthorized disclosure or access, as well as all other forms of unlawful processing. </p>
                        <p> 3Techno provides for the security of their users by enabling the encryption of data transmitted between 3techno Digital Pvt Ltd. and your browser during an SSL/TLS encrypted session (look for the padlock). </p>
                        <h3> WHAT ARE YOUR CHOICES </h3>
                        <p> We provide multiple choices in respect of the information we process about you:</p>

                        <ul>
                            <li>
                                <span class="spanHeading">Delete personal information</span>
                                <p>you can ask us to erase or delete all or some of the information about you.</p>
                            </li>
                            <li>
                                <span class="spanHeading">Opt-out of our use of your personal information</span>
                                <p> you may withdraw consent you have previously provided for the processing of information about you, including for email marketing by 3Techno</p>
                            </li>
                            <li>
                                <span class="spanHeading">Object to, or limit or restrict use of personal information</span>
                                <p>you can ask us to stop using all or some of the information about you (for example, if we have no legal right to keep using it) or to limit our use of it (for example, if the information about you is inaccurate).</p>
                            </li>
                            <li>
                                <span class="spanHeading">Change or correct personal information</span>
                                <p>you can edit some of the information about you by. You can also ask us to change, update or fix information about you in certain cases, particularly if it is inaccurate.</p>
                            </li>
                            <li>
                                <span class="spanHeading">Right to access and/or have your information provided to you</span>
                                <p>You can also ask us for a copy of information about you and can ask for a copy of information about you provided in machine readable form if you reside in the EU or other country that provides you this right as a matter of law.</p>
                            </li>
                        </ul>

                        <p>You can exercise these choices in accordance with applicable laws as specified on in our policies or by filling out our inquiry form. </p>


                        <h3>DO YOU COLLECT SENSITIVE INFORMATION AND INFORMATION FROM CHILDREN?</h3>

                        <span class="spanHeading">Sensitive personal information </span>
                        <p>We ask that you do not send us, and do not share any sensitive personal information (for example, government-issued IDs, information related to racial or ethnic origin, political opinions, religion or other beliefs, health, genetic, or biometric data, criminal background or trade union membership).</p>
                        <span class="spanHeading">Children’s privacy </span>
                        <p>As a company focused on serving the needs of businesses, 3Techno's sites are not directed to minors and 3Techno does not promote or market its services to minors, except in very limited circumstances as part of specific educational outreach programs with parental permission. If you believe that we have mistakenly or unintentionally collected personal information of a minor through our sites without appropriate consent, please notify us through our inquiry form so that we may immediately delete the information from our servers and make any other necessary corrections. Additionally, please use this same form to request removal of content or information that was posted to our sites when the registered user was under the age of 18. Please note that such requests may not ensure complete or comprehensive removal of the content or information, as, for example, some of the content may have been reposted by another user.</p>

                        <h3>DISPUTE RESOLUTION OR FILING A COMPLAINT</h3>
                        <p>If you have any complaints regarding our compliance with this Privacy Policy, please contact us first. We will investigate and attempt to resolve complaints and disputes regarding use and disclosure of personal information in accordance with this Privacy Policy and in accordance with applicable law.</p>

                        <h3>3TECHNO PRIVATE LIMITED HEADQUARTERS</h3>
                        3Techno's corporate headquarters are located at:<br><br>
                        Suite # 203, WindSong Palace, <br>
                        Shahrah-e-Faisal Rd, <br>
                        KCHSU Block 7/8 Jinnah Housing Society PECHS, <br>
                        Karachi, Sindh <br>
                        Pakistan, 75400
                        <br><br>
                        Contact us at: info@3techno.com for any related quries.

                    </div>
                </div>
            </div>

        </div>
                          <div class="clearfix">&nbsp;</div>
          <div class="clearfix">&nbsp;</div>
          <div class="clearfix">&nbsp;</div>
<br />
                <br />
            <br />

                <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-5">
                    <p>© 2018-2020 TechnoFinancials. All rights reserved.</p>
                </div>
                <div class="col-sm-4 d-flex justify-content-center p-0">
                    <ul class="<%--list-group list-group-horizontal fotor_ul--%> fotor_ul list-inline">
                        <li><a href="/privacy-policy">Policy</a></li>
                        <li><a href="/security">Security</a></li>
                        <li><a href="/terms-of-use">Terms of Service</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
<%--                    <div class="copyright pull-right">
                        <a href="http://3techno.com" target="_blank" class="tf-footer-name tf-font-times-new-roman">A Product of
                    <img src="/assets/images/footer_logo.png" class="tf-footer-logo">
                            3techno</a>
                    </div>--%>
                </div>
            </div>
        </div>
    </footer>
        </section>

        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>
	  <!--<div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">
                <div class="row" style="margin-left: 0px; margin-right: 0px;">
                    <div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
                        <div class="header-bg-div">
                            <div class="header-name-div">
                                <h2 class="header-name-slide">Privacy Policy</h2>
                                <h4  class="header-name-slide" style="font-size:15px;">Last Updated: March 12, 2019</h4>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row" style="margin-left: 0px; margin-right: 0px;">
                    <div class="col-sm-12">
                        <div class="terms-of-use-main-div">
                            <br />
                            <h3>INTRODUCTION</h3>
                            <p>
                                This 3Techno Digital Private Limited’s (hereinafter referred to as ‘3Techno’) Privacy Policy delivers info use, collection; processing and sharing of personal information in connection with your utilization of 3Techno social media pages, websites and mobile applications that link to this Privacy Policy, in the context of other offline sales and marketing activities, and your interactions with 3Techno during in-person meetings or at 3Techno events.</p> 
                            <p>The most up-to-date version of the Privacy Policy can be found on 3Techno website. This Privacy Policy can change over time, for example to comply with legal requirements or to meet changing business needs. We will inform you in case there is a change in the Privacy Policy via notice or statement of changes on our website.</p>
                            <p>‘Personal Information’ or ‘personal data’ includes your name, address, email address, business contact details, or information gathered through your interactions with us via our websites or at events. This information relates to an identified individual or to an identifiable individual.
                            </p>
                            <h3>SCOPE</h3>

                            This Privacy Policy applies to the processing of personal information by 3Techno of:
                            <ul>
                                <li>Our social media pages that link to this Privacy Policy, (collectively referred to as the sites) users and visitors of the various 3Techno sites, including our websites on www.3techno.com or www.technofinancials.com or computer or mobile software applications;</li>
                                <li>Customers and prospective customers and their representatives;</li>
                                <li>Suppliers and business partners and their representatives.</li>
                            </ul>
							<p></p>
                            <p>
                                While utilizing our websites, you also have the ability to link with non-3Techno, social networks, websites, social networks, other features or applications. This will lead to other parties than 3Techno processing information about you. 3Techno does not have any control over these features of other parties. We encourage you to review the privacy policies of these parties before using these features.
                            </p>
                            <h3>RESPONSIBLITY OF YOUR PERSONAL INFORMATION</h3>

                            3Techno Private Limited is responsible for processing your personal information described in this Privacy Policy. <br />

                           <h3> TYPES OF PERSONAL INFORMATION WE DO PROCESS AND FROM WHICH SOURCES</h3>
                            <p>3Techno can process information about you collected both offline and online.<p>
                           
						   
						   <p> <span class="spanHeading"> Online information </span> about you originates from the use of cookies and similar technologies (for example, pixel tags and device identifiers) on our sites or sites of third parties. Information about you may also be provided by third party sources, such as data aggregators who may not have a relationship with you. </p>
                            <p>Online information about you may also from your activities on our sites, for example, in relation with your 3Techno accounts, (pre-) sales inquiries or from your interactions with 3Techno via electronic communication tools such as email or telephone.</p>
                           <p> <span class="spadHeading"> Offline information </span> about you originates from our interactions with you during in-person meetings or at 3Techno events, conferences, workshops or gatherings. </p>
                            <p>Information about you that 3Techno may collect and process includes:</p>
							
							<ul>
							<li>company data such as the name, size and location of the company you work for and your role within the company;</li>
							<li>name and physical address, email addresses, and telephone numbers;</li>
							<li>demographic attributes, when tied to personal information that identifies you;</li>
							<li>transactional data, including products and services ordered, financial details and payment methods;</li>
							<li>unique IDs such as your mobile device identifier or cookie ID on your browser;</li>
							<li>IP address and information that may be derived from IP address, such as geographic location;</li>
							</ul>
                            <p>Please note that you should carefully consider whether you wish to submit personal information to 3Techno Communities forums or social networks; in some cases, such content may be publicly available on the Internet. 3Techno does not control the content that you may post to 3Techno Communities forums or social networks.</p>
                            
                            <h3>HOW AND WHY DO 3Techno USE YOUR PERSONAL INFORMATION?</h3>

                            <span class="spanHeading">1-To communicate and respond to your requests and inquiries to 3Techno</span>
                            <p>We process information about you to communicate with you and to respond to your requests or other inquiries, if you get in touch with us. We can also process personal information to interact with you on third party social networks.</p>
                            <span class="spanHeading">2-To deliver functionality on our sites and for their technical and functional management</span>
                            <p>We need to process the personal information provided by you so that we can create and manage a personal account for you, as you choose to register with us. Upon creating your account, we will send you your personal login information. This personal information enables us to administer your account, for example by changing your password for you.</p>
                            <span class="spanHeading">3-To market our products and services or related products and services and to tailor marketing and sales activities</span>
                           <p> 3Techno may use personal information to advertise 3Techno's products and services or related products and services. 3Techno may use information about you to notify you about new product releases and events, service developments, updates, alerts, prices, terms, special offers and promotions and associated campaigns. </p>
                           <p> We may also process your personal information to post testimonials on our sites, but will first obtain your consent to use your name and testimonial.</p>
                            <span class="spanHeading">4-To engage in transactions with customers, suppliers and business partners and to process purchases of our products and services</span>
                           <p> If you provide services to 3Techno, our customers, employees or business partner, or partners as a supplier 3Techno processes information about you to engage in and administer your order, administer the relevant transactions, and help you get started and adopt our products and services (e.g., by contacting you to activate your TF credits). If you download products or services from our sites, 3Techno uses information about you to confirm certain information about your order.</p>
                            <span class="spanHeading">5-To analyze, develop, improve and optimize the use, function and performance of our sites and products and services</span>
                           <p> 3Techno may process personal information when moderating activities, in case the sites permit you to participate in interactive discussions, create a profile, post comments, opportunities or other content, or communicate directly with another user or otherwise engage in networking activities. We may process personal information in order to analyze, develop, improve and optimize the use, function and performance of our sites and products and services, as well as marketing and sales campaigns. </p>
                            <span class="spanHeading">6-To manage the security of our sites, networks and systems</span>
                           <p> We may collect site use data for security and to help keep our sites, networks and systems secure, or to investigate and prevent potential fraud, including ad fraud and cyber-attacks and to detect bots.</p>
                            <span class="spanHeading">7-To comply with applicable laws and regulations and to operate our business</span>
                           <p> In some cases, we have to process personal information to comply with applicable laws and regulations. We may also process personal information in the performance and operation of our business, such as finance and accounting or to conduct internal audits and investigations and insurance purposes and archiving.</p>
                            <h3>RETENTION OF PERSONAL INFORMATION</h3>
                           <p> 3Techno maintains personal information for the following retention periods:</p>
						   <ul>
						   <li>Information about you we collect to process purchases of our products and services will be retained for the duration of the transaction or services period, or longer as necessary for record retention and legal compliance purpose, or to engage in transactions with our customers, suppliers and business partners.</li>
						   <li>Your account information and account will be deleted if you do not log in for 18 consecutive months. 3Techno retains records of that deletion for 90 days. If you have registered for a 3Techno account, your account information will be retained for as long as you maintain an active account.</li>
						   <li>If you have registered for our newsletters and blogs, your subscription data will be retained for as long as you are subscribed to our distribution lists. 3Techno retains records of that deletion for 30 days.</li>
						   <li>If you have reached out to us via 3Techno Sales chat, we will delete all chat transcripts 90 days after the chat has concluded.</li>
						   <li>Personal information needed to retain your opt-out preferences are retained for 5 years (or longer as necessary to comply with applicable law)</li>
						   </ul>
                            <h3>WHEN AND HOW CAN WE SHARE YOUR PERSONAL INFORMATION</h3>
                            <span class="spanHeading">Sharing within 3Techno </span>
                            <p>3Techno employees are authorized to access personal information only to the extent necessary to serve the applicable purpose(s) and to perform their job functions.</p>
                            <span class="spanHeading">Sharing with third parties</span>
                            <p>We may share personal information with the following third parties:</p>
                            <p>Third-party service providers (for example, order fulfillment, website management, information technology and related e-mail delivery, infrastructure provision, customer service, auditing, and other similar service providers) in order for those service providers to perform business functions on behalf of 3Techno.</p>

                            <p>Specific partners that offer complementary products and services or with third parties to facilitate interest-based advertising. </p>
 
                           <p> As required by law, such as to comply with legal process, when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to government requests, including public and government authorities outside your country of residence, for national security and/or law enforcement purposes.</p>

                           <p> When third parties are given access to personal information, we will take appropriate contractual, technical and organizational measures designed to ensure that personal information is processed only to the extent that such processing is necessary, consistent with this Privacy Policy, and in accordance with applicable law.</p>

                            <h3>PERSONAL INFORMATION HANDLED BY 3TECHNO</h3>

                            <p>3Techno is a Pakistan based registered organization with operations happening worldwide. 3Techno will take measures designed to adequately protect information about you, if personal information is transferred to a 3Techno recipient in a country that does not provide an adequate level of protection for personal information, such as ensuring that such transfers are subject to the terms of the personal data protection of Pakistan. </p>
                           <h3> HOW IS YOUR PERSONAL INFORMATION SECURED? </h3>
                           <p> 3Techno has implemented appropriate physical, technical organizational measures designed to protect personal information against alteration or unlawful destruction, damage unauthorized disclosure or access, as well as all other forms of unlawful processing. </p>
                           <p> 3Techno provides for the security of their users by enabling the encryption of data transmitted between 3techno Digital Pvt Ltd. and your browser during an SSL/TLS encrypted session (look for the padlock). </p>
                           <h3> WHAT ARE YOUR CHOICES </h3>
                           <p> We provide multiple choices in respect of the information we process about you:</p>
									
									<ul>
									<li>
									<span class="spanHeading">Delete personal information</span>
									<p>you can ask us to erase or delete all or some of the information about you.</p>
									</li>
									<li>
									<span class="spanHeading">Opt-out of our use of your personal information</span>
									<p> you may withdraw consent you have previously provided for the processing of information about you, including for email marketing by 3Techno</p>
									</li>
									<li>
									<span class="spanHeading">Object to, or limit or restrict use of personal information</span>
									<p>you can ask us to stop using all or some of the information about you (for example, if we have no legal right to keep using it) or to limit our use of it (for example, if the information about you is inaccurate).</p>
									</li>
									<li>
									<span class="spanHeading">Change or correct personal information</span>
									<p>you can edit some of the information about you by. You can also ask us to change, update or fix information about you in certain cases, particularly if it is inaccurate.</p>
									</li>
									<li>
									<span class="spanHeading">Right to access and/or have your information provided to you</span>
									<p>You can also ask us for a copy of information about you and can ask for a copy of information about you provided in machine readable form if you reside in the EU or other country that provides you this right as a matter of law.</p>
									</li>
									</ul>
                            <p>You can exercise these choices in accordance with applicable laws as specified on in our policies or by filling out our inquiry form. </p>

                            
                            <h3>DO YOU COLLECT SENSITIVE INFORMATION AND INFORMATION FROM CHILDREN?</h3>

                            <span class="spanHeading">Sensitive personal information </span>
                            <p>We ask that you do not send us, and do not share any sensitive personal information (for example, government-issued IDs, information related to racial or ethnic origin, political opinions, religion or other beliefs, health, genetic, or biometric data, criminal background or trade union membership).</p>
                            <span class="spanHeading">Children’s privacy </span>
                            <p>As a company focused on serving the needs of businesses, 3Techno's sites are not directed to minors and 3Techno does not promote or market its services to minors, except in very limited circumstances as part of specific educational outreach programs with parental permission. If you believe that we have mistakenly or unintentionally collected personal information of a minor through our sites without appropriate consent, please notify us through our inquiry form so that we may immediately delete the information from our servers and make any other necessary corrections. Additionally, please use this same form to request removal of content or information that was posted to our sites when the registered user was under the age of 18. Please note that such requests may not ensure complete or comprehensive removal of the content or information, as, for example, some of the content may have been reposted by another user.</p>
                            
                            <h3>DISPUTE RESOLUTION OR FILING A COMPLAINT</h3>
                            <p>If you have any complaints regarding our compliance with this Privacy Policy, please contact us first. We will investigate and attempt to resolve complaints and disputes regarding use and disclosure of personal information in accordance with this Privacy Policy and in accordance with applicable law.</p>
                            
                            <h3>3TECHNO PRIVATE LIMITED HEADQUARTERS</h3>
                            3Techno's corporate headquarters are located at:<br /><br />
                            Suite # 203, WindSong Palace, <br />
                            Shahrah-e-Faisal Rd, <br />
                            KCHSU Block 7/8 Jinnah Housing Society PECHS, <br />
                            Karachi, Sindh <br />
                            Pakistan, 75400
                            <br /><br />
                            Contact us at: info@3techno.com for any related quries.

                        </div>
                    </div>
                </div>  -->
<%--                          <uc:Footer ID="Footer1" runat="server"></uc:Footer>   --%>

	<style>
	        ul{
            list-style-type: disc !important;
            padding: 0px 35px !important;
            margin-top:0px !important;
            margin-bottom:10px !important;
        }
          .terms-of-use-main-div p {
    font-size: 14px;
    text-align: justify;
}
        .spanHeading {
            /*font-weight: bold;
            color: #6a6c6f;
            font-size: 21px;*/
            font-weight: bold;
    color: rgb(0, 55, 128);
    font-size: 18px;
    padding: 5px 0px;
        }
        .terms-of-use-main-div{
    margin:auto 60px !important;
    padding-right:20px;
}
        .container-fluid{
            padding-left:0px !important; 
        }
 .main-footer {
    color: #000 !important;
    font-size: 14px;
    padding: 10px 30px;
    box-shadow: inset 0px 0 9px 0px #c3c1c1;
    border-top: 1px solid;
    position: fixed;
    width: 100%;
    bottom: 0;
    background: #fff;
    right: 0;
    transition: all 0.3s;
    z-index: 1;
}
        .main-footer ul {
    list-style-type: none;
    margin-bottom: 0px !important;
}
        .main-footer ul li {
    padding: 0 10px;
}
        .main-footer ul li a {
    font-family: Noto-Regular;
    color: inherit;
    text-decoration: none;
    transition: all 0.3s;
    outline: none !important;
}
	</style>
</body>
</html>
