﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Technofinancials.SuperAdmin.manage.Users" %>

<%@ Register Src="~/usercontrols/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/SuperAdmin/usercontrols/Sidebar.ascx" TagName="SideBar" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Footer.ascx" TagName="Footer" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary ">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrptmngr" runat="server"></asp:ScriptManager>
        <uc:Header ID="header1" runat="server"></uc:Header>
        <uc:SideBar ID="sidebar2" runat="server"></uc:SideBar>
        <!-- APP MAIN ==========-->
        <main id="app-main" class="app-main">

            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdPnl" runat="server">
                <ProgressTemplate>
                    <div class="upd_panel">
                        <div class="center">
                            <img src="/assets/images/Loading.gif" />
                        </div>


                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


            <div class="wrap">
                <asp:UpdatePanel ID="UpdPnl" runat="server">
                    <ContentTemplate>
                        <section class="app-content">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="/assets/images/Users-2.png" class="img-responsive tf-page-heading-img" />
                                    <h3 class="tf-page-heading-text">Users</h3>
                                </div>
                                <div class="col-md-4">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="divAlertMsg" runat="server">
                                                <div class="alert tf-alert-danger" id="divAlertTheme" runat="server">
                                                    <span>
                                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                                    </span>
                                                    <p id="pAlertMsg" runat="server">
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="col-sm-4" style="margin-top: 10px;">
                                    <div class="pull-right">
                                        <button class="tf-save-btn" "Save" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" validationgroup="btnValidate" type="button"><i class="far fa-save"></i></button>
                                        <a id="btnBack" runat="server" class="tf-back-btn" "Back"><i class="fas fa-arrow-left"></i></a>
                                    </div>

                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr />
                                </div>
                            </div>
                            <div class="row ">
                                <div class="tab-content ">
                                    <div class="tab-pane active row">
                                        <div class="col-sm-12">
                                        </div>
                                        <!-- ADMINISTRATOR TABLE -->
                                        <div class="clearfix">&nbsp;</div>

                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3>Administrator</h3>
                                                    <table class="table table-bordered" id="tblAdministrator" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th>Module
                                                                </th>
                                                                <th>Sub Module
                                                                </th>
                                                                <th></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Company Profile</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chkCompanyProfile" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Child Companies</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chkChildCompanies" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Locations</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chkLocations" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Organization Structure </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chkOrganizationStructure" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Departments</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_OS" id="chk_OS_Departments" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Designations</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_OS" id="chk_OS_Designations" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Grades</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_OS" id="chk_OS_Grades" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Job Descriptions</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_OS" id="chk_OS_JobDescriptions" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Organizational Chart</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_OS" id="chk_OS_OrganizationalChart" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Black Listing </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chkBlackListing" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Employees</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_BL" id="chk_BL_Employees" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Vendors</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_BL" id="chk_BL_Vendorst" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Users </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chkUsers" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Audit Logs </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chkAuditLogs" runat="server" /></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- CRM TABLE -->
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3>CRM</h3>

                                                    <table class="table table-bordered" id="Table1" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th>Module
                                                                </th>
                                                                <th>Sub Module
                                                                </th>
                                                                <th></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Leads</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkLeads" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Clients</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkClients" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Business Division</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkBusinessdivision" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Products & Services </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkProductsServices" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Tickets </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkTickets" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Create Tickets</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_TIC" id="Chk_Create_Tickets" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>All Tickets</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_TIC" id="Chk_All_Tickets" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Open Tickets</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_TIC" id="Chk_Open_Tickets" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Close Ticket</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_TIC" id="Chk_Close_Tickets" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Document Template </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkDocumentTemplate" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>CRM Report</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_CRM_DT" id="Chk_crm_report" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Reports </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkReports" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Leads</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_CRM_REP" id="Chk_Leads" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Leads Summary</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_CRM_REP" id="Chk_eads_Summary" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Bid Amount</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_CRM_REP" id="Chk_Bid" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Follow Up</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_CRM_REP" id="Chk_Follow" runat="server" /></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>




                                                </div>
                                            </div>
                                        </div>
                                        <!-- FIXED ASSET TABLE -->
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3>Fixed Asset Management</h3>

                                                    <table class="table table-bordered" id="Table2" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th>Module
                                                                </th>
                                                                <th>Sub Module
                                                                </th>
                                                                <th>Sub Sub Module</th>
                                                                <th></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <tr>
                                                                <td>Fixed Assets</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkFixedAssets" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>List of assets</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FA" id="Chk_List_Assets" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Add new assets</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FA" id="Chk_Add_New_Assets" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Revaluation of assets</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FA" id="Chk_Revaluation_Assets" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Disposal of assets</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FA" id="Chk_Disposal_Assets" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Idle assets</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FA" id="Chk_Idle_Assets" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Transfer assets</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FA" id="Chk_Transfer_Assets" runat="server" /></td>
                                                            </tr>



                                                            <tr>
                                                                <td>CWIP</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkCWIP" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Pending Assets </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkPendingAssets" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Policies & Procedure </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkPolicies" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Design Report Template </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkDesignReport" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Reports </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Chk_FAM_report" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Assets List</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_REP" id="Chk_FAM_REP_Assets_List" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td>All Assets</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_REP check_FAM_REP_ALL" id="chk_FAM_REP_ALLASSET" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td>Addition</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_REP check_FAM_REP_ALL" id="chk_FAM_REP_Add" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td>Dispose</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_REP check_FAM_REP_ALL" id="chk_FAM_REP_DIS" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td>Idle</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_REP check_FAM_REP_ALL" id="chk_FAM_REP_IDL" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td>CWIP</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_REP check_FAM_REP_ALL" id="chk_FAM_REP_CWIP" runat="server" /></td>
                                                            </tr>



                                                            <tr>
                                                                <td></td>
                                                                <td>Fixed Assets Register</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_REP" id="Chk_Fixed_Asset_Register" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Fixed Assets Summary Report</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_REP" id="Chk_Fixed_Asset_Summary" runat="server" /></td>
                                                            </tr>



                                                            <tr>
                                                                <td></td>
                                                                <td>Fixed Assets Register(Including Tax)</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_REP" id="Chk_Fixed_Asset_Register_IncludingTax" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>
                                                                <td>Fixed Assets Summary Report(Including Tax)</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_REP" id="Chk_Fixed_Asset_Summary_IncludingTax" runat="server" /></td>
                                                            </tr>



                                                            <tr>
                                                                <td>Setting </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="ChkSettings" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Asset Category</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_SET" id="Chk_asset_category" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>

                                                                <td></td>
                                                                <td>Main Category</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_SET check_FAM_ASS_CAT" id="Chk_Main_category" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>

                                                                <td></td>
                                                                <td>Sub Category</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_SET check_FAM_ASS_CAT" id="Chk_Sub_category" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>

                                                                <td></td>
                                                                <td>Tax Category</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_SET check_FAM_ASS_CAT" id="Chk_Tax_category" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Financial Year</td>
                                                                <td></td>

                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_SET" id="Chk_financial_year" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>

                                                                <td>Tax Year</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_FAM_SET" id="Chk_Tax_year" runat="server" /></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>




                                                </div>
                                            </div>
                                        </div>
                                        <!-- PROCRUMENT MANAGEMENT TABLE -->
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3>Procrument Management</h3>

                                                    <table class="table table-bordered" id="Table3" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th>Module
                                                                </th>
                                                                <th>Sub Module
                                                                </th>
                                                                <th></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Inventory</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chk_Inventory" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Categories</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_IN" id="chk_Categories" runat="server" /></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td>Items</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_IN" id="chk_Items" runat="server" /></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td>VDMS</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_IN" id="chk_VDMS" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Inventory Valuation</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chk_Inventory_Valuation" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Stock Report</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_IV" id="chk_Stock_Report" runat="server" /></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td>Projet Vise</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_IV" id="chk_Projet_Vise" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Purchase</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chk_Purchase" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Purchase Plan</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_PP" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Quick Purchase</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_QP" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Purchase Requisite</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_PR" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Assets Transfer Request</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_ATR" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Deleviery Notes</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_DN" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Request For Qoutes</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_RFQ" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Qoute Campaign Reports</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_QCR" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Purchase Order</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_PO" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Good Recieve Notes</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_GRN" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Material Return Notes</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_MRN" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Debit Notes</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_DEBN" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Gate Pass</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_GP" runat="server" /></td>
                                                            </tr>


                                                            <tr>

                                                                <td></td>
                                                                <td>Bill Of Demand</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_PUR" id="chk_BOD" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Settings </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chk_Settings" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Tax Rate</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_SET" id="chk_TAX_RATE" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Document Template </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chk_PRO_DT" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Procrument Report</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DL_REP" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Purchase Plan</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_PP" runat="server" /></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td>Quick Purchase </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_QP" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Purchase Requiste</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_PR" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Assets Transfer Request </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_ATR" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Delivery Notes </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_DN" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Request For Qoutes </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_RFQ" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Qoute Comparasion Reports </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_QCR" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Purchase Orders </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_PO" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Good Recieve Notes </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_GRN" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Material Return Notes </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_MRN" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Debit Notes </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_DEBN" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Gate Pass </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_GP" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Bill Of Demand </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_DT" id="chk_PRO_DT_BOD" runat="server" /></td>

                                                            </tr>



                                                            <tr>
                                                                <td>Reports </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="chk_PRO_REP" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>
                                                                <td>Purchase Plan</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_PP" runat="server" /></td>
                                                            </tr>


                                                            <tr>

                                                                <td></td>
                                                                <td>Purchase Requiste</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_QP" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Assets Transfer Request </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_ATR" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Delevery Notes </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_DN" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Request For Qoutes </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_RFQ" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Qoute Comparasion Reports </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_QCR" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Purchase Orders </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_PO" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Good Recieve Notes </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_GRN" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Material Return Notes </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_MRN" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Debit Notes </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_DEBN" runat="server" /></td>

                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Vendor Block Lists </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_PRO_REP" id="chk_PRO_REP_VBL" runat="server" /></td>

                                                            </tr>


                                                        </tbody>
                                                    </table>




                                                </div>
                                            </div>
                                        </div>
                                        <!-- HR TABLE -->
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3>HR Management</h3>

                                                    <table class="table table-bordered" id="Table4" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th>Module
                                                                </th>
                                                                <th>Sub Module
                                                                </th>
                                                                <th></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Hiring</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox51" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>New Vacnacies</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox74" runat="server" /></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td>Candidates</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox75" runat="server" /></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td>Interviews</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox76" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Personal Management</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox61" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Attendance </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox63" runat="server" /></td>
                                                            </tr>


                                                            <tr>

                                                                <td></td>
                                                                <td>Attendance Sheet</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox62" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Individual Attendace</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox64" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Manage Attendance</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox65" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Manage Leaves</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox66" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Holidays</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox67" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Shifts</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox68" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Employee Finance </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox69" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Payroll</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox70" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Provinent Fund</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox71" runat="server" /></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td>Loans </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox72" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Training & Development </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox73" runat="server" /></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td>Training </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox77" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Tests </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox78" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Performance Evaluation </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox79" runat="server" /></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td>Succession </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox80" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Performance Appraisals </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox81" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Surveys </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox82" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Mailing Lists </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox83" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Documents Templates </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox84" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Employee Summary </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox85" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Offer Letter </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox86" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Warning Letter </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox87" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Payeroll Sheet </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox88" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Promotion Letter </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox89" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Attendance Sheet </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox90" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>HR Report </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox91" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Reports </td>
                                                                <td></td>

                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox92" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Vacancies Reports </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox93" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>New Hiring Report </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox94" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Employee Backlist Report </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox95" runat="server" /></td>
                                                            </tr>


                                                            <tr>

                                                                <td></td>
                                                                <td>Payroll Report </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox96" runat="server" /></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td>Attendance Report </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox97" runat="server" /></td>
                                                            </tr>


                                                            <tr>

                                                                <td></td>
                                                                <td>Surveys Report </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox98" runat="server" /></td>
                                                            </tr>


                                                            <tr>

                                                                <td></td>
                                                                <td>Training Report </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox99" runat="server" /></td>
                                                            </tr>

                                                            <tr>

                                                                <td></td>
                                                                <td>Organizational Chart With Photos </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox100" runat="server" /></td>
                                                            </tr>



                                                            <tr>

                                                                <td></td>
                                                                <td>Organizational Chart Without Photos </td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox101" runat="server" /></td>
                                                            </tr>


                                                        </tbody>
                                                    </table>




                                                </div>
                                            </div>
                                        </div>
                                        <!-- FNANCE -->
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3>Finance</h3>

                                                    <table class="table table-bordered" id="Table5" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th>Module
                                                                </th>
                                                                <th>Sub Module
                                                                </th>
                                                                <th>Sub Sub Module</th>
                                                                <th></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <tr>
                                                                <td>Revenue Management</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox102" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Dashboard</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox103" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Qoutation</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox104" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Invoice</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox105" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Statement Sharing</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox106" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Product & Services </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox107" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>CDMS</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox108" runat="server" /></td>
                                                            </tr>



                                                            <tr>
                                                                <td>Payment Management</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox109" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Dashboard</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox131" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Purchase Order</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox132" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Debit Note</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox133" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Other Transaction</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox134" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Payroll Management </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox110" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Cashflow Management </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox111" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Dashboard</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox135" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Accounts</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox136" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Transfers Money</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox137" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Bank reconcilation</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox138" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Petty Cash Management</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox139" runat="server" /></td>
                                                            </tr>



                                                            <tr>
                                                                <td>Document Template </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox112" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Report Letter</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox140" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Invoice</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox141" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Expense Voucher</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox142" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>PO JV</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox143" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Expenxe JV</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox144" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Invoice JV</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox145" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Payment Voucher</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox146" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>
                                                                <td>Bank Voucher</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox147" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Cash Voucher</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox148" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Reports </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox113" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Purchase Order Report </td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox114" runat="server" /></td>
                                                            </tr>




                                                            <tr>
                                                                <td></td>
                                                                <td>Invoice report</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox120" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Recivable Aging</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox121" runat="server" /></td>
                                                            </tr>



                                                            <tr>
                                                                <td></td>
                                                                <td>Payabel Aging</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox122" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>
                                                                <td>Profit/Loss Satement</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox123" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Trial Balance</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox115" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Vouchers </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox116" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>
                                                                <td>All Journal Vouchers</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox117" runat="server" /></td>
                                                            </tr>



                                                            <tr>
                                                                <td></td>
                                                                <td>Purchase Order JVs</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox118" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>
                                                                <td>Invoice JVs</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox119" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>
                                                                <td>Expense JVs</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox149" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>
                                                                <td>Bank Vouchers</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox150" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td></td>
                                                                <td>Cash Vouchers</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox151" runat="server" /></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Setting </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox124" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Chart Of Accounts</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox125" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>

                                                                <td></td>
                                                                <td>Setup COA</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox126" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>

                                                                <td></td>
                                                                <td>Add COA</td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox127" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>Multi Currency</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox128" runat="server" /></td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>Tax</td>
                                                                <td></td>
                                                                <td>
                                                                    <input type="checkbox" class="check_Administrator" id="Checkbox129" runat="server" /></td>
                                                            </tr>


                                                        </tbody>
                                                    </table>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- #dash-content -->
            </div>
            <!-- .wrap -->
            <uc:Footer ID="footer1" runat="server"></uc:Footer>
        </main>
        <!--========== END app main -->
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
        <style>
            .tf-back-btn {
                background-color: #575757;
                padding: 10px 10px 10px 10px;
                border-radius: 100px;
                border: none !important;
                color: #fff;
            }

                .tf-back-btn i {
                    color: #fff !important;
                }
        </style>

        <!-- Modal -->
        <script>


            $('#chkOrganizationStructure').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_OS').prop('checked', true);
                }
                else {
                    $('.check_OS').prop('checked', false);
                }
            });


            $('#chkBlackListing').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_BL').prop('checked', true);
                }
                else {
                    $('.check_BL').prop('checked', false);
                }
            });


            $('#ChkTickets').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_TIC').prop('checked', true);
                }
                else {
                    $('.check_TIC').prop('checked', false);
                }
            });


            $('#ChkDocumentTemplate').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_CRM_DT').prop('checked', true);
                }
                else {
                    $('.check_CRM_DT').prop('checked', false);
                }
            });


            $('#ChkReports').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_CRM_REP').prop('checked', true);
                }
                else {
                    $('.check_CRM_REP').prop('checked', false);
                }
            });


            $('#ChkFixedAssets').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_FA').prop('checked', true);
                }
                else {
                    $('.check_FA').prop('checked', false);
                }
            });


            $('#Chk_FAM_report').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_FAM_REP').prop('checked', true);
                }
                else {
                    $('.check_FAM_REP').prop('checked', false);
                }
            });


            $('#Chk_FAM_REP_Assets_List').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_FAM_REP_ALL').prop('checked', true);
                }
                else {
                    $('.check_FAM_REP_ALL').prop('checked', false);
                }
            });


            $('#ChkSettings').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_FAM_SET').prop('checked', true);
                }
                else {
                    $('.check_FAM_SET').prop('checked', false);
                }
            });


            $('#Chk_asset_category').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_FAM_ASS_CAT').prop('checked', true);
                }
                else {
                    $('.check_FAM_ASS_CAT').prop('checked', false);
                }
            });
            $('#chk_Inventory').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PRO_IN').prop('checked', true);
                }
                else {
                    $('.check_PRO_IN').prop('checked', false);
                }
            });
            $('#chk_Inventory_Valuation').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PRO_IV').prop('checked', true);
                }
                else {
                    $('.check_PRO_IV').prop('checked', false);
                }
            });
            $('#chk_Purchase').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PRO_PUR').prop('checked', true);
                }
                else {
                    $('.check_PRO_PUR').prop('checked', false);
                }
            });
            $('#chk_Settings').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PRO_SET').prop('checked', true);
                }
                else {
                    $('.check_PRO_SET').prop('checked', false);
                }
            });
            $('#chk_PRO_DT').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PRO_DT').prop('checked', true);
                }
                else {
                    $('.check_PRO_DT').prop('checked', false);
                }
            });
            $('#chk_PRO_REP').change(function () {
                if ($(this).is(':checked')) {
                    $('.check_PRO_REP').prop('checked', true);
                }
                else {
                    $('.check_PRO_REP').prop('checked', false);
                }
            });
        </script>
    </form>
</body>
</html>
