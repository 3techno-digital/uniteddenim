﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.SuperAdmin.manage
{
    public partial class Users : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                btnBack.HRef = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/super-admin/manage/users";
        
                

                clearFields();
                divAlertMsg.Visible = false;
       

                if (HttpContext.Current.Items["UserID"] != null)
                {
                    //userID = Convert.ToInt32(HttpContext.Current.Items["UserID"].ToString());
                    getUserByID(Convert.ToInt32(HttpContext.Current.Items["UserID"].ToString()));
                }
            }
        }

        private void getUserByID(int userID)
        {
            DataTable dt = new DataTable();
            objDB.UserID = userID;
            dt = objDB.GetUserDetailsByID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                }
            }
            Common.addlog("View", "Admin", "User \"" + "\" Viewed", "Users", objDB.UserID);



        }



        private void clearFields()
        {

        }



        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["EmployeeID"] == null)
                Response.Redirect("/login");
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            CheckSessions();
            int userID = 0;


            string res = "";
            if (HttpContext.Current.Items["UserID"] != null)
            {
                userID = Convert.ToInt32(HttpContext.Current.Items["UserID"]);
                objDB.ModifiedBy = Session["UserName"].ToString();
                objDB.UserID = userID;
                res = objDB.UpdateUser();
            }
            else
            {
                objDB.CreatedBy = Session["UserName"].ToString();
                userID = Convert.ToInt32(objDB.AddUser());
                res = "New User Added";
                clearFields();
            }
            






            if (res == "New User Added" || res == "User Data Updated")
            {
                if (res == "New User Added") { Common.addlog("Add", "Admin", "New User \"" + objDB.Email + "\" Added", "Users"); }
                if (res == "User Data Updated") { Common.addlog("Update", "Admin", "User \"" + objDB.Email + "\" Updated", "Users", objDB.UserID); }

                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-success";
                pAlertMsg.InnerHtml = res;
            }
            else
            {
                divAlertMsg.Visible = true;
                divAlertTheme.Attributes["class"] = "alert tf-alert-danger";
                pAlertMsg.InnerHtml = res;
            }
        }

        //protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    CheckSessions();
        //    BindEmployeesDropDown(Convert.ToInt32(ddlCompany.SelectedItem.Value));
        //}


    }
}