﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Technofinancials.SuperAdmin.view
{
    public partial class Companies : System.Web.UI.Page
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckSessions();
                GetData();
            }
        }

        private void GetData()
        {
            CheckSessions();

            DataTable dt = new DataTable();
            dt = objDB.GetAllCompaniesForSuperAdmin(ref errorMsg);
            gv.DataSource = dt;
            gv.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gv.UseAccessibleHeader = true;
                    gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            //Common.addlog("ViewAll", "Admin", "All Companies Viewed", "Companies");

        }

        private void CheckSessions()
        {
            if (Session["UserID"] == null && Session["UserID"] == null)
                Response.Redirect("/login");
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            //CheckSessions();
            //LinkButton btn = (LinkButton)sender as LinkButton;
            //int ID = Convert.ToInt32(btn.CommandArgument);
            //objDB.CompanyID = ID;
            //objDB.DeletedBy = Session["UserName"].ToString();
            //objDB.DeleteCompany();
            //Common.addlog("Delete", "Admin", "Company of ID \"" + objDB.CompanyID + "\" deleted", "Companies", objDB.CompanyID);

            //GetData();
        }
    }
}