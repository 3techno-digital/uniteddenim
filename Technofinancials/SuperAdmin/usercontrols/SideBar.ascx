﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SideBar.ascx.cs" Inherits="Technofinancials.SuperAdmin.usercontrols.SideBar" %>
<aside id="menubar" class="menubar tf-sidebar">
   

    <div class="menubar-scroll">
        <div class="menubar-scroll-inner tf-sidebar-menu-items">
            <ul class="app-menu">
                <li class="tf-first-menu">
                    <a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/dashboard"); %>">
                        <img src="/assets/images/dashboard__2.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Dashboard</span>
                    </a>
                </li>
          
              <%--  <li class="has-submenu ">
                    <a href="javascript:void(0)" class="submenu-toggle">
                        <img src="/assets/images/hr-07__2.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Performance Evaluation</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right tf-menu-caret"></i>
                    </a>
                    <ul class="submenu">
                        <li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/promotions"); %>"><span class="menu-text">Succession</span></a></li>
                        <li><a href="<% Response.Write("/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() + "/people-management/view/performance-appraisal"); %>"><span class="menu-text">Performance Appraisals</span></a></li>
                    </ul>
                </li>--%>
             
                <li>
                    <a href="<% Response.Write("/technofinancials/super-admin/view/companies"); %>">
                        <img src="/assets/images/MailingList__02.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Companies</span>
                    </a>
                </li>
             
                <li>
                    <a href="<% Response.Write("/technofinancials/super-admin/manage/users/add-new-user"); %>">
                        <img src="/assets/images/users.png" class="img-responsive tf-sidebar-icons" />
                        <span class="menu-text">Add Users</span>
                    </a>
                </li>
               
            </ul>
            <!-- .app-menu -->
        </div>
        <!-- .menubar-scroll-inner -->
    </div>
    <div class="tf-vno">tf-v1.0 <%Response.Write(DateTime.Now.ToString("dd-MMM-yyyy")); %></div>
    <!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->
