﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login-slider.aspx.cs" Inherits="Technofinancials.login_slider" %>

<%@ Register Src="~/usercontrols/Stylesheets.ascx" TagName="StyleSheets" TagPrefix="uc" %>
<%@ Register Src="~/usercontrols/Scripts.ascx" TagName="Scripts" TagPrefix="uc" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <uc:StyleSheets ID="styleSheet1" runat="server"></uc:StyleSheets>
    <link rel="stylesheet" href="/assets/css/main-inner-style.css" />
</head>
<body class="slider-body">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScrptMangr" runat="server"></asp:ScriptManager>
        <section>
            <div class="container-fluid" style="padding-left:12px;padding-right:12px;">
                <div class="row" >
                    <div class="col-sm-8">

                       <div class="row" style="height:75vh;">
                           <div class="col-sm-12" style="padding:0px;">
                                <img class="mySlides" style="width:100%;height:75vh;" src="/assets/images/login-slider-1.png"/>
<img class="mySlides" style="width:100%;height:75vh;" src="/assets/images/login-slider-2.png"/>
                           </div>
                       </div> 

                        <div class="row" style="background:#d1d1d1;height:24.99vh;">
                            <div class="col-sm-3">
                                <div class="bottom-section-data-div">
                                    
                                <h3 class="heading-color">What's New</h3>
                                <p style="color:#757575;">Get the latest on Techno Financial upgrades and upcoming <a href="/features">features</a>.</p>
                                </div>
                            </div>

                             <div class="col-sm-5">
                                   <div  class="bottom-section-data-div">
                                       <h3 class="heading-color">Training & Certifications</h3>
                                <p style="color:#757575;font-weight:bold;">Coming Soon!</p>
                                   </div>
                            </div>

                             <div class="col-sm-4">
                                 <div class="bottom-section-data-div">
                                     
                                   <h3 class="heading-color">News & Media</h3>
                                <p style="color:#757575;">Get the latest on Techno Financial <a href="/news">news</a>, <a href="/events">events</a>, <a href="/blogs">blogs</a>, <a href="/webnars">webnars</a> and <a href="/awards">awards</a>.</p>
                                 </div>
                            </div>
                        </div>
                    </div>


                     <asp:UpdatePanel ID="UpdPnl" runat="server">
                                <ContentTemplate>
                    <div class="col-sm-4 form-col">
                        <div class="signup-inner">
                            <a href="/"><img src="assets/images/tf-logo-blue.png" class="img-responsive tf-logo-center" /></a>
                            <div>
                                <div class="signup-form">
                                    <div class="input-group"><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtUserEmail" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red" ></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" runat="server" ControlToValidate="txtUserEmail" ForeColor="Red" ErrorMessage=" Email Address Not Valid" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="btnValidate"></asp:RegularExpressionValidator>
                                        <input class="form-control" placeholder="Username" type="text" id="txtUserEmail" runat="server"/>
                                    </div>
                                    <div class="input-group"><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUserPassword" ErrorMessage=" *Required" Display="Dynamic" ValidationGroup="btnValidate" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:TextBox CssClass="form-control" placeholder="Password" TextMode="password" id="txtUserPassword" runat="server"></asp:TextBox>
                                    </div>
                                    <asp:Button  CssClass="btn-sucess btn btn-default btn-login" ID="btnLogin" runat="server" ValidationGroup="btnValidate" Text="SIGN IN" OnClick="btnLogin_ServerClick"></asp:Button>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" id="chkRememberMe" runat="server" />
                                                <label for="chkRememberMe">Remember Me</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="/forgot-password" class="pull-right">Forgot Password</a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <p class="text-center">By clicking on the Sign In button, you understand and agree to our <a href="/terms-of-use">Terms of Use</a> and <a href="/privacy-policy">Privacy Policy</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group" id="divAlertMsg" runat="server">
                                <div class="alert tf-alert-danger">
                                    <span>
                                        <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                                    </span>
                                    <p id="pAlertMsg" runat="server">
                                       
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                                     </ContentTemplate>
                                     </asp:UpdatePanel>
                </div>
            </div>
        </section>
        <uc:Scripts ID="script1" runat="server"></uc:Scripts>
    </form>

            <script>
                var slideIndex = 0;
                carousel();

                function carousel() {
                    var i;
                    var x = document.getElementsByClassName("mySlides");
                    for (i = 0; i < x.length; i++) {
                        x[i].style.display = "none";
                    }
                    slideIndex++;
                    if (slideIndex > x.length) { slideIndex = 1 }
                    x[slideIndex - 1].style.display = "block";
                    setTimeout(carousel, 5000); // Change image every 5 seconds
                }
            </script>

</body>
</html>
