﻿using SelectPdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public static class Common
{

    public static List<T> ConvertDataTable<T>(DataTable dt)
    {
        List<T> data = new List<T>();
        foreach (DataRow row in dt.Rows)
        {
            T item = GetItem<T>(row);
            data.Add(item);
        }
        return data;
    }
    public static T GetItem<T>(DataRow dr)
    {
        Type temp = typeof(T);
        T obj = Activator.CreateInstance<T>();

        foreach (DataColumn column in dr.Table.Columns)
        {
            foreach (PropertyInfo pro in temp.GetProperties())
            {
                if (pro.Name == column.ColumnName)
                {
                    if (dr[column.ColumnName] == DBNull.Value)
                    { pro.SetValue(obj, null, null); }
                    else
                    {
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    }

                }
                else
                    continue;
            }
        }
        return obj;
    }

    private static String[] units = { "Zero", "One", "Two", "Three",
    "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven",
    "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen",
    "Seventeen", "Eighteen", "Nineteen" };
    private static String[] tens = { "", "", "Twenty", "Thirty", "Forty",
    "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
    public static String NumberToWords(string val)
    {
        double amount = double.Parse(val);
        return NumberToWords(amount);

    }

    public static string getCompleteVoucherName(string shortName)
    {
        if (shortName == "BRV")
        {
            return "Bank Receipt Voucher";
        }
        else if (shortName == "CRV")
        {
            return "Cash Receipt  Voucher";
        }
        else if (shortName == "BPV")
        {
            return "Bank Payment Voucher";
        }
        else if (shortName == "CPV")
        {
            return "Cash Payment Voucher";
        }

        return "Journal Voucher";
    }

    public static String NumberToWords(double amount)
    {
        try
        {
            Int64 amount_int = (Int64)amount;
            Int64 amount_dec = (Int64)Math.Round((amount - (double)(amount_int)) * 100);
            if (amount_dec == 0)
            {
                return ConvertNumber(amount_int) + " Rupees Only.";
            }
            else
            {
                return ConvertNumber(amount_int) + " Point " + ConvertNumber(amount_dec) + " Rupees Only.";
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
        return "0";
    }


    public static String ConvertNumber(Int64 i)
    {
        if (i < 20)
        {
            return units[i];
        }
        if (i < 100)
        {
            return tens[i / 10] + ((i % 10 > 0) ? " " + ConvertNumber(i % 10) : "");
        }
        if (i < 1000)
        {
            return units[i / 100] + " Hundred"
                    + ((i % 100 > 0) ? " And " + ConvertNumber(i % 100) : "");
        }
        if (i < 1000000)
        {
            return ConvertNumber(i / 1000) + " Thousand "
            + ((i % 1000 > 0) ? " " + ConvertNumber(i % 1000) : "");
        }
        //if (i < 1000000)
        //{
        //    return ConvertNumber(i / 100000) + " Lakh "
        //            + ((i % 100000 > 0) ? " " + ConvertNumber(i % 100000) : "");
        //}
        //if (i >= 1000000)
        //{
        //    return ConvertNumber(i / 1000000) + " Million "
        //            + ((i % 1000000 > 0) ? " " + ConvertNumber(i % 1000000) : "");
        //}

        //if (i < 1000000000)
        //{
        //    return ConvertNumber(i / 10000000) + " Crore "
        //            + ((i % 10000000 > 0) ? " " + ConvertNumber(i % 10000000) : "");
        //}
        //return ConvertNumber(i / 1000000000) + " Arab "
        //        + ((i % 1000000000 > 0) ? " " + ConvertNumber(i % 1000000000) : "");

        return ConvertNumber(i / 1000000) + " Million "
                   + ((i % 1000000 > 0) ? " " + ConvertNumber(i % 1000000) : "");
    }



    public static void PrintVoucher(int TraID)
    {
        try
        {
            DBQueries objDB = new DBQueries();
            string errorMsg = "";
            if (TraID != 0)
            {
                string DocName = "Voucher";
                DataTable dt = new DataTable();
                string txtHeader = "";
                string txtContent = "";
                string txtFooter = "";
                objDB.CompanyID = Convert.ToInt32(HttpContext.Current.Session["CompanyID"].ToString());
                objDB.DocType = "GRN Journal Voucher";
                dt = objDB.GetDocumentDesign(ref errorMsg);
                if (dt == null)
                {
                    objDB.CompanyID = 2;
                    objDB.DocType = "";
                    dt = objDB.GetDocumentDesign(ref errorMsg);
                }

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtHeader = dt.Rows[0]["DocHeader"].ToString();


                        if (HttpContext.Current.Session["CompanyName"].ToString() != null)
                        {
                            txtHeader = txtHeader.Replace("##COMPANY_NAME##", HttpContext.Current.Session["CompanyName"].ToString());

                        }
                        txtContent = dt.Rows[0]["DocContent"].ToString();
                        txtFooter = dt.Rows[0]["DocFooter"].ToString();
                    }
                }


                objDB.DirectPaymentID = TraID;
                DataTable dtVoucher = objDB.GetDirectPaymentDetailByDirectPaymentID(ref errorMsg);
                double totAmount = 0;


                if (dtVoucher != null && dtVoucher.Rows.Count > 0)
                {
                    DocName = getCompleteVoucherName(dtVoucher.Rows[0]["VoucherType"].ToString());
                    txtContent = txtContent.Replace("##DATE##", dtVoucher.Rows[0]["Date"].ToString());
                    txtContent = txtContent.Replace("##VOUCHER_NO##", dtVoucher.Rows[0]["VoucherNo"].ToString());
                    txtContent = txtContent.Replace("##SERIAL_NO##", dtVoucher.Rows[0]["SerialNo"].ToString());
                    txtHeader = txtHeader.Replace("##LOCATION_NAME##", dtVoucher.Rows[0]["LocationName"].ToString());
                    txtHeader = txtHeader.Replace("##VOUCHER_TYPE##", DocName);
                    txtContent = txtContent.Replace("##VOUCHER_TYPE##", DocName);

                    DocName = DocName + "-" + dtVoucher.Rows[0]["SerialNo"].ToString() + "-" + dtVoucher.Rows[0]["VoucherNo"].ToString();
                    string table = "";
                    table += "<table class='item-table' style = 'width:100%; border-collapse: collapse;'>";
                    table += "<tr><th>A/c Code</th><th>Title of A/c</th><th>Instrument#</th><th>Transaction Detail</th><th>Debit</th><th>Credit</th></tr>";
                    for (int i = 0; i < dtVoucher.Rows.Count; i++)
                    {
                        table += "<tr><td>" + dtVoucher.Rows[i]["COA_NO"].ToString() + "</td><td>" + dtVoucher.Rows[i]["COA_Title"].ToString() + "</td><td>" + dtVoucher.Rows[i]["Instrument"].ToString() + "</td><td>" + dtVoucher.Rows[i]["TransactionDetail"].ToString() + "</td><td>" + ((dtVoucher.Rows[i]["Debit"].ToString() == "0.00" || dtVoucher.Rows[i]["Debit"].ToString() == "0") ? "" : dtVoucher.Rows[i]["Debit"].ToString()) + "</td><td>" + ((dtVoucher.Rows[i]["Credit"].ToString() == "0.00" || dtVoucher.Rows[i]["Credit"].ToString() == "0") ? "" : dtVoucher.Rows[i]["Credit"].ToString()) + "</td></tr>";
                        totAmount += double.Parse(dtVoucher.Rows[i]["Debit"].ToString(), NumberStyles.Currency);
                    }

                    table += "<tr><td colspan = '4' style = 'text-align:right;'>" + "Net Total" + "</td><td>" + string.Format("{0:n2}", totAmount) + "</td><td>" + string.Format("{0:n2}", totAmount) + "</td></tr>";
                    table += "</table>";
                    table += "<p style='font-size:14px; font-weight:bold;'>" + NumberToWords(totAmount) + "</p>";
                    txtContent = txtContent.Replace("##DETAIL_TABLE##", table);

                    generatePDF(txtHeader, txtFooter, txtContent, "Technofinancials" + DocName.Replace(" ", "-"), "A4", "Portrait");
                }

            }

        }

        catch (Exception)
        {
        }
        finally
        {
            TraID = 0;
        }


    }



    public static string RemoveSpecialCharacter(string contents)
    {
        return Regex.Replace(contents, "[^a-zA-Z0-9_]+", "-", RegexOptions.Compiled);
    }
    public static DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();

        foreach (DataRow drow in dTable.Rows)
        {
            if (hTable.Contains(drow[colName]))
                duplicateList.Add(drow);
            else
                hTable.Add(drow[colName], string.Empty);
        }


        foreach (DataRow dRow in duplicateList)
            dTable.Rows.Remove(dRow);

        return dTable;
    }
    public static string SpecifiedLenString(this string contents, int length)
    {
        if (contents.Length > length)
            return contents.Substring(0, length);
        return contents;
    }


    public static string Encrypt(string key)
    {
        string encryptedstring;
        byte[] data;
        data = System.Text.ASCIIEncoding.ASCII.GetBytes(key);
        encryptedstring = Convert.ToBase64String(data);
        return encryptedstring;
    }
    public static string Decrypt(string key)
    {
        byte[] data;
        string decryptedstring;
        data = Convert.FromBase64String(key);
        decryptedstring = System.Text.ASCIIEncoding.ASCII.GetString(data);
        return decryptedstring;
    }

    public static DataTable filterTable(DataTable dt, string column, string val)
    {
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                DataTable filteredTable = dt.Clone();
                var rows = dt.AsEnumerable()
                    .Where(r => r.Field<string>(column) == val);
                if (rows.Any())
                    filteredTable = rows.CopyToDataTable();
                return filteredTable;
            }
        }
        return dt;
    }


    public static void addlog(string Action, string Module, string Description, string TableName, int? PrimeryKeyID = null)
    {
        string msg = "";
        DBQueries objDB = new DBQueries();
        msg = objDB.AddLog(Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString()), HttpContext.Current.Session["UserName"].ToString(), Action, Module, Description, Convert.ToInt32(HttpContext.Current.Session["CompanyID"].ToString()), TableName, PrimeryKeyID, HttpContext.Current.Session["LoginIP"].ToString(), HttpContext.Current.Session["LoginCountryName"].ToString(), HttpContext.Current.Session["LoginCityName"].ToString(), HttpContext.Current.Session["LoginState"].ToString(), HttpContext.Current.Session["Loginlatitude"].ToString(), HttpContext.Current.Session["Loginlongitude"].ToString());
    }

    public static void addlogNew(string Action, string Module, string Description, string DocumentViewPageLink, string DocumentManagePageLink, string NotificationMsg, string TableName, string DocName, int? ess = null, int? PrimeryKeyID = null)
    {
        string msg = "";
        DBQueries objDB = new DBQueries();
        msg = objDB.AddLogNew(Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString()), HttpContext.Current.Session["UserName"].ToString(), Action, Module, Description, Convert.ToInt32(HttpContext.Current.Session["CompanyID"].ToString()), TableName, PrimeryKeyID, HttpContext.Current.Session["LoginIP"].ToString(), HttpContext.Current.Session["LoginCountryName"].ToString(), HttpContext.Current.Session["LoginCityName"].ToString(), HttpContext.Current.Session["LoginState"].ToString(), HttpContext.Current.Session["Loginlatitude"].ToString(), HttpContext.Current.Session["Loginlongitude"].ToString(), DocumentViewPageLink, DocumentManagePageLink, NotificationMsg, DocName, ess);
    }



    public static DataTable filterTable(DataTable dt, string Fromproject, string fromDate, string toDate, string DocStatus, string Toproject = "0")
    {
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                dt = ReversefilterTable(dt, "DocStatus", "Saved as Draft");
                DataTable filteredTable = dt;

                if (Fromproject != "0" && Fromproject != null && Fromproject != "")
                {
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                         .Where(r => r.Field<int>("FromSite") == int.Parse(Fromproject));
                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();
                    filteredTable = tempdt;
                }

                if (Toproject != "0" && Toproject != null && Toproject != "")
                {
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                         .Where(r => r.Field<int>("ToSite") == int.Parse(Toproject));
                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();
                    filteredTable = tempdt;
                }


                if (fromDate != "" && fromDate != null)
                {
                    DateTime FD = DateTime.Parse(fromDate);
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                        .Where(r => r.Field<DateTime>("PreparedDate").Date >= FD.Date);
                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();

                    filteredTable = tempdt;
                }


                if (toDate != "" && toDate != null)
                {
                    DateTime TD = DateTime.Parse(toDate);
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                        .Where(r => r.Field<DateTime>("PreparedDate").Date <= TD.Date);

                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();
                    filteredTable = tempdt;

                }
                if (DocStatus != "0" && DocStatus != null)
                {
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                        .Where(r => r.Field<string>("DocStatus") == DocStatus);

                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();
                    filteredTable = tempdt;
                }

                return filteredTable;
            }
        }
        return dt;
    }

    public static DataTable filterTable(DataTable dt, string fromDate, string toDate, string DocStatus, string DateColumn = "CreatedDate")
    {
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                //dt = ReversefilterTable(dt, "DOC_STATUS", "Saved as Draft");
                DataTable filteredTable = dt;


                if (fromDate != "" && fromDate != null)
                {
                    DateTime FD = DateTime.Parse(fromDate);
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                        .Where(r => r.Field<DateTime>(DateColumn).Date >= FD.Date);
                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();

                    filteredTable = tempdt;
                }


                if (toDate != "" && toDate != null)
                {
                    DateTime TD = DateTime.Parse(toDate);
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                        .Where(r => r.Field<DateTime>(DateColumn).Date <= TD.Date);

                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();
                    filteredTable = tempdt;

                }
                if (DocStatus != "0" && DocStatus != null)
                {
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                        .Where(r => r.Field<string>("DocStatus") == DocStatus);

                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();
                    filteredTable = tempdt;
                }



                return filteredTable;
            }
        }
        return dt;
    }

    public static DataTable filterTable5(DataTable dt, string fromDate, string toDate, string DocStatus, string DateColumn = "PRDate")
    {
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                //dt = ReversefilterTable(dt, "DOC_STATUS", "Saved as Draft");
                DataTable filteredTable = dt;


                if (fromDate != "" && fromDate != null)
                {
                    DateTime FD = DateTime.Parse(fromDate);
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                        .Where(r => r.Field<DateTime>(DateColumn).Date >= FD.Date);
                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();

                    filteredTable = tempdt;
                }


                if (toDate != "" && toDate != null)
                {
                    DateTime TD = DateTime.Parse(toDate);
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                        .Where(r => r.Field<DateTime>(DateColumn).Date <= TD.Date);

                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();
                    filteredTable = tempdt;

                }
                if (DocStatus != "0" && DocStatus != null)
                {
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                        .Where(r => r.Field<string>("DocStatus") == DocStatus);

                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();
                    filteredTable = tempdt;
                }



                return filteredTable;
            }
        }
        return dt;
    }
    public static DataTable filterTable2(DataTable dt, string ColumnValue, string ColumnName = "SupplierID")
    {
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                DataTable filteredTable = dt;

                if (ColumnValue != "0" && ColumnValue != null)
                {
                    DataTable tempdt = dt.Clone();
                    var rows = filteredTable.AsEnumerable()
                        .Where(r => r.Field<int>(ColumnName) == int.Parse(ColumnValue));

                    if (rows.Any())
                        tempdt = rows.CopyToDataTable();
                    filteredTable = tempdt;
                }

                return filteredTable;
            }
        }
        return dt;
    }


    public static DataTable ReversefilterTable(DataTable dt, string column, string val)
    {
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                DataTable filteredTable = dt.Clone();
                var rows = dt.AsEnumerable()
                    .Where(r => r.Field<string>(column) != val);
                if (rows.Any())
                    filteredTable = rows.CopyToDataTable();

                return filteredTable;

            }
        }

        return dt;

    }

    public static string addAccessLevels(string btnType, string tableName, string primaryColumnName, string primaryColumnValue, string userName)
    {
        string msg = "";
        DBQueries objDB = new DBQueries();

        if (btnType == "btnSubForReview")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "PreparedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "PreparedDate";
            objDB.DocStatus = "Data Submitted for Review";
            objDB.UpdateDocStatus();

            msg = "Data Submitted for Review";
        }
        else if (btnType == "btnReview")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ReviewedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ReviewedDate";
            objDB.DocStatus = "Reviewed";

            objDB.UpdateDocStatus();
            msg = "Reviewed Sucessfull";

        }
        else if (btnType == "btnApprove")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ApprovedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ApprovedDate";
            objDB.DocStatus = "Approved";

            objDB.UpdateDocStatus();
            msg = "Approved Sucessfull";
        }
        else if (btnType == "btnRevApprove")
        {

            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ReviewedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ReviewedDate";
            objDB.DocStatus = "Reviewed";

            objDB.UpdateDocStatus();

            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ApprovedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ApprovedDate";
            objDB.DocStatus = "Approved";

            objDB.UpdateDocStatus();

            msg = "Reviewed & Approved Sucessfull";
        }
        else if (btnType == "btnRejDisApprove")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ReviewedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ReviewedDate";
            objDB.DocStatus = "Rejected";

            objDB.UpdateDocStatus();

            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ApprovedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ApprovedDate";
            objDB.DocStatus = "Disapproved";

            objDB.UpdateDocStatus();

            msg = "Rejected & Disapproved Sucessfull";
        }
        else if (btnType == "btnDisapprove")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ApprovedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ApprovedDate";
            objDB.DocStatus = "Disapproved";

            objDB.UpdateDocStatus();

            msg = "Disapproved";
        }
        else if (btnType == "Reject")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ReviewedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ReviewedDate";
            objDB.DocStatus = "Rejected";

            objDB.UpdateDocStatus();
            msg = "Rejected";
        }

        else if (btnType == "Delete")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "DeletedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "DeletedDate";
            objDB.DocStatus = "Deleted";

            objDB.DeleteDraft();


            msg = "Deleted Succesfully";
        }
        else if (btnType == "btnApproveByProcurement")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ApprovedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ApprovedDate";
            objDB.DocStatus = "Approved By Procurement";

            objDB.UpdateDocStatus();
            msg = "Approved Sucessfull";
        }
        else if (btnType == "btnApproveByHR")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ApprovedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ApprovedDate";
            objDB.DocStatus = "Approved By HR";

            objDB.UpdateDocStatus();
            msg = "Approved Sucessfull";
        }

        else if (btnType == "btnRevApproveByProcurement")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ReviewedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ReviewedDate";
            objDB.DocStatus = "Reviewed";
            objDB.UpdateDocStatus();

            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ApprovedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ApprovedDate";
            objDB.DocStatus = "Approved By Procurement";

            objDB.UpdateDocStatus();
            msg = "Approved Sucessfull";
        }
        else if (btnType == "btnRevApproveByHR")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ReviewedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ReviewedDate";
            objDB.DocStatus = "Reviewed";
            objDB.UpdateDocStatus();

            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ApprovedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ApprovedDate";
            objDB.DocStatus = "Approved By HR";

            objDB.UpdateDocStatus();
            msg = "Approved Sucessfull";
        }
        else if (btnType == "btnSubForReviewByFinance")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "PreparedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "PreparedDate";
            objDB.DocStatus = "Data Submitted for Review By Finance";
            objDB.UpdateDocStatus();

            msg = "Data Submitted for Review";
        }
        else if (btnType == "btnReviewByFinance")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ReviewedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ReviewedDate";
            objDB.DocStatus = "Reviewed By Finance";

            objDB.UpdateDocStatus();
            msg = "Reviewed Sucessfull";

        }
        else if (btnType == "btnApproveByFinance")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ApprovedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ApprovedDate";
            objDB.DocStatus = "Approved By Finance";

            objDB.UpdateDocStatus();
            msg = "Approved Sucessfull";
        }

        else if (btnType == "btnRevApproveByFinance")
        {
            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ReviewedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ReviewedDate";
            objDB.DocStatus = "Reviewed By Finance";

            objDB.UpdateDocStatus();

            objDB.TableName = tableName;
            objDB.PrimaryColumnnName = primaryColumnName;
            objDB.PrimaryColumnValue = primaryColumnValue;
            objDB.UpdateColumnName = "ApprovedBy";
            objDB.UpdateColumnValue = userName;
            objDB.DateColumnName = "ApprovedDate";
            objDB.DocStatus = "Approved By Finance";

            objDB.UpdateDocStatus();

            msg = "Reviewed & Approved Sucessfull";
        }


        return msg;
    }

    public static string GetDocNotesInString(DataTable dt)
    {
        string returnMsg = "";

        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                string table = "";
                table += "<table class='notesTable'>";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    table += "<tr><td>" + dt.Rows[i]["CreatedDate"] + "</td><td>" + dt.Rows[i]["CreatedBy"] + "</td><td>" + dt.Rows[i]["Notes"] + "</td></tr>";
                }
                table += "</table>";
                returnMsg = table;
            }
            else
            {
                returnMsg = "";
            }
        }
        else
        {
            returnMsg = "";
        }

        return returnMsg;
    }

    public static bool getCheckBoxValue(string val)
    {
        if (val == "True")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool isDataTableNullOrRowsZero(DataTable dt)
    {
        bool val = false;
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                val = true;
            }
        }
        return val;
    }
    public static string GetTemplate(GridView gd)
    {
        try
        {
            StringBuilder sheetBody = new StringBuilder();
            StringWriter sw = new StringWriter(sheetBody);
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gd.RenderControl(hw);
            return sheetBody.ToString();
        }
        catch (Exception ex)
        {
            return "";
        }

    }
    public static void generatePDF(string strHeader, string strFooter, string strContent, string strFileName, string strPageSize, string strPageOrientation, int headerSize = 100, int footerSize = 100)
    {
        // create a new pdf document converting an url
        PdfDocument doc = GetPDFDocumentToSave(strHeader, strFooter, strContent, strPageSize, strPageOrientation, headerSize, footerSize);
       
        // save pdf document
        doc.Save(HttpContext.Current.Response, false, strFileName + ".pdf");
       
        // close pdf document
        doc.Close();
    }

    public static void GeneratePDFFileWithPath(string strHeader, string strFooter, string strContent, string strFileName, string strPageSize, string strPageOrientation, int headerSize, int footerSize, string strFilePath)
    {
        // create a new pdf document converting an url
        PdfDocument doc = GetPDFDocumentToSave(strHeader, strFooter, strContent, strPageSize, strPageOrientation, headerSize, footerSize);

        // save pdf document
        doc.Save($@"{strFilePath}{strFileName}.pdf");

        // close pdf document
        doc.Close();
    }

    public static byte[] GeneratePDFFileBytes(string strHeader, string strFooter, string strContent, string strPageSize, string strPageOrientation, int headerSize, int footerSize)
    {
        // create a new pdf document converting an url
        PdfDocument doc = GetPDFDocumentToSave(strHeader, strFooter, strContent, strPageSize, strPageOrientation, headerSize, footerSize);

        // save pdf document
        return doc.Save();
    }

    public static DataTable excelToDataTable(string filePath)
    {
        DataTable dtexcel = new DataTable();
        bool hasHeaders = false;
        string HDR = hasHeaders ? "Yes" : "No";
        string strConn;
        if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
            strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
        else
            strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
        OleDbConnection conn = new OleDbConnection(strConn);
        conn.Open();
        DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
        //Looping Total Sheet of Xl File
        /*foreach (DataRow schemaRow in schemaTable.Rows)
        {
        }*/
        //Looping a first Sheet of Xl File
        DataRow schemaRow = schemaTable.Rows[0];
        string sheet = schemaRow["TABLE_NAME"].ToString();
        if (!sheet.EndsWith("_"))
        {
            string query = "SELECT  * FROM [Sheet$]";
            OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
            dtexcel.Locale = CultureInfo.CurrentCulture;
            daexcel.Fill(dtexcel);
        }

        conn.Close();
        return dtexcel;

    }

    public static DataTable CalculateBookValue(DataTable dt, string ud_FinallDate = "not_define", string DepMethodColumnName = "DEPRECIATION_METHOD", string AvgMethodColumnName = "AVERAGE_METHOD", string DepPerYearColumn = "DEP_PER_YEAR", string DepStartDateColumn = "DEPRECIATION_START_DATE", string netAmountColumn = "NETAMOUNT", string DepRateColumn = "DEPRECIATION_RATE", string EffectiveLifeColumn = "EFFECTIVE_LIFE", string ResidualRateColumn = "RESIDUAL_RATE")
    {
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                string errorMsg = "";
                dt.Columns.Add("CAL_DEP_VALUE");
                dt.Columns.Add("CAL_BOOK_VALUE");
                dt.AcceptChanges();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtTemp = new DataTable();
                    DBQueries objDB = new DBQueries();
                    objDB.AssetID = Convert.ToInt32(dt.Rows[i]["ASSET_ID"].ToString());
                    dtTemp = objDB.GetAssetRevalutionByAseetID(ref errorMsg);


                    if (dtTemp != null)
                    {
                        if (dtTemp.Rows.Count > 0)
                        {
                            //DepMtd = dtTemp.Rows[0][DepMethodColumnName].ToString();
                            //AvgMtd = dtTemp.Rows[0][AvgMethodColumnName].ToString();
                            //DepStrtDate = DateTime.Parse(dtTemp.Rows[0][DepStartDateColumn].ToString());
                            //netAmount = Convert.ToDouble(dtTemp.Rows[0][netAmountColumn].ToString());
                            //DepPerYear = double.Parse(dtTemp.Rows[0][DepPerYearColumn].ToString());
                            dt.Rows[i][EffectiveLifeColumn] = dtTemp.Rows[0][EffectiveLifeColumn].ToString();
                            dt.Rows[i][DepMethodColumnName] = dtTemp.Rows[0][DepMethodColumnName].ToString();
                            dt.Rows[i][AvgMethodColumnName] = dtTemp.Rows[0][AvgMethodColumnName].ToString();
                            dt.Rows[i][DepStartDateColumn] = dtTemp.Rows[0][DepStartDateColumn].ToString();
                            dt.Rows[i][netAmountColumn] = dtTemp.Rows[0][netAmountColumn].ToString();
                            dt.Rows[i][DepPerYearColumn] = dtTemp.Rows[0][DepPerYearColumn].ToString();
                            dt.Rows[i][DepRateColumn] = dtTemp.Rows[0][DepRateColumn].ToString();
                            dt.Rows[i][ResidualRateColumn] = dtTemp.Rows[0][ResidualRateColumn].ToString();
                        }
                    }

                    string DepMtd = dt.Rows[i][DepMethodColumnName].ToString();
                    string AvgMtd = dt.Rows[i][AvgMethodColumnName].ToString();
                    DateTime DepStrtDate = DateTime.Parse(dt.Rows[i][DepStartDateColumn].ToString());
                    double netAmount = Convert.ToDouble(dt.Rows[i][netAmountColumn].ToString());
                    double resRate = Convert.ToDouble(dt.Rows[i][ResidualRateColumn].ToString());
                    if (resRate <= 0)
                    {
                        resRate = 1;
                    }
                    double DepRate = Convert.ToDouble(dt.Rows[i][DepRateColumn].ToString());
                    double DepPerYear = (dt.Rows[i][DepPerYearColumn].ToString() == "") ? 0 : double.Parse(dt.Rows[i][DepPerYearColumn].ToString());

                    if (dt.Rows[i]["NON_IDLE_DATE"].ToString() != "")
                    {
                        DepStrtDate = DateTime.Parse(dt.Rows[i]["NON_IDLE_DATE"].ToString());
                        netAmount = Convert.ToDouble(dt.Rows[i]["BOOK_VALUE"].ToString());
                    }


                    double DepVal = 0.0;
                    double BookVal = 0.0;
                    double TotDays = 0.0;
                    double TotMon = 0.0;

                    if (DepMtd != "No Depreciation")
                    {
                        DateTime stDate = DateTime.Now;
                        if (ud_FinallDate != "not_define")
                        {
                            stDate = DateTime.Parse(ud_FinallDate);
                        }
                        DateTime endDate = DepStrtDate;


                        if (DepMtd == "Full Depreciation")
                        {
                            DepPerYear = netAmount;

                            if (AvgMtd != "Actual Days")
                            {
                                DepVal = (int)(((stDate.Year - endDate.Year) * 12) + stDate.Month - endDate.Month);
                                DepVal = DepVal * (DepPerYear / 12.0);
                            }
                            else
                            {
                                DepVal = (int)(stDate - endDate).TotalDays;
                                DepVal = DepVal * (DepPerYear / 365.0);

                            }
                        }
                        else if (DepMtd == "Straight Line" && DepPerYear != 0)
                        {
                            if (AvgMtd != "Actual Days")
                            {
                                DepVal = (int)(((stDate.Year - endDate.Year) * 12) + stDate.Month - endDate.Month);
                                DepVal = DepVal * (DepPerYear / 12.0);
                            }
                            else
                            {
                                DepVal = (int)(stDate - endDate).TotalDays;
                                DepVal = DepVal * (DepPerYear / 365.0);

                            }
                        }
                        else if (DepMtd == "Reducing Balance" && DepPerYear != 0)
                        {
                            if (AvgMtd != "Actual Days")
                            {
                                TotMon = (int)(((stDate.Year - endDate.Year) * 12.0) + stDate.Month - endDate.Month);
                                double tempAmount = netAmount;
                                for (int j = 0; j < TotMon; j++)
                                {
                                    if (j % 12 == 0)
                                    {
                                        DepPerYear = (tempAmount * DepRate) / 100.0;
                                    }
                                    tempAmount = tempAmount - (DepPerYear / 12.0);
                                }

                                DepVal = netAmount - tempAmount;
                            }
                            else
                            {
                                TotDays = (int)(stDate - endDate).TotalDays;
                                double tempAmount = netAmount;
                                for (int j = 0; j < TotDays; j++)
                                {
                                    if (j % 365 == 0)
                                    {
                                        DepPerYear = (tempAmount * DepRate) / 100.0;
                                    }
                                    tempAmount = tempAmount - (DepPerYear / 365.0);
                                }

                                DepVal = netAmount - tempAmount;

                            }
                        }

                    }

                    if (dt.Rows[i]["NON_IDLE_DATE"].ToString() != "")
                    {
                        DepVal = DepVal + (Convert.ToDouble(dt.Rows[i][netAmountColumn].ToString()) - netAmount);
                        netAmount = Convert.ToDouble(dt.Rows[i][netAmountColumn].ToString());
                    }

                    if (DepVal >= (netAmount - resRate))
                    {
                        DepVal = netAmount - resRate;
                    }

                    BookVal = netAmount - DepVal;

                    if (DepVal < 0)
                    {
                        DepVal = 0;
                    }




                    dt.Rows[i]["CAL_DEP_VALUE"] = string.Format("{0:f2}", DepVal);
                    dt.Rows[i]["CAL_BOOK_VALUE"] = string.Format("{0:f2}", BookVal);
                }

            }
        }
        return dt;
    }



    public static DataTable CalculateCurrentBookValue(DataTable dt, string ud_FinallDate = "not_define", string DepMethodColumnName = "DEPRECIATION_METHOD", string AvgMethodColumnName = "AVERAGE_METHOD", string DepPerYearColumn = "DEP_PER_YEAR", string DepStartDateColumn = "DEPRECIATION_START_DATE", string netAmountColumn = "NETAMOUNT", string DepRateColumn = "DEPRECIATION_RATE", string EffectiveLifeColumn = "EFFECTIVE_LIFE", string ResidualRateColumn = "RESIDUAL_RATE")
    {
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                string errorMsg = "";
                dt.Columns.Add("CAL_DEP_VALUE");
                dt.Columns.Add("CAL_BOOK_VALUE");
                dt.Columns.Add("CURRENT_DEP_VALUE");
                dt.AcceptChanges();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtTemp = new DataTable();
                    DBQueries objDB = new DBQueries();
                    objDB.AssetID = Convert.ToInt32(dt.Rows[i]["ASSET_ID"].ToString());
                    dtTemp = objDB.GetAssetRevalutionByAseetID(ref errorMsg);


                    if (dtTemp != null)
                    {
                        if (dtTemp.Rows.Count > 0)
                        {
                            dt.Rows[i][EffectiveLifeColumn] = dtTemp.Rows[0][EffectiveLifeColumn].ToString();
                            dt.Rows[i][DepMethodColumnName] = dtTemp.Rows[0][DepMethodColumnName].ToString();
                            dt.Rows[i][AvgMethodColumnName] = dtTemp.Rows[0][AvgMethodColumnName].ToString();
                            dt.Rows[i][DepStartDateColumn] = dtTemp.Rows[0][DepStartDateColumn].ToString();
                            dt.Rows[i][netAmountColumn] = dtTemp.Rows[0][netAmountColumn].ToString();
                            dt.Rows[i][DepPerYearColumn] = dtTemp.Rows[0][DepPerYearColumn].ToString();
                            dt.Rows[i][DepRateColumn] = dtTemp.Rows[0][DepRateColumn].ToString();
                            dt.Rows[i][ResidualRateColumn] = dtTemp.Rows[0][ResidualRateColumn].ToString();
                        }
                    }

                    string DepMtd = dt.Rows[i][DepMethodColumnName].ToString();
                    string AvgMtd = dt.Rows[i][AvgMethodColumnName].ToString();
                    DateTime DepStrtDate = DateTime.Parse(dt.Rows[i][DepStartDateColumn].ToString());
                    double netAmount = Convert.ToDouble(dt.Rows[i][netAmountColumn].ToString());
                    double resRate = Convert.ToDouble(dt.Rows[i][ResidualRateColumn].ToString());
                    if (resRate <= 0)
                    {
                        resRate = 1;
                    }
                    double DepRate = Convert.ToDouble(dt.Rows[i][DepRateColumn].ToString());
                    double DepPerYear = (dt.Rows[i][DepPerYearColumn].ToString() == "") ? 0 : double.Parse(dt.Rows[i][DepPerYearColumn].ToString());

                    if (dt.Rows[i]["NON_IDLE_DATE"].ToString() != "")
                    {
                        DepStrtDate = DateTime.Parse(dt.Rows[i]["NON_IDLE_DATE"].ToString());
                        netAmount = Convert.ToDouble(dt.Rows[i]["BOOK_VALUE"].ToString());
                    }


                    double DepVal = 0.0;
                    double BookVal = 0.0;
                    double TotDays = 0.0;
                    double TotMon = 0.0;

                    if (DepMtd != "No Depreciation")
                    {
                        DateTime stDate = DateTime.Now;
                        if (ud_FinallDate != "not_define")
                        {
                            stDate = DateTime.Parse(ud_FinallDate);
                        }
                        DateTime endDate = DepStrtDate;


                        if (DepMtd == "Full Depreciation")
                        {
                            DepPerYear = netAmount;

                            if (AvgMtd != "Actual Days")
                            {
                                DepVal = (int)(((stDate.Year - endDate.Year) * 12) + stDate.Month - endDate.Month);
                                DepVal = DepVal * (DepPerYear / 12.0);
                            }
                            else
                            {
                                DepVal = (int)(stDate - endDate).TotalDays;
                                DepVal = DepVal * (DepPerYear / 365.0);

                            }
                        }
                        else if (DepMtd == "Straight Line" && DepPerYear != 0)
                        {
                            if (AvgMtd != "Actual Days")
                            {
                                DepVal = (int)(((stDate.Year - endDate.Year) * 12) + stDate.Month - endDate.Month);
                                DepVal = DepVal * (DepPerYear / 12.0);
                            }
                            else
                            {
                                DepVal = (int)(stDate - endDate).TotalDays;
                                DepVal = DepVal * (DepPerYear / 365.0);

                            }
                        }
                        else if (DepMtd == "Reducing Balance" && DepPerYear != 0)
                        {
                            if (AvgMtd != "Actual Days")
                            {
                                TotMon = (int)(((stDate.Year - endDate.Year) * 12.0) + stDate.Month - endDate.Month);
                                double tempAmount = netAmount;
                                for (int j = 0; j < TotMon; j++)
                                {
                                    if (j % 12 == 0)
                                    {
                                        DepPerYear = (tempAmount * DepRate) / 100.0;
                                    }
                                    tempAmount = tempAmount - (DepPerYear / 12.0);
                                }

                                DepVal = netAmount - tempAmount;
                            }
                            else
                            {
                                TotDays = (int)(stDate - endDate).TotalDays;
                                double tempAmount = netAmount;
                                for (int j = 0; j < TotDays; j++)
                                {
                                    if (j % 365 == 0)
                                    {
                                        DepPerYear = (tempAmount * DepRate) / 100.0;
                                    }
                                    tempAmount = tempAmount - (DepPerYear / 365.0);
                                }

                                DepVal = netAmount - tempAmount;

                            }
                        }

                    }

                    if (dt.Rows[i]["NON_IDLE_DATE"].ToString() != "")
                    {
                        DepVal = DepVal + (Convert.ToDouble(dt.Rows[i][netAmountColumn].ToString()) - netAmount);
                        netAmount = Convert.ToDouble(dt.Rows[i][netAmountColumn].ToString());
                    }

                    if (DepVal >= (netAmount - resRate))
                    {
                        DepVal = netAmount - resRate;
                    }

                    BookVal = netAmount - DepVal;

                    if (DepVal < 0)
                    {
                        DepVal = 0;
                    }


                    double CurDepVal = DepVal - Convert.ToDouble(dt.Rows[i]["LastDepriciationAmount"].ToString());

                    dt.Rows[i]["CAL_DEP_VALUE"] = string.Format("{0:f2}", DepVal);
                    dt.Rows[i]["CURRENT_DEP_VALUE"] = string.Format("{0:f2}", CurDepVal);
                    dt.Rows[i]["CAL_BOOK_VALUE"] = string.Format("{0:f2}", BookVal);
                }

            }
        }
        return dt;
    }



    public static DataTable CalculateTaxBookValue(DataTable dt, string ud_FinallDate = "not_define", string DepMethodColumnName = "TAX_DEP_METHOD", string ISActualDaysColumn = "IS_ACTUAL_DAYS", string isFullMonth = "IS_FULL_MONTH", string DepPerYearColumn = "TAX_DEP_PER_YEAR", string DepStartDateColumn = "DEPRECIATION_START_DATE", string netAmountColumn = "NETAMOUNT", string DepRateColumn = "TAX_DEP_PER", string EffectiveLifeColumn = "EFFECTIVE_LIFE", string ResidualRateColumn = "RESIDUAL_RATE")
    {
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                string errorMsg = "";
                dt.Columns.Add("CAL_DEP_VALUE");
                dt.Columns.Add("CAL_BOOK_VALUE");
                dt.AcceptChanges();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtTemp = new DataTable();
                    DBQueries objDB = new DBQueries();
                    objDB.AssetID = Convert.ToInt32(dt.Rows[i]["ASSET_ID"].ToString());
                    dtTemp = objDB.GetAssetRevalutionByAseetID(ref errorMsg);

                    if (dtTemp != null)
                    {
                        if (dtTemp.Rows.Count > 0)
                        {
                            dt.Rows[i][EffectiveLifeColumn] = dtTemp.Rows[0][EffectiveLifeColumn].ToString();
                            dt.Rows[i][DepMethodColumnName] = dtTemp.Rows[0][DepMethodColumnName].ToString();

                            dt.Rows[i][DepStartDateColumn] = dtTemp.Rows[0][DepStartDateColumn].ToString();
                            dt.Rows[i][netAmountColumn] = dtTemp.Rows[0][netAmountColumn].ToString();
                            dt.Rows[i][DepPerYearColumn] = dtTemp.Rows[0][DepPerYearColumn].ToString();
                            dt.Rows[i][DepRateColumn] = dtTemp.Rows[0][DepRateColumn].ToString();
                            dt.Rows[i][ResidualRateColumn] = dtTemp.Rows[0][ResidualRateColumn].ToString();
                        }
                    }


                    string DepMtd = dt.Rows[i][DepMethodColumnName].ToString();
                    string AvgMtd = "";

                    if (dt.Rows[i][ISActualDaysColumn].ToString() == "1")
                    {
                        AvgMtd = "Actual Days";
                    }
                    else if (dt.Rows[i][isFullMonth].ToString() == "1")
                    {
                        AvgMtd = "Full Month";
                    }
                    DateTime DepStrtDate = DateTime.Parse(dt.Rows[i][DepStartDateColumn].ToString());
                    double netAmount = Convert.ToDouble(dt.Rows[i][netAmountColumn].ToString());
                    double resRate = Convert.ToDouble(dt.Rows[i][ResidualRateColumn].ToString());
                    if (resRate <= 0)
                    {
                        resRate = 1;
                    }
                    double DepRate = Convert.ToDouble(dt.Rows[i][DepRateColumn].ToString());
                    double InitialAllowance = ((Convert.ToDouble(dt.Rows[i]["INITIAL_ALLOWANCE"].ToString()) * netAmount) / 100.0);
                    double DepPerYear = (dt.Rows[i][DepPerYearColumn].ToString() == "") ? 0 : double.Parse(dt.Rows[i][DepPerYearColumn].ToString());

                    if (dt.Rows[i]["NON_IDLE_DATE"].ToString() != "")
                    {
                        DepStrtDate = DateTime.Parse(dt.Rows[i]["NON_IDLE_DATE"].ToString());
                        netAmount = Convert.ToDouble(dt.Rows[i]["TAX_BOOK_VALUE"].ToString());
                        InitialAllowance = 0;
                    }


                    double DepVal = 0.0;
                    double BookVal = 0.0;
                    double TotDays = 0.0;
                    double TotMon = 0.0;

                    if (DepMtd != "No Depreciation")
                    {
                        DateTime stDate = DateTime.Now;
                        DateTime endDate = DepStrtDate;

                        if (ud_FinallDate != "not_define")
                        {
                            stDate = DateTime.Parse(ud_FinallDate);
                        }


                        if (DepMtd == "Full Depreciation")
                        {
                            DepPerYear = netAmount;

                            if (AvgMtd != "Actual Days")
                            {
                                DepVal = (int)(((stDate.Year - endDate.Year) * 12) + stDate.Month - endDate.Month);
                                DepVal = DepVal * (DepPerYear / 12.0);
                            }
                            else
                            {
                                DepVal = (int)(stDate - endDate).TotalDays;
                                DepVal = DepVal * (DepPerYear / 365.0);

                            }
                            DepVal += InitialAllowance;
                        }
                        else if (DepMtd == "Straight Line" && DepPerYear != 0)
                        {
                            if (AvgMtd != "Actual Days")
                            {
                                DepVal = (int)(((stDate.Year - endDate.Year) * 12) + stDate.Month - endDate.Month);
                                DepVal = DepVal * (DepPerYear / 12.0);
                            }
                            else
                            {
                                DepVal = (int)(stDate - endDate).TotalDays;
                                DepVal = DepVal * (DepPerYear / 365.0);

                            }
                            DepVal += InitialAllowance;
                        }
                        else if (DepMtd == "Reducing Balance" && DepPerYear != 0)
                        {
                            if (AvgMtd != "Actual Days")
                            {
                                TotMon = (int)(((stDate.Year - endDate.Year) * 12.0) + stDate.Month - endDate.Month);
                                double tempAmount = (netAmount - InitialAllowance);
                                for (int j = 0; j < TotMon; j++)
                                {
                                    if (j % 12 == 0)
                                    {
                                        DepPerYear = (tempAmount * DepRate) / 100.0;
                                    }

                                    tempAmount = tempAmount - (DepPerYear / 12.0);
                                }

                                DepVal = netAmount - tempAmount;
                            }
                            else
                            {
                                TotDays = (int)(stDate - endDate).TotalDays;
                                double tempAmount = (netAmount - InitialAllowance);
                                for (int j = 0; j < TotDays; j++)
                                {
                                    if (j % 365 == 0)
                                    {
                                        DepPerYear = (tempAmount * DepRate) / 100.0;
                                    }

                                    tempAmount = tempAmount - (DepPerYear / 365.0);
                                }

                                DepVal = netAmount - tempAmount;

                            }
                        }

                    }
                    if (dt.Rows[i]["NON_IDLE_DATE"].ToString() != "")
                    {
                        DepVal = DepVal + (Convert.ToDouble(dt.Rows[i][netAmountColumn].ToString()) - netAmount);
                        netAmount = Convert.ToDouble(dt.Rows[i][netAmountColumn].ToString());
                    }
                    //DepVal+= InitialAllowance;
                    if (DepVal >= (netAmount - resRate))
                    {
                        DepVal = netAmount - resRate;
                    }

                    BookVal = netAmount - DepVal;

                    if (DepVal < 0)
                    {
                        DepVal = 0;
                    }



                    dt.Rows[i]["CAL_DEP_VALUE"] = string.Format("{0:f2}", DepVal);
                    dt.Rows[i]["CAL_BOOK_VALUE"] = string.Format("{0:f2}", BookVal);
                }

            }
        }
        return dt;
    }

    private static PdfDocument GetPDFDocumentToSave(string strHeader, string strFooter, string strContent, string strPageSize, string strPageOrientation, int hdrHeight, int ftrHeight)
    {
        SelectPdf.GlobalProperties.LicenseKey = "TWZ8bX94fG1+fHp0bXx1Y31tfnxjfH9jdHR0dA==";
        string pdf_page_size = strPageSize;
        PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
            pdf_page_size, true);

        string pdf_orientation = strPageOrientation;
        PdfPageOrientation pdfOrientation =
            (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
            pdf_orientation, true);

        int webPageWidth = 1024;
        int webPageHeight = 0;

        // instantiate a html to pdf converter object
        HtmlToPdf converter = new HtmlToPdf();

        // header settings
        int headerHeight = hdrHeight;
        bool showHeaderOnFirstPage = true;
        bool showHeaderOnOddPages = true;
        bool showHeaderOnEvenPages = true;

        converter.Options.DisplayHeader = showHeaderOnFirstPage ||
            showHeaderOnOddPages || showHeaderOnEvenPages;
        converter.Header.DisplayOnFirstPage = showHeaderOnFirstPage;
        converter.Header.DisplayOnOddPages = showHeaderOnOddPages;
        converter.Header.DisplayOnEvenPages = showHeaderOnEvenPages;
        converter.Header.Height = headerHeight;

        PdfHtmlSection headerHtml = new PdfHtmlSection(strHeader, "");
        headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
        converter.Header.Add(headerHtml);

        int footerHeight = ftrHeight;
        bool showFooterOnFirstPage = true;
        bool showFooterOnOddPages = true;
        bool showFooterOnEvenPages = true;

        converter.Options.DisplayFooter = showFooterOnFirstPage ||
            showFooterOnOddPages || showFooterOnEvenPages;
        converter.Footer.DisplayOnFirstPage = showFooterOnFirstPage;
        converter.Footer.DisplayOnOddPages = showFooterOnOddPages;
        converter.Footer.DisplayOnEvenPages = showFooterOnEvenPages;
        converter.Footer.Height = footerHeight;

        PdfHtmlSection footerHtml = new PdfHtmlSection(strFooter, "");
        footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
        converter.Footer.Add(footerHtml);

        // set converter options
        converter.Options.PdfPageSize = pageSize;
        converter.Options.PdfPageOrientation = pdfOrientation;
        converter.Options.WebPageWidth = webPageWidth;
        converter.Options.WebPageHeight = webPageHeight;

        // create a new pdf document converting an url
        PdfDocument doc = converter.ConvertHtmlString(strContent, "");

        return doc;
    }
}

