﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

public class DBQueries
{
    #region general 
    public string Date { get; set; }
    public string Title { get; set; }
    public string Notes { get; set; }
    public string Status { get; set; }
    public string SelectedEmp { get; set; }
    public string ModifiedBy { get; set; }
    public string SlackBy { get; set; }
    public string SlackOn { get; set; }

    public string Channel { get; set; }
    public string CreatedBy { get; set; }
    public string PreparedBy { get; set; }
    public string ApprovedBy { get; set; }
    public int IT3_ID { get; set; }
    public double ApproveAmount { get; set; }
    public string DeletedBy { get; set; }
    public string Comments { get; set; }
    public bool isApproved { get; set; }
    public int SheetID { get; set; }
    public string SheetName { get; set; }
    public string SheetType { get; set; }

    public int GroupID { get; set; }
    public string Groupname { get; set; }
    public decimal Generalovertime { get; set; }
    public decimal Offdayovertime { get; set; }
    public decimal Holidayovertime { get; set; }
    public decimal Eidovertime { get; set; }

    #endregion

    #region users
    public int UserID { get; set; }
    public int EmployeeID { get; set; }
    public string module { get; set; }
    public int FindIndirectMgr { get; set; }
    public int IndirectMgr { get; set; }
    public int directMgr { get; set; }

    public int createdby { get; set; }
    public int uploadedby { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public int SQuesID { get; set; }
    public string SQAnswer { get; set; }
    public string PasswordKey { get; set; }
    public string Ques1 { get; set; }
    public string Ans1 { get; set; }
    public string Ques2 { get; set; }
    public string Ans2 { get; set; }
    public string DocName { get; set; }
    public string AccessLevel { get; set; }
    public string Role { get; set; }
    public bool isNormUser { get; set; }
    public bool isApprover { get; set; }
    public bool isReviewer { get; set; }
    public bool isMgr { get; set; }
    public string AddUser()
    {
        return DBManager.NonQueryCommand("usp_TF_AddUser", new object[] { EmployeeID, Email, AccessLevel, CreatedBy });
    }
    public string AddUserFinance()
    {
        return DBManager.NonQueryCommand("usp_TF_AddUserFinance", new object[] { EmployeeID, Email, AccessLevel, CreatedBy, Role });
    }

    public DataTable CheckIfManager(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetReportingLine", new object[] { EmployeeID }, ref errorMsg);
    }
    public DataTable GetWorkdayID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetWorkdayID", new object[] { EmployeeID }, ref errorMsg);
    }
    public DataTable GetAccessLevel(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAccessLevel", new object[] { EmployeeID }, ref errorMsg);
    }
    public string UpdateUser()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateUser", new object[] { UserID, EmployeeID, Email, AccessLevel, ModifiedBy });
    }
    public string UpdateUserFinance()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateUserFinance", new object[] { UserID, EmployeeID, Email, AccessLevel, ModifiedBy, Role });
    }

    public string ResetUserPassword()
    {
        return DBManager.NonQueryCommand("usp_TF_ResetUserPassword", new object[] { UserID, ModifiedBy });
    }

    public string DeleteUser()
    {
        return DBManager.NonQueryCommand("usp_TF_DelUserByID", new object[] { UserID, DeletedBy });
    }

    public string AddUserAccess()
    {
        return DBManager.NonQueryCommand("usp_TF_AddUserAccess", new object[] { UserID, DocName, isNormUser, isApprover, isReviewer });
    }

    public string DelUserAccessByUserID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelUserAccessByUserID", new object[] { UserID });
    }

    public DataTable GetUserAccessByUserID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllUserAccess", new object[] { UserID }, ref errorMsg);
    }

    public DataTable GetUserLatLng(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetUserLatLng", new object[] { EmployeeID, Date }, ref errorMsg);
    }

    public DataTable GetAllUsersLatLng(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllUsersLatLng", new object[] { Date }, ref errorMsg);
    }

    public DataTable GetUserDetailsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetUserDetailsByID", new object[] { UserID }, ref errorMsg);
    }
    public DataTable GetUserDetailsByEmployeeIDFinance(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetUserDetailsByIDFinance", new object[] { EmployeeID }, ref errorMsg);
    }

    public DataTable AuthenticateUserByUserID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_AuthenticateUserByUserID", new object[] { UserID }, ref errorMsg);
    }

    public DataTable GetUsersByEmail(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetUsersByEmail", new object[] { Email }, ref errorMsg);
    }

    public DataTable GetAllUsersByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllUsersByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public string AddUserSecurityAnswers()
    {
        return DBManager.NonQueryCommand("usp_TF_AddUserSecurityAnswsers", new object[] { UserID, SQuesID, SQAnswer });
    }

    public string UpdatePassword()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdatePassword", new object[] { UserID, Password });
    }

    public DataTable VerifySecurityAnswers(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_VerifySecurityAnswers", new object[] { UserID, Ques1, Ans1, Ques2, Ans2 }, ref errorMsg);
    }

    public DataTable AuthenticateUser(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_AuthenticateUser", new object[] { Email, Password }, ref errorMsg);
    }
    public DataTable AuthenticateUserOKTA(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_AuthenticateUserOKTA", new object[] { Email }, ref errorMsg);
    }

    public DataTable GetUserDetailsByEmail(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetUserDetailsByEmail", new object[] { Email }, ref errorMsg);
    }

    public DataTable GetSecurityQuestionDD1(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetSQFirstDD", new object[] { }, ref errorMsg);
    }

    public DataTable GetSecurityQuestionDD2(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetSQSecondDD", new object[] { }, ref errorMsg);
    }

    public string AddPasswordKey()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPasswordKey", new object[] { UserID, Email, PasswordKey });
    }

    public DataTable GetPassowrdKeyByEmail(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPassowrdByEmail", new object[] { Email, PasswordKey }, ref errorMsg);
    }

    #endregion

    #region address
    public int CountryID { get; set; }
    public string CountryName { get; set; }
    public int StateID { get; set; }
    public string StateName { get; set; }
    public int CityID { get; set; }
    public string CityName { get; set; }
    public string AddressLine1 { get; set; }
    public string AddressLine2 { get; set; }
    public string ZIPCode { get; set; }

    public DataTable GetAllCounties(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCountries", new object[] { }, ref errorMsg);
    }

    public DataTable GetAllStatesByCountryID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllStatesByCountryID", new object[] { CountryID }, ref errorMsg);
    }

    public DataTable GetAllCitiesByStateID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCitiesByStateID", new object[] { StateID }, ref errorMsg);
    }
    public DataTable GetAllOvertimeGroup(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllOvertimeGroup", new object[] { CompanyID }, ref errorMsg);
    }
    #endregion

    #region Companies
    public int CompanyID { get; set; }
    public int ParentCompanyID { get; set; }
    public string CompanyName { get; set; }
    public string CompanyShortName { get; set; }
    public string CompanyDesc { get; set; }
    public string CompanyNTN { get; set; }
    public string CompanySTRN { get; set; }
    public string CompanySECP { get; set; }
    public string CompanyARN { get; set; }
    public string CompanySTN { get; set; }
    public string CompanyLogo { get; set; }
    public string CEOName { get; set; }

    public string AddCompany()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCompany", new object[] { CompanyName, CompanyShortName, CompanyDesc, ParentCompanyID, CompanyNTN, CompanySTRN, CompanySECP, CompanyARN, CompanySTN, CompanyLogo, CEOName, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, CreatedBy, CompanyCode });
    }
    public string UpdateCompany()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateCompany", new object[] { CompanyID, CompanyName, CompanyShortName, CompanyDesc, ParentCompanyID, CompanyNTN, CompanySTRN, CompanySECP, CompanyARN, CompanySTN, CompanyLogo, CEOName, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, ModifiedBy, CompanyCode });
    }

    public DataTable GetCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetByIDCompanies", new object[] { CompanyID }, ref errorMsg);
    }

  
    public DataTable GetNewCompanyCode(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNewCompanyCode", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable AddIT3FORM(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_Add_IT3_KT", new object[] { EmployeeID, FilePath, TotalClaim, PayRollCycle, CreatedBy, Status }, ref errorMsg);
    }

    public DataTable GetNewLocationCode(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNewLocationCode", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetCompaniesByParentID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCompaniesByParentCompany", new object[] { ParentCompanyID }, ref errorMsg);
    }

    public string DeleteCompany()
    {
        return DBManager.NonQueryCommand("usp_TF_DelCompanyByID", new object[] { CompanyID, DeletedBy });
    }
    #endregion

    #region Departments
    public int DeptID { get; set; }
    public string LocationName { get; set; }
    public string DepartmentID { get; set; }
    public string DeptName { get; set; }
    public string DeptDesc { get; set; }
    public int DeptParentID { get; set; }
    public string Department { get; set; }

    public string AddDepartment()
    {
        return DBManager.NonQueryCommand("usp_TF_AddDepartment", new object[] { CompanyID, DeptName, DeptDesc, DeptParentID, CreatedBy });
    }

    public string UpdateDepartment()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateDepartment", new object[] { DeptID, CompanyID, DeptName, DeptDesc, DeptParentID, ModifiedBy });
    }

    public DataTable GetDepartmentByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetByIDDepartments", new object[] { DeptID }, ref errorMsg);
    }

    public DataTable GetAllDepartmentsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDepartmentsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
	public string AutoAttendanceMark(List<string> markedempids, List<string> unmarkedempids)
	{
		List<SqlParameter> param = new List<SqlParameter>();
        param.Add(new SqlParameter("@companyid", CompanyID));
        DataTable dtemp = new DataTable();
        dtemp.Columns.Add("Val");
		foreach (string item in markedempids)
            dtemp.Rows.Add(item);
		SqlParameter empparam = new SqlParameter("@MarkedEmployeeID", SqlDbType.Structured);
		empparam.Value = dtemp;
		empparam.TypeName = "dbo.lstvarchar";
		param.Add(empparam);


        DataTable dtemp2 = new DataTable();
        dtemp2.Columns.Add("Val");
        foreach (string item in unmarkedempids)
            dtemp2.Rows.Add(item);
        empparam = new SqlParameter("@UnmarkedEmployeeID", SqlDbType.Structured);
        empparam.Value = dtemp2;
        empparam.TypeName = "dbo.lstvarchar";
        param.Add(empparam);
       
        return DBManager.NonQueryCommandParam("usp_TF_AutoAttendance", param.ToArray());
       

    }
	public DataTable GetAllAddonsHead(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAddonsHead", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAllDeductionsHead(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDeductionsHead", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAllLcoationsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLocation", new object[] { CompanyID }, ref errorMsg);
    }

    public string DeleteDepartment()
    {
        return DBManager.NonQueryCommand("usp_TF_DelDeptByID", new object[] { DeptID, DeletedBy });
    }
    #endregion

    #region CostCenters
    public int CostCenterID { get; set; }
    public string CostCenterName { get; set; }
    public string CostCenterDesc { get; set; }

    public string AddCostCenter()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCostCenter", new object[] { CompanyID, CostCenterName, CostCenterDesc, CreatedBy,CostCenterID });
    }

    public string UpdateCostCenter()                                                                                    
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateCostCenter", new object[] { CostCenterID, CompanyID, CostCenterName, CostCenterDesc, ModifiedBy });
    }

    public DataTable GetCostCenterByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetByIDCostCenter", new object[] { CostCenterID }, ref errorMsg);
    }

    public DataTable GetAllCostCentersByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCostCenterByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public string DeleteCostCenter()
    {
        return DBManager.NonQueryCommand("usp_TF_DelCostCenterByID", new object[] { CostCenterID, DeletedBy });
    }
    #endregion

    #region Workday Sheet Integration
    internal int MasterId { get; set; }
    public DataTable GetAllWorkDaySheets(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetWorkDaySheets", new object[] { }, ref errorMsg);
    }
    public DataTable GetAllWorkDaySheetDetails(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetWorkDaySheetDeatilsByMasterID", new object[] { MasterId }, ref errorMsg);
    }
    public string DeleteWorkDaySheetByID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelWorkDaySheet", new object[] { MasterId });
    }

    #endregion

    #region Grades
    public int GradeID { get; set; }
    public int NotificationID { get; set; }
    public string GradeName { get; set; }

    public string AddGrade()
    {
        return DBManager.NonQueryCommand("usp_TF_AddGrades", new object[] { CompanyID, GradeName, CreatedBy });
    }

    public string UpdateGrade()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateGrade", new object[] { GradeID, CompanyID, GradeName, ModifiedBy });
    }
    public string DeleteGrade()
    {
        return DBManager.NonQueryCommand("usp_TF_DelGradeByID", new object[] { GradeID, DeletedBy });
    }

    public string UpdateNotificationViewAll()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateNotificationViewAll", new object[] { UserID });
    }

    public string UpdateNotificationView()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateNotificationView", new object[] { NotificationID });
    }

    public DataTable GetGradeByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetByIDGrades", new object[] { GradeID }, ref errorMsg);
    }

    public DataTable GetAllGradesByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllGradesByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllNotificationsByUserID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllNotificationsByUserID", new object[] { UserID }, ref errorMsg);
    }
    #endregion

    #region grade leaves
    public string LeaveID { get; set; }
    public string LeaveTitle { get; set; }
    public int NoOfLeaves { get; set; }

    public string AddGradeLeaves()
    {
        return DBManager.NonQueryCommand("usp_TF_AddGradeLeaves", new object[] { GradeID, LeaveTitle, NoOfLeaves, CreatedBy });
    }
    public DataTable GetAllLeavesByGradeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllGradeLeavesByGradeID", new object[] { GradeID }, ref errorMsg);
    }
    public string DeleteLeavesByGradeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelLeavesByGradeID", new object[] { GradeID, DeletedBy });
    }
    #endregion

    #region Grade General KPIs
    public string KPIDesc { get; set; }
    public string KPIType { get; set; }

    public string AddGradeKPI()
    {
        return DBManager.NonQueryCommand("usp_TF_AddGeneralKPIs", new object[] { GradeID, KPIDesc, KPIType, CreatedBy });
    }

    public string DeleteGradeKPIByGradeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelGeneralKPIsByGradeID", new object[] { GradeID, DeletedBy });
    }

    public DataTable GetGeneralKPIsByGradeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetGeneralKPIsByGradeID", new object[] { GradeID }, ref errorMsg);
    }

    #endregion

    #region Designations
    public int DesgID { get; set; }
    public string DesgTitle { get; set; }

    public string AddDesignation()
    {
        return DBManager.NonQueryCommand("usp_TF_AddDesignation", new object[] { DeptID, DesgTitle, DirectReportTo, IndirectReportTo, CreatedBy });
    }

    public string UpdateDesignation()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateDesignation", new object[] { DesgID, DeptID, DesgTitle, DirectReportTo, IndirectReportTo, ModifiedBy });
    }
    public string DeleteDesignation()
    {
        return DBManager.NonQueryCommand("usp_TF_DelDesgByID", new object[] { DesgID, DeletedBy });
    }

    public DataTable GetDesignationByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetByIDDesignations", new object[] { DesgID }, ref errorMsg);
    }

    public DataTable GetAllDesignationByDepartmentID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDesignationsByDepartmentID", new object[] { DeptID }, ref errorMsg);
    }

    public DataTable GetAllDesignationByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDesignationsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllDesignations(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDesignations", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAllDesignationByCompanyIDForChart(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDesignationsByCompanyIDForChart", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllDesignationByDepartmentIDAndCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDesignationsByDepartmentIdAndCompanyId", new object[] { CompanyID, DeptID }, ref errorMsg);
    }
    public string AddNewDesignation()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewDesignation", new object[] { @CompanyID, DesgTitle, Description , DeptID, CreatedBy, DeptID });
    }
    public string AddJobDescreption()
    {
            return DBManager.NonQueryCommand("usp_TF_AddJobDescreption", new object[] { DesgID, DeptID, Name, Description, CreatedBy });
        }
   public string UpdateDesignationsByDesgID()
    {
            return DBManager.NonQueryCommand("usp_TF_UpdateDesignationByDesID", new object[] { DesgID, DesgTitle, Description, DeptID, CreatedBy });
        }
    public string UpdateJobDescByID()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateJobDescreption", new object[] { DesgID, Name, Description, JDID, ModifiedBy, Code });
    }
    public DataTable GetDesignationsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDesignationByID", new object[] { DesgID}, ref errorMsg);
    }
    public DataTable GetJobDescreptionByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetJobDescreptionByID", new object[] { JDID }, ref errorMsg);
    }
    public DataTable GetAllJobDescriptions(ref string errorMsg)
    {   
       return DBManager.DataTableCommand("usp_TF_GetAllJobDescriptions", new object[] {  },ref errorMsg);
    }
    public string DeleteJobDes()
    {
        return DBManager.NonQueryCommand("usp_TF_DelJobDescreption", new object[] { JDID, DeletedBy});
    }
    public string DisAppproveIT3()
    {
        return DBManager.NonQueryCommand("usp_TF_DisApproveIT3FormByIT3ID", new object[] { Remarks, IT3_ID, ApprovedBy });
    }

    public DataTable GetEmployeeYearlyTaxSlabs(ref string errorMsg, string TaxValue, string TaxYear)
    {
        return DBManager.DataTableCommand("USP_PF_GetYearlyTaxReportSlabs", new object[] { TaxValue, TaxYear }, ref errorMsg);
    }

    public DataTable GetIT3AdjustmentStatus(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetIT3FormStatus", new object[] { }, ref errorMsg);
    }

    public DataTable GetAllIT3Forms(List<string> EmployeeIDs, ref string errorMsg)
    {
        List<SqlParameter> param = new List<SqlParameter>();
        DataTable dtEmp = new DataTable();
        dtEmp.Columns.Add("Val");
        foreach (string item in EmployeeIDs)
            dtEmp.Rows.Add(item);
        SqlParameter empparam = new SqlParameter("@EmployeeID", SqlDbType.Structured);
        empparam.Value = dtEmp;
        empparam.TypeName = "dbo.lstvarchar";
        param.Add(empparam);
        param.Add(new SqlParameter("@Status", Status));
        return DBManager.DataSetCommand("usp_TF_GetIT3Submissions", param.ToArray(), ref errorMsg).Tables[0];
    }

    public string UpdateIT3Status(List<string> IT3IDs, ref string errorMsg)
    {
    List < SqlParameter > param = new List<SqlParameter>();
    DataTable dtEmp = new DataTable();
    dtEmp.Columns.Add("Val");
            foreach (string item in IT3IDs)
        dtEmp.Rows.Add(item);
    SqlParameter empparam = new SqlParameter("@IT3IDs", SqlDbType.Structured);
    empparam.Value = dtEmp;
    empparam.TypeName = "dbo.lstvarchar";
    param.Add(empparam);
    param.Add(new SqlParameter("@Status", Status));
    param.Add(new SqlParameter("@ApprovedBy", ApprovedBy));
            return DBManager.NonQueryCommandParam("usp_TF_UpdateIT3Status", param.ToArray());
    
        }
#endregion
    
    #region jds or orgchart
public int NodeID { get; set; }
    public string NodeTitle { get; set; }
    public int DirectReportTo { get; set; }
    public int IndirectReportTo { get; set; }
    public int ProbabtionTime { get; set; }
    public float EmployeeSalary { get; set; }
    public float BasicSalary { get; set; }
    public bool isActive { get; set; }


    public DataTable GetAllNodesByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllNodesByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
   
    public string AddNode()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNode", new object[] { NodeTitle, GradeID, DesgID, ProbabtionTime, isActive, BasicSalary, CreatedBy });
    }
    public string DeleteNode()
    {
        return DBManager.NonQueryCommand("usp_TF_DelNodeByID", new object[] { NodeID, DeletedBy });
    }
    
    public DataTable GetNodeByNodeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNodesByNodeID", new object[] { NodeID }, ref errorMsg);
    }

    public DataTable GetNodeByDesgID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNodesByDesgID", new object[] { DesgID }, ref errorMsg);
    }

    public string UpdateNode()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateNode", new object[] { NodeID, NodeTitle, GradeID, DesgID, ProbabtionTime, BasicSalary, ModifiedBy });
    }

    #endregion

    #region JDS Assets
    public string AssetTitle { get; set; }
    public int Quantity { get; set; }
    public string AssetDesc { get; set; }

    public string AddJDSAssets()
    {
        return DBManager.NonQueryCommand("usp_TF_AddJDAssets", new object[] { NodeID, AssetTitle, Quantity, AssetDesc, CreatedBy });
    }

    public string DeleteJDSAssetsByNodeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelJDAssetsByNodeID", new object[] { NodeID, DeletedBy });
    }
    public DataTable GetAllJDSAssetsByNodeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetJDAssetsByNodeID", new object[] { NodeID }, ref errorMsg);
    }
    #endregion

    #region JDS Budget
    public string BudgetTitle { get; set; }
    public string BudgetType { get; set; }
    public string Amount { get; set; }
  
    public string TransictionType { get; set; }

    public string AddJDSBudget()
    {
        return DBManager.NonQueryCommand("usp_TF_AddJDBudgets", new object[] { NodeID, BudgetTitle, BudgetType, Amount, TransictionType, CreatedBy });
    }

    public string DeleteJDSBudgetByNodeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelJDBudgetsByNodeID", new object[] { NodeID, DeletedBy });
    }
    public DataTable GetAllJDSBudgetByNodeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetJDBudgetsByNodeID", new object[] { NodeID }, ref errorMsg);
    }
	#endregion

	#region Loans 

    public string MainPuroposeID { get; set; }
    public string SelectedPuroposeID { get; set; }

    public string Findrateof { get; set; }
    public bool isAllowDeduction { get; set; }
    #endregion

    #region ApprovedProcedures

    public DataTable GetApproveAllWorkingShiftsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetApproveAllWorkingShiftsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID", new object[] { EmployeeID, CompanyID }, ref errorMsg);
    }
    public DataSet GetAllApprovedDirectIndirectReportedEmployeesByEmployeeID(ref string errorMsg, int numberOfRecord, int from)
    {
        return DBManager.DataSetCommand("usp_TF_GetAllApprovedDirectIndirectReportedEmployeesByEmployeeIDWithPagination", new object[] { EmployeeID ,numberOfRecord
, from}, ref errorMsg);
    }
    public DataTable GetIndirectReportedEmployeeByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetIndirectReportedEmployeeByEmployeeID", new object[] { FindIndirectMgr }, ref errorMsg);
    }
    public DataTable CheckIFDirectMgrBelongsToUnitedState(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_CheckIFDirectMgrBelongsToUnitedState", new object[] { FindIndirectMgr }, ref errorMsg);
    }
    public DataTable GetAllApproveEmployeesByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApproveEmployeesByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApproveEmployeesByCompanyIDandDptIDonly(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeesByCompanyIDandDeptID", new object[] { CompanyID, DeptID }, ref errorMsg);
    }

    public DataTable GetAllAutoAttendanceEmployeesByDeptID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("GetAllAutoAttendanceEmployeesByDeptID", new object[] { CompanyID, DeptID }, ref errorMsg);
    }
    public DataTable GetEmployeesIndex(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeesIndex", new object[] { CompanyID, DeptID, LocationName }, ref errorMsg);
    }
    public DataTable GetAllLeftEmployees(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLeftEmployees", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApproveEmployeesByCompanyIDonly(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApproveEmployeesByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApproveEmployeesByCompanyIDandDptID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeesByCompanyIDandDeptIDandLocation", new object[] { CompanyID , DeptID ,LocationName}, ref errorMsg);
    }
    
    public DataTable GetAllApproveEmployeesByCompanyIDandDptIDLocationOnly(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApproveEmployeesbyLocationOnly", new object[] { CompanyID,  LocationName }, ref errorMsg);
    }
    public DataTable GetAllApproveDepartmentbyLcoation(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDepartmentsByLocationandCompanyID", new object[] { CompanyID, LocationName }, ref errorMsg);
    }
    public DataTable GetAllApproveEmployeesAcceptHimByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApproveEmployeesAcceptHimByCompanyID", new object[] { CompanyID, EmployeeID }, ref errorMsg);
    }

    #endregion

    #region DocNotes

    public string AddDocNotes()
    {
        return DBManager.NonQueryCommand("usp_TF_AddDocNotes", new object[] { DocID, DocType, Notes, CreatedBy });
    }

    public string GetDocNotes()
    {
        string returnMsg = "";

        DataTable dt = new DataTable();
        dt = DBManager.DataTableCommand("usp_TF_GetDocNotes", new object[] { DocID, DocType }, ref returnMsg);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                string table = "";
                table += "<table class='notesTable'>";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    table += "<tr><td>" + dt.Rows[i]["CreatedDate"] + "</td><td>" + dt.Rows[i]["CreatedBy"] + "</td><td>" + dt.Rows[i]["Notes"] + "</td></tr>";
                }
                table += "</table>";
                returnMsg = table;
            }
            else
            {
                returnMsg = "";
            }
        }
        else
        {
            returnMsg = "";
        }

        return returnMsg;
    }

    public string GenerateExpenseItemTable()
    {
        string returnMsg = "";
        DataTable dt = new DataTable();

        dt = DBManager.DataTableCommand("usp_TF_GetAllExpenseItems", new object[] { ExpencesID }, ref returnMsg);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                string table = "";
                table += "<table class='item-table' style = 'width:100%;'>";
                table += "<tr><th>Item Name</th><th>COA</th><th>Price</th><th>Quantity</th><th>Net Amount</th></tr>";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    table += "<tr><td>" + dt.Rows[i]["ItemName"] + "</td><td>" + dt.Rows[i]["SubAccount"] + "</td><td>" + dt.Rows[i]["Price"] + "</td><td>" + dt.Rows[i]["Tax"] + "</td><td>" + dt.Rows[i]["NetAmount"] + "</td></tr>";
                }

                table += "</table>";
                returnMsg = table;
            }
            else
            {
                returnMsg = "";
            }
        }
        else
        {
            returnMsg = "";
        }

        return returnMsg;
    }

    #endregion

    #region new vacancy
    public int VacancyID { get; set; }
    public int PositionToBeFilledIn { get; set; }
    public string Education { get; set; }
    public string Experiencce { get; set; }

    public string AddVacancy()
    {
        return DBManager.NonQueryCommand("usp_TF_AddVacancy", new object[] { DesgID, PositionToBeFilledIn, Education, Experiencce, BasicSalary, CreatedBy });
    }

    public string UpdateVacancy()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateVacancy", new object[] { VacancyID, DesgID, PositionToBeFilledIn, Education, Experiencce, BasicSalary, ModifiedBy });
    }

    public DataTable GetVacancyByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetVacancyByID", new object[] { VacancyID }, ref errorMsg);
    }

    public DataSet GetVacancyByIDWithChildTables(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetVacancyByIDWithChildTables", new object[] { VacancyID }, ref errorMsg);
    }

    #endregion

    #region New Vacancy Assets
    public string AddNewVacancyAssets()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewVacancyAssets", new object[] { VacancyID, AssetTitle, Quantity, AssetDesc, CreatedBy });
    }
    public string DeleteNewVacancyAssetsByVacancyID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelNewVacancyAssetsByVacancyID", new object[] { VacancyID, DeletedBy });
    }
    public DataTable GetAllNewVacancyAssetsByVacancyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNewVacancyAssetsByVacancyID", new object[] { VacancyID }, ref errorMsg);
    }
    #endregion

    #region New Vacancy Budget

    public string AddNewVacancyBudget()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewVacancyBudgets", new object[] { VacancyID, BudgetTitle, BudgetType, Amount, TransictionType, CreatedBy });
    }

    public string DeleteNewVacancyBudgetByVacancyID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelNewVacancyBudgetsByVacancyID", new object[] { VacancyID, DeletedBy });
    }
    public DataTable GetAllNewVacancyBudgetByVacancyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNewVacancyBudgetsByVacancyID", new object[] { VacancyID }, ref errorMsg);
    }
    #endregion

    #region New Vacany SkillSet
    public string AddNewVacancySkillSet()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewVacancySkillSet", new object[] { VacancyID, Title, CreatedBy });
    }

    public string DeleteNewVacancySkillSetByVacancyID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelNewVacancySkillSetByVacancyID", new object[] { VacancyID, DeletedBy });
    }

    public DataTable GetAllNewVacancySkillSetByVacancyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNewVacancySkillSetByVacancyID", new object[] { VacancyID }, ref errorMsg);
    }

    #endregion

    #region candidates
    public int CandidateID { get; set; }
    public string CNIC { get; set; }
    public string Gender { get; set; }
    public string ContactNo { get; set; }
    public string Remarks { get; set; }
    public string FilePath { get; set; }
    public string FileType { get; set; }

    #endregion

    #region Trainings
    public int TrainingID { get; set; }
    public string TrainingDate { get; set; }
   
    public DataTable GetAllTrainingsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllTrainingsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllTrainingsByCompanyIDAndTrainingDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllTrainingsByCompanyIDAndDate", new object[] { CompanyID, TrainingDate }, ref errorMsg);
    }

    public string DeleteTraining()
    {
        return DBManager.NonQueryCommand("usp_TF_DelTraining", new object[] { TrainingID, DeletedBy });
    }

    #endregion

    #region Working Shift
    public int WorkingShiftID { get; set; }
    public string WorkingShiftName { get; set; }
    public string ShiftDayName { get; set; }
    public string StartTime { get; set; }
    public string EndTime { get; set; }
    public string LateInTime { get; set; }
    public string TimeInExemption { get; set; }
    public string BreakEarlyStartTime { get; set; }
    public string BreakStartTime { get; set; }
    public string BreakEndTime { get; set; }
    public string BreakLateTime { get; set; }
    public string HalfDayStart { get; set; }
    public string EarlyOut { get; set; }
    public string Hours { get; set; }
    public bool Monday { get; set; }
    public bool Tuesday { get; set; }
    public bool Wednesday { get; set; }
    public bool Thursday { get; set; }
    public bool Friday { get; set; }
    public bool Saturday { get; set; }
    public bool Sunday { get; set; }
    public bool isExpectedZero { get; set; }

    public bool isNightShift { get; set; }
    public bool isOffDayOT { get; set; }
    public int parentShiftID { get; set; }

    public string usp_TF_AddWorkingShiftDaysClient()
    {
        return DBManager.NonQueryCommand("usp_TF_AddWorkingShiftDaysClient", new object[] { ShiftID });
    }

    public string DeleteWorkingShift()
    {
        return DBManager.NonQueryCommand("usp_TF_DelWorkingShift", new object[] { WorkingShiftID, DeletedBy });
    }

    public DataTable GetWorkingShiftByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllWorkingShiftsByID", new object[] { WorkingShiftID }, ref errorMsg);
    }

    public DataTable GetWorkingShiftByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllWorkingShiftsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetWorkingShiftByCompanyIDParentOnly(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllWorkingShiftsByCompanyIDParentOnly", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetWorkingShiftByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllWorkingShiftsByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }

    public string UpdateShift()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateShift", new object[] { WorkingShiftID, CompanyID, WorkingShiftName, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, ModifiedBy, Hours });
    }

    public string AddShift()
    {
        return DBManager.NonQueryCommand("usp_TF_AddShift", new object[] { CompanyID, WorkingShiftName, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, CreatedBy, Hours });
    }

    public string AddShiftFromESS()
    {
        return DBManager.NonQueryCommand("usp_TF_AddShiftFromESS", new object[] { parentShiftID, EmployeeID, DeptID, CompanyID, WorkingShiftName, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, CreatedBy, Hours });
    }
    public string AllowWorkingHoliday()
    {
        return DBManager.NonQueryCommand("usp_TF_AllowWorkingHoliday", new object[] { ShiftID, isSpecial });
    }
    public string AddShiftFromESSNew1(List<string> EmployeeIDs)
    {
        List<SqlParameter> param = new List<SqlParameter>();
        param.Add(new SqlParameter("@ParentID",parentShiftID));
        DataTable dtEmp = new DataTable();
        dtEmp.Columns.Add("Val");
        foreach (string  item in EmployeeIDs)
            dtEmp.Rows.Add(item);
        SqlParameter empparam = new SqlParameter("@EmployeeID", SqlDbType.Structured);
        empparam.Value = dtEmp;
        empparam.TypeName = "dbo.lstvarchar";
        param.Add(empparam);       
        param.Add(new SqlParameter("@DeptID", DeptID));
        param.Add(new SqlParameter("@CompanyID", CompanyID));
        param.Add(new SqlParameter("@ShiftName", WorkingShiftName));
        param.Add(new SqlParameter("@Monday", Monday));
        param.Add(new SqlParameter("@Tuesday", Tuesday));
        param.Add(new SqlParameter("@Wednesday", Wednesday));
        param.Add(new SqlParameter("@Thursday", Thursday));
        param.Add(new SqlParameter("@Friday", Friday));
        param.Add(new SqlParameter("@Saturday", Saturday));
        param.Add(new SqlParameter("@Sunday", Sunday));
        param.Add(new SqlParameter("@CreatedBy", CreatedBy));
        param.Add(new SqlParameter("@Hours", Hours));
        param.Add(new SqlParameter("@LocationName", LocationName));
        return DBManager.NonQueryCommandParam("usp_TF_AddShiftFromESSNew1", param.ToArray());
    }

    public string AddWorkingShiftsDays()
    {
        return DBManager.NonQueryCommand("usp_TF_AddWorkingShiftsDays", new object[] { WorkingShiftID, ShiftDayName, StartTime, EndTime, LateInTime, HalfDayStart, EarlyOut, BreakEarlyStartTime, BreakStartTime, BreakEndTime, BreakLateTime, isNightShift, isExpectedZero, CreatedBy });
    }
    public string AddWorkingShiftsDays2()
    {
        return DBManager.NonQueryCommand("usp_TF_AddWorkingShiftsDays2", new object[] { WorkingShiftID, ShiftDayName, StartTime, EndTime, LateInTime, HalfDayStart, EarlyOut, BreakEarlyStartTime, BreakStartTime, BreakEndTime, BreakLateTime, isNightShift,isOffDayOT,isSpecial, isExpectedZero, CreatedBy, IsFlexible });
    }
    public string CheckWorkingShiftsHours()
    {
        return DBManager.NonQueryCommand("usp_TF_CheckWorkingShiftsHours", new object[] { StartTime, EndTime, BreakStartTime, BreakEndTime, isNightShift, isExpectedZero, Hours });
    }

    public string CheckWorkingShiftsDays()
    {
        return DBManager.NonQueryCommand("usp_TF_CheckWorkingShiftDays", new object[] { WorkingShiftID, ShiftDayName, StartTime, EndTime, isNightShift, isSpecial, isOverTime, TimeInExemption, isExpectedZero , IsFlexible });
    }

    public string DelWorkingShiftDaysByShiftID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelWorkingShiftDaysByShiftID", new object[] { WorkingShiftID });
    }
    public DataTable GetWorkingShiftDaysByShiftID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetWorkingShiftDaysByShiftID", new object[] { WorkingShiftID }, ref errorMsg);
    }
    public DataTable GetWorkingShiftDaysByShiftIDForESS(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetWorkingShiftDaysByShiftIDForESS", new object[] { WorkingShiftID }, ref errorMsg);
    }

    #endregion

    #region ToDo List
    public int ToDoListID { get; set; }

    public string AddToDoListItem()
    {
        return DBManager.NonQueryCommand("usp_TF_AddToDoListItem", new object[] { UserID, Description });
    }

    public string UpdateToDoListItem()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateToDoListItem", new object[] { ToDoListID, Description });
    }

    public string DeleteToDoListItem()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteToDoListItem", new object[] { ToDoListID });
    }

    public string UpdateToDoListItemStatus()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateToDoListItemStatus", new object[] { ToDoListID });
    }

    public DataTable GetToDoListByUserID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetToDoListByUserID", new object[] { UserID }, ref errorMsg);
    }
    
    public DataTable GetCompletedToDoListItemsByUserID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCompletedToDoListItemsByUserID", new object[] { UserID }, ref errorMsg);
    }
    
    public DataTable GetInCompleteToDoListItemsByUserID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetInCompleteToDoListItemsByUserID", new object[] { UserID }, ref errorMsg);
    }

    #endregion

    #region Attendace Shift
    public string TimeIn { get; set; }
    public string TimeOUt { get; set; }
    public string LateReason { get; set; }
    public string Resultant { get; set; }
    public string date { get; set; }
    public string mergedDateTime { get; set; }
    public int AttendanceID { get; set; }

    public string AddEmployeeAttendance()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeAttendance", new object[] { ShiftID, EmployeeID, TimeIn, TimeOUt, LateReason, Resultant });
    }

    public string AddEmployeeAttendanceFromSheet()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeAttendanceFromSheet", new object[] { CompanyID, EmployeeCode, mergedDateTime, CreatedBy });
    }

    public DataTable UpdateEmployeeAttendance(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_UpdateEmployeeAttendance", new object[] { EmployeeID, TimeIn, TimeOUt, LateReason, Resultant }, ref errorMsg);
    }

    public string AddAdjustment(ref string errorMsg)
    {
        return DBManager.NonQueryCommand("usp_TF_AddAdjustment", new object[] { AttendanceID, LateReason });
    }

    public DataTable GetEmployeeAttendanceByMonthForAdjustment(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeAttendanceByMonthForAdjustment", new object[] { EmployeeID, date }, ref errorMsg);
    }

    public DataTable GetEmployeeAttendanceByDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeAttendanceByDate", new object[] { EmployeeID, date }, ref errorMsg);
    } 
    
    public DataTable GetEmployeeAttendanceByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeAttendanceByID", new object[] { AttendanceID }, ref errorMsg);
    }

    public DataTable GetEmployeeOverTimeRecord(int AttendanceID,ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeesOverTimeRecord", new object[] { AttendanceID }, ref errorMsg);
    }

    public DataTable GetAllEmployeeAttendanceByDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeAttendanceByDate", new object[] { CompanyID, date }, ref errorMsg);
    }

    public DataTable GetEmployeeAttendanceByMonth(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeAttendanceByMonth", new object[] { EmployeeID, date }, ref errorMsg);
    }

    public string CheckEmployeeAttendanceByDate()
    {
        return DBManager.NonQueryCommand("usp_TF_CheckEmployeeAttendanceByDate", new object[] { EmployeeID, date });
    }

    public string UpdateEmployeeAttendanceReason()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEmployeeAttendanceReason", new object[] { AttendanceID, LateReason });
    }
        
    public DataTable CheckAttendanceButtonStatusWithTimeNew(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_CheckAttendanceButtonStatusWithTimeNew", new object[] { EmployeeID, date }, ref errorMsg);
    }

    public string MarkAttendanceTimeOut()
    {
        return DBManager.NonQueryCommand("usp_TF_MarkAttendanceBreakOut", new object[] { EmployeeID});
    }

    public string MarkAttendanceTimeIn()
    {
        return DBManager.NonQueryCommand("usp_TF_MarkAttendanceBreakIn", new object[] { EmployeeID});
    }
    public string MarkTimeInAttendanceNew()
    {
        return DBManager.NonQueryCommand("Set_Attendance", new object[] { EmployeeID, Status });
    }

    public string MarkGoogleLocation()
    {
        return DBManager.NonQueryCommand("Set_Google_Location", new object[] { EmployeeID, Status, Latitude, Longitude, GeographicalLocation });
    }

    public string AttendanceMarkTimeINNew()
    {
        return DBManager.NonQueryCommand("usp_TF_AttendanceMarkTimeINNew", new object[] { EmployeeID, date });
    }

    public string AttendanceMarkBreakOut()
    {
        return DBManager.NonQueryCommand("usp_TF_AttendanceMarkBreakOut", new object[] { EmployeeID, date });
    }

    public string AttendanceMarkBreakIn()
    {
        return DBManager.NonQueryCommand("usp_TF_AttendanceMarkBreakIn", new object[] { EmployeeID, date });
    }
        
    public string AttendanceMarkTimeOutNew()
    {
        return DBManager.NonQueryCommand("usp_TF_AttendanceMarkTimeOutNew", new object[] { EmployeeID, date });
    }

    public string UpdateEmployeeShift()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateShiftForEmployee", new object[] { EmployeeID, WorkingShiftID });
    }

    public string UpdateShiftForMultipleEmployees(List<string> EmployeeIDs)
    {
        List<SqlParameter> param = new List<SqlParameter>();
        DataTable dtEmp = new DataTable();
        dtEmp.Columns.Add("Val");
        foreach (string item in EmployeeIDs)
            dtEmp.Rows.Add(item);
        SqlParameter empparam = new SqlParameter("@EmployeeID", SqlDbType.Structured);
        empparam.Value = dtEmp;
        empparam.TypeName = "dbo.lstvarchar";
        param.Add(empparam);
        param.Add(new SqlParameter("@ShiftId", WorkingShiftID));
        return DBManager.NonQueryCommandParam("usp_TF_UpdateShiftForEmployeeNew1", param.ToArray());
    }

    public DataTable GetBirthdayOfNext7DaysByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetBirthdayOfNext7DaysByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAttendanceSummaryKTByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAttendanceSummaryKTByEmployeeID", new object[] { EmployeeID, CompanyID }, ref errorMsg);
    }

    public DataTable GetESSDashboardDetailWidgets(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetESSDashboardDetailWidgets", new object[] { SelectedEmp, CompanyID,FromDate, ToDate}, ref errorMsg);
    }  

    public DataTable GetESSDashboardWidgets(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetESSDashboardWidgets", new object[] { EmployeeID, CompanyID }, ref errorMsg);
    }
    public DataTable CheckAutoAttendanceStatus(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_CheckAutoAttendanceStatus", new object[] { EmployeeID, CompanyID }, ref errorMsg);
    }
    
    public DataTable GetAllEmployeeAttendenceAdjustmentReport(DateTime Fdate ,DateTime Tdate , ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeAttendenceAdjustmentReport", new object[] { CompanyID,EmployeeID ,Fdate,Tdate}, ref errorMsg);
    }

    public DataTable Get_DepartmentWiseMaleFemaleRatio( ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_DepartmentWiseMaleFemaleRatio", new object[] { CompanyID }, ref errorMsg);
    }

    #endregion

    #region Holidays
    public int HolidayID { get; set; }
    public string HolidayTitle { get; set; }
    public string HolidayType { get; set; }
    public string StartDate { get; set; }
    public string EndDate { get; set; }
    public bool isSpecial { get; set; }
    public bool IsFlexible { get; set; }
    public DataTable GetAllHolidaysByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllHolidaysByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApprovedHolidaysByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedHolidaysByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public string AddHoliday()
    {
        return DBManager.NonQueryCommand("usp_TF_AddHolidays", new object[] { CompanyID, HolidayTitle, StartDate, EndDate, CreatedBy, AnnualCalendar, isSpecial,HolidayType });
    }

    public string UpdateHoliday()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateHolidays", new object[] { HolidayID, HolidayTitle, StartDate, EndDate, ModifiedBy, AnnualCalendar,CompanyID, isSpecial, HolidayType });
    }

    public string DeleteHoliday()
    {
        return DBManager.NonQueryCommand("usp_TF_DelHolidays", new object[] { HolidayID, DeletedBy });
    }

    public DataTable GetHolidayByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllHolidaysByID", new object[] { HolidayID }, ref errorMsg);
    }
    #endregion

    #region Announcement
    public int AnnouncementID { get; set; }
    public string AnnouncementTitle { get; set; }

    public DataTable GetAllAnnouncementsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAnnouncementByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAllApproveAnnouncementByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApproveAnnouncementByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetTop5AnnouncementByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetTop5AnnouncementByCompanyID", new object[] { CompanyID, Department }, ref errorMsg);
    }

    public string AddAnnouncement()
    {
        return DBManager.NonQueryCommand("usp_TF_AddAnnouncement", new object[] { CompanyID, AnnouncementTitle, Description, Date, CreatedBy });
    }
    public string UpdateAnnouncement()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAnnouncement", new object[] { AnnouncementID, AnnouncementTitle, Description, ModifiedBy });
    }
    public string AnnouncementDptChannel()
    {
        return DBManager.NonQueryCommand("usp_TF_AnnouncementDptChannel", new object[] { AnnouncementID, Channel});
    }
    public void SlackIT()
    {
         DBManager.NonQueryCommand("Slack_Post_API", new object[] { AnnouncementID, Description,  SlackBy, AnnouncementTitle + " - " + SlackOn, Channel});
    }
    public string DeleteAnnouncement()
    {
        return DBManager.NonQueryCommand("usp_TF_DelAnnouncement", new object[] { AnnouncementID, DeletedBy });
    }

    public DataTable GetSlackChannels(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetSlackChannels", new object[] {  }, ref errorMsg);
    }
    

    public DataTable GetDepartments(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDepartments", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAnnouncementByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAnnouncementByID", new object[] { AnnouncementID }, ref errorMsg);
    }
    #endregion

    #region PolicyProcedure
    public int PolicyProcedureID { get; set; }
    public string PolicyProcedureTitle { get; set; }
    public string DocPath { get; set; }

    public string AddPolicyProcedure()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPolicyProcedure", new object[] { CompanyID, PolicyProcedureTitle, Description, DocPath, CreatedBy });
    }

    public string UpdatePolicyProcedure()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdatePolicyProcedure", new object[] { PolicyProcedureID, PolicyProcedureTitle, Description, DocPath, ModifiedBy });
    }

    public string DeletePolicyProcedure()
    {
        return DBManager.NonQueryCommand("usp_TF_DelPolicyProcedure", new object[] { PolicyProcedureID, DeletedBy });
    }

    public DataTable GetAllPolicyProceduresByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllPolicyProcedureByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApprovePolicyProcedureByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovePolicyProcedureByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetPolicyProcedureByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllPolicyProcedureByID", new object[] { PolicyProcedureID }, ref errorMsg);
    }
    #endregion

    #region OtherExpense
    public int OtherExpenseID { get; set; }
    public string ExpType { get; set; }
    public DateTime PayRollCycle { get; set; }
    public double TotalClaim { get; set; }


    public string AddOtherExpense()
    {
        return DBManager.NonQueryCommand("usp_TF_AddOtherExpense", new object[] { CompanyID, EmployeeID, Code, Date, NetAmount, Title, Notes, CreatedBy });
    }
    public DataTable GetAllExpenseHeads(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllExpenseHead", new object[] { CompanyID }, ref errorMsg);
    }
    public string UpdateOtherExpense()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateOtherExpense", new object[] { OtherExpenseID, EmployeeID, Code, Date, NetAmount, Title, Notes, ModifiedBy });
    }

    public string UpdateAddOnstatus()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAddOnstatus", new object[] { AddOnsExpenseID, Status });
    }

    public string DeleteOtherExpense()
    {
        return DBManager.NonQueryCommand("usp_TF_DelOtherExpense", new object[] { OtherExpenseID, DeletedBy });
    }
    public string GenerateOtherExpenseCode()
    {
        return DBManager.NonQueryCommand("usp_TF_GenerateOtherExpenseCode", new object[] { CompanyID });
    }

    public string AddOtherExpenseDetailsDetails()
    {
        return DBManager.NonQueryCommand("usp_TF_AddOtherExpenseDetailsDetails", new object[] { OtherExpenseID, ExpType, FilePath, NetAmount, Description, CreatedBy });
    }
    public DataTable GetAllOtherExpensesByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllOtherExpensesByCompanyID", new object[] { FromDate, ToDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetAllOtherExpensesByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllOtherExpensesByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }

    public DataTable GetOtherExpenseByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetOtherExpenseByID", new object[] { OtherExpenseID }, ref errorMsg);
    }
    public string DelOtherExpenseDetailsByOtherExpnseID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelOtherExpenseDetailsByOtherExpnseID", new object[] { OtherExpenseID });
    }

    public DataTable GetOtherExpenseDetailsByOtherExpenseID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetOtherExpenseDetailsByOtherExpenseID", new object[] { OtherExpenseID }, ref errorMsg);
    }
    #endregion

    #region EmployeeTests
    public int EmployeeTestID { get; set; }
    public int ShiftID { get; set; }
    public string EmployeeTestDate { get; set; }

    public DataTable GetAllEmployeeTestsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeTestsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllEmployeeTestsByCompanyIDAndEmployeeTestDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeTestsByCompanyIDAndDate", new object[] { CompanyID, EmployeeTestDate }, ref errorMsg);
    }

    public string DeleteEmployeeTest()
    {
        return DBManager.NonQueryCommand("usp_TF_DelEmployeeTests", new object[] { EmployeeTestID, DeletedBy });
    }
   
    #endregion

    #region MailingLists
    public int MailingListID { get; set; }
    public string MailingListTitle { get; set; }
    public string MailingListDate { get; set; }

    public DataTable GetAllMailingListsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllMailingListsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllMailingListsByCompanyIDAndMailingListDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllMailingListsByCompanyIDAndDate", new object[] { CompanyID, MailingListDate }, ref errorMsg);
    }

    public string AddMailingList()
    {
        return DBManager.NonQueryCommand("usp_TF_AddMailingLists", new object[] { CompanyID, MailingListTitle, CreatedBy });
    }
    public string UpdateMailingList()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateMailingLists", new object[] { MailingListID, MailingListTitle, ModifiedBy, CompanyID });
    }
    public string DeleteMailingList()
    {
        return DBManager.NonQueryCommand("usp_TF_DelMailingLists", new object[] { MailingListID, DeletedBy });
    }
    public DataTable GetMailingListByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllMailingListsByID", new object[] { MailingListID }, ref errorMsg);
    }
    #endregion

    #region MailingList invitaions
    public string AddMailingListEmail()
    {
        return DBManager.NonQueryCommand("usp_TF_AddMailingListEmail", new object[] { MailingListID, EmployeeID, CreatedBy });
    }
    public string DeleteMailingListEmails()
    {
        return DBManager.NonQueryCommand("usp_TF_DelMailingListEmail", new object[] { MailingListID, DeletedBy });
    }
    public DataTable GetAllMailingListEmails(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetMailingListEmails", new object[] { MailingListID }, ref errorMsg);
    }
    #endregion

    #region documents
    public int DocID { get; set; }
    public string DocType { get; set; }
    public string DocHeader { get; set; }
    public string DocContent { get; set; }
    public string DocFooter { get; set; }

    public string AddDocumentDesgin()
    {
        return DBManager.NonQueryCommand("usp_TF_AddDocumentDesign", new object[] { CompanyID, DocType, DocHeader, DocContent, DocFooter, CreatedBy });
    }

    public DataTable GetDocumentDesign(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDocumentDesign", new object[] { CompanyID, DocType }, ref errorMsg);
    }
    #endregion

    #region Dashboard
    public string Latitude { get; set; }
    public string Longitude { get; set; }

    public string GeographicalLocation { get; set; }
    public DataTable GetLoanSummaryByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLoansSummaryByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetProvinentFundSummaryByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetProvinentFundSummaryByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetGradeRatioByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeGradeRatioByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetGradeDesignationByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeDesignationRatioByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetEmployeeTypeRatioByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeTypeRatioByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetMaleVsFemaleRatioByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetMaleVsFemailRatio", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetFinanceChartYearlyByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetYearWiseMontlyPayroll", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetDepartWiseMonthlySalary(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDepartWiseMonthlySalary", new object[] { Month, CompanyID }, ref errorMsg);
    }
    public DataTable GetPayrollDataLocationWiseSP(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPayrollDataLocationWise", new object[] { Month, CompanyID }, ref errorMsg);
    }

    public DataTable GetPayrollBreakupChart(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPayrollBreakupChart", new object[] { Month, CompanyID }, ref errorMsg);
    }
    public DataTable Get_EmployeeWiseShiftChart(ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_EmployeeWiseShiftChart", new object[] {  CompanyID }, ref errorMsg);
    }

    public DataTable Get_DepartmentWiseCheckInChart(ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_DepartmentWiseCheckInChart", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable Get_EmployeeWiseShiftReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_EmployeeWiseShiftReport", new object[] { CompanyID, ShiftID, DeptID, DesgID, EmployeeID, KTID, Location }, ref errorMsg);
    }
    public DataTable Get_EmployeeAbsentReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_EmployeeAbsentReport", new object[] { CompanyID, FromDate, ToDate, DeptID, DesgID, EmployeeID, KTID, Location }, ref errorMsg);
    }
    public DataTable Get_NewJoinersReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_NewJoinersReport", new object[] { CompanyID, FromDate, ToDate, DeptID, DesgID, EmployeeID, WDID, Location }, ref errorMsg);
    }
    public DataTable Get_DepartmentWiseTurnOver(DateTime fdate, DateTime tdate, ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_DepartmentWiseTurnOver", new object[] { CompanyID, fdate, tdate}, ref errorMsg);
    }
    public DataTable Get_LeftEmployeeReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_LeftEmployeeReport", new object[] { CompanyID, FromDate, ToDate, DeptID, DesgID, EmployeeID, KTID, Location }, ref errorMsg);
    }
    public DataTable Get_EmployeeLeaveReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_EmployeeLeaveReport", new object[] { CompanyID, FromDate, ToDate, DeptID, DesgID, EmployeeID, KTID, Location }, ref errorMsg);
    }
    public DataTable Get_LastChangeInShifts(int NoOfRecords,ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_LastChangeInShifts", new object[] { CompanyID, NoOfRecords }, ref errorMsg);
    }
    public DataTable Get_ITDashboardWidgets( ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_ITDashboardWidgets", new object[] { CompanyID}, ref errorMsg);
    }
    public DataTable GetMonthlyChart(string   Year,ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetMonthlyChart", new object[] { Year, CompanyID }, ref errorMsg);
    }

    public DataTable GetAttendanceDashboard(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAttendanceDashboard", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetLoanSummaryByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLoanSummaryByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }

    public DataTable GetPaySlipByEmployeeID_YTD(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPaySlipByEmployeeID_YTD", new object[] { EmployeeID, PayrollDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetAnnualTaxableIncomePayslips(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetTotal_IncomeTax_Lia_ByPayAmount_YTD_PaySlips", new object[] { PayAmount }, ref errorMsg);
    }
    
    public DataTable GetPaySlipKTByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPaySlipKTByEmployeeID", new object[] { EmployeeID, PayrollDate, CompanyID }, ref errorMsg);
    }
    public DataTable GetSumUpPaySlipKTByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetSumUpPaySlipKTByEmployeeID", new object[] { EmployeeID, PayrollDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetPayMiniSlipKTByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetMiniPaySlipKTByEmployeeID", new object[] { EmployeeID, PayrollDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetPaySlipPFKTByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPFPaylips", new object[] { EmployeeID, PayrollDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetSCBReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetSCBReport", new object[] {  PayrollDate, CompanyID }, ref errorMsg);
    }
    public DataTable GetFnFJEReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetFnFJEReport", new object[] { PayrollDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetMiniJEReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetMiniJEReport", new object[] { PayrollDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetJEReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetJEReport", new object[] { PayrollDate, CompanyID }, ref errorMsg);
    }
    public DataTable GetPaySlipKTByEmployeeIDNew(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPayrollDetailsbyCompanyID", new object[] { PayrollDate, CompanyID }, ref errorMsg);
    }
    public DataTable GetFnFKTByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetFnFKTByEmployeeID", new object[] { SelectedEmp, CompanyID,  }, ref errorMsg);
    }

    public DataTable GetNewPayrollDetailsByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNewPayrollDetailsByEmployeeID", new object[] { EmployeeID, PayrollDate, CompanyID }, ref errorMsg);
    }

    #endregion

    #region Employees
    public string AnnualCalendar { get; set; }
    public bool autoattendance { get; set; }
    public string EmployeeCode { get; set; }
    public string EmployeeFName { get; set; }
    public string EmployeeMName { get; set; }
    public string EmployeeLName { get; set; }
    public string EmployeePhoto { get; set; }
    public string EmergencyContactPerson { get; set; }
    public string EmergencyContactNo { get; set; }
    public string DateOfJoining { get; set; }
    public string BankName { get; set; }
    public string BranchName { get; set; }
    public string AccountTitle { get; set; }
    public string AccountNo { get; set; }
    public string EmploymentType { get; set; }
    public string BlackListedBy { get; set; }
    public string BlackListedReason { get; set; }
    public string BlackListedAttachment { get; set; }
    public bool DeductPF { get; set; }
    public int DirectReportingPerson { get; set; }
    public int InDirectReportingPerson { get; set; }
    public string SecEmail { get; set; }
    public string FatherName { get; set; }
    public string BloodGroup { get; set; }
    public string DOB { get; set; }
    public string PlaceOfPosting { get; set; }
    public string CostDeductor { get; set; }
    public float FuelAllowance { get; set; }
    public float MobileAllowance { get; set; }
    public float CarAllowance { get; set; }
    public float TravelAllowance { get; set; }
    public float FoodAllowance { get; set; }
    public float OtherAllowance { get; set; }
    public float MedicalAllowance { get; set; }
    public float GrossAmount { get; set; }
    public float NetSalary { get; set; }
    public float TaxableIncome { get; set; }
    public float CompanyEOBI { get; set; }
    public float ExemptGraduity { get; set; }
    public float HouseAllownace { get; set; }
    public float UtitlityAllowance { get; set; }
    public float EOBI { get; set; }
    public float NightFoodAllowance { get; set; }
    public float MaintenanceAllowance { get; set; }
    public float OverTimeRate { get; set; }
    public bool isOverTime { get; set; }
    public bool isHolidayWorking { get; set; }
    public int WDID { get; set; }
    public string KTID { get; set; }
    public string WorkDayID { get; set; }

    public string ToggleEmployeeBlackListing()
    {
        return DBManager.NonQueryCommand("usp_Comm_ToggleEmployeeBlackListing", new object[] { EmployeeID, BlackListedReason, BlackListedAttachment, BlackListedBy });
    }

    public DataTable GetAllEmployeesByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeesByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    public DataSet GetAllEmployeesByCompanyID(ref string errorMsg, int numberOfRecord, int from)
    {
        return DBManager.DataSetCommand("usp_TF_GetAllEmployeesByCompanyIDWithPagination", new object[] { CompanyID, numberOfRecord, from }, ref errorMsg);
    }

    public DataTable GetAllEmployeesByDirectReporting(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetTeamUsingDirectReporting", new object[] { directMgr }, ref errorMsg);
    }
    public DataTable GetAllEmployeesByManagerID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeesByManagerID", new object[] { EmployeeID }, ref errorMsg);
    }
    public DataTable GetIT3_Submission(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_Get_IT3_Submission_byEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public string Update_IT3_Submission()
    {
        return DBManager.NonQueryCommand("usp_TF_Update_IT3_Submission", new object[] { IT3_ID,Status, PayrollDate, ModifiedBy, ApproveAmount, isActive,Remarks });
    }

    public DataTable GetIT3ByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetIT3ByID", new object[] { IT3_ID }, ref errorMsg);
    }
    public DataTable GetIT3_SubmissionForApproval(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_Get_IT3_SubmissionApprovals", new object[] { Month }, ref errorMsg);
    }
    public DataTable GetIT3_Submission_Approved(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_Get_IT3_Submission_Approved_byEmployeeID", new object[] { CompanyID }, ref errorMsg);
    }
   
    public DataTable GetAttendanceSheetKT(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAttendanceSheetKT", new object[] { WhereClause, FromDate, ToDate }, ref errorMsg);
    }
    public DataSet GetAttendanceSheetClockwise(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetAttendanceClockwise", new object[] { WhereClause, FromDate, ToDate }, ref errorMsg);
    }

    public DataTable GetAllEmployeeAttendenceSummaryByDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeAttendenceSummaryByDate", new object[] { SelectedEmp, FromDate, ToDate, Status }, ref errorMsg);
    }

    public DataTable GetAttendanceSummaryKT(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAttendanceSummaryKT", new object[] { WhereClause, FromDate, ToDate }, ref errorMsg);
    }

    public DataTable GetAttendanceSheetESSKT(int ManagerId, ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAttendanceSheetESSKT", new object[] { CompanyID, EmployeeID, FromDate, ToDate, ManagerId }, ref errorMsg);
    }

    public DataTable GetAttendanceSheetESSKTnew(int ManagerId, ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAttendancedetailedReportKT", new object[] { CompanyID, EmployeeID,ManagerId, FromDate, ToDate, Resultant, DeptID, DesgID, WDID, Location }, ref errorMsg);
    }

    public DataTable GetAttendanceSheetFlagWise(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAttendanceDetailedReport", new object[] { CompanyID, EmployeeID, FromDate, ToDate, Resultant, DeptID, DesgID, WDID, Location }, ref errorMsg);
    }

    public DataTable GetBreakInBreakout(int ManagerId,ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetBreakinBreakoutReport", new object[] { CompanyID, EmployeeID, ManagerId, FromDate, ToDate, DeptID, DesgID, WDID, Location }, ref errorMsg);
    }

    public DataTable GetAllEmployeesByShiftID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeesByShiftID", new object[] { ShiftID }, ref errorMsg);
    }

    public DataTable CheckEmail(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_CheckEmail", new object[] { CompanyID, Email }, ref errorMsg);
    }

    public DataTable GetAllEmployeesByCompanyIDForBlackListing(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeesByCompanyIDForBlackListing", new object[] { CompanyID }, ref errorMsg);
    }

    public string AddEmployeesByCode()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployees", new object[] { EmployeeCode, NodeID, EmployeeFName, EmployeeMName, EmployeeLName, EmployeePhoto, CNIC, Gender, DateOfJoining, ContactNo, Email, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, BankName, BranchName, AccountTitle, AccountNo, EmploymentType, null, null, GradeID, EmergencyContactPerson, EmergencyContactNo, 0, DeductPF, CreatedBy, ShiftID, null, null, null, null, null, null, null, null, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
    }

    public string AddNewEmployees()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewEmployees", new object[] { EmployeeCode, NodeID, EmployeeFName, EmployeeMName, EmployeeLName, EmployeePhoto, CNIC, Gender, DateOfJoining, ContactNo, Email, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, BankName, BranchName, AccountTitle, AccountNo, EmploymentType, DirectReportTo, IndirectReportTo, GradeID, EmergencyContactPerson, EmergencyContactNo, BasicSalary, DeductPF, CreatedBy, ShiftID, DirectReportingPerson, InDirectReportingPerson, SecEmail, FatherName, DOB, BloodGroup, PlaceOfPosting, CostDeductor, FuelAllowance, MobileAllowance, CarAllowance, TravelAllowance, FoodAllowance, OtherAllowance, MedicalAllowance, GrossAmount, NetSalary, HouseAllownace, UtitlityAllowance, EOBI, NightFoodAllowance, MaintenanceAllowance, OverTimeRate, COA_ID, PayrolllExpCOA, EmployeeCOA, EmployeeTax });
    }
   
    public string AddNewEmployeesNew()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewEmployeesNew", new object[] { EmployeeCode, NodeID, EmployeeFName, EmployeeMName, EmployeeLName, EmployeePhoto, CNIC, Gender, DateOfJoining, ContactNo, Email, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, BankName, BranchName, AccountTitle, AccountNo, EmploymentType, DirectReportTo, IndirectReportTo, GradeID, EmergencyContactPerson, EmergencyContactNo, BasicSalary, DeductPF, CreatedBy, ShiftID, DirectReportingPerson, InDirectReportingPerson, SecEmail, FatherName, DOB, BloodGroup, PlaceOfPosting, CostDeductor, FuelAllowance, MobileAllowance, CarAllowance, TravelAllowance, FoodAllowance, OtherAllowance, MedicalAllowance, GrossAmount, NetSalary, HouseAllownace, UtitlityAllowance, EOBI, NightFoodAllowance, MaintenanceAllowance, OverTimeRate, COA_ID, PayrolllExpCOA, EmployeeCOA, EmployeeTax, CompanyID, QRcodePath, AnnualCalendar, isOverTime,WDID, DesgID, DeptID, JDID,autoattendance,GroupID });
    }
    public string UpdateNewEmployee()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateNewEmployee", new object[] { EmployeeID, EmployeeCode, NodeID, EmployeeFName, EmployeeMName, EmployeeLName, EmployeePhoto, CNIC, Gender, DateOfJoining, ContactNo, Email, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, BankName, BranchName, AccountTitle, AccountNo, EmploymentType, DirectReportTo, IndirectReportTo, GradeID, EmergencyContactPerson, EmergencyContactNo, BasicSalary, DeductPF, ModifiedBy, ShiftID, DirectReportingPerson, InDirectReportingPerson, SecEmail, FatherName, DOB, BloodGroup, PlaceOfPosting, CostDeductor, FuelAllowance, MobileAllowance, CarAllowance, TravelAllowance, FoodAllowance, OtherAllowance, MedicalAllowance, GrossAmount, NetSalary, HouseAllownace, UtitlityAllowance, EOBI, NightFoodAllowance, MaintenanceAllowance, OverTimeRate, COA_ID, PayrolllExpCOA, EmployeeCOA, EmployeeTax, CompanyID, AnnualCalendar, isOverTime, WDID, DesgID, DeptID, JDID, autoattendance, GroupID });
    } 
    
    public string UpdateEmployeeShiftAndOvertimeOnly()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEmployeeShiftAndOvertimeOnly", new object[] { EmployeeID, isOverTime, isHolidayWorking });
    }
    public string UpdateIndirectPerson()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateIndirectPerson", new object[] { IndirectMgr, isMgr, directMgr, createdby  });
    }

    public string approveIT3()
    {
        return DBManager.NonQueryCommand("usp_TF_AppIT3byIT3ID", new object[] { Remarks, IT3_ID, ApprovedBy });
    }

    public string UpdateNewEmployeeUpload()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateNewEmployeeUpload", new object[] { EmployeeID, BasicSalary, FuelAllowance, MobileAllowance, CarAllowance, TravelAllowance, FoodAllowance, OtherAllowance, MedicalAllowance, GrossAmount, NetSalary, HouseAllownace, UtitlityAllowance, EOBI, NightFoodAllowance, MaintenanceAllowance, OverTimeRate, ModifiedBy, EmployeeTax, EmployeeFName, EmployeeMName, EmployeeLName, EmployeeCode, CNIC, Gender, DateOfJoining, ContactNo, Email, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, BankName, BranchName, AccountTitle, AccountNo });
    }
    public string UpdateNewEmployeeUpload2()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateNewEmployeeUpload2", new object[] { EmployeeID, BasicSalary, FuelAllowance, GrossAmount, EOBI, MaintenanceAllowance, OverTimeRate, ModifiedBy, EmployeeTax });
    }

    public string UpdateEmployeeBankDetails()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEmployeeAccountDetails", new object[] { EmployeeID, BankName, BranchName, AccountTitle, AccountNo, ModifiedBy });
    }
    public string UpdateEmployeeBasicInformation()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEmployeeBasicInrormation", new object[] { EmployeeID, EmployeeFName, EmployeeMName, EmployeeLName, EmployeePhoto, CNIC, Gender, ContactNo, Email, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, ModifiedBy });
    }

    public string DeleteEmployeeByID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelEmployeeByID", new object[] { EmployeeID, DeletedBy });
    }
    public DataTable GetEmployeeByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeesByID", new object[] { EmployeeID }, ref errorMsg);
    }
    public DataTable GetEmployeeDetailsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeDetailsByID", new object[] { EmployeeID }, ref errorMsg);
    }
    public DataTable GetEmployeePersonalDetails(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeePersonalDetails", new object[] { EmployeeID }, ref errorMsg);
    }
    

    public string GenerateEmployeeCode()
    {
        return DBManager.NonQueryCommand("usp_TF_GenerateEmployeeCode", new object[] { CompanyID });
    }

    public string GetEmployeeCount()
    {
        return DBManager.NonQueryCommand("usp_TF_GetEmployeeCount", new object[] { CompanyID});
    }

    public string GetPresentEmployees()
    {
        return DBManager.NonQueryCommand("usp_TF_GetPresentEmployeeCount", new object[] { DateTime.Now.ToString() });
    }

    public string Get_NewRecruits()
    {
        return DBManager.NonQueryCommand("Get_NewJoiningEmployees", new object[] { CompanyID, FromDate, ToDate });
    }

    public string Get_LeftEmployeesCount()
    {
        return DBManager.NonQueryCommand("Get_LeftEmployees", new object[] { CompanyID, FromDate, ToDate });
    }

    public DataTable GetEmployeeLeavesByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeLeavesByID", new object[] { LeaveID }, ref errorMsg);
    }

    public DataTable GetAllEmployeesByCompanyDepartmentAndDesignationID(ref string errorMsg, string Department = "0", string Designation = "0")
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeesByCompanyDeptAndDesgID", new object[] { CompanyID, Department, Designation }, ref errorMsg);
    }

    public DataTable GetEmployeesEOBIReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_EmployeeEOBIReport", new object[] { PlaceOfPosting, PayrollDate }, ref errorMsg);
    }
    public DataTable EmployeeWHTReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_EmployeeWHTReport", new object[] { PlaceOfPosting, PayrollDate }, ref errorMsg);
    }
    public DataTable GetEmployeesIESSISESSIPESSIReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_EmployeeIESSISESSIPESSIReport", new object[] { PlaceOfPosting, PayrollDate }, ref errorMsg);
    }
    public DataTable GetPendingRequestsList(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPendingRequestsList", new object[] { PayrollDate, CompanyID }, ref errorMsg);
    }
    public DataTable GetDirectReportingPerson(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDirectReportingPerson", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetPendingRequestsDeatils(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPendingRequestsDetails", new object[] { PayrollDate, EmployeeID }, ref errorMsg);
    }
    public string GetPendingRequestsCount()
    {
        return DBManager.NonQueryCommand("usp_TF_GetPendingRequestsCount", new object[] { Month });
    }

    public DataTable GetEmployeeYearlyTax(ref string errorMsg)
    {
        return DBManager.DataTableCommand("USP_PF_GetEmployeeYearlyTaxReport", new object[] { CompanyID, FinancialYear, DeptID, DesgID, EmployeeID, WorkDayID, Location }, ref errorMsg);
    }
    public DataTable GetEmployeeYearlyTaxSlabs(string TaxValue, string TaxYear, ref string errorMsg)
    {
        return DBManager.DataTableCommand("USP_PF_GetYearlyTaxReportSlabs", new object[] { TaxValue, TaxYear }, ref errorMsg);
    }

    public DataTable GetFinancialYearsList(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetFinancialYearsList", new object[] { }, ref errorMsg);
    }

    #endregion

    #region EmployeeTable
    public string Type { get; set; }

    public string AddEmployeeTable()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeTable", new object[] { EmployeeID, Title, Type, CreatedBy });
    }
    public DataTable GetAllEmployeeTableByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeTableByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }

    public string DeleteEmployeeTableByEmployeeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteEmployeeTableByEmployeeID", new object[] { EmployeeID, DeletedBy });
    }
    #endregion

    #region EmployeeBudget

    public string AddEmployeeBudget()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeBudget", new object[] { EmployeeID, Title, Type, Amount, TransictionType, CreatedBy });
    }
    public DataTable GetAllEmployeeBudgetByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeBudgetByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public string DeleteEmployeeBudgetByEmployeeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteEmployeeBudgetByEmployeeID", new object[] { EmployeeID, DeletedBy });
    }
    #endregion

    #region EmployeeLoan
    public string LoanTitle { get; set; }
    public string LoanNotes { get; set; }
    public string LoanGrantDate { get; set; }
    public string PayrollDate { get; set; }
    public float LoanAmount { get; set; }
    public int NoOfInstallments { get; set; }
    public int LoanID { get; set; }
    public float DeductionPerSalary { get; set; }

    public string AddEmployeeLoan()
    {
        return DBManager.NonQueryCommand("usp_TF_AddLoan", new object[] { EmployeeID, LoanTitle, LoanNotes, LoanAmount, NoOfInstallments, DeductionPerSalary, LoanGrantDate, CreatedBy });
    }
    public string UpdateEmployeeLoan()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateLoan", new object[] { LoanID, EmployeeID, LoanTitle, LoanNotes, LoanAmount, NoOfInstallments, DeductionPerSalary, LoanGrantDate, ModifiedBy });
    }
    public string UpdateEmployeeLoanStatus()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEmployeeLoanStatus", new object[] { LoanID, module, createdby, Status, Comments });
    }

   
     public DataTable CreateNewLoanAdvanceSchedule(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_CreateNewLoanAdvanceSchedule", new object[] { LoanID }, ref errorMsg);
    }

    public DataTable UpdateLoanAdvanceSchedule(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_UpdateLoanAdvanceSchedule", new object[] { LoanID }, ref errorMsg);
    }
    public string AddEmployeeLaonInstallment()
    {
        return DBManager.NonQueryCommand("usp_TF_AddLoanInstalments", new object[] { EmployeeID, CreatedBy });
    }
    public string UpdateEmployeeLoanInstallment()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEmployeeLoanInstallment", new object[] { LoanID, isAllowDeduction, EffectiveDate, ModifiedBy });
    }

    public DataSet GetEmployeeLoanNAdvanceByID(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetLoanNAdvanceByID", new object[] { LoanID }, ref errorMsg);
    }
    public DataSet GetPaymentScheduleReport(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetPaymentScheduleReport", new object[] { LoanID }, ref errorMsg);
    }
    public DataTable GetEmployeeLoanByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLoanByID", new object[] { LoanID }, ref errorMsg);
    }
    public DataSet GetLoanAndAdvanceDetailsByLoanID(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetLoanAndAdvanceDetailsByLoanID", new object[] { LoanID }, ref errorMsg);
    }
    public DataTable GetLoanTypes(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLoanTypes", new object[] {  }, ref errorMsg);
    }
    public DataSet GetPurposeOfWithdrawalParent(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetPurposeofwithdrawalParent", new object[] { }, ref errorMsg);
    }
    public DataTable GetSubPurpose(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetSubPurpose", new object[] { MainPuroposeID }, ref errorMsg);
    }
    public DataTable GetWithdrawalDocs(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetWithdrawalDocs", new object[] { SelectedPuroposeID }, ref errorMsg);
    }
    public string SubmitWithdrawalRequest(string withdrawaltype, string mainpurpose, string subpurpose, int noofinstallment, float amount, float deductpermonth, int currency, string notes, string title, DateTime requireddate,bool zakat)

    {
        return DBManager.NonQueryCommand("usp_TF_SubmitWithdrawalRequest", new object[] {  withdrawaltype, mainpurpose, subpurpose, noofinstallment, amount, deductpermonth,currency, notes, title , requireddate, zakat, CreatedBy, EmployeeID });
    }
    public string AddWithdrawalAttachments(int LoanID, string attachmentpath, int attachid)
    {
        return DBManager.NonQueryCommand("usp_TF_AddWithdrawalAttachments", new object[] { LoanID, attachmentpath, attachid });
    }
    public string DeletePaymentScheduleSheet(ref string errorMsg)
    {
        return DBManager.NonQueryCommand("usp_TF_DeletePaymentScheduleSheet", new object[] { SheetID });
    }
    public DataSet GetPaymentScheduleRecords(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetPaymentScheduleSheet", new object[] { SheetID }, ref errorMsg);
    }
    public DataSet RunPaymentScheduleFile(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_RunPaymentScheduleFile", new object[] { SheetID, CreatedBy, LoanID, CompanyID }, ref errorMsg);
    }
    public string AddnewSheetLoanPaymentSchedule(ref string errorMsg)
    {
        List<SqlParameter> param = new List<SqlParameter>();


        param.Add(new SqlParameter("@SheetName", SheetName));
        param.Add(new SqlParameter("@SheetType", SheetType));
        param.Add(new SqlParameter("@CreatedBy", CreatedBy));
        param.Add(new SqlParameter("@uploadedby", uploadedby));
        return DBManager.NonQueryCommandParam("usp_TF_AddnewSheetLoanPaymentSchedule", param.ToArray());
    }

    public DataTable GetAllLoanPaymentScheduleFiles(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLoanPaymentScheduleFiles", new object[] { module, uploadedby }, ref errorMsg);
    }

    public DataTable GetAllLoanApplicationEmployees(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLoanApplicationEmployees", new object[] {  }, ref errorMsg);
    }
    public DataTable GetEmployeeLoanByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLoanByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetEmployeeLoanByEmploteeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLoanByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public DataTable GetLoanAndAdvanceByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLoanAndAdvanceByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }

    public DataTable GetAllLoanAndAdvance(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLoanAndAdvance", new object[] { CompanyID }, ref errorMsg);
    }
    public string DeleteEmployeeLoanByLoanID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelLoans", new object[] { LoanID, DeletedBy });
    }
    public string UpdateLoanStatus()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateLoanStatus", new object[] { LoanID, Status });
    }

    #endregion

    #region ProvinentFunds
    public float CompanyRatio { get; set; }
    public float EmployeeRatio { get; set; }
    public float EOBICompanyRatio { get; set; }
    public float EOBIEmployeeRatio { get; set; }
    public float IESSICompanyRatio { get; set; }
    public float IESSIEmployeeRatio { get; set; }
    public float OvertimeHolidayHours { get; set; }
    public float OvertimeGeneralHours { get; set; }
    public float OverTimeTotalHours { get; set; }
    public int TotalDays { get; set; }
    public int WorkingDays { get; set; }
    public int AbsentDays { get; set; }
    public int PayrolllExpCOA { get; set; }
    public float OverTimeSimple { get; set; }
    public float OverTimeSpecial { get; set; }
    public float AttendanceDays { get; set; }

    public string AddProvinentFund()
    {
        return DBManager.NonQueryCommand("usp_TF_AddProvinentFunds", new object[] { CompanyID, CompanyRatio, EmployeeRatio,  EOBICompanyRatio, EOBIEmployeeRatio, IESSICompanyRatio, IESSIEmployeeRatio, OverTimeSimple, OverTimeSpecial, CreatedBy });
    }
    public DataTable GetProvinentFundByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetProvinentFunds", new object[] { CompanyID }, ref errorMsg);
    }
    #endregion

    #region PFDeduction
    public int PFID { get; set; }
    public float PFEmpoyeeAmount { get; set; }
    public float PFCompanyAmount { get; set; }
    public string PFMonth { get; set; }
    public string PFYear { get; set; }

    public string AddPFDeduction()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPFDeductions", new object[] { PFID, EmployeeID, PFCompanyAmount, PFEmpoyeeAmount, PFMonth, PFYear, CreatedBy });
    }

    public string AddPFWithdraw()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPFWithdraw", new object[] { EmployeeID, PFEmpoyeeAmount, PFMonth, PFYear, CreatedBy });
    }
    public DataTable GetProvinentFundsSummary(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPFDeductionSummary", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetRate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetConstantRate", new object[] { Findrateof }, ref errorMsg);
    }

    public DataTable GetProvinentFundByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPFDeductionByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }

    public DataTable GetPFTransactionsByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPFAdditionDetailByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }

    #endregion

    #region EmployeePromotion
    public string PromotionDate { get; set; }
    public int DirectReportingID { get; set; }
    public int InDirectReportingID { get; set; }
    public int PromotionID { get; set; }

    public string AddEmployeePromotion()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPromotion", new object[] { EmployeeID, PromotionDate, Notes, DesgID, GradeID, DirectReportingID, InDirectReportingID, BasicSalary, MaintenanceAllowance, FuelAllowance, OverTimeRate, EOBI, NetSalary, NodeID, CreatedBy });
    }

    public string UpdateEmployeePromotion()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdatePromotion", new object[] { PromotionID, EmployeeID, PromotionDate, Notes, DesgID, GradeID, DirectReportingID, InDirectReportingID, BasicSalary, MaintenanceAllowance, FuelAllowance, OverTimeRate, EOBI, NetSalary, NodeID, ModifiedBy });
    }
    public string UpdateEmployeeSalary()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEmployeeSalary", new object[] { EmployeeID, PromotionDate, DesgID, GradeID, DirectReportingID, InDirectReportingID, BasicSalary, MaintenanceAllowance, FuelAllowance, OverTimeRate, EOBI, NetSalary, NodeID, ModifiedBy, CompanyID });
    }

    public DataTable GetAllEmployeePromotionByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPromotionsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetEmployeePromotionByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPromotionsByID", new object[] { PromotionID }, ref errorMsg);
    }
    public string DeleteEmployeePromotionByPromotionID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeletePromotions", new object[] { PromotionID, DeletedBy });
    }
    #endregion

    #region EmployeeAssets
    public string AssetStatus { get; set; }

    public string AddEmployeeAsset()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeAssets", new object[] { EmployeeID, AssetTitle, Quantity, AssetDesc, AssetStatus, CreatedBy });
    }
    public DataTable GetAllEmployeeAssetByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeAssetsByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public string DeleteEmployeeAssetByEmployeeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteEmployeeAssets", new object[] { EmployeeID, DeletedBy });
    }
    #endregion

    #region EmployeeAttachment
    public string AttachmentTitle { get; set; }
    public string AttachmentPath { get; set; }

    public string AddEmployeeAttachment()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeAttachment", new object[] { EmployeeID, AttachmentTitle, AttachmentPath, CreatedBy });
    }
    public DataTable GetAllEmployeeAttachmentByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeAttachmentByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public string DeleteEmployeeAttachmentByEmployeeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteEmployeeAttachment", new object[] { EmployeeID, DeletedBy });
    }
    #endregion

    #region EmployeeQualification
    public string DegreeLevel { get; set; }
    public string DegreeTitle { get; set; }
    public string DegreeYear { get; set; }
    public string DegreeMonth { get; set; }
    public string Grade { get; set; }
    public string DegreeInst { get; set; }

    public string AddEmployeeQualification()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeQualification", new object[] { EmployeeID, DegreeLevel, DegreeTitle, DegreeYear, DegreeMonth, Grade, DegreeInst, CountryName, StateName, CityName, Remarks, CreatedBy });
    }
    public DataTable GetAllEmployeeQualificationByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeQualificationByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public string DeleteEmployeeQualificationByEmployeeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteEmployeeQualification", new object[] { EmployeeID, DeletedBy });
    }
    #endregion

    #region EmployeeCareerTracking
    public string Designation { get; set; }
    public string ProjectTitle { get; set; }

    public string AddEmployeeCareerTracking()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeCareerTracking", new object[] { EmployeeID, Name, Designation, ProjectTitle, StartDate, EndDate, CreatedBy });
    }
    public DataTable GetAllEmployeeCareerTrackingByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeCareerTrackingByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public string DeleteEmployeeCareerTracking()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteEmployeeCareerTracking", new object[] { EmployeeID, DeletedBy });
    }
    #endregion

    #region EmployeeExperience
    public string Jobtitle { get; set; }
    public string Organization { get; set; }
    public string StartYear { get; set; }
    public string StartMonth { get; set; }
    public string EndYear { get; set; }
    public string EndMonth { get; set; }
    public bool CurrentlyWorkHere { get; set; }

    public string AddEmployeeExperience()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeExperience", new object[] { EmployeeID, Jobtitle, Organization, StartYear, StartMonth, EndYear, EndMonth, CurrentlyWorkHere, CountryID, StateID, CityID, Remarks, CreatedBy });
    }
    public DataTable GetAllEmployeeExperienceByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeExperienceByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public string DeleteEmployeeExperienceByEmployeeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteEmployeeExperience", new object[] { EmployeeID, DeletedBy });
    }
    #endregion

    #region EmployeeSkillSet
    public string Skilltitle { get; set; }
    public string SkillLevel { get; set; }

    public string AddEmployeeSkillSet()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeSkillSet", new object[] { EmployeeID, Skilltitle, SkillLevel, CreatedBy });
    }
    public DataTable GetAllEmployeeSkillSetByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeSkillSetByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public string DeleteEmployeeSkillSetByEmployeeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteEmployeeSkillSet", new object[] { EmployeeID, DeletedBy });
    }
    #endregion

    #region EmployeeCertAndTra
    public string Year { get; set; }
    public string Month { get; set; }
    public string Score { get; set; }
    public string Institution { get; set; }
    public string Details { get; set; }

    public string AddEmployeeCertAndTra()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeCertAndTra", new object[] { EmployeeID, Title, Year, Month, Institution, CountryID, StateID, CityID, Score, Details, CreatedBy });
    }
    public DataTable GetAllEmployeeCertAndTraByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeCertAndTraByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public string DeleteEmployeeCertAndTraByEmployeeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteEmployeeCertAndTra", new object[] { EmployeeID, DeletedBy });
    }
    #endregion

    #region Employees Seperations
    public int ClearanceItemID { get; set; }
    public string TypeOfSeperation { get; set; }
    public string ReasonForLeaving { get; set; }
    public string CommentsRegardingSalary { get; set; }
    public string ThingsLikeAboutCompany { get; set; }
    public string NoticePeriodEndDate { get; set; }
    public string EmailForwardingPeriod { get; set; }
    public string EmailStatus { get; set; }
    public string DomainAccess { get; set; }
    public int SeperationDetailsID { get; set; }
    public bool AccessStatusRevoked { get; set; }
    public bool CompanyInfoStoredPersonally { get; set; }
    public string ITComments { get; set; }
    public bool LockerKeysReturned { get; set; }
    public bool ReturnedCar { get; set; }
    public bool ReturnedStationary { get; set; }
    public bool ReturnedPhone { get; set; }
    public bool ReturnedInternetDevice { get; set; }
    public string AdminComments { get; set; }
    public string NoticePeriodDeductions { get; set; }
    public string AtendanceDetails { get; set; }
    public string UnAwailedLeaveBalance { get; set; }
    public string MedicalRefurbishments { get; set; }
    public string HRNotes { get; set; }
    public string LoansAndAdvances { get; set; }
    public string CompanyLoans { get; set; }
    public string AnyOtherAdvance { get; set; }
    public string TotalDues { get; set; }
    public string FinanceNotes { get; set; }

    public string AddEmployeeSeperation()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeSeperation", new object[] { EmployeeID, TypeOfSeperation, ReasonForLeaving, CommentsRegardingSalary, ThingsLikeAboutCompany, NoticePeriodEndDate, EmailForwardingPeriod, EmailStatus, DomainAccess, AccessStatusRevoked, CompanyInfoStoredPersonally, ITComments, LockerKeysReturned, ReturnedCar, ReturnedStationary, ReturnedPhone, ReturnedInternetDevice, AdminComments, NoticePeriodDeductions, AtendanceDetails, UnAwailedLeaveBalance, MedicalRefurbishments, HRNotes, LoansAndAdvances, CompanyLoans, AnyOtherAdvance, TotalDues, FinanceNotes, CreatedBy });
    }

    public DataTable GetEmployeeSeperationDetails(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeSeperation", new object[] { EmployeeID }, ref errorMsg);
    }
    
    public string AddClearanceItem()
    {
        return DBManager.NonQueryCommand("usp_TF_AddClearanceItem", new object[] { CompanyID, Title, Notes, CreatedBy, DeptID });
    }
    public string AddOvertimeGroup()
    {
        return DBManager.NonQueryCommand("usp_TF_AddOvertimeGroup", new object[] { CompanyID, Groupname, Notes, Generalovertime, Offdayovertime, Holidayovertime, Eidovertime, CreatedBy });
    }
    public string UpdateOvertimeGroup()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateOvertimeGroup", new object[] {  Groupname, Notes, Generalovertime, Offdayovertime, Holidayovertime, Eidovertime, ModifiedBy, GroupID });
    }


    public string UpdateClearanceItem()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateClearanceItem", new object[] { CompanyID, Title, Notes, DeptID, ModifiedBy, ClearanceItemID });
    }

    public string DeleteClearanceItemByID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteClearanceItemByID", new object[] { DeletedBy, ClearanceItemID });
    }

    public DataTable GetClearanceItemsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetClearanceItemsByID", new object[] { ClearanceItemID }, ref errorMsg);
    }
    public DataTable GetGroupByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetGroupByID", new object[] { GroupID }, ref errorMsg);
    }
    

    public DataTable GetAllClearanceItemsByCopmanyAndDepartmentId(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllClearanceItemsByCopmanyAndDepartmentId", new object[] { CompanyID, DeptID }, ref errorMsg);
    }

    public DataTable GetAllEmployeeSeperationDetailsByDepartment(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeSeperationDetailsByDepartment", new object[] { CompanyID, FromDate, ToDate, DeptID, DesgID, EmployeeID, WDID, Location, Department, isApproved }, ref errorMsg);
    }

    public DataTable GetEmployeeSeperationDetailsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeSeperationDetailsByID", new object[] { SeperationDetailsID }, ref errorMsg);
    }
    public string ApproveEmployeeSeperationDetailsByDepartment()
    {
        return DBManager.NonQueryCommand("usp_TF_ApproveEmployeeClearanceDetailsByDepartment", new object[] { SeperationDetailsID, ApprovedBy, Department, Comments, Deductions });
    }

    public DataTable GetSeperationDeductionItemsByEmployeeId(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetSeperationDeductionItemsByEmployeeId", new object[] { CompanyID, EmployeeID}, ref errorMsg);
    }

    public string AddSeperationDeductionItem()
    {
        return DBManager.NonQueryCommand("usp_TF_AddSeperationDeductionItem", new object[] { CompanyID, EmployeeID, ItemID, Assetno , NetAmount, Remarks, Department, CreatedBy });
    }
    public string AddSeperationDeductionItemOthers()
    {
        return DBManager.NonQueryCommand("usp_TF_AddSeperationDeductionItemOthers", new object[] { CompanyID, EmployeeID, ItemID, Assetno, NetAmount, Remarks, Department, CreatedBy, SeperationDetailsID });
    }
  

    #endregion

    #region employee leaves
    public int LeavesConsumed { get; set; }
    public string AddEmployeeLeaves()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeLeaves", new object[] { EmployeeID, LeaveTitle, NoOfLeaves, LeavesConsumed, CreatedBy });
    }
    
    public DataTable GetAllLeavesByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeLeavesByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public DataTable GetAllLeavesByEmployeeIDForDropDown(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeLeavesByEmployeeIDForDropDown", new object[] { EmployeeID }, ref errorMsg);
    }
    public string DeleteLeavesByEmployeeID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelLeavesByEmployeeID", new object[] { EmployeeID, DeletedBy });
    }
    #endregion

    #region Payroll
    public int PayrollID { get; set; }
    public string PayrollMonth { get; set; }
    public string PayrollYear { get; set; }

    public string AddPayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPayroll", new object[] { CompanyID, PayrollMonth, PayrollYear, CreatedBy, ChartOfAccountID });
    }

    public string UpdatePayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdatePayroll", new object[] { PayrollID, PayrollMonth, PayrollYear, ModifiedBy, ChartOfAccountID });
    }
    public string DeletePayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_DelPayrollByID", new object[] { PayrollID, DeletedBy });
    }

    public DataTable GetPayrollByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetByIDPayroll", new object[] { PayrollID }, ref errorMsg);
    }

    public DataTable GetAllPayrollsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllPayrollsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    #endregion

    #region payroll details
    public float Allownaces { get; set; }
    public float Deductions { get; set; }
    public float NetAmount { get; set; }
    public float Taxes { get; set; }

    public string AddPayrollDetails()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPayrollDetails", new object[] { PayrollID, EmployeeID, BasicSalary, Allownaces, Deductions, NetAmount, Taxes, PFEmpoyeeAmount, PFCompanyAmount, CreatedBy, GrossAmount });
    }

    public string DeletePayrollDetails()
    {
        return DBManager.NonQueryCommand("usp_TF_DelPayrollDetailsByPayrollID", new object[] { PayrollID, DeletedBy });
    }

    public DataTable GetPayrollDetailsByPayrollID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPayrollDetailsByPayrollID", new object[] { PayrollID }, ref errorMsg);
    }
    #endregion

    #region Performance Appraisals
    public int AppraisersID { get; set; }
    public int PerformanceAppraisalID { get; set; }
    public string TypeOfReview { get; set; }
    public string AppraisalDate { get; set; }

    public string AddPerformanceAppraisal()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPerformanceAppraisal", new object[] { EmployeeID, AppraisersID, TypeOfReview, AppraisalDate, CreatedBy });
    }

    public string UpdatePerformanceAppraisal()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdatePerformanceAppraisal", new object[] { EmployeeID, AppraisersID, TypeOfReview, AppraisalDate, ModifiedBy, PerformanceAppraisalID });
    }
    
    #endregion

    #region Appraisal Functionalt Traits
    public int EmpRating { get; set; }
    public int MngRating { get; set; }

    public string DeleteAppraisalFunctionaltTraits()
    {
        return DBManager.NonQueryCommand("usp_TF_DelAppraisalFunctionalTraits", new object[] { PerformanceAppraisalID, DeletedBy });
    }

    #endregion

    #region Appraisal QA
    public int AppraisalScheduleID { get; set; }
    public int QID { get; set; }
    public string EmpAnswer { get; set; }
    public string ManAnswer { get; set; }

    public string AddAppraisalQAByEmployee()
    {
        return DBManager.NonQueryCommand("usp_TF_AddAppraisalQAByEmployee", new object[] { PerformanceAppraisalID, AppraisalScheduleID, QID, EmpAnswer, Type, CompanyID, CreatedBy });
    }

    public string AddAppraisalQAByManager()
    {
        return DBManager.NonQueryCommand("usp_TF_AddAppraisalQAByManager", new object[] { PerformanceAppraisalID, AppraisalScheduleID, QID, ManAnswer, Type, CompanyID, CreatedBy });
    }

    #endregion

    #region Appraisal Schedule

    public string AddAppraisalSchedule()
    {
        return DBManager.NonQueryCommand("usp_TF_AddAppraisalSchedule", new object[] { StartDate, EndDate, CompanyID, CreatedBy });
    }

    public string UpdateAppraisalSchedule()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAppraisalSchedule", new object[] { AppraisalScheduleID, StartDate, EndDate, ModifiedBy });
    }

    public DataTable GetAllAppraisalSchedule(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAppraisalSchedule", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAppraisalScheduleByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAppraisalScheduleByID", new object[] { AppraisalScheduleID }, ref errorMsg);
    }

    #endregion

    #region HRReports
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string THLocationwhereClause { get; set; }
    public string WhereClause { get; set; }

    public DataTable GetBlackListReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetBlackListReport", new object[] { CompanyID, FromDate, ToDate }, ref errorMsg);
    }
   
    public DataTable GetNewHiringReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNewHiringReport", new object[] { CompanyID, FromDate, ToDate }, ref errorMsg);
    }
   
    public DataTable GetPayrollReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPayrollReport", new object[] { CompanyID, PayrollYear }, ref errorMsg);
    }
    public DataTable GetAttendanceReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAttendanceReport", new object[] { CompanyID, FromDate, ToDate }, ref errorMsg);
    }

    #endregion

    #region Doc Status
    public string TableName { get; set; }
    public string PrimaryColumnnName { get; set; }
    public string PrimaryColumnValue { get; set; }
    public string UpdateColumnName { get; set; }
    public string UpdateColumnValue { get; set; }
    public string DateColumnName { get; set; }
    public string DocStatus { get; set; }

    public string UpdateDocStatus()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateDocStatus", new object[] { TableName, PrimaryColumnnName, PrimaryColumnValue, UpdateColumnName, UpdateColumnValue, DateColumnName, DocStatus });
    }

    public string DeleteDraft()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteDraft", new object[] { TableName, PrimaryColumnnName, PrimaryColumnValue, UpdateColumnName, UpdateColumnValue, DateColumnName, DocStatus });
    }

    public string CheckDocAccessLevel(int LocUserID)
    {
        return DBManager.NonQueryCommand("usp_TF_CheckDocAccessLevel", new object[] { LocUserID, PrimaryColumnnName, PrimaryColumnValue, DocName, TableName });
    }

    public DataTable GetUserAccessByUserIDandDocName(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetUserAccessByUserIDandDocName", new object[] { UserID, DocName }, ref errorMsg);
    }
    #endregion

    #region sites
    public int SiteID { get; set; }
    public string SiteName { get; set; }
    public string SitePhone { get; set; }
    public string SiteEmail { get; set; }
    public string SiteCode { get; set; }
    public string ProjectManagerName { get; set; }
    public bool isCenteralWarehouse { get; set; }

    public DataTable GetAllSites(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllSites", new object[] { CompanyID }, ref errorMsg);
    }

    public string GetProjectCodeForNewItem()
    {
        return DBManager.NonQueryCommand("usp_TF_GetProjectCodeForNewItem", new object[] { CityName, CompanyID });
    }

    #endregion

    #region taxrates
    public int TaxRateID { get; set; }
    public string TaxRateName { get; set; }
    public string TaxRateShortName { get; set; }
    public float TaxRatePer { get; set; }
    public bool IsRecoverable { get; set; }
    public string TaxRateType { get; set; }

    public string AddTaxRates()
    {
        return DBManager.NonQueryCommand("usp_TF_AddTaxRates", new object[] { CompanyID, TaxRateName, TaxRateShortName, TaxRatePer, IsRecoverable, ChartOfAccountID, Description, ModifiedBy, TaxRateType });
    }

    public string UpdateTaxRates()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateTaxRates", new object[] { TaxRateID, TaxRateName, TaxRateShortName, TaxRatePer, ModifiedBy, Status, IsRecoverable, ChartOfAccountID, Description, TaxRateType });
    }

    public DataTable GetTaxRatesByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetTaxRatesByID", new object[] { TaxRateID }, ref errorMsg);
    }

    public DataTable GetAllTaxRates(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllTaxRates", new object[] { CompanyID }, ref errorMsg);
    }

    #endregion

    #region supplier
    public int SupplierID { get; set; }
    public string SupplierName { get; set; }
    public string SupplierEmail { get; set; }
    public string SupplierPhone { get; set; }
    public string NTN { get; set; }
    public string BusinessName { get; set; }

    public string AddSupplier()
    {
        return DBManager.NonQueryCommand("usp_TF_AddSupplier", new object[] { CompanyID, SupplierName, SupplierEmail, SupplierPhone, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, ModifiedBy, NTN, BusinessName, BankName, AccountNo, COAID });
    }

    public string usp_TF_GetSupplierCOAIDByID()
    {
        return DBManager.NonQueryCommand("usp_TF_GetSupplierCOAIDByID", new object[] { SupplierID });
    }

    public string GetEmployeeCOAIDByID()
    {
        return DBManager.NonQueryCommand("usp_TF_GetEmployeeCOAIDByID", new object[] { EmployeeID });
    }

    public DataTable GetAllSupplier(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllSupplier", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetActiveSupplier(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetActiveSupplier", new object[] { CompanyID }, ref errorMsg);
    }

    #endregion

    #region categories
    public int CategoryID { get; set; }
    public int SubCategoryID { get; set; }
    public string CategoryName { get; set; }
    
    #endregion

    #region items
    public int ItemID { get; set; }
    public string Assetno { get; set; }
    public string ItemName { get; set; }
    public string QRcodePath { get; set; }
    public string ItemCode { get; set; }
    public float UnitPrice { get; set; }
    public int Qty { get; set; }
    public string ItemUnit { get; set; }

    public string GetItemsCOAIDByID()
    {
        return DBManager.NonQueryCommand("usp_TF_GetItemsCOAIDByID", new object[] { ItemID });
    }

    public DataTable GetProductAndServiceCOAByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetProductAndServiceCOAByID", new object[] { ProductAndServicesID }, ref errorMsg);
    }

    public string GetAccountsCOAIDByID()
    {
        return DBManager.NonQueryCommand("usp_TF_GetAccountsCOAIDByID", new object[] { AccountID });
    }

    public string GetItemsCOATitleByID()
    {
        return DBManager.NonQueryCommand("usp_TF_GetItemsCOATitleByID", new object[] { ItemID });
    }

    public DataTable GetAllItems(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllItems", new object[] { CompanyID }, ref errorMsg);
    }
    
    #endregion

    #region rfqs
    public int RFQID { get; set; }

    public DataTable GetRFQItemsByRFQID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetRFQItemsByRFQID", new object[] { RFQID }, ref errorMsg);
    }
    #endregion

    #region pos
    public int POID { get; set; }
    public float POAmount { get; set; }
    public float WOT { get; set; }
    public float STWH { get; set; }
    public string ProgressStatus { get; set; }
    
    public string UpdatePOsProgressStatus()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdatePOsProgressStatus", new object[] { POID, ProgressStatus });
    }

    public DataTable GetPOsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPOsByID", new object[] { POID }, ref errorMsg);
    }

    public DataTable GetPOsByIDFotReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPOsByIDFotReport", new object[] { POID }, ref errorMsg);
    }

    public DataTable GetGRNByIDFotReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetGRNByIDFotReport", new object[] { GRNID }, ref errorMsg);
    }

    public DataTable GetActivePOs(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetActivePOs", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllPOsApprovedByFinance(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllPOsApprovedByFinance", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAllPOsForDirectPayment(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllPOsForDirectPayment", new object[] { CompanyID }, ref errorMsg);
    }

    public string GenereatePOCodeByRFQID()
    {
        return DBManager.NonQueryCommand("usp_TF_GenereatePOCodeByRFQID", new object[] { RFQID });
    }
    
    public string AddPOPay()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPOPay", new object[] { POID, CompanyID, PayDate, PayAmount, ModifiedBy, PayCOAID, PayType, AccountID, WOT, STWH });
    }

    public string AddPayrollPay()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPayrollPay", new object[] { PayrollID, CompanyID, PayDate, PayAmount, ModifiedBy, PayCOAID, PayType, AccountID, EmployeeID });
    }

    public string AddPOPayAgainstGRN()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPOPayAgainstGRN", new object[] { POID, GRNID, CompanyID, PayDate, PayAmount, ModifiedBy, PayCOAID, PayType, AccountID, WOT, STWH });
    }

    public string DeductPOPay()
    {
        return DBManager.NonQueryCommand("usp_TF_DeductPOPay", new object[] { POID, CompanyID, ModifiedBy });
    }

    public string AddGRNPay()
    {
        return DBManager.NonQueryCommand("usp_TF_AddGRNPay", new object[] { GRNID, CompanyID, PayDate, PayAmount, ModifiedBy, PayCOAID, PayType, AccountID });
    }
    #endregion

    #region poitems
    public float NetPrice { get; set; }

    public DataTable GetPOItemsByPOID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPOItemsByPOID", new object[] { POID }, ref errorMsg);
    }

    public string AddPOAttachments()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPOAttachments", new object[] { POID, AttachmentPath, ModifiedBy });
    }
    #endregion

    #region grns
    public int GRNID { get; set; }
    public string GRNCode { get; set; }
    public string SalesInvoiceNo { get; set; }
    public string GRNDate { get; set; }

    public string AddGRNsFinance()
    {
        return DBManager.NonQueryCommand("usp_TF_AddGRNsFinance", new object[] { CompanyID, GRNCode, SalesInvoiceNo, POID, SupplierID, GRNDate, Notes, ModifiedBy, POAmount, ChartOfAccountID, 0, JVID, LocationID });
    }
    
    public string UpdateGRNsFinance()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateGRNsFinance", new object[] { GRNID, GRNCode, SalesInvoiceNo, POID, SupplierID, GRNDate, Notes, POAmount, ChartOfAccountID, 0, JVID, LocationID });
    }

    public DataTable GetGRNsByIDFinance(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetGRNsByIDFinance", new object[] { GRNID }, ref errorMsg);
    }

    public DataTable GetGRNPayHistory(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetGRNPayHistory", new object[] { GRNID }, ref errorMsg);
    }

    public DataTable GetAllGRNs(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllGRNs", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllGRNsApprovedByFinance(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllGRNsApprovedByFinance", new object[] { CompanyID }, ref errorMsg);
    }

    public string GenereateGRNCodeByPOID()
    {
        return DBManager.NonQueryCommand("usp_TF_GenereateGRNCodeByPOID", new object[] { POID });
    }

    #endregion

    #region GRNitems
    
    public string AddGRNItemsFinance()
    {
        return DBManager.NonQueryCommand("usp_TF_AddGRNItemsFinance", new object[] { GRNID, ItemID, ItemUnit, Qty, Remarks, ModifiedBy, UnitPrice, NetPrice, ChartOfAccountID });
    }

    public string DeleteGRNItemsByGRNID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteGRNItemsByGRNID", new object[] { GRNID });
    }

    public DataTable GetGRNItemsByGRNIDFinance(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetGRNItemsByGRNIDFinance", new object[] { GRNID }, ref errorMsg);
    }
    public DataTable GetGRNAttachmentsByGRNID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetGRNAttachmentsByGRNID", new object[] { GRNID }, ref errorMsg);
    }

    #endregion

    #region mrns
    public int MRNID { get; set; }

    public DataTable GetMRNsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetMRNsByID", new object[] { MRNID }, ref errorMsg);
    }

    public DataTable GetActiveMRNs(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetActiveMRNs", new object[] { CompanyID }, ref errorMsg);
    }

    #endregion

    #region dbns
    public int DBNID { get; set; }
    public string DBNCode { get; set; }
    public string DBNDate { get; set; }
    public float AmountOfInvoice { get; set; }

    public string AddDBNs()
    {
        return DBManager.NonQueryCommand("usp_TF_AddDBNs", new object[] { CompanyID, DBNCode, DBNDate, MRNID, POID, SupplierID, SalesInvoiceNo, AmountOfInvoice, Notes, ModifiedBy, ChartOfAccountID });
    }

    public string UpdateDBNs()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateDBNs", new object[] { DBNID, DBNCode, DBNDate, MRNID, POID, SupplierID, SalesInvoiceNo, AmountOfInvoice, Notes, ChartOfAccountID });
    }

    public DataTable GetDBNsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDBNsByID", new object[] { DBNID }, ref errorMsg);
    }

    public DataTable GetAllDBNs(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDBNs", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetActiveDBNs(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetActiveDBNs", new object[] { }, ref errorMsg);
    }

    public string GenereateDBNCodeByMRNID()
    {
        return DBManager.NonQueryCommand("usp_TF_GenereateDBNCodeByMRNID", new object[] { MRNID });
    }

    #endregion

    #region DBNitems
    public string AddDBNItems()
    {
        return DBManager.NonQueryCommand("usp_TF_AddDBNItems", new object[] { DBNID, ItemID, ItemUnit, Qty, UnitPrice, NetPrice, Remarks, ModifiedBy });
    }

    public string DeleteDBNItemsByDBNID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteDBNItemsByDBNID", new object[] { DBNID });
    }

    public DataTable GetDBNItemsByDBNID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDBNItemsByDBNID", new object[] { DBNID }, ref errorMsg);
    }



    #endregion

    #region DBNattachments

    public string AddDBNAttachments()
    {
        return DBManager.NonQueryCommand("usp_TF_AddDBNAttachments", new object[] { DBNID, AttachmentPath, ModifiedBy });
    }

    public DataTable GetDBNAttachmentsByDBNID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDBNAttachmentsByDBNID", new object[] { DBNID }, ref errorMsg);
    }

    #endregion

    #region qcrs
    public string GenrationDate { get; set; }
    public float Price { get; set; }

    #endregion

    #region potaxes
    
    public string AddGRNTax()
    {
        return DBManager.NonQueryCommand("usp_TF_AddGRNTax", new object[] { GRNID, TaxRateID, ModifiedBy });
    }

    public string DelGRNTax()
    {
        return DBManager.NonQueryCommand("usp_TF_DelGRNTaxes", new object[] { GRNID, TaxRateID, ModifiedBy });
    }

    public DataTable GetPOTaxesByPOID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPOTaxesByPOID", new object[] { POID }, ref errorMsg);
    }
    public DataTable GetGRNTaxesByGRNID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetGRNTaxesByGRNID", new object[] { GRNID }, ref errorMsg);
    }

    #endregion

    #region COATranferMoney 

    public int COABalanceTransferID { get; set; }

    public DataTable GetAllCOABalanceTransfer(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCOABalanceTransfer", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetCOABalanceTransferByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCOABalanceTransferByID", new object[] { COABalanceTransferID }, ref errorMsg);
    }

    public string UpdateCOABalanceTransfer()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateCOABalanceTransfer", new object[] { COABalanceTransferID, FromCOA, ToCOA, AmountTransfer, TransferDate, Description, ModifiedBy, CompanyID });
    }
    public string AddCOABalanceTransfer()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCOABalanceTransfer", new object[] { FromCOA, ToCOA, AmountTransfer, TransferDate, Description, CreatedBy, CompanyID });
    }

    #endregion

    #region Lead 
    public int LeadID { get; set; }
    public string Description { get; set; }
    public float Discount { get; set; }

    public DataTable GetLeadByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLeadByID", new object[] { LeadID }, ref errorMsg);
    }

    public DataTable GetLeadByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLeadByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }
    public DataTable GetAllLeadsByClientID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLeadsByClientID", new object[] { ClientID }, ref errorMsg);
    }

    #endregion

    #region Client 
    public int ClientID { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string LastName { get; set; }
    public string CompanyEmail { get; set; }
    public string CompanyPhone1 { get; set; }
    public string CompanyPhone2 { get; set; }
    
    public string AddClient()
    {
        return DBManager.NonQueryCommand("usp_TF_AddClient", new object[] { CompanyID, FirstName, MiddleName, LastName, ContactNo, Email, AddressLine1, AddressLine2, CountryName, StateName, CityName, ZIPCode, UserID, CompanyName, CompanyShortName, CompanyDesc, CompanyEmail, CompanyPhone1, CompanyPhone2, CompanyLogo, CreatedBy, ChartOfAccountID, BusinessName, NTN, BankName, AccountNo, false, null });
    }
   
    public string UpdateClient()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateClient", new object[] { ClientID, CompanyID, FirstName, MiddleName, LastName, ContactNo, Email, AddressLine1, AddressLine2, CountryName, StateName, CityName, ZIPCode, UserID, CompanyName, CompanyShortName, CompanyDesc, CompanyEmail, CompanyPhone1, CompanyPhone2, CompanyLogo, ModifiedBy, ChartOfAccountID, BusinessName, NTN, BankName, AccountNo, false, null });
    }
   
    public DataTable GetClientByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetClientByID", new object[] { ClientID }, ref errorMsg);
    }

    public DataTable GetAllApprovedClients(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedClients", new object[] { CompanyID }, ref errorMsg);
    }
    #endregion

    #region Lead Reviews 

    public string Review { get; set; }
    public string AddLeadReview()
    {
        return DBManager.NonQueryCommand("usp_TF_AddLeadReviews", new object[] { LeadID, EmployeeID, Review, CreatedBy });
    }
    public string DelLeadReview()
    {
        return DBManager.NonQueryCommand("usp_TF_DelLeadReview", new object[] { LeadID, DeletedBy });
    }
    public DataTable GetAllLeadReviewsByLeadID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLeadReviewsByLeadID", new object[] { LeadID }, ref errorMsg);
    }
    #endregion

    #region Lead Attachments 

    public string Attachment { get; set; }
    
    public DataTable GetAllLeadAttachmentsByLeadID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLeadAttachmentsByLeadID", new object[] { LeadID }, ref errorMsg);
    }
    #endregion

    #region Lead Task 
    public int TaskID { get; set; }
    public string ManagerName { get; set; }

    public string CompleteLeadTask()
    {
        return DBManager.NonQueryCommand("usp_TF_CompleteLeadTask", new object[] { TaskID, ModifiedBy });
    }

    public DataTable GetAllLeadTaskByLeadIDAndEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLeadTaskByLeadIDAndEmployeeID", new object[] { LeadID, EmployeeID }, ref errorMsg);
    }

    #endregion

    #region Product And Services
    public int ProductAndServicesID { get; set; }
    public bool isBillableExpense { get; set; }
    public string AddProductAndServices()
    {
        return DBManager.NonQueryCommand("usp_TF_AddProductAndServices", new object[] { Name, Description, Price, ChartOfAccountID, isBillableExpense, CompanyID, CreatedBy });
    }

    public string UpdateProductAndServices()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateProductAndServices", new object[] { ProductAndServicesID, Name, Description, Price, ChartOfAccountID, isBillableExpense, ModifiedBy });
    }

    public DataTable GetProductAndServiceByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetProductAndServiceByID", new object[] { ProductAndServicesID }, ref errorMsg);
    }

    public DataTable GetAllApprovedProductAndService(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedProductAndService", new object[] { CompanyID }, ref errorMsg);
    }

    public string AddProductAndServicesTax()
    {
        return DBManager.NonQueryCommand("usp_TF_AddProductAndServicesTax", new object[] { ProductAndServicesID, TaxRateID });
    }
    public string DelProductAndServicesTax()
    {
        return DBManager.NonQueryCommand("usp_TF_DelProductAndServicesTax", new object[] { ProductAndServicesID });
    }
    public DataTable GetProductAndServiceTaxByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetProductAndServiceTaxByID", new object[] { ProductAndServicesID }, ref errorMsg);
    }
    #endregion

    #region Custom Access

    public string AddCustomAccess()
    {
        string s;
        return s = DBManager.NonQueryCommand("usp_TF_AddCustomAccess", new object[] { UserID, DocName, isNormUser, isApprover, isReviewer });
    }
    public string DelCustomAccessByUserID()
    {
        return DBManager.NonQueryCommand("usp_TF_DelCustomAccessByUserID", new object[] { UserID });
    }

    public DataTable GetCustomAccessByUserID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCustomAccessByUserID", new object[] { UserID }, ref errorMsg);
    }

    #endregion

    #region Dashboard

    public DataTable GetAssetCostByLastMonth(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_COST_BY_LAST_MON", new object[] { Month, CompanyID }, ref errorMsg);
    }

    public DataTable GetAssetCostBYDepDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_COST_BY_DEP_DATE", new object[] { StartDate, EndDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetAdditionByDepDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_COUNT_ADD_BY_DEP_DATE", new object[] { StartDate, EndDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetRevaluationByDepDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_COUNT_REV_BY_DEP_DATE", new object[] { StartDate, EndDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetDisposalByDepDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_COUNT_DIS_BY_DEP_DATE", new object[] { StartDate, EndDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetCWIPByDepDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_COUNT_CWIP_BY_DEP_DATE", new object[] { StartDate, EndDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetCWIPByLastMonth(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_CWIP_BY_LAST_MON", new object[] { Month, CompanyID }, ref errorMsg);
    }

    public DataTable GetAdditionByLastMonth(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_ADD_BY_LAST_MON", new object[] { Month, CompanyID }, ref errorMsg);
    }

    public DataTable GetDisposalByLastMonth(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_DIS_BY_LAST_MON", new object[] { Month, CompanyID }, ref errorMsg);
    }

    public DataTable GetRevaluationByLastMonth(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_REV_BY_LAST_MON", new object[] { Month, CompanyID }, ref errorMsg);
    }

    public DataTable GeTAssetCatByLastMonth(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_CAT_BY_LAST_MON", new object[] { Month, CompanyID }, ref errorMsg);
    }

    public DataTable GetAssetCatByDepDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_COUNT_CAT_BY_DEP_DATE", new object[] { StartDate, EndDate, CompanyID }, ref errorMsg);
    }

    #endregion

    #region Direct Payments

    public int DirectPaymentID { get; set; }
    public float TotalDebit { get; set; }
    public float TotalCredit { get; set; }
    public string Instrument { get; set; }
    public string SerialNo { get; set; }
    public string RefNO { get; set; }
    public string GeneratedDate { get; set; }

    public DataTable AddDirectPayments(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_AddDirectPayments", new object[] { CompanyID, TotalDebit, TotalCredit, VoucherNo, SerialNo, RefNO, GeneratedDate, ModifiedBy, Type, Description, Against, TableName, TableID, LocationID, isApproved, DocStatus }, ref errorMsg);
    }

    public DataTable GetDirectPaymentsByAgainstAndID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDirectPaymentsByAgainstAndID", new object[] { Against, TableID }, ref errorMsg);
    }

    public string UpdateAllDirectPaymentAmounts()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAllDirectPaymentAmounts", new object[] { });
    }

    public string UpdateDirectPayments()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateDirectPayments", new object[] { DirectPaymentID, TotalDebit, TotalCredit, VoucherNo, SerialNo, RefNO, GeneratedDate, ModifiedBy, Type, Description, Against, TableName, TableID, LocationID, DocStatus });
    }

    public DataTable GetDirectPaymentDetailByDirectPaymentID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDirectPaymentDetailByDirectPaymentID", new object[] { DirectPaymentID }, ref errorMsg);
    }
    public DataTable GetDirectPaymentsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDirectPaymentsByID", new object[] { DirectPaymentID }, ref errorMsg);
    }
    public DataTable GetAllDirectPayments(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDirectPayments", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GenerateSerialAndVoucherNo(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GenerateSerialAndVoucherNo", new object[] { CompanyID, Type }, ref errorMsg);
    }

    #endregion

    #region Assets List
    public int AssetID { get; set; }
    public string SerNo { get; set; }
    public string FACode { get; set; }
    public string PurchaseDate { get; set; }
    public string Location { get; set; }
    public string AssetsCustodian { get; set; }
    public string DepMethod { get; set; }
    public string AvgMethod { get; set; }
    public string DepRate { get; set; }
    public double TaxDepRate { get; set; }
    public string Path { get; set; }
    
    #endregion

    #region CWIP
    public string VoucherNo { get; set; }
    public double VoucherAmount { get; set; }

    #endregion

    #region Chart Of Account
    public double TaxRate { get; set; }
    public int CompanyID2 { get; set; }
    public int ChartOfAccountID { get; set; }
    public string CompanyCode { get; set; }
    public string GeoLocation { get; set; }
    public string GeoLocationCode { get; set; }
    public string Category { get; set; }
    public string CategoryCode { get; set; }
    public string SubCategoryName { get; set; }
    public string SubCategoryCode { get; set; }
    public string Account { get; set; }
    public string AccountCode { get; set; }
    public string SubAccount { get; set; }
    public string SubAccountCode { get; set; }
    public string GLCode { get; set; }
    public string AssetCode { get; set; }
    public string AmortizationCode { get; set; }
    public string Bank { get; set; }
    public string DepreciationCode { get; set; }
    public string GainLoss { get; set; }
    public int CostCenter { get; set; }
    public string AccumulatedDepreciationCode { get; set; }
    public string Code { get; set; }
    public int COA_AccountID { get; set; }
    public int COA_CategoryID { get; set; }
    public int COA_SubCategoryID { get; set; }
    public int COA_AccountTypeID { get; set; }
    public float OpeningBalance { get; set; }
    public int COA_ID { get; set; }
    public string Coatype { get; set; }
    public string Memo { get; set; }
    public string Mapping { get; set; }
    public string accountfor { get; set; }


    public int EmployeeCOA { get; set; }
    public float EmployeeTax { get; set; }
    public string COAFor { get; set; }

    public string AddCOA()
    {
        return DBManager.NonQueryCommand("usp_TF_ADD_COA", new object[] { GLCode, Description, CreatedBy, LocationID, CompanyID, COA_AccountTypeID, Account, AccountCode, TaxRateID, OpeningBalance, COAFor });
    }

    public string AddCOANew()
    {
        return DBManager.NonQueryCommand("usp_TF_ADD_COA_NEW", new object[] { GLCode, Description, CreatedBy, LocationID, CompanyID, COA_AccountTypeID, Account, AccountCode, TaxRateID, OpeningBalance, COAFor });
    }

    public string AddCOANewMapping()
    {
        return DBManager.NonQueryCommand("usp_TF_ADD_COA_V3", new object[] { Mapping, Coatype, Memo, accountfor, CreatedBy, CompanyID });
    }
	//public string AddCOANew(List<string> locationids)
	//{
	//    List<SqlParameter> param = new List<SqlParameter>();
	//    DataTable dtloc = new DataTable();
	//    dtloc.Columns.Add("Val");
	//    foreach (string item in locationids)
	//        dtloc.Rows.Add(item);
	//    SqlParameter empparam = new SqlParameter("@locations", SqlDbType.Structured);
	//    empparam.Value = dtloc;
	//    empparam.TypeName = "dbo.lstvarchar";
	//    param.Add(empparam);
	//    param.Add(new SqlParameter("@AccountType", Coatype));
	//    param.Add(new SqlParameter("@CoaCode", Memo));
	//    param.Add(new SqlParameter("@Accountfor", accountfor));
	//    param.Add(new SqlParameter("@Currency", CurrencyName));
	//    param.Add(new SqlParameter("@description", Description));
	//    param.Add(new SqlParameter("@createdby", CreatedBy));
	//    param.Add(new SqlParameter("@companyid", CompanyID));
	//    return DBManager.NonQueryCommandParam("usp_TF_ADD_COA_V3", param.ToArray());


	//}
	//public string UpdateCOA()
	//{
	//    return DBManager.NonQueryCommand("usp_TF_UPDATE_COA", new object[] { ChartOfAccountID, GLCode, Description, ModifiedBy, LocationID, CompanyID, COA_AccountTypeID, Account, AccountCode, TaxRateID, OpeningBalance, COAFor });
	//}
	public string UpdateCOA()
	{
		return DBManager.NonQueryCommand("usp_TF_UPDATE_COA", new object[] { ChartOfAccountID, Coatype, Memo, accountfor, CreatedBy, CompanyID });
	}
	public DataTable GetPayrollFields(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPayrollFields", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetCOAByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_COA_BY_ID", new object[] { ChartOfAccountID }, ref errorMsg);
    }

    public string AddCOAOpeningBal()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCOAOpeningBal", new object[] { CompanyID, ChartOfAccountID, LocationID, OpeningBalance });
    }

    public string DeleteCOAOpeningBalingBal()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteCOAOpeningBal", new object[] { CompanyID, ChartOfAccountID });
    }

    public DataTable GetCOAOpeningBalCOAID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCOAOpeningBalCOAID", new object[] { ChartOfAccountID }, ref errorMsg);
    }

    public DataTable GetAllCOAs(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_ALL_COA_v3", new object[] { CompanyID }, ref errorMsg);
    }
    //public DataTable GetAllCOAs(ref string errorMsg)
    //{
    //    return DBManager.DataTableCommand("usp_TF_GET_ALL_COA", new object[] { CompanyID }, ref errorMsg);
    //}
    
    public DataTable GetAllApprovedCOAs(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_ALL_APPROVED_COA", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApprovedCOAsFor(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_ALL_APPROVED_COAFor", new object[] { CompanyID, COAFor }, ref errorMsg);
    }
    
    public string AddCOA_Account()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCOA_Account", new object[] { Name, Code, COA_SubCategoryID, ParentCompanyID, CreatedBy });
    }
   
    public string AddCOA_Category()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCOA_Category", new object[] { Name, Code, CompanyID, CreatedBy });
    }
    
    public string AddCOA_SubCategory()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCOA_SubCategory", new object[] { Name, Code, CategoryID, ParentCompanyID, CreatedBy });
    }
    
    public DataTable GetAllCOA_Account(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCOA_Account", new object[] { ParentCompanyID }, ref errorMsg);
    }

    public DataTable GetAllCOA_Category(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCOA_Category", new object[] { ParentCompanyID }, ref errorMsg);
    }

    public DataTable GetAllCOA_SubCategory(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCOA_SubCategory", new object[] { ParentCompanyID }, ref errorMsg);
    }

    public string UpdateCOA_Account()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateCOA_Account", new object[] { COA_AccountID, Name, Code, ModifiedBy });
    }
    
    public string UpdateCOA_SubCategory()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateCOA_SubCategory", new object[] { COA_SubCategoryID, Name, Code, ModifiedBy });
    }

    public DataTable GetCOA_AccountCode(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_Get_AccountTypeCode", new object[] { CompanyID, LocationID, COA_AccountTypeID }, ref errorMsg);
    }

    public DataTable Gen_COA_SubCatCode(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_Gen_COA_SubCatCode", new object[] { COA_CategoryID, CompanyID }, ref errorMsg);
    }

    public DataTable Gen_COA_AccountType(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_Gen_COA_AccountType", new object[] { COA_SubCategoryID, CompanyID }, ref errorMsg);
    }

    public DataTable Gen_COA_SubAccount(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_Gen_COA_SubAccount", new object[] { COA_AccountTypeID, LocationID, CompanyID }, ref errorMsg);
    }

    #endregion

    #region Credit Debit Report
    public DataTable GetCOADetail(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCOADetail", new object[] { COA_ID, CompanyID }, ref errorMsg);
    }
    public DataTable GetCategoryCreditDebit(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCategoryCreditDebit", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetSubCategoryCreditDebit(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetSubCategoryCreditDebit", new object[] { CategoryID }, ref errorMsg);
    }
    public DataTable GetAccountCreditDebit(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAccountCreditDebit", new object[] { SubCategoryID }, ref errorMsg);
    }
    public DataTable GetCOACreditDebit(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCOACreditDebit", new object[] { AccountID }, ref errorMsg);
    }
    #endregion
       
    #region Accounting Depreciation
    
    public DataTable GetAllAccountingDepreciations(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_ALL_ACCOUNT_DEP", new object[] { CompanyID }, ref errorMsg);
    }
    #endregion

    #region Asset Type
    public int ATID { get; set; }
    public int ParentID { get; set; }
    public string AssetTypeName { get; set; }
    public string Name { get; set; }
    public string Prefix { get; set; }
    public int ActualDays { get; set; }
    public int FullMonth { get; set; }
    public double PerDep { get; set; }
    public double EffLife { get; set; }

    public DataTable GetAllAssetTypes(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_ALL_ASSET_TYPE", new object[] { CompanyID }, ref errorMsg);
    }

    #endregion

    #region Location
    public int LocationID { get; set; }

    public string AddLocation()
    {
        return DBManager.NonQueryCommand("usp_TF_ADD_LOCATION", new object[] { Name, Prefix, Description, CreatedBy, CompanyID, Code, SiteCode, SitePhone, SiteEmail, ProjectManagerName, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, isCenteralWarehouse });

    }

    public string UpdateLocation()
    {
        return DBManager.NonQueryCommand("usp_TF_UPDATE_LOCATION", new object[] { LocationID, Name, Prefix, Description, ModifiedBy, Code, SiteCode, SitePhone, SiteEmail, ProjectManagerName, CountryName, StateName, CityName, AddressLine1, AddressLine2, ZIPCode, isCenteralWarehouse });

    }
    public string DeleteLocation()
    {
        return DBManager.NonQueryCommand("usp_TF_DELETE_LOCATION", new object[] { LocationID, DeletedBy });

    }
    public DataTable GetLocationByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_LOCATION_BY_ID", new object[] { LocationID }, ref errorMsg);

    }

    public DataTable GetAllLocations(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_ALL_LOCATION", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAllLocationsNew(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GET_ALL_LOCATION_New", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApprovedLocations(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GETALLAPP_LOCATION", new object[] { CompanyID }, ref errorMsg);
    }

    #endregion

    #region Assets Revalution
    public double BookValue { get; set; }

    public DataTable GetAssetRevalutionByAseetID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GETALL_AR_BY_ASID", new object[] { AssetID }, ref errorMsg);
    }
    
    #endregion

    #region Bank Account
    public int AccountID { get; set; }
    public string AccountType { get; set; }
    public string NatureOfAccount { get; set; }
    public bool isOnline { get; set; }
    public double Balance { get; set; }

    public string AddAccount()
    {
        return DBManager.NonQueryCommand("usp_TF_AddAccount", new object[] { AccountTitle, AccountNo, AccountType, BankName, BranchName, NatureOfAccount, ChartOfAccountID, ManagerName, ContactNo, isOnline, Balance, CreatedBy, CompanyID });
    }

    public string UpdateAccount()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAccount", new object[] { AccountID, AccountTitle, AccountNo, AccountType, BankName, BranchName, NatureOfAccount, ChartOfAccountID, ManagerName, ContactNo, isOnline, Balance, CreatedBy });
    }

    public DataTable GetAllAccount(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAccount", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApprovedAccount(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedAccount", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAccountReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAccountReport", new object[] { StartDate, CompanyID }, ref errorMsg);
    }

    public DataTable GetAccountByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAccountByID", new object[] { AccountID }, ref errorMsg);
    }

    #endregion

    #region UserLog

    public string AddLog(int UserID, string UserName, string Action, string Module, string Description, int CompanyID, string TableName, int? TableID, string LoginIP, string LoginCountryName, string LoginCityName, string LoginState, string Loginlatitude, string Loginlongitude)
    {
        return DBManager.NonQueryCommand("usp_TF_AddLog", new object[] { UserID, UserName, Action, Module, Description, CompanyID, TableName, TableID, LoginIP, LoginCountryName, LoginCityName, LoginState, Loginlatitude, Loginlongitude });
    }

    public string AddLogNew(int UserID, string UserName, string Action, string Module, string Description, int CompanyID, string TableName, int? TableID, string LoginIP, string LoginCountryName, string LoginCityName, string LoginState, string Loginlatitude, string Loginlongitude, string DocumentViewPageLink, string DocumentManagePageLink, string NotificationMsg, string DocName, int? ESS)
    {
        return DBManager.NonQueryCommand("usp_TF_AddLogNew", new object[] { UserID, UserName, Action, Module, Description, CompanyID, TableName, TableID, LoginIP, LoginCountryName, LoginCityName, LoginState, Loginlatitude, Loginlongitude, DocumentViewPageLink, DocumentManagePageLink, NotificationMsg, DocName,ESS });
    }

    public DataTable GetAllLog(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLog", new object[] { CompanyID, FromDate, ToDate }, ref errorMsg);
    }
    public DataSet GetAllLog(ref string errorMsg, int numberOfRecord, int from)
    {
        return DBManager.DataSetCommand("usp_TF_GetAllLogWithPagination", new object[] { CompanyID, numberOfRecord, from }, ref errorMsg);
    }

    #endregion

    #region Balance Transfer
    public int BalanceTransferID { get; set; }
    public string TransferDate { get; set; }
    public int FromAccount { get; set; }
    public int ToAccount { get; set; }
    public double AmountTransfer { get; set; }

    public string AddBalanceTransfer()
    {
        return DBManager.NonQueryCommand("usp_TF_AddBalanceTransfer", new object[] { FromAccount, ToAccount, AmountTransfer, TransferDate, Description, CreatedBy, CompanyID });
    }

    public string UpdateBalanceTransfer()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateBalanceTransfer", new object[] { BalanceTransferID, FromAccount, ToAccount, AmountTransfer, TransferDate, Description, ModifiedBy, CompanyID });
    }

    public string TransferBalance()
    {
        return DBManager.NonQueryCommand("usp_TF_TransferBalance", new object[] { BalanceTransferID, AmountTransfer, FromAccount, ToAccount });
    }

    public DataTable GetAllBalanceTransfer(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllBalanceTransfer", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetBalanceTransferByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetBalanceTransferByID", new object[] { BalanceTransferID }, ref errorMsg);
    }
    #endregion

    #region Expenses
    public int EDMS_ID { get; set; }
    public int ExpencesID { get; set; }
    public int ExpenseItemID { get; set; }
    public string NatureOfVoucher { get; set; }
    public string ExpencesDate { get; set; }
    public float TotalBalance { get; set; }
    public int FromCOA { get; set; }
    public int ToCOA { get; set; }
    public string chequeNo { get; set; }
    public string chequeDate { get; set; }
    public string refNo { get; set; }
    public string paidTo { get; set; }

    public DataTable GetAllExpenseItems(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllExpenseItems", new object[] { ExpencesID }, ref errorMsg);
    }

    public string ADDExpenseItem()
    {
        return DBManager.NonQueryCommand("usp_TF_ADD_ExpenseItem", new object[] { ExpencesID, ItemID, ChartOfAccountID, ItemName, Price, TaxRate, NetAmount, ModifiedBy });
    }

    public string UpdateExpenseItem()
    {
        return DBManager.NonQueryCommand("usp_TF_Update_ExpenseItem", new object[] { ExpenseItemID, ItemID, ChartOfAccountID, ItemName, Price, TaxRate, NetAmount, ModifiedBy });
    }

    public DataTable GetAllExpenses(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllExpenses", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllExpensesBank(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllExpensesBank", new object[] { FromDate, ToDate, AccountID, CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApprovedExpensesBank(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedExpensesBank", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetExpenseVoucherNo(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetExpenseVoucherNo", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetExpensesByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetExpensesByID", new object[] { ExpencesID }, ref errorMsg);
    }

    public string ADDExpenses()
    {
        return DBManager.NonQueryCommand("usp_TF_ADD_Expense", new object[] { Title, ChartOfAccountID, Description, EDMS_ID, NatureOfVoucher, VoucherNo, ExpencesDate, isBillableExpense, ClientID, Taxes, TotalBalance, FromCOA, ToCOA, AccountNo, chequeNo, chequeDate, refNo, paidTo, CreatedBy, CompanyID });
    }

    public string UpdateExpenses()
    {
        return DBManager.NonQueryCommand("usp_TF_UPDATE_Expense", new object[] { ExpencesID, Title, ChartOfAccountID, Description, EDMS_ID, NatureOfVoucher, VoucherNo, ExpencesDate, isBillableExpense, ClientID, Taxes, TotalBalance, FromCOA, ToCOA, AccountNo, chequeNo, chequeDate, refNo, paidTo, ModifiedBy });
    }

    #endregion

    #region Quotation
    public int QuoteID { get; set; }
    public float TotalAmount { get; set; }


    public string AddQuotation()
    {
        return DBManager.NonQueryCommand("usp_TF_AddQuotation", new object[] { CompanyID, Code, Title, Description, ClientID, SiteID, Date, TotalAmount, Notes, ModifiedBy, ChartOfAccountID });
    }

    public string UpdateQuotation()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateQuotation", new object[] { QuoteID, CompanyID, Code, Title, Description, ClientID, SiteID, Date, TotalAmount, Notes, ModifiedBy, ChartOfAccountID });
    }

    public DataTable GetQuotationByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetQuotationByID", new object[] { QuoteID }, ref errorMsg);
    }

    public DataTable GetAllApprovedQuotations(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedQuotations", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetQuatationCode(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetQuatationCode", new object[] { CompanyID }, ref errorMsg);
    }
    #endregion

    #region Quotation items

    public string AddQuotationItems()
    {
        return DBManager.NonQueryCommand("usp_TF_AddQuotationItems", new object[] { QuoteID, ItemID, ItemUnit, Qty, UnitPrice, NetPrice, Remarks, ModifiedBy });
    }

    public string DeleteQuotationItemsByQuoteID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteQuotationItemsByQuoteID", new object[] { QuoteID });
    }

    public DataTable GetQuotationItemsByQuoteID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetQuotationItemsByQuoteID", new object[] { QuoteID }, ref errorMsg);
    }

    public DataTable GetQuotationItemsByQuoteIDForInvoice(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetQuotationItemsByQuoteIDForInvoice", new object[] { QuoteID }, ref errorMsg);
    }
    #endregion

    #region Quotation attachments
    public string AddQuotationAttachments()
    {
        return DBManager.NonQueryCommand("usp_TF_AddQuotationAttachments", new object[] { QuoteID, AttachmentPath, ModifiedBy });
    }

    public DataTable GetQuotationAttachmentsByQuoteID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetQuotationAttachmentsByQuoteID", new object[] { QuoteID }, ref errorMsg);
    }

    #endregion

    #region Quatation taxes

    public string AddQuotationTax()
    {
        return DBManager.NonQueryCommand("usp_TF_AddQuotationTax", new object[] { QuoteID, TaxRateID, ModifiedBy });
    }

    public string DelQuotationTaxes()
    {
        return DBManager.NonQueryCommand("usp_TF_DelQuotationTaxes", new object[] { QuoteID, TaxRateID, ModifiedBy });
    }

    public DataTable GetQuotationTaxesByQuoteID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetQuotationTaxesByQuoteID", new object[] { QuoteID }, ref errorMsg);
    }
    #endregion

    #region Invoice
    public int InvoiceID { get; set; }
    public bool isRecurring { get; set; }
    public bool isAuto { get; set; }
    public int InvoiceDuration { get; set; }
    public int DueDuration { get; set; }
    public int NoOfInvoices { get; set; }
    public float PayAmount { get; set; }
    public string PayDate { get; set; }
    public string PayType { get; set; }
    public int PayCOAID { get; set; }


    public string AddInvoice()
    {
        return DBManager.NonQueryCommand("usp_TF_AddInvoice", new object[] { CompanyID, Code, Title, Description, ClientID, SiteID, Date, TotalAmount, Notes, ModifiedBy, ChartOfAccountID, QuoteID, isRecurring, isAuto, InvoiceDuration, DueDuration, NoOfInvoices, Discount });
    }

    public string UpdateInvoice()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateInvoice", new object[] { InvoiceID, CompanyID, Code, Title, Description, ClientID, SiteID, Date, TotalAmount, Notes, ModifiedBy, ChartOfAccountID, QuoteID, isRecurring, isAuto, InvoiceDuration, DueDuration, NoOfInvoices, Discount });
    }

    public DataTable GetInvoiceByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetInvoiceByID", new object[] { InvoiceID }, ref errorMsg);
    }

    public DataTable GetInvoiceByIDForReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetInvoiceByIDForReport", new object[] { InvoiceID }, ref errorMsg);
    }

    public DataTable GetAllInvoices(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllInvoices", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApprovedInvoices(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedInvoices", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllInvoicesForDirectPayment(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllInvoicesForDirectPayment", new object[] { CompanyID }, ref errorMsg);
    }
    
    public DataTable GetAllInvoicesByClientIDByDate(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllInvoicesByClientIDByDate", new object[] { FromDate, ToDate, CompanyID, ClientID }, ref errorMsg);
    }

    public DataTable GetInvoiceCode(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetInvoiceCode", new object[] { CompanyID }, ref errorMsg);
    }

    public string AddInvoicePay()
    {
        return DBManager.NonQueryCommand("usp_TF_AddInvoicePay", new object[] { InvoiceID, CompanyID, PayDate, PayAmount, ModifiedBy, PayCOAID, PayType, AccountID, WOT, STWH });
    }
    public DataTable GetAllInvoicesPayHistoryBank(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllInvoicesPayHistoryBank", new object[] { FromDate, ToDate, AccountID, CompanyID }, ref errorMsg);
    }
    #endregion

    #region Invoice items
    public string AddInvoiceItems()
    {
        return DBManager.NonQueryCommand("usp_TF_AddInvoiceItems", new object[] { InvoiceID, ItemID, ItemUnit, Qty, UnitPrice, NetPrice, Remarks, ChartOfAccountID, ModifiedBy });
    }

    public string DeleteInvoiceItemsByInvoiceID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteInvoiceItemsByInvoiceID", new object[] { InvoiceID });
    }

    public DataTable GetInvoiceItemsByInvoiceID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetInvoiceItemsByInvoiceID", new object[] { InvoiceID }, ref errorMsg);
    }
    public DataTable GetInvoiceItemsByInvoiceIDWithFormat(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetInvoiceItemsByInvoiceIDWithFormat", new object[] { InvoiceID }, ref errorMsg);
    }

    public string GetClientCOAByClientID()
    {
        return DBManager.NonQueryCommand("usp_TF_GetClientCOAByClientID", new object[] { ClientID });
    }

    #endregion

    #region Invoice attachments
    public string AddInvoiceAttachments()
    {
        return DBManager.NonQueryCommand("usp_TF_AddInvoiceAttachments", new object[] { InvoiceID, AttachmentPath, ModifiedBy });
    }

    public DataTable GetInvoiceAttachmentsByInvoiceID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetInvoiceAttachmentsByInvoiceID", new object[] { InvoiceID }, ref errorMsg);
    }

    #endregion

    #region Invoice taxes
    public string AddInvoiceTax()
    {
        return DBManager.NonQueryCommand("usp_TF_AddInvoiceTax", new object[] { InvoiceID, TaxRateID, ModifiedBy });
    }

    public string DelInvoiceTaxes()
    {
        return DBManager.NonQueryCommand("usp_TF_DelInvoiceTaxes", new object[] { InvoiceID, TaxRateID, ModifiedBy });
    }

    public DataTable GetInvoiceTaxesByInvoiceID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetInvoiceTaxesByInvoiceID", new object[] { InvoiceID }, ref errorMsg);
    }
    #endregion

    #region CDNs
    public int CDNID { get; set; }
    public string CDNCode { get; set; }
    public string CDNDate { get; set; }

    public string AddCDNs()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCDNs", new object[] { CompanyID, CDNCode, CDNDate, InvoiceID, ClientID, SiteID, SalesInvoiceNo, AmountOfInvoice, Notes, ModifiedBy, ChartOfAccountID });
    }

    public string UpdateCDNs()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateCDNs", new object[] { CDNID, CDNCode, CDNDate, InvoiceID, ClientID, SiteID, SalesInvoiceNo, AmountOfInvoice, Notes, ChartOfAccountID });
    }

    public DataTable GetCDNsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCDNsByID", new object[] { CDNID }, ref errorMsg);
    }

    public DataTable GetAllCDNs(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCDNs", new object[] { CompanyID }, ref errorMsg);
    }

    public string GenereateCDNCodeByInvoiceID()
    {
        return DBManager.NonQueryCommand("usp_TF_GenereateCDNCodeByInvoiceID", new object[] { InvoiceID });
    }
    #endregion

    #region CDNitems

    public string AddCDNItems()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCDNItems", new object[] { CDNID, ItemID, ItemUnit, Qty, UnitPrice, NetPrice, Remarks, ModifiedBy });
    }

    public string DeleteCDNItemsByCDNID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteCDNItemsByCDNID", new object[] { CDNID });
    }

    public DataTable GetCDNItemsByCDNID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCDNItemsByCDNID", new object[] { CDNID }, ref errorMsg);
    }

    public string AddCDNAttachments()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCDNAttachments", new object[] { CDNID, AttachmentPath, ModifiedBy });
    }

    public DataTable GetCDNAttachmentsByCDNID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCDNAttachmentsByCDNID", new object[] { CDNID }, ref errorMsg);
    }

    #endregion

    #region Bank Reconcilation
    public int BankReconcilationID { get; set; }
    public int BankReconcilationDetailID { get; set; }
    public int? MatchBy { get; set; }
    public float? Credit { get; set; }
    public float? Debit { get; set; }

    public string AddBankReconcilation()
    {
        return DBManager.NonQueryCommand("usp_TF_AddBankReconcilation", new object[] { AccountID, FromDate, ToDate, Path, CreatedBy, CompanyID });
    }
    public string UpdateBankReconcilationDetails()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateBankReconcilationDetails", new object[] { BankReconcilationDetailID, MatchBy, Comments, ModifiedBy });
    }
    public string AddBankReconcilationDetails()
    {
        return DBManager.NonQueryCommand("usp_TF_AddBankReconcilationDetails", new object[] { BankReconcilationID, GenrationDate, Description, Credit, Debit, MatchBy, Comments, CreatedBy, CompanyID });
    }
    public DataTable GetAllBankReconcilation(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllBankReconcilation", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetBankReconcilationByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetBankReconcilationByID", new object[] { BankReconcilationID }, ref errorMsg);
    }
    public DataTable GetAllBankReconcilationDetailByBankReconcilationID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetBankReconcilationDetailByBankReconcilationID", new object[] { BankReconcilationID }, ref errorMsg);
    }
    #endregion

    #region Finance Report

    public DataTable GetPayableAgingByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPayableAgingByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetReceivableAgingByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetReceivableAgingByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetEmployeePFDeductionDetails(ref string errorMsg)
    {
        return DBManager.DataTableCommand("USP_PF_GetEmployeePFDeductionDetails", new object[] { CompanyID, FromDate, ToDate, DeptID, DesgID, EmployeeID, WorkDayID, Location }, ref errorMsg);
    }
    public DataTable GetEmployeeTaxDeductionDetails(ref string errorMsg)
    {
        return DBManager.DataTableCommand("USP_PF_GetEmployeeTaxDeductionDetails", new object[] { CompanyID, FromDate, ToDate, DeptID, DesgID, EmployeeID, WorkDayID, Location }, ref errorMsg);
    }

    #endregion

    #region TransactionHistory
    public int TransactionHistoryID { get; set; }
    public int TableID { get; set; }

    public string TransactionDetail { get; set; }


    public string AddTransactionHistoryDebit()
    {
        return DBManager.NonQueryCommand("usp_TF_AddTransactionHistoryDebit", new object[] { TableName, TableID, Debit, CompanyID, CreatedBy, ChartOfAccountID, ParentID, TransactionDetail, SerialNo, VoucherNo, Type, Against, LocationID, Instrument, DirectPaymentID, isApproved, DocStatus });
    }

    public string AddTransactionHistoryCredit()
    {
        return DBManager.NonQueryCommand("usp_TF_AddTransactionHistoryCredit", new object[] { TableName, TableID, Credit, CompanyID, CreatedBy, ChartOfAccountID, ParentID, TransactionDetail, SerialNo, VoucherNo, Type, Against, LocationID, Instrument, DirectPaymentID, isApproved, DocStatus });
    }

    public string DeleteTransactionHistoryByParentID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteTransactionHistoryByParentID", new object[] { ParentID, TableName });
    }
    
    public string DeleteTransactionHistoryByTableID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteTransactionHistoryByTableID", new object[] { TableID, TableName });
    }

    public DataTable GetCOABalance(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCOABalance", new object[] { COA_ID }, ref errorMsg);
    }

    #endregion

    #region Finance Voucher
    public int JVID { get; set; }
    public int BCVoucherID { get; set; }
    public int CreditCOA { get; set; }
    public int DebitCOA { get; set; }
    public string VouchrType { get; set; }
    public string Against { get; set; }
    public string Table1 { get; set; }
    public string Table2 { get; set; }
    public string Table3 { get; set; }

    public string AddFinanceVoucher()
    {
        return DBManager.NonQueryCommand("usp_TF_AddFinanceVoucher", new object[] { VoucherNo, Against, CreatedBy, CompanyID });
    }
    public DataTable GetAllFinanceVouchers(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllFinanceVouchers", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAllFinanceVouchersAgainst(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllFinanceVouchersAgainst", new object[] { Against, CompanyID }, ref errorMsg);
    }
    public string AddJVIDinPO()
    {
        return DBManager.NonQueryCommand("usp_TF_AddJVIDinPO", new object[] { JVID, POID });
    }

    public string AddJVIDinGRN()
    {
        return DBManager.NonQueryCommand("usp_TF_AddJVIDinGRN", new object[] { JVID, GRNID });
    }

    public string AddJVIDinExpense()
    {
        return DBManager.NonQueryCommand("usp_TF_AddJVIDinExpense", new object[] { JVID, ExpencesID });
    }

    public string AddJVIDinPayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_AddJVIDinPayroll", new object[] { JVID, NewPayrollID });
    }

    public string AddJVIDinInvoice()
    {
        return DBManager.NonQueryCommand("usp_TF_AddJVIDinInvoice", new object[] { JVID, InvoiceID });
    }
    
    public DataTable GetPODebitForVoucher(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPODebitForVoucher", new object[] { POID }, ref errorMsg);
    }

    public DataTable GetGRNDebitForVoucher(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetGRNDebitForVoucher", new object[] { GRNID }, ref errorMsg);
    }

    public DataTable GetPOCreditForVoucher(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPOCreditForVoucher", new object[] { POID }, ref errorMsg);
    }

    public DataTable GetGRNCreditForVoucher(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetGRNCreditForVoucher", new object[] { GRNID }, ref errorMsg);
    }

    public DataTable GetDebitForVoucher(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDebitForVoucher", new object[] { TableID, Table1, Table2, Table3 }, ref errorMsg);
    }

    public DataTable GetCreditForVoucher(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCreditForVoucher", new object[] { TableID, Table1, Table2, Table3 }, ref errorMsg);
    }

    public DataTable GetDebitForPayVoucher(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDebitForPayVoucher", new object[] { TableID, Table1 }, ref errorMsg);
    }

    public DataTable GetCreditForPayVoucher(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCreditForPayVoucher", new object[] { TableID, Table1 }, ref errorMsg);
    }

    public DataTable GetDepreciationCredit(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDepreciationCredit", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetDepreciationDebit(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDepreciationDebit", new object[] { CompanyID }, ref errorMsg);
    }

    public string AddBankCashVoucher()
    {
        return DBManager.NonQueryCommand("usp_TF_AddBankCashVoucher", new object[] { VouchrType, VoucherNo, Against, TableName, TableID, VoucherAmount, CreditCOA, DebitCOA, TransactionDetail, CreatedBy, CompanyID });
    }

    public DataTable GetAllBankVouchers(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllBankVouchers", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllCashVouchers(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCashVouchers", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetBankCashVoucherByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetBankCashVoucherByID", new object[] { BCVoucherID }, ref errorMsg);
    }
    #endregion

    #region super admin

    public DataTable GetAllCompaniesForSuperAdmin(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCompaniesForSuperAdmin", new object[] { }, ref errorMsg);
    }

    #endregion

    #region Dep Category

    public string AddDepCategory()
    {
        return DBManager.NonQueryCommand("usp_TF_AddDepCategory", new object[] { DeptID, CategoryName, Score, CreatedBy, CompanyID });
    }

    public string UpdateDepCategory()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateDepCategory", new object[] { CategoryID, DeptID, CategoryName, Score, ModifiedBy });
    }

    public DataTable GetDepCategoryByDepID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDepCategoryByDepID", new object[] { DeptID }, ref errorMsg);
    }

    public DataTable GetAllDepCategories(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDepCategories", new object[] { CompanyID }, ref errorMsg);
    }

    #endregion

    #region Category Question
    public int QuestionID { get; set; }
    public string Question { get; set; }
    public string QuestionType { get; set; }

    public string AddCatQuestion()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCatQuestion", new object[] { CategoryID, QuestionType, Question, CreatedBy, CompanyID });
    }

    public string UpdateCatQuestion()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateCatQuestion", new object[] { QuestionID, CategoryID, QuestionType, Question, ModifiedBy });
    }

    public DataTable GetCatQuestionsByDepID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCatQuestionsByDepID", new object[] { DeptID }, ref errorMsg);
    }

    #endregion

    #region PA Instruction
    public string Instructions { get; set; }
    public string AddPAInstuctions()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPAInstuctions", new object[] { Instructions, CreatedBy, CompanyID });
    }

    public DataTable GetPAInstructions(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetPAInstructions", new object[] { CompanyID }, ref errorMsg);
    }

    #endregion

    #region All Tables Attachments 

    public string AddAttachments()
    {
        return DBManager.NonQueryCommand("usp_TF_AddAttachments", new object[] { TableID, TableName, Title, Attachment, CreatedBy });
    }
    public string DelAttachments()
    {
        return DBManager.NonQueryCommand("usp_TF_DelAttachments", new object[] { TableID, TableName, DeletedBy });
    }
    public DataTable GetAllAttachmentsByTableIDAndName(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAttachmentsByTableIDAndName", new object[] { TableID, TableName }, ref errorMsg);
    }
    #endregion

    #region EmployeeBonus
    public int EmployeeBonusID { get; set; }
    public string BounusDate { get; set; }

    public string AddEmployeeBonus()
    {
        string res = DBManager.NonQueryCommand("usp_TF_AddEmployeeBonus", new object[] { CompanyID, Title, Description, BounusDate, CreatedBy });
        return res;
    }

    public string UpdateEmployeeBonus()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEmployeeBonus", new object[] { EmployeeBonusID, Title, Description, BounusDate, ModifiedBy });
    }
    
    public DataTable GetEmployeeBonusByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeBonusByID", new object[] { EmployeeBonusID }, ref errorMsg);
    }
    
    public string AddEmployeeBonusDetail()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeBonusDetail", new object[] { EmployeeID, Type, Amount, EmployeeBonusID, CreatedBy });
    }

    public string DeleteEmployeeBonusDetailByEmployeeBonusID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteEmployeeBonusDetailByEmployeeBonusID", new object[] { EmployeeBonusID });
    }

    public DataTable GetAllEmployeeBonusDetailByEmployeeBonusID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeBonusDetailByEmployeeBonusID", new object[] { EmployeeBonusID }, ref errorMsg);
    }
    #endregion

    #region Employee Leaves
    public int EMPLeaveID { get; set; }

    public DataTable GetAllEMPLeavesByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEMPLeavesByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetEMPLeavesByDirectNIndirectReporting(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEMPLeavesByDirectNIndirectReporting", new object[] { CompanyID, EmployeeID,module}, ref errorMsg);
    }
    public DataTable GetEMPLeavesByHR(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEMPLeavesByDirectNIndirectReporting", new object[] { CompanyID, EmployeeID }, ref errorMsg);
    }
    public DataTable GetEMPLeavesDetailsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEMPLeavesDetailsByID", new object[] { CompanyID, SelectedEmp }, ref errorMsg);
    }

    
    public string AddEMPLeave()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEMPLeaves", new object[] { CompanyID, EmployeeID, LeaveID, StartDate, EndDate, CreatedBy });
    }

    public string UpdateEMPLeave()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEMPLeaves", new object[] { EMPLeaveID, EmployeeID, LeaveID, StartDate, EndDate, ModifiedBy });
    }

    public string AddLeavesToEmployeeAttendance()
    {
        return DBManager.NonQueryCommand("usp_TF_AddLeavesToEmployeeAttendance", new object[] { EMPLeaveID });
    }
    
    public string AddHolidaysToEmployeeAttendanceKT()
    {
        return DBManager.NonQueryCommand("usp_TF_KT_AddHolidaysToEmployeeAttendance", new object[] { HolidayID, CompanyID });
    }
    
    public string CheckLeavesAvailability()
    {
        return DBManager.NonQueryCommand("usp_TF_CheckLeavesAvailability", new object[] { LeaveID, StartDate, EndDate });
    }
    public DataTable GetEMPLeaveByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEMPLeavesByID", new object[] { EMPLeaveID }, ref errorMsg);
    }
    
    #endregion

    #region LeaveEnCashment
    public int LeaveEnCashmentID { get; set; }
    public int LeaveEnCashQty { get; set; }
    public float PerDaySalary { get; set; }

    public DataTable GetAllLeaveEnCashmentByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLeaveEnCashmentByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public string AddLeaveEnCashment()
    {
        return DBManager.NonQueryCommand("usp_TF_AddLeaveEnCashment", new object[] { CompanyID, EmployeeID, LeaveID, LeaveEnCashQty, BasicSalary, PerDaySalary, TotalAmount, CreatedBy });
    }

    public string UpdateLeaveEnCashment()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateLeaveEnCashment", new object[] { LeaveEnCashmentID, EmployeeID, LeaveID, LeaveEnCashQty, BasicSalary, PerDaySalary, TotalAmount, ModifiedBy });
    }

    public string CheckLeavesAvailabilityForEnCashment()
    {
        return DBManager.NonQueryCommand("usp_TF_CheckLeavesAvailabilityForEnCashment", new object[] { LeaveID, LeaveEnCashQty });
    }

    public string UpdateEncashedLeaves()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEncashedLeaves", new object[] { LeaveEnCashmentID });
    }

    public DataTable GetLeaveEnCashmentByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllLeaveEnCashmentByID", new object[] { LeaveEnCashmentID }, ref errorMsg);
    }
   
    #endregion

    #region NewPayroll
    public int NewPayrollID { get; set; }
    public bool isMiniPayroll { get; set; }
    public string FinancialYear { get; set; }


    public string AddNewPayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewPayroll", new object[] { CompanyID, PayrollDate, isMiniPayroll,CreatedBy });
    }

    public string AddNewFnFPayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewFnFPayroll", new object[] { CompanyID, PayrollDate, CreatedBy });
    }

    public string DisburseNewPayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_DisburseNewPayroll", new object[] { NewPayrollID, PayrollDate, ModifiedBy });
    }
    public string DisburseNewFnF()
    {
        return DBManager.NonQueryCommand("usp_TF_DisburseNewFnF", new object[] { CompanyID, PayrollDate, ModifiedBy });
    }
    
    public string DisburseMiniPayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_DisburseMiniPayroll", new object[] { NewPayrollID, PayrollDate, ModifiedBy });
    }
    public string UpdateNewPayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateNewPayroll", new object[] { NewPayrollID, PayrollDate, ModifiedBy });
    }
    public string DeleteNewPayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_DelNewPayrollByID", new object[] { NewPayrollID, DeletedBy });
    }

    public DataTable GetNewPayrollByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetByIDNewPayroll", new object[] { NewPayrollID }, ref errorMsg);
    }
    public DataTable GetFnFPayrollByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetFnFPayrollByID", new object[] { NewPayrollID }, ref errorMsg);
    }
    public DataTable GetAllNewPayrollsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllNewPayrollsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAllFnFPayrollsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllFnFPayrollsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllFnFPendingsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllFnFPendingsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }


    public DataTable GetFiscalYear(ref string errorMsg)
    {
        return DBManager.DataTableCommand("Get_FiscalYear",new object[] { }, ref errorMsg);
    }

    public DataTable Get_Monthly_PF_report(string FiscalYear,ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_Get_Monthly_PF_report", new object[] { FiscalYear, CompanyID }, ref errorMsg);
    }
    public DataTable Get_PF_Detail_report(string MonthYear, ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_Get_PF_Detail_report", new object[] { MonthYear, CompanyID }, ref errorMsg);
    }
    public DataTable GetAllMiniPayrollsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllMiniPayrollsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAllAddOnsExpenseForMiniPayroll(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAddOnsExpenseForMiniPayroll", new object[] { CompanyID }, ref errorMsg);
    }
    
    #endregion

    #region New Payroll details

    public string EmployeeName { get; set; }
    public float OverTimeAmount { get; set; }
    public float FoodAllowence { get; set; }
    public float Maintenance { get; set; }
    public float Fuel { get; set; }
    public float AbsentAmount { get; set; }
    public float GrossSalary { get; set; }
    public float Tax { get; set; }
    public float AdditionalTax { get; set; }
    public float GrossSalaryWithOT { get; set; }
    public float Advance { get; set; }
    public float TotalSalry { get; set; }
    public float TotalSalryWithOT { get; set; }
    public float OverTime { get; set; }
    public float OverTimeGeneralAmount { get; set; }
    public float OvertimeHolidayAmount { get; set; }
    public float OverTimeTotalAmount { get; set; }
    public int Tax_COA_ID { get; set; }
    public int PF_COA_ID { get; set; }
    public int EMP_COA { get; set; }
    public double DepAmount { get; set; }
    public float TotalSalry2 { get; set; }
    public float OverTimeTotal { get; set; }
    public float GrossSalary2 { get; set; }
    public float Additions { get; set; }
    public float EmpDeductions { get; set; }
    public float EmployeePF { get; set; }
    public float TotalDeductions { get; set; }
    public float EmployeePFNeww { get; set; }    
    public float CompanyPF { get; set; }
    public float AddOnsAmount { get; set; }
    public float OtherExpensesAmount { get; set; }
    public string lblCOA { get; set; }

    public string RunPayroll()
    {
        return DBManager.NonQueryCommand("usp_TF_RunPayroll", new object[] { CompanyID, NewPayrollID, CreatedBy });
    }

    public string AddNewPayrollDetails()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewPayrollDetails", new object[] { NewPayrollID, EmployeeID, EmployeeCode, BasicSalary, EmployeeName, OverTime, OverTimeAmount, FoodAllowence, Maintenance, Fuel, AbsentAmount, GrossSalary, Tax, GrossSalaryWithOT, EOBI, Advance, TotalSalry, TotalSalryWithOT, COA_ID, PayrolllExpCOA, EMP_COA, GrossSalary2, OverTimeTotal, TotalSalry2, Additions, EmpDeductions, EmployeePF, CompanyPF });
    } 

    public string AddNewPayrollDetailsKT()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewPayrollDetailsKT", new object[] { NewPayrollID, EmployeeID, EmployeeCode, BasicSalary, EmployeeName, OverTime, OverTimeAmount, FoodAllowence, Maintenance, Fuel, AbsentAmount, GrossSalary, Tax, GrossSalaryWithOT, EOBI, Advance, TotalSalry, TotalSalryWithOT, COA_ID, PayrolllExpCOA, EMP_COA, GrossSalary2, OverTimeTotal, TotalSalry2, Additions, EmpDeductions, EmployeePF, CompanyPF, AddOnsAmount, OtherExpensesAmount });
    }

    public string AddNewKTPayrollDetailsKT()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewKTPayrollDetailsKT", new object[] { NewPayrollID, EmployeeID,DesgID,DeptID,CostCenterID,GradeID, EmployeeSalary, BasicSalary, HouseAllownace,UtitlityAllowance,MedicalAllowance,OverTimeGeneralAmount, OvertimeHolidayAmount, OverTimeTotalAmount,Commissions, EidBonus, Spiffs, Arrears,LeaveEncashment,Graduity, Severance,OtherBonuses,TotalBonuses,OtherExpensesAmount, GrossSalary,AbsentAmount,Tax , AdditionalTax ,EOBI,Advance,Deductions ,EmployeePFNeww,TotalDeductions ,NetSalary,TaxableIncome,CompanyEOBI , ExemptGraduity,CompanyPF,IESSIEmployeeRatio, OvertimeHolidayHours,OvertimeGeneralHours,OverTimeTotalHours,TotalDays,WorkingDays, AbsentDays ,PayrolllExpCOA,EmployeeCOA,CreatedBy});
    }
    
    public string UpdateAddOnsStatusByPayrollID()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAddOnsStatusByPayrollID", new object[] { NewPayrollID });
    }
    
    public string UpdateAddOnsPaidStatus()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAddOnsPaidStatus", new object[] { NewPayrollID });
    }  

    public string ResetAddOnsStatus()
    {
        return DBManager.NonQueryCommand("usp_TF_ResetAddOnsStatus", new object[] { NewPayrollID });
    }

    public string AddPayrollCOA()
    {
        return DBManager.NonQueryCommand("usp_TF_AddPayrollCOA", new object[] { CompanyID, lblCOA, COA_ID, CreatedBy });
    }

    public string DeleteNewPayrollDetails()
    {
        return DBManager.NonQueryCommand("usp_TF_DelNewPayrollDetailsByNewPayrollID", new object[] { NewPayrollID });
    }

    public string DeleteNewPayrollDetailsKT()
    {
        return DBManager.NonQueryCommand("usp_TF_DelNewPayrollDetailsByPayrollIDKT", new object[] { NewPayrollID });
    }
    public string SaveNewPayrollDetailsKT()
    {
        return DBManager.NonQueryCommand("usp_TF_SaveNewPayrollDetailsByPayrollIDKT", new object[] { NewPayrollID, PayrollDate, CreatedBy,CompanyID });
    }
    public string SaveMiniPayrollDetailsKT()
    {
        return DBManager.NonQueryCommand("usp_TF_SaveMiniPayrollDetailsByPayrollIDKT", new object[] { NewPayrollID, PayrollDate, CreatedBy,CompanyID });
    }
    public string UpdateNewPayrollFinance()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateNewPayrollFinance", new object[] { NewPayrollID, PayrollDate, ModifiedBy, ChartOfAccountID, Tax_COA_ID, PF_COA_ID });
    }

    public string AddNewPayrollFinance()
    {
        return DBManager.NonQueryCommand("usp_TF_AddNewPayrollFinance", new object[] { CompanyID, PayrollDate, CreatedBy, ChartOfAccountID, Tax_COA_ID, PF_COA_ID });
    }

    public DataTable GetNewPayrollDetailsByNewPayrollID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNewPayrollDetailsByNewPayrollID", new object[] { NewPayrollID }, ref errorMsg);
    }

    public DataTable GetKTPayrollDetailsByNewPayrollID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetKTPayrollDetailsByNewPayrollID", new object[] { NewPayrollID }, ref errorMsg);
    }


    public DataTable GetKTPayrollDetailsForActiveEmployeesByNewPayrollID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetKTPayrollDetailsForActiveEmployeesByNewPayrollID", new object[] { NewPayrollID, PayrollDate }, ref errorMsg);
    }

    public DataSet GetKTPayrollnNoPayroll(ref string errorMsg)
    {
        return DBManager.DataSetCommand("GetKTPayrollnNoPayroll", new object[] { NewPayrollID, PayrollDate,isMiniPayroll,  CompanyID}, ref errorMsg);
    }
    public DataTable GetKTPayrollDetailsForLeftEmployeesByNewPayrollID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetKTPayrollDetailsForLeftEmployees", new object[] { CompanyID,PayrollDate, CreatedBy }, ref errorMsg);
    }
    public DataTable IsFnFSettled(ref string errorMsg)
    {
        return DBManager.DataTableCommand2("usp_TF_IsFnFSettled", new object[] { PayrollDate }, ref errorMsg);
    }
    public DataTable GetFnFLockDetails(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetFnFLockDetails", new object[] { CompanyID, PayrollDate, CreatedBy }, ref errorMsg);
    }
    public DataSet GetFnFPendingDetails(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetFnFPendingDetails", new object[] { CompanyID, PayrollDate, CreatedBy }, ref errorMsg);
    }
    public DataTable GetKTPayrollDetailsForLeftEmployeesReport(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetKTPayrollDetailsForLeftEmployeesReport", new object[] { PayrollDate }, ref errorMsg);
    }
    
    public DataTable GetAssetsAndDeductions(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAssetsAndDeductions", new object[] { PayrollDate }, ref errorMsg);
    }
    public DataTable GetFnFSlipEmployeeByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetFnFSlipEmployeeByID", new object[] { SelectedEmp }, ref errorMsg);
    }
   
    public string usp_TF_AddnewFnF()
    {
        return DBManager.NonQueryCommand("usp_TF_AddnewFnF", new object[] { CompanyID, PayrollDate, CreatedBy });
    }

    public string ApproveEmployeeClearance(List<string> EmployeeIDs)
    {
        List<SqlParameter> param = new List<SqlParameter>();
        DataTable dtEmp = new DataTable();
        dtEmp.Columns.Add("Val");
        foreach (string item in EmployeeIDs)
            dtEmp.Rows.Add(item);
        SqlParameter empparam = new SqlParameter("@EmployeeID", SqlDbType.Structured);
        empparam.Value = dtEmp;
        empparam.TypeName = "dbo.lstvarchar";
        param.Add(empparam);
        param.Add(new SqlParameter("@PayrollMonth", Month));
        return DBManager.NonQueryCommandParam("usp_TF_ApproveEmployeeClearance", param.ToArray());
    }
    public string ReleaseFnF(int NewPayrollID, string[] EmployeeIDs, string payrollMonth, string approvedby)
    {
        List<SqlParameter> param = new List<SqlParameter>();
        DataTable dtEmp = new DataTable();
        dtEmp.Columns.Add("Val");
        foreach (string item in EmployeeIDs)
            dtEmp.Rows.Add(item);
        SqlParameter empparam = new SqlParameter("@EmployeeID", SqlDbType.Structured);
        empparam.Value = dtEmp;
        empparam.TypeName = "dbo.lstvarchar";
        param.Add(empparam);
        param.Add(new SqlParameter("@PayrollMonth", payrollMonth));
        param.Add(new SqlParameter("@ApprovedBy", approvedby));
        param.Add(new SqlParameter("@NewFnFID", NewPayrollID));
        return DBManager.NonQueryCommandParam("usp_TF_ReleaseFnF", param.ToArray());
    }
    public string LockPaymentFnF(int NewPayrollID,string[] EmployeeIDs, string payrollMonth, string approvedby)
    {
        List<SqlParameter> param = new List<SqlParameter>();
        DataTable dtEmp = new DataTable();
        dtEmp.Columns.Add("Val");
        foreach (string item in EmployeeIDs)
            dtEmp.Rows.Add(item);
        SqlParameter empparam = new SqlParameter("@EmployeeID", SqlDbType.Structured);
        empparam.Value = dtEmp;
        empparam.TypeName = "dbo.lstvarchar";
        param.Add(empparam);
        param.Add(new SqlParameter("@PayrollMonth", payrollMonth));
        param.Add(new SqlParameter("@NewPayrollID", NewPayrollID));
        param.Add(new SqlParameter("@ApprovedBy", approvedby));
        return DBManager.NonQueryCommandParam("usp_TF_LockPaymentFnF", param.ToArray());
    }
    public DataTable GetNewPayrollDetailsByNewPayrollIDForPay(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetNewPayrollDetailsByNewPayrollIDForPay", new object[] { NewPayrollID }, ref errorMsg);
    }

    public DataTable EmployeeAttendanceSheet(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_EmployeeAttendanceSheet", new object[] { EmployeeID, Date }, ref errorMsg);
    }

    public DataTable GeneratePayroll(ref string errorMsg)
    {
        return DBManager.DataTableCommand2("usp_TF_GenerateKTPayroll", new object[] { CompanyID, PayrollDate, CreatedBy }, ref errorMsg);
    }
    public DataTable IsPayrollPaid(ref string errorMsg)
    {
        return DBManager.DataTableCommand2("usp_TF_IsPayrollPaid", new object[] {  PayrollDate, CompanyID }, ref errorMsg);
    }
    public DataTable IsMiniPayrollPaid(ref string errorMsg)
    {
        return DBManager.DataTableCommand2("usp_TF_IsMiniPayrollPaid", new object[] { PayrollDate }, ref errorMsg);
    }
    public DataSet GenerateKTPayrollnNoPayroll(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_Payroll_V4", new object[] { CompanyID, PayrollDate, CreatedBy }, ref errorMsg);
    }
    public DataSet GenerateStandardPayrollnNoPayroll(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_Payroll_V5", new object[] { CompanyID, PayrollDate, CreatedBy }, ref errorMsg);
    }
    public DataSet GenerateMiniPayrollonNoPayroll(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_MiniPayrollOnNoPayroll_V3", new object[] { CompanyID, PayrollDate, CreatedBy }, ref errorMsg);
    }
    public DataSet GenerateMiniPayrollonAddons(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_MiniPayrollOnAddons_V3", new object[] { CompanyID, PayrollDate, CreatedBy }, ref errorMsg);
    }
    //public DataSet GenerateKTMiniPayrollnNoPayroll(ref string errorMsg)
    //{

    //    List<SqlParameter> param = new List<SqlParameter>();

    //    param.Add(new SqlParameter("@CompanyID", CompanyID));
    //    param.Add(new SqlParameter("@date", PayrollDate));
    //    param.Add(new SqlParameter("@CreatedBy", CreatedBy));
    //    return DBManager.DataSetCommandNoTimeOut("usp_TF_MiniPayroll_V3", param.ToArray(), ref errorMsg);
    //}


    //public DataSet GenerateKTMiniPayrollnNoPayroll(ref string errorMsg)
    //{
    //    return DBManager.DataSetCommandNoTimeOut("usp_TF_MiniPayroll_V3", new object[] { CompanyID, PayrollDate, CreatedBy }, ref errorMsg);
    //}
    public DataTable GenerateKTPayrollNew(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GenerateKTPayrollNew", new object[] { CompanyID, PayrollDate, CreatedBy }, ref errorMsg);
    }

    public DataTable GenerateKTMiniPayrollNew(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GenerateKTMiniPayrollNew", new object[] { CompanyID, CreatedBy }, ref errorMsg);
    }
    public class addOnModel
    {
        public string employeeid { get; set; }
        public string addonid { get; set; }
    }
    public DataTable GenerateKTMiniPayrollWithAddOns(List<string> AddOnIDs)
	{
		
        List<SqlParameter> param = new List<SqlParameter>();
        DataTable dtEmp = new DataTable();
        dtEmp.Columns.Add("Val");
		foreach (string item in AddOnIDs)
		    dtEmp.Rows.Add(item);
		SqlParameter empparam = new SqlParameter("@Addons", SqlDbType.Structured);
        empparam.Value = dtEmp;
        empparam.TypeName = "dbo.lstvarchar";param.Add(empparam);
		param.Add(new SqlParameter("@PayrollMonth", PayrollDate));
        param.Add(new SqlParameter("@CompanyID", CompanyID));
        param.Add(new SqlParameter("@CreatedBy", CreatedBy));
        return DBManager.NonQueryCommandParamTable("usp_TF_GenerateKTMiniPayrollNewManually", param.ToArray());
	}

	public DataTable GetDailyAttendanceSheet(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetDailyAttendanceSheet", new object[] { CompanyID, PayrollDate }, ref errorMsg);
    }

    public DataTable GetLeaveConsumption(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLeaveConsumption", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetLateComersAttendanceSheet(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetLateComersAttendanceSheet", new object[] { CompanyID, PayrollDate }, ref errorMsg);
    }

    #endregion

    #region AttendanceAdjustment
    public int AttendanceAdjustmentID { get; set; }
    public string AttendanceAdjustmentRemarks { get; set; }
    public bool isNextDay { get; set; }
    public int BreakAdjustmentID { get; set; }
    public string BreakAdjustmentRemarks { get; set; }

    public string AddAttendanceAdjustment()
    {
        return DBManager.NonQueryCommand("usp_TF_AddAttendanceAdjustment", new object[] { CompanyID, EmployeeID, TimeIn, TimeOUt, Resultant, BreakStartTime, BreakEndTime, Date, isNextDay, CreatedBy });
    }

    public string AddAttendanceAdjustment(ref int AdjustmentId)
    {
        string errormsg = "";
        DataTable dt = DBManager.DataTableCommand("usp_TF_AddAttendanceAdjustment_New", new object[] { CompanyID, EmployeeID, TimeIn, TimeOUt, Resultant, BreakStartTime, BreakEndTime, Date, isNextDay, CreatedBy },ref errormsg);
        AdjustmentId = (int)dt.Rows[0]["id"];
        return dt.Rows[0]["msg"].ToString() ;
    }

    public string UpdateAttendanceAdjustment()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAttendanceAdjustment", new object[] { AttendanceAdjustmentID, EmployeeID, TimeIn, TimeOUt, Resultant, BreakStartTime, BreakEndTime, Date, isNextDay, ModifiedBy });
    }

    public DataTable GetAllAttendanceAdjustmentByCompanyID(string employeeId, string departmentId, string designationId, string ApprovalStatus, ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAttendanceAdjustmentByCompanyID", new object[] { FromDate, ToDate, CompanyID, ApprovalStatus, employeeId, departmentId, designationId, KTID }, ref errorMsg);
    }

    public DataTable GetAllAttendanceAdjustmentByDirectReportingPerson(ref string errorMsg, int EmployeeID, string ApprovalStatus = "0")
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAttendanceAdjustmentByCompanyID", new object[] { CompanyID, ApprovalStatus , EmployeeID }, ref errorMsg);
    }

    public DataTable GetAllAttendanceAdjustmentByEmployeeID(string ManagerID, ref string errorMsg, string ApprovalStatus = "0")
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAttendanceAdjustmentByEmployeeID", new object[] { FromDate, ToDate, ManagerID, EmployeeID, ApprovalStatus }, ref errorMsg);
    }
    public DataTable GetAuthorizationLogsByEmployeeID(string ManagerID, int selectedEmp, ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAuthorizationLogsByEmployeeID", new object[] { FromDate, ToDate, ManagerID, selectedEmp }, ref errorMsg);
    }
    public DataSet GetAuthorizationLogsByEmployeeID(string ManagerID, int selectedEmp, int numberOfRecords, int from, ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetAuthorizationLogsByEmployeeIDWithPagination", new object[] { FromDate, ToDate, ManagerID, selectedEmp, numberOfRecords , from }, ref errorMsg);
    }
    public DataTable GetTrackingByEmployeeID(string EmployeeID, int selectedEmp, ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetTrackingByEmployeeID", new object[] { FromDate, ToDate, EmployeeID, selectedEmp }, ref errorMsg);
    }
    
    public DataTable GetEmployeesOverTime(string ManagerID,string ApprovalStatus, ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeesOverTime", new object[] { FromDate, ToDate, CompanyID, DeptID, DesgID, EmployeeID, ManagerID == EmployeeID.ToString() ? "0" : ManagerID,ApprovalStatus }, ref errorMsg);
    }
    public DataTable GetOverTimeGroup(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetOverTimeGroup", new object[] { CompanyID },ref errorMsg);
    }

    public string AddBreakAdjustment(ref int AdjustmentId)
    {
        string errormsg = "";
        DataTable dt = DBManager.DataTableCommand("usp_TF_AddBreakAdjustment_New", new object[] { EmployeeID, BreakStartTime, BreakEndTime, Resultant, Date, CreatedBy }, ref errormsg);
        AdjustmentId = (int)dt.Rows[0]["id"];
        return dt.Rows[0]["msg"].ToString();
    }

    public string UpdateBreakAdjustment()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateBreakAdjustment", new object[] { BreakAdjustmentID, EmployeeID, BreakStartTime, BreakEndTime, Resultant, Date, ModifiedBy });
    }

    public string ApproveBreakAdjustment()
    {
        return DBManager.NonQueryCommand("usp_TF_ApproveBreakAdjustment", new object[] { BreakAdjustmentID });
    }

    public string EnableBreakAdjustment()
    {
        return DBManager.NonQueryCommand("usp_TF_EnableBreakAdjustment", new object[] { BreakAdjustmentID });
    }

    public DataTable GetAllBreakAdjustmentByEmployeeID(string ManagerID, ref string errorMsg, string ApprovalStatus = "0")
    {
        return DBManager.DataTableCommand("usp_TF_GetAllBreakAdjustmentByEmployeeID", new object[] { FromDate, ToDate, ManagerID, EmployeeID, ApprovalStatus }, ref errorMsg);
    }

    public DataTable GetAllBreakAdjustments(ref string errorMsg, string ApprovalStatus = "0")
    {
        return DBManager.DataTableCommand("usp_TF_GetAllBreakAdjustments", new object[] { FromDate, ToDate, EmployeeID, ApprovalStatus }, ref errorMsg);
    }
    public DataTable GetBreakAdjustmentStatus(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetBreakAdjustmentStatus", new object[] { }, ref errorMsg);
    }

    public DataTable GetBreakAdjustmentByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetBreakAdjustmentByID", new object[] { BreakAdjustmentID }, ref errorMsg);
    }

    public string UpdateBreakAdjustmentStatus()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateBreakAdjustmentStatus", new object[] { BreakAdjustmentID, Status });
    }

    public string RejectBreakAdjustment()
    {
        return DBManager.NonQueryCommand("usp_TF_RejectBreakAdjustment", new object[] { BreakAdjustmentID, BreakAdjustmentRemarks, ModifiedBy });
    }

    public string GetEmployeeShiftStartTime()
    {
        return DBManager.NonQueryCommand("usp_TF_GetEmployeeShiftStartTime", new object[] { Date, StartDate, EndDate, EmployeeID }); ;
    }

    public DataTable GetAttendanceAdjustmentByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAttendanceAdjustmentByID", new object[] { AttendanceAdjustmentID }, ref errorMsg);
    }

    public string ApproveAttendanceAdjustment()
    {
        return DBManager.NonQueryCommand("usp_TF_ApproveAttendanceAdjustmentNew", new object[] { AttendanceAdjustmentID,ApprovedBy });
    }

    public string EnableAttendanceAdjustment()
    {
        return DBManager.NonQueryCommand("usp_TF_EnableAttendanceAdjustment", new object[] { AttendanceAdjustmentID });
    }

    public string UpdateAttendanceAdjustmentStatus()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAttendanceAdjustmentStatus", new object[] { AttendanceAdjustmentID, Status });
    }

    public DataTable GetAttendanceAdjustmentStatus(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAttendanceAdjustmentStatus", new object[] { }, ref errorMsg);
    }

    public string RejectAttendanceAdjustment()
    {
        return DBManager.NonQueryCommand("usp_TF_RejectAttendanceAdjustment", new object[] { AttendanceAdjustmentID, AttendanceAdjustmentRemarks, ModifiedBy });
    }

    public DataTable GetEmployeeMonthlyAttendanceDetails(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeMonthlyAttendanceDetails", new object[] { Month,CompanyID }, ref errorMsg);
    }

    public string LockAttendanceForPayroll()
    {
      
        return DBManager.NonQueryCommand("usp_TF_LockAttendanceForPayroll", new object[] { Month, CompanyID });
    }
    //public string LockAttendanceForPayroll(List<string> EmployeeIDs)
    //{
    //    List<SqlParameter> param = new List<SqlParameter>();
    //    DataTable dtEmp = new DataTable();
    //    dtEmp.Columns.Add("Val");
    //    foreach (string item in EmployeeIDs)
    //        dtEmp.Rows.Add(item);
    //    SqlParameter empparam = new SqlParameter("@EmployeeID", SqlDbType.Structured);
    //    empparam.Value = dtEmp;
    //    empparam.TypeName = "dbo.lstvarchar";
    //    param.Add(empparam);
    //    param.Add(new SqlParameter("@PayrollMonth", Month));
    //    return DBManager.NonQueryCommandParam("usp_TF_LockAttendanceForPayroll", param.ToArray());
    //}

    #endregion

    #region COA Quick Setup

    public int COAQuickSetupID { get; set; }
    public int COAID { get; set; }
    public bool isNew { get; set; }

    public string AddCOAQuickSetup()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCOAQuickSetup", new object[] { CompanyID, Title, COAID, CreatedBy, isNew });
    }

    public string UpdateCOAQuickSetup()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateCOAQuickSetup", new object[] { COAQuickSetupID, Title, COAID, ModifiedBy });
    }

    public string GetCOAQuickSetupBybYtITLE()
    {
        return DBManager.NonQueryCommand("usp_TF_GetCOAQuickSetupBybYtITLE", new object[] { Title, CompanyID });
    }

    public DataTable GetAllCOAQuickSetupByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCOAQuickSetupByID", new object[] { COAQuickSetupID }, ref errorMsg);
    }

    public DataTable GetAllCOAQuickSetupByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCOAQuickSetupByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetJournalLedgerNew(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_Comm_GetJournalLedgerNew", new object[] { WhereClause }, ref errorMsg);
    }
    public DataTable GetTrialBalance(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_Comm_GetTrialBalance", new object[] { StartDate, EndDate, THLocationwhereClause, CompanyID }, ref errorMsg);
    }

    public DataTable GetAEPLTrialBalance(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_Comm_GetAEPLTrialBalance", new object[] { StartDate, EndDate, THLocationwhereClause, CompanyID }, ref errorMsg);
    }

    #endregion

    #region Currency
    public int CurrencyID { get; set; }
    public string TypeID { get; set; }
    public string CurrencyName { get; set; }
    public string Abbrivation { get; set; }
    public double Rate { get; set; }

    public string AddCurrency()
    {
        return DBManager.NonQueryCommand("usp_TF_AddCurrency", new object[] { CompanyID, CurrencyName, Abbrivation, Rate, Notes });
    }
    public string UpdateCurrencyByID()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateCurrency", new object[] { CurrencyID, CurrencyName, Abbrivation, Rate, ModifiedBy, Notes });
    }
    public string DeleteCurrency()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteCurrencyByID", new object[] { ModifiedBy, CurrencyID });
    }
    public DataTable getCurrencyByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetCurrencyByID", new object[] { CurrencyID }, ref errorMsg);
    }
    public DataTable GetAllCurrencyByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllCurrencyByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    #endregion
    
    #region  Addons 
    public int IsAllow { get; set; }
    public int AddOnsID { get; set; }
    public bool IsDeduction { get; set; }

    public string AddAddOns()
    {
        return DBManager.NonQueryCommand("usp_TF_AddAddOnsHead", new object[] { CompanyID, Title, CurrencyID, IsAllow, Notes, IsDeduction, CreatedBy, TypeID, DeptID });
    }

    public string UpdateAddOns()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAddOnsHead", new object[] { CompanyID, Title, CurrencyID, IsAllow, Notes, AddOnsID, ModifiedBy, TypeID, DeptID });
    }
    
    public DataTable GetAddOnsByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAddOnsHeadByID", new object[] { AddOnsID }, ref errorMsg);
    } 
    
    public DataTable GetAllDeductionsHeadByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDeductionsHeadByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllApprovedAddOnsHeadByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedAddOnsHeadByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetEmployeeSalaryDetails(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeSalaryDetails", new object[] { SelectedEmp, CompanyID, StartDate }, ref errorMsg);
    }

    public DataTable GetAllApprovedAddOnsHeadByCompanyIDandEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedAddOnsHeadByCompanyIDandEmployeeID", new object[] { CompanyID,EmployeeID }, ref errorMsg);
    }  
    
    public DataTable GetAllApprovedAddOnsHeadByCompanyIDandEmployeeIDForESS(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedAddOnsHeadByCompanyIDandEmployeeIDForESS", new object[] { CompanyID,EmployeeID }, ref errorMsg);
    }

    public DataTable GetAllApprovedDeductionsHeadByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllApprovedDeductionsHeadByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllAddOnsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAddOnsHeadByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetAllAttendanceFiles(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAttendanceFiles", new object[] { module, uploadedby }, ref errorMsg);
    }


    public DataTable GetAllAddonsBulkFiles(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAddonsBulkFiles", new object[] { module, uploadedby }, ref errorMsg);
    }
    public DataSet GetBulkSuccessRecords(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetBulkSuccessRecords", new object[] { SheetID }, ref errorMsg);
    }
    public DataSet GetAttendanceBulkSuccessRecords(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_GetAttendanceBulkSuccessRecords", new object[] { SheetID }, ref errorMsg);
    }

    public string DeleteBulkSheet(ref string errorMsg)
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteBulkSheet", new object[] { SheetID });
    }
    public string DeleteAttendanceBulkSheet(ref string errorMsg)
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteAttendanceBulkSheet", new object[] { SheetID });
    }
    public DataSet RunBulkFile(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_RunBulkFile", new object[] { SheetID, CreatedBy, PayrollDate, CompanyID }, ref errorMsg);
    }
    public DataSet RunAttendanceBulkFile(ref string errorMsg)
    {
        return DBManager.DataSetCommand("usp_TF_RunAttendanceBulkFile", new object[] { SheetID, CreatedBy, PayrollDate }, ref errorMsg);
    }
    public string AddnewSheetAddons(ref string errorMsg)
    {
        List<SqlParameter> param = new List<SqlParameter>();


        param.Add(new SqlParameter("@SheetName", SheetName));
        param.Add(new SqlParameter("@SheetType", SheetType));
        param.Add(new SqlParameter("@CreatedBy", CreatedBy));
        param.Add(new SqlParameter("@uploadedby", uploadedby));
        return DBManager.NonQueryCommandParam("usp_TF_AddnewSheetAddons", param.ToArray());
    }
    public string AddnewSheetAttendance(ref string errorMsg)
    {
        List<SqlParameter> param = new List<SqlParameter>();


        param.Add(new SqlParameter("@SheetName", SheetName));
        param.Add(new SqlParameter("@SheetType", SheetType));
        param.Add(new SqlParameter("@Month", Month));
        param.Add(new SqlParameter("@CreatedBy", CreatedBy));
        param.Add(new SqlParameter("@uploadedby", uploadedby));
        return DBManager.NonQueryCommandParam("usp_TF_AddnewSheetAttendance", param.ToArray());
    }


    #endregion

    #region AddOnsExpense
    public int AddOnsExpenseID { get; set; }
    public double CurrencyRate { get; set; }
    public double AmountAfterCurrency { get; set; }
    public int Days { get; set; }
    public string AddonMonth { get; set; }
    
    public string AddAddOnsExpense()
    {
        return DBManager.NonQueryCommand("usp_TF_AddAddOnsExpense", new object[] { CompanyID, EmployeeID, AddOnsID, CurrencyID, Amount, CurrencyRate, AmountAfterCurrency, Notes, IsDeduction, Status, Attachment, PreparedBy });
    }

    public string AddAddOnsExpenseNew(string AttachmentPath)
    {
        return DBManager.NonQueryCommand("usp_TF_AddAddOnsExpenseNew", new object[] { CompanyID, EmployeeID, AddOnsID, CurrencyID, Amount, CurrencyRate, AmountAfterCurrency, Notes, IsDeduction, Days,OverTimeSimple,OverTimeSpecial, AddonMonth , AttachmentPath, Salary, CreatedBy });
    }

    public string UpdateAddOnsExpense()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAddOnsExpense", new object[] { CompanyID, EmployeeID, AddOnsID, CurrencyID, Amount, CurrencyRate, AmountAfterCurrency, ModifiedBy, Notes, AddOnsExpenseID });
    }
    
    public string UpdateAddOnsExpenseNew(string AttachmentPath)
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateAddOnsExpenseNew", new object[] { CompanyID, EmployeeID, AddOnsID, CurrencyID, Amount, CurrencyRate, AmountAfterCurrency, ModifiedBy, Notes, AddOnsExpenseID, Days, OverTimeSimple, OverTimeSpecial, AddonMonth, AttachmentPath,Salary });
    }

    public string DeleteAddOnsExpenseByID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteAddOnsExpenseByID", new object[] { DeletedBy, AddOnsExpenseID });
    }

    public DataTable GetAddOnsExpenseByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAddOnsExpenseByID", new object[] { AddOnsExpenseID }, ref errorMsg);
    }

    
     public DataTable GetAllAddOnsByCurrentUser(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAddOnsByCurrentUser", new object[] { CreatedBy }, ref errorMsg);
    }
   
    public DataTable GetAllAddOnsExpenseByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAddOnsExpenseByCompanyID", new object[] { CompanyID, FromDate, ToDate }, ref errorMsg);
    }
    
    public DataTable GetAllDeductionsByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDeductionsByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetAllAddOnsExpenseByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllAddOnsExpenseByEmployeeID", new object[] { EmployeeID }, ref errorMsg);
    }

    public string Get_EmployeeTurnover()
    {
        return DBManager.NonQueryCommand("usp_TF_GetEmpTurnOverRatio", new object[] { CompanyID, FromDate, ToDate });
    }

    #endregion
    
    #region Overtime
    public int OvertimeRatioID { get; set; }
    public int WorkingDaysRatio { get; set; }
    public int WeakendsRatio { get; set; }
    public int JDID { get; set; }
    public int Active { get; set; }

    public string AddOvertimeRatio()
    {
        return DBManager.NonQueryCommand("usp_TF_AddOvertimeRatio", new object[] { CompanyID, JDID, WorkingDaysRatio, WeakendsRatio, Active, CreatedBy, Notes });
    }

    public string UpdateOvertimeRatio()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateOvertimeRatio", new object[] { JDID, WorkingDaysRatio, WeakendsRatio, Active, CreatedBy, Notes, OvertimeRatioID });
    }
    public string DeleteOvertimeRatiobyID()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteOvertimeRatio", new object[] { DeletedBy, AddOnsExpenseID });
    }
    public DataTable GetAllOvertimeRatioByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllOvertimeRatioByID", new object[] { OvertimeRatioID }, ref errorMsg);
    }
    public DataTable GetAllOvertimeRatioByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllOvertimeRatioByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    #endregion
    
    #region PayrollDate
    public int PayrollStartDate { get; set; }
    public int PayrollEndDate { get; set; }
    public int PayrollDateID { get; set; }
    public int DeviceUserID { get; set; }
    public string Salary { get; set; }
    public string Bonus { get; set; }

    public DataTable CalculateTaxWithBonuses(ref string errorMsg)
    {
        //return DBManager.ItaxDataTableCommand("usp_itax_CalculateTaxWithBonus", new object[] { Salary, Bonus, Year, 1 }, ref errorMsg);
        return DBManager.DataTableCommand("usp_TF_CalculateTaxWithBonus", new object[] { Salary, Bonus, Year }, ref errorMsg);
    }

    public DataTable GetPayrollDates(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllPayrollDate", new object[] { CreatedBy, CompanyID }, ref errorMsg);
    }

    public string UpdatePayrollDates()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdatePayrollDate", new object[] { PayrollStartDate, PayrollEndDate, ModifiedBy, CompanyID, PayrollDateID });
    }
    #endregion

    #region ChangedWorkingShift
    public DateTime EffectiveDate { get; set; }
    public int ChangeWorkingShiftID { get; set; }
    public string AddWorkingChangedShift()
    {
        return DBManager.NonQueryCommand("usp_TF_AddWorkingShiftChanged", new object[] { CompanyID, EmployeeID, ShiftID, CreatedBy, Notes });
    }
    public string AddApprovedWorkingShiftChanged()
    {
        return DBManager.NonQueryCommand("usp_TF_AddApprovedWorkingShiftChanged", new object[] { CompanyID, EmployeeID, ShiftID, CreatedBy, Notes });
    }
    public string UpdateWorkingShiftChanged()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateWorkingShiftChanged", new object[] { CompanyID, EmployeeID, ShiftID, ModifiedBy, Notes, ChangeWorkingShiftID });
    }
    public string DeleteWorkingShiftChanged()
    {
        return DBManager.NonQueryCommand("usp_TF_DeleteWorkingShiftChanged", new object[] { DeletedBy, ChangeWorkingShiftID });
    }
    public DataTable getWorkingShiftChangedByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllWorkingShiftChangedByID", new object[] { ChangeWorkingShiftID }, ref errorMsg);
    }
    public DataTable GetAllWorkingShiftChangedByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllWorkingShiftChangedByCompanyID", new object[] { CompanyID }, ref errorMsg);
    }
    public DataTable GetEmployees_CurrentShifts(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployees_CurrentShifts", new object[] { CompanyID, DeptID, DesgID, EmployeeID, KTID, Location }, ref errorMsg);
    }
    public DataTable GetAllWorkingShiftChangedAcceptHimByCompanyID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllWorkingShiftChangedAcceptHimByCompanyID", new object[] { CompanyID, EmployeeID }, ref errorMsg);
    }

    public DataTable GetWorkingShiftDetailsById(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetWorkingShiftDetailsByID", new object[] { CompanyID, EmployeeID}, ref errorMsg);
    }

    #endregion

    #region EmployeSeperation
    public int EmployeeFullAndFinalID { get; set; }
    public double Bouses { get; set; }
    public double Commissions { get; set; }
    public float EidBonus { get; set; }
    public float Spiffs { get; set; }
    public float Arrears { get; set; }
    public double Graduity { get; set; }
    public double SeverancePay { get; set; }
    public double AttendanceTotal { get; set; }
    public int LeaveEncashmentDays { get; set; }
    public double LeaveEncashment { get; set; }
    public double Severance { get; set; }
    public float OtherBonuses { get; set; }
    public float TotalBonuses { get; set; }
    public string AddEmployeeFullAndFinal()
    {
        return DBManager.NonQueryCommand("usp_TF_AddEmployeeFullAndFinal", new object[] { EmployeeID, Days,OverTimeSimple,OverTimeSpecial, AttendanceTotal, LeaveEncashmentDays, LeaveEncashment, Bouses, Commissions, Graduity, EmployeePF, CompanyPF, SeverancePay, CreatedBy,Notes, CompanyID });
    }

    public string UpdateEmployeeFullAndFinal()
    {
        return DBManager.NonQueryCommand("usp_TF_UpdateEmployeeFullAndFinal", new object[] { EmployeeID, Days, OverTimeSimple, OverTimeSpecial, AttendanceTotal, LeaveEncashmentDays, LeaveEncashment, Bouses, Commissions, Graduity, EmployeePF, CompanyPF, SeverancePay, CreatedBy, Notes, EmployeeFullAndFinalID });
    }


    public string ApproveEmployeeFullAndFinal()
    {
        return DBManager.NonQueryCommand("usp_TF_ApproveEmployeeFullAndFinal", new object[] { EmployeeID, SeverancePay,  Notes, EmployeeFullAndFinalID,CreatedBy, CompanyID });
    }


    public DataTable GetAllEmployeeFullAndFinal(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllEmployeeFullAndFinal", new object[] { CompanyID }, ref errorMsg);
    }

    public DataTable GetEmployeeFullAndFinalByID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeFullAndFinalByID", new object[] { EmployeeFullAndFinalID }, ref errorMsg);
    }

    public DataTable GetSeperationDataByEmployeeID(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetEmployeeSeperationDataByEmployeeID", new object[] { EmployeeID,CompanyID }, ref errorMsg);
    }

    #endregion
    
    #region OverTime Request
    public DataTable OverTimeRequest(int attendanceId, int EmployeeId, float RequestOT,string Remarks,int RequestBy ,
        ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_OverTimeRequest", new object[] { attendanceId,EmployeeId,RequestOT,Remarks,RequestBy}, ref errorMsg);
    }
    public DataTable OverTimeReview(DateTime date, int EmployeeId, float ReviewOT, string Remarks, int ReviewBy,
        ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_OverTimeReview", new object[] { date, EmployeeId, ReviewOT, Remarks, ReviewBy }, ref errorMsg);
    }
    public DataTable OverTimeApprove(DateTime date, int EmployeeId, float ApproveOT, string Remarks, int ApproveBy,
        ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_OverTimeApprove", new object[] { date, EmployeeId, ApproveOT, Remarks, ApproveBy }, ref errorMsg);
    }
    #endregion

    #region Access Level Checking
    public bool CheckModuleAccessLevel(string moduleName, int userId)
    {
        DataTable dt = new DataTable();
        string errorMsg = string.Empty;
        UserID = userId;

        dt = GetCustomAccessByUserID(ref errorMsg);

        if (dt == null)
        {
            dt = GetUserDetailsByID(ref errorMsg);
        }
        else
        if (dt.Rows.Count == 0)
        {
            dt = GetUserDetailsByID(ref errorMsg);
        }

        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["UserAccess"].ToString().ToLower() == moduleName.ToLower())
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    #endregion

    public DataTable GetScrubData(ref string errorMsg)
    {
        return DBManager.DataTableCommand("usp_TF_GetAllDepartmentsByCompanyID", new object[] { EmployeeID }, ref errorMsg);
    }
}
