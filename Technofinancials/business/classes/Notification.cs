﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Technofinancials.business.classes
{
    public class Notification
    {
        public string NotificationID { get; set; }
        public string NotificationMsg { get; set; }
        public string DocumentViewPageLink { get; set; }
        public string DocumentManagePageLink { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string isBell { get; set; }
        public string UserID { get; set; }
    }
}