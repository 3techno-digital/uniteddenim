﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class AdminVaraiables
{
    public string Status { get; set; }
    public string ModifiedBy { get; set; }
    public string CreatedBy { get; set; }
    public string PreparedBy { get; set; }
    public string PreparedDate { get; set; }
    public string ReviewedBy { get; set; }
    public string ReviewedDate { get; set; }
    public string ApprovedBy { get; set; }
    public string ApprovedDate { get; set; }
    public string DeletedBy { get; set; }

    public string CountryID { get; set; }
    public string CountryName { get; set; }
    public string StateID { get; set; }
    public string StateName { get; set; }
    public string CityID { get; set; }
    public string CityName { get; set; }
    public string AddressLine1 { get; set; }
    public string AddressLine2 { get; set; }
    public string ZIPCode { get; set; }

    public string CompanyID { get; set; }
    public string ParentCompanyID { get; set; }
    public string CompanyName { get; set; }
    public string CompanyShortName { get; set; }
    public string CompanyDesc { get; set; }
    public string CompanyNTN { get; set; }
    public string CompanySTRN { get; set; }
    public string CompanySECP { get; set; }
    public string CompanyARN { get; set; }
    public string CompanySTN { get; set; }
    public string CompanyLogo { get; set; }
    public string CEOName { get; set; }

    public string DeptID { get; set; }
    public string DeptName { get; set; }
    public string DeptDesc { get; set; }
    public string DeptParentID { get; set; }

    public string GradeID { get; set; }
    public string GradeName { get; set; }

    public string KPIID { get; set; }
    public string KPIDesc { get; set; }
    public string KPIType { get; set; }

    public string DesgID { get; set; }
    public string DesgTitle { get; set; }

    public string UserID { get; set; }
    public string EmployeeID { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string SQuesID { get; set; }
    public string SQAnswer { get; set; }
    public string PasswordKey { get; set; }
    public string Ques1 { get; set; }
    public string Ans1 { get; set; }
    public string Ques2 { get; set; }
    public string Ans2 { get; set; }
    public string UserName { get; set; }
    public string EmpCode { get; set; }
}

public class DesignationVariables
{
    public string DesgID { get; set; }
    public string DesgTitle { get; set; }
    public string DeptID { get; set; }
    public string Status { get; set; }
}

public class EmployeeDropdownVariables
{
    public string EmployeeID { get; set; }
    public string EmployeeName { get; set; }
}