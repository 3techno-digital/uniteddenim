﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Technofinancials.business.classes
{
    public class EssAttendance
    {
        public bool TimeIn { get; set; }
        public bool BreakIn { get; set; }
        public bool BreakOut { get; set; }
        public bool TimeTimeOut { get; set; }

    }
    public class EssTimeIn
    {

        public string TimeIN { get; set; }
        public string BreakTime { get; set; }
        public string StartTime { get; set; }
        public string BreakStartTime { get; set; }
        public string BreakEndTime { get; set; }
        public string EndTime { get; set; }
        public string BreakStartEndTime { get; set; }
        public string BreakInSec { get; set; }
        public string WorkHoursSec { get; set; }
        public string RemainingBreakTime { get; set; }
        public string BtnStatus { get; set; }
    }
}