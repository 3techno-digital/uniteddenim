﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Technofinancials.business.classes
{
	public static class Helper
	{
		public static string GetDashWhenZero(this string text)
		{
			if (text== "0" || text == "0.00")
			{
				text = "-";
			}

			return text;
		}
		public static decimal RoundDecimalTwoDigit(this decimal number)
		{
			return decimal.Round(number, 2, MidpointRounding.AwayFromZero);
		}
	}
}