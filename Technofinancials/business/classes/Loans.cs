﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Technofinancials.business.classes
{
	public class Loans
	{

		public int subpurposeid { get; set; }
		public string subpurposename { get; set; }

		public int attachmentid { get; set; }
		public string attachmentname { get; set; }
	}
}