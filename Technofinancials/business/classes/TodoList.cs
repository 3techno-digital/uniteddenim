﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Technofinancials.business.classes
{
    public class TodoList
    {
        public string ToDoListID { get; set; }
        public string Description { get; set; }
        public string IsDone { get; set; }
        public string Checked { get; set; }

    }
}