﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Technofinancials.business.classes
{
	public class FnF
	{
		public string employeename { get; set; }
		public string wdid { get; set; }
		public string OtherDeductionsInput { get; set; }

		public string BasicSalary { get; set; }
		public string MedicAllowance { get; set; }
		public string UtitlityAllownce { get; set; }
		 
		public string HouseRent { get; set; }
		public string OtherDeduction { get; set; }
		public string TotalDeduction { get; set; }
		public string AdditionalTaxperMonth { get; set; }
		public string YearlyTaxableIncome { get; set; }
		public string YearlyTax { get; set; }
		public string AdditionalTax_ { get; set; }
		public string SeperationDetailsID { get; set; }
		public string empid { get; set; }
		public string PFContribution { get; set; }
		public string EmpPF { get; set; }
		public string CompPF { get; set; }
		public string TypeOfSeperation { get; set; }
		public string ReasoForLeaving { get; set; }
		public string IsITCleared { get; set; }
		public string ITClearedBy { get; set; }
		public string ITClearedOn { get; set; }
		public string ITComments { get; set; }
		public string ITStatus { get; set; }
		public string IsHRCleared { get; set; }
		public string HRClearedBy { get; set; }
		public string HRClearedOn { get; set; }
		public string HRComments { get; set; }
		public string HRStatus { get; set; }
		public string IsFinanceCleared { get; set; }
		public string FinanceClearedBy { get; set; }
		public string FinanceClearedOn { get; set; }
		public string FinanceComments { get; set; }
		public string FinanceStatus { get; set; }
		public string ClearanceDeductionAmount { get; set; }
		public string CommentsRegardingSalary { get; set; }
		public string EmployeeSeperationDate { get; set; }
		public string EmailOrCallForwardingPeriod { get; set; }
		public string NoticePeriodEndDate { get; set; }
		public string EmailIDStatus { get; set; }
		public string CB { get; set; }
		public string CD { get; set; }
		public string ModifiedBy { get; set; }
		public string isDeleted { get; set; }
		public string DeletedBy { get; set; }
		public string DeletedDate { get; set; }
		public string DocStatus { get; set; }
		public string ApprovedBy { get; set; }
		public string ApprovedOn { get; set; }
		public string IsPayrollGenerated { get; set; }
		public string TaxableIncome_ { get; set; }
		public string TotalDays_ { get; set; }
		public string PaidAdditionalTax { get; set; }
		public string deptName { get; set; }
		public string GrossSalary { get; set; }
		public string graduityAddonid { get; set; }
		public string Gratuity { get; set; }
		public string deductAddonid { get; set; }
		public string AdonsDeductions { get; set; }
		public string NetAmount { get; set; }
		public string  GeneralOverTimeAmount          { get; set; }
		public string  HolidayOverTimeAmount		  { get; set; }
		 public string  WeekendOverTimeAmount		  { get; set; }
		 public string  EidOverTimeAmount    		  { get; set; }
		 public string  TotalOverTimeAmount  		  { get; set; }
		 public string  TotalIncrementedAmount		  { get; set; }
		 public string  TotalDecrementedAmount		  { get; set; }
		 public string  Commission_          		  { get; set; }
		 public string  SalesComission_      		  { get; set; }
		 public string  Arrears_             		  { get; set; }
		 public string  Spiffs_              		  { get; set; }
		 public string  Severance_           		  { get; set; }
		 public string  Reimbursement_       		  { get; set; }
		 public string  LeaveEncashment_     		  { get; set; }
		 public string  PreviousDaysAmount_  		  { get; set; }
		 public string  BonusTotal_          		  { get; set; }
		 public string  ExpenseReimb_        		  { get; set; }
		 public string  AddonOverTime_       		  { get; set; }
		 public string  AttendanceAdjustment_		  { get; set; }
		 public string  PaidLeaves_          		  { get; set; }
		 public string  TotalTaxableAddons_  		  { get; set; }
		 public string  AbsentDaysDeduction_ 		  { get; set; }
		 public string  AddonEOBI_           		  { get; set; }
		 public string  EOBI_                		  { get; set; }
		 public string  ProvidentFund				  { get; set; }
		 public string MonthlyTax  { get; set; }








	}
}