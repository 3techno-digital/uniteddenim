﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Technofinancials.business.classes
{
	public class FnFSlipModel
	{
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
       
            public int wdid { get; set; }
            public string location { get; set; }
            public string designation { get; set; }
            public string Section { get; set; }
            public DateTime DateOfJoining { get; set; }
            public string Address { get; set; }
            public string Bank { get; set; }
            public string branch { get; set; }
            public string NTN { get; set; }
            public int Leave { get; set; }
            public DateTime Resign { get; set; }
            public int NoticeDays { get; set; }
            public string Employeename { get; set; }
            public string department { get; set; }
            public string EmploymentType { get; set; }
            public string Grade { get; set; }
            public string DateofConfirmation { get; set; }
            public string dob { get; set; }
            public string Accountno { get; set; }
            public string cnic { get; set; }
            public object Shift { get; set; }
            public double StandardGross { get; set; }
            public DateTime lastworkingday { get; set; }
            public string Tenure { get; set; }
            public int BasicSalary { get; set; }
            public double commission { get; set; }
            public double EidBonus { get; set; }
            public double otherbonuses { get; set; }
            public int HouseRent { get; set; }
            public int MedicalAllowance { get; set; }
            public int UtilityAllowance { get; set; }
            public double leaveencashment { get; set; }
            public double otherexpense { get; set; }
            public double spiffs { get; set; }
            public double Overtime { get; set; }
            public double gratuity { get; set; }
            public double employeePF { get; set; }
            public double CompanyPF { get; set; }
            public int IncomeTax { get; set; }
            public double AdditionalTaxpermonth { get; set; }
            public int EOBI { get; set; }
            public int EOBIArrears { get; set; }
            public int PFDeduction { get; set; }
            public string itemname { get; set; }
            public double Itemamount { get; set; }
            public string deductingDept { get; set; }
     


    }
}