﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public static class DBManager
{
    private static Database _database;
    private static Database _databaseItax;

    private static DatabaseProviderFactory _dbFactory;

    private static void CreateDB()
    {
        _dbFactory = new DatabaseProviderFactory();
        _database = _dbFactory.Create("DefaultConnection");
        _databaseItax = _dbFactory.Create("itaxConnection");
    }

 public static Database GetConnection()
	{
        _dbFactory = new DatabaseProviderFactory();
        _database = _dbFactory.Create("DefaultConnection");
        _databaseItax = _dbFactory.Create("itaxConnection");
        return _database;

    }

    public static string NonQueryCommand(string uspName, Object[] uspParams)
    {
        try
        {
            CreateDB();
            return Convert.ToString(_database.ExecuteScalar(uspName, uspParams));
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
     public static DataTable  NonQueryCommandParamTable(string uspName, SqlParameter[] param)
    {
        try
        {
            CreateDB();
            using (var dbCommand = _database.GetStoredProcCommand(uspName))
            {
                dbCommand.Parameters.AddRange(param);
                
                return _database.ExecuteDataSet(dbCommand).Tables[0];

            }            
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    public static string  NonQueryCommandParam(string uspName, SqlParameter[] param)
    {
        try
        {
            CreateDB();
            using (var dbCommand = _database.GetStoredProcCommand(uspName))
            {
                dbCommand.Parameters.AddRange(param);
                return Convert.ToString(_database.ExecuteScalar(dbCommand));

            }            
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    public static DataTable DataTableCommand(string uspName, Object[] uspParams, ref string errorMsg)
    {
        try
        {
            CreateDB();
            DataTable dt = _database.ExecuteDataSet(uspName, uspParams).Tables[0];
            if (dt != null)
            {
                //if (dt.Rows.Count > 0)
                //{
                //    return dt;
                //}
                //else
                //{
                //    errorMsg = "Something Went Wrong";
                //    return null;
                //}
                return dt;
            }
            else
            {
                errorMsg = "Something Went Wrong";
                return null;
            }
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
            return null;
        }
    }
    public static DataSet DataSetCommand(string uspName, SqlParameter[] param, ref string errorMsg)
    {
        try
        {
            CreateDB();
            DataSet ds = new DataSet();
            using (var dbCommand = _database.GetStoredProcCommand(uspName))
            {
                dbCommand.Parameters.AddRange(param);
                ds = _database.ExecuteDataSet(dbCommand);
                dbCommand.CommandTimeout = 0;
            }
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    return ds;
                }
                else
                {
                    errorMsg = "Something Went Wrong";
                    return null;
                }
            }
            else
            {
                errorMsg = "Something Went Wrong";
                return null;
            }
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
            return null;
        }
    }

    public static DataSet DataSetCommandNoTimeOut(string uspName, SqlParameter[] param, ref string errorMsg)
    {
        try
        {
            CreateDB();
            DataSet ds = new DataSet();
            using (var dbCommand = _database.GetStoredProcCommand(uspName))
            {
                dbCommand.CommandTimeout = 0;
                dbCommand.Parameters.AddRange(param);
                ds = _database.ExecuteDataSet(dbCommand);
                
            }
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    return ds;
                }
                else
                {
                    errorMsg = "Something Went Wrong";
                    return null;
                }
            }
            else
            {
                errorMsg = "Something Went Wrong";
                return null;
            }
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
            return null;
        }
    }

    public static DataTable DataTableCommand2(string uspName, Object[] uspParams, ref string errorMsg)
    {
        try
        {
            CreateDB();
            DataTable dt = _database.ExecuteDataSet(uspName, uspParams).Tables[0];
            if (dt != null)
            {
                return dt;
            }
            else
            {
                errorMsg = "Something Went Wrong";
                return null;
            }
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
            return null;
        }
    }


    public static DataTable ItaxDataTableCommand(string uspName, Object[] uspParams, ref string errorMsg)
    {
        try
        {
            CreateDB();
            DataTable dt = _databaseItax.ExecuteDataSet(uspName, uspParams).Tables[0];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    errorMsg = "Something Went Wrong";
                    return null;
                }
            }
            else
            {
                errorMsg = "Something Went Wrong";
                return null;
            }
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
            return null;
        }
    }

    public static DataSet DataSetNoTimeOut(string uspName, Object[] uspParams, ref string errorMsg)
    {
        try
        {
            CreateDB();

            DataSet ds = _database.ExecuteDataSet(uspName, uspParams);
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    return ds;
                }
                else
                {
                    errorMsg = "Something Went Wrong";
                    return null;
                }
            }
            else
            {
                errorMsg = "Something Went Wrong";
                return null;
            }
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
            return null;
        }
    }

    public static DataSet DataSetCommand(string uspName, Object[] uspParams, ref string errorMsg)
    {
        try
        {
            CreateDB();
            DataSet ds = _database.ExecuteDataSet(uspName, uspParams);
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    return ds;
                }
                else
                {
                    errorMsg = "Something Went Wrong";
                    return null;
                }
            }
            else
            {
                errorMsg = "Something Went Wrong";
                return null;
            }
        }
         catch (Exception ex)
        {
            errorMsg = ex.Message;
            return null;
        }
    }
}