﻿function getUserDetails() {
    var varEmail = $('#txtUserEmail').val();
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/GetUserDetailsByEmail",
        data: '{userEmail: ' + JSON.stringify(varEmail) +'}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result != 'User Exist') {
                $('#divAlertMsg').css('display', 'block');
                $('#pAlertMsg').html(response.d + '');
            } else {
                window.location.href = "/reset-options";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


function resetViaEmail() {
    checkSessions();
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/ResetViaEmail",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#divAlertMsg').css('display', 'block');
            $('#pAlertMsg').html(response.d + '');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/forgot-password";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}
