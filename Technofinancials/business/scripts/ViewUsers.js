﻿//Page Load events
$(function () {
    //check sessions
    checkSessions();
    getAllUsers();
});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


//get all departments
function getAllUsers() {
    var url = "/" + $('#hdnCompanyName').val() + "-" + $('#hdnCompanyID').val() + "/administrator/manage/edit-user?userID=";
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetAllUsers",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#tb tbody').append('<tr><td style="padding-top:10px">' + response[index].UserName + '</td> <td style="padding-top:10px">' + response[index].Email + '</td><td style="padding-top:10px">' + response[index].EmpCode + ' </td><td style="padding-top:10px">' + response[index].DeptName + ' </td><td style="padding-top:10px">' + response[index].DesgTitle + ' </td><td><a href=' + url + response[index].UserID + ' class="tf-note-btn" ><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;<a onclick="deleteUsers(' + response[index].UserID + ');" class="tf-disapproved-btn" ><i class="far fa-trash"></i></a></td></tr>');
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}
function deleteUsers(userID) {
    $.ajax({
        type: "POST",
        url: "/business/services/AdminPanel.asmx/DeleteUserByID",
        data: '{UserID: ' + JSON.stringify(userID) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#tb tbody').empty();
            getAllDepartments();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error');
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}
