﻿//Page Load events
$(function () {
    //check sessions
    checkSessions();
    getAllDepartments();
});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


//get all departments
function getAllDepartments() {
    var url = "/" + $('#hdnCompanyName').val() + "/directors/manage/departments/edit-department?deptID=";
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetAllDepartments",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#tb tbody').append('<tr><td style="padding-top:10px">' + response[index].DeptName + '</td> <td style="padding-top:10px">' + response[index].DeptDesc + '</td><td style="padding-top:10px">' + response[index].Status + ' </td><td><a href=' + url + response[index].DeptID + ' class="tf-note-btn" ><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;<a onclick="deleteDepartment(' + response[index].DeptID + ');" class="tf-disapproved-btn" ><i class="far fa-trash"></i></a></td></tr>');
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}
    function deleteDepartment(DeptID) {
        $.ajax({
            type: "POST",
            url: "/business/services/AdminPanel.asmx/DeleteDepartmentByID",
            data: '{DeptID: ' + JSON.stringify(DeptID) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#tb tbody').empty();
                getAllDepartments();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error');
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }

        });
}
