﻿//Page Load events
$(function () {
    //check sessions
    checkSessions();


    var url = window.location.pathname;
    var arrURL = url.split('/');
    var pathname = arrURL[arrURL.length - 1];

    if (pathname != 'add-new-department') {
        //getDepartmentByID();
        $.when(bindCompaniesDD()).done(function () {
            $.when(bindDepartmentDD()).done(getDepartmentByID());
        });
    }
    else {
        bindCompaniesDD();
        bindDepartmentDD();
    }


});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//Companies dropDown
function bindCompaniesDD() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetCompaniesDD",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#ddlCompany').append($("<option></option>").attr("value", response[index].CompanyID).text(response[index].CompanyName));
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}
//Department dropDown
function bindDepartmentDD() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetDepartmentDD",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#ddlDept').append($("<option></option>").attr("value", response[index].DeptID).text(response[index].DeptName));
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//check sessions
function AddDepartment() {
    var url = window.location.pathname;
    var arrURL = url.split('/');
    var pathname = arrURL[arrURL.length - 1];
   

    if (pathname == 'add-new-department') {
        var companyID = $('#ddlCompany').val();
        var DepartmentName = $('#txtName').val();
        var DeptParentID = $('#ddlDept').find(":selected").val();
        var Desc = $('#comment').val();
        
        $.ajax({
            method: "POST",
            url: "/business/services/AdminPanel.asmx/AddDepartment", //change method name here
            data: '{CompanyID: ' + JSON.stringify(companyID) + ',DeptName: ' + JSON.stringify(DepartmentName) + ',DeptDesc: ' + JSON.stringify(Desc) + ',DeptParentID: ' + JSON.stringify(DeptParentID) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d + '';
                if (result == 'New Department Added') {
                    $('#divAlertMsg').css('display', 'block');
                    $('#divAlertMsg div').attr('class', 'alert tf-alert-success');
                    $('#pAlertMsg').html(result + '');

                    $('#ddlCompany').val();
                    $('#txtName').val();
                    $('#ddlDept').find(":selected").val('0');
                    $('#comment').val();

                } else {
                    $('#divAlertMsg div').attr('class', 'alert tf-alert-danger');
                    $('#divAlertMsg').css('display', 'block');
                    $('#pAlertMsg').html(result + '');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    } else {
        var DeptID = getParameterByName("deptID");
        var companyID = $('#ddlCompany').val();
        var DepartmentName = $('#txtName').val();
        var DeptParentID = $('#ddlDept').val();
        var Desc = $('#comment').val();
        $.ajax({
            method: "POST",
            url: "/business/services/AdminPanel.asmx/UpdateDepartment", //change method name here
            data: '{DeptID: ' + JSON.stringify(DeptID) + ',CompanyID: ' + JSON.stringify(companyID) + ',DeptName: ' + JSON.stringify(DepartmentName) + ',DeptDesc: ' + JSON.stringify(Desc) + ',DeptParentID: ' + JSON.stringify(DeptParentID) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d + '';
                if (result == 'Department Updated') {
                    $('#divAlertMsg').css('display', 'block');
                    $('#divAlertMsg div').attr('class', 'alert tf-alert-success');
                    $('#pAlertMsg').html(result + '');
                    

                } else {
                    $('#divAlertMsg div').attr('class', 'alert tf-alert-danger');
                    $('#divAlertMsg').css('display', 'block');
                    $('#pAlertMsg').html(result + '');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    }


}

function getDepartmentByID() {
    var DeptID = getParameterByName("deptID");
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetDepartmentByID",
        data: { DeptID: DeptID + "" },
        dataType: "json",
        success: function (response) {
            var deptID = response[0].DeptParentID + '';
            var companyID = response[0].CompanyID + '';
            $('#ddlDept').val(deptID+'');
            $('#txtName').val(response[0].DeptName);
            $('#comment').val(response[0].DeptDesc);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

function getIndexOfOption(ddlID, value) {
    $('#' + ddlID + ' option').each(function () {
        //console.log($(this).val());
        var currOptionValue = ($(this).val())+'';
        //console.log(currOptionValue);
        //console.log(value);
        if (currOptionValue == value) {
            return $(this).index();
        }
    });
}

//get url parameters
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}