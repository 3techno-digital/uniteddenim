﻿//Page Load events
$(function () {
    //check sessions
    checkSessions();
    bindDepartmentDD();

    var url = window.location.pathname;
    var arrURL = url.split('/');
    var pathname = arrURL[arrURL.length - 1];
    if (pathname != 'add-new-designation') {

    }
});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//Department dropDown
function bindDepartmentDD() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetDepartmentDD",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#ddlDept').append($("<option></option>").attr("value", response[index].DeptID).text(response[index].DeptName));
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//check sessions
function AddDesignation() {
    var url = window.location.pathname;
    var arrURL = url.split('/');
    var pathname = arrURL[arrURL.length - 1];
    if (pathname == 'add-new-designation') {
        var DeptID = $('#ddlDept').find(":selected").val();
        var Designation = $('#txtName').val();

        $.ajax({
            method: "POST",
            url: "/business/services/AdminPanel.asmx/AddDesignation", //change method name here
            data: '{DeptID: ' + JSON.stringify(DeptID) + ',DesgTitle: ' + JSON.stringify(Designation) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d + '';
                if (result == 'New Designation Added') {
                    $('#divAlertMsg').css('display', 'block');
                    $('#divAlertMsg div').attr('class', 'alert tf-alert-success');
                    $('#pAlertMsg').html(result + '');

                    $('#ddlDept').find(":selected").val();
                    $('#txtName').val();
                } else {
                    $('#divAlertMsg div').attr('class', 'alert tf-alert-danger');
                    $('#divAlertMsg').css('display', 'block');
                    $('#pAlertMsg').html(result + '');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    } else {
        var DesgID = getParameterByName("designationID");
        var DeptID = $('#ddlDept').val();
        var Designation = $('#txtName').val();
        $.ajax({
            method: "POST",
            url: "/business/services/AdminPanel.asmx/UpdateDesignation", //change method name here
            data: '{DesgID: ' + JSON.stringify(DesgID) + ',DeptID: ' + JSON.stringify(DeptID) + ',DesgTitle: ' + JSON.parse(Designation) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#divAlertMsg').css('display', 'block');
                $('#pAlertMsg').html(response.d + '');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    }


}

function getDesignationByID() {
    var desgID = getParameterByName("desgID");
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetDesignationByID",
        dataType: "json",
        success: function (response) {
            $('#ddlDept').val(respone.DeptID);
            $('#txtName').val(respone.Designation);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


//get url parameters
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}