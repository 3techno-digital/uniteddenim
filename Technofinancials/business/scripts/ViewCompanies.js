﻿//Page Load events
$(function () {
    //check sessions
    checkSessions();
    getAllCompanies();
});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


//get all companies
function getAllCompanies() {
    var url = "/" + $('#hdnCompanyName').val() + "-" + $('#hdnCompanyID').val() + "/administrator/manage/edit-company?companyID=";
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetChildCompanies",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#tb tbody').append('<tr><td style="padding-top:10px">' + response[index].CompanyName + '</td> <td><a href=' + url + response[index].CompanyID + ' class="tf-note-btn" ><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;<a onclick="deleteCompany(' + response[index].CompanyID + ');" class="tf-disapproved-btn" ><i class="far fa-trash"></i></a></td></tr>');
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}
function deleteCompany(companyID) {
    $.ajax({
        type: "POST",
        url: "/business/services/AdminPanel.asmx/DeleteCompanyByID",
        data: '{CompanyID: ' + JSON.stringify(companyID) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#tb tbody').empty();
            getAllCompanies();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error');
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }

    });
}
