﻿//Page Load events
$(function () {
    //check sessions
    checkSessions();
    getAllGrades();
});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


//get all grades
function getAllGrades() {
    var url = "/" + $('#hdnCompanyName').val() +"/directors/manage/grades/edit-grade?gradeID=";
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetAllGrades",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#tb tbody').append('<tr><td style="padding-top:10px">' + response[index].GradeName + '</td> <td style="padding-top:10px">' + response[index].Status + ' </td><td><a href=' + url + response[index].GradeID + ' class="tf-note-btn" ><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;<a onclick="deleteGrade(' + response[index].GradeID + ');" class="tf-disapproved-btn" ><i class="far fa-trash"></i></a></td></tr>');
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


function deleteGrade(gradeID) {
    $.ajax({
        type: "POST",
        url: "/business/services/AdminPanel.asmx/DeleteGradeByID",
        data: '{gradeID: ' + JSON.stringify(gradeID) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#tb tbody').empty();
            getAllGrades();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error');
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }

    });
}