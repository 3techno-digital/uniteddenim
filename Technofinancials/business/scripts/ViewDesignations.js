﻿//Page Load events
$(function () {
    checkSessions();
    getAllDesignations();
});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


//get all grades
function getAllDesignations() {
    var url = "/" + $('#hdnCompanyName').val() + "/directors/manage/designations/edit-designation?designationID=";
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetAllDesignationsByCompanyID",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#tb tbody').append('<tr><td style="padding-top:10px">' + response[index].DesgTitle + '</td> <td style="padding-top:10px">' + response[index].DeptName + '</td><td style="padding-top:10px">' + response[index].Status + ' </td><td><a href=' + url + response[index].DesgID + ' class="tf-note-btn" ><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;<a onclick="deleteDesigntion(' + response[index].DesgID + ');" class="tf-disapproved-btn" ><i class="far fa-trash"></i></a></td></tr>');
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


function deleteDesigntion(DesgID) {
    $.ajax({
        type: "POST",
        url: "/business/services/AdminPanel.asmx/DeleteDesgnationByID",
        data: '{DesgID: ' + JSON.stringify(DesgID) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#tb tbody').empty();
            getAllDesignations();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error');
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }

    });
}

//get all grades
function GetAllDesignationsByCompanyID() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetAllDesignationsByCompanyID",
        dataType: "json",
        success: function (response) {
            var result = JSON.parse(response.d);
            $("#ddlDesignation").empty();
            $("#ddlDesignation").append($("<option     />").val("0").text("ALL"));

            if (result.length > 0) {
                $.each(result, function () {
                    $("#ddlDesignation").append($("<option     />").val(this.DesgID).text(this.DesgTitle));
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:' + jqXHR);
            console.log('textStatus:' + textStatus);
            console.log('errorThrown:' + errorThrown);
        }
    });
}

function GetDesignationsByDepartmentIdAndCompanyId() {
    var departmentId = $('#ddlDepartment').val();
    $.ajax({
        type: "POST",
        url: '/business/services/AdminPanel.asmx/GetAllDesignationsByDepartmentId',
        data: '{departmentId: ' + JSON.stringify(departmentId) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = JSON.parse(response.d);
            $("#ddlDesignation").empty();
            $("#ddlDesignation").append($("<option     />").val("0").text("ALL"));

            if (result.length > 0) {
                $.each(result, function () {
                    $("#ddlDesignation").append($("<option     />").val(this.DesgID).text(this.DesgTitle));
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}


function GetEmployeesByDesignationIdAndDepartmentId() {
    var departmentId = $('#ddlDepartment').val();
    var designationId = $('#ddlDesignation').val();
    $.ajax({
        type: "POST",
        url: '/business/services/AdminPanel.asmx/GetAllEmployeesByDesignationIdAndDepartmentId',
        data: '{departmentId: ' + JSON.stringify(departmentId) + ', designationId: ' + designationId + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = JSON.parse(response.d);
            $("#ddlEmployee").empty();
            $("#ddlEmployee").append($("<option     />").val("0").text("ALL"));

            if (result.length > 0) {
                $.each(result, function () {
                    $("#ddlEmployee").append($("<option     />").val(this.EmployeeID).text(this.EmployeeName));
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}


