﻿//Page Load events
$(function () {
    //check sessions
    //checkSessions();

    //bind dropdowns
    bindFirstDropDownofQuestion();
    bindSecondDropDownofQuestion();
});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//security questions dropDown 1
function bindFirstDropDownofQuestion() {
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/GetSecurityQuestionDD1",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#ddlSQ1').append($("<option></option>").attr("value", response[index].QuesID).text(response[index].Ques));
            });
           
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//security questions dropDown 2
function bindSecondDropDownofQuestion() {
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/GetSecurityQuestionDD2",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#ddlSQ2').append($("<option></option>").attr("value", response[index].QuesID).text(response[index].Ques));
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


// add security question
function addSecurityQuestions() {
    var ques1ID = $('#ddlSQ1').find(":selected").val();
    var ques2ID = $('#ddlSQ2').find(":selected").val();
    var ans1 = $('#answer1').val();
    var ans2 = $('#answer2').val();

    var pathname = (window.location.pathname).replace('/', '');
    if (pathname == 'update-security-questions') {
        checkSessions();
        $.ajax({
            method: "POST",
            url: "/business/services/Login.asmx/AddUserSecurityAnswers",
            data: '{Ques1ID: ' + JSON.stringify(ques1ID) + ', Ans1: ' + JSON.stringify(ans1) + ', Ques2ID: ' + JSON.stringify(ques2ID) + ', Ans2: ' + JSON.stringify(ans2) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d + '';
                if (result == 'Security Answers Added') {
                    window.location.href = "/update-password";
                }
                else {
                    $('#divAlertMsg').css('display', 'block');
                    $('#pAlertMsg').html(response + '');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    }
    else {
        $.ajax({
            method: "POST",
            url: "/business/services/Login.asmx/VerifySecurityAnswers",
            data: '{Ques1: ' + JSON.stringify(ques1ID) + ', Ans1: ' + JSON.stringify(ans1) + ', Ques2: ' + JSON.stringify(ques2ID) + ', Ans2: ' + JSON.stringify(ans2) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d + '';
                if (result == 'Valid Answsers') {
                    window.location.href = "/update-password";
                }
                else {
                    $('#divAlertMsg').css('display', 'block');
                    $('#pAlertMsg').html(result + '');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    }
}


//get url values
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
