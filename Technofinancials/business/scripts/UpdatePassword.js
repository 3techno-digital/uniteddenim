﻿var globalEmail = '';

$(function () {
    var pathname = (window.location.pathname).replace('/', '');
    if (pathname == 'update-password') {
       checkSessions();
    }
    else if (pathname == 'reset-password') {
        $('#divForm').hide();
        var email = getParameterByName("user-email");
        globalEmail = getParameterByName("user-email");
        var access_token = getParameterByName("access-token");
        $.ajax({
            method: "POST",
            url: "/business/services/Login.asmx/GetPasswordKeyByEmail",
            data: '{userEmail: ' + JSON.stringify(email) + ', passwordKey: ' + JSON.stringify(access_token) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d + '';
                if (result == 'Valid Access Token') {
                    $('#divForm').show();
                } else {
                    $('#divAlertMsg').css('display', 'block');
                    $('#pAlertMsg').html(result + '');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    }
});



//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


//update password
function updatePassword() {
    //check sessions
    //checkSessions();

    var pass = $('#txtPassword').val();

    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/UpdatePassword",
        data: '{Password: ' + JSON.stringify(pass) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'Password Updated') {
                authenticateUser(globalEmail, pass);
            }
            else {
                $('#divAlertMsg').css('display', 'block');
                $('#pAlertMsg').html(response + '');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


function authenticateUser(varEmail, varPassword) {
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/AuthenticateUser",
        data: '{userEmail: ' + JSON.stringify(varEmail) + ', userPassword: ' + JSON.stringify(varPassword) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result != 'Logged In Successfully' && result != 'New User') {
                $('#divAlertMsg').css('display', 'block');
                $('#pAlertMsg').html(response.d + '');
            } else {
                // new user
                if ((response.d + '') == 'New User') {
                    window.location.href = "/update-security-questions";
                } else {
                    var lastURL = getUrlVars()["url"];
                    if (getUrlVars()["url"] != null) {
                        window.location.href = lastURL; //check access level on page
                    } else {
                        window.location.href = "/redirect-url"; // check access level
                    }
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//get url values
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}