﻿//Page Load events
$(function () {
    //Load Cookies
    checkCookies();

    //check it user is already login
    checkSessions();
});


//Login Button Click
$("#btnLogin").click(function () {

    //check if rememeber me is clicked
    if ($('#chkRememberMe').prop('checked') == true) {
        saveCookies();
    }

    //login api
    var varEmail = $('#txtUserEmail').val();
    var varPassword = $('#txtUserPassword').val();
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/AuthenticateUser",
        data: '{userEmail: ' + JSON.stringify(varEmail) + ', userPassword: ' + JSON.stringify(varPassword) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result != 'Logged In Successfully' && result != 'New User') {
                $('#divAlertMsg').css('display', 'block');
                $('#pAlertMsg').html(response.d + '');
            } else {
                // new user
                if ((response.d + '') == 'New User') {
                    window.location.href = "/update-security-questions";
                } else {
                    checkAccessLevel();
                    //var lastURL = getUrlVars()["url"];
                    //if (getUrlVars()["url"] != null) {
                    //    window.location.href = lastURL; //check access level on page
                    //} else {
                    //    window.location.href = "/temp"; // check access level
                    //}
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });

});


//save cookies
function saveCookies() {
    var userEmail = $('#txtUserEmail').val();
    var userPass = $('#txtUserPassword').val();
    document.cookie = "TFUserEmail=" + userEmail;
    document.cookie = "TFUserPass=" + userPass;
}

//check sessions
function checkAccessLevel() {
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/CheckAccessLevel",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            window.location.href = response.d + '';
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


//check cookies
function checkCookies() {
    var userEmail = getCookie("TFUserEmail");
    if (userEmail == null) { userEmail = ""; }

    var userPass = getCookie("TFUserPass");
    if (userPass == null) { userPass = ""; }

    $('#txtUserEmail').val(userEmail);
    $('#txtUserPassword').val(userPass);
    $('#chkRememberMe').prop('checked', true);
}


//get cookies
function getCookie(name) {
    var nameEQ = name + "=";
    //alert(document.cookie);
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(nameEQ) != -1) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'true') {
                if ((response.d + '') == 'New User') {
                    window.location.href = "/update-security-questions";
                } else {
                    var lastURL = getUrlVars()["url"];
                    if (getUrlVars()["url"] != null) {
                        window.location.href = lastURL; // check access level on page
                    } else {
                        window.location.href = "/redirect-url"; // check access level
                    }
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


//get url values
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


