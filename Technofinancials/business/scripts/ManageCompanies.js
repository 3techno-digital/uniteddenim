﻿//Page Load events
$(function () {
    //check sessions
    checkSessions();

    //$("#form1").validate({
    //    rules: {
    //        companyName: "required",
    //        companyShortName: "required",
    //        parentCompanyId: "required",
    //        companyDescription: "required",
    //        compnayNtn: "required",
    //        companyStrn: "required",
    //        companySecp: "required",
    //        companyArn: "required",
    //        companyStn: "required",
    //        ceo: "required",
    //        country: "required",
    //        city: "required",
    //        province: "required",
    //        addressLine1: "required",
    //    }
    //});

    //jQuery.extend(jQuery.validator.messages, {
    //    required: "* Required",
    //    select: "* Required",
    //});

    //$("#button").click(function () {
    //    $("#form1").valid();
    //});

    //$(".month-name-table").click(function () {
    //    $(".month-table-show").toggle();
    //});
});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}