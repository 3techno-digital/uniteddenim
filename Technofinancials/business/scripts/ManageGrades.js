﻿//Page Load events
$(function () {
    //check sessions
    checkSessions();
    bindCompaniesDD();
});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//Companies dropDown
function bindCompaniesDD() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetCompaniesDD",
        dataType: "json",
        success: function (response) {
            jQuery.each(response, function (index, item) {
                $('#ddlCompany').append($("<option></option>").attr("value", response[index].CompanyID).text(response[index].CompanyName));
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//check sessions
function AddGrades() {
    var url = window.location.pathname;
    var arrURL = url.split('/');
    var pathname = arrURL[arrURL.length - 1];
    if (pathname == 'add-new-grade') {
        var companyID = $('#ddlCompany').find(":selected").val();
        var gradeName = $('#txtName').val();

        $.ajax({
            method: "POST",
            url: "/business/services/AdminPanel.asmx/AddGrade", //change method name here
            data: '{CompanyID: ' + JSON.stringify(companyID) + ',GradeName: ' + JSON.stringify(gradeName) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d + '';
                if (result == 'New Grade Added') {
                    $('#divAlertMsg').css('display', 'block');
                    $('#divAlertMsg div').attr('class', 'alert tf-alert-success');
                    $('#pAlertMsg').html(result + '');

                    $('#ddlDept').find(":selected").val();
                    $('#txtName').val();
                } else {
                    $('#divAlertMsg div').attr('class', 'alert tf-alert-danger');
                    $('#divAlertMsg').css('display', 'block');
                    $('#pAlertMsg').html(result + '');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    } else {
        var gradeID = getParameterByName("gradeID");
        var companyID = $('#ddlCompany').val();
        var gradeName = $('#txtName').val();
        $.ajax({
            method: "POST",
            url: "/business/services/AdminPanel.asmx/UpdateGrade", //change method name here
            data: '{GradeID: ' + JSON.stringify(gradeID) + ',CompanyID: ' + JSON.stringify(companyID) + ',GradeName: ' + JSON.stringify(gradeName) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('#divAlertMsg').css('display', 'block');
                $('#pAlertMsg').html(response.d + '');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    }

    
}

function getGradeByID() {
    $.ajax({
        method: "POST",
        url: "/business/services/AdminPanel.asmx/GetGradeByID",
        dataType: "json",
        success: function (response) {
            $('#ddlCompany').val(respone.companyID);
            $('#txtName').val(respone.GradeName);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}


//get url parameters
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}