﻿//Page Load events
$(function () {
    //check sessions
    checkSessions();
});

//check sessions
function checkSessions() {
    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/CheckSessions",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d + '';
            if (result == 'false') {
                window.location.href = "/login";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}

//check sessions
function updateUserPassword() {
    var oldPassword = $('#txtOldPassword').val();
    var newPassword = $('#txtNewPassword').val();

    $.ajax({
        method: "POST",
        url: "/business/services/Login.asmx/UpdateUserPassword",
        data: '{oldPassword: ' + JSON.stringify(oldPassword) + ', newPassword: ' + JSON.stringify(newPassword) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response.d;
            
            $('#txtOldPassword').val();
            $('#txtNewPassword').val();
            $('#txtConfirmPassword').val();

            if (result == 'Password Updated') {
                $('#divAlertMsg').css('display', 'block');
                $('#divAlertMsg div').attr('class', 'alert tf-alert-success');
                $('#pAlertMsg').html(result + '');
            } else {
                $('#divAlertMsg div').attr('class', 'alert tf-alert-danger');
                $('#divAlertMsg').css('display', 'block');
                $('#pAlertMsg').html(result + '');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        }
    });
}