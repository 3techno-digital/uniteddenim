﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;                                                                                                                
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace Technofinancials.business.services
{
    /// <summary>
    /// Summary description for AdminPanel
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AdminPanel : System.Web.Services.WebService
    {
        DBQueries objDB = new DBQueries();
        private const string apiKey = "3Techno*0519!";
        private const string encKey = "TF2o!eight";
        string errorMsg = "";


        private string CheckPassKeys()
        {
            string returnMsg = "";
            string ConfigAPIKey = ConfigurationManager.AppSettings["APIKey"].ToString();
            string ConfigEncKey = ConfigurationManager.AppSettings["EncKey"].ToString();

            if (!string.IsNullOrEmpty(ConfigEncKey))
            {
                if (ConfigEncKey == encKey)
                {
                    if (!string.IsNullOrEmpty(ConfigAPIKey))
                    {
                        if (ConfigAPIKey == apiKey)
                        {
                            returnMsg = "Passkeys Authenticated";
                        }
                        else
                        {
                            returnMsg = "Invalid API Key";
                        }
                    }
                    else
                    {
                        returnMsg = "API Key Not Defined";
                    }
                }
                else
                {
                    returnMsg = "Invalid Encryption Key";
                }
            }
            else
            {
                returnMsg = "Encryption Key Not Defined";
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string CheckSessions()
        {
            string returnMsg = "false";
            if (Session["UserID"] != null && Session["EmployeeID"] != null)
                returnMsg = "true";

            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public void GetAllGrades()
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                var lstGrades = new List<AdminVaraiables>();
                DataTable dt = objDB.GetAllGradesByCompanyID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemTest = new AdminVaraiables
                            {
                                GradeID = dt.Rows[i]["GradeID"].ToString(),
                                GradeName = dt.Rows[i]["GradeName"].ToString(),
                                Status = dt.Rows[i]["Status"].ToString()
                            };
                            lstGrades.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstGrades));
            }
        }

        [WebMethod(EnableSession = true)]
        public void GetGradeByID(string GradID)
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.GradeID = Convert.ToInt32(GradID);
                var lstGrades = new List<AdminVaraiables>();
                DataTable dt = objDB.GetGradeByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemGrade = new AdminVaraiables
                            {
                                GradeID = dt.Rows[i]["GradeID"].ToString(),
                                CompanyID = dt.Rows[i]["CompanyID"].ToString(),
                                GradeName = dt.Rows[i]["GradeName"].ToString(),
                                Status = dt.Rows[i]["Status"].ToString()
                            };
                            lstGrades.Add(itemGrade);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstGrades));
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddGrade(string CompanyID, string GradeName)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.CompanyID = Convert.ToInt32(CompanyID);
                objDB.GradeName = GradeName.ToString();
                objDB.CreatedBy = Session["UserName"].ToString();
                returnMsg = objDB.AddGrade();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateGrade(string GradeID, string CompanyID, string GradeName)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.GradeID = int.Parse(GradeID);
                objDB.CompanyID = Convert.ToInt32(CompanyID);
                objDB.GradeName = GradeName.ToString();
                objDB.ModifiedBy = Session["UserName"].ToString();
                returnMsg = objDB.UpdateGrade();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public void GetAllDepartments()
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                var lstDepartments = new List<AdminVaraiables>();
                DataTable dt = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemTest = new AdminVaraiables
                            {
                                DeptID = dt.Rows[i]["DeptID"].ToString(),
                                DeptName = dt.Rows[i]["DeptName"].ToString(),
                                DeptDesc = dt.Rows[i]["DeptDesc"].ToString(),
                                DeptParentID = dt.Rows[i]["DeptParentID"].ToString(),
                                Status = dt.Rows[i]["Status"].ToString()
                            };
                            lstDepartments.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstDepartments));
            }
        }

        [WebMethod(EnableSession = true)]
        public void GetDepartmentByID(string DeptID)
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.DeptID = Convert.ToInt32(DeptID);
                var lstDepartments = new List<AdminVaraiables>();
                DataTable dt = objDB.GetDepartmentByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemDept = new AdminVaraiables
                            {
                                DeptID = dt.Rows[i]["DeptID"].ToString(),
                                CompanyID = dt.Rows[i]["CompanyID"].ToString(),
                                DeptName = dt.Rows[i]["DeptName"].ToString(),
                                DeptDesc = dt.Rows[i]["DeptDesc"].ToString(),
                                DeptParentID = dt.Rows[i]["DeptParentID"].ToString(),
                                Status = dt.Rows[i]["Status"].ToString()
                            };
                            lstDepartments.Add(itemDept);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstDepartments));
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddDepartment(string CompanyID, string DeptName, string DeptDesc, string DeptParentID)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.CompanyID = Convert.ToInt32(CompanyID);
                objDB.DeptName = DeptName.ToString();
                objDB.DeptDesc = DeptDesc.ToString();
                objDB.DeptParentID = int.Parse(DeptParentID.ToString());
                objDB.CreatedBy = Session["UserName"].ToString();
                returnMsg = objDB.AddDepartment();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateDepartment(string DeptID, string CompanyID, string DeptName, string DeptDesc, string DeptParentID)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.DeptID = int.Parse(DeptID);
                objDB.CompanyID = Convert.ToInt32(CompanyID);
                objDB.DeptName = DeptName.ToString();
                objDB.DeptDesc = DeptDesc.ToString();
                objDB.DeptParentID = int.Parse(DeptParentID.ToString());
                objDB.ModifiedBy = Session["UserName"].ToString();
                returnMsg = objDB.UpdateDepartment();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public void GetAllDesignations()
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.DeptID = Convert.ToInt32(Session["DeptID"]);
                var lstDesignations = new List<AdminVaraiables>();
                DataTable dt = objDB.GetAllDesignationByDepartmentID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemTest = new AdminVaraiables
                            {
                                DesgID = dt.Rows[i]["DesgID"].ToString(),
                                DeptID = dt.Rows[i]["DeptID"].ToString(),
                                DesgTitle = dt.Rows[i]["DesgTitle"].ToString(),
                                DeptName = dt.Rows[i]["DeptName"].ToString(),
                                Status = dt.Rows[i]["Status"].ToString()
                            };
                            lstDesignations.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstDesignations));
            }
        }

        [WebMethod(EnableSession = true)]
        public void GetAllDesignationsByCompanyID()
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                var lstDesignations = new List<AdminVaraiables>();
                DataTable dt = objDB.GetAllDesignationByCompanyID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemTest = new AdminVaraiables
                            {
                                DesgID = dt.Rows[i]["DesgID"].ToString(),
                                DeptID = dt.Rows[i]["DeptID"].ToString(),
                                DesgTitle = dt.Rows[i]["DesgTitle"].ToString(),
                                DeptName = dt.Rows[i]["DeptName"].ToString(),
                                Status = dt.Rows[i]["Status"].ToString()
                            };
                            lstDesignations.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstDesignations));
            }
        }

        [WebMethod(EnableSession = true)]
        public void GetDesignationByID(string DesgID)
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.DesgID = Convert.ToInt32(DesgID);
                var lstDesignations = new List<AdminVaraiables>();
                DataTable dt = objDB.GetDesignationByID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemTest = new AdminVaraiables
                            {
                                DesgID = dt.Rows[i]["DesgID"].ToString(),
                                DeptID = dt.Rows[i]["DeptID"].ToString(),
                                DesgTitle = dt.Rows[i]["DesgTitle"].ToString(),
                                Status = dt.Rows[i]["Status"].ToString()
                            };
                            lstDesignations.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstDesignations));
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddDesignation(string DeptID, string DesgTitle)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.DeptID = Convert.ToInt32(DeptID);
                objDB.DesgTitle = DesgTitle.ToString();
                objDB.CreatedBy = Session["UserName"].ToString();
                returnMsg = objDB.AddDesignation();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateDesignation(string DesgID, string DeptID, string DesgTitle)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.DesgID = int.Parse(DesgID);
                objDB.DeptID = Convert.ToInt32(DeptID);
                objDB.DesgTitle = DesgTitle.ToString();
                objDB.ModifiedBy = Session["UserName"].ToString();
                returnMsg = objDB.UpdateDesignation();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public void GetAllUsers()
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                var lstUsers = new List<AdminVaraiables>();
                DataTable dt = objDB.GetAllUsersByCompanyID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemTest = new AdminVaraiables
                            {
                                UserName = dt.Rows[i]["UserName"].ToString(),
                                Email = dt.Rows[i]["Email"].ToString(),
                                EmpCode = dt.Rows[i]["EmployeeCode"].ToString(),
                                DeptName = dt.Rows[i]["DeptName"].ToString(),
                                DesgTitle = dt.Rows[i]["DesgTitle"].ToString(),
                                GradeName = dt.Rows[i]["GradeName"].ToString()
                            };
                            lstUsers.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstUsers));
            }
        }

        [WebMethod(EnableSession = true)]
        public string DeleteGradeByID(string gradeID)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.GradeID = Convert.ToInt32(gradeID);
                objDB.DeletedBy = Session["UserName"].ToString();
                returnMsg = objDB.DeleteGrade();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }


        [WebMethod(EnableSession = true)]
        public string DeleteUserByID(string UserID)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.GradeID = Convert.ToInt32(UserID);
                objDB.DeletedBy = Session["UserName"].ToString();
                returnMsg = objDB.DeleteUser();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }


        [WebMethod(EnableSession = true)]
        public string DeleteDepartmentByID(string DeptID)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.DeptID = Convert.ToInt32(DeptID);
                objDB.DeletedBy = Session["UserName"].ToString();
                returnMsg = objDB.DeleteDepartment();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteDesgnationByID(string DesgID)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.DesgID = Convert.ToInt32(DesgID);
                objDB.DeletedBy = Session["UserName"].ToString();
                returnMsg = objDB.DeleteDesignation();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public void GetCompaniesDD()
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {

                objDB.ParentCompanyID = int.Parse(Session["CompanyID"].ToString());
                var lstComp = new List<AdminVaraiables>();
                DataTable dt = objDB.GetCompaniesByParentID(ref errorMsg);
                var itemTest = new AdminVaraiables
                {
                    CompanyID = Session["CompanyID"].ToString(),
                    CompanyName = Session["CompanyName"].ToString()
                };
                lstComp.Add(itemTest);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            itemTest = new AdminVaraiables
                            {
                                CompanyID = dt.Rows[i]["CompanyID"].ToString(),
                                CompanyName = dt.Rows[i]["CompanyName"].ToString()
                            };
                            lstComp.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstComp));
            }
        }

        [WebMethod(EnableSession = true)]
        public void GetDepartmentDD()
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {

                objDB.CompanyID = int.Parse(Session["CompanyID"].ToString());
                var lstDept = new List<AdminVaraiables>();
                DataTable dt = objDB.GetAllDepartmentsByCompanyID(ref errorMsg);
                var itemTest = new AdminVaraiables
                {
                    DeptID = "0",
                    DeptName = "None"
                };
                lstDept.Add(itemTest);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            itemTest = new AdminVaraiables
                            {
                                DeptID = dt.Rows[i]["DeptID"].ToString(),
                                DeptName = dt.Rows[i]["DeptName"].ToString()
                            };
                            lstDept.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstDept));
            }
        }

        [WebMethod(EnableSession = true)]
        public string DeleteCompanyByID(string CompanyID)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.CompanyID = Convert.ToInt32(CompanyID);
                objDB.DeletedBy = Session["UserName"].ToString();
                returnMsg = objDB.DeleteCompany();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public void GetChildCompanies()
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {

                objDB.ParentCompanyID = int.Parse(Session["CompanyID"].ToString());
                var lstComp = new List<AdminVaraiables>();
                DataTable dt = objDB.GetCompaniesByParentID(ref errorMsg);
                var itemTest = new AdminVaraiables
                {
                    CompanyID = Session["CompanyID"].ToString(),
                    CompanyName = Session["CompanyName"].ToString(),
                };
                lstComp.Add(itemTest);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            itemTest = new AdminVaraiables
                            {
                                CompanyID = dt.Rows[i]["CompanyID"].ToString(),
                                CompanyName = dt.Rows[i]["CompanyName"].ToString()
                            };
                            lstComp.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstComp));
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetAllEmployeesByDesignationIdAndDepartmentId(string departmentId, string designationId)
        {
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                List<EmployeeDropdownVariables> lstEmployees = new List<EmployeeDropdownVariables>();
                DataTable dt = objDB.GetAllEmployeesByCompanyDepartmentAndDesignationID(ref errorMsg, departmentId, designationId);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            lstEmployees.Add(new EmployeeDropdownVariables()
                            {
                                EmployeeID = dt.Rows[i]["EmployeeID"].ToString(),
                                EmployeeName = dt.Rows[i]["EmployeeName"].ToString()
                            });
                        }

                        return Json.Encode(lstEmployees);
                    }
                }

                return "Data Failed";
            }

            return "Passkeys Authentication Failed";
        }

        [WebMethod(EnableSession = true)]
        public string GetAllDesignationsByDepartmentId(string departmentId)
        {
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.DeptID = Convert.ToInt32(departmentId);
                List<DesignationVariables> lstDesignations = new List<DesignationVariables>();
                DataTable dt = objDB.GetAllDesignationByDepartmentIDAndCompanyID(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            lstDesignations.Add(new DesignationVariables()
                            {
                                DesgID = dt.Rows[i]["DesgID"].ToString(),
                                DesgTitle = dt.Rows[i]["DesgTitle"].ToString(),
                                Status = dt.Rows[i]["Status"].ToString()
                            });
                        }

                        //var js = new JavaScriptSerializer();
                        //Context.Response.Write(js.Serialize(lstDesignations));
                    }
                    
                    return Json.Encode(lstDesignations);
                }

                return "Data Failed";
            }

            return "Passkeys Authentication Failed";
        }
    }
}
