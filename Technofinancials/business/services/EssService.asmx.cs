﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using Technofinancials.business.classes;

namespace Technofinancials.business.services
{
    /// <summary>
    /// Summary description for EssService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class EssService : System.Web.Services.WebService
    {
        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        [WebMethod(EnableSession = true)]
        public List<EssTimeIn> checkStatus()
        {
            string btnStatus = "";
            var lstGrades = new List<EssTimeIn>();

            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            objDB.date = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss tt");
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());

            DataTable dt = objDB.CheckAttendanceButtonStatusWithTimeNew(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        btnStatus = dt.Rows[0]["BtnStatus"].ToString();
                    }

                    if (btnStatus == "None")
                    {
                        if (dt.Rows[0]["BreakTime"].ToString() != "0")
                        {
                            int workingSeconds = int.Parse(dt.Rows[0]["TimeIN"].ToString());
                            int totalBreakSeconds = int.Parse(dt.Rows[0]["BreakTime"].ToString());
                            var itemTest = new EssTimeIn
                            {
                                BtnStatus = btnStatus,
                                TimeIN = dt.Rows[0]["TimeIN"].ToString(),
                                WorkHoursSec = (workingSeconds - totalBreakSeconds).ToString(),
                                BreakInSec = dt.Rows[0]["BreakTime"].ToString(),
                                StartTime = Convert.ToDateTime(dt.Rows[0]["StartTime"]).ToString("hh:mm tt"),
                                BreakStartEndTime = Convert.ToDateTime(dt.Rows[0]["BreakStartTime"]).ToString("hh:mm tt") + "-" + Convert.ToDateTime(dt.Rows[0]["BreakEndTime"]).ToString("hh:mm tt"),
                                EndTime = Convert.ToDateTime(dt.Rows[0]["EndTime"]).ToString("hh:mm tt"),
                                RemainingBreakTime = ((totalBreakSeconds < 0 || totalBreakSeconds > 3600 ? 0 : (3600 - totalBreakSeconds)) / 60).ToString(),

                            };
                            lstGrades.Add(itemTest);
                        }
                        else
                        {
                            int workingSeconds = int.Parse(dt.Rows[0]["TimeIN"].ToString());
                            int totalBreakSeconds = int.Parse(dt.Rows[0]["BreakTime"].ToString());
                            var itemTest = new EssTimeIn
                            {
                                BtnStatus = btnStatus,
                                TimeIN = dt.Rows[0]["TimeIN"].ToString(),
                                WorkHoursSec = (workingSeconds - totalBreakSeconds).ToString(),
                                RemainingBreakTime = ((totalBreakSeconds < 0 || totalBreakSeconds > 3600 ? 0 : (3600 - totalBreakSeconds)) / 60).ToString(),
                                BreakInSec = dt.Rows[0]["BreakTime"].ToString()
                            };
                            lstGrades.Add(itemTest);
                        }
                    }
                    else if (btnStatus == "TimeOut")
                    {
                        int workingSeconds = int.Parse(dt.Rows[0]["TimeIN"].ToString());
                        int totalBreakSeconds = int.Parse(dt.Rows[0]["BreakTime"].ToString());

                        var itemTest = new EssTimeIn
                        {
                            BtnStatus = btnStatus,
                            StartTime = Convert.ToDateTime(dt.Rows[0]["StartTime"]).ToString("hh:mm tt"),
                            TimeIN = dt.Rows[0]["TimeIN"].ToString(),
                            WorkHoursSec = (workingSeconds - totalBreakSeconds).ToString(),
                            BreakStartEndTime = Convert.ToDateTime(dt.Rows[0]["BreakStartTime"]).ToString("hh:mm tt") + "-" + Convert.ToDateTime(dt.Rows[0]["BreakEndTime"]).ToString("hh:mm tt"),
                            BreakInSec = dt.Rows[0]["BreakTime"].ToString(),
                            RemainingBreakTime = ((totalBreakSeconds < 0 || totalBreakSeconds > 3600 ? 0 : (3600 - totalBreakSeconds)) / 60).ToString(),
                        };

                        lstGrades.Add(itemTest);
                    }
                    else if (btnStatus == "BreakEndTime")
                    {
                        int workingSeconds = int.Parse(dt.Rows[0]["TimeIN"].ToString());
                        int totalBreakSeconds = int.Parse(dt.Rows[0]["BreakTime"].ToString());

                        var itemTest = new EssTimeIn
                        {
                            BtnStatus = btnStatus,
                            BreakStartTime = Convert.ToDateTime(dt.Rows[0]["BreakStart"]).ToString("hh:mm tt"),
                            StartTime = Convert.ToDateTime(dt.Rows[0]["StartTime"]).ToString("hh:mm tt"),
                            TimeIN = dt.Rows[0]["TimeIN"].ToString(),
                            WorkHoursSec = (workingSeconds - totalBreakSeconds).ToString(),
                            BreakInSec = dt.Rows[0]["BreakTime"].ToString(),
                            RemainingBreakTime = ((totalBreakSeconds < 0 || totalBreakSeconds > 3600 ? 0 : (3600 - totalBreakSeconds)) / 60).ToString(),
                        };

                        lstGrades.Add(itemTest);
                    }
                    else if (btnStatus == "BreakTime")
                    {

                        int wh = int.Parse(dt.Rows[0]["TimeIN"].ToString());
                        wh = (wh < 0) ? 0 : wh;
                        int totalBreakSeconds = int.Parse(dt.Rows[0]["BreakTime"].ToString());

                        var itemTest = new EssTimeIn
                        {
                            BtnStatus = btnStatus,
                            StartTime = Convert.ToDateTime(dt.Rows[0]["StartTime"]).ToString("hh:mm tt"),
                            TimeIN = dt.Rows[0]["TimeIN"].ToString(),
                            WorkHoursSec = (wh - totalBreakSeconds).ToString(),
                            RemainingBreakTime = ((totalBreakSeconds < 0 || totalBreakSeconds > 3600 ? 0 : (3600 - totalBreakSeconds)) / 60).ToString(),
                        };

                        lstGrades.Add(itemTest);
                    }
                    else
                    {
                        var itemTest = new EssTimeIn
                        {
                            BtnStatus = btnStatus,
                        };

                        lstGrades.Add(itemTest);
                    }
                }
            }

            return lstGrades;
        }
        [WebMethod(EnableSession = true)]
        public string InsertClearanceDeduction(string employeeid,string amount, string remarks,string sepID )
        {
            string res = "";
            objDB.FilePath = "";
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.EmployeeID = Convert.ToInt32(employeeid);
            objDB.ItemID = 1011;
            objDB.Assetno = "";
            objDB.NetAmount = float.Parse(amount.ToString());
            objDB.Remarks = remarks;
            objDB.Department = "Finance";
            objDB.SeperationDetailsID = Convert.ToInt32(sepID);
            //objDB.FilePath = dtItemDetails.Rows[i]["FilePath"].ToString();
            objDB.CreatedBy = Session["UserName"].ToString();
            res = objDB.AddSeperationDeductionItemOthers();
            return "";
        }

        [WebMethod(EnableSession = true)]
        public List<Loans> Getsubpurposeofwithdrawal(string val)
		{
            List<Loans> lstOfsubPurpose = new List<Loans>();
            objDB.MainPuroposeID = val;
            DataTable dt = objDB.GetSubPurpose(ref errorMsg);


            lstOfsubPurpose = (from DataRow dr in dt.Rows
                           select new Loans()
                           {
                               subpurposeid = Convert.ToInt32(dr["optionid"]),
                               subpurposename = dr["optionname"].ToString(),
                              
                           }).ToList();

           
            return lstOfsubPurpose;
        }
        [WebMethod(EnableSession = true)]
        public List<Loans> usp_TF_GetWithdrawalDocs(string val)
        {
            List<Loans> lstWithdrawalAttachments = new List<Loans>();
            objDB.SelectedPuroposeID = val;
            DataTable dt = objDB.GetWithdrawalDocs(ref errorMsg);


            lstWithdrawalAttachments = (from DataRow dr in dt.Rows
                               select new Loans()
                               {
                                   attachmentid = Convert.ToInt32(dr["attachmentoptionid"]),
                                   attachmentname = dr["attachmentname"].ToString(),

                               }).ToList();


            return lstWithdrawalAttachments;
        }
        public class FileData
        {
            arrayOfAttachments arrayOfAttachments { get; set; }
            public string Name { get; set; }
            public string Data { get; set; }
        }
        public class arrayOfAttachments
        {
            public string name { get; set; }
            public HttpPostedFile filedata { get; set; }
        }
        [WebMethod(EnableSession = true)]
        public string SubmitWithdrawalRequest()
        {

            int LoanID = 0;
            string withdrawaltype = HttpContext.Current.Request.Form.GetValues("withdrawaltype")[0].ToString();
            string temppurpose = HttpContext.Current.Request.Form.GetValues("temppurpose")[0].ToString();
            string permpurpose = HttpContext.Current.Request.Form.GetValues("permpurpose")[0].ToString();
            string subpurpose = HttpContext.Current.Request.Form.GetValues("subpurpose")[0].ToString();
            int noofinstallment = int.Parse(HttpContext.Current.Request.Form.GetValues("noofinstallment")[0].ToString());
            int amount = int.Parse(HttpContext.Current.Request.Form.GetValues("amount")[0].ToString());
            int currency = int.Parse(HttpContext.Current.Request.Form.GetValues("currency")[0].ToString());
            string notes = HttpContext.Current.Request.Form.GetValues("notes")[0].ToString();
            string title = HttpContext.Current.Request.Form.GetValues("title")[0].ToString();
            DateTime requireddate = Convert.ToDateTime(HttpContext.Current.Request.Form.GetValues("requireddate")[0].ToString());
            bool zakat = HttpContext.Current.Request.Form.GetValues("zakat")[0].ToString()=="0" ? false: true;
            string zakatamount = HttpContext.Current.Request.Form.GetValues("zakatamount")[0].ToString();
            //string rateofzakat = HttpContext.Current.Request.Form.GetValues("zakatrate")[0].ToString();
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            string mainpurpose = temppurpose == "0" ? permpurpose : temppurpose;
            float deductpermonth = noofinstallment == 0 ? amount : (amount / noofinstallment);
            string res = objDB.SubmitWithdrawalRequest(withdrawaltype, mainpurpose, subpurpose, noofinstallment, amount, deductpermonth, currency, notes, title, requireddate, zakat);
            if (int.TryParse(res, out LoanID))
            {
               
                res = "Request Submitted";
            }

			if (LoanID != 0)
			{
                // check if the user selected a file to upload
                if (HttpContext.Current.Request.Files.Count > 0)
                {
                    try
                    {
                        HttpFileCollection files = HttpContext.Current.Request.Files;
                        var key = files.AllKeys.ToArray();
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFile file = files[i];
                            string fileName = file.FileName;
                            int attachmentNumber = Convert.ToInt32(key[i]);
                            Random rand = new Random((int)DateTime.Now.Ticks);
                            int randnum = 0;

                            string fn = "";
                            string exten = "";
                            string destDir = Server.MapPath("~/assets/Attachments/PFWithdrawal/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/");
                            randnum = rand.Next(1, 100000);
                            fn = Common.RemoveSpecialCharacter(DateTime.Now.ToString("ddMMyyyy")).ToLower().Replace(" ", "-") + "_" + randnum;

                            if (!Directory.Exists(destDir))
                            {
                                Directory.CreateDirectory(destDir);
                            }
                            string fname = Path.GetFileName(file.FileName);
                            exten = Path.GetExtension(file.FileName);
                            file.SaveAs(destDir + fn + exten);
                            string Attachment = "https://" + HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + "/assets/Attachments/PFWithdrawal/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).ToLower().Replace(" ", "-") + "/" + fn + exten;
                            string uploaded = objDB.AddWithdrawalAttachments(LoanID, Attachment,attachmentNumber);


                        }


                        
                        return "Application Submitted";
                    }

                    catch (Exception e)
                    {
                        return e.Message;
                    }
                }


            }



            return "Application Submitted";
        }
  //      [WebMethod(EnableSession = true)]
		//public string SubmitWithdrawalRequest(string withdrawaltype, string temppurpose, string permpurpose, string subpurpose, int noofinstallment, int amount, int currency, string notes, string title, DateTime requireddate)
		//{
  //          //public string SubmitWithdrawalRequest(object fileData) { 
  //         // var httpPostedFile = HttpContext.Current.Request.Files["UploadedFile"];
  //          //HttpFileCollection files = HttpContext.Current.Request.Files;
  //          // check if the user selected a file to upload
  //          if (HttpContext.Current.Request.Files.Count > 0)
  //          {
  //              try
  //              {
  //                  HttpFileCollection files = HttpContext.Current.Request.Files;

  //                  HttpPostedFile file = files[0];
  //                  string fileName = file.FileName;

  //                  // create the uploads folder if it doesn't exist
  //                  //Directory.CreateDirectory(Server.MapPath("~/uploads/"));
  //                  //string path = Path.Combine(Server.MapPath("~/uploads/"), fileName);

  //                  //// save the file
  //                  //file.SaveAs(path);
  //                  //return Json("File uploaded successfully");
  //              }

  //              catch (Exception e)
  //              {

  //              }
  //          }

  //          objDB.CreatedBy = Session["UserName"].ToString();
  //          objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
  //          //         string mainpurpose = temppurpose == "0" ? permpurpose : temppurpose;
  //          //float deductpermonth = noofinstallment == 0 ? amount : (amount/ noofinstallment);
  //          //         string res = objDB.SubmitWithdrawalRequest(withdrawaltype, mainpurpose, subpurpose, noofinstallment, amount, deductpermonth, currency, notes, title, requireddate);
  //          //         return res;
  //          return "";
  //      }

        
        [WebMethod(EnableSession = true)]
        public void FnFGeneratePDF( string content)
        {
            string header = "";
            string footer = "";
            string employeename = "ok";
            string month = "jul";
			try
			{
				Common.generatePDF(header, footer, content, "FnF Slip-" + Session["UserName"].ToString() + " (" + month + ")", "A4", "Portrait", 30, 30);
			}
			catch (Exception ex)
			{ 

				throw;
			}

          
        }

        [WebMethod(EnableSession = true)]
        public string ReleaseFnF(string month, string[] employeeIDs)
        {
            // JavaScriptSerializer js = new JavaScriptSerializer();
            //  Emp[] persons = js.Deserialize<Emp[]>(employeeIDs);

            // create fnf ID
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.PayrollDate = "01-" + month;
            objDB.CreatedBy = Session["UserName"].ToString();
            int NewPayrollID = Convert.ToInt32(objDB.usp_TF_AddnewFnF());
            
            string res = "";
            
            // res = objDB.ReleaseFnF();
            res = objDB.ReleaseFnF(NewPayrollID, employeeIDs, objDB.PayrollDate, objDB.CreatedBy);
            return "";
        }
        [WebMethod(EnableSession = true)]
        public string LockPaymentFnF(string month, string[] employeeIDs)
        {
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //  Emp[] persons = js.Deserialize<Emp[]>(employeeIDs);

            string res = "";
            objDB.PayrollDate = month;
            objDB.PayrollDate = "01-" + month;
            //objDB.FilePath = dtItemDetails.Rows[i]["FilePath"].ToString();
            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            // res = objDB.ReleaseFnF();
            int NewPayrollID = Convert.ToInt32(objDB.AddNewFnFPayroll());
            res = objDB.LockPaymentFnF(NewPayrollID,employeeIDs, objDB.PayrollDate, objDB.CreatedBy);
            return "";
        }
        
        [WebMethod(EnableSession = true)]
        public List<FnF> GetFnFLockDetails(string month)
        {
            objDB.PayrollDate = month;
            objDB.PayrollDate = "01-" + month;
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"]);
            objDB.CreatedBy = Session["UserName"].ToString();

            objDB.PayrollDate = Convert.ToDateTime($"01-{month}").ToString("dd-MMM-yyyy");
            DataTable dt = objDB.GetFnFLockDetails(ref errorMsg);

            var lstFnF = new List<FnF>();


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var itemTest = new FnF
                {
                    wdid = dt.Rows[i]["WORKDAYID"].ToString(),
                    employeename = dt.Rows[i]["EmployeeName"].ToString(),
                    BasicSalary = dt.Rows[i]["BasicSal"].ToString(),
                    MedicAllowance = dt.Rows[i]["MedicAllowance"].ToString(),
                    HouseRent = dt.Rows[i]["HouseRent"].ToString(),
                    UtitlityAllownce = dt.Rows[i]["UtitlityAllownce"].ToString(),
                    GrossSalary = (Convert.ToDouble(dt.Rows[i]["BasicSal"].ToString()) + Convert.ToDouble(dt.Rows[i]["MedicAllowance"].ToString()) + Convert.ToDouble(dt.Rows[i]["HouseRent"].ToString()) + Convert.ToDouble(dt.Rows[i]["UtitlityAllownce"].ToString())).ToString(),
                    GeneralOverTimeAmount = dt.Rows[i]["GeneralOverTimeAmount"].ToString(),
                    HolidayOverTimeAmount = dt.Rows[i]["HolidayOverTimeAmount"].ToString(),
                    WeekendOverTimeAmount = dt.Rows[i]["WeekendOverTimeAmount"].ToString(),
                    EidOverTimeAmount = dt.Rows[i]["EidOverTimeAmount"].ToString(),
                    TotalOverTimeAmount = dt.Rows[i]["TotalOverTimeAmount"].ToString(),
                    TotalIncrementedAmount = dt.Rows[i]["TotalIncrementedAmount"].ToString(),
                    TotalDecrementedAmount = dt.Rows[i]["TotalDecrementedAmount"].ToString(),
                    Commission_ = dt.Rows[i]["Commission_"].ToString(),
                    SalesComission_ = dt.Rows[i]["SalesComission_"].ToString(),
                    Arrears_ = dt.Rows[i]["Arrears_"].ToString(),
                    Spiffs_ = dt.Rows[i]["Spiffs_"].ToString(),
                    Severance_ = dt.Rows[i]["Severance_"].ToString(),
                    Reimbursement_ = dt.Rows[i]["Reimbursement_"].ToString(),
                    LeaveEncashment_ = dt.Rows[i]["LeaveEncashment_"].ToString(),
                    PreviousDaysAmount_ = dt.Rows[i]["PreviousDaysAmount_"].ToString(),
                    BonusTotal_ = dt.Rows[i]["BonusTotal_"].ToString(),
                    ExpenseReimb_ = dt.Rows[i]["ExpenseReimb_"].ToString(),
                    AddonOverTime_ = dt.Rows[i]["AddonOverTime_"].ToString(),
                    AttendanceAdjustment_ = dt.Rows[i]["AttendanceAdjustment_"].ToString(),
                    PaidLeaves_ = dt.Rows[i]["PaidLeaves_"].ToString(),
                    TotalTaxableAddons_ = dt.Rows[i]["TotalTaxableAddons_"].ToString(),
                    AbsentDaysDeduction_ = dt.Rows[i]["AbsentDaysDeduction_"].ToString(),
                    AddonEOBI_ = dt.Rows[i]["AddonEOBI_"].ToString(),
                    EOBI_ = dt.Rows[i]["EOBI_"].ToString(),
                    ProvidentFund = dt.Rows[i]["ProvidentFund"].ToString(),
                    MonthlyTax = dt.Rows[i]["MonthlyTax"].ToString(),


                    OtherDeductionsInput = dt.Rows[i]["OtherDeductionsInput"].ToString(),
                    OtherDeduction = dt.Rows[i]["OtherDeduction"].ToString(), // manuall deduction by finance
                    TotalDeduction = dt.Rows[i]["TotalDeductions"].ToString(),
                    AdditionalTaxperMonth = dt.Rows[i]["AdditionalTaxperMonth"].ToString(),
                    YearlyTaxableIncome = dt.Rows[i]["YearlyTaxableIncome_"].ToString(),
                    YearlyTax = dt.Rows[i]["YearlyTaxpayable_"].ToString(),
                    AdditionalTax_ = dt.Rows[i]["AdditionalTaxperMonth"].ToString(),
                    SeperationDetailsID = dt.Rows[i]["SeperationDetailsID"].ToString(),
                    empid = dt.Rows[i]["leftempid"].ToString(),
                    PFContribution = dt.Rows[i]["PFContribution"].ToString(),
                    EmpPF = dt.Rows[i]["FinalEmpPF"].ToString(),
                    CompPF = dt.Rows[i]["FinalCompPF"].ToString(),
                    TypeOfSeperation = dt.Rows[i]["TypeOfSeperation"].ToString(),
                    ReasoForLeaving = dt.Rows[i]["ReasoForLeaving"].ToString(),
                    IsITCleared = dt.Rows[i]["IsITCleared"].ToString(),
                    ITClearedBy = dt.Rows[i]["ITClearedBy"].ToString(),
                    ITClearedOn = dt.Rows[i]["ITClearedOn"].ToString(),
                    ITComments = dt.Rows[i]["ITComments"].ToString(),
                    ITStatus = dt.Rows[i]["ITStatus"].ToString(),
                    IsHRCleared = dt.Rows[i]["IsHRCleared"].ToString(),
                    HRClearedBy = dt.Rows[i]["HRClearedBy"].ToString(),
                    HRClearedOn = dt.Rows[i]["HRClearedOn"].ToString(),
                    HRComments = dt.Rows[i]["HRComments"].ToString(),
                    HRStatus = dt.Rows[i]["HRStatus"].ToString(),
                    IsFinanceCleared = dt.Rows[i]["IsFinanceCleared"].ToString(),
                    FinanceClearedBy = dt.Rows[i]["FinanceClearedBy"].ToString(),
                    FinanceClearedOn = dt.Rows[i]["FinanceClearedOn"].ToString(),
                    FinanceComments = dt.Rows[i]["FinanceComments"].ToString(),
                    FinanceStatus = dt.Rows[i]["FinanceStatus"].ToString(),
                    ClearanceDeductionAmount = dt.Rows[i]["ClearanceDeductionAmount"].ToString(),
                    CommentsRegardingSalary = dt.Rows[i]["CommentsRegardingSalary"].ToString(),
                    EmployeeSeperationDate = dt.Rows[i]["EmployeeSeperationDate"].ToString(),
                    EmailOrCallForwardingPeriod = dt.Rows[i]["EmailOrCallForwardingPeriod"].ToString(),
                    NoticePeriodEndDate = dt.Rows[i]["NoticePeriodEndDate"].ToString(),
                    EmailIDStatus = dt.Rows[i]["EmailIDStatus"].ToString(),
                    CB = dt.Rows[i]["CB"].ToString(),
                    CD = dt.Rows[i]["CD"].ToString(),
                    ModifiedBy = dt.Rows[i]["ModifiedBy"].ToString(),
                    isDeleted = dt.Rows[i]["isDeleted"].ToString(),
                    DeletedBy = dt.Rows[i]["DeletedBy"].ToString(),
                    DeletedDate = dt.Rows[i]["DeletedBy"].ToString(),
                    DocStatus = dt.Rows[i]["DocStatus"].ToString(),
                    ApprovedBy = dt.Rows[i]["ApprovedBy"].ToString(),
                    ApprovedOn = dt.Rows[i]["ApprovedOn"].ToString(),
                    IsPayrollGenerated = dt.Rows[i]["IsPayrollGenerated"].ToString(),
                    TaxableIncome_ = dt.Rows[i]["TaxableMonthlyIncome_"].ToString(),
                    TotalDays_ = dt.Rows[i]["SalaryOfDays"].ToString(),
                    PaidAdditionalTax = dt.Rows[i]["PaidAdditionalTax_"].ToString(),
                    deptName = dt.Rows[i]["deptName"].ToString(),

                    //graduityAddonid          =    dt.Rows[i]["graduityAddonid"].ToString(),
                    Gratuity = dt.Rows[i]["Gratuity_"].ToString(),
                    //deductAddonid            =    dt.Rows[i]["deductAddonid"].ToString(),
                    AdonsDeductions = dt.Rows[i]["DeductionsFromAddons_"].ToString(),
                    NetAmount = dt.Rows[i]["NetSalary"].ToString(),

                };
                lstFnF.Add(itemTest);
            }


            return lstFnF;
        }


        [WebMethod(EnableSession = true)]
        public List<FnF> GetFnFDetails(string month)
        {
            objDB.PayrollDate = month;
            objDB.PayrollDate = "01-" + month;
            var lstFnF = new List<FnF>();
            // ispaid/is disbursed
            DataTable dt = objDB.IsFnFSettled(ref errorMsg);
            if (dt.Rows.Count > 0)
            {
                return lstFnF;
            }
			else
            {
                objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objDB.ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"]);
                objDB.CreatedBy = Session["UserName"].ToString();

                objDB.PayrollDate = Convert.ToDateTime($"01-{month}").ToString("dd-MMM-yyyy");
                 dt = objDB.GetKTPayrollDetailsForLeftEmployeesByNewPayrollID(ref errorMsg);

                


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var itemTest = new FnF
                    {
                        wdid = dt.Rows[i]["WORKDAYID"].ToString(),
                        employeename = dt.Rows[i]["EmployeeName"].ToString(),
                        BasicSalary = dt.Rows[i]["BasicSal"].ToString(),
                        MedicAllowance = dt.Rows[i]["MedicAllowance"].ToString(),
                        HouseRent = dt.Rows[i]["HouseRent"].ToString(),
                        UtitlityAllownce = dt.Rows[i]["UtitlityAllownce"].ToString(),
                        GrossSalary = (Convert.ToDouble(dt.Rows[i]["BasicSal"].ToString()) + Convert.ToDouble(dt.Rows[i]["MedicAllowance"].ToString()) + Convert.ToDouble(dt.Rows[i]["HouseRent"].ToString()) + Convert.ToDouble(dt.Rows[i]["UtitlityAllownce"].ToString())).ToString(),
                        GeneralOverTimeAmount = dt.Rows[i]["GeneralOverTimeAmount"].ToString(),
                        HolidayOverTimeAmount = dt.Rows[i]["HolidayOverTimeAmount"].ToString(),
                        WeekendOverTimeAmount = dt.Rows[i]["WeekendOverTimeAmount"].ToString(),
                        EidOverTimeAmount = dt.Rows[i]["EidOverTimeAmount"].ToString(),
                        TotalOverTimeAmount = dt.Rows[i]["TotalOverTimeAmount"].ToString(),
                        TotalIncrementedAmount = dt.Rows[i]["TotalIncrementedAmount"].ToString(),
                        TotalDecrementedAmount = dt.Rows[i]["TotalDecrementedAmount"].ToString(),
                        Commission_ = dt.Rows[i]["Commission_"].ToString(),
                        SalesComission_ = dt.Rows[i]["SalesComission_"].ToString(),
                        Arrears_ = dt.Rows[i]["Arrears_"].ToString(),
                        Spiffs_ = dt.Rows[i]["Spiffs_"].ToString(),
                        Severance_ = dt.Rows[i]["Severance_"].ToString(),
                        Reimbursement_ = dt.Rows[i]["Reimbursement_"].ToString(),
                        LeaveEncashment_ = dt.Rows[i]["LeaveEncashment_"].ToString(),
                        PreviousDaysAmount_ = dt.Rows[i]["PreviousDaysAmount_"].ToString(),
                        BonusTotal_ = dt.Rows[i]["BonusTotal_"].ToString(),
                        ExpenseReimb_ = dt.Rows[i]["ExpenseReimb_"].ToString(),
                        AddonOverTime_ = dt.Rows[i]["AddonOverTime_"].ToString(),
                        AttendanceAdjustment_ = dt.Rows[i]["AttendanceAdjustment_"].ToString(),
                        PaidLeaves_ = dt.Rows[i]["PaidLeaves_"].ToString(),
                        TotalTaxableAddons_ = dt.Rows[i]["TotalTaxableAddons_"].ToString(),
                        AbsentDaysDeduction_ = dt.Rows[i]["AbsentDaysDeduction_"].ToString(),
                        AddonEOBI_ = dt.Rows[i]["AddonEOBI_"].ToString(),
                        EOBI_ = dt.Rows[i]["EOBI_"].ToString(),
                        ProvidentFund = dt.Rows[i]["ProvidentFund"].ToString(),
                        MonthlyTax = dt.Rows[i]["MonthlyTax"].ToString(),


                        OtherDeductionsInput = dt.Rows[i]["OtherDeductionsInput"].ToString(),
                        OtherDeduction = dt.Rows[i]["OtherDeduction"].ToString(), // manuall deduction by finance
                        TotalDeduction = dt.Rows[i]["TotalDeductions"].ToString(),
                        AdditionalTaxperMonth = dt.Rows[i]["AdditionalTaxperMonth"].ToString(),
                        YearlyTaxableIncome = dt.Rows[i]["YearlyTaxableIncome_"].ToString(),
                        YearlyTax = dt.Rows[i]["YearlyTaxpayable_"].ToString(),
                        AdditionalTax_ = dt.Rows[i]["AdditionalTaxperMonth"].ToString(),
                        SeperationDetailsID = dt.Rows[i]["SeperationDetailsID"].ToString(),
                        empid = dt.Rows[i]["leftempid"].ToString(),
                        PFContribution = dt.Rows[i]["PFContribution"].ToString(),
                        EmpPF = dt.Rows[i]["FinalEmpPF"].ToString(),
                        CompPF = dt.Rows[i]["FinalCompPF"].ToString(),
                        TypeOfSeperation = dt.Rows[i]["TypeOfSeperation"].ToString(),
                        ReasoForLeaving = dt.Rows[i]["ReasoForLeaving"].ToString(),
                        IsITCleared = dt.Rows[i]["IsITCleared"].ToString(),
                        ITClearedBy = dt.Rows[i]["ITClearedBy"].ToString(),
                        ITClearedOn = dt.Rows[i]["ITClearedOn"].ToString(),
                        ITComments = dt.Rows[i]["ITComments"].ToString(),
                        ITStatus = dt.Rows[i]["ITStatus"].ToString(),
                        IsHRCleared = dt.Rows[i]["IsHRCleared"].ToString(),
                        HRClearedBy = dt.Rows[i]["HRClearedBy"].ToString(),
                        HRClearedOn = dt.Rows[i]["HRClearedOn"].ToString(),
                        HRComments = dt.Rows[i]["HRComments"].ToString(),
                        HRStatus = dt.Rows[i]["HRStatus"].ToString(),
                        IsFinanceCleared = dt.Rows[i]["IsFinanceCleared"].ToString(),
                        FinanceClearedBy = dt.Rows[i]["FinanceClearedBy"].ToString(),
                        FinanceClearedOn = dt.Rows[i]["FinanceClearedOn"].ToString(),
                        FinanceComments = dt.Rows[i]["FinanceComments"].ToString(),
                        FinanceStatus = dt.Rows[i]["FinanceStatus"].ToString(),
                        ClearanceDeductionAmount = dt.Rows[i]["ClearanceDeductionAmount"].ToString(),
                        CommentsRegardingSalary = dt.Rows[i]["CommentsRegardingSalary"].ToString(),
                        EmployeeSeperationDate = dt.Rows[i]["EmployeeSeperationDate"].ToString(),
                        EmailOrCallForwardingPeriod = dt.Rows[i]["EmailOrCallForwardingPeriod"].ToString(),
                        NoticePeriodEndDate = dt.Rows[i]["NoticePeriodEndDate"].ToString(),
                        EmailIDStatus = dt.Rows[i]["EmailIDStatus"].ToString(),
                        CB = dt.Rows[i]["CB"].ToString(),
                        CD = dt.Rows[i]["CD"].ToString(),
                        ModifiedBy = dt.Rows[i]["ModifiedBy"].ToString(),
                        isDeleted = dt.Rows[i]["isDeleted"].ToString(),
                        DeletedBy = dt.Rows[i]["DeletedBy"].ToString(),
                        DeletedDate = dt.Rows[i]["DeletedBy"].ToString(),
                        DocStatus = dt.Rows[i]["DocStatus"].ToString(),
                        ApprovedBy = dt.Rows[i]["ApprovedBy"].ToString(),
                        ApprovedOn = dt.Rows[i]["ApprovedOn"].ToString(),
                        IsPayrollGenerated = dt.Rows[i]["IsPayrollGenerated"].ToString(),
                        TaxableIncome_ = dt.Rows[i]["TaxableMonthlyIncome_"].ToString(),
                        TotalDays_ = dt.Rows[i]["SalaryOfDays"].ToString(),
                        PaidAdditionalTax = dt.Rows[i]["PaidAdditionalTax_"].ToString(),
                        deptName = dt.Rows[i]["deptName"].ToString(),

                        //graduityAddonid          =    dt.Rows[i]["graduityAddonid"].ToString(),
                        Gratuity = dt.Rows[i]["Gratuity_"].ToString(),
                        //deductAddonid            =    dt.Rows[i]["deductAddonid"].ToString(),
                        AdonsDeductions = dt.Rows[i]["DeductionsFromAddons_"].ToString(),
                        NetAmount = dt.Rows[i]["NetSalary"].ToString(),

                    };
                    lstFnF.Add(itemTest);
                }
           
           
         
         
            }


            return lstFnF;
        }


        [WebMethod(EnableSession = true)]
        public List<FnF> GetFnFReport(string month)
        {
            objDB.PayrollDate = month;
            objDB.PayrollDate = "01-" + month;
            DataTable dt = objDB.GetKTPayrollDetailsForLeftEmployeesReport(ref errorMsg);

            var lstFnF = new List<FnF>();


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var itemTest = new FnF
                {
                    wdid = dt.Rows[i]["wdid"].ToString(),
                    employeename = dt.Rows[i]["EmployeeName"].ToString(),

                    OtherDeductionsInput = dt.Rows[i]["OtherDeductionsInput"].ToString(),

                    TotalDeduction = dt.Rows[i]["TotalDeduction"].ToString(),
                    AdditionalTaxperMonth = dt.Rows[i]["AdditionalTaxperMonth"].ToString(),
                    YearlyTaxableIncome = dt.Rows[i]["YearlyTaxableIncome"].ToString(),
                    YearlyTax = dt.Rows[i]["YearlyTax"].ToString(),
                    AdditionalTax_ = dt.Rows[i]["AdditionalTax_"].ToString(),
                    SeperationDetailsID = dt.Rows[i]["SeperationDetailsID"].ToString(),
                    empid = dt.Rows[i]["empid"].ToString(),
                    PFContribution = dt.Rows[i]["PFContribution"].ToString(),
                    EmpPF = dt.Rows[i]["EmpPF"].ToString(),
                    CompPF = dt.Rows[i]["CompPF"].ToString(),
                    TypeOfSeperation = dt.Rows[i]["TypeOfSeperation"].ToString(),
                    ReasoForLeaving = dt.Rows[i]["ReasoForLeaving"].ToString(),
                    IsITCleared = dt.Rows[i]["IsITCleared"].ToString(),
                    ITClearedBy = dt.Rows[i]["ITClearedBy"].ToString(),
                    ITClearedOn = dt.Rows[i]["ITClearedOn"].ToString(),
                    ITComments = dt.Rows[i]["ITComments"].ToString(),
                    ITStatus = dt.Rows[i]["ITStatus"].ToString(),
                    IsHRCleared = dt.Rows[i]["IsHRCleared"].ToString(),
                    HRClearedBy = dt.Rows[i]["HRClearedBy"].ToString(),
                    HRClearedOn = dt.Rows[i]["HRClearedOn"].ToString(),
                    HRComments = dt.Rows[i]["HRComments"].ToString(),
                    HRStatus = dt.Rows[i]["HRStatus"].ToString(),
                    IsFinanceCleared = dt.Rows[i]["IsFinanceCleared"].ToString(),
                    FinanceClearedBy = dt.Rows[i]["FinanceClearedBy"].ToString(),
                    FinanceClearedOn = dt.Rows[i]["FinanceClearedOn"].ToString(),
                    FinanceComments = dt.Rows[i]["FinanceComments"].ToString(),
                    FinanceStatus = dt.Rows[i]["FinanceStatus"].ToString(),
                    ClearanceDeductionAmount = dt.Rows[i]["ClearanceDeductionAmount"].ToString(),
                    CommentsRegardingSalary = dt.Rows[i]["CommentsRegardingSalary"].ToString(),
                    EmployeeSeperationDate = dt.Rows[i]["EmployeeSeperationDate"].ToString(),
                    EmailOrCallForwardingPeriod = dt.Rows[i]["EmailOrCallForwardingPeriod"].ToString(),
                    NoticePeriodEndDate = dt.Rows[i]["NoticePeriodEndDate"].ToString(),
                    EmailIDStatus = dt.Rows[i]["EmailIDStatus"].ToString(),
                    CB = dt.Rows[i]["CB"].ToString(),
                    CD = dt.Rows[i]["CD"].ToString(),
                    ModifiedBy = dt.Rows[i]["ModifiedBy"].ToString(),
                    isDeleted = dt.Rows[i]["isDeleted"].ToString(),
                    DeletedBy = dt.Rows[i]["DeletedBy"].ToString(),
                    DeletedDate = dt.Rows[i]["DeletedBy"].ToString(),
                    DocStatus = dt.Rows[i]["DocStatus"].ToString(),
                    ApprovedBy = dt.Rows[i]["ApprovedBy"].ToString(),
                    ApprovedOn = dt.Rows[i]["ApprovedOn"].ToString(),
                    IsPayrollGenerated = dt.Rows[i]["IsPayrollGenerated"].ToString(),
                    TaxableIncome_ = dt.Rows[i]["TaxableIncome_"].ToString(),
                    TotalDays_ = dt.Rows[i]["TotalDays_"].ToString(),
                    PaidAdditionalTax = dt.Rows[i]["PaidAdditionalTax"].ToString(),
                    deptName = dt.Rows[i]["deptName"].ToString(),
                    
                    graduityAddonid = dt.Rows[i]["graduityAddonid"].ToString(),
                    Gratuity = dt.Rows[i]["Gratuity"].ToString(),
                    deductAddonid = dt.Rows[i]["deductAddonid"].ToString(),
                    AdonsDeductions = dt.Rows[i]["AdonsDeductions"].ToString(),
                    NetAmount = dt.Rows[i]["NetAmount"].ToString(),

                };
                lstFnF.Add(itemTest);
            }


            return lstFnF;
        }


        [WebMethod(EnableSession = true)]
        public string GetBreakupChartData(string month)
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.Month = month == "" ? DateTime.Now.ToString("MMM-yyyy") : month;
            string[] colors = new string[] { "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#2eccc1", "#c45850" };
            DataTable dt = objDB.GetPayrollBreakupChart(ref errorMsg);
            dynamic record = null;
            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    string[] Categories = dt.Rows.Cast<DataRow>().Select(row =>  row["catagory"].ToString() ).ToArray();
                    string[] Totals = dt.Rows.Cast<DataRow>().Select(row => row["Total"].ToString()).ToArray();
                    string[] Colors = new string[Categories.Length];
                    Random rnd = new Random();
                    for (int i = 0; i < Categories.Length; i++)
                    {   Colors[i] = colors[i >= colors.Length ? 0 : i] ;
                    }
                    record = new
                    {
                        catagory = Categories,//new object[] {"sdf","gdfg","gdfg","gdfg","gdfg"},
                         Totals = Totals,
                         Colors = Colors,
                    };
                }
            }
            return JsonConvert.SerializeObject(record);
        }

        [WebMethod(EnableSession = true)]
        public string GetMonthlyWiseChartData(string year)
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
             year = year == "" ? DateTime.Now.ToString("yyyy") : year;
            string[] colors = new string[] { "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#2eccc1", "#c45850" };
            DataTable dt = objDB.GetMonthlyChart(year, ref errorMsg);
            dynamic record = null;
            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    string[] Months = dt.Rows.Cast<DataRow>().Select(row => "'" + row["Months"].ToString() + "'").ToArray();
                    string[] Totals = dt.Rows.Cast<DataRow>().Select(row => row["Total"].ToString()).ToArray();
                    string[] Colors = new string[Months.Length];
                    Random rnd = new Random();
                    for (int i = 0; i < Months.Length; i++)
                    {
                        Colors[i] = colors[i >= colors.Length ? 0 : i];
                    }
                    record = new
                    {
                        months = Months ,
                        Totals = Totals,
                        Colors = Colors,
                    };
                }
            }
            return JsonConvert.SerializeObject(record);
        }
        [WebMethod(EnableSession = true)]
        public string GetPayrollDataLocationWise(string month)
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.Month = month == "" ? DateTime.Now.ToString("MMM-yyyy") : month;
            string[] colors = new string[] { "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#2eccc1", "#c45850" };
            DataTable dt = objDB.GetPayrollDataLocationWiseSP(ref errorMsg);
            dynamic record = null;
            string[] Cities = new string[0];
            string[] Total = new string[0];
            string[] Colors = new string[0];
            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    Cities = dt.Rows.Cast<DataRow>().Select(row => row["City"].ToString()).ToArray();
                    Total = dt.Rows.Cast<DataRow>().Select(row => row["Total"].ToString()).ToArray();
                    Colors = new string[Cities.Length];
                    Random rnd = new Random();
                    for (int i = 0; i < Cities.Length; i++)
                    {
                        Colors[i] = colors[i >= colors.Length ? 0 : i];
                    }
                    
                }
            }
            record = new
            {
                cities = Cities,
                Totals = Total,
                Colors = Colors,
            };
            return JsonConvert.SerializeObject(record);
        }
        [WebMethod(EnableSession = true)]
        public string GetDeptWiseChartData(string month)
        {
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.Month = month == "" ? DateTime.Now.ToString("MMM-yyyy") : month;
            string[] colors = new string[] { "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#2eccc1", "#c45850" };
            //DataTable dt = objDB.GetPayrollDataLocationWiseSP(ref errorMsg);
            DataTable dt = objDB.GetDepartWiseMonthlySalary(ref errorMsg);

            dynamic record = null;
            string[] DeptNames = new string[0];
            string[] Totals =   new string[0];
            string[] Colors =   new string[0];

            if (dt != null)
            {

                if (dt.Rows.Count > 0)
                {
                    DeptNames = dt.Rows.Cast<DataRow>().Select(row => row["DeptName"].ToString()).ToArray();
                    Totals    = dt.Rows.Cast<DataRow>().Select(row => row["Total"].ToString()).ToArray();
                    Colors    = new string[DeptNames.Length];
                    Random rnd = new Random();
                    for (int i = 0; i < DeptNames.Length; i++)
                    {
                        Colors[i] = colors[i >= colors.Length ? 0 : i];
                    }
                }
            }
            record = new
            {
                departments = DeptNames,
                Totals = Totals,
                Colors = Colors,
            };
            return JsonConvert.SerializeObject(record);
        }


        [WebMethod(EnableSession = true)]
        public string TimeIN()
        {
            DateTime date = DateTime.Now;
            objDB.date = date.ToString();
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            objDB.Status = "Time In";
            string ss = objDB.MarkTimeInAttendanceNew();
            if (ss == "Success")
            {
                Common.addlogNew("Message", "ESS", $"Time In at {date.ToString("dd-MMM-yyyy hh:mm:ss tt")}",
                    "/keeptruckin/employee-self-service/view/attendence-detail-"+ objDB.EmployeeID+"", "/keeptruckin/employee-self-service/view/attendence-detail-" + objDB.EmployeeID + "", $"Time In at {date.ToString("dd-MMM-yyyy hh:mm:ss tt")}", "EmployeeAttendance", "Direct Reporting", Convert.ToInt32(objDB.EmployeeID));
            }
           
            return string.IsNullOrEmpty(ss) ? "Time In Failed" : ss == "Success" ? "Time In Successfully" : ss;
        }

        [WebMethod(EnableSession = true)]
        public string GoogleLocation(string latitute, string longitude, string addr, string status)
        {
            DateTime date = DateTime.Now;
            objDB.date = date.ToString();
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            objDB.Status = status;
            objDB.Latitude = latitute;
            objDB.Longitude = longitude;
            objDB.GeographicalLocation = addr;

            string ss = objDB.MarkGoogleLocation();

            return "Success";
        }
        [WebMethod(EnableSession = true)]
        public string BreakTimeOut()
        {
            DateTime date = DateTime.Now;
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            string ss = objDB.MarkAttendanceTimeIn();

            if (ss == "Success")
            {
                Common.addlogNew("Message", "ESS", $"Break Start at {date.ToString("dd-MMM-yyyy hh:mm:ss tt")}",
                    "", "", $"Break Start at {date.ToString("dd-MMM-yyyy hh:mm:ss tt")}", "EmployeeAttendance", "Direct Reporting", Convert.ToInt32(objDB.EmployeeID));
            }

            return string.IsNullOrEmpty(ss) ? "Break Start Failed" : ss == "Success" ? "Break Started Successfully" : ss;
        }

        [WebMethod(EnableSession = true)]
        public string BreakTimeIN()
        {
            DateTime date = DateTime.Now;
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            string ss = objDB.MarkAttendanceTimeOut();

            if (ss == "Success")
            {
                Common.addlogNew("Message", "ESS", $"Break End at {date.ToString("dd-MMM-yyyy hh:mm:ss tt")}",
                    "", "", $"Break End at {date.ToString("dd-MMM-yyyy hh:mm:ss tt")}", "EmployeeAttendance", "Direct Reporting", Convert.ToInt32(objDB.EmployeeID));
            }

            return string.IsNullOrEmpty(ss) ? "Break End Failed" : ss == "Success" ? "Break Ended Successfully" : ss;
        }

        [WebMethod(EnableSession = true)]
        public string TimeOut()
        {

            DateTime date = DateTime.Now;
            objDB.date = date.ToString();
            objDB.EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            objDB.Status = "Time Out";

            string ss = objDB.MarkTimeInAttendanceNew();

            if (ss == "Success")
            {
                Common.addlogNew("Message", "ESS", $"Time Out at {date.ToString("dd-MMM-yyyy hh:mm:ss tt")}",
                   "/keeptruckin/employee-self-service/view/attendence-detail-" + objDB.EmployeeID + "", "/keeptruckin/employee-self-service/view/attendence-detail-" + objDB.EmployeeID + "", $"Time Out at {date.ToString("dd-MMM-yyyy hh:mm:ss tt")}", "EmployeeAttendance", "Direct Reporting", Convert.ToInt32(objDB.EmployeeID));
            }

            
            return string.IsNullOrEmpty(ss) ? "Time Out Failed" : ss == "Success" ? "Time Out Successfully" : ss;
        }

        [WebMethod(EnableSession = true)]
        public void AddToDoListItem(string Description)
        {
            objDB.UserID = Convert.ToInt32(Session["UserID"].ToString());
            objDB.Description = Description;

            string ss = objDB.AddToDoListItem();
        }

        [WebMethod(EnableSession = true)]
        public void UpdateToDoListItem(int ToDoListID, string Description)
        {
            objDB.ToDoListID = ToDoListID;
            objDB.Description = Description;

            string ss = objDB.UpdateToDoListItem();
        }

        [WebMethod(EnableSession = true)]
        public void DeleteToDoListItem(int ToDoListID)
        {
            objDB.ToDoListID = ToDoListID;
            string ss = objDB.DeleteToDoListItem();
        }

        [WebMethod(EnableSession = true)]
        public void UpdateToDoListItemStatus(int ToDoListID)
        {
            objDB.ToDoListID = ToDoListID;
            string ss = objDB.UpdateToDoListItemStatus();
        }

        [WebMethod(EnableSession = true)]
        public List<TodoList> GetToDoListByUserID()
        {
            var ToDoList = new List<TodoList>();
            objDB.UserID = Convert.ToInt32(Session["UserID"].ToString());

            DataTable dt = objDB.GetToDoListByUserID(ref errorMsg);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var itemTest = new TodoList
                        {
                            ToDoListID = dt.Rows[i]["ToDoListID"].ToString(),
                            Description = dt.Rows[i]["Description"].ToString(),
                            IsDone = dt.Rows[i]["IsDone"].ToString(),
                            Checked = dt.Rows[i]["IsDone"].ToString() == "True" ? "checked" : "",
                        };

                        ToDoList.Add(itemTest);
                    }
                }
            }

            return ToDoList;
        }

        [WebMethod(EnableSession = true)]
        public List<TodoList> GetCompletedToDoListItemsByUserID()
        {
            var ToDoList = new List<TodoList>();
            objDB.UserID = Convert.ToInt32(Session["UserID"].ToString());

            DataTable dt = objDB.GetCompletedToDoListItemsByUserID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var itemTest = new TodoList
                        {
                            ToDoListID = dt.Rows[i]["ToDoListID"].ToString(),
                            Description = dt.Rows[i]["Description"].ToString(),
                            IsDone = dt.Rows[i]["IsDone"].ToString(),
                            Checked = dt.Rows[i]["IsDone"].ToString() == "True" ? "checked" : "",
                        };

                        ToDoList.Add(itemTest);
                    }
                }
            }

            return ToDoList;
        }

        [WebMethod(EnableSession = true)]
        public List<TodoList> GetInCompleteToDoListItemsByUserID()
        {
            var ToDoList = new List<TodoList>();
            objDB.UserID = Convert.ToInt32(Session["UserID"].ToString());

            DataTable dt = objDB.GetInCompleteToDoListItemsByUserID(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var itemTest = new TodoList
                        {
                            ToDoListID = dt.Rows[i]["ToDoListID"].ToString(),
                            Description = dt.Rows[i]["Description"].ToString(),
                            IsDone = dt.Rows[i]["IsDone"].ToString(),
                            Checked = dt.Rows[i]["IsDone"].ToString() == "True" ? "checked" : "",
                        };

                        ToDoList.Add(itemTest);
                    }
                }
            }

            return ToDoList;
        }

        [WebMethod(EnableSession = true)]
        public string AttendanceAdjustment(string DateAdjust, string timein, string timeout, string remarks, string EmpID)
        {
            DateTime Fromdate = DateTime.Parse(timein), Todate = DateTime.Parse(timeout);
            DateTime Date = DateTime.Parse(DateAdjust);
            Fromdate = DateTime.Parse(Date.ToString("dd-MMM-yyyy") + " " + Fromdate.ToString("hh:mm:ss tt"));
            Todate = DateTime.Parse(Date.ToString("dd-MMM-yyyy") + " " + Todate.ToString("hh:mm:ss tt"));
            Todate = Todate < Fromdate ? Todate.AddDays(1) : Todate;

            if ((Todate - Fromdate).TotalHours > 16)
            {
                return "Invalid Timing";
            }
            else
            {
                objDB.Date = Date.ToString("dd-MMM-yyyy");
                objDB.StartDate = Fromdate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                objDB.EndDate = Todate.ToString("dd-MMM-yyyy hh:mm:ss tt");
                objDB.EmployeeID = Convert.ToInt32(EmpID);
            }

            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"].ToString());
            objDB.EmployeeID = Convert.ToInt32(EmpID);
            objDB.TimeIn = Fromdate.ToString("dd-MMM-yyyy hh:mm:ss tt");
            objDB.TimeOUt = Todate.ToString("dd-MMM-yyyy hh:mm:ss tt");
            objDB.BreakStartTime = "";
            objDB.BreakEndTime = "";
            objDB.Resultant = remarks;
            objDB.Date = Date.ToString("dd-MMM-yyyy");

            int AdjustmentId = 0;
            string s = objDB.AddAttendanceAdjustment(ref AdjustmentId);

            if (s == "New Attendance Adjustment Added")
            {
                Common.addlogNew("Data Submitted for Review", "AttendanceAdjustment",
                    "Add Attendance Adjustment \"" + objDB.Title + "\" Added",
                    "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + AdjustmentId.ToString(),
                    "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + AdjustmentId.ToString(), "Attendance Adjustment", "AttendanceAdjustment", "Direct Reporting");
            }

            return s;
        }

        [WebMethod(EnableSession = true)]
        public string ChangeShiftAndOvertime(string EmployeeID, string isOverTime, string HolidayWorking)
        {
            objDB.EmployeeID = Convert.ToInt32(EmployeeID);
            objDB.isOverTime = (isOverTime == "True") ? true : false;
            objDB.isHolidayWorking = (HolidayWorking == "True") ? true : false;
            return objDB.UpdateEmployeeShiftAndOvertimeOnly();
        }

        [WebMethod(EnableSession = true)]
        public string MakeIndirectMgr(string IndirectMgr, string status)
        {
            objDB.directMgr = Convert.ToInt32(Session["EmployeeID"].ToString());
            objDB.IndirectMgr = Convert.ToInt32(IndirectMgr);
            objDB.isMgr = (status == "True") ? true : false;
            objDB.createdby = Convert.ToInt32(Session["EmployeeID"].ToString());


            return objDB.UpdateIndirectPerson();
          


        }
        [WebMethod(EnableSession = true)]
        public string MakeIndirectMgrByHRFinance(string directMgr ,string IndirectMgr, string status)
        {
            objDB.directMgr = Convert.ToInt32(directMgr);
            objDB.IndirectMgr = Convert.ToInt32(IndirectMgr);
            objDB.isMgr = (status == "True") ? true : false;
            objDB.createdby = Convert.ToInt32(Session["EmployeeID"].ToString());
            return objDB.UpdateIndirectPerson();



        }
        [WebMethod(EnableSession = true)]
       
        public string GetTeamMembers(string directMgr)
        {
            objDB.directMgr = Convert.ToInt32(directMgr);


            DataTable dt = objDB.GetAllEmployeesByDirectReporting(ref errorMsg);
            return JsonConvert.SerializeObject(dt);


        }
        [WebMethod(EnableSession = true)]
        public string ApproveIT3(string IT3_IT, string Remarks)
        {
            objDB.Remarks = Remarks;
            objDB.IT3_ID = int.Parse(IT3_IT);
            objDB.ApprovedBy = Session["UserName"].ToString();
            objDB.approveIT3();

            return IT3_IT;
        }

        [WebMethod(EnableSession = true)]
        public string BreakAdjustment(string DateAdjust, string breakTimeIn, string breakTimeOut, string remarks, string EmpID)
        {
            DateTime breakStart = DateTime.Parse(breakTimeIn), breakEnd = DateTime.Parse(breakTimeOut);
            DateTime Date = DateTime.Parse(DateAdjust);
            breakStart = DateTime.Parse(Date.ToString("dd-MMM-yyyy") + " " + breakStart.ToString("hh:mm:ss tt"));
            breakEnd = DateTime.Parse(Date.ToString("dd-MMM-yyyy") + " " + breakEnd.ToString("hh:mm:ss tt"));
            breakEnd = breakEnd < breakStart ? breakEnd.AddDays(1) : breakEnd;

            if ((breakEnd - breakStart).TotalHours > 12)
            {
                return "Invalid Timing";
            }
            else
            {
                objDB.Date = Date.ToString("dd-MMM-yyyy");
                objDB.BreakStartTime = breakStart.ToString("dd-MMM-yyyy hh:mm:ss tt");
                objDB.BreakEndTime = breakEnd.ToString("dd-MMM-yyyy hh:mm:ss tt");
                objDB.EmployeeID = Convert.ToInt32(EmpID);
            }

            objDB.CreatedBy = Session["UserName"].ToString();
            objDB.EmployeeID = Convert.ToInt32(EmpID);
            objDB.Resultant = remarks;
            objDB.Date = Date.ToString("dd-MMM-yyyy");

            int AdjustmentId = 0;
            string s = objDB.AddBreakAdjustment(ref AdjustmentId);
            if (s == "New Break Adjustment Added")
            {
                Common.addlogNew("Data Submitted for Review", "BreakAdjustment",
                    "Add Break Adjustment \"" + objDB.Title + "\" Added",
                    "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + AdjustmentId.ToString(),
                    "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", " - ").ToLower() + "/employee-self-service/manage/attendance-adjustment/edit-attendance-adjustment-" + AdjustmentId.ToString(), "Break Adjustment", "BreakAdjustment", "Direct Reporting");
            }

            Common.addlogNew("Add", "BreakAdjustment", "Add Break Adjustment \"" + objDB.Title + "\" Added", "DocumentViewPageLink", "", "Break Adjustment", "BreakAdjustment", "BreakAdjustment");
            return s;
        }

        [WebMethod(EnableSession = true)]
        public string GetDepartmentWiseBreakupChartData(string month)
        {
            string[] colors = new string[] { "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#2eccc1", "#c45850" };
            PayrollBreakup payrollBreakup = new PayrollBreakup();

            objDB.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objDB.Month = month == "" ? DateTime.Now.ToString("MMM-yyyy") : month;

            DataTable dt = objDB.GetPayrollBreakupChart(ref errorMsg);

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    string[] Categories = dt.Rows.Cast<DataRow>().Select(row => "'" + row["catagory"].ToString() + "'").ToArray();
                    string[] Totals = dt.Rows.Cast<DataRow>().Select(row => row["Total"].ToString()).ToArray();
                    string[] Colors = new string[Categories.Length];
                    string strcategory = string.Join(",", Categories);
                    string strtotal = string.Join(",", Totals);

                    Random rnd = new Random();

                    for (int i = 0; i < Categories.Length; i++)
                    {
                        Colors[i] = "'" + colors[i >= colors.Length ? 0 : i] + "'";
                    }

                    string color = string.Join(",", Colors);
                    //str2 = @"<script> new Chart(document.getElementById('BreakupPayrollChart'), { type: 'bar', data: { labels:[" + strcategory + "], datasets: [ { label: '', backgroundColor:[" + color + "], data:[" + strtotal + "] } ] }, options: { legend: { display: false },scales: {xAxes:[{ticks:{autoSkip: false}}]}, title: { display: true, text: '' } } }); </script> ";
                    payrollBreakup.Category = strcategory;
                    payrollBreakup.TotalAmount = strtotal;
                    payrollBreakup.Color = color;
                }
            }

            return JsonConvert.SerializeObject(payrollBreakup);
        }
    }
}
