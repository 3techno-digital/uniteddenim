﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Net.Security;

namespace Technofinancials.business.services
{
    /// <summary>
    /// Summary description for Login
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Login : System.Web.Services.WebService
    {
        DBQueries objDB = new DBQueries();
        private const string apiKey = "3Techno*0519!";
        private const string encKey = "TF2o!eight";
        string errorMsg = "";


        private string CheckPassKeys()
        {
            string returnMsg = "";
            string ConfigAPIKey = ConfigurationManager.AppSettings["APIKey"].ToString();
            string ConfigEncKey = ConfigurationManager.AppSettings["EncKey"].ToString();

            if (!string.IsNullOrEmpty(ConfigEncKey))
            {
                if (ConfigEncKey == encKey)
                {
                    if (!string.IsNullOrEmpty(ConfigAPIKey))
                    {
                        if (ConfigAPIKey == apiKey)
                        {
                            returnMsg = "Passkeys Authenticated";
                        }
                        else
                        {
                            returnMsg = "Invalid API Key";
                        }
                    }
                    else
                    {
                        returnMsg = "API Key Not Defined";
                    }
                }
                else
                {
                    returnMsg = "Invalid Encryption Key";
                }
            }
            else
            {
                returnMsg = "Encryption Key Not Defined";
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string CheckSessions()
        {
            string returnMsg = "false";
            if (Session["UserID"] != null && Session["EmployeeID"] != null)
                returnMsg = "true";

            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string CheckAccessLevel()
        {
            string url = "/" + Common.RemoveSpecialCharacter(Session["CompanyName"].ToString()).Replace(" ", "-").ToLower() +"/";

            if (Session["DeptName"].ToString() == "Directors")
            {
                url += "directors/view/companies";
            }

            return url;
        }

        [WebMethod(EnableSession = true)]
        public string LogOut()
        {
            string returnMsg = "false";

            Session["UserID"] = null;
            Session["EmployeeID"] = null;
            Session["UserEmail"] = null;
            Session["UserName"] = null;
            Session["UserPhoto"] = null;
            Session["CompanyID"] = null;
            Session["CompanyName"] = null;
            Session["DeptID"] = null;
            Session["DesgID"] = null;

            returnMsg = "true";
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string ResetViaEmail()
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                try
                {
                    DBQueries objDB = new DBQueries();
                    string errorMsg = "";
                    objDB.Email = Session["UserEmail"].ToString();

                    DataTable dt = new DataTable();
                    dt = objDB.GetUserDetailsByEmail(ref errorMsg);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            objDB.UserID = Convert.ToInt32(dt.Rows[0]["UserID"].ToString());
                            objDB.Email = dt.Rows[0]["Email"].ToString();

                            Random rand = new Random((int)DateTime.Now.Ticks);
                            int randnum = 0;
                            randnum = rand.Next(1, 100000);
                            objDB.PasswordKey = randnum.ToString();
                            objDB.AddPasswordKey();

                            string passLink = "http://technofinancials.com/reset-password?user-email=" + dt.Rows[0]["Email"].ToString() + "&&access-token=" + Common.Encrypt(randnum.ToString());

                            string file = "";
                            string html = "";
                            file = System.Web.HttpContext.Current.Server.MapPath("/assets/emails/email.html");
                            StreamReader sr = new StreamReader(file);
                            FileInfo fi = new FileInfo(file);

                            if (System.IO.File.Exists(file))
                            {
                                html += sr.ReadToEnd();
                                sr.Close();
                            }

                            html = html.Replace("##NAME##", dt.Rows[0]["UserName"].ToString());
                            html = html.Replace("##PASSWORD##", "Please use the following link to reset password. Link is only useable once. <br /> " + passLink);

                            MailMessage mail = new MailMessage();
                            mail.Subject = "User Account Credentials for Technofinancials";
                            mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString(), "Techno Financials");
                            mail.To.Add(Session["UserEmail"].ToString());
                            mail.Body = html;
                            mail.IsBodyHtml = true;

                            SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SenderSMTP"].ToString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SenderPort"].ToString()));
                            smtp.EnableSsl = true;
                            NetworkCredential netCre = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["SenderPassword"].ToString());
                            smtp.Credentials = netCre;
                            ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                            smtp.Send(mail);

                            returnMsg = "An Email has been sent to your email address, please check your inbox";
                        }
                        else
                        {
                            returnMsg = "Invalid Email Address";
                        }
                    }
                    else
                    {
                        returnMsg = "Invalid Email Address";
                    }
                }
                catch (Exception ex)
                {
                    returnMsg = ex.Message;
                }
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string AuthenticateUser(string userEmail, string userPassword)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.Email = userEmail;
                if (string.IsNullOrEmpty(userEmail))
                {
                    if (Session["UserEmail"] != null)
                    {
                        objDB.Email = Session["UserEmail"].ToString();
                    }
                }
                objDB.Password = userPassword;
                DataTable dt = objDB.AuthenticateUser(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        returnMsg = "Logged In Successfully";
                        if (dt.Rows[0][0].ToString() != "Invalid Email Address" && dt.Rows[0][0].ToString() != "Account Not Approved" && dt.Rows[0][0].ToString() != "Wrong Password!!")
                        {
                            Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                            Session["EmployeeID"] = dt.Rows[0]["EmployeeID"].ToString();
                            Session["UserEmail"] = dt.Rows[0]["Email"].ToString();
                            Session["UserPassword"] = dt.Rows[0]["Password"].ToString();
                            Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                            Session["UserPhoto"] = dt.Rows[0]["Photo"].ToString();
                            Session["CompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            Session["CompanyName"] = dt.Rows[0]["CompanyName"].ToString();
                            Session["DeptID"] = dt.Rows[0]["DeptID"].ToString();
                            Session["DeptName"] = dt.Rows[0]["DeptName"].ToString();
                            Session["DesgID"] = dt.Rows[0]["DesgID"].ToString();
                            Session["CompanyLogo"] = dt.Rows[0]["CompanyLogo"].ToString();

                            if (dt.Rows[0]["isNew"].ToString() == "True")
                            {
                                Session["isNewUser"] = "1";
                                returnMsg = "New User";
                            }
                        }
                        else
                            returnMsg = dt.Rows[0][0].ToString();
                    }
                    else
                    {
                        returnMsg = errorMsg;
                    }
                }
                else
                {
                    returnMsg = errorMsg;
                }
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string GetUserDetailsByEmail(string userEmail)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.Email = userEmail;
                DataTable dt = objDB.GetUserDetailsByEmail(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        returnMsg = "User Exist";

                        if (dt.Rows[0][0].ToString() != "Invalid Email Address" && dt.Rows[0][0].ToString() != "Account Not Approved")
                        {
                            Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                            Session["EmployeeID"] = dt.Rows[0]["EmployeeID"].ToString();
                            Session["UserPassword"] = dt.Rows[0]["Password"].ToString();
                            Session["UserEmail"] = dt.Rows[0]["Email"].ToString();
                            Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                            Session["UserPhoto"] = dt.Rows[0]["Photo"].ToString();
                            Session["CompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            Session["CompanyName"] = dt.Rows[0]["CompanyName"].ToString();
                            Session["DeptID"] = dt.Rows[0]["DeptID"].ToString();
                            Session["DesgID"] = dt.Rows[0]["DesgID"].ToString();
                        }
                        else
                            returnMsg = dt.Rows[0][0].ToString();
                    }
                    else
                    {
                        returnMsg = errorMsg;
                    }
                }
                else
                {
                    returnMsg = errorMsg;
                }
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string VerifySecurityAnswers(string Ques1, string Ans1, string Ques2, string Ans2)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.UserID = Convert.ToInt32(Session["UserID"].ToString());
                objDB.Ques1 = Ques1;
                objDB.Ans1 = Ans1;
                objDB.Ques2 = Ques2;
                objDB.Ans2 = Ans2;
                DataTable dt = objDB.VerifySecurityAnswers(ref errorMsg);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0][0].ToString() != "Invalid Questions and Answers")
                        {
                            Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                            Session["EmployeeID"] = dt.Rows[0]["EmployeeID"].ToString();
                            Session["UserEmail"] = dt.Rows[0]["Email"].ToString();
                            Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                            Session["UserPassword"] = dt.Rows[0]["Password"].ToString();
                            Session["UserPhoto"] = dt.Rows[0]["Photo"].ToString();
                            Session["CompanyID"] = dt.Rows[0]["CompanyID"].ToString();
                            Session["CompanyName"] = dt.Rows[0]["CompanyName"].ToString();
                            Session["DeptID"] = dt.Rows[0]["DeptID"].ToString();
                            Session["DesgID"] = dt.Rows[0]["DesgID"].ToString();

                            returnMsg = "Valid Answsers";
                        }
                        else
                           returnMsg = dt.Rows[0][0].ToString();
                    }
                    else
                    {
                        returnMsg = errorMsg;
                    }
                }
                else
                {
                    returnMsg = errorMsg;
                }
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string GetPasswordKeyByEmail(string userEmail, string passwordKey)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.Email = userEmail;
                objDB.PasswordKey = Common.Decrypt(passwordKey);
                DataTable dt = objDB.GetPassowrdKeyByEmail(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        returnMsg = "Valid Access Token";
                    }
                    else
                    {
                        returnMsg = "Link Expired !! Please <a href='/forgot-password'>Click Here</a> to generate new Link";
                    }
                }
                else
                {
                    returnMsg = "Link Expired !! Please <a href='/forgot-password'>Click Here</a> to generate new Link";
                }
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string AddUserSecurityAnswers(string Ques1ID, string Ans1, string Ques2ID, string Ans2)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.UserID = Convert.ToInt32(Session["UserID"]);
                objDB.SQuesID = Convert.ToInt32(Ques1ID);
                objDB.SQAnswer = Ans1;
                returnMsg = objDB.AddUserSecurityAnswers();


                objDB.UserID = Convert.ToInt32(Session["UserID"]);
                objDB.SQuesID = Convert.ToInt32(Ques2ID);
                objDB.SQAnswer = Ans2;
                returnMsg = objDB.AddUserSecurityAnswers();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string UpdatePassword(string Password)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                objDB.UserID = Convert.ToInt32(Session["UserID"]);
                objDB.Password = Password;
                returnMsg = objDB.UpdatePassword();
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateUserPassword(string oldPassword, string newPassword)
        {
            string returnMsg = "";
            string chkPassKeys = CheckPassKeys();

            if (chkPassKeys == "Passkeys Authenticated")
            {
                if (Session["UserPassword"].ToString() == oldPassword)
                {
                    Session["UserPassword"] = newPassword;
                    objDB.UserID = Convert.ToInt32(Session["UserID"]);
                    objDB.Password = newPassword;
                    returnMsg = objDB.UpdatePassword();
                }
                else
                    returnMsg = "Wrong Password";
            }
            else
            {
                returnMsg = chkPassKeys;
            }
            return returnMsg;
        }

        [WebMethod]
        public void GetSecurityQuestionDD1()
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                var lstQues = new List<SecurityQuestions>();
                DataTable dt = objDB.GetSecurityQuestionDD1(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemTest = new SecurityQuestions
                            {
                                QuesID = dt.Rows[i]["QuesID"].ToString(),
                                Ques = dt.Rows[i]["Ques"].ToString()
                            };
                            lstQues.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstQues));
            }
        }


        [WebMethod]
        public void GetSecurityQuestionDD2()
        {
            string chkPassKeys = CheckPassKeys();
            if (chkPassKeys == "Passkeys Authenticated")
            {
                var lstQues = new List<SecurityQuestions>();
                DataTable dt = objDB.GetSecurityQuestionDD2(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemTest = new SecurityQuestions
                            {
                                QuesID = dt.Rows[i]["QuesID"].ToString(),
                                Ques = dt.Rows[i]["Ques"].ToString()
                            };
                            lstQues.Add(itemTest);
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(lstQues));
            }
        }
    }
}
