﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using Technofinancials.business.classes;

namespace Technofinancials.business.services
{
    /// <summary>
    /// Summary description for NotificationService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class NotificationService : System.Web.Services.WebService
    {

        DBQueries objDB = new DBQueries();
        string errorMsg = "";
        [WebMethod(EnableSession = true)]
        public List<Notification> GetAllNotificationsByUserID()
        {
            
                objDB.UserID = Convert.ToInt32(Session["UserID"]);
                var lstGrades = new List<Notification>();
                DataTable dt = objDB.GetAllNotificationsByUserID(ref errorMsg);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var itemTest = new Notification
                            {
                                NotificationMsg = dt.Rows[i]["NotificationMsg"].ToString(),
                                NotificationID = dt.Rows[i]["NotificationID"].ToString(),
                                DocumentViewPageLink = dt.Rows[i]["DocumentViewPageLink"].ToString(),
                                DocumentManagePageLink = dt.Rows[i]["DocumentManagePageLink"].ToString(),
                                CreatedBy = dt.Rows[i]["CreatedByName"].ToString(),
                                CreatedDate = dt.Rows[i]["CreatedDate"].ToString(),
                                UserID = dt.Rows[i]["UserID"].ToString(),
                                isBell = dt.Rows[i]["isBell"].ToString()
                            };
                            lstGrades.Add(itemTest);
                        }
                    }
                }

            return lstGrades;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateNotificationViewAll()
        {
            string returnMsg = "";
            objDB.UserID = Convert.ToInt32(Session["UserID"]);
            returnMsg = objDB.UpdateNotificationViewAll();
            return returnMsg;
        } 
        
        [WebMethod(EnableSession = true)]
        public string UpdateNotificationView(string NotificationID)
        {
            string returnMsg = "";
            objDB.NotificationID = Convert.ToInt32(NotificationID);
            returnMsg = objDB.UpdateNotificationView();             
            return returnMsg;
        }


        


    }
}
